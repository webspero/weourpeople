<?php if (defined('HTML5_ENABLED') && HTML5_ENABLED == 1){ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php } else { ?>
<!DOCTYPE html>

<?php } ?>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#">
<head>
    <?php  if (FAVICON) { ?>
        <link rel="icon" type="image/png"
              href="<?php echo ConfigUrl::root(); ?>project/styles/images/favicon.ico">
    <?php } ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title><?php echo((PageContext::$metaTitle != '') ? PageContext::$metaTitle : META_TITLE); ?></title>
    <meta name="description"
          content="<?php echo((PageContext::$metaDes != '') ? PageContext::$metaDes : META_DES); ?>"/>
    <meta name="keywords"
          content="<?php echo((PageContext::$metaKey != '') ? PageContext::$metaKey : META_KEYWORDS); ?>"/>
    <?php if (PageContext::$Ogfbpage) { ?>
        <meta property="og:url" content="<?php echo PageContext::$ogurl; ?>"/>
        <meta property="og:type" content="<?php echo PageContext::$ogtype; ?>"/>
        <meta property="og:title" content="<?php echo PageContext::$ogtitle; ?>"/>
        <meta property="og:description" content="<?php echo PageContext::$ogdescription; ?>"/>
        <meta property="og:image" content="<?php echo PageContext::$ogimage; ?>"/>
        <?php
    } ?>
    <!-- Style Sheets -->
    <link rel="stylesheet" href="<?php echo ConfigUrl::root() ?>public/styles/jquery-ui-1.8.23.custom.css"
          type="text/css" media="screen" title="default"/>
    <link rel="stylesheet" href="<?php echo ConfigUrl::root() ?>public/styles/fw.css" type="text/css" media="screen"
          title="default"/>
    <link rel="stylesheet" href="<?php echo ConfigUrl::root(); ?>project/styles/app.css" type="text/css" media="screen"
          title="default"/>
    <link rel="stylesheet" href="<?php echo ConfigUrl::root(); ?>project/styles/magnific-popup.css" type="text/css" media="screen"
          title="default"/>
    <link rel="stylesheet" href="<?php echo ConfigUrl::root(); ?>public/styles/sweetalert.css" type="text/css" media="screen"
          title="default"/>
    <link rel="stylesheet" href="<?php echo ConfigUrl::root(); ?>public/styles/jquery.emojiarea.css" type="text/css" media="screen" title="default"/>
    <?php if (PageContext::$enableBootStrap) { ?>
        <link href="<?php echo ConfigUrl::root(); ?>public/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <?php } ?>

    <!-- Add StyleSheets -->
    <?php
    if (PageContext::$styleObj) {
        foreach (PageContext::$styleObj->urls as $url) {
            ?>
            <?php if (preg_match('/http:/', $url)) { ?>
                <link rel="stylesheet" href="<?php echo $url; ?>" type="text/css"/>
            <?php } else { ?>
                <link rel="stylesheet" href="<?php echo ConfigUrl::root(); ?>project/styles/<?php echo $url; ?>"
                      type="text/css"/>
            <?php }
        }
    }
    ?>

    <!-- Add Theme BasedStyleSheets -->
    <?php
    if (PageContext::$themeStyleObj) {
        foreach (PageContext::$themeStyleObj->urls as $url) {
            ?>
            <link rel="stylesheet" href="<?php PageContext::printThemePath(); ?>css/<?php echo $url; ?>"
                  type="text/css"/>
            <?php
        }
    }

    ?>
            <?php if(CHAT_DISPLAY == 1 && !PageContext::$isCMS){ ?>
    <link rel="stylesheet" href="<?php echo ConfigUrl::root() ?>project/styles/chat.css" type="text/css" media="screen"
          title="default"/>
            <?php } ?>
    <link rel="stylesheet" href="<?php echo ConfigUrl::root() ?>project/styles/screen.css" type="text/css"
          media="screen" title="default"/>
   
    <!-- Add JS Vars -->
    <script type="text/javascript">
        <?php
        if (PageContext::$jsVarsObj) {
            foreach (PageContext::$jsVarsObj->jsvar as $jsvar) {
                echo 'var ' . $jsvar->variable . ' = "' . $jsvar->value . '";';
            }
        }
        ?>
        var slideRight;
        var slideLeft;
        var FB_APP_ID = '<?php echo FB_APP_ID; ?>';

        <?php if(isset($_SESSION['default_user_id'])) {?>
        var isUserLoggedIn = true;
        <?php } else { ?>
        var isUserLoggedIn = false;
        <?php } ?>
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript"></script>
    <!-- JS Files -->
    <?php if (PageContext::$includeLatestJquery) { ?>
        <script src="<?php echo ConfigUrl::root()?>public/js/jquery-1.11.3.min.js" type="text/javascript"></script>
        <script src="<?php echo ConfigUrl::root() ?>public/js/jquery-migrate-1.2.1.min.js"
                type="text/javascript"></script>
    <?php } ?>

    <script src="<?php echo ConfigUrl::root() ?>public/js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script src="<?php echo ConfigUrl::root() ?>public/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>project/js/pgwmodal.min.js"></script>
    <script src="<?php echo ConfigUrl::root(); ?>public/js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="<?php echo ConfigUrl::root() ?>public/js/fw.js" type="text/javascript"></script>
    <script src="<?php echo ConfigUrl::root(); ?>public/js/jquery.emojiarea.js" type="text/javascript"></script>
    <script src="<?php echo ConfigUrl::root(); ?>public/js/emojis.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>project/js/jquery.metadata.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>project/js/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo ConfigUrl::root(); ?>project/js/app.js" type="text/javascript"></script>
    <script src="<?php echo ConfigUrl::root(); ?>public/js/sweetalert.min.js" type="text/javascript"></script>
    <?php if(CHAT_DISPLAY == 1 && !PageContext::$isCMS){ ?>
    <script src="<?php echo ConfigUrl::root(); ?>project/js/chat.js" type="text/javascript"></script>
    <?php } ?>
    <script src="<?php echo ConfigUrl::root(); ?>project/js/jquery.idle.js" type="text/javascript"></script>
    <?php if(CHAT_DISPLAY == 1 && !PageContext::$isCMS){ ?>
    <script src="<?php echo ConfigUrl::root(); ?>project/js/useractivity.js" type="text/javascript"></script>
    <?php } ?>
    <script src="<?php echo ConfigUrl::root(); ?>project/js/jquery.magnific-popup.min.js" type="text/javascript"></script>

    

    <?php if (PageContext::$enableBootStrap) { ?>
        <script src="<?php echo ConfigUrl::root(); ?>public/bootstrap/js/bootstrap.min.js"></script>
    <?php } ?>
    <?php if (PageContext::$enableFusionchart) { ?>
        <script src="<?php echo ConfigUrl::root(); ?>public/fusioncharts/JSClass/FusionCharts.js"
                type="text/javascript"></script>
    <?php } ?>
    <?php if (PageContext::$enableFCkEditor) { ?>
        <script src="<?php echo ConfigUrl::root(); ?>public/fckeditor/fckeditor.js" type="text/javascript"></script>
    <?php } ?>
    <!-- Add Scripts -->
    <?php
    if (PageContext::$scriptObj) {
        foreach (PageContext::$scriptObj->urls as $url) {
            ?>

            <?php if ((preg_match('/http:/', $url)) || (preg_match('/https:/', $url))) { ?>
                <script src="<?php echo $url; ?>" type="text/javascript"></script>
            <?php }else { ?>
                <script src="<?php echo ConfigUrl::root(); ?>project/js/<?php echo $url; ?>"
                        type="text/javascript"></script>
            <?php }
        }
    }
    ?>
    <!-- Head Code Snippet -->
    <?php if (PageContext::$headerCodeSnippet) echo PageContext::$headerCodeSnippet; ?>
</head>
<body class='<?php if (PageContext::$body_class) echo PageContext::$body_class; ?>'
      id="<?php if (PageContext::$body_id) echo PageContext::$body_id; ?>">
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: FB_APP_ID,
            xfbml: true,
            version: 'v2.7'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

</script>
<?php

if (DYNAMIC_THEME_ENABLED == true && PageContext::$isCMS == false) {
    echo PageContext::renderCurrentTheme();
} else {
    echo $this->_layout;
}

?>
<!-- Footer Code Snippet -->
<?php if (PageContext::$footerCodeSnippet) echo PageContext::$footerCodeSnippet; ?>
</body>
    <?php if (FB_ENABLED && !isset($_SESSION['default_user_id'])) { ?>
        <script src="<?php echo ConfigUrl::root(); ?>project/js/facebook.js" type="text/javascript"></script>
    <?php } ?>
</html>