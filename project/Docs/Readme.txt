******************************************************************************************
                        iScripts.com Socialware 4.1 Readme File
                                  December 06, 2016
******************************************************************************************
(c) Copyright Armia Systems,Inc 2016. All rights reserved.

This file contains information that describes the installation of iScripts Socialware


******************************************************************************************
Contents
******************************************************************************************
1.0 Introduction
2.0 Requirements
3.0 Installing iScripts Socialware 4.1

******************************************************************************************
1.0 Introduction
******************************************************************************************
This file contains important information you should read before installing iScripts
Socialware.

For any support, visit us at http://www.iscripts.com/support/kb/

******************************************************************************************
2.0 Requirements
******************************************************************************************

The iScripts Socialware is developed in PHP and the database is MySQL.

The server requirements can be summarized as given below:
	1. PHP > 5.2
	You can get the latest PHP version at http://www.php.net
	2. MySQL > 3.x.x
	
Other requirements for trouble free installation/working

	1. SendMail - (Yes)
	2. PHP safe mode - (OFF)
	3. PHP register_globals - (ON)
	4. PHP open_basedir - (OFF)
	5. PHP magic_quotes_gpc - (ON)
	6. PHP short_open_tag - (ON)
	7. CURL extension - (Yes)


******************************************************************************************
3.0 Installing iScripts Socialware
******************************************************************************************

3.1) Unzip the entire contents to a folder of your choice.

	a) Upload the contents to the server to the desired folder using an FTP client.  If you do not 	have a FTP client we suggest  CoreFTP or FTPzilla

3.2) Set 'Write' permission to the following files/folders.
files/",
    1. files/medium
    2. files/small
    3. files/thumb
    4. files/default
    5. files/timeline
    6. files/logo/
    7. temp/templates_c 
    8. styles/images


	
3.3) Provide 'Write' permission for the following files.
	
    1. config/config.php
    2. config/settings.php
	

3.4) Run the following URL in your browser and follow the instructions.

	http://www.yoursitename/install/

	If you have uploaded the files in the root (home directory), you can access the iScripts Socialware install wizard at http://www.yoursitename

	You can also install the script in any directory under the root. For example if you have uploaded the zip file in a directory like http://www.yoursitename/Socialware then you can access the Socialware site at http://www.yoursitename/Socialware

	In the installation step, please provide the site url as described above, without any trailing 	slashes.

3.5) Make sure you enter the same license key you received at the time of purchase,in the
	"License Key" field. The script would function only for the domain it is licensed. If you cannot recall the license its also included in the email you received with subject:iScripts.com software download link . You can also get the license key from your user panel at www.iscripts.com

3.6) Remove the 'Write' permission provided to the file 'settings.php'.

3.7) Delete the 'install' folder.

3.8) Change the settings to match your specific requirements
	at http://www.yoursitename/admin/


