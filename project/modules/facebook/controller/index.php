<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
// +----------------------------------------------------------------------+
// | This page is for user section management. Login checking , new user registration, user listing etc.                                      |
// | File name : index.php                                                  |
// | PHP version >= 5.2                                                   |
// | Created On	: 	Nov 17 2011                                               |
// +----------------------------------------------------------------------+
// | Author: Jinson Mathew <jinson.m@armiasystems.com>              |
// +----------------------------------------------------------------------+
// | Copyrights Armia Systems ? 2010                                      |
// | All rights reserved                                                  |
// +------------------------------------------------------


class ControllerIndex extends BaseController
{
	/*
		construction function. we can initialize the models here
	*/
     public function init()
    {
        parent::init();
		
		$this->_common				= new ModelCommon;

        }
    
	public function authenticate()
	{
	
	}
	
	/*
	function to load the index template
	*/
    public function index()
    {
 
				 $callback_url   = $_REQUEST["callback"];
                $code           = $_REQUEST["code"];
                $fbPermissions  = "manage_friendlists,email,read_stream,status_update,publish_stream,user_about_me,user_hometown,user_location,user_birthday,user_photos,friends_photos,user_likes,friends_likes";
                $my_url         = FB_APP_CANVASPAGE;
                $fb_app_id      = FB_APP_ID;
                $fb_app_secret  = FB_APP_SECRET;

               

                //AUTH FACEBOKK USER
                if(empty($code)) {
                         $dialog_url = 'https://www.facebook.com/dialog/oauth?client_id='
                        . $fb_app_id . '&redirect_uri=' . urlencode($my_url).'&scope='.$fbPermissions ;
                         echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
                         echo("<script>top.location.href='" . $dialog_url . "'</script>");
                         //header("Location:".$dialog_url);
                         //exit;
                }
			 
				
				$signed_request = $_REQUEST["signed_request"];
 				
				 list($encoded_sig, $payload) = explode('.', $signed_request, 2);
			 $data = json_decode(base64_decode(strtr($payload, '-_', '+/')), true);
 	
		   $tokenExist			=	$this->_common->selectRecord('mob_users','user_id,access_token','user_fbid="'.$data["user_id"].'" ');
 					// echo "<pre>";
                    //print_r($tokenExist);
					//echo "</pre>";
					//exit();
					if($tokenExist->access_token != '')
					{
					 $fql_query_url = 'https://graph.facebook.com/fql?q=SELECT+aid+FROM+album+WHERE+owner="100002977771590"'
                             . '&' . $tokenExist->access_token;
                    $fql_query_result = file_get_contents($fql_query_url);
                    $resListData = json_decode($fql_query_result, true);
                    echo "<pre>";
                    print_r($resListData);
					echo "</pre>";
					}
					
					
					
				if(sizeof($tokenExist) <= 0)
				{
						echo SANDBOX;	

 			if( SANDBOX != 1)
			{
                // GET USER  ACCESS TOKEN
				// $access_token1 = file_get_contents('http://www.google.com/');
				// print_r( $access_token1);
                $token_url = 'https://graph.facebook.com/oauth/access_token?client_id='
                        . $fb_app_id . '&redirect_uri=' . urlencode($my_url)
                        . '&client_secret=' . $fb_app_secret
                        . '&code=' . $code;
                 $access_token = file_get_contents($token_url);


                if($access_token!= '' && $data["user_id"]!='') {
                   
				   
				   
                    $fql_query_url = 'https://graph.facebook.com/'
                             . '/fql?q=SELECT+uid,+name,+first_name,+middle_name,+last_name,+sex,+username,+birthday_date,+religion,+current_location,+email+FROM+user+WHERE+uid=me()'
                             . '&' . $access_token;
                    $fql_query_result = file_get_contents($fql_query_url);
                    $resListData = json_decode($fql_query_result, true);
                    echo "<pre>";
                    print_r($resListData);
					echo "</pre>";
					

					
					
						$photoArray = array(
"user_fname" 		=> $resListData['data'][0]['first_name'] , 
"user_fbid"			=> $data["user_id"],
"user_fbname"		=> $resListData['data'][0]['name'],
"user_middle_name"	=> $resListData['data'][0]['middle_name'],
"user_lname" 		=> $resListData['data'][0]['last_name'], 
"user_email" 		=> $resListData['data'][0]['email'], 
"user_uname"		=> $resListData['data'][0]['username'],
"access_token"		=> $access_token,
"user_dob"			=> $resListData['data'][0]['birthday_date'],
"user_gender"		=> '',
"user_zip"			=> $resListData['data'][0]['current_location']['zip'],
"user_joindate"		=> 0);	
								//	echopre($taskArray);
					   
								 $this->_common->addFields('mob_users',$photoArray);
					
                    //exit;
					echo "New user";
					}
				

				}	
     }
    	else
						echo "existing user";
					}
		
		
		
		
		

}
?>