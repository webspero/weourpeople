<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
// +----------------------------------------------------------------------+
// | Common database model class. It use for common functions             |
// | File name : common.php                                               |
// | PHP version >= 5.2                                                   |
// | Created On 4 January 2012                                                   |
// +----------------------------------------------------------------------+
// | Author: Jinson Mathew <jinson.m@armiasystems.com>              |
// +----------------------------------------------------------------------+
// | Copyrights Armia Systems ? 2010                                      |
// | All rights reserved                                                  |
// +------------------------------------------------------


class ModelCommon extends BaseModel
{
	public function __construct()
	{
		parent::__construct();
	}
 
 

	/*
	Function to insert the values to table
	*/
	function addFields($table,$postedArray)
	{
		return $this->insert($this->tablePrefix.$table, $postedArray);
	}
	
	/*
	Function to update the table details
	*/
	function updateFields($table,$postedArray,$condition)
	{
		return $this->update($this->tablePrefix.$table, $postedArray,$condition);
	}
	
	/*
	Common function to check the existance of an item
	*/
	function checkExists($table,$field,$where)
    {
		if($where!='')
			$where= ' WHERE '.$where;
    	$query = "SELECT count(".$field.") as cnt
    	          FROM ".$this->tablePrefix .$table.$where ;
				  
				  
        $res = $this->execute($query);
    	return $this->fetchOne($res);
    }
	
	/*
	Common function to return the row fields
	*/
	function selectRow($table,$field,$where)
    {
		if($where!='')
			$where= ' WHERE '.$where;
    	$query = "SELECT ".$field."
    	          FROM ".$this->tablePrefix .$table.$where ;
        $res = $this->execute($query);
    	return $this->fetchOne($res);
    }

	/*
	Common function to return the row fields
	*/
	function selectRecord($table,$field,$where)
    {
		if($where!='')
			$where= ' WHERE '.$where;
    	$query = "SELECT ".$field."
    	          FROM ".$this->tablePrefix .$table.$where ;
	
        $res = $this->execute($query);
    	return $this->fetchRow($res);
    }

	/*
 	Common function to return the resultset details
	*/
	function selectResult($table,$field,$where)
    {
		if($where!='')
			$where= ' WHERE '.$where;
    	$query = "SELECT ".$field."
    	          FROM ".$this->tablePrefix .$table.$where ;
        $res = $this->execute($query);
    	return $this->fetchAll($res);
    }

	/*
	Function to delete the  keywords of a Brands
	*/
	function deleteRecord($table,$where)
	{
		$query = "DELETE FROM ".$this->tablePrefix  .$table.'  WHERE '.$where ;
        $res = $this->execute($query);

	}
	
	/*
	Function to execute a query to delete a records
	*/
	function deleteQuery($query)
	{
         $res = $this->execute($query);

	}
	

	/*
	Function to delete the  keywords of a Brands
	*/
	function selectQuery($query)
	{
		if($query != '')
		{
	        $res = $this->execute($query);
			return $this->fetchAll($res);
			 
		}
	}


 	





	


}
?>