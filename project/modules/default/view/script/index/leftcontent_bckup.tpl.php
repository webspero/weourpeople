<div class="content_left home_page_left_content"> 
{php}PageContext::renderRegisteredPostActions('messagebox');{/php}
<div class="welcome_area">
    <img src="{PageContext::$response->baseUrl}/project/themes/default/images/left_icon.jpg"> 
    <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua eiusmod tempordolor sit amet</h3>
</div>
<div class="clear"></div>
{if PageContext::$response->sess_user_id eq ''}
    <div class="home_loginsec">

    <div class="home_loginsec_left">
        <h3>Already BizB Friend Member</h3>
        <div class="home_log_form_blk" id="jhomelogin">
            <div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
            {PageContext::$response->objForm}

            <div class="clear"></div>
            <div class="home_log_form_forgotblk">
                <!--<input name="" type="checkbox" value="Remember me" class="checkbox"><label class="checkbox_label">Remember me</label>-->
                <span><a href="{PageContext::$response->baseUrl}lost-password">Forgot Password?</a></span>
            </div>
        </div>
    </div>
    <div class="home_loginsec_right">
        <h3>New To BizB Friend</h3>
        <div class="home_log_form_blk" id="jhomeregister">
            <div class="{PageContext::$response->regestrationMessage['msgClass']}">{PageContext::$response->regestrationMessage['msg']}</div>
            {PageContext::$response->objRegistrationForm}
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>   
</div>
{/if}
</div>