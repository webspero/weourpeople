<div class="container">
    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-4 "></div>
        <div class="col-sm-4 col-md-4 col-lg-4 ">
            <section class="whitebox marg40col">
            <div class="signinform">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3><i class="fa fa-envelope-o"></i> Sign in</h3>
                    </div>
                </div>
                <div class="form-bottom signup_form">
                    <form name="formLogin" id="formLogin" action="" method="post" role="form">
                        <!-- alert failed -->
                        {if PageContext::$response->message['msg']!=''}
                        <div class="alert alert-danger {PageContext::$response->message['msgClass']}">
                            <i class="fa fa-frown-o"></i> 
                            {PageContext::$response->message['msg']}
                        </div>
                        {/if}
                        <div class="form-group relative">
                            <i class="fa fa-envelope"></i>
                            <input type="text" value="{PageContext::$response->mail}" name="user_email" class="form-email form-control" placeholder="Your Email" >
                            <label class="error" style="display:none;" generated="true" for="user_email"></label>
                        </div>
                        <div class="form-group relative">
                            <i class="fa fa-lock"></i>
                            <input type="password" value="{PageContext::$response->password}" name="user_password" class="form-control" placeholder="Password" >
                            <label class="error" style="display:none;" generated="true" for="user_password"></label>
                        </div>
                        <div class="temrs_blk_signup">
                            <span class="remember-box checkbox">
                                <label for="rememberme">
                                    <input type="checkbox" value="1" name="remember_me" id="rememberme" checked="{PageContext::$response->rememberCheck}">Remember Me
                                </label>
                            </span>
                        </div>
                        <input type="submit" name="btnSubmit" data-loading-text="Loading..." class="btn btn-primary btn-block yellow_btn2" value="Sign in">
                        <div class="clearfix"></div>
                        {if PageContext::$response->isFBEnabled}
                        <!--start facebook div-->
                            <input id="fbBtn" type="button" class="loginfb_btn" value="Login with facebook">
                       {/if}
                       <div class="clearfix"></div>
                        <div class="wid100per">
                            <span class="forg_pwrd"><a href="{PageContext::$response->baseUrl}lost-password">Forgot Password?</a></span>
                        </div>
                    </form>
                </div>
            </div>
            </section>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 "></div>
    </div>
</div>


