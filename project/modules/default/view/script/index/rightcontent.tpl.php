   <div class="content_right">
    {if PageContext::$response->flagDisplay!= 1}
    <div class="right_nav">
        {if PageContext::$response->sess_user_id >0}
        <ul>
            <li class="title jMenuTitle" sid="jMenuProfile">Profile</li>
            {if PageContext::$response->activeSubMenu eq 'profile'}
            {assign var="profile_display" value="block"}
            {else}
            {assign var="profile_display" value="none"}
            {/if}
            <div id="jMenuProfile" class="jMenu" style="display:{$profile_display}">
                <li><a href="{PageContext::$response->baseUrl}profile/{PageContext::$response->sess_user_alias}#Edit">Edit Profile</a></li>
                <!--<li><a href="{PageContext::$response->baseUrl}champions-portfolio">Edit my business affiliations</a></li>-->
            </div>
            <div class="clear"></div>
            
            <li class="title jMenuTitle" sid="jMenuBizBfriends">BizBfriends</li>
            {if PageContext::$response->activeSubMenu eq 'friends'}
            {assign var="friends_display" value="block"}
            {else}
            {assign var="friends_display" value="none"}
            {/if}
            <span id="jMenuBizBfriends" class="jMenu" style="display:{$friends_display}">
                <li><a href="{PageContext::$response->baseUrl}my-friends">My Friends</a></li>
                <li><a href="{php} echo PageContext::$response->baseUrl; {/php}pending-request">Pending Friend Requests</a></li>
            </span>
            <div class="clear"></div>

<!--            <li class="title jMenuTitle" sid="jMenuRecommendFriend">Recommend a friend</li>
            {if PageContext::$response->activeSubMenu eq 'referral_offer1'}
            {assign var="referral_offer_display1" value="block"}
            {else}
            {assign var="referral_offer_display1" value="none"}
            {/if}
            <span id="jMenuRecommendFriend" class="jMenu" style="display:{$referral_offer_display1}">
                <li><a href="{PageContext::$response->baseUrl}referral-offer">Make a friend recommendation</a></li>
            </span>
            <div class="clear"></div>-->

            <li class="title jMenuTitle" sid="jMenuMessage">Message</li>
            {if PageContext::$response->activeSubMenu eq 'messages'}
            {assign var="messages_display" value="block"}
            {else}
            {assign var="messages_display" value="none"}
            {/if}
            <span id="jMenuMessage" class="jMenu" style="display:{$messages_display}">
                <li><a href="{PageContext::$response->baseUrl}inbox">Inbox</a></li>
                <li class=""><a href="{PageContext::$response->baseUrl}invitation-sent">Invitations sent</a></li>
            </span>
            <div class="clear"></div>

            <li class="title jMenuTitle" sid="jMenuBusinessCommunity">Business Communities</li>
            {if PageContext::$response->activeSubMenu eq 'community'}
            {assign var="community_display" value="block"}
            {else}
            {assign var="community_display" value="none"}
            {/if}
            <span id="jMenuBusinessCommunity" class="jMenu" style="display:{$community_display}">
                <li><a href="{php} echo PageContext::$response->baseUrl; {/php}add-community">Create a new managed BizCom</a></li>
                <li><a href="{PageContext::$response->baseUrl}my-community">My Managed BizComs</a></li>
                <li><a href="{PageContext::$response->baseUrl}my-subscribed-group">BizComs to which I subscribe</a></li>
                <li><a href="{PageContext::$response->baseUrl}my-community">Create an announcement to a BizCom</a></li>
                <li><a href="{PageContext::$response->baseUrl}pending-bizcom-request">Outstanding invitations received</a></li>
                <li><a href="{PageContext::$response->baseUrl}pending-bizcom-members">Pending Bizcom Members</a></li>
            </span>
            <div class="clear"></div>

            <li class="title jMenuTitle" sid="jMenuBusinessEndoresement">Business Endorsements</li>
            {if PageContext::$response->activeSubMenu eq 'endorsement'}
            {assign var="endorsement_display" value="block"}
            {else}
            {assign var="endorsement_display" value="none"}
            {/if}
            <span id="jMenuBusinessEndoresement" class="jMenu" style="display:{$endorsement_display}">
                <li><a href="{PageContext::$response->baseUrl}directory">Search and rate a business</a></li>
                <li><a href="{PageContext::$response->baseUrl}directory">Search and add testimonials</a></li>
            </span>
            <div class="clear"></div>
            
<!--            <li class="title jMenuTitle" sid="jMenuCharity">Charity Organisation</li>
            {if PageContext::$response->activeSubMenu eq 'charity'}
            {assign var="charity_display" value="block"}
            {else}
            {assign var="charity_display" value="none"}
            {/if}
            <span id="jMenuCharity" class="jMenu" style="display:{$charity_display}">
                <li><a href="{PageContext::$response->baseUrl}add-charity">Create new charity organisation</a></li>
                <li><a href="{PageContext::$response->baseUrl}my-charity">My charity organisation</a></li>
            </span>
            <div class="clear"></div>-->

            <li class="title jMenuTitle" sid="jMenuBusinessRewards">Rewards and Incentives Account</li>
            {if PageContext::$response->activeSubMenu eq 'account'}
            {assign var="account_display" value="block"}
            {else}
            {assign var="account_display" value="none"}
            {/if}
            <span id="jMenuBusinessRewards" class="jMenu" style="display:{$account_display}">
                <li><a href="#">My Personal Account</a></li>
                <li><a href="#">My Business Account</a></li>
                <li><a href="{PageContext::$response->baseUrl}champions-portfolio">Business Champions portfolio</a></li>
            </span>
            <div class="clear"></div>

            <li class="title jMenuTitle" sid="jMenuBusinessRefferals">Business Referrals Campaigns</li>
            {if PageContext::$response->activeSubMenu eq 'campaign'}
            {assign var="campaign_display" value="block"}
            {else}
            {assign var="campaign_display" value="none"}
            {/if}
            <span id="jMenuBusinessRefferals" class="jMenu" style="display:{$campaign_display}">
                <li><a href="{php} echo PageContext::$response->baseUrl; {/php}refferal-request">Create new Referral Campaign
                        Goodwill (FOC)
                        Financial incentive
                        Product, gift or voucher
                        Charitable Donation</a></li>
                <li><a href="{PageContext::$response->baseUrl}my-active-campaigns">My active campaigns</a></li>
                <li><a href="{PageContext::$response->baseUrl}my-saved-campaigns">My Saved Campaigns</a></li>
                <li><a href="{PageContext::$response->baseUrl}my-closed-campaigns">My closed campaigns</a></li>
                <li><a href="{PageContext::$response->baseUrl}my-closed-campaigns">Resurrect a closed campaign</a></li>
                <!--                <li><a href="{PageContext::$response->baseUrl}my-campaigns">My Existing Campaigns</a></li>-->
                <li><a href="{PageContext::$response->baseUrl}my-pending-introductions">Refferal Request Received</a></li>
                <li><a href="{PageContext::$response->baseUrl}my-accepted-introductions">Accepted Refferal Request</a></li>
            </span>
            <div class="clear"></div>
            
            
            <li class="title jMenuTitle" sid="jMenuBusinessRefferalOffer">Business Referral Offers</li>
            {if PageContext::$response->activeSubMenu eq 'referral_offer'}
            {assign var="referral_offer_display" value="block"}
            {else}
            {assign var="referral_offer_display" value="none"}
            {/if}
            <span id="jMenuBusinessRefferalOffer" class="jMenu" style="display:{$referral_offer_display}">
                <li><a href="{PageContext::$response->baseUrl}pending-referral-offer">Pending Referral Offers</a></li>
                <li><a href="{PageContext::$response->baseUrl}accepted-referral-offer">Accepted Referral Offer</a></li>
             </span>
            <div class="clear"></div>

            <li class="title jMenuTitle" sid="jMenuPayment">Payments</li>
            {if PageContext::$response->activeSubMenu eq 'payments'}
            {assign var="payment_display" value="block"}
            {else}
            {assign var="payment_display" value="none"}
            {/if}
            <span id="jMenuPayment" class="jMenu" style="display:{$payment_display}">
                <li><a href="#">Referral acceptance approvals outstanding</a></li>
                <li><a href="#">Referral acceptance approvals pending</a></li>
                <li><a href="{PageContext::$response->baseUrl}my-payments">Referral acceptance payments closed</a></li>
            </span>
            <div class="clear"></div>
            
            <li class="title jMenuTitle" sid="jMenuVoucher">Vouchers</li>
            {if PageContext::$response->activeSubMenu eq 'vouchers'}
            {assign var="voucher_display" value="block"}
            {else}
            {assign var="voucher_display" value="none"}
            {/if}
            <span id="jMenuVoucher" class="jMenu" style="display:{$voucher_display}">
                <li><a href="{PageContext::$response->baseUrl}vouchers">Vouchers Sent</a></li>
                
            </span>
            <div class="clear"></div>

            <li class="title jMenuTitle" sid="jMenuDirectory">Business Directories</li>
            {if PageContext::$response->activeSubMenu eq 'business'}
            {assign var="business_display" value="block"}
            {else}
            {assign var="business_display" value="none"}
            {/if}
            <span id="jMenuDirectory" class="jMenu" style="display:{$business_display}">
<!--                <li><a href="{PageContext::$response->baseUrl}my-directory">My BizDirectory</a></li>-->
       <li><a href="{PageContext::$response->baseUrl}businesses">My BizDirectory</a></li>
                <li><a href="{php} echo PageContext::$response->baseUrl; {/php}add-directory">Create a new BizDirectory</a></li>
            </span>
            <div class="clear"></div>
            
            <li class="title jMenuTitle" sid="jMenuClassifieds">Classifieds</li>
            {if PageContext::$response->activeSubMenu eq 'classifieds'}
            {assign var="classifieds_display" value="block"}
            {else}
            {assign var="classifieds_display" value="none"}
            {/if}
            <span id="jMenuClassifieds" class="jMenu" style="display:{$classifieds_display}">
                <li><a href="{php} echo PageContext::$response->baseUrl; {/php}add-classifieds">Create a new Advertisement</a></li>
                <li><a href="{PageContext::$response->baseUrl}my-classifieds">My existing advertisements</a></li>
            </span>
            <div class="clear"></div>
        </ul>
        {else}
        <ul>
            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}businesses">BizDirectory</a></li>
            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}community">BizCom</a></li>
            <li><a href="{PageContext::$response->baseUrl}aboutus">About Us</a></li>
            <li class="no_btmborder"><a href="{PageContext::$response->baseUrl}contact">Contact Us</a></li>
        </ul>
        {/if}
    </div>
    {/if}
    <div class="clear"></div>
    <div class="right_search">
        <h3>Search</h3>
        <form name="serachprofile" id="search" action="{PageContext::$response->baseUrl}index/searchprofile" method="GET">
            <input name="searchtext" id="jSearchProfile" type="text" placeholder="Search Member,Bizcom,Bizdirectory">
            <input type="hidden" name="search_selected_activity" id="search_selected_activity" >
            <input name="" type="submit" value="" class="search_btn">
        </form>
    </div>
</div>