<script type="text/javascript">
 $(document).ready(function(){
    Captcha(); 
 });   
                 function Captcha(){
                     var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
                     var i;
                     for (i=0;i<6;i++){
                       var a = alpha[Math.floor(Math.random() * alpha.length)];
                       var b = alpha[Math.floor(Math.random() * alpha.length)];
                       var c = alpha[Math.floor(Math.random() * alpha.length)];
                       var d = alpha[Math.floor(Math.random() * alpha.length)];
                       var e = alpha[Math.floor(Math.random() * alpha.length)];
                       var f = alpha[Math.floor(Math.random() * alpha.length)];
                       var g = alpha[Math.floor(Math.random() * alpha.length)];
                      }
                    var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' '+ f + ' ' + g;
                    document.getElementById("mainCaptcha").value = code
                  }
                  function ValidCaptcha(){
                      var string1 = removeSpaces(document.getElementById('mainCaptcha').value);
                      var string2 = removeSpaces(document.getElementById('txtInput').value);
                      if (string2 == 'undefined' || string2 == ''){
                         $(".err_div").html('Please enter capcha code');
                          return false;
                      }
                      if (string1 == string2){
                        return true;
                      }
                      else{        
                        $(".err_div").html('Please enter valid capcha code');
                         return false;
                      }
                  }
                  function removeSpaces(string){
                    return string.split(' ').join('');
                  }
             </script>    
<div class="container">
<div class="row">
	<div class="col-sm-12 col-md-3 col-lg-3 "></div>
	<div class="col-sm-12 col-md-6 col-lg-6 ">
		<div class="whitebox marg40col pad25px_left pad25px_right forgotform">
				<div class="form-top">
	        		<div class="form-top-left">
	        			<h3>Contact Us</h3>
	        		</div>
	        		<div class="form-top-right">
	        			<!--<i class="fa fa-envelope-o"></i>-->
	        		</div>
	            </div>
	            <div class="form-bottom signup_form">
	            	<!--<p>To reset your password, enter the email address you use to login in to {PageContext::$response->siteName} </p>-->
	            	<div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
	                <form name="frmContact" id="frmContact" action="" method="post" role="form">
	                	
	                    <div class="form-group relative">
	                    	<!--<i class="fa fa-envelope"></i>-->
                            {literal}
                            <input type="email"  name="sender_email" class="form-email form-control" placeholder="Your Email" required="true" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" >
                            {/literal}
	                                <label class="error" style="display:none;" generated="true" for="user_email"></label>
                                        <input type="text"  name="email_subject" class="margtop10 form-control" placeholder="Subject" required="true" >
                                <textarea name="email_description" class="margtop10 form-control" placeholder="Content" required="true"></textarea>
	                    </div>
                                                        <div class='err_div'></div>
                              
          <div class="form-group relative">
	     <input type="text" id="mainCaptcha"/>
             <img style='width: 4%;' src='{PageContext::$response->ImagePath}refresh.png' id="refresh" value="Refresh" onclick="Captcha();">
	  </div>
                                   <div class="form-group relative">
                                        <input type="text" id="txtInput"/>   
                                   </div>
	                    <input type="submit" name="btnSubmit" data-loading-text="Loading..." class="btn btn-primary yellow_btn2" value="Send" onclick="return(ValidCaptcha());">
	                </form>
	            </div>
	    </div>
	</div>
	<div class="col-sm-12 col-md-3 col-lg-3 "></div>
	</div>
</div>