{if $smarty.session.default_user_id}
{if PageContext::$response->chat_display eq 1}
{if PageContext::$response->sess_user_status eq 'A'}
<a class="chatbutton" href="javascript:void(0)" id="chatIdentifier"><i class="fa fa-comments-o" aria-hidden="true"></i> Chat Now</a>
<div class="chatlist" id="onlineUsersList" style="display:none">
    <div class="chatlisthead">
        <h3>Chat List <a href="javascript:void(0)" class="right" id="chatMinimize"><i class="fa fa-minus" aria-hidden="true"></i></a></h3>
    </div>
    <ul id="chatListContainer">        
        {if empty(PageContext::$response->leftMenuUsersList->records) }            
            <li>
                <div class="display_table">
                    No Friends Added Yet!
                </div>
            </li>
        {else}
            {foreach from = PageContext::$response->leftMenuUsersList->records item = friendEntity }
            {if $friendEntity->file_path == ''}
                {assign var="user_image" value="member_noimg.jpg"}
            {else}
                {assign var="user_image" value=$friendEntity->file_path}
            {/if}
            
            {if $friendEntity->user_online_status == 'Idle'}
                {assign var="chat_status" value="la"}
            {else if $friendEntity->user_online_status == 'Offline'}
                {assign var="chat_status" value="ma"}
            {else}
                {assign var="chat_status" value=""}
            {/if}
                <li>
                    <div class="display_table">
                    <div id="profilepic" class="profilepicsec">
                        <a href="{PageContext::$response->baseUrl}timeline/{$friendEntity->user_alias}">
                            <span class="mediapost_pic" style="background-image: url('{PageContext::$response->userImagePath}{$user_image}');">
                            </span>
                        </a>
                    </div>
                    <div class="profilenamesec">
                        <h3 class="profile_name">
                            <a href="javascript:void(0)" onclick="chatWith('{$friendEntity->user_alias}','{$friendEntity->user_firstname}');" id="jProfile1" class="jtab " alias="{$friendEntity->user_firstname}">
                                {$friendEntity->user_firstname}
                            </a>
                        </h3>
                    </div>
                    <div class="picstatus">
                           <i class="fa fa-circle {$chat_status}" aria-hidden="true"></i>
                    </div>
                    </div>
                </li>
               {/foreach}
               {/if}
    </ul>
</div>
{/if}
{/if}
{/if}
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-8">
                <ul class="ftr_links">
                    <li><a href="{PageContext::$response->baseUrl}">Home</a></li>
                    <li><a href="{PageContext::$response->baseUrl}aboutus">About Us</a></li>
                    <li><a href="{PageContext::$response->baseUrl}contact">Contact Us</a></li>
                    <li><a href="{PageContext::$response->baseUrl}faq">FAQ</a></li>
                    <li><a href="{PageContext::$response->baseUrl}feedback">Feedback</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="copyright">
                    © Copyright {PageContext::$response->curYear}. Socialware. All rights reserved
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="clear"></div>
