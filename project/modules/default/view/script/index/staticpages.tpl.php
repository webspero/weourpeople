<div class="container">
<div class="row"> 
    <div class="col-sm-12 col-md-12 col-lg-12">
        <h3>{PageContext::$response->pageDetails->content_title}</h3>
        <section class="description">
            <p>
                {PageContext::$response->pageDetails->content_description|stripslashes}
            </p>
            <div class="clear"></div>
        </section>
    </div>
</div>
</div>


