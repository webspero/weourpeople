
<header>
    <div class="container">
        <div class="row"> 
            <div class="col-sm-4 col-md-6 col-lg-6 ">
                <div class="logo">
                    <a href="{if PageContext::$response->sess_user_id>0}
                   {PageContext::$response->baseUrl}newsfeed/{PageContext::$response->sess_user_alias}
                    {else}
                    {PageContext::$response->baseUrl}
                    {/if}">
                       <img src="{PageContext::$response->sitelogo}"/>
                    </a>
                </div>
            </div>
            <div class="col-sm-8 col-md-6 col-lg-6 ">
                <div class="">
                    {if PageContext::$response->sess_user_id>0}
                    {else}
                    <div class="login_main_sec">
                    <form name='frmLogin' id='frmLogin' method='post' action= '{PageContext::$response->baseUrl}index#jhomelogin' class=''>
                    <div class="display_table">
                        <div class="display_table_cell wid40per pad_right1px vlaign_top">
                            <input id='user_email' name = 'user_email' value ='{PageContext::$response->user_email}' type="text" placeholder="Email" tabindex="1" class="login_namebox">
                            <!--
                            <div class="clserror"><label generated="true" for="user_email" class="error">Please enter a valid email</label></div>-->
                            <div class="clearfix"></div>
                            <div class="wid100per remember_blk pull-left">
                                <input type="checkbox" class="pull-left" title="Remember Me" placeholder="" {PageContext::$response->remember_me} value="1" name="remember_me"> <span class="pull-left rememberstext">Remember Me</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="display_table_cell wid40per pad_right1px vlaign_top">
                            <input type="password" tabindex="2" placeholder="Password" class="login_namebox" name = 'user_password' value = '{PageContext::$response->user_password}' size = 10>
                            <!--
                            <div class="clserror"><label generated="true" for="user_password" class="error">Please enter password</label></div> -->
                            <div class="clearfix"></div>
                            <div class="wid100per">
                               <span class="forg_pwrd">
                                   <a href="{PageContext::$response->baseUrl}lost-password">Forgot Password?</a>
                               </span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="display_table_cell wid20per vlaign_top">
                            <input type="submit" class="dark_blue_btn" value="Log in" name="btnSubmit">
                        </div>
                        {if PageContext::$response->isFBEnabled}
                        <!--start facebook div-->
                        <div class="display_table_cell wid20per vlaign_top">
                            <input id="fbBtn" type="button" class="dark_blue_btn" value="Login with facebook">
                        </div><!--end div facebook-->
                        {/if}
                    </div>
                    </form>
                    </div>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</header>