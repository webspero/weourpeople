


<div class="banner_outer">
	<div class="bnr_content_sec">
	  <div class="container">
	  <div class="row"> 
			<div class="col-sm-12 col-md-8 col-lg-8 ">
				<div class="bnrright_img">
					<img src="{PageContext::$response->themeImage}bnr_left.png">
				</div>
				<div class="bnrright_hds">
					<!--<h3>Your <span>Personalised</span> Social Network Is Here</h3>-->
                                        <h3>{PageContext::$response->site_description}</h3>
				</div>
	        </div>
               {if PageContext::$response->flagDisplay neq 1}
	        <div class="col-sm-12 col-md-4 col-lg-4 ">
	        	<section class="signuphome">
					<div class="form-top">
                		<div class="form-top-left">
                			<h3>New To {PageContext::$response->site_label} <span>? Join Today!</span></h3>
                		</div>
                    </div>
                    <div class="form-bottom signup_form">
	                    <form name="frmUserRegister" id="frmUserRegister" method="post" action= '{PageContext::$response->baseUrl}index' class='' role="form">
	                        <div class="form-group relative">
	                        	<i class="fa fa-envelope"></i>
	                        	<input type="text" id="user_emails" name = "user_emails" value ="{PageContext::$response->user_emails}" class="form-email form-control" placeholder="Your Email" >
                                        <label {if !PageContext::$response->message0} style="display:none;" {/if} generated="true" class="error">{PageContext::$response->message0}</label>
	                        </div>
	                        <div class="form-group relative">
	                        	<i class="fa fa-lock"></i>
	                        	<input type="password" id="user_passwords" name = "user_passwords" value ="" class="form-control" placeholder="Password" >
                                        <label {if !PageContext::$response->message1} style="display:none;" {/if} generated="true" class="error">{PageContext::$response->message1}</label>
	                        </div>
	                        <div class="form-group relative">
	                        	<i class="fa fa-unlock-alt"></i>
	                        	<input type="password" id="user_confirm_password" name = "user_confirm_password" value ="" class="form-control" placeholder="Re - Enter Password">
                                        <label {if !PageContext::$response->message2} style="display:none;" {/if} generated="true" class="error">{PageContext::$response->message2}</label>
	                        </div>
	                        <div class="temrs_blk_signup">
	                        	<input id = "terms_conditions" name = "terms_conditions" value = "1" size = "10" type="checkbox" class="" title="I agree to the &lt;a href='{PageContext::$response->baseUrl}terms' target='_blank'&gt;Terms of Service&lt;/a&gt; and &lt;a href='{PageContext::$response->baseUrl}privacy-policy' target='_blank'&gt;Privacy Policy&lt;/a&gt;." placeholder=""> I agree to the <a target="_blank" href="{PageContext::$response->baseUrl}terms">Terms of Service</a> and <a target="_blank" href="{PageContext::$response->baseUrl}privacy-policy">Privacy Policy</a>.<div class="clserror"><label generated="true" for="terms_conditions" class="error"></label></div>
                                        <label {if !PageContext::$response->message3} style="display:none;" {/if} generated="true" class="error">{PageContext::$response->message3}</label>
	                        </div>
	                        <input type="submit" class="btn btn-primary btn-block blue_btn2" value="Sign Up" name="btnSubmit">
	                    </form>
                    </div>
                </section>
                   
	        </div>
               {/if}
               <div class="clearfix"></div>
	  </div>
	  </div>
	</div>


 
 <!-- 
	<div class="container">
		<div id="da-slider" class="da-slider">
			{foreach from=PageContext::$response->banner item=banner}
				<div class="da-slide">
						<h2>{$banner->banner_title}</h2>
						<p>{$banner->banner_description}</p>
						{if $banner->banner_link_text neq ''}
						<a href="{PageContext::$response->baseUrl}{$banner->banner_link}" class="da-link">{$banner->banner_link_text}</a>
						{/if}
						<div class="da-img">
                                                    {if $banner->banner_image_name !=''}
                                                    <img src="{PageContext::$response->userImagePath}slider/{$banner->banner_image_name}" alt="{$banner->banner_title}" />
				             {/if}
                                             </div> 
                                </div> 
				{/foreach}		
				<nav class="da-arrows">
					<span class="da-arrows-prev"></span>
					<span class="da-arrows-next"></span>
				</nav>
			</div>
	</div>
	
   -->
    <div class="clearfix"></div>
</div>


<script>
$(function () {

      // Slideshow 1
      $("#slider1").responsiveSlides({
        
        speed: 800
      });
    });
</script>