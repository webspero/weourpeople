<link type="text/css" rel="stylesheet" href="<?php echo BASE_URL?>project/styles/popup.css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery-1.8.0.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>bbf/public/js/jquery-ui-1.8.23.custom.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery.blockUI.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/fw.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.metadata.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.validate.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/validations_form.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/app.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.colorbox.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/comment_popup.js"></script>
<script>
    var mainUrl = '<?php echo BASE_URL?>';
</script>
<script type="text/javascript"> 
    jQuery(document).ready(function($) {
        $("#loader").hide();
    $('#frmLogin').submit(function(e){
        e.preventDefault();
        var user_email= $("#user_email").val();
        var user_password= $("#user_password").val();
        var flag = '<?php echo PageContext::$response->flag;?>';
        var flag1 = '<?php echo PageContext::$response->type;?>';
        var dataString          = 'user_email='+user_email+'&user_password='+user_password;
        var bus_id = $("#business_id").val();
        //alert(flag1);
        if(bus_id)
        var business_id = bus_id.toString().split('*');
        $("#loader").show();
        $.ajax({
            type    : "POST",
            dataType: 'json',
           
            url     : "<?php echo BASE_URL?>index/ajaxLogin/"+business_id[0]+"/"+business_id[1]+"/"+business_id[2],
            async : false,
            data    : dataString,
            cache   : false,
            success : function(data){
               if(data.status=='SUCCESS')
                   {
                      // console.log(data);
                       $("#loader").hide();
                       window.parent.$("#login_status").val(data.data.user_id);
                      
                      // alert(data.data.user_id);
                       if(data.data.user_status == 'A'){  
                    	   if(flag == 'rate'){      
                    		   // alert(parent.$(this).attr("data-rateit-id"));       
                               parent.$(".rateit").attr("data-rateit-id",data.data.user_id);                              
                               parent.checkRated(); 
                                 window.parent.location.reload();
                           }
                           else if(data.data.user_uid == 'Owner' && data.data.tflag == 'Tstimonial'){
                                $("#msg").html("You are the owner of the business,You cant post Testimonial");
                                $("#msg").addClass('error_div');
                                setTimeout(function(){ window.parent.location.reload(); }, 2000);
                                
                           }
                           else if(data.data.user_uid == 'Owner' && data.data.tflag == 'Inquire'){
                               $("#msg").html("You are the owner of the business,You cant inquire");
                               $("#msg").addClass('error_div');
                               setTimeout(function(){ window.parent.location.reload(); }, 2000);
                                
                           }
                           else if(data.data.tflag == 'Tstimonial'){ 
                      			 window.parent.showTestimonialsBox('<?php echo PageContext::$response->flag?>');
                      			 parent.$(".rateit").attr("data-rateit-id",data.data.user_id);
                           }
                            else if(flag1 == 'refer'){
                            
                               window.location = mainUrl+"index/joingroup?community_id="+business_id[0]+"&type="+business_id[2];
                           }
                            else if(flag1 == 'referprivate'){
                               window.location = mainUrl+"index/joinmail?community_id="+business_id[0]+"&type="+business_id[2];
                           }
                           else{
                               window.parent.showInquireBox(business_id[0],business_id[2]);
                               parent.$(".rateit").attr("data-rateit-id",data.data.user_id);
                           }
                       }else{
                           alert("User is Inactive .Since your account is not activated you cannot comment/rate.");
                           parent.$(".rateit").attr("data-rateit-id",data.data.user_id);
                       }                      
                       $.colorbox.close();
                       window.parent.location.reload();
                   }else{
                       
                       $("#msg").html("Invalid Credentials");
                       $("#msg").addClass('error_div');
                      setTimeout(function(){ window.location.reload(); }, 1000);
                   }
              
            }
        });
        
     });
       $('#frmUserRegister_sign').submit(function(e){
  
      e.preventDefault();
        var user_email      = $("#user_email_sign").val();
        var user_password       = $("#user_password_sign").val();
        var user_confirm_password = $("#user_confirm_password_sign").val();
        var flag = '<?php echo PageContext::$response->type;?>';
        //alert("flag"+flag);
        var bus_id = $("#business_id").val();
        if(bus_id)
           var business_id = bus_id.toString().split('*');
       //alert(business_id[1]);
        var dataString          = 'user_email='+user_email+'&user_password='+user_password+'&user_confirm_password='+user_confirm_password;
        $("#loader").show();
        $.ajax({
            type    : "POST",
            dataType: 'json',
            url     : "<?php echo BASE_URL?>index/ajaxRegister",
            data    : dataString,
            cache   : false,
            success : function(data){
               //console.log(data);exit;
               if(data.status=='SUCCESS')
                   {
                        // console.log(data);exit;
                        // alert(flag);exit;
                       $("#loader").hide();
                       window.parent.$("#login_status").val(data.data.user_id);
                        window.parent.$(".inrheader").load(window.parent.location.href + " .inrheader>*", "");
                      // alert(data.data.user_status);
                      if(data.data.user_status == 'A'){
                       if(flag == 'rate'){      
                    		   // alert(parent.$(this).attr("data-rateit-id"));       
                               parent.$(".rateit").attr("data-rateit-id",data.data.user_id);                              
                               parent.checkRated();  
                              //  window.parent.location.reload(); 
                           }
                           else if(flag == 'refer'){
                                 //  alert("here");exit;
                                 
                               window.location = mainUrl+"index/joingroup?community_id="+business_id[0]+"&type="+business_id[2];
                                       // $.colorbox.close(); 

                              // exit;
                           }
                            else if(flag == 'referprivate'){
                              window.location = mainUrl+"index/joinmail?community_id="+business_id[0]+"&type="+business_id[2];
                           }
                           else if(business_id[1] == 'Inquire'){
                                window.parent.showInquireBox(business_id[0],business_id[2]);
                               parent.$(".rateit").attr("data-rateit-id",data.data.user_id);
                           }
                           else{ 
                      			 window.parent.showTestimonialsBox('<?php echo PageContext::$response->type?>','<?php echo PageContext::$response->flag?>');
                      			 parent.$(".rateit").attr("data-rateit-id",data.data.user_id);
                           }
                          // window.parent.location.reload(); 
                       }else{
                           alert(data.data.msg+' Since your account is not activated you cannot comment/rate.');
                           parent.$(".rateit").attr("data-rateit-id",data.data.user_id);
                       }   
                      
                       $.colorbox.close();
                    //  window.parent.location.reload(); 
                   }else{
                        $("#loader").hide();
                       $("#msg_sign").html(data.message);
                       $("#msg_sign").addClass('error_div');
                     // setTimeout(function(){ window.location.reload(); }, 1000);
                   }
                     // $.colorbox.close();
               // window.parent.location.reload(); 
            }
         
        });
     });
    //  window.parent.location.reload(); 
      });
</script>
<!--<div id="modal">-->
     <span class="loader" id="loader"> <img src="<?php echo PageContext::$response->userImagePath;?>default/loader.gif" /> Loading.... </span>
<div class="content_login">
        <div class="" id="msg"></div>
        <h3>Login</h3>
        <div class="register_form">
            <?php echo PageContext::$response->objFormLogin; ?>
			<a href="#" class="signuplink" id="signup" onclick="showDiv('content_register','content_login')">Sign Up</a>
        </div>
<input type="hidden" id="business_id" value="<?php echo PageContext::$response->tflag."*".PageContext::$response->flag."*".PageContext::$response->type ?>">        
  <input type="hidden" id="community_id" name="community_id">     
  <input type="hidden" id="type"  name ="type">     
      
    </div>
    <div class="clear"></div>
    
  <div class="content_register">
         <div class="" id="msg_sign"></div>
        <h3>Sign Up</h3>
        <div class="register_form">
            <?php echo PageContext::$response->objFormRegister; ?>
        </div>
        
        <a href="#" class="signuplink" id="login" onclick="showDiv('content_login','content_register')">Login</a>
    </div>
    <!--</div>-->