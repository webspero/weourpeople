<link type="text/css" rel="stylesheet" href="<?php echo BASE_URL?>project/styles/popup.css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery-1.8.0.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>bbf/public/js/jquery-ui-1.8.23.custom.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery.blockUI.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/fw.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.metadata.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.validate.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/validations_form.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/app.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.colorbox.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/comment_popup.js"></script>

<script type="text/javascript"> 
    $(document).ready(function() {
    $('#frmLogin_rate').submit(function(e){
//    $(this).validate({
//        rules: {
//                
//            user_password: {
//                required: true,
//                minlength: 5
//            },
//               
//            user_email: {
//                required: true,
//                email: true
//            }
//        },
//        messages: {
//                
//            user_password: {
//                required: "Please provide a password",
//                minlength: "Your password must be at least 5 characters long"
//            },
//            user_email: {
//                required:"Please provide an email address",
//                email : "Please enter a valid email"
//            }
//        }
//    });
    
        e.preventDefault();
        var user_email= $("#user_email").val();
        var user_password= $("#user_password").val();
        var flag = '<?php echo PageContext::$response->flag;?>';
        var dataString          = 'user_email='+user_email+'&user_password='+user_password;
     //   if(flag = 'rate'){  alert($(this).attr("data-rateit-value"));}
        $.ajax({
            type    : "POST",
            dataType: 'json',
           
            url     : "<?php echo BASE_URL?>index/ajaxLogin",
            async : false,
            data    : dataString,
            cache   : false,
            success : function(data){
               if(data.status=='SUCCESS')
                   {
                       window.parent.$("#login_status").val(data.data.user_id);
                      // console.log()
                       
                       if(data.data.user_status == 'A'){  
                           //alert(flag);exit;
                    	   if(flag == 'rate'){      
                    		   // alert(parent.$(this).attr("data-rateit-id"));       
                               parent.$(".rateit").attr("data-rateit-id",data.data.user_id);                              
                               parent.checkRated1('<?php echo PageContext::$response->rateField;?>');                              
                           }
                           else{ 
                      			 window.parent.showCommentBox('<?php echo PageContext::$response->type?>','<?php echo PageContext::$response->flag?>');
                      			 parent.$(".rateit").attr("data-rateit-id",data.data.user_id);
                           }
                       }else{
                           alert("User is In active.Since your account is not activated you cannot rate");
                           parent.$(".rateit").attr("data-rateit-id",data.data.user_id);
                       }   
                     //window.parent.location.reload();
                       $.colorbox.close();
                      
                   }else{
                       setTimeout(function(){ $("#msg").html("Invalid Credentials");
                       $("#msg").addClass('error_div');},4000);
                      
                   }
             window.parent.location.reload();
            }
        });
        
     });
       $('#frmUserRegister_rate').submit(function(e){
      
       e.preventDefault();
        var user_email      = $("#user_email_rate").val();
        var user_password       = $("#user_password_rate").val();
        var user_confirm_password = $("#user_confirm_password_rate").val();
        var flag = '<?php echo PageContext::$response->flag;?>';
       //alert(user_email);
        var dataString          = 'user_email='+user_email+'&user_password='+user_password+'&user_confirm_password='+user_confirm_password;
        $.ajax({
            type    : "POST",
            dataType: 'json',
            url     : "<?php echo BASE_URL?>index/ajaxRegister",
            data    : dataString,
            cache   : false,
            success : function(data){
               if(data.status=='SUCCESS')
                   {
                         
                       //window.parent.location.reload();
                        window.parent.$("#login_status").val(data.data.user_id);
                        window.parent.$(".inrheader").load(window.parent.location.href + " .inrheader>*", "");
                       parent.$.colorbox.close();
                          //alert("hi");
//                          $("#cboxOverlay").hide();
//                          $("#colorbox").css('display','none');
                      // window.parent.$("header").load('header_inner.tpl.php');
                      if(data.data.user_status == 'A'){
                       if(flag == 'rate'){
                         
                    	   parent.$(".rateit").attr("data-rateit-id",data.data.user_id);                              
                           parent.checkRated1('<?php echo PageContext::$response->rateField;?>');       
                       }
                       else{  
                      		 window.parent.showCommentBox('<?php echo PageContext::$response->type?>','<?php echo PageContext::$response->flag?>');
                      		parent.$(".rateit").attr("data-rateit-id",data.data.user_id);
                       }
                       }else{
                           alert(data.data.msg+' Since your account is not activated you cannot rate');
                           parent.$(".rateit").attr("data-rateit-id",data.data.user_id);
                       } 
                       
                      //$.colorbox.();
                    
                   }else{
                       setTimeout(function(){$("#msg_sign").html(data.message);
                       $("#msg_sign").addClass('error_div')},4000);
                       
                   }
              
            }
        });
        
     });
      });
</script>
<div class="content_login">
        <div class="" id="msg"></div>
        <h3>Login</h3>
        <div class="register_form">
            <?php echo PageContext::$response->objFormLogin; ?>
			<a href="#" class="signuplink" id="signup" onclick="showDiv('content_register','content_login')">Sign Up</a>
        </div>
        
        
    </div>
    <div class="clear"></div>
    
  <div class="content_register">
         <div class="" id="msg_sign"></div>
        <h3>Sign Up</h3>
        <div class="register_form">
            <?php echo PageContext::$response->objFormRegister; ?>
        </div>
        
        <a href="#" class="signuplink" id="login" onclick="showDiv('content_login','content_register')">Login</a>
    </div>