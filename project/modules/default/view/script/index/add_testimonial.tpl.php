<div class="margin_l_1 ">
    <div class="mediapost" id="comment_<?php echo PageContext::$response->testimonial_id; ?>">
        <div class="row">
            <div class="col-sm-1 col-md-1 col-lg-1">
                <div class="mediapost_left">
                    <span class="mediapost_pic">
                        <?php if(PageContext::$response->user_image) {?>
                        <img src="<?php echo PageContext::$response->userImagePath; ?><?php echo PageContext::$response->user_image; ?>">
                        <?php }else{ ?>
                         <img src="<?php echo PageContext::$response->userImagePath; ?>small/member_noimg.jpg">
                        <?php } ?>
                        </span>
                </div>
            </div>
            <div class="col-sm-9 col-md-9 col-lg-9">
                <div class="media-body">
                     <div class="wid100per pad5top testi_icon">
                        <span id="jtestimonial_content_<?php echo PageContext::$response->testimonial_id; ?>"><?php echo PageContext::$response->testimonial_content; ?></span>
                    </div>
                    <div class="wid100per">
                        <h4 class="media-heading">
                            <a class="yellow_text" href="<?php echo PageContext::$response->baseUrl?>timeline/<?php echo PageContext::$response->user_alias?>">
                                <?php echo PageContext::$response->testimonial_user; ?>
                            </a>
                        </h4>
                    </div>
                   
                </div>
            </div>
            <div class="col-sm-2 col-md-2 col-lg-2">
                <div class="pad5p">
                    <div class="datepaosted wid100per">
                        <div class="align-right">
                            <?php echo date('m-d-Y H:i:s', strtotime(PageContext::$response->testimonial_date)) ; ?>     
                        </div>
                    </div>
                    <div class="wid100per pad15top">
                        <div class="right">
                            <a class="edititem left marg5right jedit_link_testimonial" href="#" cid="<?php echo PageContext::$response->testimonial_id; ?>" id="<?php echo PageContext::$response->testimonial_id; ?>">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a class="deleteitem left jdeleteTestimonial" href="javascript:void(0)" aid="<?php echo PageContext::$response->testimonial_id ?>">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>