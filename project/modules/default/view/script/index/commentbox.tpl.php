<link type="text/css" rel="stylesheet" href="<?php echo BASE_URL?>project/styles/popup.css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery-1.8.0.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>bbf/public/js/jquery-ui-1.8.23.custom.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery.blockUI.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/fw.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.metadata.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.validate.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/app.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.colorbox.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/comment_popup.js"></script>
<script type="text/javascript"> 
   $( window ).load(function() {
    $('#frmComment').submit(function(e){
  
        e.preventDefault();
        var comment_content      = $("#comment_content").val();
        if(comment_content!=''){
        var entity_type      = $("#entity_type").val();
        var entity_id      = $("#entity_id").val();
       
        var dataString          = 'comment_content='+comment_content+"&entity_type="+entity_type+"&entity_id="+entity_id;
        $.ajax({
            type    : "POST",
            dataType: 'json',
            url     : "<?php echo BASE_URL?>index/addComment",
            data    : dataString,
            cache   : false,
            success : function(data){ 
               if(data.status=='SUCCESS')
                   { 
                       //window.parent.$("#login_status").val(data.data.user_id);
                 
                        parent.location.reload();
                       $.colorbox.close();
                      
                   }else{
                      
                       $("#msg").html("Invalid Credentials");
                       $("#msg").addClass('error_div');
                   }
              
            }
        });
      }
        
     });
     
      });
</script>
<div class="content_comment">
        <div class="" id="msg"></div>
        <h3>Post Your Comment</h3>
        <div class="register_form">
            <?php echo PageContext::$response->objFormComment; ?>
        </div>
        
      
    </div>
    <div class="clear"></div>
    
  