<div class="main_nav">
    <div class="nav wrapper">
        <ul id="menu">
            {if  PageContext::$response->sess_user_id <= 0}
            <li class="{if PageContext::$response->activeSubMenu eq ''}active{else} {/if}"><a href="{PageContext::$response->baseUrl}" class="nopadhome-nav">Home</a></li>
            {/if}
            {if  PageContext::$response->sess_user_id > 0}
            {/if}
            <li class="{if PageContext::$response->activeSubMenu eq 'community'}active{else} {/if}"><a  href="{PageContext::$response->baseUrl}community" {if  PageContext::$response->sess_user_id > 0} class="nopadhome-nav"{/if} >Bizcom</a></li>
            <li class="{if PageContext::$response->activeSubMenu eq 'business'}active{else} {/if}"><a  href="{PageContext::$response->baseUrl}businesses" >BizDirectory</a></li>
<!--            <li class="{if PageContext::$response->activeSubMenu eq 'classifieds'}active{else} {/if}"><a  href="{PageContext::$response->baseUrl}classifieds" >Classifieds</a></li>-->
            <li class="{if PageContext::$response->activeSubMenu eq 'aboutus'}active{else} {/if}"><a href="{PageContext::$response->baseUrl}aboutus" >About US</a></li>
           <!-- <li class="{if PageContext::$response->activeSubMenu eq 'features'}active{else} {/if}"><a href="{PageContext::$response->baseUrl}features">BizDash</a></li>-->
            <li class="{if PageContext::$response->activeSubMenu eq 'contactus'}active{else} {/if}"><a href="{PageContext::$response->baseUrl}contact">Contact Us</a></li>
            {if  PageContext::$response->sess_user_id > 0}
            <li class="{if PageContext::$response->activeSubMenu eq 'introduction'}active{else} {/if}"><a href="{PageContext::$response->baseUrl}refferal-request">Referral Request</a></li>
            {/if}
            {if  PageContext::$response->sess_user_id > 0}
            <li class="nomarg-nav {if PageContext::$response->activeSubMenu eq 'logout'}active{else} {/if} right_name">
                <div class="userdisplayname">
                    <a href="#" class="username_drpdwn">
                        {if PageContext::$response->sess_user_image != ''}
                        <img src="{PageContext::$response->userImagePath}{PageContext::$response->sess_user_image}">
                        {/if}
                        {if PageContext::$response->sess_user_image == ''}
                        <img src="{PageContext::$response->defaultImage}user_thumb.png">
                        {/if}
                        {PageContext::$response->sess_user_name}
                    </a></div>
                  <ul class="sub-menu">
                    <li class="{if PageContext::$response->activeSubMenu eq 'profile'}active{else} {/if}">
                        <a href="{PageContext::$response->baseUrl}profile/{PageContext::$response->sess_user_alias}">My Account</a>
                    </li>
                    <li>
                        <a href="{PageContext::$response->baseUrl}changepassword">Settings</a>
                    </li>
                    <li>
                        <a href="{PageContext::$response->baseUrl}logout">Log out</a>
                    </li>
                </ul>
            </li>  
            {else}
            <li class="{if PageContext::$response->activeSubMenu eq 'join'}active{else} {/if}"><a href="{PageContext::$response->baseUrl}join" >Sign up</a></li>
            <li class="nomarg-nav {if PageContext::$response->activeSubMenu eq 'login'}active{else} {/if}"><a href="{PageContext::$response->baseUrl}login" >Sign in</a></li>
            {/if}
        </ul>
    </div>
</div>
<div class="clear"></div>