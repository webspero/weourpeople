<div class="main_nav">
    <div class="nav wrapper">
        <ul id="menu">
            <li>
                <div class="login_db_search">
                    <form name="serachprofile" id="search" action="{PageContext::$response->baseUrl}index/searchprofile" method="GET">
                        <input  class="login_db_searchbox" name="searchtext" id="jSearchProfile" type="text" placeholder="Search Member,BizCom,BizDirectory">
                        <input type="hidden" name="search_selected_activity" id="search_selected_activity" >
                        <input name="" type="submit" value="" class="login_db_searchbtn">
                    </form>
                    <div class="clear"></div>
                </div>
            </li>
            {if  PageContext::$response->sess_user_id > 0}
            <li style="margin-top:5px;">
                <div class="frnd_requests">
                    <a class="notifi_no">16</a>
                    <a href="#" class="frnd_requests_icon"><span>Friends Requests</span></a>
                    <div class="clear"></div>
                </div>
            </li>
            <li style="margin-top:5px;">
                <div class="chat_list">
                    <a class="notifi_no">16</a>
                    <a href="#" class="chat_list_icon"><span>chats</span></a>
                    <div class="clear"></div>
                </div>
            </li>
            <li style="margin-top:5px;">
                <div class="mail_list">
                    <a class="notifi_no">16</a>
                    <a href="#" class="mail_list_icon"><span>mails</span></a>
                    <div class="clear"></div>
                </div>
            </li>
            <li style="margin-top:5px;">
                <div class="notifi_list">
                    <a class="notifi_no">16</a>
                    <a href="#" class="notifi_list_icon"><span>notifications</span></a>
                    <div class="clear"></div>
                </div>
            </li>
            
            <li class="nomarg-nav {if PageContext::$response->activeSubMenu eq 'logout'}active{else} {/if} right_name">
                <div class="userdisplayname">
                    <a href="#"  class="username_drpdwn">
                        {if PageContext::$response->sess_user_image != ''}
                        <img src="{PageContext::$response->userImagePath}{PageContext::$response->sess_user_image}">
                        {/if}
                        {if PageContext::$response->sess_user_image == ''}
                        <img src="{PageContext::$response->defaultImage}user_thumb.png">
                        {/if}
                        {PageContext::$response->sess_user_name}

                    </a>
                </div>
                    <!--<p class="user"> <img src="{PageContext::$response->defaultImage}user_thumb.png">
                <!--<a class="" href="{PageContext::$response->baseUrl}profile/{PageContext::$response->sess_user_alias}">{PageContext::$response->sess_user_name}</a> </p>
                <a class="account">{PageContext::$response->sess_user_name}</a> </p>-->
                <ul class="sub-menu">
                    <li class="{if PageContext::$response->activeSubMenu eq 'profile'}active{else} {/if}">
                    <img src="{PageContext::$response->ImagePath}account-icon.png"/><a href="{PageContext::$response->baseUrl}profile/{PageContext::$response->sess_user_alias}">My Account</a>
                    </li>
                    <li class="{if PageContext::$response->activeSubMenu eq 'profile'}active{else} {/if}">
                    <img src="{PageContext::$response->ImagePath}account-icon.png"/><a href="{PageContext::$response->baseUrl}my-friends">My Friends</a>
                    </li>
                    <li class="{if PageContext::$response->activeSubMenu eq 'profile'}active{else} {/if}">
                    <img src="{PageContext::$response->ImagePath}account-icon.png"/><a href="{PageContext::$response->baseUrl}pending-request">Pending Friend Request</a>
                    </li>
                    <li>
                    <img src="{PageContext::$response->ImagePath}setting-icon.png"/>    <a href="{PageContext::$response->baseUrl}changepassword">Settings</a>
                    </li>
                    <li>
                     <img src="{PageContext::$response->ImagePath}logout-icon.png"/>
                        <a href="{PageContext::$response->baseUrl}logout">Log out</a>
                    </li>
                </ul>
            </li>  
            {else}
<!--            <li class="{if PageContext::$response->activeSubMenu eq 'join'}active{else} {/if}"><a href="{PageContext::$response->baseUrl}join" >Sign Up</a></li>
            <li class="nomarg-nav {if PageContext::$response->activeSubMenu eq 'login'}active{else} {/if}"><a href="{PageContext::$response->baseUrl}login" >Sign In</a></li>-->
            {/if}
        </ul>
    </div>
</div>
<div class="clear"></div>