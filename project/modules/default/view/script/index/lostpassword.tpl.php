<div class="container">
    <div class="row">
        <div class="col-sm-2 col-md-3 col-lg-4 "></div>
        <div class="col-sm-8 col-md-6 col-lg-4 ">
            <section class="whitebox marg40col">
                <div class="signinform forgotform">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Forgot Password</h3>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                    </div>
                    <div class="form-bottom signup_form">
                        <p>To reset your password, enter the email address you use to login in to {PageContext::$response->siteName} </p>
                        <div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
                        <form name="frmLostPassword" id="frmLostPassword" action="" method="post" role="form">

                            <div class="form-group relative">
                                <i class="fa fa-envelope"></i>
                                <input type="text" value="{PageContext::$response->mail}"  name="user_email" class="form-email form-control" placeholder="Your Email" >
                                <label class="error" style="display:none;" generated="true" for="user_email"></label>
                            </div>
                            <input type="submit" name="btnSubmit" data-loading-text="Loading..." class="btn btn-primary btn-block yellow_btn2" value="Send">
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-sm-2 col-md-3 col-lg-4 "></div>
    </div>
</div>

