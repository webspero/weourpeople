<div class="content_right">
    {if PageContext::$response->flagDisplay!= 1}
    <div class="right_nav">
        {if PageContext::$response->sess_user_id >0}
        <ul>

            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}my-directory">My BizDirectory</a></li>
            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}my-community">My BizCom</a></li>
            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}my-classifieds">My Classifieds</a></li>
            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}my-campaigns">My Campaigns</a></li>
            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}my-saved-campaigns">My Saved Campaigns</a></li>
            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}my-pending-introductions">My Pending Introduction</a></li>
            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}my-accepted-introductions">My Accepted Introduction</a></li>
            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}my-pending-incentives-approval">My Pending Incentives Approval</a></li>
            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}pending-bizcom-members">Pending Bizcom Members</a></li>
            <li><a href="{PageContext::$response->baseUrl}pending-request">Pending Request</a></li>
            <li><a href="{PageContext::$response->baseUrl}pending-bizcom-request">Pending BizCom Request</a></li>
            <li class=""><a href="{PageContext::$response->baseUrl}invitation-sent">Invitations</a></li>
            <li class="no_btmborder"><a href="{PageContext::$response->baseUrl}inbox">Inbox</a></li>
            <!--<li class="no_btmborder"><a href="{PageContext::$response->baseUrl}friend-suggestions">Friend suggestion</a></li>-->
        </ul>
        {else}
        <ul>

            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}directory">BizDirectory</a></li>
            <li><a href="{php} echo PageContext::$response->baseUrl; {/php}community">BizCom</a></li>
            <li><a href="{PageContext::$response->baseUrl}aboutus">About Us</a></li>
            <li class="no_btmborder"><a href="{PageContext::$response->baseUrl}contact">Contact Us</a></li>
        </ul>
        {/if}
    </div>
    {/if}
    <div class="clear"></div>
    <div class="right_search">
        <h3>Search</h3>
        <form name="serachprofile" id="search" action="{PageContext::$response->baseUrl}index/searchprofile" method="GET">
            <input name="searchtext" id="jSearchProfile" type="text" placeholder="Search Member,Bizcom,Bizdirectory">
            <input type="hidden" name="search_selected_activity" id="search_selected_activity" >
            <input name="" type="submit" value="" class="search_btn">
        </form>
    </div>
</div>