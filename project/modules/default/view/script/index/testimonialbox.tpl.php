<link type="text/css" rel="stylesheet" href="<?php echo BASE_URL?>project/styles/popup.css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery-1.8.0.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>bbf/public/js/jquery-ui-1.8.23.custom.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery.blockUI.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/fw.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.metadata.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.validate.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/app.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.colorbox.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/comment_popup.js"></script>
<script type="text/javascript">
    $(window).load(function () {
        $('#frmComment').submit(function (e) {
            e.preventDefault();
            var testimonial_content = $("#testimonial_content").val();
            if (testimonial_content != '') {
                var my_company = $("#my_company").val();
                var my_designation = $("#my_designation").val();
                var entity_id = $("#entity_id").val();

                var dataString = 'my_designation=' + my_designation + "&my_company=" + my_company + "&testimonial_content=" + testimonial_content + "&entity_id=" + entity_id;

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "<?php echo BASE_URL?>index/add_testimonial",
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $('jTestimonials').prepend(data);
                        $.colorbox.close();
                    }
                });
            }

        });

    });
</script>
<div class="content_comment">
    <div class="" id="msg"></div>
    <h3>Post Your Testimonial</h3>
    <div class="register_form testimonialpopup_box_outer">
        <?php echo PageContext::$response->objFormComment; ?>
    </div>
</div>
<div class="clear"></div>

