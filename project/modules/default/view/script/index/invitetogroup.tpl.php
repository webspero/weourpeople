<link type="text/css" rel="stylesheet" href="<?php echo BASE_URL?>project/styles/popup.css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery-1.8.0.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>bbf/public/js/jquery-ui-1.8.23.custom.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery.blockUI.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/fw.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.metadata.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.validate.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/app.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.colorbox.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/comment_popup.js"></script>
<script type="text/javascript"> 
   $( window ).load(function() {
    $('#frmInvite').submit(function(e){
  
        e.preventDefault();
      var request;
     var $form = $('#frmInvite');
    // let's select and cache all the fields
    var $inputs = $form.find("input");
    
    // serialize the data in the form
    var serializedData = $form.serialize();
   
    // let's disable the inputs for the duration of the ajax request
  //  $inputs.attr("disabled", true);

    // fire off the request to /form.php
   $.post('<?php echo BASE_URL?>index/sendCommunityInviatation/', serializedData, function(response) {
     var response = jQuery.parseJSON(response);
 
     if(response.status == 'SUCCESS')
         {
             
               parent.location.reload();
                       $.colorbox.close();
         }else{
             $("#msg").html("Operation failed!");
                       $("#msg").addClass('error_div');
         }
     
     
     //$inputs.attr("disabled", false);
});
       
      });
      
      
      
      
      
      });
      
      function sendInvitation(senderId,communityId,userId,key)
      {
          $("#loading_invite"+key).html("<div class='addLoader'></div>");
          
         
           var dataString          = 'senderId='+senderId+'&communityId='+communityId+'&userId='+userId;
        $.ajax({
            type    : "POST",
            dataType: 'json',
            url     : "<?php echo BASE_URL?>index/sendCommunityInviatation",
            data    : dataString,
            cache   : false,
            success : function(data){
               if(data.status=='SUCCESS')
                   {
                      
                      
                       $("#"+key).html("Invitation Sent Successfully");
                       $("#"+key).addClass('success_div');
                      
                   }else{
                       
                       $("#"+key).html("Failed");
                       $("#"+key).addClass('error_div');
                   }
              
            }
        });
          
      }
</script>
<div class="content_login">
        <div class="" id="msg"></div>
        <h3>Invite Your Friends</h3>
     
        <form name="frmInvite" method="post" id="frmInvite"> 
            
         <?php
         if(PageContext::$response->members){
         foreach (PageContext::$response->members as $key=>$val) {  ?>  
       <div class="invite_list_blk">
					<div class="invite_list_img">
                                            <a target="_blank" href="<?php echo PageContext::$response->baseUrl.'profile/'.$val->user_alias?>">
                               <img src="<?php echo PageContext::$response->userImagePath?><?php  echo ($val->file_path)?$val->file_path:'member_noimg.jpg';?>">
                               </a>
					</div>
					<div class="invite_frnd_list_name">
						   <a target="_blank" href="<?php echo PageContext::$response->baseUrl.'profile/'.$val->user_alias?>" style="text-decoration:none">  <h5> <?php echo ($val->user_firstname)?$val->user_firstname:$val->user_email ?></h5></a>
					</div>
           <div class="invite_act_blk" id="<?php echo $key;?>"> 
						<div class="loading_invite" id="loading_invite<?php echo $key;?>"></div>
                                                <input type="button" name="invite" value="Invite" class="invite_btn" onclick="sendInvitation('<?php echo $val->user_id;?>','<?php echo PageContext::$response->communityId; ?>','<?php echo PageContext::$response->userId; ?>','<?php echo $key;?>')"> 
						
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
            
        <?php }}else {  echo "No Records found"; } ?>  
            
            <input type="hidden" name="communityId" value="<?php echo PageContext::$response->communityId; ?>">
            <input type="hidden" name="userId" value="<?php echo PageContext::$response->userId; ?>">
            
        </form>
        
  </div>  