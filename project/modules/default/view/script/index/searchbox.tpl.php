<form name="frmsearch" method="get" action="{PageContext::$response->search['action']}">
    <div class="srch_list">
        {php}
        if(sizeof(PageContext::$response->search['fields']) > 0) {
        echo '<select name="searchtype" class="right">';
            foreach(PageContext::$response->search['fields'] as $key=>$fields){
            echo '<option '.((PageContext::$request['searchtype']==$key)?'selected="selected"':'').' value="'.$key.'">'.$fields.'</option>';
            }
            echo ' </select>';
        }
        else
        echo '<input type="hidden" value="all" id= "searchtype" name="searchtype">';
        {/php}
    </div>
    <div class="input-group searchblk_box">
        <div class="display_table wid100per">
            <div class="display_table_cell wid90per vlaign_bottom">
                <input name="searchtext" type="text" class="input-sm form-control height32 no-box-shadow" placeholder="Search" value="{PageContext::$response->searchtext}">
            </div>
            <div class="display_table_cell wid10per vlaign_bottom">
                <button type="submit" class="btn btn-sm btn-primary border-radious-left"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </div>
</form>

    <!--<div class="input-group min_width252px"><input type="text" placeholder="Search" class="input-sm form-control"> 
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-sm btn-primary"> <i class="fa fa-search"></i></button> 
                        </span>
                        </div>-->