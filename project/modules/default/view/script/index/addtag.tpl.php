<div class="col-sm-12 col-md-12 col-lg-12" id="tag_list_<?php echo PageContext::$response->tagDetails->tagg_id; ?>">
    <div class="mediapost">
        <div class="mediapost_left pull-left">
            <span class="mediapost_pic">

                <a href="{PageContext::$response->baseUrl}page/{$lead->business_alias}">
                    <img src="<?php echo PageContext::$response->userImagePath; ?><?php if (PageContext::$response->tagDetails->file_path != '') {
    echo "small/" . PageContext::$response->tagDetails->file_path;
} elseif (PageContext::$response->tagDetails->file_path == '') {
    echo "default/no_preview_med.jpg";
} ?>" class="business_profile_desc_colside_pic">
                </a>
            </span>
        </div>

        <div class="media-body">
            <h4 class="media-heading"><span><a id="business_<?php echo PageContext::$response->tagDetails->tagg_id; ?>"><?php echo PageContext::$response->tagDetails->business_name; ?></a></span></h4>
                <div class="display_table">
                <div class="display_table_cell wid70per">
                    <p><span id="role_<?php echo PageContext::$response->tagDetails->tagg_id; ?>"><?php echo PageContext::$response->tagDetails->role; ?></span></p>
                </div>
                <div class="display_table_cell wid30per">
                    <div class="right">
                        <a class="edititem left marg5right" href="javascript:void(0)" title="Edit" onclick="updateTags('<?php echo PageContext::$response->tagDetails->tagg_id; ?>','<?php echo PageContext::$response->tagDetails->business_id; ?>')"><i class="fa fa-pencil"></i></a>
                        <a class="deleteitem left" href="javascript:void(0)"   title="Delete" onclick="deleteTags('<?php echo PageContext::$response->tagDetails->tagg_id; ?>','<?php echo PageContext::$response->tagDetails->business_id; ?>')"><i class="fa fa-trash"></i></a> 
                    </div>
                </div>
                </div>
                <p><span id="email_id_<?php echo PageContext::$response->tagDetails->tagg_id; ?>"><?php echo PageContext::$response->tagDetails->tag_buisness_email; ?></span> </p>
            </div>
        <div class="clearfix"></div>
    </div>
</div>




<!--<div class="list_item_container" id="tag_list_<?php echo PageContext::$response->tagDetails->tagg_id; ?>">
           <div class="image">
<img src="<?php echo PageContext::$response->userImagePath; ?><?php if (PageContext::$response->tagDetails->file_path != '') {
    echo "small/" . PageContext::$response->tagDetails->file_path;
} elseif (PageContext::$response->tagDetails->file_path == '') {
    echo "default/no_preview_med.jpg";
} ?>">
</div>
           
     <div class="label"> Works at <a id="business_<?php echo PageContext::$response->tagDetails->tagg_id; ?>" class=""><?php echo PageContext::$response->tagDetails->business_name; ?></a> as <span id="role_<?php echo PageContext::$response->tagDetails->tagg_id; ?>"><?php echo PageContext::$response->tagDetails->role; ?></span><a href="javascript:void(0)" class="delete_link_business" onclick="deleteTags('<?php echo PageContext::$response->tagDetails->tagg_id; ?>','<?php echo PageContext::$response->tagDetails->business_id; ?>')">Delete</a> <a href="javascript:void(0)" class="edit_link_business" onclick="updateTags('<?php echo PageContext::$response->tagDetails->tagg_id; ?>','<?php echo PageContext::$response->tagDetails->business_id; ?>')">Edit</a> &nbsp;</div>         
           </div>-->