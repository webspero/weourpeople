
<div class="footer_main">
    <div class="footer">
        <div class="footer_nav">
            <h5>Quick Links</h5>
            <ul>
                <li><a href="{PageContext::$response->baseUrl}">Home</a></li>
                <li><a href="{PageContext::$response->baseUrl}aboutus">About Us</a></li>
                <li><a href="{PageContext::$response->baseUrl}contact">Contact Us</a></li>
                <li><a href="{PageContext::$response->baseUrl}faq">FAQ</a></li>
            </ul>
        </div>
        <div class="footer_nav nav_pad">
            <h5>Privacy </h5>
            <ul>
                <li><a href="{PageContext::$response->baseUrl}terms">Terms</a></li>
                <li><a href="{PageContext::$response->baseUrl}privacy-policy">Privacy</a></li>
                <li><a href="{PageContext::$response->baseUrl}site-map">Sitemap</a></li>
                <li><a href="{PageContext::$response->baseUrl}disclaimer">Disclaimer</a></li>
                
            </ul>
        </div>
        <div class="footer_nav nav_pad">
            <h5>Business</h5>
            <ul>
<!--                <li><a href="{PageContext::$response->baseUrl}directory">BizDirectory</a></li>
                <li><a href="{PageContext::$response->baseUrl}community">BizCom</a></li>
                <li><a href="{PageContext::$response->baseUrl}classifieds">Classifieds</a></li>
                <li><a href="{PageContext::$response->baseUrl}features">BizDash</a></li>-->
            </ul>
        </div>
       <div class="footer_nav nav_nobg">
            <img src="{PageContext::$response->sitelogo}">
        </div>

    </div>
    <div class="clear"></div>
    <div class="copyright">
        © Copyright {PageContext::$response->curYear}. Socialware. All rights reserved
    </div>
</div>
<div class="clear"></div>