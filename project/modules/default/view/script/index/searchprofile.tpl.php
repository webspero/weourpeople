<div class="container">
    <div class="row"> 
    <div class="whitebox marg15col">
        <div class="col-sm-12 col-md-12 col-lg-12">

{if PageContext::$response->usersList|@count >0} 
            <h3>Find Friends</h3>
            {/if}
        </div>
        <section class="bizcomlisting listin">
            
            {if PageContext::$response->usersList|@count >0} 
            {foreach from=PageContext::$response->usersList key=id item=user}

            <div class="col-sm-4 col-md-4 col-lg-4">
            <div class="mediapost">
                <div class="picpost_left pull-left">
                    <span class="picpost_left_pic marg10top">
                        <img src="{if $user->file_path eq ''}{PageContext::$response->userImagePath}member_noimg.jpg{else}{PageContext::$response->userImagePath}{$user->file_path}{/if}">
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><a href="{PageContext::$response->baseUrl}timeline/{$user->user_alias}">{$user->user_firstname} {$user->user_lastname}</a></h4>
                    <p>{$user->user_friends_count} Friends </p>
                    <p>
                        {if PageContext::$response->sess_user_id > 0 && $user->user_id neq PageContext::$response->sess_user_id}
                        {if in_array($user->user_id, PageContext::$response->myfriends)} 
                        <a uid="{$user->user_id}" href="#"><span class="accept"><i class="fa fa-check"></i> Friends</span></a>
                        {else if in_array($user->user_id,PageContext::$response->myinvitedfriends)}
                        <a uid="{$user->user_id}" href="#"><span class="accept" id="jFriend_{$user->user_id}"><i class="fa fa-check"></i> Friend request sent</span></a>
                        {else if in_array($user->user_id,PageContext::$response->myPendingRequests)}
                        <a uid="{$user->user_id}" href="#" class="jaddfriend1" inviteid="{$user->inviteId}"><span class="accept" id="jFriend_{$user->user_id}"><b>+</b> Accept Friend</span></a>
                        {else}
                        <a uid="{$user->user_id}" href="#" class="jaddfriend" inviteid="{$user->inviteId}"><span class="accept" id="jFriend_{$user->user_id}"><b>+</b> Add Friend</span></a>
                        {/if}
                        {/if}
                    </p>
                    <div class="loader loaderposition1" id="loading_{$user->user_id}">
                      <img src="{PageContext::$response->userImagePath}default/loader.gif" />
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            </div>
            {/foreach}
            {else}
                <div class="rownoborder">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    No records found
                </div>
                </div>    
             {/if}

            
            <!--<div class="row rownoborder">
                <div class="colside1">
                    <div class="colside1_pic">
                        <img src="images/no_frnd_image.jpg">
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="colmiddle">
                    <h6><a href="#">Stuart brosnen</a></h6>
                    <p>
                        3 Friends
                    </p>
                </div>
                <div class="colside3">
                    <a class="friends_btn fright" href="#" style="float:left; ">
                        <span class="addfriend_icon"></span>
                        Add Friend
                    </a>

                </div>
            </div>-->
            
        </section>
        <div class="col-sm-12 col-md-12 col-lg-12">
            {PageContext::$response->pagination}
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        </div>
    </div>

    <div class="clear"></div>

</div>
