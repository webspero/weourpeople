<link rel="stylesheet" href="<?php echo BASE_URL ?>project/styles/rateit.css" type="text/css"/>
<script src="<?php echo BASE_URL ?>project/js/jquery.rateit.js" type="text/javascript"></script>
<script src="<?php echo BASE_URL ?>project/js/ratinglist.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        var page = 1;
        var _throttleTimer = null;
        var _throttleDelay = 100;
        $(window).scroll(function () {
            clearTimeout(_throttleTimer);
            _throttleTimer = setTimeout(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                    if (page < TOTAL_PAGES) {
                        page++;
                        $.blockUI({message: ''});
                        var business_id = $("#business_id").val();
                        var business_name = $("#business_name").val();
                        $.ajax({
                            type: 'Post',
                            url: mainUrl + 'user/view_organization_associated_bizcomajax/' + page + '/' + business_id,
                            data: 'business_id=' + business_id + '&business_name=' + business_name,
                            cache: false,
                            success: function (data) {
                                $('.margin_l_1').append(data);
                                $('.listitem').fadeIn(3000);
                                $('.listitem').hide();
                            }
                        })
                    }
                }
            }, _throttleDelay);
        });
    });
</script>
<div class="col-sm-12 col-md-12 col-lg-12">
<section class="whitebox marg20top">
<div class="bizcom_h3">
<h3> Groups Associated with <?php echo PageContext::$response->business_name;?>
<span class="addbusiness"><a href="<?php echo PageContext::$response->baseUrl;?>add-group/?business_id=<?php echo PageContext::$response->business_id; ?>">Add Group</a></span></h3>
</div>
<input type="hidden" id="business_id" value="<?php echo PageContext::$response->business_id;?>">
<input type="hidden" id="business_name" value="<?php echo PageContext::$response->business_name;?>">
<div id="jmessage"></div>
     <div id="tagged_people"> 
        <div class="row">
            <?php if (sizeof(PageContext::$response->associatedBizcom)== 0){?>
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="margin_l_1">No Groups Associated So Far</div>
                </div>
            <?php
            }
            else{
               
            foreach(PageContext::$response->associatedBizcom AS $id=>$user){?>
             <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="mediapost">
                <div class="picpost_left pull-left">
                    <span class="picpost_left_pic">
                        <a href="<?php echo PageContext::$response->baseUrl . 'group/' . $user->community_alias ?>">
                        <div class="thumbnail">
                        <img src="<?php echo ($user->file_orig_name == '') ? PageContext::$response->userImagePath . 'medium/member_noimg.jpg' : PageContext::$response->userImagePath . 'medium/' . $user->file_path; ?>">
                            </div>
                                </a>
                    </span>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl . 'group/' . $user->community_alias ?>"><?php echo $user->community_name ?>
                                            (<?php echo $user->member_count ?>)</a></h4>
                                </div>

                                <div id="starring">
                                    <div class="rateit" id="rateit_<?php echo $user->ratetype_id; ?>" data-rateit-value="<?php echo $user->ratevalue; ?>" data-rateit-ispreset="true"
                                         data-rateit-id="<?php echo $user->rateuser_id; ?>" data-rateit-type="<?php echo $user->ratetype; ?>" data-rateit-typeid="<?php echo $user->ratetype_id; ?>"
                                         data-rateit-flag="<?php echo $user->rateflag; ?>" data-rateit-step=1 data-rateit-resetable="false">
                                    </div>
                                    <div class="right">
                                        <?php if (PageContext::$response->sess_user_id == $user->community_created_by) { ?>
                                            <a href="<?php echo PageContext::$response->baseUrl; ?>edit-group/<?php echo $user->community_id; ?>" class="edititem left marg5right"><i
                                                    class="fa fa-pencil"></i></a>
                                            <a href="<?php echo PageContext::$response->baseUrl; ?>index/delete_community/<?php echo $user->community_id; ?>" onclick="return deleteConfirm();"
                                               class="deleteitem left"><i class="fa fa-trash"></i></a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <p>
                                    <?php echo substr($user->community_description, 0, 200) . "..."; ?>
                                </p>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    <?php } ?>
                    <!-- <div class="clear"></div>
                     </div>-->
                    <!-- <div class="row">-->
                    <!-- <div class="col-sm-12 col-md-12 col-lg-12">-->
                    <div><?php echo PageContext::$response->paginationAffilate; ?> </div>
                <?php } ?>
                <!--  </div>-->
                <!-- </div>-->
            </div>
        </div>
        <!--  <div class="clear"></div>-->
    </section>
</div>