<div class="container">
    <div class="row"> 
        <div class="col-md-offset-3 col-lg-offset-3 col-sm-12 col-md-6 col-lg-6 ">
            <div class="whitebox marg40col pad25px_left pad25px_right">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Create A New Group</h3>
                    </div>
                </div>
                <div class="form-bottom signup_form">
                    <div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
                    <form enctype="multipart/form-data" class="" action="" method="post" id="frmBusinessRegister" name="frmBusinessRegister" novalidate="novalidate">
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="text" value="" name="community_name" class="form-control" placeholder="Group Name">
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cnameadd" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="community_name" class="error"></label>   
                        </div>
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <textarea class="form-control" name="community_description" placeholder="Group Description"></textarea>
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                </div>
                            </div>
                            <label generated="true" for="community_description" class="error"></label>	                    
                        </div>
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <select class="form-control" value="" name="community_type" id="community_type" type="select"><option value="">Access Type</option><option value="PUBLIC">PUBLIC</option><option value="PRIVATE">PRIVATE</option></select>
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_ctypeadd" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="community_type" class="error"></label>  
                        </div>
                        {if PageContext::$response->org_grp eq 1}
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="radio" class="radiobusiness_type" name="radiobusiness_type" value="Personal">Personal
                                     <input type="radio" class="radiobusiness_type" name="radiobusiness_type" value="Business">Page
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_btypeadd" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="radiobusiness_type" class="error"></label>  
                        </div>
                        
                        <div class="form-group relative">
                             <div class="display_table wid100p">
                            <!--{PageContext::$response->business_id}--> 
<!--                            {PageContext::$response->businessArray|print_r}-->
                                <div class="display_table_cell wid98per">
                                    <select class="form-control" value="" name="business_type" id="business_type" type="select" style="display: none;">
                                        {foreach from=PageContext::$response->businessArray key=businessname item=id}

                                                <option value="{$id}" {if PageContext::$response->getData['community_business_id'] eq $id OR {PageContext::$response->business_id} eq $id}selected{/if}>{$businessname}</option>
                                        {/foreach}
                                            <!--, <option value="0">Personal</option>-->

                                    </select>
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                </div>
                            </div>
                                <label generated="true" for="business_type" class="error"></label>
                        </div>
                        {/if}

                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="file" class="left" onchange="upload(1140,258,'community_image_id')" id="community_image_id" placeholder="Upload File" size="10" value="" name="community_image_id"><span id="spn_asterisk_cimageadd" style="color:red;">*</span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span class="left" id="spn_support_imgadd"></span>
                                </div>
                            </div>
                            <div class="wid100p">(Supported format:jpg,jpeg,gif,png)</div>
                             <label generated="true" id="label_image_id" for="community_image_id" class="error"></label>
                        </div>
<!--                         <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    Upload logo 
                                    <input type="file" class="left" placeholder="Upload File" size="10" value="" name="community_logo_id"><span id="spn_asterisk_clogoadd" style="color:red;">*</span>
                                <div class="clearfix"></div>
                                <span class="left" id="spn_support_logoadd">(Supported format:jpg,jpeg,gif,png)</span>
                                </div>
                                
                                
                               
                            </div>
                        </div>-->
                        <div class="form-group relative">
                            <input type="submit" class="btn btn-primary yellow_btn2" value="Save" name="btnSubmit" id="btnSubmit" ondblclick="this.disabled=true;">
                            <input type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary yellow_btn2" value="Cancel" name="btnCancel">
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--

<div class="content_left">
    <div class="content">
        <div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
        <h3>Add BizCom</h3>
        <div class="register_form">
            {PageContext::$response->objForm}
        </div>
    </div>
    <div class="clear"></div>
</div>
-->
