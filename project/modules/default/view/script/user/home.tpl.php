<!-- {php}PageContext::renderRegisteredPostActions('messagebox');{/php} -->
<div class="home_content_left">
<div class="login_dashboard_wrapper">
	 <div class="dashboard_blocks contents" id="tab-Personal">
			<div class="dashboard_block">
				<div class="col_1">
					<div class="db_block_img bizbfrndz">
					
					</div>
				</div>
				<div class="col_2">
					<p>Interact with friends, businesses,
	communities and extend your network with new
	contacts</p>
					<a href="{PageContext::$response->baseUrl}profile/{PageContext::$response->sess_user_alias}#Friends">BizBFriend</a>
				</div>
			</div>        
			<div class="dashboard_block">
				<div class="col_1">
					<div class="db_block_img bizcom">
					
					</div>
				</div>
				<div class="col_2">
					<p>Find, create, join, and interact with Business communities.</p>
					<a href="{PageContext::$response->baseUrl}my-community">BizCom</a>
				</div>
			</div>
			<div class="dashboard_block dashboard_block_nomargin">
				<div class="col_1">
					<div class="db_block_img bizbdirectory">
					
					</div>
				</div>
				<div class="col_2">
					<p>Follow businesses, rate them, & keep up to date with their offerings </p>
					<a href="{PageContext::$response->baseUrl}businesses">BizDirectory</a>
				</div>
			</div>
			
		<div class="dashboard_block_row">
			<div class="dashboard_block">
				<div class="col_1">
					<div class="db_block_img rewards">
					
					</div>
				</div>
				<div class="col_2">
					<p>Support the businesses you value most and create a “Champions portfolio”</p>
					<a href="{PageContext::$response->baseUrl}champions-portfolio">Business Champions</a>
				</div>
			</div>
			<div class="dashboard_block">
				<div class="col_1">
					<div class="db_block_img newintr">
					
					</div>
				</div>
				<div class="col_2">
					<p>Meet new people through the people you already know through Business champions</p>
					<a href="{PageContext::$response->baseUrl}refferal-request">Recommend A BizBfriend</a>
				</div>
			</div>
			<div class="dashboard_block dashboard_block_nomargin">
				<div class="col_1">
					<div class="db_block_img rewards">
					
					</div>
				</div>
				<div class="col_2">
					<p>Collect and manage participation in business campaigns through online interest stages.</p>
					<a href="#">Rewards & Incentives</a>
				</div>
			</div>
		</div>
		<div class="dashboard_block_row">
			<div class="dashboard_block">
				<div class="col_1">
					<div class="db_block_img rewards">
					
					</div>
				</div>
				<div class="col_2">
					<p>Contribute to charities through online participation</p>
					<a href="#">Charitable Donations</a>
				</div>
			</div>
			<div class="dashboard_block">
				<div class="col_1">
					<div class="db_block_img newintr">
					
					</div>
				</div>
				<div class="col_2">
					<p>Offer and receive gifts, promotions and vouchers within communities for new business generation.</p>
					<a href="#">Products and Vouchers</a>
				</div>
			</div>
			<div class="dashboard_block dashboard_block_nomargin">
				<div class="col_1">
					<div class="db_block_img messg">
					
					</div>
				</div>
				<div class="col_2">
					<p>Offer and benefit from direct cash contributions based on business value based conversations.</p>
					<a href="#">Cash Incentives</a>
				</div>
			</div>
		</div>
     </div>
	 
    
    <div class="dashboard_blocks contents" id="tab-Business" style="display:none">
     	<div class="dashboard_block">
        	<div class="col_1">
            	<div class="db_block_img bizbfrndz">
                
                </div>
            </div>
        	<div class="col_2">
            	<p>Interact with Friends,businesses,communities & find new contacts</p>
                <a href="{PageContext::$response->baseUrl}profile/{PageContext::$response->sess_user_alias}#Friends">BizbFriends</a>
            </div>
        </div>
        <div class="dashboard_block">
        	<div class="col_1">
            	<div class="db_block_img bizcom">
                
                </div>
            </div>
        	<div class="col_2">
            	<p>Find, Create and join Business communities</p>
                <a href="{PageContext::$response->baseUrl}my-community">BizCom</a>
            </div>
        </div>
        <div class="dashboard_block dashboard_block_nomargin">
        	<div class="col_1">
            	<div class="db_block_img bizbdirectory">
                
                </div>
            </div>
        	<div class="col_2">
            	<p>Find new business that you want to Follow and keep up date With theri offerings</p>
                <a href="{PageContext::$response->baseUrl}my-directory">BizbDirectory</a>
            </div>
        </div>
        <div class="dashboard_block">
        	<div class="col_1">
            	<div class="db_block_img rewards">
                
                </div>
            </div>
        	<div class="col_2">
            	<p>Collect create and store your rewards</p>
                <a href="#">Rewards</a>
            </div>
        </div>
        
     	<div class="clear"></div>
     </div>
     
	<div class="clear"></div>
</div>
</div>


    
    

  
