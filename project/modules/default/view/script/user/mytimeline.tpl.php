<div id="fb-root"></div>
{literal}
<script>

    $(document).ready(function () { //alert('hi');
        magnificPopupSingleFn();
        var page = 1;
        var _throttleTimer = null;
        var _throttleDelay = 100;
        $(window).scroll(function () {
            clearTimeout(_throttleTimer);
            _throttleTimer = setTimeout(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                    if (page < TOT_TIMELINE_PAGES) {
                        page++;
                        $.blockUI({message: ''});
                        var user_alias = $("#user_alias").val();
                        $.ajax({
                            type: 'Post',
                            url: mainUrl + 'user/timelineajax/' + page,
                            data: 'alias=' + user_alias,
                            success: function (data) {

                                var html = $.parseHTML(data);
                                enableEmojiOn(html);
                                $('.jfeeddisplay_div').append(html);

                                // parse facebook share button                               
                                FB.XFBML.parse();

                                magnificPopupSingleFn();
                            }
                        })
                    }
                }
            }, _throttleDelay);
        });
    });

</script>
{/literal}

<!-- Profile Timeline-------->
<div class="container">
    <div class="row">
        {if PageContext::$response->user_name neq ''}
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="timelineheader">

                <div class="timelineheader_right">
                    <div class="cover-wrapper"
                         style="background-image: url('{PageContext::$response->userImagePath}timeline/{PageContext::$response->timelineimage}');">
                        <div class="profilepic_timelineblk">
                            <div class="timelineheader_left">
                                {if PageContext::$response->user_image neq ''}
                                <div class="timeline_left_pic"
                                     style="background-image: url('{PageContext::$response->userImagePath}{PageContext::$response->user_image}');"></div>
                                {else}
                                <div class="timeline_left_pic"
                                     style="background-image: url('{PageContext::$response->userImagePath}member_noimg.jpg');">

                                </div>
                                {/if}
                                <h3><a href="#" id="jtimelinepic_user" target="_blank">{PageContext::$response->user_name}</a></h3>
                                {if PageContext::$response->alias eq PageContext::$response->sess_user_alias}
                                <form name="frmtimelineimageupload" id="jfrmtimelineimageupload"
                                      action="{PageContext::$response->baseUrl}user/timelineimageresize"
                                      enctype="multipart/form-data" method="post">
                                    <i class="fa fa-camera" id="jcamProfile"></i><span class="name"></span>
                                    <input type="file" name="timelinepic" id="file_profile" style="display:none;">
                                    <input type="hidden" value="{PageContext::$response->sess_user_id}" id="user_id"
                                           name="user_id">
                                </form>
                                {if PageContext::$response->user_image neq ''}     
                                    <a href="#" aid="{$feed['community_announcement_id']}" class="dltbtn jProfileDeletetButton colorgrey_text"><i class="fa fa-trash icn-fnt-size"></i></a>
                                {/if}
                               
                                {/if}
                            </div>
                        </div>
                        <div class="menufloattop">
                            {if PageContext::$response->alias neq PageContext::$response->sess_user_alias &&
                            PageContext::$response->sess_user_id > 0}
                            {if in_array(PageContext::$response->id, PageContext::$response->myfriends)}
                            <a href="#" name="reply_message" class="replymessage" id=""
                               onclick="replay_message({PageContext::$response->friend_to},'');" title="Send Message"
                               src="#">
                                <span class="accept"><i class="fa fa-envelope-o"></i> Send Message</span>
                            </a>
                            {if PageContext::$response->userDetails['user_friends_privacy']=='N' &&
                            PageContext::$response->sess_user_id > 0}
                            <a href="#" name="mutual_friends" id=""
                               onclick="mutual_friends({PageContext::$response->id},{PageContext::$response->sess_user_id});">
                                <span class="accept pull-right"> Mutual Friends({PageContext::$response->mutual_friend_count})</span>
                            </a>
                            {/if}
                            {/if}
                            {/if}
                            {if PageContext::$response->sess_user_id > 0 && PageContext::$response->id neq
                            PageContext::$response->sess_user_id && PageContext::$response->sess_user_status != 'I'}
                            {if in_array(PageContext::$response->id, PageContext::$response->myfriends)}

                            <a class=" jUnfriend" uid="{PageContext::$response->friend_to}" href="#"
                               id="jUnfriend_{PageContext::$response->friend_to}">
                                <span class="reject"><i class="fa fa-times"></i> Unfriend</span>
                            </a>
                            {else if in_array(PageContext::$response->id,PageContext::$response->myinvitedfriends)}
                            <a class="friends_btn fright friend " uid="{PageContext::$response->id}" href="#">
                                <span class="addfriend_icon jaddfriend"></span>
                                <span id="jFriend_{PageContext::$response->id}">Friend request sent</span>
                            </a>
                            {else if in_array(PageContext::$response->id,PageContext::$response->myPendingRequests)}
                            <a class="friends_btn fright friend jaddfriend1" uid="{PageContext::$response->id}" href="#"
                               inviteid="{PageContext::$response->inviteId}">
                                <span class="addfriend_icon "></span>
                                <span id="jFriend_{PageContext::$response->id}">Accept Friend</span>
                            </a>
                            {else}
                            <a class="friends_btn fright friend jaddfriend" uid="{PageContext::$response->id}" href="#">
                                <span class="addfriend_icon jaddfriend"></span>
                                <span id="jFriend_{PageContext::$response->id}">Add Friend</span>

                            </a>

                            <div class="loading right" id="loading_{PageContext::$response->id}" style="display: none;">
                                <img src="{PageContext::$response->userImagePath}default/loader.gif"/>
                            </div>

                            {/if}
                            {/if}
                        </div>

                        {if PageContext::$response->alias eq PageContext::$response->sess_user_alias}
                        <div class="timelineimage">
                            <form name="frmtimelineimageupload" id="jfrmtimelineimageupload"
                                  action="{PageContext::$response->baseUrl}user/timelineimageresize"
                                  enctype="multipart/form-data" method="post">
                                <i class="fa fa-camera" id="jcam"></i><span class="name"></span>
                                <input type="file" name="timelinepic" id="jtimelinepic" style="display:none;">
                            </form>
                        </div>
                        {/if}
                    </div>
                    <div class="timeline_tabmenu">
                        {if !in_array(PageContext::$response->id, PageContext::$response->myfriends) &&
                        PageContext::$response->id neq PageContext::$response->sess_user_id && PageContext::$response->sess_user_id gt 0}
                        <input type='hidden' id='timeline_view' value='no_friend'>
                        <input type='hidden' id='user_id' value={PageContext::$response->id}>
                        <input type='hidden' id='user_alias' value={PageContext::$response->alias}>
                        {/if}
                        <ul id="myTab" class="nav nav-tabs nav-justified">
                          
                            <li class="active jShowFeeds"><a href="#newsfeed" data-toggle="tab">News Feed</a>
                            </li>
                   

                            <li class="jShowProfile" alias="{PageContext::$response->alias}"
                                uid="{PageContext::$response->sess_user_id}"><a href="#myprofile" data-toggle="tab">{if
                                    PageContext::$response->alias eq PageContext::$response->sess_user_alias &&
                                    PageContext::$response->sess_user_alias != '' }My Profile{else}Profile{/if}</a>
                            </li>
                            <li class="jShowFriends jShowFriends_tab" alias="{PageContext::$response->alias}"
                                uid="{PageContext::$response->sess_user_id}"><a href="#tab-Friends" data-toggle="tab">Friends
                                    ({if
                                    PageContext::$response->userDetails['user_friends_count']>0}{PageContext::$response->userDetails['user_friends_count']}{else}0{/if})</a>
                            </li>
                            {if PageContext::$response->alias eq PageContext::$response->sess_user_alias}
                            <li class="jShowFriends_tab jshowtimeline" alias="{PageContext::$response->alias}"
                                uid="{PageContext::$response->sess_user_id}"><a href="#tab-Friends" data-toggle="tab">Photos</a>
                            </li>
                            {/if}
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="marg15col">
    <div class="container">
        <div class="row">
            {if PageContext::$response->friend_display eq '1' || PageContext::$response->gallery_display eq '1'}
            {if count(PageContext::$response->friend_display_list) gt 0 ||
            count(PageContext::$response->gallery_display_list) gt 0}
            {assign var=first_col value='col-sm-4 col-md-3 col-lg-3'}
            {assign var=second_col value='col-sm-8 col-md-9 col-lg-9'}


            {else}
            {assign var=first_col value='col-sm-12 col-md-12 col-lg-12'}
            {assign var=second_col value='col-sm-12 col-md-12 col-lg-12'}


            {/if}
            {else}
            {assign var=first_col value='col-sm-12 col-md-12 col-lg-12'}
            {assign var=second_col value='col-sm-12 col-md-12 col-lg-12'}

            {/if}
            <div class="{$first_col}">
                {if PageContext::$response->friend_display eq '1'}
                {if count(PageContext::$response->friend_display_list) gt 0}
                <div class="whitebox">
                    <div class="hdsec">
                        <h3><i class="fa fa-user" aria-hidden="true"></i> Friends <a href="#" id="jMorephotos"
                                                                                     class="pull-right">More <i
                                    class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                    </div>
                    <ul class="row frndboxlist">
                        {foreach from=PageContext::$response->friend_display_list key=id item=friendslist}
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <a class="friendphotobox_blk"
                               href="{PageContext::$response->baseUrl}timeline/{$friendslist->user_alias}">
                                {if $friendslist->file_path}
                                <div class="friendphotobox"
                                     style="background-image: url('{PageContext::$response->userImagePath}{$friendslist->file_path}');">
                                </div>
                                {else}
                                <div class="friendphotobox"
                                     style="background-image: url('{PageContext::$response->userImagePath}medium/member_noimg.jpg');">
                                </div>
                                {/if}
                                <h4>{$friendslist->user_firstname|ucfirst} {$friendslist->user_lastname|ucfirst}</h4>
                            </a>
                        </li>
                        {/foreach}
                    </ul>
                </div>
                {/if}
                {/if}

                {if in_array(PageContext::$response->id, PageContext::$response->myfriends) ||
                PageContext::$response->id eq PageContext::$response->sess_user_id}
                {if PageContext::$response->gallery_display eq '1'}
                {if count(PageContext::$response->gallery_display_list) gt 0}
                <div class="whitebox">
                    <div class="hdsec">
                        <h3><i class="fa fa-file-image-o" aria-hidden="true"></i> Photos <a href="#" id="jMoreGallery"
                                                                                            class="pull-right">More <i
                                    class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                    </div>
                    <ul id="portfolio_timeline" class="row frndboxlist">
                        {foreach from=PageContext::$response->gallery_display_list key=id item=gallerylist}
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4 vert_align_top"><a class="friendphotobox_blk"
                                                                                  href="{PageContext::$response->userImagePath}{$gallerylist->news_feed_image_name}"
                                                                                  title="{$gallerylist->news_feed_comment}"><img
                                    src="{PageContext::$response->userImagePath}{$gallerylist->news_feed_image_name}"
                                    alt=""></a></li>
                        {/foreach}
                    </ul>
                </div>
                {/if}
                {/if}
                {/if}
            </div>
            <div class="{$second_col} jdisplay_div">
                {if PageContext::$response->sess_user_id gt 0 &&
                PageContext::$response->id eq PageContext::$response->sess_user_id}
                <form role="form" id="logoform" method="post" action="" enctype="multipart/form-data">
                <div class="whitebox jpost_comment_div">
                    <div class="emotionsbtn_textblk">
                        <textarea placeholder="Post your comments.." name="newsfeed_comment" id="newsfeed_comment"
                                  class="jEmojiTextarea"></textarea>
                    </div>
                    <div class="wid100 pad5top">
                        <div class="display_image_div">
                            <div id="juploadimagepreview"></div>
                        </div>
                        <div class="pull-left">
                            <a href="#" class="camicon"><label for="file_feed"><i class="fa fa-camera"
                                                                                  aria-hidden="true"></i></label></a>
                            <input type="file" id="file_feed" class="" onchange="previewFile1()"
                                   style="cursor: pointer;  display: none"/>
                        </div>
                        <div class="pull-right">
                               <input type="hidden" id="image_id" value="">
                            <input type="button" onclick="postNewsFeed()" value="Post" class="btn primary post_btn">
                            <div class="loader" id="post_feed_loader" style="display: none;">
                                <img src="{PageContext::$response->userImagePath}default/loader.gif"/>
                            </div>
                        </div>
                <div class="profile-bnr">
                            <img class="img img-responsive center" src="{if PageContext::$response->campaignDetails->voucher_image_name neq ''}{PageContext::$response->userImagePath}{PageContext::$response->campaignDetails->voucher_image_name}{/if}" height="50">
                            </div>
                        <div class="clearfix"></div>
                        <div id="jerrordiv" style="color:#f00"></div>
                    </div>
                </div>
                    </form>
                {/if}
                <div class='jfeeddisplay_div'>
                    {foreach PageContext::$response->news_feed1 as $feed}

                    {assign var=flag value={$flag}+1}
                    <div class="whitebox timelinebox" id="news_feed_{$feed['news_feed_id']}">
                        <div class="wid100per">
                            <div class="postheadtablediv">
                                <div class="post_left">
                                    <span class="mediapost_pic">
                                        <a href="{PageContext::$response->baseUrl}timeline/{$feed['user_alias']}">
                                        {if $feed['file_path'] neq ''}
                                        <img class='timeline_feed_user_image'
                                            src="{PageContext::$response->userImagePath}{$feed['file_path']}">
                                        {else}
                                        <img class='timeline_feed_user_image' src="{PageContext::$response->userImagePath}medium/member_noimg.jpg">
                                        {/if}
                                        </a>
                                    </span>
                                </div>
                                <div class="post_right">
                                    <div class="posthead"><h4 class="media-heading"><a href="{PageContext::$response->baseUrl}timeline/{$feed['user_alias']}">{$feed['Username']}</a>
                                            <span></span></h4></div>
                                    <div class="postsubhead">
                                        <div class="postsubhead_right"><span>{if $feed['days'] gt 0} {$feed['days']} days ago 
                                                {else} {assign var="timesplit" value=":"|explode:$feed['output_time']}
                                                {if $timesplit[0]>0}{$timesplit[0]|ltrim:'0'} hrs ago
                                                {else}
                                                {if $timesplit[1]>0}
                                                {$timesplit[1]|ltrim:'0'}{if $timesplit[1] eq 1} min{else} mins{/if} ago
                                                {else}
                                                Just Now
                                                {/if}
                                                {/if}
                                                {/if}</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="postsubhead_left">
                                {assign var="reg_exUrl" value= '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/'}

                                {if preg_match({$reg_exUrl}, {$feed['news_feed_comment']},$url)}

                                    <div class="jEmojiable">{preg_replace({$reg_exUrl}, "<a href='{$url[0]}'>{$url[0]}</a>" , {$feed['news_feed_comment']})}</div>

                                {else}

                                    <div class="jEmojiable">{$feed['news_feed_comment']}</div>

                                {/if}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <a href="#">
                            {if $feed['news_feed_image_name']}
                            {assign var="pimg" value=$feed['news_feed_image_name']}
                            <div class="postpic">

                                <ul class="portfolio" class="clearfix main_gallary">
                                    <li><a href="{PageContext::$response->userImagePath}{$pimg}" title=""><img
                                                src="{PageContext::$response->userImagePath}{$pimg}" alt=""></a>
                                    </li>
                                </ul>

                            </div>
                            {/if}
                        </a>
                        <!----------------Statistics ------------------------>
                        {if PageContext::$response->sess_user_id gt 0}
                        <div class="mediapost_user-side">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="likesec">
                                        <div class="display_table_cell pad10_right">
                                            <a href="#"
                                               class=" colorgrey_text jLikeNewsFeed {if $feed['news_feed_num_like'] > 0} liked {/if}"
                                               id="jlike_{$feed['news_feed_id']}" cid="{$feed['news_feed_id']}">
                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                                <span id="jlikedisplay_{$feed['news_feed_id']}">Like </span></a>
                                        </div>
                                        <div class="display_table_cell pad10_right">
                                            <i class="fa fa-comments-o icn-fnt-size"></i>
                                            <a href="#" id="jCommentButton_{$feed['news_feed_id']}"
                                               aid="{$feed['news_feed_id']}"
                                               class="jNewsfeedCommentButton colorgrey_text">Comment</a>
                                        </div>
                                        {if PageContext::$response->sess_user_id eq $feed['news_feed_user_id']}
                                        <div class="display_table_cell pad10_right">
                                            <i class="fa fa-trash-o icn-fnt-size"></i>
                                            <a href="#" id="jDeleteButton_{$feed['news_feed_id']}"
                                               aid="{$feed['news_feed_id']}"
                                               class="jNewsfeedDeletetButton colorgrey_text">Delete</a>
                                        </div>
                                        {/if}
                                    </div>
                                </div>
                                <div class="col-sm-8 col-md-8 col-lg-8">
                                    <div class="shareitemsblks">
                                        <div id="feed_like_users_{$feed['news_feed_id']}" class="like_users_div" style=" display: none;">
                      {foreach from=PageContext::$response->newsFeedLikeUsers[$feed['news_feed_id']] key = id item=val } 
                      <span>{$val->news_feed_like_user_name}<br></span>
                      {/foreach}  
                    </div>
                                        {php} $url_share=urlencode(PageContext::$response->baseUrl.'newsfeed-detail/');{/php}
                                        <span class="mediapost_links jShowFeedLikeUsers" id="jcountlike_{$feed['news_feed_id']}">{if $feed['news_feed_num_like'] > 0}{$feed['news_feed_num_like']} Likes {/if}</span>
                                    <span class="mediapost_links jNewsfeedCommentButton count_class"
                                          aid="{$feed['news_feed_id']}" id="jcountcomment_{$feed['news_feed_id']}">{if $feed['news_feed_num_comments'] > 0}{$feed['news_feed_num_comments']}
                                        Comments 
                                        {/if}
                                    </span>
                                        <div class="fb-share-button"
                                             data-href="{PageContext::$response->baseUrl}newsfeed-detail/{$feed['news_feed_alias']}"
                                             data-layout="button_count" data-mobile-iframe="true"><a
                                                class="fb-xfbml-parse-ignore" target="_blank"
                                                href="https://www.facebook.com/sharer/sharer.php?u={$url_share}{$feed['news_feed_alias']};src=sdkpreparse">Share</a>
                                        </div>
                                        <a href="{PageContext::$response->baseUrl}newsfeed-detail/{$feed['news_feed_alias']}"
                                           title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                                    </div>
                                </div>
                            </div>
                        </div>
{/if}
                        <div class="mediapost_user-side-top clear jCommentDisplayDiv"
                             id="jNewsfeedCommentBoxDisplayDiv_{$feed['news_feed_id']}" style="display:none">
                            <p class="lead emoji-picker-container">
                            <div class="emotionsbtn_textblk">
                                <textarea class="jEmojiTextarea form-control textarea-control"
                                          id="watermark_{$feed['news_feed_id']}" rows="3"
                                          placeholder="Write your comment here..."></textarea>
                                <a class="upload_cambtn" id="{$feedlist->community_announcement_id}">
                                    <label for="file_{$feed['news_feed_id']}"><i
                                            class="fa fa-camera feed-camera"></i></label></a>
                                <input type="file" id="file_{$feed['news_feed_id']}" aid="{$feed['news_feed_id']}"
                                       cmid="{$feed['news_feed_comment_id']}" class="jfeedimageimage_file"
                                       style="cursor: pointer;  display: none"/>
                                <input type="hidden" id="announcement_id"
                                       value="{$feedlist->community_announcement_id}">
                            </div>

                            </p>
                            <div class="display_image_div">
                                <div id="juploadcommentfeedimagepreview_{$feed['news_feed_id']}"></div>
                            </div>
                            <div class="msg_display" id="msg_display_{$feed['news_feed_id']}"></div>
                            <div class="clearfix"></div>
                            <a id="postButton" cid="{$feedlist->community_id}" cmid="" aid="{$feed['news_feed_id']}"
                               class="btn yellow_btn fontupper pull-right jPostNewsfeedCommentButton"> Post</a>
                        </div>

                        <div class="clear commentblk jCommentDisplayDiv"
                             id="jNewsfeedCommentDisplayDiv_{$feed['news_feed_id']}" style="display:none">
                            <div id="posting_{$feed['news_feed_id']}">
                                <!------------------------Comments-------------------------------->
                                {assign var=val value=0}
                                {if count(PageContext::$response->newsFeed[{$feed['news_feed_id']}]) gt 0}
                                {foreach from=PageContext::$response->newsFeed[{$feed['news_feed_id']}] key=id
                                item=comments}
                                {if $comments->news_feed_comments_id}
                                <input type="hidden" id="val_{$feed['news_feed_id']}"
                                       value="{count(PageContext::$response->newsFeed[{$feed['news_feed_id']}])}">
                                <div class="col-md-12 btm-mrg pad10p" id="jdivComment_{$comments->news_feed_comments_id}">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <a href="{PageContext::$response->baseUrl}timeline/{$comments->user_alias}">
                                                {if $comments->user_image neq ''}
                                            <span class="mediapost_pic">
                                                <img class="ryt-mrg"
                                                     src="{PageContext::$response->userImagePath}{$comments->user_image}">
                                            </span>
                                                {/if}
                                                {if $comments->user_image eq ''}
                                            <span class="mediapost_pic">
                                                <img class="ryt-mrg"
                                                     src="{PageContext::$response->userImagePath}medium/member_noimg.jpg">
                                            </span>
                                                {/if}
                                            </a>
                                        </div>
                                        <div class="col-md-11">
                                            <span class="name"> <a href="{PageContext::$response->baseUrl}timeline/{$comments->user_alias}">{$comments->Username}</a> </span>
                                            <span class="jEmojiable">{$comments->news_feed_comment_content}</span>
                                            <span class="new-text">

                                                {$comments->commentDate} 
                                            </span>
                                            <div class="commentimg_box">
                                                {if $comments->file_path neq ''}
                                                <img class="image_zoom"
                                                     src="{PageContext::$response->userImagePath}medium/{$comments->file_path}">
                                                {/if}
                                            </div>
                                            <div class="mediapost_user-side1">
                                                <span rel="tooltip" title="{$comments->users} liked this comment" class="mediapost_links"
                                                      id="jcountfeedcommentlike_{$comments->news_feed_comments_id}">{if $comments->num_comment_likes > 0}{$comments->num_comment_likes} Likes {/if}</span>
                                                <input type="hidden" id="reply_{$comments->news_feed_comments_id}"
                                                       value="{if $comments->num_replies > 0}{$comments->num_replies}{else}0{/if}">
                                                <div class="mediapost_links " cid="{$comments->announcement_comment_id}"
                                                     id="jcountreply_{$comments->news_feed_comments_id}">
                                                    <a href="#" class="jShowFeedReply"
                                                       id="{$comments->news_feed_comments_id}">
                                                        <span
                                                            id="jcountreply_{$comments->news_feed_comments_id}">{if PageContext::$response->newsFeed[{$feed['news_feed_id']}][{$comments->news_feed_comments_id}] gt 0}{PageContext::$response->newsFeed[{$feed['news_feed_id']}][{$comments->news_feed_comments_id}]} Replies {/if}
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="comment_sharelike_sec">
                                                {if $comments->news_feed_comment_user_id eq PageContext::$response->sess_user_id }
                                                <a href="#" class="jNewsFeedCommentDelete marg10right"
                                                   aid="{$comments->news_feed_id}"
                                                   id="{$comments->news_feed_comments_id}"><i class="fa fa-times"></i>
                                                    Delete</a>
                                                {/if}
                                                <a href="#" class="jFeedCommentReply marg10right"
                                                   aid="{$comments->news_feed_id}"
                                                   cid="{$comments->news_feed_comments_id}"
                                                   id="{$comments->news_feed_comments_id}"><i class="fa fa-reply"></i>
                                                    Reply</a>
                                                <a href="#"
                                                   class="jNewsFeedCommentLike {if $comments->num_comment_likes>0} liked {/if}"
                                                   id="jlikeComment_{$comments->news_feed_comments_id}"
                                                   cid="{$comments->news_feed_comments_id}"
                                                   aid="{$comments->news_feed_id}">

                                                    <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                                    <span id="jlikedisplaycomment_{$comments->announcement_comment_id}">Like </span></a>
                                            </div>

                                            <div id="jdivReply_{$comments->news_feed_comments_id}"
                                                 class="jDisplayReply">
                                                <p class="lead emoji-picker-container">
                                                    <div class="emotionsbtn_textblk">
                                                        <textarea placeholder="Write your reply here..."
                                                                  class="form-control textarea-control jEmojiTextarea"
                                                                  style="height:35px;"
                                                                  id="watermark_{$comments->news_feed_id}_{$comments->news_feed_comments_id}"
                                                                  class="watermark" name="watermark">
                                                        </textarea>
                                                        <a class="upload_cambtn"
                                                           id="{$feedlist['community_announcement_id']}">
                                                            <label for="imagereply_file_{$comments->news_feed_comments_id}"><i
                                                                    class="fa fa-camera feed-reply-camera"></i></label>
                                                            <div class="file_button">
                                                                <input type="file"
                                                                       id="imagereply_file_{$comments->news_feed_comments_id}"
                                                                       cid="{$comments->news_feed_comments_id}"
                                                                       class="jfeedimagereply_file"
                                                                       style="display:none;"/>
                                                            </div>
                                                        </a>
                                                        <input type="hidden" id="reply_comment_id"
                                                               value="{$comments->announcement_comment_id}">
                                                    </div>
                                                </p>
                                                <div class="msg_display"
                                                     id="msg_display_{$comments->news_feed_comments_id}"></div>
                                                <div class="clearfix"></div>
                                                <a id="shareButton" cid="{$feedlist['community_id']}"
                                                   cmid="{$comments->news_feed_comments_id}"
                                                   aid="{$comments->news_feed_id}"
                                                   class="btn yellow_btn fontupper pull-right jPostNewsfeedCommentButton">
                                                    Post</a>
                                            </div>
                                            <div class="loader"
                                                 id="imagereply_loader_{$comments->announcement_comment_id}">
                                                <img src="{PageContext::$response->userImagePath}default/loader.gif"/>
                                            </div>
                                            <div id="jDivReplyImagePreview_{$comments->news_feed_comments_id}"></div>
                                            <div id="postingReply_{$comments->news_feed_comments_id}"></div>
                                        </div>
                                    </div>
                                </div>
                                {/if}
                                {/foreach}
                                {else}
                           
                                {/if}
                                <!------------------------EOF Comments-------------------------------->
                            </div>


                        </div>
                        <div class="clearfix"></div>
                    </div>


                    {/foreach}
                </div>

                <!-----------Profile display and edit section--------------------------------------->
                <div class='jFriends_div whitebox' style='display:none'></div>
                <div class='jGallary_div whitebox' style='display:none'></div>
                <div class="tab-pane fade" id="myprofile" style='display:none'>

                    <div id="msgDisplay"></div>
                    <div id="jMyProfile" class='whitebox' style="display:none;">
                        <img src="{PageContext::$response->userImagePath}default/loader.gif"/> Loading....
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins pad5top">
                                <div class="ibox-content bordertop_0" id="jDivDisplayEdit" style="display:none">
                                    <form id="frmprofile_edit">
                                        <div class="form-group"><label class="col-lg-3 control-label">First Name</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="text" id="user_firstname"
                                                               class="form-control valid"
                                                               value="{PageContext::$response->userDetails['user_firstname']}"
                                                               placeholder="First Name" name="user_firstname"
                                                               required="true">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"> *</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="spn_asterisk" id="spn_asterisk_cnamemsg"
                                                  style="color:red"></span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Last Name</label>

                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="text" class="form-control" placeholder="Last Name"
                                                               size="10"
                                                               value="{PageContext::$response->userDetails['user_lastname']}"
                                                               id="user_lastname" name="user_lastname" required="true">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"> *</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="spn_asterisk" id="spn_asterisk_clnamemsg"
                                                  style="color:red"></span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Email </label>

                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="email" class="form-control"
                                                               placeholder="Your Email" size="10"
                                                               value="{PageContext::$response->userDetails['Email']}"
                                                               id="user_email" name="user_email" required="true">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"> *</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <span class="spn_asterisk" id="spn_asterisk_emailmsg"
                                                  style="color:red"></span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Date of
                                                Birth</label>

                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="text" class="form-control "
                                                               placeholder="Date of Birth" size="10"
                                                               value="{PageContext::$response->userDetails['Date_of_Birth']}"
                                                               id="user_date_of_birth" name="user_date_of_birth">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Gender</label>

                                            <div class="col-lg-9">

                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <select class="form-control valid" placeholder="Gender"
                                                                selected="Array" id="user_gender" name="user_gender"
                                                                type="select">
                                                            <option value="">Select</option>
                                                            <option value="M" {if PageContext::$response->
                                                                userDetails['Gender'] eq 'M'}selected{/if} >Male
                                                            </option>
                                                            <option value="F" {if PageContext::$response->
                                                                userDetails['Gender'] eq 'F'}selected{/if}>Female
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Address</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <div class="emotionsbtn_textblk">
                                                            <textarea class="form-control" placeholder="Address"
                                                                      id="user_address" name="user_address"
                                                                      type="textarea">{PageContext::$response->userDetails['Address']}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">City</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="text" class="form-control" placeholder=" City"
                                                               size="10"
                                                               value="{PageContext::$response->userDetails['City']}"
                                                               id="user_city" name="user_city">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Country</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <select class="form-control user_country valid"
                                                                placeholder="Business Country" selected="Array"
                                                                id="user_country" name="user_country" type="select">
                                                            {foreach from=PageContext::$response->countrydropdown key=id
                                                            item=country}
                                                            <option value="{$country}" {if PageContext::$response->
                                                                userDetails['user_country'] eq
                                                                $country}selected{/if}>{$id}
                                                            </option>
                                                            {/foreach}
                                                            <!--<option value="AF">Afghanistan</option><option value="AL">Albania</option><option value="DZ">Algeria</option>-->
                                                        </select>
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">State</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <select class="form-control user_state valid" id="user_state"
                                                                placeholder="Business Country" selected="Array"
                                                                name="user_state" type="select" required="true">
                                                        </select>
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Phone</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="text" class="form-control" placeholder="Telephone"
                                                               size="10"
                                                               value="{PageContext::$response->userDetails['Phone']}"
                                                               id="user_phone" name="user_phone">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Zipcode</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="text" class="form-control" placeholder="Zip Code"
                                                               size="10"
                                                               value="{PageContext::$response->userDetails['Zip_Code']}"
                                                               id="user_zipcode" name="user_zipcode">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span class="spn_asterisk" id="spn_asterisk_zipmsg"
                                                              style="color:red"></span></div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-3 col-lg-9">
                                                <input id="user_alias" class=" " type="hidden" placeholder="" size="10"
                                                       value="{PageContext::$response->alias}" name="user_alias">
                                                <input type="button" class="btn btn-sm yellow_btn3 marg5right left"
                                                       value="Update" id="jUpdate" name="btnSubmit"> <input
                                                    type="button" class="btn btn-sm grey_btn jShowProfile left"
                                                    alias="{PageContext::$response->alias}"
                                                    uid="{PageContext::$response->sess_user_id}" value="Cancel"
                                                    name="btnCancel">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="tagg-business" style="display:none">
                        <div id="tagform" class="tagform whitebox">
                            <h3>Organisation</h3>
                            <div class="row">
                                <div id="jmessage"></div>
                                <div class="col-lg-12">
                                    <div class="ibox-content">
                                        <div class="row">
                                            <form name="serachbuisnessform" id="serachbuisnessform" action=""
                                                  method="GET">

                                                <div class="form-group"><label class="col-lg-3 control-label">Select
                                                        Organisation</label>
                                                    <div class="col-lg-9">
                                                        <input name="searchtext" id="serachbuisness" type="text"
                                                               placeholder="Organisation Name"
                                                               class="busname form-control" required="true">

                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-lg-9">
                                                    <input type="hidden" name="search_selected_activity_buisness"
                                                           id="search_selected_activity_buisness">
                                                    <input type="hidden" name="search_selected_activity_buisness_name"
                                                           id="search_selected_activity_buisness_name">
                                                    <input type="hidden" name="search_selected_activity_buisness_alias"
                                                           id="search_selected_activity_buisness_alias">
                                                    <input type="hidden" name="search_selected_activity_buisness_image"
                                                           id="search_selected_activity_buisness_image">
                                                    <input type="hidden" name="user_image_path" id="user_image_path"
                                                           value="{php} echo PageContext::$response->userImagePath;{/php}">
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-3 col-lg-9">
                                                        <input type="submit" name="add" value="Add"
                                                               class="invite_btn btn btn-sm yellow_btn3"
                                                               title="invite-block" style="margin-top:0px!important;">
                                                        <input type="hidden" name="tagg_id" value="0" id="tagg_id">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <section class="businesseditlists">
                                        <div class="row whitebox" id="tagList">
                                            {foreach from=PageContext::$response->tagDetails key=id item=lead}
                                            <div class="col-sm-12 col-md-12 col-lg-12" id="tag_list_{$lead->tagg_id}">
                                                <div class="mediapost">
                                                    <div class="mediapost_left pull-left">
                                                        <span class="mediapost_pic">
                                                            <a id="image_link_{$lead->tagg_id}"
                                                               href="{PageContext::$response->baseUrl}organisation/{$lead->business_alias}">
                                                                <img
                                                                    src="{php} echo PageContext::$response->userImagePath;{/php}{if $lead->file_path neq ''}thumb/{$lead->file_path}{elseif $lead->file_path eq ''}thumb/noimage.jpg{/if}"
                                                                    alt="" id="image_{$lead->tagg_id}"
                                                                    class="business_profile_desc_colside_pic">
                                                            </a>
                                                        </span>
                                                    </div>

                                                    <div class="media-body">
                                                        <h4 class="media-heading"><span><a
                                                                    id="business_{$lead->tagg_id}"
                                                                    href="{PageContext::$response->baseUrl}directory/{$lead->business_alias}">{$lead->business_name}</a></span>
                                                        </h4>
                                                        <div class="display_table">
                                                            <div class="display_table_cell wid70per">
                                                            </div>
                                                            <div class="display_table_cell wid30per">
                                                                <div class="right">
                                                                    <a class="edititem left marg5right"
                                                                       href="javascript:void(0)" title="Edit"
                                                                       onclick="updateTags('{$lead->tagg_id}','{$lead->business_id}')"><i
                                                                            class="fa fa-pencil"></i></a>
                                                                    <a class="deleteitem left" href="javascript:void(0)"
                                                                       title="Delete"
                                                                       onclick="deleteTags('{$lead->tagg_id}','{$lead->business_id}')"><i
                                                                            class="fa fa-trash"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            {/foreach}
                                        </div>
                                    </section>
                                    <div class="clearfix"></div>

                                </div>
                            </div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>

                <!--EOF profile section-->


            </div>
{else}
 <div class="clear"></div>
 <br><br>
<div class='whitebox'>
    No data found
</div>
{/if}
        </div>
    </div>
</section>

<!--codeversion_dev-->


<div class="clear"></div>
<div class="listitem loader">
    <img src="{PageContext::$response->userImagePath}default/loader.gif"/> Loading....
</div>
</div>
</div>
</div>
</div>
</div>

<!--Friends end-->
</div>
<div class="clear"></div>
</div>
