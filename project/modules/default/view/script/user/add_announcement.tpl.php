<div class="container">
    <div class="row"> 
<div class="col-md-offset-3 col-lg-offset-3 col-sm-12 col-md-6 col-lg-6 ">
<div class="whitebox marg40col pad25px_left pad25px_right">
<div class="form-top">
<div class="form-top-left">
<h3>Make New Announcement</h3>
</div>
</div>
<div class="form-bottom signup_form">
<form method="post" enctype="multipart/form-data" name="add_announcement" id="add_announcement"><div id="anouncement_add_sec"  class="annsection_cont_outer" style="" >
<div class="{PageContext::$response->messagebox['msgClass']}">{PageContext::$response->messagebox['msg']}</div>
<input type="hidden" name="announcement_id" id="announcement_id" value=""/>

<div class="row">
<!--            <form>-->
<div class="form-group">
<div class="form-group relative">
<div class="display_table wid100p">
<div class="display_table_cell wid98per">
<select class="form-control" value="" name="community" id="select_community" type="select" required="true">
<option value="" >--Select BizCom--</option>
{foreach from=PageContext::$response->communityDetails key=communityname item=id}
<!--{$communityname|print_r}-->
<option value="{$id->community_id}" {if PageContext::$response->getData['community_id'] eq $id OR {PageContext::$response->community_id} eq $id}selected{/if}>{$id->community_name}</option>
{/foreach}

</select>
</div>
<div class="display_table_cell wid2per vlaign_middle align-right">
<span id="spn_asterisk_cnameadd" style="color:red">*</span>
</div>
</div>
<span id="errcommunity" class="communityerrmsg"></span> 
</div>

<div class="form-group relative">
<div class="display_table wid100p">
<div class="display_table_cell wid98per">
<input name="announcement_title" id="announcement_title" type="text" class="form-control" placeholder="Title" required="true" >
</div>
<div class="display_table_cell wid2per vlaign_middle align-right">
<span id="spn_asterisk_cnameadd" style="color:red">*</span>
</div>
</div>
<span id="errtitle" class="announcerrmsg"></span>
</div>

<div class="form-group relative">
<div class="display_table wid100p">
<div class="display_table_cell wid98per">
<textarea class="form-control" id="announcement_desc" name="announcement_desc" placeholder="Description"></textarea>
</div>
<div class="display_table_cell wid2per vlaign_middle align-right">
<span id="spn_asterisk_cnameadd" style="color:red">*</span>
</div>
</div>
<span id="errdesc" class="announcerrmsg"></span>
</div>

<div class="form-group relative">
<div class="display_table wid100p">
<div class="display_table_cell wid98per">
<input type="radio" value="A" name="announcement_status" checked="checked" required="true"/> <span>Active</span> &nbsp; 
<input type="radio" name="announcement_status" value="I" required="true"/> <span>Inactive</span>
<input type="hidden" name ="hiddate" id="hiddate" value="<?php echo date('m/d/Y'); ?> ">
<input type="hidden" name="hidcommunity_id" id="hidcommunity_id" value="<?php echo PageContext::$response->communityId;?>">
</div>
</div>
</div>

<div class="form-group relative">
<div class="display_table wid100p">
<div class="display_table_cell wid98per">
<input type="file" class="left" placeholder="Upload File" id="file" name="file"/>   

 <div class="clearfix"></div>
<span class="left" id="spn_support_imgadd">(Supported format:jpg,jpeg,gif,png)</span>
</div>
</div>
</div>

<div class="form-group relative">
<input type='submit' value='ADD' class="btn btn-primary yellow_btn2" name='submit'>
<input type="hidden" name="tagg_id" value="0" id="tagg_id">
</div>






<!-- 
<label class="col-lg-3 control-label">Select BizCom <span style="color: red;">*</span></label>

<div class="col-lg-9">
<select class="form-control" value="" name="community" id="select_community" type="select" required="true">
<option value="" >--Select BizCom--</option>
{foreach from=PageContext::$response->communityDetails key=communityname item=id}

<option value="{$id->community_id}" {if PageContext::$response->getData['community_id'] eq $id OR {PageContext::$response->community_id} eq $id}selected{/if}>{$id->community_name}</option>
{/foreach}

</select>
</div>  
<div class="clearfix"></div>
<span id="errcommunity" class="communityerrmsg"></span>
</div>
<div class="form-group">
<label class="col-lg-3 control-label">Title <span style="color: red;">*</span></label>
<div class="col-lg-9">
<input name="announcement_title" id="announcement_title" type="text" class="form-control" required="true" >
</div>
<div class="clearfix"></div>
<span id="errtitle" class="announcerrmsg"></span>
</div>
<div class="form-group"><label class="col-lg-3 control-label">Description <span style="color: red;">*</span></label>
<div class="col-lg-9">
<textarea name="announcement_desc" id="announcement_desc"   class="form-control" required> </textarea>
<div class="clear"></div>
</div>
<div class="clearfix"></div>
<span id="errdesc" class="announcerrmsg"></span>
</div>
<div class="form-group"><label class="col-lg-3 control-label">Status</label>
<div class="col-lg-9">
<input type="radio" value="A" name="announcement_status" checked="checked" required="true"/> <span>Active</span> &nbsp; 
<input type="radio" name="announcement_status" value="I" required="true"/> <span>Inactive</span>
<input type="hidden" name ="hiddate" id="hiddate" value="<?php echo date('Y-m-d'); ?> ">
<input type="hidden" name="hidcommunity_id" id="hidcommunity_id" value="<?php echo PageContext::$response->communityId;?>">
</div>
<div class="clearfix"></div>
<span id="errdb" class="announcerrmsg"></span>
</div>
<div class="form-group"><label class="col-lg-3 control-label">Select Picture </label>
<div class="col-lg-9">

<input type="file" id="file" name="file"/>   
</div>
<div class="clear"></div>

<div class="clearfix"></div>

</div>
<div class="form-group">
<div class="col-lg-offset-3 col-lg-9">
<input type='submit' value='ADD' class='yellow_btn' name='submit'>

<input type="hidden" name="tagg_id" value="0" id="tagg_id">
</div>
</div>
</div> -->


</div>
</form>
</div>
</div>
</div>

</div>
    </div>