<div class="container">
    <div class="row"> 
        <div class="col-md-offset-3 col-lg-offset-3 col-sm-12 col-md-6 col-lg-6 ">
         
<!--<div>{PageContext::$response->message}</div>-->
            <div class="whitebox marg40col pad25px_left pad25px_right">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Send Message</h3>
                    </div>
                </div>
                <div class="form-bottom signup_form">
                    <div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
                    <form enctype="multipart/form-data" class="" action="" method="post" id="frmSendMessage" name="frmSendMessage">
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input  class="form-email form-control margtop10" name="friend_name" id="jSearchFriend" type="text" placeholder="Search Your Friends">
                                    <input  class="" name="friend_id" id="friend_id" type="hidden">

                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cnameadd" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="friend_name" class="error"></label>   
                     
                      
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                   <input  class="margtop10 form-control" value="{PageContext::$request['message_subject']}" name="message_subject" id="message_subject" type="text" placeholder="Subject">
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                </div>
                            </div>
                            <label generated="true" for="message_subject" class="error"></label>	                    
                        
                   
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <textarea class="form-control margtop10" name="message_detail" id ="message_detail" placeholder="Message">{PageContext::$request['message_detail']}</textarea>
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_ctypeadd" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="message_detail" class="error"></label>  
                        </div>
                        <div class="form-group relative">
                            <input type="submit" class="btn btn-primary yellow_btn2" value="Send" name="btnSubmit">
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--

<div class="content_left">
    <div class="content">
        <div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
        <h3>Add BizCom</h3>
        <div class="register_form">
            {PageContext::$response->objForm}
        </div>
    </div>
    <div class="clear"></div>
</div>
-->
