<?php foreach (PageContext::$response->list as $id => $category) { ?>
    <div class="col-sm-6 col-md-6 col-lg-4">
            <div class="cateforylist_items">
            <div class="display_table_cell vlaign_middle ht70">
            <div class="wid100per display_block">
                 <img src="<?php echo PageContext::$response->userImagePath;?>thumb/<?php echo $category->file_path;?>">
            <a href="<?php echo PageContext::$response->baseUrl; ?>page/<?php echo $category->business_alias; ?>"><?php echo $category->business_name; ?></a>
            
            <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <?php if($category->business_created_by == PageContext::$response->sess_user_id){?>
            <div class="wid100per display_block">         
                <span>
                <a class="edititem left marg5right"
                   href="<?php echo PageContext::$response->baseUrl;?>edit-page/<?php echo $category->business_id;?>"><i
                        class="fa fa-pencil"></i></a>
                <a class="deleteitem left" onclick="return deleteConfirm();"  href="<?php echo PageContext::$response->baseUrl; ?>index/delete_organization/<?php echo $category->business_id;?>"><i
                        class="fa fa-trash"></i></a>
                </span>
                <div class="clearfix"></div>
            </div>
            <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>