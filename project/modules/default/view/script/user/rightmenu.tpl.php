<nav id="c-menu--slide-right" class="c-menu c-menu--slide-right">
  <button class="c-menu__close">Close Menu &rarr;</button>
  <ul class="c-menu__items">
      <div id='cssmenu'>
      <ul>
        <li class='has-sub'><a class ='main' href='#'>Profile</a>
            <ul>
              <li><a href="{PageContext::$response->baseUrl}profile/{PageContext::$response->sess_user_alias}">My Profile</a></li>
            </ul>
        </li>
        <li class='has-sub'><a class ='main' href='#'>Friends</a>
            <ul>
              <li><a class="tooltip-left" data-tooltip="Your Friends" href="{PageContext::$response->baseUrl}my-friends">My Friends</a></li>
              <li><a class="tooltip-left" data-tooltip="Your Pending Friend Requests" href="{php} echo PageContext::$response->baseUrl; {/php}pending-request">Pending Friend Requests</a></li>
            </ul>
        </li>
        <li class='has-sub'><a class ='main' href='#'>Message</a>
            <ul>
              <li><a href="{PageContext::$response->baseUrl}inbox">Inbox</a></li>
              <li><a href="{PageContext::$response->baseUrl}sentitems">Sent Items</a></li>
              <!--<li><a href="{PageContext::$response->baseUrl}invitation-sent">Invitations Sent</a></li>-->
            </ul>
        </li>
        <li class='has-sub'><a class ='main' href='#'>Groups</a>
            <ul>
              <li><a href="{php} echo PageContext::$response->baseUrl; {/php}add-group">Create A New Group</a></li>
              <li><a href="{PageContext::$response->baseUrl}my-groups">Manage an existing Group</a></li>
              <li><a href="{PageContext::$response->baseUrl}my-subscribed-group">My Subscribed Groups</a></li>
              <li><a href="{PageContext::$response->baseUrl}add-announcement">Make New Announcement</a></li>
              <li><a href="{PageContext::$response->baseUrl}pending-bizcom-request">Invitations for you to join other Groups</a></li>
              <li><a href="{PageContext::$response->baseUrl}pending-bizcom-members">Pending requests to join your Groups</a></li>
              <li><a href="{PageContext::$response->baseUrl}pending-members">Pending approvals to join your Groups</a></li>
            </ul>
        </li>

        <li class='has-sub'><a href='#'>Organiasations</a>
            <ul>
              <li><a href="{PageContext::$response->baseUrl}pages">My Pages</a></li>
              <li><a href="{php} echo PageContext::$response->baseUrl; {/php}add-page">Create A New Page</a></li>
            </ul>
        </li>
      </ul>

      </div>
  </ul>
</nav><!-- /c-menu slide-right -->








            
        




























<div id="c-mask" class="c-mask"></div><!-- /c-mask -->

