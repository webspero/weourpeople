
<header class="inrheader">
    <div class="container">
        <div class="row"> 
            <div class="col-sm-12 col-md-1 col-lg-1">
                <div class="logo">
                    <a href="{if PageContext::$response->sess_user_id>0}
                       {PageContext::$response->baseUrl}newsfeed/{PageContext::$response->sess_user_alias}
                       {else}
                       {PageContext::$response->baseUrl}
                       {/if}">
                        <img src="{PageContext::$response->innerlogo}"/>
                    </a>
                </div>
            </div>
            <div class="col-sm-12 col-md-5 col-lg-5">
                <div class=" header-search login_db_search">

                    <form role="form" class="form" name="serachprofile" id="search" action="{PageContext::$response->baseUrl}index/searchprofile" method="GET">
                        <div class="input-group">
                            <input id="jSearchProfile"  name="searchtext" type="text" placeholder="Search Member, Group, Page" class="form-control login_db_searchbox">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default login_db_searchbtn"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>

                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 ">
                <ul class="hdr_indication_icon pull-right" id="menu">

                    {if  PageContext::$response->sess_user_id > 0}
                    <li>
                        <div class="mail_list">
                            {if PageContext::$response->messageCount > 0}
                                <a href="{PageContext::$response->baseUrl}inbox" class="notifi_no notifi_no_msg" title="Mails">{PageContext::$response->messageCount}</a>
                            {/if}
                            <a href="{PageContext::$response->baseUrl}inbox" class="mail_list_icon" title="Mails"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li>
                        <div class="notifi_list">
                            {if PageContext::$response->NumFriendRequest > 0}
                                <a href="{PageContext::$response->baseUrl}pending-request" class="notifi_no notifi_no_friend" title="New Friend Request">{PageContext::$response->NumFriendRequest}</a>
                              {/if}
                            <a href="{PageContext::$response->baseUrl}pending-request" class="notifi_list_icon" title="New Friend Request"><i class="fa fa-user-plus" aria-hidden="true"></i></a>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li>
                        <div class="notifi_list">
                            {if PageContext::$response->NumPrivateRequest > 0}
                                <a href="{PageContext::$response->baseUrl}pending-members" class="notifi_no notifi_no_request" title="New Pending Joinee">{PageContext::$response->NumPrivateRequest}</a>
                              {/if}
                            <a href="{PageContext::$response->baseUrl}pending-members" class="notifi_pending_list_icon" title="New Pending Joinee"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li class="nomarg-nav {if PageContext::$response->activeSubMenu eq 'logout'}active{else} {/if} right_name">
                        <ul class="hdrtopnav nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle right" data-toggle="dropdown">

                                    <div class="userdisplayname">
                                        {if PageContext::$response->sess_user_image != ''}
                                        <img class="my_pic" src="{PageContext::$response->userImagePath}{PageContext::$response->sess_user_image}">
                                        {elseif PageContext::$response->sess_Image != ''}
                                        <img class="my_pic" src="{PageContext::$response->userImagePath}{PageContext::$response->sess_Image}">
                                        {else}
                                        <img src="{PageContext::$response->defaultImage}user_thumb.png">
                                        {/if}
                                        {PageContext::$response->sess_user_name}
                                        <i class="fa fa-sort-desc" aria-hidden="true"></i>

                                    </div>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="{if PageContext::$response->activeSubMenu eq 'profile'}active{else} {/if}">
                                        <a href="{PageContext::$response->baseUrl}timeline/{PageContext::$response->sess_user_alias}">My Timeline</a>
                                    </li>

                                    <li>
                                        <a href="{PageContext::$response->baseUrl}my-groups">My Groups</a>
                                    </li>

                                    <li>
                                        <a href="{PageContext::$response->baseUrl}pages">My Pages</a>
                                    </li>

                                    <li>
                                        <a href="{PageContext::$response->baseUrl}changepassword">Settings</a>
                                    </li>
                                    <li>
                                        <a href="{PageContext::$response->baseUrl}logout">Log out</a>
                                    </li>
                                    <div class="clearfix"></div>
                                </ul>
                                <div class="clearfix"></div>
                            </li>
                        </ul>

                    </li> 


                    {/if}
                </ul>
                <div class="clearfix"></div>

            </div>
        </div>
</header>