<div class="container">
    <div class="row"> 
        <div class="col-md-offset-3 col-lg-offset-3 col-sm-12 col-md-6 col-lg-6 ">
            <div class="whitebox marg40col pad25px_left pad25px_right">
				<div class="form-top">
	        		<div class="form-top-left">
	        			<h3>Edit Group</h3>
	        		</div>
	            </div>
	            <div class="form-bottom signup_form">
	            	<div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
	                <form enctype="multipart/form-data" class="" method="post" id="frmBusinessEdit" name="frmBusinessEdit">
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="text" name="community_name" value="{PageContext::$response->getData['community_name']}" class="form-control" placeholder="Group Name">
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cnameedit" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="community_name" class="error"></label>
	                </div>
	                <div class="form-group relative">
                            <div class="display_table wid100p">
                            <div class="display_table_cell wid98per">
                            <textarea class="form-control" name="community_description" placeholder="Group Description">{PageContext::$response->getData['community_description']|stripslashes}</textarea>
                             </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cnameedit" style="color:red"></span>
                                </div>
                            </div>
                            <label generated="true" for="community_description" class="error"></label>	                    	                    
			</div>
                             {if PageContext::$response->org_grp eq 1}
                            <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="radio" class="radiobusiness_type" name="radiobusiness_type" value="Personal" {if PageContext::$response->getData['community_business_id'] eq 0} checked {/if}>Personal
                                    <input type="radio" class="radiobusiness_type" name="radiobusiness_type" value="Business" {if PageContext::$response->getData['community_business_id'] neq 0} checked {/if}>Page
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_btypeadd" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="radiobusiness_type" class="error"></label>  
                        </div>
                           
                        <div class="form-group relative business_div" style="{if PageContext::$response->getData['community_business_id'] eq 0}display:none;{/if}">
                            <div class="display_table wid100p">
                            <div class="display_table_cell wid98per">
                            <select class="form-control" placeholder="Group type" selected="Array" name="community_business_id" id="community_business_id" type="select" >

                                {foreach from=PageContext::$response->businessArray key=businessname item=id}
                                    <option value="{$id}" {if PageContext::$response->getData['community_business_id'] eq $id}selected{/if}>{$businessname}</option>
                                {/foreach}
                            </select>
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cnameedit" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="community_business_id" class="error"></label>
                        </div>
                            {/if}
                            
<!--                        <div class="form-group relative category_div" style="{if PageContext::$response->getData['community_business_id'] neq 0}display:none;{/if}">
                            <div class="display_table wid100p">
                            <div class="display_table_cell wid98per">
                            <select class="form-control" placeholder="Category" name="community_category_id" id="community_category_id" value="" >

                                        {foreach from=PageContext::$response->categories key=category item=id}
                                            <option value="{$id}" {if  PageContext::$response->getData['community_category_id'] eq $id ||  PageContext::$response->getData eq $id } selected="selected" {/if}>{$category}</option>
                                        {/foreach}

                                    </select>
                            </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cnameedit" style="color:red">*</span>
                                </div>
                            </div>
                        </div>-->
                            
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                            <div class="display_table_cell wid98per">
                            <select class="form-control" placeholder="Group type" selected="Array" name="community_type" id="community_type" type="select">
                                <option value="" {if PageContext::$response->getData['community_type'] eq ''}selected{/if}>--Access Type--</option>
                                <option value="PUBLIC" {if PageContext::$response->getData['community_type'] eq 'PUBLIC'}selected{/if}>PUBLIC</option>
                                <option value="PRIVATE" {if PageContext::$response->getData['community_type'] eq 'PRIVATE'}selected{/if}>PRIVATE</option></select>
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    
                            <span id="spn_asterisk_ctypeedit" style="color:red">*</span>
                            </div>
                            </div>
                            <label generated="true" for="business_type" class="error"></label>
                        </div>
                             
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="file" class="left" placeholder="Upload File" size="10" id="community_image_id" onchange="upload(1140,258,'community_image_id')"  value="" name="community_image_id">
                            <!-- <img class="masterTooltip" src="{PageContext::$response->baseUrl}modules/cms/images/help_icon.png" title="Community Image(Supported format:jpg,jpeg,gif,png)"> -->
                             <span id="spn_support_imgedit">(Supported format:jpg,jpeg,gif,png)</span>
                             </div>
                                
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cimageadd" style="color:red;">*</span>
                                </div>
                               
                            </div>
                           <span><img src="{PageContext::$response->userImagePath}{PageContext::$response->img}"></span>
                            <label generated="true" id="label_image_id" for="community_image_id" class="error"></label>
                        </div>
                        
<!--                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    Upload Logo
                            <input type="file" class="left" placeholder="Upload File" size="10" value="" name="community_logo_id">
                             <img class="masterTooltip" src="{PageContext::$response->baseUrl}modules/cms/images/help_icon.png" title="Community Image(Supported format:jpg,jpeg,gif,png)"> 
                             <span id="spn_support_logoedit">(Supported format:jpg,jpeg,gif,png)</span>
                             </div>
                                
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_clogoadd" style="color:red;">*</span>
                                </div>
                               
                            </div>
                           
                        </div>    -->
	                <div class="form-group relative">
	                    <input type="submit" class="btn btn-primary yellow_btn2" value="Save" name="btnSubmit" ondblclick="this.disabled=true;">
                            <input type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary yellow_btn2" value="Cancel" name="btnCancel">
	                </div>
	                	<div class="clearfix"></div>
	                </form>
	            </div>
	    </div>
	        </div>
    </div>
</div>

<!--
<div class="content_left">
    <div class="content">
        <div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
        <h3>Edit BizCom</h3>
        <div class="register_form">
            {PageContext::$response->objForm}
        </div>
    </div>
    <div class="clear"></div>
</div>
-->