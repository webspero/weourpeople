<div class="container">
    <section class="whitebox marg20col">
    <div class="row"> 
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h3>Manage an existing Group <span class="round-search">{PageContext::$response->totalrecords}</span> {if PageContext::$response->sess_user_id neq 0} <span class="addbusiness"><a href="{php} echo PageContext::$response->baseUrl;{/php}add-group">Add Group</a></span> {/if}</h3>
        </div>
        <div class="searchbar">

            <div class="col-sm-12 col-md-12 col-lg-12">

                <div class="businesslist_search_blk">
                    <div class="row"> 
                        <div class="col-sm-6 col-md-8 col-lg-8">
                            <input type="hidden" value="{PageContext::$response->sess_user_id}" class="login_status" name="" id="login_status">
                            Sort by : A-Z <a href="{php} echo PageContext::$response->baseUrl; {/php}my-groups?sort=community_name&sortby={php} echo PageContext::$response->order; {/php}&searchtext={php} echo PageContext::$response->searchtext; {/php}&searchtype={php} echo PageContext::$response->searchtype; {/php}&page={php} echo PageContext::$response->page; {/php}">{PageContext::$response->community_name_sortorder}</a>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="right">{php}PageContext::renderRegisteredPostActions('searchbox');{/php}</div>
                        </div>

                    </div>
                   
                </div>
                {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="grplisting">
            {if PageContext::$response->totalrecords > 0}
            {assign var="i" value=PageContext::$response->slno}
            {foreach from=PageContext::$response->leads key=id item=lead}
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="mediapost">
                    <div class="picpost_left pull-left">
                        <span class="picpost_left_pic">

                            <a href="{php} echo PageContext::$response->baseUrl;{/php}group/{$lead->community_alias}"><img class="business_profile_desc_colside_pic" alt="{$lead->file_orig_name}" src="{php} echo PageContext::$response->userImagePath;{/php}{if $lead->file_path neq ''}{$lead->file_path}{elseif $lead->file_path eq ''}default/no_image_bbf.jpg{/if}"> </a>
                        </span>
                    </div>

                    <div class="media-body">
                        <h4 class="media-heading"><span><a href="{php} echo PageContext::$response->baseUrl;{/php}group/{$lead->community_alias}">{$lead->community_name}{if PageContext::$response->org_grp eq 1}({$lead->business_name}){/if}</a></span></h4>
                        {if $lead->community_status =='I'}   
                        <span class="status clserror" >Pending Approval</span> 
                        <div class="right">
                                        {if PageContext::$response->sess_user_id eq $lead->community_created_by}    
                                        <a href="{php} echo PageContext::$response->baseUrl;{/php}edit-group/{$lead->community_id}" class="edititem left marg5right"><i class="fa fa-pencil"></i></a>
                                        <a href="{php} echo PageContext::$response->baseUrl;{/php}index/delete_community/{$lead->community_id}"  onclick="return deleteConfirm();" class="deleteitem left"><i class="fa fa-trash"></i></a> 
                                        {/if}
                                    </div>
                        {else}
                        <div class="display_table">
                            <div class="display_table_cell wid60per">
                                <input type="hidden" id="created_by" value="{php} echo $lead->community_created_by;{/php}">
                                <div id="starring">
                                    <div class="rateit" id="rateit_{$lead->ratetype_id}" data-rateit-value="{$lead->ratevalue}" data-rateit-ispreset="true" data-rateit-id="{$lead->rateuser_id}" data-rateit-type="{$lead->ratetype}" data-rateit-typeid="{$lead->ratetype_id}" data-rateit-flag="{$lead->rateflag}" data-rateit-step=1 data-rateit-resetable="false">
                                    </div>
                                    <div class="right">
                                        {if PageContext::$response->sess_user_id eq $lead->community_created_by}    
                                        <a href="{php} echo PageContext::$response->baseUrl;{/php}edit-group/{$lead->community_id}" class="edititem left marg5right"><i class="fa fa-pencil"></i></a>
                                        <a href="{php} echo PageContext::$response->baseUrl;{/php}index/delete_community/{$lead->community_id}"  onclick="return deleteConfirm();" class="deleteitem left"><i class="fa fa-trash"></i></a> 
                                        {else}
                                         <a href="#" style="color: #0000FF;" onclick="DisjoinCommunity({$lead->community_id},'decline')">
                        <input type="button" name="invite" value="Unsubscribe" class="btn yellow_btn fontupper btt_full_width" title="invite-block">
                    </a>
                                        {/if}
                                    </div>
                                </div>
                            </div>

                        </div>
                        {/if}
                        <p class="paragraph-height">
                            {$lead->community_description|substr:0:200}....
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            {assign var="i" value=$i+1}
            {/foreach} 	
            {else}
            <br>
            <div class="col-sm-12 col-md-12 col-lg-12">{if PageContext::$response->searchtype eq ''}No groups added/joined yet.{else}No matches found.{/if}.</div>
            {/if} 
            <div class="clear"></div>
            {if PageContext::$response->totalrecords gt PageContext::$response->itemperpage}
            <div class="col-sm-12 col-md-12 col-lg-12">{PageContext::$response->pagination} </div>
            {/if}

        </div>
    </div>
    </section>
    <input type="hidden" id="searchtext" name="searchtext" value="{PageContext::$response->searchtext}">
    <input type="hidden" id="searchtype" name="searchtype" value="{PageContext::$response->searchtype}">
    <!--<input type="hidden" id="page" name="page" value="{PageContext::$response->page}">-->
    <input type="hidden" id="orderfield" name="orderfield" value="{PageContext::$response->orderfield}">
    <input type="hidden" id="orderby" name="orderby" value="{PageContext::$response->orderby}">

</div>

<script type ="text/javascript">
    $(document).ready(function() {
   // alert("jdfgj");
        var page = 1;  
        var searchtext = $("#searchtext").val();
        var searchtype = $("#searchtype").val();
        var orderfield = $("#orderfield").val();
        var orderby = $("#orderby").val();
       	var _throttleTimer = null;
	var _throttleDelay = 100;
            $(window ).scroll(function() {
              console.log('TotalPage'+TOT_PAGE+'Page'+page);
	    clearTimeout(_throttleTimer);
	    _throttleTimer = setTimeout(function () {
	    if($(window).scrollTop() + $(window).height() > $(document).height()-100) {
	        if(page <= TOT_PAGE){
                   // alert(page);
                    page++;
                    $.blockUI({ message: '' }); 
                     $.ajax({
                        type    : 'Post',
                        url     : mainUrl+'user/my_groupsajax/'+page+'/'+orderfield+'/'+orderby+'/'+searchtext+'/'+searchtype,
                        cache   : false,
                        success :function(data){
                            $('.grplisting').append(data);
                            $('.listitem').fadeIn(3000);
                            $('.listitem').hide();
                        }
                    }) 
    	        }
	    }
	    },_throttleDelay);
	    });
    });

</script>









