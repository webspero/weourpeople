<script>

    $(document).ready(function () {
        magnificPopupSingleFnNewsfeed();

        var page = 1;
        var _throttleTimer = null;
        var _throttleDelay = 100;
        $(window).scroll(function () {

            clearTimeout(_throttleTimer);
            _throttleTimer = setTimeout(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                    if (page < TOTAL_FEED_PAGES) {
                        page++;
                        $.blockUI({ message : '' });
                        var user_alias = $("#user_alias").val();
                        $.ajax({
                            type: 'Post',
                            url: mainUrl + 'user/newsfeedajax/' + page,
                            data: 'alias=' + user_alias,
                            success: function (data) {

                                var html = $.parseHTML(data);
                                enableEmojiOn(html);
                                $('.jfeeddisplay_div').append(html);

                                // parse facebook share button
                                FB.XFBML.parse();

                                magnificPopupSingleFnNewsfeed();
                                $.unblockUI();
                            }
                        })
                    }
                }
            }, _throttleDelay);
        });
       
    });

</script>
<div class="container" id="div_container">
<div class="marg20col"> 
<div class="row">
    {if PageContext::$response->sess_user_name neq ''}
    <div id='msg'></div>
    <input type="hidden" value="{PageContext::$response->alias}" id="user_alias">
{if PageContext::$response->friend_display eq '1' || PageContext::$response->gallery_display eq '1'}
{if count(PageContext::$response->friend_display_list) gt 0 || count(PageContext::$response->gallery_display_list) gt 0} 
  {assign var=first_col value='col-sm-12 col-md-3 col-lg-3'} 
  {assign var=second_col value='col-sm-8 col-md-5 col-lg-6'} 
  {assign var=third_col value='col-sm-4 col-md-4 col-lg-3'} 
{else}
  {assign var=first_col value='col-sm-4 col-md-4 col-lg-3'} 
  {assign var=second_col value='col-sm-8 col-md-8 col-lg-9'} 
  {assign var=third_col value='col-sm-12 col-md-12 col-lg-12'}
{/if}
{else}
  {assign var=first_col value='col-sm-4 col-md-4 col-lg-3'} 
  {assign var=second_col value='col-sm-8 col-md-8 col-lg-9'} 
  {assign var=third_col value='col-sm-12 col-md-12 col-lg-12'}
{/if}
<div class="{$first_col}">
<div id="sticky-anchor"></div>
<div id="sticky" class="profilesec_left">
    <div class="display_table profilesec_left_top">
    <div class="display_table_cell wid20per">
        <div id="profilepic" class="profile_leftpicthumb">
            <a href="{PageContext::$response->baseUrl}timeline/{PageContext::$response->sess_user_alias}"    alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">
                <img  id="profile_image" class="profile_sm_pic" src="{PageContext::$response->userImagePath}{PageContext::$response->image}">
            </a>
        </div>
    </div>
<!--        {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
        <a href="#" class="changepic" onclick="" ><label for="file_profile" ><i class="fa fa-camera"></i></label>
        </a>
        <input type="file" id="file_profile" class="jprofile_image" style="cursor: pointer;  display: none"/>
        <input type="hidden" value="{PageContext::$response->sess_user_id}" id="user_id" name="user_id">        
        {/if}-->
   
    <div class="profilenamesec" >
        <h3 class="profile_name"><a href="{PageContext::$response->baseUrl}timeline/{PageContext::$response->sess_user_alias}"  alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">
                    {PageContext::$response->username}</a>
        </h3>
        {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
            <h5>Last logged in : <span>{time_elapsed_string(PageContext::$response->sess_user_last_login)}</span></h5>
        {/if}
    </div>
<!--    <div class="changepicsec" >
        {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
        <a href="#" class="changepic" onclick="" ><label for="file_profile" ><i class="fa fa-pencil"></i></label>
        </a>
        <input type="file" id="file_profile" class="jprofile_image" style="cursor: pointer;  display: none"/>
        <input type="hidden" value="{PageContext::$response->sess_user_id}" id="user_id" name="user_id">        
        {/if}
    </div>-->
    </div>
    <div class="profilesec_left_bottom">
        <h4>LINKS</h4>
        <ul>
            {if PageContext::$response->sess_user_id neq ''}
            <li><a href="{PageContext::$response->baseUrl}timeline/{PageContext::$response->sess_user_alias}"><i class="fa fa-newspaper-o" aria-hidden="true"></i>  Timeline</a></li>
            <li><a href="{PageContext::$response->baseUrl}inbox"><i class="fa fa-envelope-o" aria-hidden="true"></i>  Messages</a></li>
            {/if}
            {if PageContext::$response->sess_user_id eq ''}
            <li><a><i class="fa fa-users" aria-hidden="true"></i> Friends({PageContext::$response->userDetails['user_friends_count']})</a></li>
            {/if}
            <li><a {if PageContext::$response->sess_user_id neq ''} href="{PageContext::$response->baseUrl}pages"{/if}><i class="fa fa-university" aria-hidden="true"></i>  Pages{if PageContext::$response->sess_user_id eq ''}({PageContext::$response->userDetails['user_business_count']}){/if}</a></li>
            <li><a {if PageContext::$response->sess_user_id neq ''} href="{PageContext::$response->baseUrl}my-groups"{/if}><i class="fa fa-users" aria-hidden="true"></i>  Groups{if PageContext::$response->sess_user_id eq ''}({PageContext::$response->userDetails['user_community_count']}){/if}</a></li>
            {if PageContext::$response->sess_user_id neq ''}
            <!--<li><a href="{PageContext::$response->baseUrl}my-subscribed-group"><i class="fa fa-user-plus" aria-hidden="true"></i>  Subscribed Groups</a></li>-->
            {if PageContext::$response->organization_display eq '1'}
            <h4>
                PAGES 
                <div class="more_add pull-right">
                {if count(PageContext::$response->organization_display_list) gt 0}
                <a href="{PageContext::$response->baseUrl}pages">More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                {else}
                <a href="{PageContext::$response->baseUrl}add-page">Add Page <i class="fa fa-plus" aria-hidden="true"></i></a>
                {/if} 
                </div>  
            </h4>
           
                {foreach from=PageContext::$response->organization_display_list  key=id item=comments}
                <li>
                    <a href="{PageContext::$response->baseUrl}page/{$comments->business_alias}"><i class="fa fa-university" aria-hidden="true"></i>{$comments->business_name}</a>
                </li>
                {/foreach}
                 
            {/if}
            {if PageContext::$response->group_display eq '1'}
            <h4>
                GROUPS
                <div class="more_add pull-right">
                    {if count(PageContext::$response->group_display_list) gt 0}
                    <a href="{PageContext::$response->baseUrl}my-groups">More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    {else}
                    <a href="{PageContext::$response->baseUrl}add-group">Add Group <i class="fa fa-plus" aria-hidden="true"></i></a>
                    {/if}
                </div>
            </h4>
            
           
                {foreach from=PageContext::$response->group_display_list  key=id item=comments}
                <li>
                    <a href="{PageContext::$response->baseUrl}group/{$comments->community_alias}"><i class="fa fa-users" aria-hidden="true"></i>{$comments->community_name}</a>
                </li>
                {/foreach}
            {/if}
<!--            {if PageContext::$response->friend_display eq '1'}
            {if count(PageContext::$response->friend_display_list) gt 0}
            <h4>
                FRIENDS
                <div class="more_add pull-right">
                    <a id="jShowFriends" href="{PageContext::$response->baseUrl}timeline/{PageContext::$response->sess_user_alias}">More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </h4>
           
                {foreach from=PageContext::$response->friend_display_list  key=id item=comments}
                <li>
                    <a href="{PageContext::$response->baseUrl}timeline/{$comments->user_alias}"><i class="fa fa-user" aria-hidden="true"></i>{$comments->user_firstname} {$comments->user_lastname}</a>
                </li>
                {/foreach}
            {/if}   
            {/if}-->

            {/if}
        </ul>

    </div>
</div>

<div class="clear"></div>
</div>
        

<div class="{$second_col}">    
    <form role="form" id="logoform" method="post" action="" enctype="multipart/form-data">
    <div class="whitebox">
    <div class="emotionsbtn_textblk">
        <textarea class="jEmojiTextarea" placeholder="Post your message.." name="newsfeed_comment" id="newsfeed_comment"></textarea>
        </div>
        <div class="wid100 pad5top">
             <div class="display_image_div">
                <div id="juploadimagepreview"></div>
             </div>
            <div class="pull-left">
                <a href="#" class="camicon"><label for="file_feed" ><i class="fa fa-camera" aria-hidden="true"></i></label></a>
                <input type="file" id="file_feed" class="" onchange="previewFile()" style="cursor: pointer;  display: none"/>

            </div>
            <div class="pull-right">
                 <input type="hidden" id="image_id" value="">
                <input type="button" onclick="postNewsFeed()" value="Post" ondblclick="this.disabled=true;" class="btn primary post_btn">
                <div class="loader" id="post_feed_loader" style="display: none;">
                    <img src="{PageContext::$response->userImagePath}default/loader.gif" />
                </div>
            </div>
            <div class="profile-bnr">
            <img style="display: none;" class="img img-responsive center" src="{if PageContext::$response->campaignDetails->voucher_image_name neq ''}{PageContext::$response->userImagePath}{PageContext::$response->campaignDetails->voucher_image_name}{/if}" height="50">
            </div>
            <div class="clearfix"></div>
            <div id="jerrordiv" style="color:#f00"></div>
        </div>
    </div>
        </form>
    <div class="jfeeddisplay_div"> 
     {foreach from=PageContext::$response->news_feed key=id item=feed}
                                {assign var=flag value={$flag}+1}
                     
     {if $feed['community_announcement_id']}  
    <div class="whitebox timelinebox" id="group_feed_{$feed['community_announcement_id']}">
        <div class="wid100per">
            <div class="postheadtablediv">
            <div class="post_left">
                <span class="mediapost_pic">
                   <a href="{PageContext::$response->baseUrl}timeline/{$feed['user_alias']}">
                    {if $feed['file_path'] neq ''}
                       <img src="{PageContext::$response->userImagePath}{$feed['file_path']}">
                        {else}
                         <img src="{PageContext::$response->userImagePath}medium/member_noimg.jpg">
                        {/if}
                        </a>
                </span>
            </div>
            <div class="post_right">
                <div class="posthead">
                    <h4 class="media-heading">
                        <a href="{PageContext::$response->baseUrl}timeline/{$feed['user_alias']}">{$feed['user_firstname']} {$feed['user_lastname']}</a> 
                        <a href="{PageContext::$response->baseUrl}group/{$feed['community_alias']}"><i class="fa fa-play" aria-hidden="true"></i> {$feed['community_name']}</a>
                    </h4>
                </div>
                <div class="postsubhead">
                    <div class="postsubhead_right">
                        <span> 
                            {if $feed['days'] gt 0}
                                {$feed['days']} days ago
                            {else}
                                {assign var="timesplit" value=":"|explode:$feed['output_time']}
                                {if $timesplit[0]>0}
                                    {$timesplit[0]|ltrim:'0'} hrs ago
                                {else}
                                    {if $timesplit[1]>0}
                                        {$timesplit[1]|ltrim:'0'}
                                            {if $timesplit[1] eq 1} min
                                            {else} mins
                                            {/if} ago
                                    {else}
                                        Just Now
                                    {/if}
                                {/if}
                            {/if}
                        </span>
                    </div>
                </div>
            </div>
            </div>
            <div class="clearfix"></div>
            <div class="postsubhead_left">
                {assign var="reg_exUrl" value= '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/'}
                {if preg_match({$reg_exUrl}, {$feed['community_announcement_content']},$url)}
                  
                <div class="jEmojiable">{preg_replace({$reg_exUrl}, "<a href='{$url[0]}'>{$url[0]}</a>" , {$feed['community_announcement_content']})}</div>

                {else}

                <div class="jEmojiable">{$feed['community_announcement_content']}</div>

                {/if}
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>
        {if $feed['community_announcement_image_path']}
            <div class="postpic">
<!--                <img src="{PageContext::$response->userImagePath}{$feed['community_announcement_image_path']}" class="fancybox">-->
                <ul  class="portfolio" class="clearfix">
                        <li><a href="{PageContext::$response->userImagePath}{$feed['community_announcement_image_path']}" title=""><img src="{PageContext::$response->userImagePath}{$feed['community_announcement_image_path']}" alt=""></a></li>
                </ul>
            </div>
        {/if}
        <div class="clearfix"></div>

        <div class="mediapost_user-side">
            <div class="row">
            <div class="col-sm-4 col-md-4 col-lg-5">
                <div class="likesec">
                <div class="display_table_cell pad10_right">
                <a href="#" class=" colorgrey_text jLikeFeedcommunity {if $feed->LIKE_ID gt 0}  liked {/if}" id="jlike_{$feed['community_announcement_id']}" 
                   aid="{$feed['community_announcement_id']}" cid="{$feed['community_id']}">
                    <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                    <span id="jlikedisplay_{$feed['community_announcement_id']}">Like </span></a>
                </div>
                 <div class="display_table_cell pad10_right">
                    <i class="fa fa-comments-o icn-fnt-size"></i>
                <a href="#" id="jCommentButton_{$feed['community_announcement_id']}" aid="{$feed['community_announcement_id']}" class="jCommentButton colorgrey_text">Comment</a>
                </div>
                {if $feed['community_announcement_user_id'] == PageContext::$response->sess_user_id}
                    <div class="display_table_cell pad10_right">
                    <i class="fa fa-trash-o icn-fnt-size"></i>
                    <a href="#" id="jDeleteButton_{$feed['community_announcement_id']}" aid="{$feed['community_announcement_id']}" class="jGroupDeletetButton colorgrey_text">Delete</a>
                    </div>
               {/if}
                </div>
            </div>
            <div class="col-sm-8 col-md-8 col-lg-7">
                <div class="sharefnt">
                                                                     <div id="group_like_users_{$feed['community_announcement_id']}" class="like_users_div" style=" display: none;">
                      {foreach from=PageContext::$response->newsFeedLikeUsers[$feed['community_announcement_id']] key = id item=val } 
                      <span>{$val->user_name}<br></span>
                      {/foreach}  
                    </div>
                <span class="mediapost_links jShowGroupLikeUsers" id="jcountlike_{$feed['community_announcement_id']}">{if $feed['community_announcement_num_likes'] > 0 }{$feed['community_announcement_num_likes']} Likes {/if}</span>
                <span class="mediapost_links jCommentButton count_class"  aid ="{$feed['community_announcement_id']}" id="jcountcomment_{$feed['community_announcement_id']}">{if $feed['community_announcement_num_comments'] gt 0} {$feed['community_announcement_num_comments']}
                    Comments 
                {/if}
                </span>
                <div class="fb-share-button" id="share_button_{$feed['community_announcement_id']}" data-href="{PageContext::$response->baseUrl}announcement-detail/{$feed['community_announcement_alias']}" data-title ="Shared on facebook" data-layout="button_count" data-desc="You can share data" data-url="{PageContext::$response->baseUrl}newsfeed-detail/{$feed['community_announcement_alias']}"></div>
                <a href="{PageContext::$response->baseUrl}announcement-detail/{$feed['community_announcement_alias']}" title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                
                </div>
            </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <!---------------Referral offer-------------------------------->
        <!--                                                <div class="jReferralOptionsClass referraltype_blk" id="jDivdisplayReferralOptions_{$announcement->community_id}_{$announcement->community_announcement_id}" style="display:none;">
            <h4>Please choose the preferred referral type</h4>
            <ul>
                <li><input type="radio" name="RefferalType_{$announcement->community_id}" value="1" > Free Referral</li>
                <li><input type="radio" name="RefferalType_{$announcement->community_id}" value="2" > Financial Incentive Based Referral</li>
                <li><input type="radio" name="RefferalType_{$announcement->community_id}"  value="3"> Charitable Donation Based Referral</li>
                <li><input type="radio" name="RefferalType_{$announcement->community_id}" value="4" > Voucher or Product Based Referral</li>
                <li><input type="button" class="jClickFeedReferralOffer sendref_offr_btn" aid="{$announcement->community_announcement_id}"  bid="{$announcement->community_id}" uid="{$announcement->community_created_by}" value="Send Referral Offer"></li>
            </ul>
        </div>-->
        <div class="mediapost_user-side-top clear jCommentDisplayDiv" id="jCommentBoxDisplayDiv_{$feed['community_announcement_id']}" style="display:none">
            <p class="lead emoji-picker-container position_relatv">
                <div class="emotionsbtn_textblk">
                    <textarea class="form-control textarea-control jEmojiTextarea" id="watermark_{$feed['community_announcement_id']}" rows="3" placeholder="Write your comment here..."></textarea>
                    <a class="upload_cambtn" id="{$feed['community_announcement_id']}>">
                    <label for="file_{$feed['community_announcement_id']}" ><i class="fa fa-camera feed-camera"></i></label></a>
                    <input type="file" id="file_{$feed['community_announcement_id']}" aid="{$feed['community_announcement_id']}" class="jimage_file" style="cursor: pointer;  display: none"/>
                    <input type="hidden" id="announcement_id" value="{$feed['community_announcement_id']}">
                </div>
                
            </p>
             <div class="msg_display" id="msg_display{$feed['community_announcement_id']}"></div>
										 	<div class="clearfix"></div>
            <a id="postButton" ondblclick="this.disabled=true;" cid="{$feed['community_id']}" cmid="{$feed['announcement_comment_id']}" aid="{$feed['community_announcement_id']}"  class="btn yellow_btn fontupper pull-right jPostCommentButtonCommunity"> Post</a>
        </div>
        <div class="loading" id="loading_{$feedlist['community_announcement_id']}" style="display: none;">
            <img src="{PageContext::$response->userImagePath}default/loader.gif" />
        </div>
        <div id="juploadimagepreview_{$feed['community_announcement_id']}"></div>
        <div class="clear commentblk jCommentDisplayDiv" id="jCommentDisplayDiv_{$feed['community_announcement_id']}" style="display:none">
            <div id="posting_{$feed['community_announcement_id']}">
                <!------------------------Comments-------------------------------->
                {assign var ="val" value=0}                                           
                {foreach PageContext::$response->newsFeed[$feed['community_announcement_id']] as $key=>$comments}
                {if $comments->announcement_comment_id neq ''}
                <input type="hidden" id="val_{$feed['community_announcement_id']}" value="{count(PageContext::$response->newsFeed[{$feed['community_announcement_id']}])}">
                   
                    {assign var ="val" value={$val}+1}
                      <input type="text" id="hid_announcement_id_{$comments->announcement_comment_id}" class="hid_announcement_id" value="{$feed['community_announcement_id']}" style="display: none;" >   
                        <input type="text" id="hid_{$comments->announcement_comment_id}" value="{$val}" style="display: none;">
                        <div class="col-md-12 btm-mrg pad10p" id="jdivComment_{$comments->announcement_comment_id}">
                            <div class="row">
                                <div class="col-md-1">
                                    <span class="mediapost_pic">
                                        <a href="{PageContext::$response->baseUrl}timeline/{$comments->user_alias}">
                                        {if $comments->user_image neq ''}
                                        <img class="ryt-mrg" src="{PageContext::$response->userImagePath}{$comments->user_image}">
                                        {else}
                                        <img class="ryt-mrg" src="{PageContext::$response->userImagePath}medium/member_noimg.jpg">
                                        {/if}
                                        </a>
                                    </span>
                                </div>
                                <div class="col-md-11">
                                    <span class="name"><a href="{PageContext::$response->baseUrl}timeline/{$comments->user_alias}">{$comments->Username} </a></span>
                                    <span class="jEmojiable">{$comments->comment_content}</span>
                                     <div class="clearfix"></div>
                                        <span class="new-text">

                                        {$comments->commentDate}
                                    </span>
                                    <div class="commentimg_box">
                                        {if $comments->file_path neq ''}
<!--                                        <img class="fancybox img-responsive center" src="{PageContext::$response->userImagePath}medium/{$comments->file_path}">-->
                                        <ul class="portfolio" class="clearfix">
                                                <li><a href="{PageContext::$response->userImagePath}{$comments->file_path}" title=""><img src="{PageContext::$response->userImagePath}{$comments->file_path}" alt=""></a></li>
                                        </ul>
                                        
                                        
                                        
                                        {/if}
                                    </div>
                                    <div class="mediapost_user-side1">
                                        <span rel="tooltip" title="{$comments->users} liked this comment" class="mediapost_links" id="jcountcommentlike_{$comments->announcement_comment_id}">{if $comments->num_comment_likes gt 0}{$comments->num_comment_likes} Likes {/if}</span>
                                        <input type="hidden" id="reply_{$comments->announcement_comment_id}" value="{if $comments->num_replies gt 0}{$comments->num_replies} {else} 0 {/if}">
                                        <span class="mediapost_links jShowReply" cid="{$comments->announcement_comment_id}" id="{$comments->announcement_comment_id}"><a href="#" id="{$comments->announcement_comment_id}"><span id="jcountreply_{$comments->announcement_comment_id}">{if $comments->num_replies gt 0}{$comments->num_replies} Replies {/if}</span></a></span>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="comment_sharelike_sec">
                                      
                                        {if $comments->user_id eq PageContext::$response->sess_user_id || $comments->community_created_by eq PageContext::$response->sess_user_id}
                                        <a href="#" class="jFeedCommentDelete marg10right" aid="{$feed['community_announcement_id']}" id="{$comments->announcement_comment_id}"><i class="fa fa-times"></i> Delete</a>
                                        {/if}
                                        <a href="#" class="jFeedCommentReply marg10right" aid="{$feed['community_announcement_id']}" cid="{$comments->announcement_comment_id}" id="{$comments->announcement_comment_id}"><i class="fa fa-reply"></i> Reply</a>
                                        <a href="#" class="jFeedCommentLike {if $comments->LIKE_ID gt 0} liked {/if}" id="jlikeComment_{$comments->announcement_comment_id}" cid="{$comments->announcement_comment_id}" aid="{$feed['community_announcement_id']}" >

                                            <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                            <span id="jlikedisplaycomment_{$comments->announcement_comment_id}">Like </span></a>
                                    </div>
                                    </span>
                                    <div id="jdivReply_{$comments->announcement_comment_id}" class="jDisplayReply">
                                        <p class="lead emoji-picker-container position_relatv">
                                            <div class="emotionsbtn_textblk">
                                                <textarea placeholder="Write your reply here..."
                                                          class="form-control textarea-control jEmojiTextarea"
                                                          style="height:35px;"
                                                          id="watermark_{$feed['community_announcement_id']}_{$comments->announcement_comment_id}"
                                                          class="watermark" name="watermark"></textarea>
                                                <a class="upload_cambtn" id="{$feed['community_announcement_id']}">
                                                    <label for="imagereply_file_{$comments->announcement_comment_id}"><i
                                                            class="fa fa-camera feed-reply-camera"></i></label>
                                                    <div class="file_button"><input type="file"
                                                                                    id="imagereply_file_{$comments->announcement_comment_id}"
                                                                                    cid="{$comments->announcement_comment_id}"
                                                                                    class="jcommunityimagereply_file"
                                                                                    style="display:none;"/></div>
                                                </a>
                                            </div>
                                        </p>
                                        <div class="msg_display" id="msg_display{$comments->announcement_comment_id}"></div>
										 	<div class="clearfix"></div>
                                            <a ondblclick="this.disabled=true;" id="shareButton" cid="{$feed['community_id']}" cmid="{$comments->announcement_comment_id}" aid="{$feed['community_announcement_id']}"  class="btn yellow_btn fontupper pull-right jPostCommentButtonCommunity"> Post</a>
                                    </div>
                                    <div class="loader" id="imagereply_loader_{$comments->announcement_comment_id}">
                                        <img src="{PageContext::$response->userImagePath}default/loader.gif" />
                                    </div>
                                    <div id="jDivReplyImagePreview_{$comments->announcement_comment_id}"></div>
                                    <div id="postingReply_{$comments->announcement_comment_id}"></div>
                                </div>
                            </div> 
                        </div>
{/if}
{/foreach}
{if !empty(PageContext::$response->Comments[{$announcement->community_announcement_id}])}
          {if PageContext::$response->Page[{$announcement->community_announcement_id}]['totpages'] neq PageContext::$response->Page[{$announcement->community_announcement_id}]['currentpage']}
                <div id="jDisplayMoreComments_{$announcement->community_announcement_id}" ><a href="#" id="jMorecommentsButton" pid="{PageContext::$response->Page[{$announcement->community_announcement_id}]['currentpage']}" aid="{$announcement->community_announcement_id}">Load more comments</a></div>
            {/if}

{/if}


               
                <!------------------------EOF Comments-------------------------------->
            </div>
        </div>
        </div>                            
                                
             {/if}                   
                                
                                
             {if $feed['organization_announcement_id']}                              
    <div class="whitebox" id="org_feed_{$feed['organization_announcement_id']}">
            <div class="wid100per">
                <div class="postheadtablediv">
                <div class="post_left">
                    <span class="mediapost_pic">
                       <a href="{PageContext::$response->baseUrl}timeline/{$feed['user_alias']}" >
                        {if $feed['file_path'] neq ''}
                        <img src="{PageContext::$response->userImagePath}{$feed['file_path']}">
                        {else}
                         <img src="{PageContext::$response->userImagePath}medium/member_noimg.jpg">
                        {/if}
                        </a>
                    </span>
                </div>
                <div class="post_right">
                    <div class="posthead"><h4 class="media-heading"><a href="{PageContext::$response->baseUrl}timeline/{$feed['user_alias']}" >{$feed['user_firstname']} {$feed['user_lastname']}</a>
                         <a href="{PageContext::$response->baseUrl}page/{$feed['business_alias']}"><i class="fa fa-play" aria-hidden="true"></i> {$feed['business_name']} </a></h4></div>
                    <div class="postsubhead">
                        
                        <div class="postsubhead_right">
                            <span> 
                                {if $feed['days'] gt 0}
                                    {$feed['days']} days ago
                                {else}
                                    {assign var="timesplit" value=":"|explode:$feed['output_time']}
                                    {if $timesplit[0]>0}
                                        {$timesplit[0]|ltrim:'0'} hrs ago
                                    {else}
                                        {if $timesplit[1]>0}
                                            {$timesplit[1]|ltrim:'0'}
                                                {if $timesplit[1] eq 1} min
                                                {else} mins
                                                {/if} ago
                                        {else}
                                            Just Now
                                        {/if}
                                    {/if}
                                {/if}
                            </span>
                        </div>
                    </div>
                </div>
                </div>
                <div class="clearfix"></div>
                <div class="postsubhead_left">
                    {assign var="reg_exUrl" value= '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/'}
                {if preg_match({$reg_exUrl}, {$feed['organization_announcement_content']},$url)}
                  
                <div class="jEmojiable">{preg_replace({$reg_exUrl}, "<a href='{$url[0]}'>{$url[0]}</a>" , {$feed['organization_announcement_content']})}</div>

                {else}

                <div class="jEmojiable">{$feed['organization_announcement_content']}</div>

                {/if}
                </div>
                <div class="clearfix"></div>
            </div>


           {if $feed['organization_announcement_image_path']}
                                                            <div class="postpic">

<!--                                                                <img src="{PageContext::$response->userImagePath}{$feed['organization_announcement_image_path']}" class="fancybox">-->
                                                                <ul class="portfolio" class="clearfix">
                                                                        <li><a href="{PageContext::$response->userImagePath}{$feed['organization_announcement_image_path']}" title=""><img src="{PageContext::$response->userImagePath}{$feed['organization_announcement_image_path']}" alt=""></a></li>
                                                                </ul>
                                                            
                                                            </div>
            {/if}
            <div class="clearfix"></div>

            <div class="mediapost_user-side">
                <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-5">
                    <div class="likesec">
                    <div class="display_table_cell pad10_right">
                    <a href="#" class=" colorgrey_text jLikeFeedOrganization {if $feed['LIKE_ID'] gt 0} liked {/if}" id="jlike_{$feed['organization_announcement_id']}" 
                       aid="{$feed['organization_announcement_id']}" cid="{$feed['organization_id']}">
                        <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                        <span id="jlikedisplay_{$feed['organization_announcement_id']}">Like </span></a>
                    </div>
                     <div class="display_table_cell pad10_right">
                        <i class="fa fa-comments-o icn-fnt-size"></i>
                    <a href="#" id="jCommentButton_{$feed['organization_announcement_id']}" aid="{$feed['organization_announcement_id']}" class="jOrgCommentButton colorgrey_text">Comment</a>
                    </div> 
                        {if $feed['organization_announcement_user_id'] == PageContext::$response->sess_user_id} 
                    <div class="display_table_cell pad10_right">
                        <i class="fa fa-trash-o icn-fnt-size"></i>
                    <a href="#" id="jDeleteButton_{$feed['organization_announcement_id']}" aid="{$feed['organization_announcement_id']}" class="jOrgDeletetButton colorgrey_text">Delete</a>
                    </div>
                       {/if}
                    </div>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-7">
                    <div class="sharefnt">
                                                                       <div id="org_like_users_{$feed['organization_announcement_id']}" class="like_users_div" style=" display: none;">
                      {foreach from=PageContext::$response->newsFeedLikeUsers[$feed['organization_announcement_id']] key = id item=val } 
                      <span>{$val->user_name}<br></span>
                      {/foreach}  
                    </div>
                    <span class="mediapost_links jShowOrgLikeUsers" id="jcountlike_{$feed['organization_announcement_id']}">{if $feed['organization_announcement_num_likes'] gt 0} {$feed['organization_announcement_num_likes']} Likes {/if}</span>
                    <span class="mediapost_links jOrgCommentButton count_class"  aid ="{$feed['organization_announcement_id']}" id="jcountcomment_{$feed['organization_announcement_id']}">{if $feed['organization_announcement_num_comments'] gt 0} {$feed['organization_announcement_num_comments']}
                        Comments 
                    {/if}
                    </span>
                    <div class="fb-share-button" id="share_button_{$feed['news_feed_id']}" data-href="{PageContext::$response->baseUrl}announcement-detail/{$feed['organization_announcement_alias']}" data-title ="Shared on facebook" data-layout="button_count" data-desc="You can share data" data-url="{PageContext::$response->baseUrl}newsfeed-detail/{$feed['news_feed_alias']}"></div>
                    <a href="{PageContext::$response->baseUrl}announcement-detail/{$announcement['organization_announcement_alias']}" title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                    
                    </div>
                </div>
                </div>
                <div class="clearfix"></div>
           </div>

            <!---------------Referral offer-------------------------------->
<!--                                                <div class="jReferralOptionsClass referraltype_blk" id="jDivdisplayReferralOptions_{$announcement->community_id}_{$announcement->community_announcement_id}" style="display:none;">
                <h4>Please choose the preferred referral type</h4>
                <ul>
                    <li><input type="radio" name="RefferalType_{$announcement->community_id}" value="1" > Free Referral</li>
                    <li><input type="radio" name="RefferalType_{$announcement->community_id}" value="2" > Financial Incentive Based Referral</li>
                    <li><input type="radio" name="RefferalType_{$announcement->community_id}"  value="3"> Charitable Donation Based Referral</li>
                    <li><input type="radio" name="RefferalType_{$announcement->community_id}" value="4" > Voucher or Product Based Referral</li>
                    <li><input type="button" class="jClickFeedReferralOffer sendref_offr_btn" aid="{$announcement->community_announcement_id}"  bid="{$announcement->community_id}" uid="{$announcement->community_created_by}" value="Send Referral Offer"></li>
                </ul>
            </div>
-->          


            <div class="mediapost_user-side-top clear jCommentDisplayDiv" id="jCommentBoxDisplayDiv_{$feed['organization_announcement_id']}" style="display:none">
                <p class="lead emoji-picker-container position_relatv">
                    <div class="emotionsbtn_textblk">
                        <textarea class="form-control textarea-control jEmojiTextarea" id="watermark_{$feed['organization_announcement_id']}" rows="3" placeholder="Write your comment here..."></textarea>
                        <a class="upload_cambtn" id="{$feed['organization_announcement_id']}">
                        <label for="file_{$feed['organization_announcement_id']}" ><i class="fa fa-camera feed-camera"></i></label></a>
                        <input type="file" id="file_{$feed['organization_announcement_id']}" aid="{$feed['organization_announcement_id']}" class="jorgimage_file" style="cursor: pointer;  display: none"/>
                        <input type="hidden" id="organization_announcement_id" value="{$feed['organization_announcement_id']}">
                    </div>
                </p>
                <div class="msg_display" id="msg_display_{$feed['announcement_comment_id']}"></div>
				<div class="clearfix"></div>
                <a id="postButton" ondblclick="this.disabled=true;" cid="{$feed['organization_id']}" cmid="{$feed['announcement_comment_id']}" aid="{$feed['organization_announcement_id']}"  class="btn yellow_btn fontupper pull-right jPostCommentButtonOrganization"> Post</a>
            </div>
            <div class="loading" id="loading_{$feedlist['community_announcement_id']}" style="display: none;">
                <img src="{PageContext::$response->userImagePath}default/loader.gif" />
            </div>
            <div id="juploadimagepreview_{$feed['organization_announcement_id']}"></div>
            <div class="clear commentblk jCommentDisplayDiv" id="jCommentDisplayDiv_{$feed['organization_announcement_id']}" style="display:none">
                <div id="posting_{$feed['organization_announcement_id']}">
                    <!------------------------Comments-------------------------------->
                    {assign var ="val" value=0}
                                            
                    {foreach PageContext::$response->newsFeed[$feed['organization_announcement_id']] as $key=>$comments}
                    {if $comments->organization_announcement_comment_id neq ''}
                    <input type="hidden" id="val_{$feed['organization_announcement_id']}" value="{count(PageContext::$response->newsFeed[{$feed['organization_announcement_id']}])}">
                       {assign var ="val" value={$val}+1}
                           <input type="text" id="hid_announcement_id_{$comments->organization_announcement_comment_id}" class="hid_announcement_id" value="{$feed['organization_announcement_id']}" style="display: none;" >   
                            <input type="text" id="hid_{$comments->organization_announcement_comment_id}" value="{$val}" style="display: none;">
                            <div class="col-md-12 btm-mrg pad10p" id="jdivComment_{$comments->organization_announcement_comment_id}">
                                <div class="row">
                                    <div class="col-md-1">
                                        <span class="mediapost_pic">
                                            <a href="{PageContext::$response->baseUrl}timeline/{$comments->user_alias}" >
                                            {if $comments->user_image != ''}
                                            <img class="ryt-mrg" src="{PageContext::$response->userImagePath}{$comments->user_image}">
                                            {else}
                                            <img class="ryt-mrg" src="{PageContext::$response->userImagePath}medium/member_noimg.jpg">
                                            {/if}
                                            </a>
                                        </span>
                                    </div>
                                    <div class="col-md-11">
                                        <span class="name jEmojiable" ><a href="{PageContext::$response->baseUrl}timeline/{$comments->user_alias}" >{$comments->Username}</a></span>
                                          {$comments->comment_content}</span>
                                         <div class="clearfix"></div>
                                            <span class="new-text">

                                            {$comments->commentDate}
                                        </span>
                                        <div class="commentimg_box">
                                            {if $comments->file_path != ''}
<!--                                            <img class="fancybox img-responsive center" src="{PageContext::$response->userImagePath}medium/{$comments->file_path}">-->
                                            <ul class="portfolio" class="clearfix">
                                                    <li><a href="{PageContext::$response->userImagePath}medium/{$comments->file_path}" title=""><img src="{PageContext::$response->userImagePath}medium/{$comments->file_path}" alt=""></a></li>
                                            </ul>
                                            
                                            
                                            {/if}
                                        </div>
                                        <div class="mediapost_user-side1">
                                   
                                            <span rel="tooltip" title="{$comments->users} liked this comment" class="mediapost_links" id="jcountcommentlike_{$comments->organization_announcement_comment_id}">{if $comments->num_comment_likes gt 0} {$comments->num_comment_likes} Likes {/if}</span>
                                            <input type="hidden" id="reply_{$comments->organization_announcement_comment_id}" value="{if $comments->num_replies gt 0}{$comments->num_replies}{else} 0 {/if}">
                                            <span class="mediapost_links jOrgShowReply" cid="{$comments->organization_announcement_comment_id}" id="{$comments->organization_announcement_comment_id}">
                                                <a href="#" id="{$comments->organization_announcement_comment_id}">
                                                    <span id="jcountreply_{$comments->organization_announcement_comment_id}">{if $comments->num_replies gt 0}{$comments->num_replies} Replies {/if}
                                                    </span>
                                                </a>                                                    
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="comment_sharelike_sec">
                                            {if $comments->user_id eq PageContext::$response->sess_user_id || $comments->community_created_by eq PageContext::$response->sess_user_id}
                                            <a href="#" class="jOrgFeedCommentDelete marg10right" aid="{$feed['organization_announcement_id']}" id="{$comments->organization_announcement_comment_id}"><i class="fa fa-times"></i> Delete</a>
                                            {/if}
                                            <a href="#" class="jFeedCommentReply marg10right" aid="{$feed['organization_announcement_id']}" cid="{$comments->organization_announcement_comment_id}" id="{$comments->organization_announcement_comment_id}"><i class="fa fa-reply"></i> Reply</a>
                                            <a href="#" class="jOrgFeedCommentLike {if $comments->LIKE_ID gt 0 }  liked {/if}" id="jlikeComment_{$comments->organization_announcement_comment_id}" cid="{$comments->organization_announcement_comment_id}" aid="{$feed['organization_announcement_id']}" >

                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                                <span id="jlikedisplaycomment_{$comments->organization_announcement_comment_id}">Like </span></a>
                                        </div>
                                        </span>
                                        <div id="jdivReply_{$comments->organization_announcement_comment_id}" class="jDisplayReply">
                                            <p class="lead emoji-picker-container position_relatv">
                                                            <textarea placeholder="Write your reply here..."  class="form-control textarea-control jEmojiTextarea" style="height:35px;" id="watermark_{$feed['organization_announcement_id']}_{$comments->organization_announcement_comment_id}" class="watermark" name="watermark"  ></textarea>
                                            </p>
                                            <a class="upload_cambtn" id="{$feed['organization_announcement_id']}" >
                                                <label for="imagereply_file_{$comments->organization_announcement_comment_id}"><i class="fa fa-camera feed-reply-camera"></i></label>
                                                <div class="file_button"> <input type="file" id="imagereply_file_{$comments->organization_announcement_comment_id}" cid="{$comments->organization_announcement_comment_id}" class="jorgimagereply_file" style="display:none;" /></div></a>
                                                <a id="shareButton" ondblclick="this.disabled=true;" cid="{$feed['community_id']}" cmid="{$comments->organization_announcement_comment_id}" aid="{$feed['organization_announcement_id']}"  class="btn yellow_btn fontupper pull-right jPostCommentButtonOrganization"> Post</a>
                                        </div>
                                        <div class="loader" id="imagereply_loader_{$comments->organization_announcement_comment_id}">
                                            <img src="{PageContext::$response->userImagePath}default/loader.gif" />
                                        </div>
                                        <div id="jDivReplyImagePreview_{$comments->organization_announcement_comment_id}"></div>
                                        <div id="postingReply_{$comments->organization_announcement_comment_id}"></div>
                                    </div>
                                </div> 
                            </div>
                            {/if}
                      {/foreach}

{if !empty(PageContext::$response->Comments[{$announcement['organization_announcement_id']}])}
                                                            {if PageContext::$response->Page[{$announcement['organization_announcement_id']}]['totpages'] != PageContext::$response->Page[{$announcement['organization_announcement_id']}]['currentpage']}
                                                                <div id="jDisplayMoreComments_{$announcement['organization_announcement_id']}" ><a href="#" id="jMorecommentsButton" pid="{PageContext::$response->Page[{$announcement['organization_announcement_id']}]['currentpage']}" aid="{$announcement['organization_announcement_id']}">Load more comments</a></div>
                                                        {/if}
                                                        
                                                        {/if}

                    <!------------------------EOF Comments-------------------------------->
                </div>
            </div>
        </div>
     {/if}  
     
     {if $feed['news_feed_id']}    
    <div class="whitebox" id="news_feed_{$feed['news_feed_id']}">
        <div class="wid100per">
            <div class="postheadtablediv">
                <div class="post_left">
                    <span class="mediapost_pic">
                        <a href="{PageContext::$response->baseUrl}timeline/{$feed['user_alias']}">
                        {if $feed['file_path'] neq ''}
                        <img src="{PageContext::$response->userImagePath}{$feed['file_path']}">
                        {else}
                         <img src="{PageContext::$response->userImagePath}/member_noimg.jpg">
                        {/if}
                        </a>
                    </span>
                </div>
                <div class="post_right">
                    <div class="posthead">
                        <h4 class="media-heading"><a href="{PageContext::$response->baseUrl}timeline/{$feed['user_alias']}">{$feed['Username']}</a> <span></span></h4>
                    </div>
                    <div class="postsubhead">
                        <div class="postsubhead_right">
                             <span>
                                {if $feed['days'] gt 0}
                                    {$feed['days']} days ago
                                {else}
                                    {assign var="timesplit" value=":"|explode:$feed['output_time']}
                                    {if $timesplit[0]>0}
                                        {$timesplit[0]|ltrim:'0'} hrs ago
                                    {else}
                                        {if $timesplit[1]>0}
                                            {$timesplit[1]|ltrim:'0'}
                                                {if $timesplit[1] eq 1} min
                                                {else} mins
                                                {/if} ago
                                        {else}
                                            Just Now
                                        {/if}
                                    {/if}
                                {/if}
                            </span>
                        </div>
                </div>
            </div>
            </div>
            <div class="clearfix"></div>
            <div class="postsubhead_left">
                {assign var="url" value= ''}
            {assign var="reg_exUrl" value= '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/'}
            {if preg_match({$reg_exUrl}, {$feed['news_feed_comment']},$url)}
                  
                <div class="jEmojiable">{preg_replace({$reg_exUrl}, "<a href='{$url[0]}'>{$url[0]}</a>" , {$feed['news_feed_comment']})}</div>

            {else}
  
                <div class="jEmojiable">{$feed['news_feed_comment']}</div>

            {/if}
            </div>
                
            <div class="clearfix"></div>
        </div>
      
            {if $feed['news_feed_image_name']}
            {assign var="img" value=$feed['news_feed_image_name']}
            <div class="postpic">
                <ul class="portfolio" class="clearfix">
                        <li><a href="{PageContext::$response->userImagePath}{$img}" title=""><img src="{PageContext::$response->userImagePath}{$img}" alt=""></a></li>
                </ul>


            </div>
            {/if}
      
                                    <!----------------Statistics ------------------------>
                                    <div class="mediapost_user-side">
                                        <div class="row">
                                        <div class="col-sm-4 col-md-4 col-lg-5">
                                            <div class="likesec">
                                            <div class="display_table_cell pad10_right">
                                                <a href="#" class=" colorgrey_text jLikeNewsFeed {if $feed['news_feed_num_like'] > 0} liked {/if}" id="jlike_{$feed['news_feed_id']}" cid="{$feed['news_feed_id']}">
                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                                <span id="jlikedisplay_{$feed['news_feed_id']}">Like </span></a>
                                            </div>
                                             <div class="display_table_cell pad10_right">
                                                <i class="fa fa-comments-o icn-fnt-size"></i>
                                                <a href="#" id="jCommentButton_{$feed['news_feed_id']}" aid="{$feed['news_feed_id']}" class="jNewsfeedCommentButton colorgrey_text">Comment</a>
                                            </div>
                                                    {if $feed['news_feed_user_id'] eq PageContext::$response->sess_user_id}
                                            <div class="display_table_cell pad10_right">
                                                <i class="fa fa-trash-o icn-fnt-size"></i>
                                            <a href="#" id="jDeleteButton_{$feed['news_feed_id']}" aid="{$feed['news_feed_id']}" class="jNewsfeedDeletetButton colorgrey_text">Delete</a>
                                            </div>
                                                {/if}
                                            </div>
                                        </div>
                                        <div class="col-sm-8 col-md-8 col-lg-7">
                       
                                            <div class="sharefnt">
<!--                                                                     <div id="feed_like_users_{$feed['news_feed_id']}" class="like_users_div" style=" display: none;">
                      {foreach from=PageContext::$response->newsFeedLikeUsers[$feed['news_feed_id']] key = id item=val } 
                      <span>{$val->news_feed_like_user_name}<br></span>
                      {/foreach}  
                    </div>-->
                                            <span class="mediapost_links jShowFeedLikeUsers" id="jcountlike_{$feed['news_feed_id']}">{if $feed['news_feed_num_like'] > 0}{$feed['news_feed_num_like']} Likes {/if}</span>
                                            <span class="mediapost_links jNewsfeedCommentButton count_class"  aid ="{$feed['news_feed_id']}" id="jcountcomment_{$feed['news_feed_id']}">{if $feed['news_feed_num_comments'] > 0}{$feed['news_feed_num_comments']}
                                                Comments 
                                                {/if}
                                            </span>
                                            <div class="fb-share-button" id="share_button_{$feed['news_feed_id']}" data-href="{PageContext::$response->baseUrl}newsfeed-detail/{$feed['news_feed_alias']}" data-title ="Shared on facebook" data-layout="button_count" data-desc="You can share data" data-url="{PageContext::$response->baseUrl}newsfeed-detail/{$feed['news_feed_alias']}"></div>
                                            <a href="{PageContext::$response->baseUrl}newsfeed-detail/{$feed['news_feed_alias']}" title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                                            
                                            </div>
                                        </div>
                                        </div>
                                        <div class="clearfix"></div>
                                   </div>
                                    <!----------------End of  Statistics ------------------------>
                                    <!---------------Share buttons-------------------------------->
                                   
                                    <!---------------End of Share buttons-------------------------------->
                                    <div class="mediapost_user-side-top clear jCommentDisplayDiv" id="jNewsfeedCommentBoxDisplayDiv_{$feed['news_feed_id']}" style="display:none">
                                         <p class="lead emoji-picker-container">
                                         <div class="emotionsbtn_textblk">
                                            <textarea cid="{$feedlist['community_id']}" cmid="" aid="{$feed['news_feed_id']}" class="form-control textarea-control jtxtSearchProdAssign jEmojiTextarea"  id="watermark_{$feed['news_feed_id']}" rows="3" placeholder="Write your comment here..." ></textarea>
                                             <a class="upload_cambtn" id="{$feedlist['community_announcement_id']}">
                                             <label for="file_{$feed['news_feed_id']}" ><i class="fa fa-camera feed-camera"></i></label></a>
                                                  <input type="file" id="file_{$feed['news_feed_id']}" aid="{$feed['news_feed_id']}" cmid="{$feed['news_feed_comment_id']}" class="jfeedimageimage_file" style="cursor: pointer;  display: none"/>
                                              <input type="hidden" id="announcement_id" value="{$feedlist['community_announcement_id']}">
                                        </div>
                                         
                                         </p>
                                         <div class="display_image_div">
                                             <div id="juploadcommentfeedimagepreview_{$feed['news_feed_id']}"></div>
                                        </div>
                                          <div class="msg_display" id="msg_display_{$feed['news_feed_id']}"></div>
										 	<div class="clearfix"></div>
<!--                                         {PageContext::$response->Comments|@print_r}-->
                                         <a id="postButton" ondblclick="this.disabled=true;" cid="{$feedlist['community_id']}" cmid="" aid="{$feed['news_feed_id']}"  class="btn yellow_btn fontupper pull-left jPostNewsfeedCommentButton"> Post</a>
                                       </div>
                                           <div class="clear commentblk jCommentDisplayDiv" id="jNewsfeedCommentDisplayDiv_{$feed['news_feed_id']}" style="display:none">
                                                <div id="posting_{$feed['news_feed_id']}">
                                                   <!------------------------Comments-------------------------------->
                                                   {assign var=val value=0}
                                                
                                                    {foreach PageContext::$response->newsFeed[{$feed['news_feed_id']}] as $key=>$comments}
                                                   
                                                    {if $comments->news_feed_comments_id}
                                                    
                                                    <input type="hidden" id="val_{$feed['news_feed_id']}" value="{count(PageContext::$response->newsFeed[{$feed['news_feed_id']}])}">
                                                        <div class="col-md-12 btm-mrg pad10p" id="jdivComment_{$comments->news_feed_comments_id}">
                                                            <div class="row">
                                                                <div class="col-md-1">
                                                                    <a href="{PageContext::$response->baseUrl}timeline/{$comments->user_alias}">
                                                                    {if $comments->user_image neq ''}
                                                                        <span class="mediapost_pic">
                                                                            <img class="ryt-mrg" src="{PageContext::$response->userImagePath}{$comments->user_image}">
                                                                        </span>
                                                                    {/if} 
                                                                     {if $comments->user_image eq ''}
                                                                        <span class="mediapost_pic">
                                                                            <img class="ryt-mrg" src="{PageContext::$response->userImagePath}medium/member_noimg.jpg">
                                                                        </span>
                                                                     {/if}
                                                                     </a>
                                                                    
                                                                </div>
                                                                <div class="col-md-11">
                                                                    <span class="name"><a href="{PageContext::$response->baseUrl}timeline/{$comments->user_alias}">{$comments->Username} </a></span>
                                                                    <span class="jEmojiable">{$comments->news_feed_comment_content}</span>
                                                                    <br>
                                                                    <span class="new-text">
                                                                        
                                                                        {$comments->commentDate} 
                                                                    </span>
                                                                    <div class="commentimg_box">
                                                                        {if $comments->file_path neq ''}
<!--                                                                          <img class="fancybox" src="{PageContext::$response->userImagePath}medium/{$comments->file_path}">-->
                                                                            <ul class="portfolio" class="clearfix">
                                                                                    <li><a href="{PageContext::$response->userImagePath}{$comments->file_path}" title=""><img src="{PageContext::$response->userImagePath}{$comments->file_path}" alt=""></a></li>
                                                                            </ul>
                                                                          {/if}
                                                                    </div>
                                                                    <div class="mediapost_user-side1">
                                                                        <span  rel="tooltip" title="{$comments->users} liked this comment" class="mediapost_links" id="jcountfeedcommentlike_{$comments->news_feed_comments_id}">{if $comments->num_comment_likes > 0}{$comments->num_comment_likes} Likes {/if}</span>
                                                                        <input type="hidden" id="reply_{$comments->news_feed_comments_id}" value="{if PageContext::$response->newsFeed[{$feed['news_feed_id']}][{$comments->news_feed_comments_id}] gt 0}{PageContext::$response->newsFeed[{$feed['news_feed_id']}][{$comments->news_feed_comments_id}]}{else}0{/if}">
                                                                        <div class="mediapost_links jShowFeedReply" cid="{$comments->announcement_comment_id}" id="{$comments->news_feed_comments_id}">
                                                                            <a href="#" id="{$comments->news_feed_comments_id}">
                                                                                <span id="jcountreply_{$comments->news_feed_comments_id}">{if PageContext::$response->newsFeed[{$feed['news_feed_id']}][{$comments->news_feed_comments_id}] gt 0} {PageContext::$response->newsFeed[{$feed['news_feed_id']}][{$comments->news_feed_comments_id}]} Replies {/if}
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                        <div class="comment_sharelike_sec">
                                                                              <!--{$comments|print_r}-->
                                                                        {if $comments->news_feed_comment_user_id eq PageContext::$response->sess_user_id}
                                                                        <a href="#" class="jNewsFeedCommentDelete marg10right" aid="{$comments->news_feed_id}" id="{$comments->news_feed_comments_id}"><i class="fa fa-times"></i> Delete</a>
                                                                        {/if}
                                                                        <a href="#" class="jFeedCommentReply marg10right" aid="{$comments->news_feed_id}" cid="{$comments->news_feed_comments_id}" id="{$comments->news_feed_comments_id}"><i class="fa fa-reply"></i> Reply</a>
                                                                        <!--<a href="#" class="jFeedCommentLike" aid="{$feedlist['community_announcement_id']}" id="{$comments->announcement_comment_id}">Like</a>-->
                                                                         <a href="#" class="jNewsFeedCommentLike {if $comments->num_comment_likes>0} liked {/if}" id="jlikeComment_{$comments->news_feed_comments_id}" cid="{$comments->news_feed_comments_id}" aid="{$comments->news_feed_id}" >
                                                
                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                                    <span id="jlikedisplaycomment_{$comments->announcement_comment_id}">Like </span></a>
                                                                        </div>
                                                                    </span>
                                                                    <div id="jdivReply_{$comments->news_feed_comments_id}" class="jDisplayReply">
                                                                        <p class="lead emoji-picker-container">
                                                                        <div class="emotionsbtn_textblk">
                                                                            <textarea placeholder="Write your reply here..."  class="form-control textarea-control jEmojiTextarea" style="height:35px;" id="watermark_{$comments->news_feed_id}_{$comments->news_feed_comments_id}" class="watermark" name="watermark"  ></textarea>
                                                                            <a class="upload_cambtn" id="{$feedlist['community_announcement_id']}" >
                                                                                <label for="imagereply_file_{$comments->news_feed_comments_id}"><i class="fa fa-camera feed-reply-camera"></i></label>
                                                                               <div class="file_button"> <input type="file" id="imagereply_file_{$comments->news_feed_comments_id}" cid="{$comments->news_feed_comments_id}" class="jfeedimagereply_file" style="display:none;" /></div></a>
                                                                            </div>
                                                                        </p>
<!--                                                                        <a class="upload_cambtn" id="{$feedlist['community_announcement_id']}" >
                                                                                <label for="imagereply_file_{$comments->news_feed_comments_id}"><i class="fa fa-camera feed-reply-camera"></i></label>
                                                                               <div class="file_button"> <input type="file" id="imagereply_file_{$comments->news_feed_comments_id}" cid="{$comments->news_feed_comments_id}" class="jfeedimagereply_file" style="display:none;" /></div></a>-->
                                                                       <div class="msg_display" id="msg_display_{$comments->news_feed_comments_id}"></div>
										 	<div class="clearfix"></div>
                                                                        <a id="shareButton" ondblclick="this.disabled=true;" cid="{$feedlist['community_id']}" cmid="{$comments->news_feed_comments_id}" aid="{$comments->news_feed_id}"  class="btn yellow_btn fontupper pull-right jPostNewsfeedCommentButton"> Post</a>
                                                                    </div>
                                                                     <div class="loader" id="imagereply_loader_{$comments->announcement_comment_id}">
                                                                        <img src="{PageContext::$response->userImagePath}default/loader.gif" />
                                                                    </div>
                                                                    <div id="jDivReplyImagePreview_{$comments->news_feed_comments_id}"></div>
                                                                    <div id="postingReply_{$comments->news_feed_comments_id}"></div>
                                                                </div>
                                                            </div> 
                                                            
                                                        </div>
                                                    {/if}
                                                    {/foreach}

                                                    {if !empty(PageContext::$response->newsFeed[$feed['news_feed_id']])}
                                                    {if PageContext::$response->Page[$feed['news_feed_id']]['totpages'] != PageContext::$response->Page[$feed['news_feed_id']]['currentpage']}
                                                    <div id="jDisplayMoreComments_{$feed['news_feed_id']}" ><a href="#" id="jMorecommentsButton" pid="{PageContext::$response->Page[$feed['news_feed_id']]['currentpage']}" aid="{$feed['news_feed_id']}">Load more comments</a></div>
                                                    {/if}
                                                    {/if}
                                                    
                                                    <!------------------------EOF Comments-------------------------------->
                                                </div>
                                    </div>
                                    
                               
</div>  
     {/if}
{/foreach}
</div>
</div>
<div class="{$third_col}"> 
    
     {if PageContext::$response->friend_display eq '1'}
        {if count(PageContext::$response->friend_display_list) gt 0}            
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-user" aria-hidden="true"></i> Friends <a  href="{PageContext::$response->baseUrl}timeline/{PageContext::$response->sess_user_alias}#Friends" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist">
                    {foreach from=PageContext::$response->friend_display_list  key=id item=friendslist}
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <a class="friendphotobox_blk" href="{PageContext::$response->baseUrl}timeline/{$friendslist->user_alias}">
                                {if $friendslist->file_path}
                                <div class="friendphotobox" style="background-image: url('{PageContext::$response->userImagePath}{$friendslist->file_path}');">
                                </div>
                                {else}
                                <div class="friendphotobox" style="background-image: url('{PageContext::$response->userImagePath}medium/member_noimg.jpg');">
                                </div>
                                {/if}
                                <h4>{$friendslist->user_firstname|ucfirst} {$friendslist->user_lastname|ucfirst}</h4>
                            </a>
                        </li>
                      {/foreach}
                </ul>
                </div>  
        {/if}
     {/if}
    
     {if PageContext::$response->gallery_display eq '1'}
        {if count(PageContext::$response->gallery_display_list) gt 0}  
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-file-image-o" aria-hidden="true"></i> Photos <a href="{PageContext::$response->baseUrl}timeline/{PageContext::$response->sess_user_alias}#Gallery"  class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul id="portfolionewsfeed" class="row frndboxlist ">
                     {foreach from=PageContext::$response->gallery_display_list  key=id item=gallerylist}
                     
<!--                    <li class="display_table_cell wid32per">
                        <a class="friendphotobox_blk" href="#">
                            <div class="friendphotobox" style="background-image: url('{PageContext::$response->userImagePath}medium/{$gallerylist->news_feed_image_name}');">-->
                                
<!--                            <ul class="portfolio" class="clearfix">-->
                                    <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4 vert_align_top">
                                        <a class="friendphotobox_blk" href="{PageContext::$response->userImagePath}{$gallerylist->news_feed_image_name}" title="{$gallerylist->news_feed_comment}">
                                            <img src="{PageContext::$response->userImagePath}{$gallerylist->news_feed_image_name}" alt="">
                                        </a>
                                    </li>
                   {/foreach} 
                </ul>
                <!--<div class="sidead" style="background-image: url('{PageContext::$response->ImagePath}sidead.jpg');"> </div>-->
            </div>  
          {/if}
     {/if}
</div>
{else}
<div class='whitebox'>
    No Data Found
</div>
{/if}
</div>
</div>
</div>

