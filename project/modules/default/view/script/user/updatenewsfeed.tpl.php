<?php
foreach (PageContext::$response->news_feed as $feed) { ?>
    <div class="whitebox" id="news_feed_<?php echo $feed->news_feed_id; ?>">
        <div class="wid100per">
            <div class="postheadtablediv">
                <div class="post_left">
                <span class="mediapost_pic">
                    <?php if ($feed->file_path) { ?>
                        <img
                            src="<?php echo PageContext::$response->userImagePath; ?><?php echo $feed->file_path; ?>">
                    <?php } else { ?>
                        <img src="<?php echo PageContext::$response->userImagePath; ?>small/member_noimg.jpg">
                    <?php } ?>
                </span>
                </div>
                <div class="post_right">
                    <div class="posthead"><h4 class="media-heading"><a
                                href="#"><?php echo $feed->user_firstname.' '.$feed->user_lastname; ?></a> <span></span></h4></div>
                    <div class="postsubhead">
                        <div class="postsubhead_right">
                            <span> Just now </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="postsubhead_left">
                 <?php
                $reg_exUrl = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/';
                if(preg_match($reg_exUrl, $feed->news_feed_comment,$url)){ ?>
                  
                <div class="jEmojiable"><?php echo preg_replace($reg_exUrl, "<a href='$url[0]'>$url[0]</a>" , nl2br($feed->news_feed_comment));?></div>

                <?php
                
                }else{ ?>

                <div class="jEmojiable"><?php echo $feed->news_feed_comment;?></div>

                <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <a href="#">
            <?php
            if ($feed->news_feed_image_name) { ?>
                <div class="postpic">
                    <ul class="portfolio" class="clearfix">
                        <li>
                            <a href="<?php echo PageContext::$response->userImagePath; ?><?php echo $feed->news_feed_image_name; ?>"
                               title="<?php echo $feed->news_feed_comment;?>"><img
                                    src="<?php echo PageContext::$response->userImagePath; ?><?php echo $feed->news_feed_image_name; ?>"
                                    alt="">
                            </a>
                        </li>
                    </ul>
                </div>
            <?php } ?>
        </a>

   
 <!----------------Statistics ------------------------>
                                    <div class="mediapost_user-side">
                                        <div class="row">
                                        <div class="col-sm-4 col-md-4 col-lg-5">
                                            <div class="likesec">
                                            <div class="display_table_cell pad10_right">
                                                <a href="#" class=" colorgrey_text jLikeNewsFeed <?php if($feed->news_feed_num_like > 0){?> liked <?php } ?>" id="jlike_<?php echo $feed->news_feed_id;?>" cid="<?php echo $feed->news_feed_id;?>">
                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                                <span id="jlikedisplay_<?php echo $feed->news_feed_id;?>">Like </span></a>
                                            </div>
                                             <div class="display_table_cell pad10_right">
                                                <i class="fa fa-comments-o icn-fnt-size"></i>
                                                <a href="#" id="jCommentButton_<?php echo $feed->news_feed_id;?>" aid="<?php echo $feed->news_feed_id;?>" class="jNewsfeedCommentButton colorgrey_text">Comment</a>
                                            </div>
                                                    <?php if($feed->news_feed_user_id == PageContext::$response->sess_user_id){?>
                                            <div class="display_table_cell pad10_right">
                                                <i class="fa fa-trash-o icn-fnt-size"></i>
                                            <a href="#" id="jDeleteButton_<?php echo $feed->news_feed_id;?>" aid="<?php echo $feed->news_feed_id;?>" class="jNewsfeedDeletetButton colorgrey_text">Delete</a>
                                            </div>
                                                    <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-8 col-md-8 col-lg-7">
                                            <div class="sharefnt">
                                            <span class="mediapost_links jShowFeedLikeUsers" id="jcountlike_<?php echo $feed->news_feed_id;?>"><?php if($feed->news_feed_num_like > 0){ echo $feed->news_feed_num_like." Likes";} ?></span>
                                            <span class="mediapost_links jNewsfeedCommentButton count_class"  aid ="<?php echo $feed->news_feed_id;?>" id="jcountcomment_<?php echo $feed->news_feed_id;?>"><?php if($feed->news_feed_num_comments > 0){ echo $feed->news_feed_num_comments."Comments";}?>
                                            </span>
                                            <!--<div class="fb-share-button" id="share_button_<?php echo $feed->news_feed_id;?>" data-href="<?php echo PageContext::$response->baseUrl;?>newsfeed-detail/<?php echo $feed->news_feed_alias;?>" data-title ="Shared on facebook" data-layout="button_count" data-desc="You can share data" data-url="<?php echo PageContext::$response->baseUrl;?>newsfeed-detail/<?php echo $feed->news_feed_alias;?>"></div>-->
                                                       <div id="share_button" class="fb-share-button" data-href="<?php echo PageContext::$response->baseUrl;?>newsfeed-detail/<?php echo $feed->news_feed_alias;?>" data-layout="button_count" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url_share.$feed->news_feed_alias;?>;src=sdkpreparse">Share</a></div>
                                            <a href="<?php echo PageContext::$response->baseUrl;?>newsfeed-detail/<?php echo $feed->news_feed_alias;?>" title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                                            
                                            </div>
                                        </div>
                                        </div>
                                        <div class="clearfix"></div>
                                   </div>
                                    <!----------------End of  Statistics ------------------------>
                                    <!---------------Share buttons-------------------------------->
                                   
                                    <!---------------End of Share buttons-------------------------------->
                                    <div class="mediapost_user-side-top clear jCommentDisplayDiv" id="jNewsfeedCommentBoxDisplayDiv_<?php echo $feed->news_feed_id;?>" style="display:none">
                                         <p class="lead emoji-picker-container">
                                         <div class="emotionsbtn_textblk">
                                            <textarea cid="<?php echo $feedlist->community_id;?>" cmid="" aid="<?php echo $feed->news_feed_id;?>" class="form-control textarea-control jtxtSearchProdAssign jEmojiTextarea"  id="watermark_<?php echo $feed->news_feed_id;?>" rows="3" placeholder="Write your comment here..." ></textarea>
                                             <a class="upload_cambtn" id="<?php echo $feedlist->community_announcement_id;?>">
                                             <label for="file_<?php echo $feed->news_feed_id;?>" ><i class="fa fa-camera feed-camera"></i></label></a>
                                                  <input type="file" id="file_<?php echo $feed->news_feed_id;?>" aid="<?php echo $feed->news_feed_id;?>" cmid="<?php echo $feed->news_feed_comment_id;?>" class="jfeedimageimage_file" style="cursor: pointer;  display: none"/>
                                              <input type="hidden" id="announcement_id" value="<?php echo $feedlist->community_announcement_id;?>">
                                        </div>
                                         
                                         </p>
                                         <div class="display_image_div">
                                             <div id="juploadcommentfeedimagepreview_<?php echo $feed->news_feed_id;?>"></div>
                                        </div>
                                          <div class="msg_display" id="msg_display_<?php echo $feed->news_feed_id;?>"></div>
										 	<div class="clearfix"></div>
<!--                                         {PageContext::$response->Comments|@print_r}-->
                                         <a id="postButton" ondblclick="this.disabled=true;" cid="<?php echo $feedlist->community_id;?>" cmid="" aid="<?php echo $feed->news_feed_id;?>"  class="btn yellow_btn fontupper pull-left jPostNewsfeedCommentButton"> Post</a>
                                       </div>
                                    <div class="clear commentblk jCommentDisplayDiv" id="jNewsfeedCommentDisplayDiv_<?php echo $feed->news_feed_id;?>" style="display:none">
                                                <div id="posting_<?php echo $feed->news_feed_id;?>">
                                                   <!------------------------Comments-------------------------------->
                                                   <?php $val = 0 ;
                                                 
                                                    foreach(PageContext::$response->newsFeed[$feed->news_feed_id] as $key=>$comments){
                                                    if($comments->news_feed_comments_id){ ?>
                                                    
                                                    <input type="hidden" id="val_<?php echo $feed->news_feed_id;?>" value="<?php echo count(PageContext::$response->newsFeed[$feed['news_feed_id']]);?>">
                                                        <div class="col-md-12 btm-mrg pad10p" id="jdivComment_<?php echo $comments->news_feed_comments_id;?>">
                                                            <div class="row">
                                                                <div class="col-md-1">
                                                                    <a href="<?php echo PageContext::$response->baseUrl;?>timeline/<?php echo $comments->user_alias;?>">
                                                                    <?php if($comments->file_path != ''){?>
                                                                        <span class="mediapost_pic">
                                                                            <img class="ryt-mrg" src="<?php echo PageContext::$response->userImagePath;?><?php echo $comments->file_path;?>">
                                                                        </span>
                                                                    <?php } ?> 
                                                                     <?php if($comments->file_path == '') { ?>
                                                                        <span class="mediapost_pic">
                                                                            <img class="ryt-mrg" src="<?php echo PageContext::$response->userImagePath;?>medium/member_noimg.jpg">
                                                                        </span>
                                                                     <?php } ?>
                                                                     </a>
                                                                    
                                                                </div>
                                                                <div class="col-md-11">
                                                                    <span class="name"><a href="<?php echo PageContext::$response->baseUrl;?>timeline/<?php echo $comments->user_alias;?>"><?php echo $comments->Username;?> </a></span>
                                                                    <span class="jEmojiable"><?php echo $comments->news_feed_comment_content;?></span>
                                                                    <br>
                                                                    <span class="new-text">
                                                                        
                                                                        <?php echo $comments->commentDate;?> 
                                                                    </span>
                                                                    <div class="commentimg_box">
                                                                        <?php if($comments->file_path != ''){ ?>
<!--                                                                          <img class="fancybox" src="{PageContext::$response->userImagePath}medium/{$comments->file_path}">-->
                                                                            <ul class="portfolio" class="clearfix">
                                                                                    <li><a href="<?php echo PageContext::$response->userImagePath;?>medium/<?php echo $comments->file_path;?>" title=""><img src="<?php echo PageContext::$response->userImagePath;?>medium/<?php echo $comments->file_path;?>" alt=""></a></li>
                                                                            </ul>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="mediapost_user-side1">
                                                                        <span class="mediapost_links" id="jcountfeedcommentlike_<?php echo $comments->news_feed_comments_id;?>"><?php if($comments->num_comment_likes > 0){ echo $comments->num_comment_likes." Likes"; }?></span>
                                                                        <input type="hidden" id="reply_<?php echo $comments->news_feed_comments_id;?>" value="<?php if($comments->num_replies > 0){ echo $comments->num_replies; }else { echo "0"; }?>">
                                                                        <div class="mediapost_links jShowFeedReply" cid="<?php echo $comments->announcement_comment_id;?>" id="<?php echo $comments->news_feed_comments_id;?>">
                                                                            <a href="#" id="<?php echo $comments->news_feed_comments_id;?>">
                                                                                <span id="jcountreply_<?php echo $comments->news_feed_comments_id;?>"><?php if(PageContext::$response->newsFeed[$feed['news_feed_id']][$comments->news_feed_comments_id] > 0){ echo PageContext::$response->newsFeed[$feed['news_feed_id']][$comments->news_feed_comments_id]." Replies"; }?>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                        <div class="comment_sharelike_sec">
                                                                              <!--{$comments|print_r}-->
                                                                        <?php if($comments->news_feed_comment_user_id == PageContext::$response->sess_user_id){ ?>
                                                                        <a href="#" class="jNewsFeedCommentDelete marg10right" aid="<?php echo $comments->news_feed_id;?>" id="<?php echo $comments->news_feed_comments_id;?>"><i class="fa fa-times"></i> Delete</a>
                                                                        <?php } ?>
                                                                        <a href="#" class="jFeedCommentReply marg10right" aid="<?php echo $comments->news_feed_id;?>" cid="<?php echo $comments->news_feed_comments_id;?>" id="<?php echo $comments->news_feed_comments_id;?>"><i class="fa fa-reply"></i> Reply</a>
                                                                        <!--<a href="#" class="jFeedCommentLike" aid="{$feedlist['community_announcement_id']}" id="{$comments->announcement_comment_id}">Like</a>-->
                                                                         <a href="#" class="jNewsFeedCommentLike <?php if($comments->num_comment_likes>0){?> liked <?php } ?>" id="jlikeComment_<?php echo $comments->news_feed_comments_id;?>" cid="<?php echo $comments->news_feed_comments_id;?>" aid="<?php echo $comments->news_feed_id;?>" >
                                                
                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                                    <span id="jlikedisplaycomment_<?php echo $comments->announcement_comment_id;?>">Like </span></a>
                                                                        </div>
                                                                    </span>
                                                                    <div id="jdivReply_<?php echo $comments->news_feed_comments_id;?>" class="jDisplayReply">
                                                                        <p class="lead emoji-picker-container">
                                                                        <div class="emotionsbtn_textblk">
                                                                            <textarea placeholder="Write your reply here..."  class="form-control textarea-control jEmojiTextarea" style="height:35px;" id="watermark_<?php echo $comments->news_feed_id;?>_<?php echo $comments->news_feed_comments_id;?>" class="watermark" name="watermark"  ></textarea>
                                                                            <a class="upload_cambtn" id="<?php echo $feedlist->community_announcement_id;?>" >
                                                                                <label for="imagereply_file_<?php echo $comments->news_feed_comments_id;?>"><i class="fa fa-camera feed-reply-camera"></i></label>
                                                                               <div class="file_button"> <input type="file" id="imagereply_file_<?php echo $comments->news_feed_comments_id;?>" cid="<?php echo $comments->news_feed_comments_id;?>" class="jfeedimagereply_file" style="display:none;" /></div></a>
                                                                            </div>
                                                                        </p>
<!--                                                                        <a class="upload_cambtn" id="{$feedlist['community_announcement_id']}" >
                                                                                <label for="imagereply_file_{$comments->news_feed_comments_id}"><i class="fa fa-camera feed-reply-camera"></i></label>
                                                                               <div class="file_button"> <input type="file" id="imagereply_file_{$comments->news_feed_comments_id}" cid="{$comments->news_feed_comments_id}" class="jfeedimagereply_file" style="display:none;" /></div></a>-->
                                                                       <div class="msg_display" id="msg_display_<?php echo $comments->news_feed_comments_id;?>"></div>
										 	<div class="clearfix"></div>
                                                                        <a id="shareButton" ondblclick="this.disabled=true;" cid="<?php echo $feedlist->community_id;?>" cmid="<?php echo $comments->news_feed_comments_id;?>" aid="<?php echo $comments->news_feed_id;?>"  class="btn yellow_btn fontupper pull-right jPostNewsfeedCommentButton"> Post</a>
                                                                    </div>
                                                                     <div class="loader" id="imagereply_loader_<?php echo $comments->announcement_comment_id;?>">
                                                                        <img src="<?php echo PageContext::$response->userImagePath;?>default/loader.gif" />
                                                                    </div>
                                                                    <div id="jDivReplyImagePreview_<?php echo $comments->news_feed_comments_id;?>"></div>
                                                                    <div id="postingReply_<?php echo $comments->news_feed_comments_id;?>"></div>
                                                                </div>
                                                            </div> 
                                                            
                                                        </div>
                                                    <?php }
                                                    } ?>
                                                   
<!--                                                    {if !empty(PageContext::$response->newsFeed[$feed['news_feed_id']])}
                                                    {if PageContext::$response->Page[$feed['news_feed_id']]['totpages'] != PageContext::$response->Page[$feed['news_feed_id']]['currentpage']}
                                                    <div id="jDisplayMoreComments_{$feed['news_feed_id']}" ><a href="#" id="jMorecommentsButton" pid="{PageContext::$response->Page[$feed['news_feed_id']]['currentpage']}" aid="{$feed['news_feed_id']}">Load more comments</a></div>
                                                    {/if}
                                                    {/if}-->
                                                    
                                                    <!------------------------EOF Comments-------------------------------->
                                                </div>
                                    </div>
                                     </div>
<?php } 


?>