<?php foreach (PageContext::$response->usersList as $user) { ?>
    <div class="row">
        <div class="colside1">
            <div class="colside1_pic">
                <a href="<?php echo PageContext::$response->baseUrl; ?>community/<?php echo $user->community_alias; ?>"><img src="<?php if ($user->file_path == '') {
        echo PageContext::$response->userImagePath; ?>small/noimage.jpg<?php } else {
        echo PageContext::$response->userImagePath; ?>small/<?php echo $user->file_path;
    } ?>"></a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="colmiddle">
            <h6><a href="<?php echo PageContext::$response->baseUrl; ?>profile/<?php echo $user->user_alias; ?>"><?php echo ($user->user_firstname) ? $user->user_firstname . ' ' . $user->user_lastname : $user->user_email; ?></a>  </h6>has Invited you to Join <h6><a href="<?php echo PageContext::$response->baseUrl; ?>community/<?php echo $user->community_alias; ?>"><?php echo $user->community_name; ?> </a>  </h6>

        </div>
        <div class="colside3">
    <?php if (PageContext::$response->sess_user_id > 0) { ?>
                <a class="friends_btn fright" uid="<?php echo $user->user_id; ?>" href="<?php echo PageContext::$response->baseUrl; ?>accept-invitation/<?php echo $user->invitation_id; ?>/reject/0/pending-bizcom-request" style="float:left; ">

                    <span class="declinefriend_icon"></span>
                    Decline
                </a>
                <a class="friends_btn fright" uid="<?php echo $user->user_id; ?>" href="<?php echo PageContext::$response->baseUrl; ?>accept-invitation/<?php echo $user->invitation_id; ?>/community/0/pending-bizcom-request" style="float:left; ">

                    <span class="tik_icon"></span>
                    Accept
                </a>

    <?php } ?>

        </div>
    </div>
<?php } ?>