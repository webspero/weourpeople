<script type="text/javascript">
    $(document).ready(function(){
       $(".notifi_no_friend").html(0);
    });
</script>
<div class="container">
    <section class="whitebox marg40col">
    <div class="row"> 
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h3>Pending Friend Requests<span class="round-search">{PageContext::$response->totalrecords}</span></h3>
            {PageContext::renderRegisteredPostActions('messagebox')}
            <div class="businesslist_search_blk">
             <div class="row"> 
                        <div class="col-sm-6 col-md-4 col-lg-4 pull-right">
                            {php}PageContext::renderRegisteredPostActions('searchbox');{/php}
                        </div>
             </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <section class="bizcomlisting listin">
            {if PageContext::$response->usersList|@sizeof eq 0}
            <div class="col-sm-12 col-md-12 col-lg-12">
            <h3 class="new-titile">
            No Pending Requests Found
            </h3>
            </div>
            {else}
            {foreach from=PageContext::$response->usersList key=id item=user}
            <div class="col-sm-4 col-md-4 col-lg-4">
            <div class="mediapost display_table wid100per">
                <div class="picpost_left display_table_cell wid30per">
                    <span class="picpost_left_pic">
                        <a href="{PageContext::$response->baseUrl}timeline/{$user->user_alias}"><img src="{if $user->file_path eq ''}{PageContext::$response->userImagePath}small/member_noimg.jpg{else}{PageContext::$response->userImagePath}{$user->file_path}{/if}"></a>
                    </span>
                </div>
                <div class="media-body frndslist_media display_table_cell wid70per vert_align_middle">
                    <h4 class="media-heading"><a href="{PageContext::$response->baseUrl}timeline/{$user->user_alias}">{$user->user_firstname} {$user->user_lastname}</a></h4>
                    <p>{$user->user_friends_count} Friends</p>
                    <p>
                        {if PageContext::$response->sess_user_id > 0}
                        <a uid="{$user->user_id}" href="{PageContext::$response->baseUrl}accept-invitation/{$user->invitation_id}/friend/0/pending-request"><span class="accept"><b>+</b> Accept</span></a>
                        <a uid="{$user->user_id}" href="{PageContext::$response->baseUrl}accept-invitation/{$user->invitation_id}/reject/0/pending-request"><span class="reject"><i class="fa fa-times"></i> Reject</span></a>
                        {/if}
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
            </div>
            {/foreach}
            {/if}
        </section>
    </div>
    </section>
</div>

