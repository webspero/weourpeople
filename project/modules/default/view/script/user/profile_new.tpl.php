<div class="content_left">
    <div class="member_left_blk">     
        <div class="member_left_outer_blk">
            <div class="member_profile_picblk" id="profilepic" >
                <a href="#" title="tab-Profile"  id="jProfile" class="jtab " alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">
                    <img src="{PageContext::$response->userImagePath}medium/{PageContext::$response->image}">
                </a>
                <div class="member_profile_changeimg">
                    {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
                    <a href="#" onClick="showProfileChangeBox({PageContext::$response->sess_user_id})">Change profile photo</a>
                    {/if}
                </div>
            </div>
            <span>
                <a href="#" title="tab-Profile" id="jProfile" class="jtab " alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">
                    {PageContext::$response->username}</a>
            </span>
            <!--             <h5>Notifications</h5> -->
            <ul>
                <li><b>Last logged in</b> {time_elapsed_string(PageContext::$response->userDetails['user_last_login'])}<!-- <abbr class="timeago" title="{PageContext::$response->userLastLogin[0]}T{PageContext::$response->userLastLogin[1]}Z"></abbr> --></li>
            </ul>
            <ul>
                <li><div class="r-search-left"><b>Friends </b></div><div class="r-search-right"><div class="r-search2"><a href="#" title="tab-Friends" id="" class="jtab jShowFriends" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">{PageContext::$response->userDetails['user_friends_count']}</a></div></div></li>
                <li><div class="r-search-left"><b>Business </b></div><div class="r-search-right"><div class="r-search2">{if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }<a href="{PageContext::$response->baseUrl}my-directory">{PageContext::$response->userDetails['user_business_count']}</a>{else}{PageContext::$response->userDetails['user_business_count']}{/if}</div></div></li>
                <li><div class="r-search-left"><b>BizCom </b></div><div class="r-search-right"><div class="r-search2"> {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }<a href="{PageContext::$response->baseUrl}my-community">{PageContext::$response->userDetails['user_community_count']}</a>{else}{PageContext::$response->userDetails['user_community_count']}{/if}</div></div></li>
                {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
                <li><div class="r-search-left"><b>Pending request</b></div><div class="r-search-right"><div class="r-search2"><a href="{PageContext::$response->baseUrl}pending-request">{PageContext::$response->pendingRequestCount}</a></div></div></li>
                {/if}
            </ul>
        </div>
    </div>

    <div class="member_middle_blk" id="member_middle_blk">
        {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
        <div class="member_prof_menu">
            <ul>
                {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
                <li class="active"><a href="#" title="tab-Feed" class="jtab">Latest</a></li>
                {/if}
                <li><a href="#" title="tab-Profile" id="" class="jtab jShowProfile" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">Profile</a></li>
                <li><a href="#" title="tab-Friends" class="jtab jShowFriends" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">Friends({PageContext::$response->userDetails['user_friends_count']})</a></li>
                {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
                {/if}
            </ul>
            <div class="colside3">
                {if PageContext::$response->sess_user_id > 0 && PageContext::$response->id neq PageContext::$response->sess_user_id && PageContext::$response->sess_user_status != 'I'}
                {if in_array(PageContext::$response->id, PageContext::$response->myfriends)}
                <a class="friends_btn fright friend" uid="{PageContext::$response->id}" href="#" >
                    <span class="tik_icon"></span>
                    Friends
                    {else if in_array(PageContext::$response->id,PageContext::$response->myinvitedfriends)}
                    <a class="friends_btn fright friend " uid="{PageContext::$response->id}" href="#" >
                        <span class="addfriend_icon jaddfriend"></span>
                        <span id="jFriend_{PageContext::$response->id}">Friend request sent</span>
                        {else if in_array(PageContext::$response->id,PageContext::$response->myPendingRequests)}
                        <a class="friends_btn fright friend jaddfriend1" uid="{PageContext::$response->id}" href="#"  inviteid="{PageContext::$response->inviteId}" >
                            <span class="addfriend_icon "></span>
                            <span id="jFriend_{PageContext::$response->id}">Accept Friend</span>
                            {else}
                            <a class="friends_btn fright friend jaddfriend" uid="{PageContext::$response->id}" href="#" >
                                <span class="addfriend_icon jaddfriend"></span>
                                <span id="jFriend_{PageContext::$response->id}">Add Friend</span>
                                {/if}
                            </a>
                            {/if}
                            </div>
                            <div class="loader loaderposition" id="loading_{PageContext::$response->id}">
                                <img src="{PageContext::$response->userImagePath}default/loader.gif" />
                            </div>
                            </div>
                            <div class="clear"></div>
                            <!-----------------------News Feed-------------------------------->
                            {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
                            <div class="content"  id="tab-Feed">

                                <div class="invite_contacts_blk recent_adddtion_blk" id="inviteblock">
                                    <h3>Invite Friends</h3>
                                    <input type="button" id="jInviteFriends" title="invite-block" class="invite_btn" value="Invite" name="invite">
                                    <div class="" id="msgDisplay1"></div>
                                    <div id="jDisplayInviteGrp" style="display:none">
                                    {php}PageContext::renderRegisteredPostActions('importcontacts');{/php}
                                    <input type="button" id="jdisplayEmailOption" title="invite-block" name="cs_import" class="invite_btn" value="Enter mail ids to invite" name="invite">
                                    </div>
                                  <div id="jDivEmail">
										<textarea style="height:80px;" placeholder="Enter email id's" id="txtmailids" name="txtmailids"></textarea>
										<div class="clear"></div>
										<input class="invite_btn" type="button" uid="{PageContext::$response->sess_user_id}" id="jEmailFriendRequest" Value="Send">
                                  </div>
                                </div>
                                <div class="frnd_reqst_blk">
                                    <h3>Friends Suggestion</h3>
                                    <div class="frnd_reqst_blk_content">
                                        {if PageContext::$response->friendsuggestion|@sizeof eq 0}
                                        No Friend Suggestions Found
                                        {else}
                                        {foreach from=PageContext::$response->friendsuggestion key=id item=user}
                                        <div class="frnds_list_blk">
                                            <div class="frnd_reqst_img">
                                                <a  href="{PageContext::$response->baseUrl}profile/{$user->user_alias}"><img src="{if $user->file_path eq ''}{PageContext::$response->userImagePath}thumb/member_noimg.jpg{else}{PageContext::$response->userImagePath}thumb/{$user->file_path}{/if}"></a>
                                            </div>
                                            <div class="frnd_content">
                                                <h6><a  href="{PageContext::$response->baseUrl}profile/{$user->user_alias}">{$user->user_firstname} {$user->user_lastname}</a></h6>
                                                <p>{$user->mutual_friends_count} mutual friends</p>
                                                <div class="frnd_content_add">
                                                    + <span id="jFriend_{$user->user_id}" ><a href="#" class="jaddfriend" uid="{$user->user_id}">Add as friend</a></span>
                                                </div>
                                             </div>
                                        </div>
                                        {/foreach}
                                        {/if}
             
                                        <div class="clear"></div>
                                        <div class="seeall">
                                         </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="frnd_reqst_blk">
                                    <h3>Pending Requests</h3>
                                    <div class="frnd_reqst_blk_content">
                                        {if PageContext::$response->pendingRequest|@sizeof eq 0}
                                        No Pending Request Found
                                        {else}
                                        {foreach from=PageContext::$response->pendingRequest key=id item=user}
                                        <div class="frnds_list_blk">
                                            <div class="frnd_reqst_img">
                                                <a  href="{PageContext::$response->baseUrl}profile/{$user->user_alias}"><img src="{if $user->file_path eq ''}{PageContext::$response->userImagePath}thumb/member_noimg.jpg{else}{PageContext::$response->userImagePath}thumb/{$user->file_path}{/if}"></a>
                                            </div>
                                            <div class="frnd_content">
                                                <h6><a  href="{PageContext::$response->baseUrl}profile/{$user->user_alias}">{$user->user_firstname} {$user->user_lastname}</a></h6>
                                                <p>{$user->user_friends_count} friend</p>
                                                <div class="frnd_content_accept">
                                                    + <span><a href="{PageContext::$response->baseUrl}accept-invitation/{$user->invitation_id}/friend/0" id="{$user->user_id}">Accept</a></span>
                                                </div>
                                                <div class="frnd_content_decline">
                                                    x <span><a href="{PageContext::$response->baseUrl}accept-invitation/{$user->invitation_id}/reject/0" id="{$user->user_id}">Decline</a></span>
                                                </div>
                                            </div>
                                        </div>
                                        {/foreach}
                                        {/if}
                                        <div class="clear"></div>
                                        {if PageContext::$response->pendingRequest|@sizeof > 0}                 
                                        <div class="seeall">
                                            <a href="{PageContext::$response->baseUrl}pending-request">See All </a>
                                        </div>
                                        {/if}
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="frnd_reqst_blk ">
                                    <h3>Pending BizCom Requests</h3>
                                    <div class="frnd_reqst_blk_content">
                                        {if PageContext::$response->pendingBizcomrequests|@sizeof eq 0}
                                        No Pending Request Found
                                        {else}
                                        {foreach from=PageContext::$response->pendingBizcomrequests key=id item=community}
                                        <div class="frnds_list_blk profilebizcom_invite">
                                            <div class="frnd_reqst_img">
                                                <a  href="{PageContext::$response->baseUrl}community/{$community->community_alias}"><img src="{if $community->file_path eq ''}{PageContext::$response->userImagePath}thumb/noimage.jpg{else}{PageContext::$response->userImagePath}thumb/{$community->file_path}{/if}"></a>
                                            </div>
                                            <div class="frnd_content" >
                                                <span><h6><a  href="{PageContext::$response->baseUrl}profile/{$community->user_alias}">{if $community->user_firstname eq ''}{$community->user_email}{else}{$community->user_firstname} {$community->user_lastname}{/if} </a></h6> has	Invited  you to Join <h6><a  href="{PageContext::$response->baseUrl}community/{$community->community_alias}">{$community->community_name} </a></h6>  

                                                    <div class="frnd_content_accept">
                                                        + <span><a href="{PageContext::$response->baseUrl}accept-invitation/{$community->invitation_id}/community/0" id="{$community->user_id}">Accept</a></span>
                                                    </div>
                                                    <div class="frnd_content_decline">
                                                        x <span><a href="{PageContext::$response->baseUrl}accept-invitation/{$community->invitation_id}/reject/0" id="{$community->user_id}">Decline</a></span>
                                                    </div>
                                                </span></div>
                                        </div>
                                        {/foreach}
                                        {/if}
                                        <div class="clear"></div>
                                        {if PageContext::$response->pendingBizcomrequests|@sizeof > 0}                 
                                        <div class="seeall">
                                            <a href="{PageContext::$response->baseUrl}pending-bizcom-request">See All </a>
                                        </div>
                                        {/if}
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            {/if}
                            <!------------------End News Feed-------------------------------->
                            <!-----------------------My Profile-------------------------------->
                            <div class="recent_adddtion_blk content loader" id="tab-Profile">
                                <div id="msgDisplay"></div> 
                                <div id="jMyProfile">
                                    <img src="{PageContext::$response->userImagePath}default/loader.gif" /> Loading....
                                </div>
                                <div class="clear"></div>
                                <div id="jDivDisplayEdit" style="display:none">
                                    <div class="member_prof_abt">
                                        <div class="member_prof_abt_edit_table">
                                            {PageContext::$response->objForm}
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>	
                            </div>
                            <div class="clear"></div>
                            <!-------------------End My Profile-------------------------------->
                            <!--------------------Friends start------------------------------->
                            <div class="frnds_blk content loader" id="tab-Friends">
                                <img src="{PageContext::$response->userImagePath}default/loader.gif" /> Loading....
                                <!--            <div class="clear"></div> -->
                            </div>
                            <div class="frnds_blk content" id="tagg-business" style="display:none">
                                <div id="tagform"> 
                                    <h3>Business</h3>    
                                    <div class="bus_catprofileform">
										<div class="bus_prof_outer">
											<form name="serachbuisnessform" id="serachbuisnessform" action="" method="GET">
												<div class="bus_prof_inner">
													<label>Select Business</label>
													<input name="searchtext" id="serachbuisness" class="busname" type="text" placeholder="Business Name">
												</div>
												<div class="bus_prof_inner">
													<label>Role</label>
													<input name="role" id="role" class="rolename" type="text" placeholder="Role">
												</div>
												<div class="bus_prof_inner_1">
													<label>Email</label>
													<input name="email" id="email" class="" type="text" placeholder="Email">
													<input type="hidden" name="search_selected_activity_buisness" id="search_selected_activity_buisness" >
													<input type="submit" name="add" value="Add" class="invite_btn" title="invite-block" style="margin-top:0px!important;">
												</div>
												<input type="hidden" name="tagg_id" value="0" id="tagg_id">
											</form>
										</div>
                                    </div>  
                                </div>  
                                <div id="tagList">       
                                    {foreach from=PageContext::$response->tagDetails key=id item=lead}
                                    <div class="list_item_container" id="tag_list_{$lead->tagg_id}">
                                        <div class="bus_profile_blk business_profile_info">
                                            <div class="image"><a href="{PageContext::$response->baseUrl}page/{$lead->business_alias}">
                                                    <img src="{php} echo PageContext::$response->userImagePath;{/php}{if $lead->file_path neq ''}thumb/{$lead->file_path}{elseif $lead->file_path eq ''}thumb/noimage.jpg{/if}">
                                                </a>
                                            </div>
                                            <div class="label"> 
                                                <div class="bus_name_profile ">
                                                    <a id="business_{$lead->tagg_id}" href="{PageContext::$response->baseUrl}page/{$lead->business_alias}">{$lead->business_name}</a>
                                                    <br><span id="role_{$lead->tagg_id}">{$lead->role}</span>
                                                    <br><span id="email_id_{$lead->tagg_id}">{$lead->tag_buisness_email}</span> 
                                                    <div class="clear"></div>
                                                </div>

                                                <div class="left">
                                                    <a href="javascript:void(0)" class="delete_link_business" title="Delete" onclick="deleteTags('{$lead->tagg_id}','{$lead->business_id}')">Delete</a> 
                                                    <a href="javascript:void(0)" class="edit_link_businessprofile" title="Edit" onclick="updateTags('{$lead->tagg_id}','{$lead->business_id}')" style="margin-top:0px; ">Edit</a> 

                                                </div>

                                            </div>  
                                        </div>     
                                    </div>
                                    {/foreach} 
                                </div>
                                <div class="clear"></div> 
                            </div>
                            <div class="clear"></div>
                            <div class="listitem loader">
                                <img src="{PageContext::$response->userImagePath}default/loader.gif" /> Loading....
                            </div>
                            <!-----------------------Friends end--------------------------->
                            </div>
                            <div class="clear"></div>
                            </div>
