<div class="container">
    <div class="row"> 
        <div class="col-sm-12 col-md-12 col-lg-12">
<div id="o-wrapper" class="o-wrapper">
<!---Menu Link----------------- -->
{if PageContext::$response->sess_user_id >0}
<div class="main">
<div class="header-main">
     <main class="o-content">
        <div class="o-container">
          
          <div class="c-buttons">
            <button id="c-button--slide-right" class="button-nav-toggle"><span class="icon">&#9776;</span></button>
          </div>
          
          <div id="github-icons"></div>

        </div><!-- /o-container -->
      </main><!-- /o-content -->    
</div>
</div>  
{/if}
</div>
<!---EOF Menu Link----------------- -->
            <h3>BizCom Owners approvals for your accept and join requests<span class="round-search">{PageContext::$response->totalrecords}</span></h3>
            {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
          <div class="col-sm-12 col-md-12 col-lg-12">

        </div> 
        </div>
        
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="ibox-content">
                <div class="table-responsive">
                    {if PageContext::$response->totalrecords gt 0}
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Campaign Name</th>
                            <th>Community Name</th>
                            <th>PIF Sent Date</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=PageContext::$response->pifs key=id item=pifs}
                        <tr>
                            <td>{$pifs->refferalcampaign_title}</td>
                            <td>{$pifs->community_name}</td>
                            <td>{date('m/d/Y',strtotime($pifs->pif_sent_date))}</td>
                            <td>{if $pifs->cmember_status eq 'A'}Approved{/if}</td>
                        </tr>
                        {/foreach}
                        <div class="clear"></div>
                        </tbody>
                    </table>
                    {else}
                    No approvals found
                    {/if}
                </div>

            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12">
            {if PageContext::$response->totalrecords gt PageContext::$response->itemperpage}
            <div>{PageContext::$response->pagination} </div>
            {/if}
        </div>
    </div>
</div>