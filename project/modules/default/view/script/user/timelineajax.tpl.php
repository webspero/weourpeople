<?php foreach (PageContext::$response->news_feed1 as $feed) { ?>
    <div class="whitebox timelinebox" id="news_feed_<?php echo $feed['news_feed_id']; ?>">
        <div class="wid100per">
            <div class="postheadtablediv">
                <div class="post_left">
                <span class="mediapost_pic">
                    <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $feed['user_alias']; ?>">
                    <?php if ($feed['file_path'] != '') { ?>
                        <img class='timeline_feed_user_image'
                            src="<?php echo PageContext::$response->userImagePath; ?><?php echo $feed['file_path']; ?>">
                        <?php
                    } else { ?>
                        <img class='timeline_feed_user_image'
                            src="<?php echo PageContext::$response->userImagePath; ?>medium/member_noimg.jpg">
                    <?php } ?>
                        </a>
                </span>
                </div>
                <div class="post_right">
                    <div class="posthead">
                        <h4 class="media-heading">
                            <a href="#"><?php echo $feed['Username']; ?></a>
                            <span></span>
                        </h4>
                    </div>
                    <div class="postsubhead">
                        <div class="postsubhead_right"><span><?php
                                if ($feed['days'] > 0) {
                                    echo $feed['days'] . " days ago";
                                } else {
                                    $timesplit = explode(":", $feed['output_time']);
                                    if ($timesplit[0] > 0) {
                                        echo ltrim($timesplit[0], '0') . " hrs ago";
                                    } else {
                                        if ($timesplit[1] > 0) {
                                            echo ltrim($timesplit[1], '0');
                                            if ($timesplit[1] == 1) {
                                                echo "min";
                                            } else {
                                                echo "mins";
                                            }
                                            echo "ago";
                                        } else {
                                            echo "Just Now";
                                        }
                                    }
                                } ?>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="postsubhead_left">
                        <?php
                $reg_exUrl = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/';
                if(preg_match($reg_exUrl, $feed['news_feed_comment'],$url)){ ?>
                  
                <div class="jEmojiable"><?php echo preg_replace($reg_exUrl, "<a href='$url[0]'>$url[0]</a>" , $feed['news_feed_comment']);?></div>

                <?php }else{ ?>

                <div class="jEmojiable"><?php echo $feed['news_feed_comment'];?></div>

                <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <a href="#">
            <?php if ($feed['news_feed_image_name']) {
                $pimg = $feed['news_feed_image_name']; ?>
                <div class="postpic">
                    <!--                    <img class="img-responsive fancybox" src="<?php //echo PageContext::$response->userImagePath; ?>medium/<?php //echo $pimg; ?>">-->
                    <ul class="portfolio" class="clearfix main_gallary">
                        <li><a href="<?php echo PageContext::$response->userImagePath; ?>medium/<?php echo $pimg; ?>"
                               title=""><img
                                    src="<?php echo PageContext::$response->userImagePath; ?>medium/<?php echo $pimg; ?>"
                                    alt=""></a></li>
                    </ul>

                </div>
            <?php } ?>
        </a>
        <!----------------Statistics ------------------------>
        <?php if(PageContext::$response->sess_user_id > 0) { ?>
        <div class="mediapost_user-side">
            <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <div class="likesec">
                        <div class="display_table_cell pad10_right">
                            <a href="#"
                               class=" colorgrey_text jLikeNewsFeed <?php if ($feed['news_feed_num_like'] > 0) { ?> liked <?php } ?>"
                               id="jlike_<?php echo $feed['news_feed_id']; ?>"
                               cid="<?php echo $feed['news_feed_id'] ?>">
                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                <span id="jlikedisplay_<?php echo $feed['news_feed_id']; ?>">Like </span></a>
                        </div>
                        <div class="display_table_cell pad10_right">
                            <i class="fa fa-comments-o icn-fnt-size"></i>
                            <a href="#" id="jCommentButton_<?php echo $feed['news_feed_id']; ?>"
                               aid="<?php echo $feed['news_feed_id']; ?>" class="jNewsfeedCommentButton colorgrey_text">Comment</a>
                        </div>
                        <?php if ($feed['news_feed_user_id'] == PageContext::$response->sess_user_id) { ?>
                            <div class="display_table_cell pad10_right">
                                <i class="fa fa-trash-o icn-fnt-size"></i>
                                <a href="#" id="jDeleteButton_<?php echo $feed['news_feed_id']; ?>"
                                   aid="<?php echo $feed['news_feed_id']; ?>"
                                   class="jNewsfeedDeletetButton colorgrey_text">Delete</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8">
                    <div class="shareitemsblks">
                         <div id="feed_like_users_<?php echo $feed['news_feed_id'];?>" class="like_users_div" style=" display: none;">
                      <?php foreach(PageContext::$response->newsFeedLikeUsers[$feed['news_feed_id']] as  $key=>$val){?> 
                      <span><?php echo $val->news_feed_like_user_name;?><br></span>
                      <?php } ?> 
                    </div>
                        <?php $url_share = urlencode(PageContext::$response->baseUrl . 'newsfeed-detail/'); ?>
                        <span rel="tooltip" title="<?php echo $val->users;?> liked this comment" class="mediapost_links jShowFeedLikeUsers"
                              id="jcountlike_<?php echo $feed['news_feed_id']; ?>"><?php if ($feed['news_feed_num_like'] > 0) {
                                echo $feed['news_feed_num_like']; ?> Likes <?php } ?></span>
                        <span class="mediapost_links jNewsfeedCommentButton count_class"
                              aid="<?php echo $feed['news_feed_id']; ?>"
                              id="jcountcomment_<?php echo $feed['news_feed_id']; ?>"><?php if ($feed['news_feed_num_comments'] > 0) {
                                echo $feed['news_feed_num_comments']; ?>
                                Comments
                            <?php } ?>
                        </span>
                        <div class="fb-share-button"
                             data-href="<?php echo PageContext::$response->baseUrl; ?>newsfeed-detail/<?php echo $feed['news_feed_alias']; ?>"
                             data-layout="button_count" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore"
                                                                                     target="_blank"
                                                                                     href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url_share . $feed['news_feed_alias']; ?>src=sdkpreparse">Share</a>
                        </div>
                        <a href="<?php echo PageContext::$response->baseUrl; ?>newsfeed-detail/<?php echo $feed['news_feed_alias']; ?>"
                           title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                    </div>
                </div>
            </div>
        </div>
<?php } ?>
        <div class="mediapost_user-side-top clear jCommentDisplayDiv"
             id="jNewsfeedCommentBoxDisplayDiv_<?php echo $feed['news_feed_id']; ?>" style="display:none">
            <p class="lead emoji-picker-container">
            <div class="emotionsbtn_textblk">
                    <textarea class="form-control textarea-control jEmojiTextarea"
                              id="watermark_<?php echo $feed['news_feed_id']; ?>" rows="3"
                              placeholder="Write your comment here..."></textarea>
                <a class="upload_cambtn" id="<?php echo $feedlist->community_announcement_id; ?>">
                    <label for="file_<?php echo $feed['news_feed_id']; ?>"><i
                            class="fa fa-camera feed-camera"></i></label></a>
                <input type="file" id="file_<?php echo $feed['news_feed_id']; ?>"
                       aid="<?php echo $feed['news_feed_id']; ?>" cmid="<?php echo $feed['news_feed_comment_id']; ?>"
                       class="jfeedimageimage_file" style="cursor: pointer;  display: none"/>
                <input type="hidden" id="announcement_id" value="<?php echo $feedlist->community_announcement_id; ?>">
            </div>

            <div class="display_image_div">
                <div id="juploadcommentfeedimagepreview_<?php echo $feed['news_feed_id']; ?>"></div>
            </div>
            <div class="msg_display" id="msg_display_<?php echo $feed['news_feed_id']; ?>"></div>
            <div class="clearfix"></div>
            <a id="postButton" cid="<?php echo $feedlist->community_id; ?>" cmid=""
               aid="<?php echo $feed['news_feed_id']; ?>"
               class="btn yellow_btn fontupper pull-right jPostNewsfeedCommentButton"> Post</a>
        </div>
        <div class="clear commentblk jCommentDisplayDiv"
             id="jNewsfeedCommentDisplayDiv_<?php echo $feed['news_feed_id']; ?>" style="display:none">
            <div id="posting_<?php echo $feed['news_feed_id']; ?>">
                <!------------------------Comments-------------------------------->

                <?php if (count(PageContext::$response->newsFeed[$feed['news_feed_id']]) > 0) {
                   // echopre(PageContext::$response->newsFeed[$feed]);
                    foreach (PageContext::$response->newsFeed[$feed['news_feed_id']] as $key => $comments) {
                        if ($comments->news_feed_comments_id) {
                            ?>
                            <input type="hidden" id="val_<?php echo $feed['news_feed_id']; ?>"
                                   value="<?php echo count(PageContext::$response->newsFeed[$feed['news_feed_id']]); ?>">

                            <div class="col-md-12 btm-mrg pad10p" id="jdivComment_<?php echo $comments->news_feed_comments_id;?>">
                                <div class="row">
                                    <div class="col-md-1">
                                        <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $comments->user_alias; ?>">
                                            <?php if ($comments->user_image != '') { ?>
                                                <span class="mediapost_pic">
                                                <img class="ryt-mrg"
                                                     src="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->user_image; ?>">
                                            </span>
                                            <?php } elseif ($comments->user_image == '') { ?>
                                                <span class="mediapost_pic">
                                                <img class="ryt-mrg"
                                                     src="<?php echo PageContext::$response->userImagePath; ?>medium/member_noimg.jpg">
                                            </span>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="col-md-11">
                                        <span
                                            class="name"><?php echo $comments->Username; ?>
                                        </span>
                                        <span class="jEmojiable">
                                            <?php echo $comments->news_feed_comment_content; ?>
                                        </span>
                                        <br>
                                        <span class="new-text">
                                            <?php echo $comments->commentDate; ?>
                                        </span>
                                        <div class="commentimg_box">
                                            <?php if ($comments->file_path != '') { ?>
                                                <img class="fancybox"
                                                     src="<?php echo PageContext::$response->userImagePath; ?>medium/<?php echo $comments->file_path; ?>">
                                            <?php } ?>
                                        </div>
                                        <div class="mediapost_user-side1">
                                        <span class="mediapost_links"
                                              id="jcountfeedcommentlike_<?php echo $comments->news_feed_comments_id; ?>"><?php if ($comments->num_comment_likes > 0) {
                                                echo $comments->num_comment_likes; ?> Likes <?php } ?></span>
                                            <input type="hidden"
                                                   id="reply_<?php echo $comments->news_feed_comments_id; ?>"
                                                   value="<?php if ($comments->num_replies > 0) {
                                                       echo $comments->num_replies;
                                                   } else {
                                                       echo "0";
                                                   } ?>">
                                            <div class="mediapost_links "
                                                 cid="<?php echo $comments->announcement_comment_id; ?>"
                                                 id="jcountreply_<?php echo $comments->news_feed_comments_id; ?>">
                                                <a href="#" class="jShowFeedReply"
                                                   id="<?php echo $comments->news_feed_comments_id; ?>">
                                                <span
                                                    id="jcountreply_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"><?php if (PageContext::$response->newsFeed[$feed['news_feed_id']][$comments->news_feed_comments_id] > 0) {
                                                        echo PageContext::$response->newsFeed[$feed['news_feed_id']][$comments->news_feed_comments_id] . " Replies";
                                                    } ?>
                                                </span>
                                                </a>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="comment_sharelike_sec">
                                            <?php if ($comments->news_feed_comment_user_id == PageContext::$response->sess_user_id) { ?>
                                                <a href="#" class="jNewsFeedCommentDelete marg10right"
                                                   aid="<?php echo $comments->news_feed_id; ?>"
                                                   id="<?php echo $comments->news_feed_comments_id; ?>"><i
                                                        class="fa fa-times"></i> Delete</a>
                                            <?php } ?>
                                            <a href="#" class="jFeedCommentReply marg10right"
                                               aid="<?php echo $comments->news_feed_id; ?>"
                                               cid="<?php echo $comments->news_feed_comments_id; ?>"
                                               id="<?php echo $comments->news_feed_comments_id; ?>"><i
                                                    class="fa fa-reply"></i> Reply</a>
                                            <a href="#"
                                               class="jNewsFeedCommentLike <?php if ($comments->num_comment_likes > 0) { ?> liked <?php } ?>"
                                               id="jlikeComment_<?php echo $comments->news_feed_comments_id; ?>"
                                               cid="<?php echo $comments->news_feed_comments_id; ?>"
                                               aid="<?php echo $comments->news_feed_id; ?>">

                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                            <span
                                                id="jlikedisplaycomment_<?php echo $comments->announcement_comment_id; ?>">Like </span></a>
                                        </div>
                                        <div id="jdivReply_<?php echo $comments->news_feed_comments_id; ?>"
                                             class="jDisplayReply">
                                            <div class="emotionsbtn_textblk">
                                                <textarea placeholder="Write your reply here..."
                                                          class="form-control textarea-control jEmojiTextarea"
                                                          style="height:35px;"
                                                          id="watermark_<?php echo $comments->news_feed_id; ?>_<?php echo $comments->news_feed_comments_id; ?>"
                                                          class="watermark" name="watermark">
                                                </textarea>
                                                <a class="upload_cambtn"
                                                   id="<?php echo $comments->news_feed_id; ?>">
                                                    <label for="imagereply_file_<?php echo $comments->news_feed_comments_id; ?>"><i
                                                            class="fa fa-camera feed-reply-camera"></i></label>
                                                    <div class="file_button">
                                                        <input type="file"
                                                                id="imagereply_file_<?php echo $comments->news_feed_comments_id; ?>"
                                                                cid="<?php echo $comments->news_feed_comments_id; ?>"
                                                                class="jfeedimagereply_file"
                                                                style="display:none;"/>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="loader"
                                             id="imagereply_loader_<?php echo $comments->announcement_comment_id; ?>">
                                            <img
                                                src="<?php echo PageContext::$response->userImagePath; ?>default/loader.gif"/>
                                        </div>
                                        <div
                                            id="jDivReplyImagePreview_<?php echo $comments->news_feed_comments_id; ?>"></div>
                                        <div id="postingReply_<?php echo $comments->news_feed_comments_id; ?>"></div>
                                    </div>
                                </div>

                            </div>
                        <?php }
                    }
                }
                ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
<?php } ?>