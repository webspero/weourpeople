<div class="container">
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515e556d6259a54e" async="async"></script>
<div class="whitebox marg20col">
<div class="addthis_native_toolbox"></div>
    <div class="row"> 

        <div class="col-sm-12 col-md-12 col-lg-12">

        <div class ="joinMessage" id="joinMessage"></div>
            <h3>My Subscribed Groups <span class="round-search">{PageContext::$response->totalrecords}</span> </h3>
        </div>

        <section class="searchbar">

            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="businesslist_search_blk">
                    <div class="row"> 
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <input type="hidden" value="{PageContext::$response->sess_user_id}" class="login_status" name="" id="login_status">
                            Sort by : A-Z <a href="{php} echo PageContext::$response->baseUrl; {/php}my-subscribed-group?sort=community_name&sortby={php} echo PageContext::$response->order; {/php}&searchtext={php} echo PageContext::$response->searchtext; {/php}&searchtype={php} echo PageContext::$response->searchtype; {/php}&page={php} echo PageContext::$response->page; {/php}">{PageContext::$response->community_name_sortorder}</a>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 pull-right">
                            {php}PageContext::renderRegisteredPostActions('searchbox');{/php}
                        </div>
<!--                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 pull-right">
                        <select class="form-control" placeholder="Category" name="community_category" id="sub_community_category" value="" >
	                                    {foreach from=PageContext::$response->categories key=category item=id}
	                                        <option value="{$id}" {if PageContext::$response->cat_id eq $id} selected="selected" {/if}>{$category}</option>
	                                    {/foreach}

	                </select>
                        </div> -->
<!--                        <div class="col-xs-12 col-lg-2 col-md-3 col-lg-3 pull-right">
                        <select class="form-control" placeholder="Status" name="active_status" id="active_status" value="" >
                            <option value="">--Status--</option>
                            <option value="A" {if PageContext::$response->active_status eq 'A'} selected="selected" {/if} >Active</option>
                            <option value="P" {if PageContext::$response->active_status eq 'P'} selected="selected" {/if} >Passive</option>
	                </select>
                        </div> -->
                    </div>
                </div>
                {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
            </div>
        </section>
        <div class="clearfix"></div>
    </div>
 {if PageContext::$response->totalrecords > 0}
    <div class="row masonry"> 
        <div class="col-sm-12 col-md-12 col-lg-12">
       
             
              <section class="bizcomlisting subscribe_listing">
          
            {assign var="i" value=PageContext::$response->slno}
            {foreach from=PageContext::$response->leads key=id item=lead}
            <div class="item">
                <div class="mediapost well {if $lead->cmember_active_status eq 'A'} active_user_box {/if}" id='div_active_passive_{$lead->community_id}'>
                    <!--
                    <div class="pull-right">
                        <span>
                            <a href="#" id="{$lead->community_id}" class="show_descriptive"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a>
                        </span>
                    </div>
                    -->
                    <div class="wid100per">
                        <span class="mediapost_pic">
                            <a href="{php} echo PageContext::$response->baseUrl;{/php}group/{$lead->community_alias}">
                                <div class="mediapostpicblk" style="background-image: url('{php} echo PageContext::$response->userImagePath;{/php}{if $lead->file_path neq ''}medium/{$lead->file_path}{elseif $lead->file_path eq ''}default/no_preview_med.jpg{/if}');">
                                </div>
                            </a>
                        </span>
                    </div>

                    <div class="wid100per padding_left_10 media-body">
                        <h4 class="media-heading"><span><a href="{php} echo PageContext::$response->baseUrl;{/php}group/{$lead->community_alias}">{$lead->community_name}({$lead->business_name})</a></span></h4>
                        {if $lead->community_status =='I'}   
                        <span class="status clserror" >Pending Approval</span> 
                        {else}
                        <div class="display_table">
                            <div class="display_table_cell wid60per">
                                <div id="starring">
                                    <div class="rateit" id="rateit_{$lead->ratetype_id}" data-rateit-value="{$lead->ratevalue}" data-rateit-ispreset="true" data-rateit-id="{$lead->rateuser_id}" data-rateit-type="{$lead->ratetype}" data-rateit-typeid="{$lead->ratetype_id}" data-rateit-flag="{$lead->rateflag}" data-rateit-step=1 data-rateit-resetable="false">
                    </div>
                                    <div class="right">
                                        {if PageContext::$response->sess_user_id eq $lead->community_created_by}    
                                        <a href="{php} echo PageContext::$response->baseUrl;{/php}edit-group{$lead->community_id}" class="edititem left marg5right"><i class="fa fa-pencil"></i></a>
                                        <a href="{php} echo PageContext::$response->baseUrl;{/php}index/delete_community/{$lead->community_id}" class="deleteitem left"><i class="fa fa-trash"></i></a> 
                                        {/if}
                                    </div>
                                </div>
                            </div>

                        </div>
                        {/if}
                        <div class="referral_blk text-center margtop15" id="descriptive_div_{$lead->community_id}">
                        <p>
                            {$lead->community_description|substr:0:200}....
                        </p>
                        
<!--                            <div class="referral_blk text-center margtop15">
                                <div id="jRefDiv_{$lead->community_id}" >
                                    <a href="#" class="jShowReferralOptions referral_btn btt_full_width" bid="{$lead->community_id}">
                                    <i class="fa fa-gift referral_btn_icon"></i>
                                    Referral offer</a>
                                    <div class="clear"></div>
                                </div>
                                
								<div class="clear"></div>
                                <div class="jReferralOptionsClass referraltype_blk choose_referral_outer" id="jDivdisplayReferralOptions_{$lead->community_id}" style="display:none;">
                                    <h4>Please choose the referral type</h4>
                                    <ul>
                                        <li class="new-size"><input type="radio" name="RefferalType_{$lead->community_id}" value="1" class="radio-mrg" > Goodwill</li>
                                        <li class="new-size"><input type="radio" name="RefferalType_{$lead->community_id}" value="2" class="radio-mrg" > Financial Incentive Based Referral</li>
                                        <li class="new-size"><input type="radio" name="RefferalType_{$lead->community_id}"  value="3" class="radio-mrg"> Charitable Donation Based Referral</li>
                                        <li class="new-size"><input type="radio" name="RefferalType_{$lead->community_id}" value="4" class="radio-mrg" > Voucher or Product Based Referral</li>
                                        <textarea name="personal_message_{$lead->community_id}" placeholder="Personal Message"></textarea>
                                        <li class="new-size text-center"><input type="button" class="jClickReferralOffer sendref_offr_btn btt_full_width"  bid="{$lead->community_id}" uid="{$lead->community_created_by}" value="Send Referral Offer"></li>
                                    </ul>
                                </div>
                                
                            </div>-->
                            <div class="clearfix"></div>
<!--                            <div class="referral_blk text-center margtop15">
                         <span class="active_passive"  {if $lead->cmember_active_status neq 'P' || PageContext::$response->sess_user_id eq $lead->community_created_by} style="display:none;"{/if}> 
                    <a href="#" style="color: #0000FF;" onclick="make_active_passive({$lead->community_id},'Active',{PageContext::$response->sess_user_id})">
                        <input type="button" name="active_passive" value="Make Me Active" class="btn yellow_btn marg7col fontupper center-div btt_full_width" title="invite-block">
                    </a>
                </span>
                <span class="passive_active" {if $lead->cmember_active_status neq 'A' || PageContext::$response->sess_user_id eq $lead->community_created_by} style="display:none;"{/if}>
                    <a href="#" style="color: #0000FF;" onclick="make_active_passive({$lead->community_id},'Passive',{PageContext::$response->sess_user_id})">
                        <input type="button" name="passive_active" value="Make Me Passive" class="btn yellow_btn marg7col fontupper center-div btt_full_width" title="invite-block">
                    </a>
                </span>
                
                </div>-->
                            <div class="referral_blk text-center margtop15">
                                <span class="passive_active">
                    <a href="#" style="color: #0000FF;" onclick="DisjoinCommunity({$lead->community_id},'decline')">
                        <input type="button" name="invite" value="Unsubscribe" class="btn yellow_btn fontupper btt_full_width" title="invite-block">
                    </a>
                </span>
                            </div>
                        <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            {assign var="i" value=$i+1}
            {/foreach}  
             
        </section>
             
    </div>
    <!--<div class="clear"></div>-->
</div>
 <div class="clear"></div>
  {else}
    <br>
    <div>{if PageContext::$response->searchtype eq ''}No groups added/joined yet.{else}No matches found.{/if} </div>
    {/if}
<div class="row">
    {if PageContext::$response->totalrecords gt PageContext::$response->itemperpage}
    <div class="col-sm-12 col-md-12 col-lg-12">{PageContext::$response->pagination} </div>
    {/if}
</div>


</div>
</div>