<div class="container">
    <div class="row"> 
<!--        <div class="col-sm-12 col-md-12 col-lg-12 ">
            <div class="main">
            <div class="header-main">-->
                <div class="{PageContext::$response->messagebox['msgClass']}">{PageContext::$response->messagebox['msg']}</div>
<!--                <div class="page-wrapper">
                  <button class="button-nav-toggle"><span class="icon">&#9776;</span></button>
                </div>
            </div>
            </div> 
        </div>-->
        
        <div class="col-md-offset-3 col-lg-offset-3 col-sm-12 col-md-6 col-lg-6 ">
            <div class="whitebox marg40col pad25px_left pad25px_right">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Change Password</h3>
                    </div>
                </div>
                <div class="form-bottom signup_form">
                    <div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
                    <form name="frmChangePassword" id="frmChangePassword" method="Post">
                        <div class="form-group relative marg0btm">
                            <input required type="password" placeholder="Current Password" class="form-control" name="user_password" value="">
                            <label for="paypal_email" class="error" generated="true"></label>
                        </div>
                        <div class="form-group relative marg0btm">
                            <input required type="password" placeholder="New Password" class="form-control" name="user_newpassword" id="user_new_password" value="">
                            <label for="paypal_email" class="error" generated="true"></label>
                        </div>
                        <div class="form-group relative marg0btm">
                            <input required type="password" placeholder="Re-enter Password" class="form-control" name="user_confirm_password" value="">
                            <label for="paypal_email" class="error" generated="true"></label>
                        </div>
                        <div class="form-group relative">
                            <input type="submit" name="btnSubmit" value="Change Password" class="btn btn-primary yellow_btn2">
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
<!--            <div class="content content_changepassword">
            <h3>Change Password</h3>
            <div class="login_form">
                <div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
                {PageContext::$response->objForm}
            </div>
            </div>-->
        </div>

    </div>
    <div class="clear"></div>
</div>
