<!-- <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL?>project/themes/default/css/style.css"> -->
<link type="text/css" rel="stylesheet" href="<?php echo BASE_URL?>project/styles/popup.css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery-1.8.0.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>bbf/public/js/jquery-ui-1.8.23.custom.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery.blockUI.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/fw.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.metadata.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.validate.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/app.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.colorbox.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/comment_popup.js"></script>
<!-- <script type="text/javascript"> 
    $(document).ready(function() {
    $('#frmProfile').submit(function(e){
  
        e.preventDefault();
        var user_id      = $("#user_id").val();
        var file      = $("#file").val();
        var image_submit      = $("#image_submit").val();
       
        var dataString          = 'user_id='+user_id+"&file="+file+"&image_submit="+image_submit;
        alert(file);
        $.ajax({
            type    : "POST",
            dataType: 'json',
            url     : "<?php echo BASE_URL;?>index/addProfilePic",
            data    : dataString,
            cache   : false,
            success : function(data){ 
               if(data.status=='SUCCESS')
                   { 
                       //window.parent.$("#login_status").val(data.data.user_id);
                      
                        parent.location.reload();
                       $.colorbox.close();
                      
                   }else{
                      
                       $("#msg").html("Upload Failed");
                       $("#msg").addClass('error_div');
                   }
              
            }
        });
        
     });
     
      });
 </script> -->
<script type="text/javascript">
$(document).ready(function() {
	if($("#filename").val() != ""){
		//alert($("#filename").val());
		window.parent.$("#profilepic img").attr("src","<?php echo BASE_URL;?>project/files/medium/"+ $("#filename").val());
		}
$("#cancel_upload").live("click",function(){
	parent.location.reload();
	 $.colorbox.close();
	// $(".member_profile_picblk img").attr("src",mainUrl + "project/files/"+ $("#filename").val());
});

});
</script> 
<div class="content_comment">
        <div class="" id="msg"></div>
        <h3>Upload Image</h3>
        <div class="<?php echo PageContext::$response->message['msgClass'];?>"><?php echo PageContext::$response->message['msg'];?></div>
        <div class="register_form">
           <form action="<?php echo BASE_URL;?>user/communityprofilechange" id="frmCommunityProfile" method="post" enctype="multipart/form-data">
            <input type="hidden" value="<?php echo PageContext::$response->file; ?>" id="filename" name="filename">
           <input type="hidden" value="<?php echo PageContext::$response->community_id; ?>" id="community_id" name="community_id">
           <input type="file" id="file" name="file">
            <div class="clearfix"></div>
           	<div class="member_profile_pic">
           <?php if(PageContext::$response->file != ''){ ?>           	              
                <img src="<?php echo BASE_URL;?>project/files/thumb/<?php echo PageContext::$response->file;?>" >                             
           <?php }?>
           </div>
             <div class="clearfix"></div>
           <input type="submit" value="Save Image" class="yellow_btn" id="image_submit" name="image_submit">   
<!--             <input type="button" value="Close" class="cancel_btn" id="cancel_upload" name="cancel_upload">  -->
           </form>
        </div>
        
      
    </div>   
    <div class="clear"></div>
    
  