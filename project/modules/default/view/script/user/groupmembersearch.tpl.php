<style>
.grplisting .mediapost, #member_container_div .mediapost, #member_container_div .mediapost, #tagged_people .mediapost {
    padding-bottom: 0 !important;
}

</style>

<?php if(PageContext::$response->members) { ?>
<?php foreach(PageContext::$response->members As $members){?>

<div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="mediapost">
                        <div class="picpost_left pull-left">
                            <span class="picpost_left_pic">
                                <img src="<?php if ($members->file_path == '') {
                                    echo PageContext::$response->userImagePath . 'member_noimg.jpg';
                                } else {
                                    echo PageContext::$response->userImagePath .  $members->file_path;
                                } ?>">
                            </span>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl . 'profile/' . $members->user_alias; ?>"><?php echo ($members->user_firstname != '') ? $members->user_firstname . " " . $members->user_lastname : $members->user_email; ?> </a></h4>
                            <p><?php echo $members->user_friends_count; ?>  friends</p>
<!--                            <p>
                                <span class="accept">+ <a href="javascript:void(0)" onclick="changeCommunitMemberStatus(<?php echo $members->community_id; ?>,<?php echo $members->user_id; ?>,'accept')">Accept</a></span> 
                                <span class="reject">x <a href="javascript:void(0)" onclick="changeCommunitMemberStatus(<?php echo $members->community_id; ?>,<?php echo $members->user_id ?>,'decline')">Decline</a></span>

                            </p>-->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

<?php }}else{?>
<div class="frnds_list_blk" style="padding: 10px;"> No matches </div>
<?php } ?>
<div class="clear"></div>

<input type="hidden" id="membercount" value="<?php echo PageContext::$response->membercount;?>">