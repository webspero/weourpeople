<div class="content_left pad_cont_right findfriend">
      <h3>Classifieds - Categories <span class="round-search">{PageContext::$response->list->totalrecords}</span> </h3>
      
    <div class="find_friends_list_outer">
<!--         <h3>Categories({PageContext::$response->list->totalrecords})</h3> -->

        <div class="find_friends_list_blk">
        {if PageContext::$response->list->totalrecords > 0}
            {foreach from=PageContext::$response->list1 key=id item=category}            
            <div class="categorylist_row">            
            <div class="cateforylist_blk"> 
            {if isset($category[0])}
<!--                 <div class="colside1"> -->
                   <a href="{PageContext::$response->baseUrl}classifieds-list/{$category[0]->category_alias}">
				   	<div class="cateforylist_items">
				   		<div class="left">{$category[0]->category_name}</div>
				   		<div class="categ_count">{$category[0]->cnt}</div>
				   	</div>
				   </a>
                  {/if} 
                </div>                               
                <div class="cateforylist_blk">
                 {if isset($category[1])} 
                   <a href="{PageContext::$response->baseUrl}classifieds-list/{$category[1]->category_alias}">
				   <div class="cateforylist_items">
				   		<div class="left">{$category[1]->category_name}</div>
				   		<div class="categ_count">{$category[1]->cnt}</div>
				   </div>
				   </a>
                  {/if} 
                </div>                           
                <div class="cateforylist_blk"> 
                  {if isset($category[2])}
<!--                 <div class="colside1"> -->
                   <a href="{PageContext::$response->baseUrl}classifieds-list/{$category[2]->category_alias}">
				    <div class="cateforylist_items">
				   		<div class="left">{$category[2]->category_name}</div>
				   		<div class="categ_count">{$category[2]->cnt}</div>
				   </div>
				   </a>
                 {/if}
                </div>
                
<!--                 <div class="colmiddle">                   -->
<!--                 </div> -->
<!--                 <div class="colside3">                    -->
<!--                 </div> -->
            </div>
            {/foreach}   
            {else}
            <div class="categorylist_row rownoborder">{if PageContext::$response->searchtype eq ''}No Classifieds are available.{else}No matches found.{/if}
            {if PageContext::$response->sess_user_id neq 0 and PageContext::$response->sess_user_status neq 'I'}Click to <a href="{php} echo PageContext::$response->baseUrl;{/php}add-classifieds">post a classified</a>. {/if}
            </div>
            {/if}         
            <div class="clear"></div>          
        </div>

    </div>

    <div class="clear"></div>
    
</div>