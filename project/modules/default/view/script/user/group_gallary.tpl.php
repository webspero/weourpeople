<script>
    var TOTAL_PAGES = <?php echo PageContext::$response->totalPages?>;
    $(document).ready(function () {
        
        
//        var fancyBoxFn = function(){
//            var addToAll = false;
//            var gallery = true;
//            var titlePosition = 'inside';
//            $(addToAll ? 'img' : 'img.fancybox').each(function () {
//                var $this = $(this);
//                var title = $this.attr('title');
//                var src = $this.attr('data-big') || $this.attr('src');
//                var a = $('<a href="#" class="fancybox"></a>').attr('href', src).attr('title', title);
//                $this.wrap(a);
//            });
//            if (gallery)
//                $('a.fancybox').attr('rel', 'fancyboxgallery');
//            $('a.fancybox').fancybox({
//                titlePosition: titlePosition,
//                showNavArrows: true
//            });
//        };
//        fancyBoxFn();

//        var magnificPopupFn = function(){
//          $('#portfolio').magnificPopup({
//            delegate: 'a',
//            type: 'image',
//            image: {
//              cursor: null,
//              titleSrc: 'title'
//            },
//            gallery: {
//              enabled: true,
//              preload: [0,1], // Will preload 0 - before current, and 1 after the current image
//              navigateByImgClick: true
//                        }
//          });
//        };
        magnificPopupFn();


        var page = 1;
        var _throttleTimer = null;
        var _throttleDelay = 100;
        $(window).scroll(function () {
            clearTimeout(_throttleTimer);
            _throttleTimer = setTimeout(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                    if (page < TOTAL_PAGES) {
                        page++;
                        $.blockUI({message: ''});
                        var community_id = $("#community_id").val();
                        $.ajax({
                            type: 'Post',
                            url: mainUrl + 'user/group_gallaryajax/' + page + '/' + community_id,
                            data: 'community_id=' + community_id,
                            success: function (data) {
                                $('.main_gallary').append(data);
                                magnificPopupFn();
                                $('.listitem').fadeIn(3000);
                                $('.listitem').hide();
                            }
                        })
                    }
                }
            }, _throttleDelay);
        });
    });
    
//    $('.thumbnail').click(function(){
//    $('.modal-body').empty();
//    var title = $(this).parent('a').attr("title");
//    $('.modal-title').html(title);
//    $($(this).parents('div').html()).appendTo('.modal-body');
//    $('#myModal').modal({show:true});
//});
</script>
<div class="row">
<div class='col-sm-12 col-md-12 col-lg-12'>
<div class="whitebox">
     <?php
     if (count(PageContext::$response->imageDetails) > 0) { ?>
        <ul id="portfolio" class="clearfix main_gallary">
        <?php foreach (PageContext::$response->imageDetails As $images) {
            if ($images->community_announcement_image_path != '') {?>
                <li><a href="<?php echo PageContext::$response->userImagePath . $images->community_announcement_image_path;?>" title="<?php echo $images->community_announcement_title;?>"><img src="<?php echo PageContext::$response->userImagePath.$images->community_announcement_image_path;?>" alt="<?php echo $images->community_announcement_title;?>"></a></li>
            <?php
            }
        }
        ?>
        </ul>
    <?php
    } else {
        ?>
        <div class="frndslisting_blk_content">
            <span class="margin_l_20">No Images Found</span>
        </div>
        <?php
    }
    ?>
    <input type="hidden" id="community_id" value="<?php echo PageContext::$response->communityId; ?>">
</div>
</div>
</div>


<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <!--<h3 class="modal-title">Heading</h3>-->
    </div>
    <div class="modal-body">
        
    </div>
    <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
   </div>
  </div>
</div>