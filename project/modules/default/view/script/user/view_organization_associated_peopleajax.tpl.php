<?php
foreach (PageContext::$response->associates AS $id => $user) {
    ?>

    <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="mediapost">
            <div class="picpost_left pull-left">
                <span class="picpost_left_pic">
                    <a href="<?php echo PageContext::$response->baseUrl . 'timeline/' . $user->user_alias ?>">
                        <img
                            src="<?php echo ($user->file_path == '') ? PageContext::$response->userImagePath . 'thumb/member_noimg.jpg' : PageContext::$response->userImagePath . 'medium/' . $user->file_path; ?>">
                    </a>                                        
                </span>
            </div>
            <div class="media-body">
                <h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl . 'timeline/' . $user->user_alias ?>"><?php echo $user->user_firstname . ' ' . $user->user_lastname ?></a>
                </h4>
                <p><?php echo $user->role ?></p>
               
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
<?php } ?>
<!--            <div class="clear"></div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">-->
<!--<div><?php // echo PageContext::$response->paginationAffilate; ?> </div>-->
<?php // }?>
<!--            </div>
        </div>
   </div>-->
<div class="clear"></div>