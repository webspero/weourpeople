<div class="whitebox" id="org_feed_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>">
    <div class="wid100per">
        <div class="postheadtablediv">
            <div class="post_left">
            <span class="mediapost_pic">
                <?php if (PageContext::$response->news_feed->file_path) { ?>
                    <img
                        src="<?php
                        echo PageContext::$response->userImagePath;
                        echo PageContext::$response->news_feed->file_path;
                        ?>">
                <?php } else { ?>
                    <img
                        src="<?php
                        echo PageContext::$response->userImagePath;
                        ?>medium/member_noimg.jpg">
                <?php } ?>
            </span>
            </div>
            <div class="post_right">
                <div class="posthead">
                    <h4 class="media-heading">
                        <a href="#"><?php
                            echo PageContext::$response->news_feed->user_firstname . ' ' . PageContext::$response->news_feed->user_lastname; ?>
                        </a>
                    </h4>
                </div>
                <div class="postsubhead">
                    <div class="postsubhead_right">
                    <span>Just Now
                    </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="postsubhead_left">
                <?php
                $reg_exUrl = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/';
                if(preg_match($reg_exUrl, PageContext::$response->news_feed->organization_announcement_content,$url)){ ?>
                  
                <div class="jEmojiable"><?php echo preg_replace($reg_exUrl, "<a href='$url[0]'>$url[0]</a>" , PageContext::$response->news_feed->organization_announcement_content);?></div>

                <?php
                
                }else{ ?>

                <div class="jEmojiable"><?php echo PageContext::$response->news_feed->organization_announcement_content;?></div>

                <?php } ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <a href="#"><?php if (PageContext::$response->news_feed->organization_announcement_image_path) { ?>
            <div class="postpic">
                <ul class="portfolio" class="clearfix">
                    <li>
                        <a href="<?php
                        echo PageContext::$response->userImagePath;
                        echo PageContext::$response->news_feed->organization_announcement_image_path;
                        ?>" title="<?php echo PageContext::$response->news_feed->organization_announcement_content;?>">
                            <img src="<?php
                            echo PageContext::$response->userImagePath;
                            echo PageContext::$response->news_feed->organization_announcement_image_path;
                            ?>" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        <?php }
        ?>
    </a>
    
    <div class="mediapost_user-side">
                <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-5">
                    <div class="likesec">
                    <div class="display_table_cell pad10_right">
                    <a href="#" class=" colorgrey_text jLikeFeedOrganization <?php if($feed['LIKE_ID'] > 0){ ?> liked <?php }?>" id="jlike_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" 
                       aid="<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" cid="<?php echo PageContext::$response->news_feed->organization_id;?>">
                        <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                        <span id="jlikedisplay_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>">Like </span></a>
                    </div>
                     <div class="display_table_cell pad10_right">
                        <i class="fa fa-comments-o icn-fnt-size"></i>
                    <a href="#" id="jCommentButton_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" aid="<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" class="jOrgCommentButton colorgrey_text">Comment</a>
                    </div> 
                        <?php if(PageContext::$response->news_feed->organization_announcement_user_id == PageContext::$response->sess_user_id){?> 
                    <div class="display_table_cell pad10_right">
                        <i class="fa fa-trash-o icn-fnt-size"></i>
                    <a href="#" id="jDeleteButton_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" aid="<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" class="jOrgDeletetButton colorgrey_text">Delete</a>
                    </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-7">
                    <div class="sharefnt">
                    <span class="mediapost_links jShowOrgLikeUsers" id="jcountlike_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>"><?php if(PageContext::$response->news_feed->organization_announcement_num_likes > 0){ echo PageContext::$response->news_feed->organization_announcement_num_likes." Likes";} ?></span>
                    <span class="mediapost_links jOrgCommentButton count_class"  aid ="<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" id="jcountcomment_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>"><?php if(PageContext::$response->news_feed->organization_announcement_num_comments > 0){ echo PageContext::$response->news_feed->organization_announcement_num_comments." Comments";
                         
                    } ?>
                    </span>
                    <div class="fb-share-button" id="share_button_<?php echo PageContext::$response->news_feed->news_feed_id;?>" data-href="<?php echo PageContext::$response->baseUrl;?>announcement-detail/<?php echo PageContext::$response->news_feed->organization_announcement_alias;?>" data-title ="Shared on facebook" data-layout="button_count" data-desc="You can share data" data-url="<?php echo PageContext::$response->baseUrl;?>newsfeed-detail/<?php echo PageContext::$response->news_feed->news_feed_alias;?>"></div>
                    <a href="<?php echo PageContext::$response->baseUrl;?>announcement-detail/<?php echo $announcement['organization_announcement_alias'];?>" title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                    
                    </div>
                </div>
                </div>
                <div class="clearfix"></div>
           </div>

            <!---------------Referral offer-------------------------------->
<!--                                                <div class="jReferralOptionsClass referraltype_blk" id="jDivdisplayReferralOptions_{$announcement->community_id}_{$announcement->community_announcement_id}" style="display:none;">
                <h4>Please choose the preferred referral type</h4>
                <ul>
                    <li><input type="radio" name="RefferalType_{$announcement->community_id}" value="1" > Free Referral</li>
                    <li><input type="radio" name="RefferalType_{$announcement->community_id}" value="2" > Financial Incentive Based Referral</li>
                    <li><input type="radio" name="RefferalType_{$announcement->community_id}"  value="3"> Charitable Donation Based Referral</li>
                    <li><input type="radio" name="RefferalType_{$announcement->community_id}" value="4" > Voucher or Product Based Referral</li>
                    <li><input type="button" class="jClickFeedReferralOffer sendref_offr_btn" aid="{$announcement->community_announcement_id}"  bid="{$announcement->community_id}" uid="{$announcement->community_created_by}" value="Send Referral Offer"></li>
                </ul>
            </div>
-->                                               
            <div class="mediapost_user-side-top clear jCommentDisplayDiv" id="jCommentBoxDisplayDiv_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" style="display:none">
                <p class="lead emoji-picker-container position_relatv">
                    <div class="emotionsbtn_textblk">
                        <textarea class="form-control textarea-control jEmojiTextarea" id="watermark_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" rows="3" placeholder="Write your comment here..."></textarea>
                    </div>
                    <a class="upload_cambtn" id="<?php echo PageContext::$response->news_feed->organization_announcement_id;?>">
                        <label for="file_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" ><i class="fa fa-camera feed-camera"></i></label></a>
                    <input type="file" id="file_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" aid="<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" class="jorgimage_file" style="cursor: pointer;  display: none"/>
                    <input type="hidden" id="organization_announcement_id" value="<?php echo PageContext::$response->news_feed->organization_announcement_id;?>">
                </p>
                <div class="msg_display" id="msg_display_<?php echo PageContext::$response->news_feed->announcement_comment_id;?>"></div>
										 	<div class="clearfix"></div>
                <a id="postButton" ondblclick="this.disabled=true;" cid="<?php echo PageContext::$response->news_feed->organization_id;?>" cmid="<?php echo PageContext::$response->news_feed->announcement_comment_id;?>" aid="<?php echo PageContext::$response->news_feed->organization_announcement_id;?>"  class="btn yellow_btn fontupper pull-right jPostCommentButtonOrganization"> Post</a>
            </div>
            <div class="loading" id="loading_<?php echo $feedlist['community_announcement_id'];?>" style="display: none;">
                <img src="<?php echo PageContext::$response->userImagePath;?>default/loader.gif" />
            </div>
            <div id="juploadimagepreview_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>"></div>
            <div class="clear commentblk jCommentDisplayDiv" id="jCommentDisplayDiv_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>" style="display:none">
                <div id="posting_<?php echo PageContext::$response->news_feed->organization_announcement_id;?>">
                    <!------------------------Comments-------------------------------->
                    <?php $val=0;
                                            
                    foreach(PageContext::$response->newsFeed[$feed['organization_announcement_id']] as $key=>$comments){ 
                        if($comments->organization_announcement_comment_id){
                        ?>
                    <input type="hidden" id="val_<?php echo $feed['organization_announcement_id'];?>" value="<?php echo count(PageContext::$response->newsFeed[$feed['organization_announcement_id']]);?>">
                       <?php  $val = $val+1; ?>
                           <input type="text" id="hid_announcement_id_<?php echo $comments->organization_announcement_comment_id;?>" class="hid_announcement_id" value="<?php echo $feed['organization_announcement_id'];?>" style="display: none;" >   
                            <input type="text" id="hid_<?php echo $comments->organization_announcement_comment_id;?>" value="<?php echo $val;?>" style="display: none;">
                            <div class="col-md-12 btm-mrg pad10p" id="jdivComment_<?php echo $comments->organization_announcement_comment_id;?>">
                                <div class="row">
                                    <div class="col-md-1">
                                        <span class="mediapost_pic">
                                            <a href="<?php echo PageContext::$response->baseUrl;?>timeline/<?php echo $comments->user_alias;?>" >
                                            <?php if($comments->file_path != ''){ ?>
                                            <img class="ryt-mrg" src="<?php echo PageContext::$response->userImagePath;?><?php echo $comments->file_path;?>">
                                            <?php } else{ ?>
                                            <img class="ryt-mrg" src="<?php echo PageContext::$response->userImagePath;?>medium/member_noimg.jpg">
                                            <?php } ?>
                                            </a>
                                        </span>
                                    </div>
                                    <div class="col-md-11">
                                        <span class="name jEmojiable" ><a href="<?php echo PageContext::$response->baseUrl;?>timeline/<?php echo $comments->user_alias;?>" ><?php echo $comments->Username;?></a></span>
                                          <?php echo $comments->comment_content;?></span>
                                         <div class="clearfix"></div>
                                            <span class="new-text">

                                            <?php echo $comments->commentDate;?>
                                        </span>
                                        <div class="commentimg_box">
                                            <?php if($comments->file_path != ''){ ?>
<!--                                            <img class="fancybox img-responsive center" src="{PageContext::$response->userImagePath}medium/{$comments->file_path}">-->
                                            <ul class="portfolio" class="clearfix">
                                                    <li><a href="<?php echo PageContext::$response->userImagePath;?>medium/<?php echo $comments->file_path;?>" title=""><img src="<?php echo PageContext::$response->userImagePath;?>medium/<?php echo $comments->file_path;?>" alt=""></a></li>
                                            </ul>
                                            
                                            
                                            <?php } ?>
                                        </div>
                                        <div class="mediapost_user-side1">
                                            <span class="mediapost_links" id="jcountcommentlike_<?php echo $comments->organization_announcement_comment_id;?>"><?php if($comments->num_comment_likes > 0) { echo $comments->num_comment_likes." Likes"; }?></span>
                    <input type="hidden" id="reply_<?php echo $comments->organization_announcement_comment_id;?>" value="<?php if($comments->num_replies > 0){ echo $comments->num_replies;} else { echo "0"; } ?>">
                                            <span class="mediapost_links jOrgShowReply" cid="<?php echo $comments->organization_announcement_comment_id;?>" id="<?php echo $comments->organization_announcement_comment_id;?>">
                                                <a href="#" id="<?php echo $comments->organization_announcement_comment_id;?>">
                                                    <span id="jcountreply_<?php echo $comments->organization_announcement_comment_id;?>"><?php if($comments->num_replies > 0){ echo $comments->num_replies." Replies"; }?>
                                                    </span>
                                                </a>                                                    
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="comment_sharelike_sec">
                                            <?php if($comments->user_id == PageContext::$response->sess_user_id || $comments->community_created_by == PageContext::$response->sess_user_id){?>
                                            <a href="#" class="jOrgFeedCommentDelete marg10right" aid="<?php echo $feed['organization_announcement_id'];?>" id="<?php echo $comments->organization_announcement_comment_id;?>"><i class="fa fa-times"></i> Delete</a>
                                            <?php } ?>
                                            <a href="#" class="jFeedCommentReply marg10right" aid="<?php echo $feed['organization_announcement_id'];?>" cid="<?php echo $comments->organization_announcement_comment_id;?>" id="<?php echo $comments->organization_announcement_comment_id;?>"><i class="fa fa-reply"></i> Reply</a>
                                            <a href="#" class="jOrgFeedCommentLike <?php if($comments->LIKE_ID > 0 ){?>  liked <?php }?>" id="jlikeComment_<?php echo $comments->organization_announcement_comment_id;?>" cid="<?php echo $comments->organization_announcement_comment_id;?>" aid="<?php echo $feed['organization_announcement_id'];?>" >

                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                                <span id="jlikedisplaycomment_<?php echo $comments->organization_announcement_comment_id;?>">Like </span></a>
                                        </div>
                                        </span>
                                        <div id="jdivReply_<?php echo $comments->organization_announcement_comment_id;?>" class="jDisplayReply">
                                            <p class="lead emoji-picker-container position_relatv">
                                                            <textarea placeholder="Write your reply here..."  class="form-control textarea-control jEmojiTextarea" style="height:35px;" id="watermark_<?php echo $feed['organization_announcement_id'];?>_<?php echo $comments->organization_announcement_comment_id;?>" class="watermark" name="watermark"  ></textarea>
                                            </p>
                                            <a class="upload_cambtn" id="<?php echo $feed['organization_announcement_id'];?>" >
                                                <label for="imagereply_file_<?php echo $comments->organization_announcement_comment_id;?>"><i class="fa fa-camera feed-reply-camera"></i></label>
                                                <div class="file_button"> <input type="file" id="imagereply_file_<?php echo $comments->organization_announcement_comment_id;?>" cid="<?php echo $comments->organization_announcement_comment_id;?>" class="jorgimagereply_file" style="display:none;" /></div></a>
                                                <a id="shareButton" ondblclick="this.disabled=true;" cid="<?php echo $feed['community_id'];?>" cmid="<?php echo $comments->organization_announcement_comment_id;?>" aid="<?php echo $feed['organization_announcement_id'];?>"  class="btn yellow_btn fontupper pull-right jPostCommentButtonOrganization"> Post</a>
                                        </div>
                                        <div class="loader" id="imagereply_loader_<?php echo $comments->organization_announcement_comment_id;?>">
                                            <img src="<?php echo PageContext::$response->userImagePath;?>default/loader.gif" />
                                        </div>
                                        <div id="jDivReplyImagePreview_<?php echo $comments->organization_announcement_comment_id;?>"></div>
                                        <div id="postingReply_<?php echo $comments->organization_announcement_comment_id;?>"></div>
                                    </div>
                                </div> 
                            </div>
                    <?php }} ?>
<!--
{if !empty(PageContext::$response->Comments[{$announcement['organization_announcement_id']}])}
                                                            {if PageContext::$response->Page[{$announcement['organization_announcement_id']}]['totpages'] != PageContext::$response->Page[{$announcement['organization_announcement_id']}]['currentpage']}
                                                                <div id="jDisplayMoreComments_{$announcement['organization_announcement_id']}" ><a href="#" id="jMorecommentsButton" pid="{PageContext::$response->Page[{$announcement['organization_announcement_id']}]['currentpage']}" aid="{$announcement['organization_announcement_id']}">Load more comments</a></div>
                                                        {/if}
                                                        
                                                        {/if}-->

                    <!------------------------EOF Comments-------------------------------->
                </div>
            </div>
</div>
