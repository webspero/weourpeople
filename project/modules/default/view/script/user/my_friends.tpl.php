<div class="container">
    <div class="row"> 
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div id="o-wrapper" class="o-wrapper">
<!---Menu Link----------------- -->
{if PageContext::$response->sess_user_id >0}
<div class="main">
<div class="header-main">
     <main class="o-content">
        <div class="o-container">
          
          <div class="c-buttons">
            <button id="c-button--slide-right" class="button-nav-toggle"><span class="icon">&#9776;</span></button>
          </div>
          
          <div id="github-icons"></div>

        </div><!-- /o-container -->
      </main><!-- /o-content -->    
</div>
</div>  
{/if}
</div>
<!---EOF Menu Link----------------- -->
            <h3>My Friends<span class="round-search">{PageContext::$response->totalrecords}</span></h3>
            {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
        </div>
        <section class="searchbar">
            <div class="col-sm-12 col-md-12 col-lg-12">

                <div class="businesslist_search_blk">
                    <div class="row"> 
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <input type="hidden" value="{PageContext::$response->sess_user_id}" class="login_status" name="" id="login_status">
                            Sort by : A-Z <a href="{php} echo PageContext::$response->baseUrl; {/php}my-friends?sort=users.user_firstname&sortby={php} echo PageContext::$response->order; {/php}&searchtext={php} echo PageContext::$response->searchtext; {/php}&searchtype={php} echo PageContext::$response->searchtype; {/php}&page={php} echo PageContext::$response->page; {/php}">{PageContext::$response->business_name_sortorder}</a>
                        </div>
                        <input type='hidden' id='search_val' value='{php} echo PageContext::$response->searchtext; {/php}'>
                        <input type='hidden' id='sort_val' value='{php} echo PageContext::$response->order1; {/php}'>
                        <input type='hidden' id='page_val' value='{php} echo PageContext::$response->page; {/php}'>
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            {php}PageContext::renderRegisteredPostActions('searchbox');{/php}
                        </div>
                        <div class="col-sm-8 col-md-4 col-lg-4">
                        <select class="form-control" placeholder="Category" name="community_category" id="my_friend_category" value="" >
	                    <option value="All" >All</option>
                            <option value="Personal" {if PageContext::$response->groupby == 'Personal'} selected {/if}>Personal</option>
                            <option value="Business" {if PageContext::$response->groupby == 'Business'} selected {/if}>Business</option>
	                </select>
                        </div> 
                    </div>
                </div>
                {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
            </div>
        </section>
        <div class="clearfix"></div>
        
        <section class="bizcomlisting listin">
            <div id="msg"></div>
            {if PageContext::$response->totalrecords > 0}
            {assign var="i" value="1"}
            {foreach from=PageContext::$response->friends key=id item=friends}
            <div class="col-sm-4 col-md-4 col-lg-4">
                    <div class="mediapost display_table wid100per">
                    <div class="mediapost_left display_table_cell wid30per">
                        <span class="mediapost_pic">
                            <img src="{if $friends->file_path != ''}{PageContext::$response->userImagePath}small/{$friends->file_path}{else}{PageContext::$response->userImagePath}small/member_noimg.jpg{/if}">
                        </span>
                    </div>
                    <div class="media-body frndslist_media display_table_cell wid70per vert_align_top">
                        <h4 class="media-heading" id="{$friends->user_id}"><a href="{PageContext::$response->baseUrl}profile/{$friends->user_alias}">{$friends->user_firstname} {$friends->user_lastname}</a></h4>
                        <p>{$friends->user_friends_count} Friends</p>
                        <p><a name="reply_message" id="{$message->message_id}" onclick="return reply_message({$friends->user_id},'')" title="Send Message"  src="#"><span class="accept"><i class="fa fa-envelope-o"></i> Send Message</span> </a>
                            {if PageContext::$response->alias == PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != ''}
                            <a class="jMyUnfriend" uid="{$friends->user_id}" href="#" id="jUnfriend_{$friends->user_id}"><span class="reject"><i class="fa fa-times"></i> Unfriend</span></a> 
<!--                             <a id="{$friends->user_id}" class="friends_btn fright friend jchangestatus" href="#" uid="{$friends->user_id}">
                        <span class="tik_icon"></span>
                        Friends
                        </a>-->
                            <p>
                            <div class="dropdown">
                              <button class="btn btn-default dropdown-toggle jchangestatus" uid="{$friends->user_id}" type="button" id="{$friends->user_id}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <span class="tik_icon"></span>
                                Friends
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="{$friends->user_id}">
                                <li><a class="jChangetopersonal jChangetopersonal_{$friends->user_id}" id="{$friends->user_id}">{if $friends->friends_list_view_status eq 'P'}  <span class="tik_icon"></span>  {/if}Personal</a></li>
                                <li><a class="jChangetoBusiness jChangetoBusiness_{$friends->user_id}" id="{$friends->user_id}">{if $friends->friends_list_view_status eq 'B'}  <span class="tik_icon"></span>  {/if}Business</a></li>
                               
                              </ul>
                            </div>
                            </p>
                            {/if}
                        </p>
                    </div>
<!--                        <div class="jfriendstatus" id="friend_status_{$friends->user_id}" style="display: none; background-color: rgb(247, 247, 247);
                        border: thin double rgb(255, 210, 41);
                        height: 60px;
                        left: 127px;
                        padding: 3px;
                        position: absolute;
                        text-align: center;
                        top: 45px;
                        width: 155px;">
                            
                            <a class="jChangetopersonal jChangetopersonal_{$friends->user_id}" id="{$friends->user_id}">{if $friends->friends_list_view_status eq 'P'} * {/if}Personal</a><br>
                            <a class="jChangetoBusiness jChangetoBusiness_{$friends->user_id}" id="{$friends->user_id}">{if $friends->friends_list_view_status eq 'B'} * {/if}Business</a>
                        </div>-->
                    <div class="clearfix"></div>
                </div>
            </div>
            {assign var="i" value=$i+1}
            {/foreach}
            {else}
            <div class="col-sm-12 col-md-12 col-lg-12">
				<div class="no_found_page">
					{if PageContext::$response->searchtype eq ''}No friends added yet.{else}No matches found.{/if} .
				</div>
            </div>
        {/if}   
        <div class="clear"></div>
        {if PageContext::$response->totalrecords gt PageContext::$response->itemperpage}
        <div class="col-sm-12 col-md-12 col-lg-12">{PageContext::$response->pagination} </div>
        {/if}
        <div class="clearfix"></div>
        </section>

    </div>
</div>









    <script type ="text/javascript">
        $('#starring .rateit').bind('rated', function (e) {
            var ri = $(this);
            var rateField = $(this).attr("id");
            var status = true;
            var ratetype = ri.rateit('type');
            var ratetypeid = ri.rateit('typeid');     
            var rateflag = ri.rateit('flag');
            //if the use pressed reset, it will get value: 0 (to be compatible with the HTML range control), we could check if e.type == 'reset', and then set the value to  null .
            var ratevalue = ri.rateit('value');
            // alert("hi "+ratevalue);
            var userid = ri.rateit('id'); // if the product id was in some hidden field: ri.closest('li').find('input[name="productid"]').val()
            if(rateflag == 1 && userid <= 0){ //alert("2");
                status = checkLogin2(ratetype,ratetypeid,'rate',rateField);	    	
            }	 
            if(status != false){
                // alert("here");
                checkRated1(rateField);	    
            }
	 
        });

    </script>
