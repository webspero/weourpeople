<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="tab-content">
            <section class="marg10col">
                <div class="row">
                    {if count(PageContext::$response->communityDetails) gt 0 }
                    <div class="announcement-detail-sec">
                    <div class="mediapost">
                    
                        <div class="col-sm-2 col-md-2 col-lg-2">
                        <span class="mediapost_pic">
                               
                            <img src="{PageContext::$response->userImagePath}{if PageContext::$response->communityDetails->community_announcement_image_path neq ''}{PageContext::$response->communityDetails->community_announcement_image_path}
                            {elseif PageContext::$response->communityDetails->community_announcement_image_path eq ''}default/no_image_bbf.jpg{/if}" >
                        </span>
                        </div>
                        <div class="col-sm-8 col-md-8 col-lg-8">
                            <h3>
                                {PageContext::$response->communityDetails->community_announcement_title}
                            </h3>
                            <p>{PageContext::$response->communityDetails->community_announcement_content}</p>
                            
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 text-right">
                        <p> {PageContext::$response->communityDetails->community_announcement_date}</p>
                        </div>
                        </div>
                        </div>
                    {/if}
                </div>
            </section>
        </div>
    </div>
</div>
