<?php
$val = 1;
foreach (PageContext::$response->communityAnnouncements as $key => $announcement) {
    $val++; ?>

    <div class="whitebox" id="group_feed_<?php echo $announcement->community_announcement_id; ?>">
        <div class="wid100per">
            <div class="postheadtablediv">
                <div class="post_left">
                    
                    <span class="mediapost_pic">
                        <a href="<?php echo PageContext::$response->baseUrl;?>timeline/<?php echo $announcement->user_alias;?>">
                        <img
                            src="<?php echo PageContext::$response->userImagePath; ?><?php echo $announcement->file_path; ?>">
                        </a>
                    </span>
                </div>
                <div class="post_right">
                    <div class="posthead"><h4 class="media-heading"><a
                                href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $announcement->user_alias; ?>"><?php echo $announcement->user_firstname . " " . $announcement->user_lastname; ?></a>
                            <span></span></h4></div>
                    <div class="postsubhead">
                        <div class="postsubhead_right">
                            <span><?php if($announcement->days > 0){ echo $announcement->days." days ago"; }
                                            else { $timesplit = explode(":",$announcement->output_time);
                                            if($timesplit[0]>0){ echo ltrim($timesplit[0], '0')." hrs ago";}
                                            else{
                                            if($timesplit[1]>0){
                                            echo ltrim($timesplit[1], '0');
                                            if($timesplit[1] == 1) 
                                                { 
                                                echo "min";
                                                }
                                                else
                                                    { 
                                                    echo "mins";
                                                    } echo "ago";
                                            }
                                            else{
                                            echo "Just Now";
                                            }
                                            }
                                            }
                                         ?></span>
                    </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="postsubhead_left">
                <?php
                $reg_exUrl = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/';
                if(preg_match($reg_exUrl, $announcement->community_announcement_content,$url)){ ?>

                <div class="jEmojiable"><?php echo preg_replace($reg_exUrl, "<a href='$url[0]'>$url[0]</a>" , $announcement->community_announcement_content);?></div>

                <?php
                
                }else{ ?>

                <div class="jEmojiable"><?php echo stripcslashes($announcement->community_announcement_content);?></div>

                <?php } ?>
               </div>
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>
        <?php if ($announcement->community_announcement_image_path) { ?>
            <div class="postpic">
                <ul class="portfolio" class="clearfix">
                    <li>
                        <a href="<?php echo PageContext::$response->userImagePath . $announcement->community_announcement_image_path; ?>"
                           title=""><img
                                src="<?php echo PageContext::$response->userImagePath . $announcement->community_announcement_image_path; ?>"
                                alt="">
                        </a>
                    </li>
                </ul>
            </div>
        <?php } ?>
        <div class="clearfix"></div>

        <div class="mediapost_user-side">
            <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <div class="likesec">
                        <div class="display_table_cell pad10_right">
                            <a href="#"
                               class=" colorgrey_text jLikeFeedcommunity <?php if ($announcement->LIKE_ID > 0) { ?> liked <?php } ?>"
                               id="jlike_<?php echo $announcement->community_announcement_id; ?>"
                               aid="<?php echo $announcement->community_announcement_id; ?>"
                               cid="<?php echo $announcement->community_id; ?>">
                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                <span
                                    id="jlikedisplay_<?php echo $announcement->community_announcement_id; ?>">Like </span></a>
                        </div>
                        <div class="display_table_cell pad10_right">
                            <i class="fa fa-comments-o icn-fnt-size"></i>
                            <a href="#" id="jCommentButton_<?php echo $announcement->community_announcement_id; ?>"
                               aid="<?php echo $announcement->community_announcement_id; ?>"
                               class="jCommentButton colorgrey_text">Comment</a>
                        </div>
                        <?php if ($announcement->community_announcement_created_by == PageContext::$response->sess_user_id) { ?>
                            <div class="display_table_cell pad10_right">
                                <i class="fa fa-trash-o icn-fnt-size"></i>
                                <a href="#" id="jDeleteButton_<?php echo $announcement->community_announcement_id; ?>"
                                   aid="<?php echo $announcement->community_announcement_id; ?>"
                                   class="jGroupDeletetButton colorgrey_text">Delete</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8">
                    <div class="sharefnt">
                        <div class="fb-share-button"
                             id="share_button_<?php echo $announcement->community_announcement_id;; ?>"
                             data-href="<?php echo PageContext::$response->baseUrl; ?>announcement-detail/<?php echo $announcement->community_announcement_alias; ?>"
                             data-title="Shared on facebook" data-layout="button_count" data-desc="You can share data"
                             data-url="<?php echo PageContext::$response->baseUrl; ?>newsfeed-detail/<?php echo $announcement->community_announcement_alias; ?>">
                            Share
                        </div>
                        <a href="<?php echo PageContext::$response->baseUrl; ?>announcement-detail/<?php echo $announcement->community_announcement_alias; ?>"
                           title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>
                            <div id="group_like_users_<?php echo $announcement->community_announcement_id;?>" class="like_users_div" style=" display: none;">
                      <?php foreach(PageContext::$response->newsFeedLikeUsers[$announcement->community_announcement_id] as $key=>$val ){ ?>
                      <span><?php echo $val->user_name;?><br></span>
                      <?php } ?>  
                    </div>
                        <span class="mediapost_links jShowGroupLikeUsers"
                              id="jcountlike_<?php echo $announcement->community_announcement_id; ?>"><?php if ($announcement->community_announcement_num_likes > 0) {
                                echo $announcement->community_announcement_num_likes; ?> Likes <?php } ?>
                        </span>
                        <span class="mediapost_links jCommentButton count_class"
                              aid="<?php echo $announcement->community_announcement_id; ?>"
                              id="jcountcomment_<?php echo $announcement->community_announcement_id; ?>"><?php if ($announcement->community_announcement_num_comments > 0) {
                                echo $announcement->community_announcement_num_comments; ?>
                                Comments
                            <?php } ?>
                        </span>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="mediapost_user-side-top clear jCommentDisplayDiv"
             id="jCommentBoxDisplayDiv_<?php echo $announcement->community_announcement_id; ?>" style="display:none">
            <p class="lead emoji-picker-container position_relatv">
                <textarea class="form-control textarea-control"
                          id="watermark_<?php echo $announcement->community_announcement_id; ?>" rows="3"
                          placeholder="Write your comment here..." data-emojiable="true"></textarea>
                <a class="upload_cambtn" id="<?php echo $announcement->community_announcement_id; ?>">
                    <label for="file_<?php echo $announcement->community_announcement_id; ?>"><i
                            class="fa fa-camera feed-camera"></i></label></a>
                <input type="file" id="file_<?php echo $announcement->community_announcement_id; ?>"
                       aid="<?php echo $announcement->community_announcement_id; ?>" class="jimage_file"
                       style="cursor: pointer;  display: none"/>
                <input type="hidden" id="announcement_id"
                       value="<?php echo $announcement->community_announcement_id; ?>">
            </p>
             <div class="msg_display" id="msg_display_<?php echo $announcement->community_announcement_id; ?>"></div>
										 	<div class="clearfix"></div>
            <a id="postButton" cid="<?php echo $announcement->community_id; ?>"
               cmid="<?php echo $announcement->announcement_comment_id; ?>"
               aid="<?php echo $announcement->community_announcement_id; ?>"
               class="btn yellow_btn fontupper pull-right jPostCommentButtonCommunity"> Post</a>
        </div>
        <div class="loading" id="loading_<?php echo $feedlist['community_announcement_id']; ?>" style="display: none;">
            <img src="<?php echo PageContext::$response->userImagePath; ?>default/loader.gif"/>
        </div>
        <div id="juploadimagepreview_<?php echo $announcement->community_announcement_id; ?>"></div>
        <div class="clear commentblk jCommentDisplayDiv"
             id="jCommentDisplayDiv_<?php echo $announcement->community_announcement_id; ?>" style="display:none">
            <div id="posting_<?php echo $announcement->community_announcement_id; ?>">
                <!------------------------Comments-------------------------------->
                <?php $val = 0; ?>
                <?php 
                foreach (PageContext::$response->Comments[$announcement->community_announcement_id] as $key => $comments) { 
                    $val++;
                    if ($comments->announcement_comment_id) {
                    ?>

                    <input type="hidden" id="val_<?php echo $announcement->community_announcement_id; ?>"
                           value="<?php echo count(PageContext::$response->Comments[$announcement->community_announcement_id]); ?>">

                    <input type="text" id="hid_announcement_id_<?php echo $comments->announcement_comment_id; ?>"
                           class="hid_announcement_id" value="<?php echo $announcement->community_announcement_id; ?>"
                           style="display: none;">
                    <input type="text" id="hid_<?php echo $comments->announcement_comment_id; ?>"
                           value="<?php echo $val; ?>" style="display: none;">
                    <div class="col-md-12 btm-mrg pad10p"
                         id="jdivComment_<?php echo $comments->announcement_comment_id; ?>">
                        <div class="row">
                            <div class="col-md-1">
                                <span class="mediapost_pic">
                                     <a href="<?php echo PageContext::$response->baseUrl;?>timeline/<?php echo $comments->user_alias;?>">
                                    <?php if ($comments->user_image != '') { ?>
                                        <img class="ryt-mrg"
                                             src="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->user_image; ?>">
                                    <?php } else { ?>
                                        <img class="ryt-mrg"
                                             src="<?php echo PageContext::$response->userImagePath; ?>medium/member_noimg.jpg">
                                    <?php } ?>
                                        </a>
                                </span>
                            </div>
                            <div class="col-md-11">
                                <span
                                    class="name"><a href="<?php echo PageContext::$response->baseUrl;?>timeline/<?php echo $comments->user_alias;?>"><?php echo $comments->Username; ?> </a></span>
                                <span class="jEmojiable"><?php echo $comments->comment_content; ?></span>
                                <div class="clearfix"></div>
                                    <span class="new-text">
                                        <?php echo $comments->commentDate; ?>
                                    </span>
                                <div class="commentimg_box">
                                    <?php if ($comments->file_path != '') { ?>
                                        <ul class="portfolio" class="clearfix">
                                            <li>
                                                <a href="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->file_path; ?>"
                                                   title=""><img
                                                        src="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->file_path; ?>"
                                                        alt="">
                                                </a>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                </div>
                                <div class="mediapost_user-side1">
                                    <span  rel="tooltip" title="<?php echo $comments->users;?> liked this comment" class="mediapost_links"
                                          id="jcountcommentlike_<?php echo $comments->announcement_comment_id; ?>"><?php if ($comments->num_comment_likes > 0) {
                                            echo $comments->num_comment_likes; ?> Likes <?php } ?></span>
                                    <input type="hidden" id="reply_<?php echo $comments->announcement_comment_id; ?>"
                                           value="<?php if ($comments->num_replies > 0) {
                                               echo $comments->num_replies;
                                           } else {
                                               echo "0";
                                           } ?>">
                                    <span class="mediapost_links "
                                          cid="<?php echo $comments->announcement_comment_id; ?>"
                                          id="jcountreply_<?php echo $comments->announcement_comment_id; ?>">
                                        <a href="#"
                                           class="jShowReply"
                                           id="<?php echo $comments->announcement_comment_id; ?>">
                                            <span
                                                id="jcountreply_<?php echo $comments->announcement_comment_id; ?>"><?php if ($comments->num_replies > 0) {
                                                    echo $comments->num_replies; ?> Replies<?php } ?>
                                            </span>
                                        </a>
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="comment_sharelike_sec">
                                    <?php if ($comments->user_id == PageContext::$response->sess_user_id || $comments->community_created_by == PageContext::$response->sess_user_id) { ?>
                                        <a href="#" class="jFeedCommentDelete marg10right"
                                           aid="<?php echo $announcement->community_announcement_id; ?>"
                                           id="<?php echo $comments->announcement_comment_id; ?>"><i
                                                class="fa fa-times"></i> Delete</a>
                                    <?php } ?>
                                    <a href="#" class="jFeedCommentReply marg10right"
                                       aid="<?php echo $announcement->community_announcement_id; ?>"
                                       cid="<?php echo $comments->announcement_comment_id; ?>"
                                       id="<?php echo $comments->announcement_comment_id; ?>"><i
                                            class="fa fa-reply"></i> Reply</a>
                                    <a href="#"
                                       class="jFeedCommentLike <?php if ($comments->LIKE_ID > 0) { ?> liked <?php } ?>"
                                       id="jlikeComment_<?php echo $comments->announcement_comment_id; ?>"
                                       cid="<?php echo $comments->announcement_comment_id; ?>"
                                       aid="<?php echo $announcement->community_announcement_id; ?>">

                                        <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                        <span
                                            id="jlikedisplaycomment_<?php echo $comments->announcement_comment_id; ?>">Like </span></a>
                                </div>
                                </span>
                                <div id="jdivReply_<?php echo $comments->announcement_comment_id; ?>"
                                     class="jDisplayReply" style="display: none">
                                    <p class="lead emoji-picker-container position_relatv">
                                        <textarea placeholder="Write your reply here..."
                                                  class="form-control textarea-control" style="height:35px;"
                                                  id="watermark_<?php echo $announcement->community_announcement_id; ?>_<?php echo $comments->announcement_comment_id; ?>"
                                                  class="watermark" name="watermark"></textarea>
                                    </p>
                                    <a class="upload_cambtn"
                                       id="<?php echo $announcement->community_announcement_id; ?>">
                                        <label
                                            for="imagereply_file_<?php echo $comments->announcement_comment_id; ?>"><i
                                                class="fa fa-camera feed-reply-camera"></i></label>
                                        <div class="file_button"><input type="file"
                                                                        id="imagereply_file_<?php echo $comments->announcement_comment_id; ?>"
                                                                        cid="<?php echo $comments->announcement_comment_id; ?>"
                                                                        class="jcommunityimagereply_file"
                                                                        style="display:none;"/>
                                        </div>
                                    </a>
                                     <div class="msg_display" id="msg_display_<?php echo $comments->announcement_comment_id; ?>"></div>
										 	<div class="clearfix"></div>
                                    <a id="shareButton" cid="<?php echo $announcement->community_id; ?>"
                                       cmid="<?php echo $comments->announcement_comment_id; ?>"
                                       aid="<?php echo $announcement->community_announcement_id; ?>"
                                       class="btn yellow_btn fontupper pull-right jPostCommentButtonCommunity"> Post</a>
                                </div>
                                <div class="loader"
                                     id="imagereply_loader_<?php echo $comments->announcement_comment_id; ?>">
                                    <img src="<?php echo PageContext::$response->userImagePath; ?>default/loader.gif"/>
                                </div>
                                <div id="jDivReplyImagePreview_<?php echo $comments->announcement_comment_id; ?>"></div>
                                <div id="postingReply_<?php echo $comments->announcement_comment_id; ?>"></div>
                            </div>
                        </div>
                    </div>
                    <?php
                    }} ?>


                <?php if (!empty(PageContext::$response->Comments[$announcement->community_announcement_id])) {
                    if (PageContext::$response->Page[$announcement->community_announcement_id]['totpages'] != PageContext::$response->Page[$announcement->community_announcement_id]['currentpage']) {
                        ?>
                        <div id="jDisplayMoreComments_<?php echo $announcement->community_announcement_id; ?>"><a
                                href="#" id="jMorecommentsButton"
                                pid="<?php echo PageContext::$response->Page[$announcement->community_announcement_id]['currentpage']; ?>"
                                aid="<?php echo $announcement->community_announcement_id; ?>">Load more comments</a>
                        </div>
                    <?php }

                } ?>
                <!------------------------EOF Comments-------------------------------->
            </div>
        </div>
    </div>

<?php } ?>