<div class="container">
<section class="inbox_section whitebox">
    <div class="row"> 
        <div class="col-sm-12 col-md-12 col-lg-12">
            <!--{PageContext::$response->messagebox|print_r}-->
             <div class="{PageContext::$response->messagebox['msgClass']}">{PageContext::$response->messagebox['msg']}</div>

<span id="content_msg"></span>
            <h3>Sent Items</h3>
        </div>
    </div>
    <div class="tab_container">
            <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
            </div>
            </div>
        </div>
        <div id="myTabContent" class="tab-content find_friends_list_outer">
            <div class="tab-pane fade active in" id="tab-Messages">
                        {if PageContext::$response->message_details->records|@sizeof eq 0}
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <span class="nomessage_title">
                                    No Messages Found   
                                </span>
                            </div>
                        </div>
                        {else}
                        {foreach PageContext::$response->message_details->records item=message}
                        <div class="inboxlist" >
                            <div class="row">
                            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 border_bot">
                                <div class="mediapost_left pull-left">
                                    <a href="{PageContext::$response->baseUrl}timeline/{$message->user_alias}">
                                        <span class="mediapost_pic" style="background-image: url('{if $message->file_path eq ''} {PageContext::$response->userImagePath}small/member_noimg.jpg{else}{PageContext::$response->userImagePath}small/{$message->file_path}{/if}');">
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 border_bot">
                                <h5 class="normal_user"><a href="{PageContext::$response->baseUrl}timeline/{$message->user_alias}">{$message->user_firstname} {$message->user_lastname}</a></h5>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 border_bot">
                                <h6 class="normal_head">
                                    <a href="#" onclick="return show_message({$message->message_id})">{stripslashes($message->message_subject)} - <span>{stripslashes(substr($message->message_detail,0,140))}</span></a>
                                </h6>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 border_bot">
                                <div class="wid100per inbox_date">
                                    <span class="date left">{date('m/d/Y',strtotime($message->message_date))}</span>
                                    <span class="dropdown right">
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-ellipsis-v"></i></a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                             <li>
                                                <a href="#" onclick="return show_message({$message->message_id})"><i class="fa fa-envelope"></i> Show full Message</a>
                                            </li>
                                            <li>
                                                <a href="#" onclick="return delete_sentmessage({$message->message_id})"><i class="fa fa-trash"></i> Delete</a>
                                            </li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                            </div>
                        </div>
<!--                        <div class="inboxlist inbox_read">
                            <div class="row">
                            <div class="col-sm-1 col-md-1 col-lg-1 border_bot">
                                <div class="mediapost_left pull-left">
                                    <span class="mediapost_pic">
                                        <a href="{PageContext::$response->baseUrl}profile/{$message->user_alias}"><img src="{if $message->user_image_name eq ''} {PageContext::$response->userImagePath}small/member_noimg.jpg{else}{PageContext::$response->userImagePath}small/{$message->user_image_name}{/if}"></a>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-2 col-md-2 col-lg-2 border_bot">
                                <h5 class="normal_user"><a href="{PageContext::$response->baseUrl}profile/{$message->user_alias}">{$message->user_firstname} {$message->user_lastname}</a></h5>
                            </div>
                            <div class="col-sm-7 col-md-7 col-lg-7 border_bot">
                                <h6 class="normal_head">
                                    <a href="#" onclick="return show_message({$message->message_id})">{stripslashes($message->message_subject)} - <span>{stripslashes(substr($message->message_detail,0,140))}</span></a>
                                </h6>
                            </div>
                            <div class="col-sm-2 col-md-2 col-lg-2 border_bot">
                                <div class="wid100per">
                                    <span class="dropdown right">
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-ellipsis-v"></i></a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a name="reply_message" id="{$message->message_id}" onclick="return reply_message({$message->user_id},'{$message->message_subject}')" title="Reply Message"  src="#"><i class="fa fa-reply"></i> Reply</a>
                                            </li>
                                            <li>
                                                <a href="#" onclick="return show_message({$message->message_id})"><i class="fa fa-envelope"></i> Show full Message</a>
                                            </li>
                                            <li>
                                                <a href="#" onclick="return delete_message({$message->message_id})"><i class="fa fa-trash"></i> Delete</a>
                                            </li>
                                        </ul>
                                    </span>
                                    <div class="clearfix"></div>
                                </div>
                                <span class="wid100per date align-right">{date('m/d/Y',time($message->message_date))}</span>
                            </div>
                            </div>
                        </div>-->
                        {/foreach}{/if}
                         <div class="row">
                            {if PageContext::$response->totalrecords gt PageContext::$response->itemperpage}
                            <div class="col-sm-12 col-md-12 col-lg-12">{PageContext::$response->pagination} </div>
                            {/if}
                        </div>
            </div>
        </div>
    </section>








<!--<div class="content_left">
   
    <div class="member_middle_blk inbox_div findfriend" id="member_middle_blk" >
    <h3>Inbox</h3>
        <div class="member_prof_menu">
            <ul>
       
               

                <li class="active"><a href="#" title="tab-Messages" class="jtab" onclick="window.location.reload();" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">Messages</a></li>
                <li ><a href="#" title="tab-Feed" class="jtab jShowInvitationsinbox" >Invitations</a></li>
                
            </ul>
           
        </div>
        
        	<div class="find_friends_list_outer">
        

        <div class="find_friends_list_blk">
        {if PageContext::$response->message_details->records|@sizeof eq 0}
				No Messages Found	
			{else}
            {foreach PageContext::$response->message_details->records item=message}
           
            <div class="row">
                <div class="colside1">
                    <div class="colside1_pic">
                        <a href="{PageContext::$response->baseUrl}profile/{$message->user_alias}"><img src="{if $message->user_image_name eq ''} {PageContext::$response->userImagePath}small/member_noimg.jpg{else}{PageContext::$response->userImagePath}small/{$message->user_image_name}{/if}"></a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="colmiddle" style="width:500px">
                    <h6 class="normal_head"><a href="#" onclick="return show_message({$message->message_id})">{stripslashes($message->message_subject)}</a></h6>
                    <h5 class="normal_user"><a href="{PageContext::$response->baseUrl}profile/{$message->user_alias}">{$message->user_firstname} {$message->user_lastname}</a></h5>
                     <p class="mesage_txt">
                        {stripslashes(substr($message->message_detail,0,140))}
                    </p>
					
					<div class="replymessage">
						<a name="reply_message" id="{$message->message_id}" onclick="return reply_message({$message->user_id},'{$message->message_subject}')" title="Reply Message"  src="#">Reply</a>
					</div>
					<div class="fullmessage">
						<a href="#" onclick="return show_message({$message->message_id})">Show full Message</a>
					</div>
					<div class="deletemessage">
						<a href="#" onclick="return delete_message({$message->message_id})">Delete</a>
					</div>
					
					
					
                </div>
                <div class="colside3" style="width:60px;">
                    
                       
                        <span class="floatright">
                        <h4 class="msg_date_1">{date('m/d/Y',time($message->message_date))}</h4>
                        </span>

                </div>
                
            </div>
			{/foreach}{/if}
        

        </div>
        <div class="clear"></div>
<div class="listitem">
		<img src="{PageContext::$response->userImagePath}default/loader.gif" /> Loading....
		</div>
    </div>-->
	

