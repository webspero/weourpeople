<div class="col-md-12 btm-mrg pad10p"
     id="jdivComment_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>">
    <div class="row">
        <div class="col-md-1">
            <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo PageContext::$response->alias; ?>">
                <?php if (PageContext::$response->comments['image']) { ?>
                    <span class="mediapost_pic">
                <img class="ryt-mrg"
                     src="<?php echo PageContext::$response->userImagePath; ?><?php echo PageContext::$response->comments['image']; ?>">
            </span>
                <?php } else {
                    ?>
                    <span class="mediapost_pic">
                <img class="ryt-mrg" src="<?php echo PageContext::$response->userImagePath; ?>medium/member_noimg.jpg">
            </span>
                <?php } ?>
            </a>
        </div>
        <div class="col-md-11">
            <span
                class="name"><?php echo PageContext::$response->comments['name']; ?>
            </span>
            <span class="jEmojiable">
                <?php echo PageContext::$response->comments['comment']['comment']; ?>
            </span>
            <br>
            <span class="new-text">Just now 
               <?php if (PageContext::$response->comments['parent_comment_id'] > 0) { ?>
            <a href="#" class="jFeedReplyDelete right" aid="<?php echo PageContext::$response->comments['parent_comment_id']; ?>"
                       id="<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"><i class="fa fa-times"></i> Delete</a>
               <?php } ?>
            </span>
            <div class="commentimg_box">
                <?php if (PageContext::$response->image_file) { ?>
                    <ul class="portfolio" class="clearfix">
                        <li>
                            <a href="<?php echo PageContext::$response->userImagePath; ?>medium/<?php echo PageContext::$response->image_file; ?>"
                               title=""><img
                                    src="<?php echo PageContext::$response->userImagePath; ?>medium/<?php echo PageContext::$response->image_file; ?>"
                                    alt=""></a></li>
                    </ul>
                <?php } ?>
            </div>
            <div class="mediapost_user-side1">
                <span class="mediapost_links"
                      id="jcountcommentlike_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"></span>
                <input type="hidden" id="reply_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"
                       value="0">
                <span class="mediapost_links "
                      cid="<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"><a href="#"
                                                                                                        class="jShowReply"
                                                                                                        id="<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"><span
                            id="jcountreply_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"></span></a></span>
                <div class="clear"></div>
            </div>
            <?php if (PageContext::$response->comments['parent_comment_id'] > 0) {
            } else {
                ?>
                <div class="comment_sharelike_sec">
                    <a href="#" class="jFeedCommentDelete" aid="<?php echo PageContext::$response->comments['comment']['announcement_id']; ?>" id="<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"><i class="fa fa-times"></i> Delete</a>
                    <a href="#" class="jFeedCommentReply marg10right"
                       aid="<?php echo PageContext::$response->comments['comment']['announcement_id']; ?>"
                       cid="<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"><i
                            class="fa fa-reply"></i> Reply</a>
                    <a href="#" class="jFeedCommentLike <?php if ($comments->LIKE_ID > 0) { ?> liked <?php } ?> "
                       id="jlikeComment_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"
                       cid="<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"
                       aid="<?php echo PageContext::$response->comments['comment']['announcement_id']; ?>">


                        <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                        <span
                            id="jlikedisplaycomment_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>">Like </span></a>
                </div>
            <?php } ?>
            <div id="jdivReply_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"
                 class="jDisplayReply" style="display:none;">
                <p class="lead emoji-picker-container">
                    <div class="emotionsbtn_textblk">
                        <textarea placeholder="Write your reply here..."
                                  class="form-control textarea-control jEmojiTextarea" style="height:35px;"
                                  id="watermark_<?php echo PageContext::$response->comments['comment']['announcement_id']; ?>_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"
                                  class="watermark" name="watermark"></textarea>
                        <a class="upload_cambtn">
                            <label
                                for="imagereply_file_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"><i
                                    class="fa fa-camera feed-reply-camera"></i>
                            </label>
                        </a>
                    <div class="file_button"><input type="file"
                                                    id="imagereply_file_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"
                                                    cid="<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"
                                                    class="jcommunityimagereply_file" style="display:none;"/></div>
                    </div>
                </p>
                <!--<a id="shareButton" cid="{$feedlist['community_id']}" aid="{$feedlist['community_announcement_id']}"  class="btn yellow_btn fontupper pull-right jshareCommentButton"> Post</a>-->
                <a id="shareButton" cid="<?php echo PageContext::$response->comments['comment']['community_id']; ?>"
                   cmid="<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"
                   aid="<?php echo PageContext::$response->comments['comment']['announcement_id']; ?>"
                   class="btn yellow_btn fontupper pull-right jPostCommentButtonCommunity"> Post</a>
            </div>
            <div
                id="jDivReplyImagePreview_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"></div>
            <div id="postingReply_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"></div>
        </div>
    </div>

</div>


