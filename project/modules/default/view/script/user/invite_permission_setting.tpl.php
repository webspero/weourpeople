<link type="text/css" rel="stylesheet" href="<?php echo BASE_URL ?>project/styles/popup.css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo BASE_URL ?>public/js/jquery-1.8.0.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL ?>bbf/public/js/jquery-ui-1.8.23.custom.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL ?>public/js/jquery.blockUI.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL ?>public/js/fw.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL ?>project/js/jquery.metadata.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL ?>project/js/jquery.validate.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL ?>project/js/app.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL ?>project/js/jquery.colorbox.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>project/js/comment_popup.js"></script>
<script src="<?php echo BASE_URL;?>project/lib/ckeditor/ckeditor.js"></script>

<script>
    CKEDITOR.replace("txtMailContent",
                {
                    height: 130,
                    toolbar: [
                    //{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteFromWord','-','Undo','Redo' ] },
                    {
                        name: 'clipboard', 
                        items : [ 'Cut','Copy','Paste','PasteFromWord','-', 'Undo','Redo' ]
                    },
                    {
                        name: 'editing', 
                        items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ]
                    },
                    {
                        name: 'styles', 
                        items : [ 'FontSize' ]
                    },
                    {
                        name: 'colors', 
                        items : [ 'TextColor','BGColor' ]
                    },
                    '/',
                    {
                        name: 'basicstyles', 
                        items : [ 'Bold','Italic','Underline','Strike','-','RemoveFormat' ]
                    },
                    {
                        name: 'paragraph', 
                        items : [ 'NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ]
                    },
                    {
                        name: 'links', 
                        items : [ 'Link','Unlink','Anchor' ]
                    }  
                    ]
                }
</script>
 
<div class="content_login">
    <div class="" id="msg"></div>
    <h3>Initiate an introduction</h3>
    <form name="frmInviteSetting" method="post" id="frmInviteSetting"> 
        <?php
//        if (PageContext::$response->members) {
//            foreach (PageContext::$response->members as $key => $val) {
                ?>  
                <div class="invite_list_blk">
                    <div class="invite_list_img">
                        <img src="<?php echo PageContext::$response->userImagePath ?><?php echo ($val->file_path) ? $val->file_path : 'member_noimg.jpg'; ?>">
                    </div>
                    <div class="invite_frnd_list_name">
                        <h5> <?php echo ($val->user_firstname) ? $val->user_firstname : $val->user_email ?></h5>
                    </div>
                    <div class="invite_act_blk" id="<?php echo $key; ?>"> 
                        <input type="hidden" name="inv_check[<?php echo $val->user_id; ?>]" value="0">
                        <div class="loading_invite" id="loading_invite<?php echo $key; ?>"></div>
                        <input type="checkbox" name="inv_check[<?php echo $val->user_id; ?>]" id="<?php echo $val->user_id; ?>" value="<?php echo $val->user_id; ?>" <?php echo (($val->cmember_inv_permission == 'Y' || $val->cmember_type == 'A') ? 'checked' : ''); ?> <?php echo ($val->cmember_type == 'A' ? 'disabled' : ''); ?>/>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
   <?php // } ?>
            <div class="clear"></div>
            
            <div>
                <div>
                   Enter email address(es) of recipients (non members).Separate each address by a comma
                </div>
                <div>
                    <input type="text" name="txtRecipients">
                </div>
                 <div>
                    Enter email address(es) of people to invite .Separate each address by a comma
                </div>
              <input type="text" name="txtInvitees">
               <div>
                    Mail content
                </div>
              <div>
                <textarea name="txtMailContent" id="txtMailContent"></textarea>
              </div>
            </div>
            <div class="clear"></div>
                <input type="submit" name="inv_set_submit" value = "Set Permission" class="yellow_btt" /></div>
<?php // } else {
    echo "No Members found";
//} ?>  
        <input type="hidden" name="communityId" value="<?php echo PageContext::$response->communityId; ?>">
        <input type="hidden" name="userId" value="<?php echo PageContext::$response->userId; ?>">
    </form>

</div>  