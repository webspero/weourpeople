<script type="text/javascript">
    function reply_message(to_user_id,message_subject){
        var mlink=mainUrl+"user/create_message/"+to_user_id+"/"+message_subject;
        $.colorbox({width:"400px", height:"350px", iframe:true, href:mlink, overlayClose: false});
        return false;
    }
    </script>
<div class="row">
    <?php
    if (count(PageContext::$response->friends) > 0) {
        foreach (PageContext::$response->friends As $friends) {
            ?>
            <div class="col-sm-6 col-md-4 col-lg-6">
                <div class="mediapost display_table wid100per">
                    <div class="picpost_left display_table_cell wid30per">
                        <span class="picpost_left_pic">
                            <img src="<?php if ($friends->file_path != '') {
                                echo PageContext::$response->userImagePath . $friends->file_path;
                            } else {
                                echo PageContext::$response->userImagePath . 'small/member_noimg.jpg';
                            } ?>">
                        </span>
                    </div>
                    <div class="media-body frndslist_media display_table_cell wid70per vert_align_middle">
                        <a href="<?php echo PageContext::$response->baseUrl; ?>profile/<?php echo $friends->user_alias ?>"><h4 class="" id="<?php echo $friends->user_id;?>"><?php echo $friends->user_firstname . ' ' . $friends->user_lastname; ?></h4></a>
                        <p>
                            <?php echo $friends->user_friends_count; ?> Friends
                        </p>
                        <p><span class="accept"><a name="reply_message" class="replymessage" id="<?php echo $message->message_id; ?>" onclick="return reply_message(<?php echo $friends->user_id; ?>,'')" title="Send Message"  src="#">Send Message
                                </a></span>
        <?php if (PageContext::$response->alias == PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '') { ?>
                                <span class="reject"> x<a class="jUnfriend" uid="<?php echo $friends->user_id ?>" href="#" id="jUnfriend_<?php echo $friends->user_id ?>"> Unfriend</a></span></p>
            <?php }
        ?><div class="clearfix"></div>
                       </p>
                  
                      <?php if (PageContext::$response->alias == PageContext::$response->sess_user_alias){ ?>
<!--                           <p>
                            <div class="dropdown">
                              <button class="btn btn-default dropdown-toggle" type="button" id="<?php echo $friends->user_id;?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <span class="tik_icon"></span>
                                Friends
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="<?php echo $friends->user_id;?>">
                                <li><a class="jChangetopersonal jChangetopersonal_<?php echo $friends->user_id;?>" id="<?php echo $friends->user_id;?>"><?php if($friends->friends_list_view_status == 'P'){ echo "<span class='tik_icon'></span> "; }?>Personal</a></li>
                                <li><a class="jChangetoBusiness jChangetoBusiness_<?php echo $friends->user_id;?>" id="<?php echo $friends->user_id;?>"><?php if($friends->friends_list_view_status == 'B'){ echo "<span class='tik_icon'></span> "; }?>Business</a></li>
                               
                              </ul>
                            </div>
                            </p>-->
                         <?php } ?>
                              </div>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php
    }
}?>
<div class="clear"></div>