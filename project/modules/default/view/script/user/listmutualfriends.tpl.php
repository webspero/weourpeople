<div class="row">
    <?php if (count(PageContext::$response->friends) > 0) { ?>
        <?php foreach (PageContext::$response->friends As $friends) { ?>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <div class="mediapost">
                    <div class="mediapost_left pull-left">
                        <span class="mediapost_pic">
                            <img src="<?php
                                if ($friends->file_path != '') {
                                    echo PageContext::$response->userImagePath . 'small/' . $friends->file_path;
                                } else {
                                    echo PageContext::$response->userImagePath . 'small/member_noimg.jpg';
                                }
                                    ?>">
                        </span>
                    </div>
                    <div class="media-body">
                        <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $friends->user_alias ?>"><h4 class="media-heading"><?php echo $friends->user_name; ?></h4></a>
                        <p><?php echo $friends->count; ?> Friends</p>
                        <?php if (PageContext::$response->alias == PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '') { ?>
                        <p><span class="accept">
                                <a name="reply_message" class="replymessage" id="<?php echo $message->message_id; ?>" onclick="return reply_message(<?php echo $friends->user_id; ?>,'')" title="Send Message"  src="#">Send Message
                                </a></span>
                            
                                <span class="reject"> x<a class=" jUnfriend" uid="<?php echo $friends->user_id ?>" href="#" id="jUnfriend_<?php echo $friends->user_id ?>"> Unfriend</a></span>
                        </p>
                         <?php }
                            ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <?php
        }
        ?>
        <?php
    } else {
        ?>
        <div class="frndslisting_blk_content">
            <span class="margin_l_20">No mutual friends</span>
        </div>
        <?php
    }
    ?>
</div>
<div class="clear"></div>