<div class="container">
    <div class="row"> 
        <div class="col-md-offset-3 col-lg-offset-3 col-sm-12 col-md-6 col-lg-6 ">
            <div class="whitebox marg40col pad25px_left pad25px_right">
				<div class="form-top">
	        		<div class="form-top-left">
	        			<h3>Add Page</h3>
	        		</div>
                                    
	            </div>
	            <div class="form-bottom signup_form">
	            	<div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
	                <form enctype="multipart/form-data" class="" action="" method="post" id="frmBizdirectoryRegister" name="frmBizdirectoryRegister" novalidate="novalidate">
                        <div class="form-group relative">
                        	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
		                    		<input type="text" value="" name="business_name" class="form-control" placeholder="Page Name">
	                            </div>
	                            <div class="display_table_cell wid2per vlaign_middle align-right">
	                            	<span id="spn_asterisk_bnameadd" style="color:red">*</span>
	                            </div>
                            </div>
                            <label generated="true" for="business_name" class="error"></label>
	                    </div>
	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
	                    			<input type="text" value="{PageContext::$request['business_email']}" name="business_email" class="form-control" placeholder="Email">
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                	<span id="spn_asterisk_bemailadd" style="color:red">*</span>
                                </div>
                            </div>
	                    	<label generated="true" for="business_email" class="error"></label>
	                    </div>
	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
			                    	<input type="text" value="{PageContext::$request['business_url']}" name="business_url" class="form-control" placeholder="Website URL(http://www.domainname.com)">
	                            </div>
	                            <div class="display_table_cell wid2per vlaign_middle align-right">
	                            	<span id="spn_asterisk_burladd" style="color:red">*</span>
	                            </div>
	                        </div>
			                <label generated="true" for="business_url" class="error"></label>
	                    </div>

	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
	                    			<textarea class="form-control" name="business_description" placeholder="Page Description">{PageContext::$request['business_description']}</textarea>
	                    		</div>
	                    		<div class="display_table_cell wid2per vlaign_middle align-right">
                                	<span id="spn_asterisk_bdescriptionadd" style="color:red">*</span>
                                </div>
                            </div>
	                    	<label generated="true" for="business_description" class="error"></label>
	                    </div>

	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
	                    			<textarea class="form-control" name="business_address" placeholder="Address">{PageContext::$request['business_address']}</textarea>
	                    		</div>
	                    		<div class="display_table_cell wid2per vlaign_middle align-right"></div>
	                    	</div>
	                    	<label generated="true" for="business_address" class="error"></label>
	                    </div>
	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
	                    			<input type="text" name="business_city" class="form-control" placeholder="City" value="{PageContext::$request['business_city']}">
	                    		</div>
	                    		<div class="display_table_cell wid2per vlaign_middle align-right"></div>
	                    	</div>
	                    </div>
	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
                                <select class="form-control" placeholder="Country" selected="Array" name="business_country" id="business_country" type="select">
                                    {foreach from=PageContext::$response->countries key=country item=id}
                                    <option value="{$id}" {if PageContext::$response->data['business_country'] eq $id || PageContext::$request['business_country'] eq $id} selected="selected" {/if}>{$country}</option>
                                    {/foreach}
                                </select>
                                </div>
	                    		<div class="display_table_cell wid2per vlaign_middle align-right"></div>
	                    		</div>
	                    </div>
                            <div class="form-group relative">
                            	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
                                	<input type="text" class="form-control" name="business_state" id="business_state" placeholder="State" value="{PageContext::$request['business_state']}">
                                </div>
	                    		<div class="display_table_cell wid2per vlaign_middle align-right"></div>
								</div>
                            </div>
	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
	                    			<input type="text" name="business_phone" class="form-control" placeholder="Telephone" size="10" value="{PageContext::$request['business_phone']}">
	                    		</div>
	                    	
                            </div>
	                    </div>
	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
	                    			<input type="text" name="business_zipcode" class="form-control" placeholder="Zip Code" size="10" value="{PageContext::$request['business_zipcode']}">
                                </div>
                            </div>
	                    </div>
	                    <div class="form-group relative">
							<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
                                            <input type="file" class="  left" placeholder="Upload File" onchange="upload(150,150,'business_image_id');" size="10" value="" id="business_image_id" name="business_image_id">
			                    	<!--<img class="masterTooltip" title="Business Image(Supported format:jpg,jpeg,gif,png)" src="{PageContext::$response->baseUrl}modules/cms/images/help_icon.png">-->
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                	<span id="spn_asterisk_bzipcodeadd" style="color:red">*</span>
                                </div>
                            </div>
                            <div class="wid100p">
								<span id="spn_support_imgdirectory">(Supported format:jpg,jpeg,gif,png)</span>
                            </div>
                            <div class="wid100p">
								<label generated="true" id="label_image_id" for="business_image_id" class="error"></label>
                            </div>
	                    </div>
                              <div class="form-group relative">
<!--                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    Upload logo 
                                    <input type="file" class="left" placeholder="Upload File" size="10" value="" name="business_logo_id">
                             <img class="masterTooltip" src="{PageContext::$response->baseUrl}modules/cms/images/help_icon.png" title="Community Image(Supported format:jpg,jpeg,gif,png)"> 
                                <div class="clearfix"></div>
                                <span class="left" id="spn_support_logoadd">(Supported format:jpg,jpeg,gif,png)</span>
                                </div>
                                
                                 <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cimageadd" style="color:red;">*</span>
                                </div> 
                               
                            </div>-->
                        </div>
	                    <div class="form-group relative">
	                    	<input type="submit" class="btn btn-primary yellow_btn2" value="Save" name="btnSubmit" ondblclick="this.disabled=true;">
                                <input type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary yellow_btn2" value="Cancel" name="btnCancel">
	                	</div>
	                	<div class="clearfix"></div>
	                </form>
	            </div>
	    </div>
        </div>
    </div>
</div>




