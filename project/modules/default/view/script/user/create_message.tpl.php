<link type="text/css" rel="stylesheet" href="<?php echo BASE_URL?>project/styles/popup.css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery-1.8.0.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>bbf/public/js/jquery-ui-1.8.23.custom.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery.blockUI.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/fw.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.metadata.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.validate.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/app.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.colorbox.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/comment_popup.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/community.js"></script>
<script type="text/javascript">
$('#send_message').live('click',function(){
	var to_user_id = $('#to_user_id').val();
	var message_detail = $('#message_detail').val();
	var message_subject = $('#message_subject').val();
	var dataString = 'message_detail='+message_detail;
	var mainUrl = '<?php echo BASE_URL;?>';
        if(message_subject==''){
          $('#jdisplayMessage').html('<div class="clserror">Please enter message title</div>');  
        }
        else if(message_detail==''){
          $('#jdisplayMessage').html('<div class="clserror">Please enter message</div>');  
        }
        else{
         
         
	/*$.ajax({
        type    : "POST",
        dataType: 'json',
        url     : mainUrl+"user/send_message/"+to_user_id,
        data    : dataString,
        cache   : false,
        success : function(data){
            
                 alert('message sent');
                 parent.location.reload();
                 $.colorbox.close();
                  
              
        }
    });*/
	$.post(mainUrl+"user/send_message/"+to_user_id, {message_detail:message_detail,message_subject:message_subject}, function(response) {
		$('#content_msg').html('<div class="popup_success_msg success_div">Your message has been sent</div>');
		parent.location.reload();
       
	});
         
        }
});
</script>
<div id="content_msg">
    <div id="jdisplayMessage"></div>
	<label>Enter your message title</label>
	<input type="text" name="message_subject" id="message_subject" value="<?php echo (rawurldecode(PageContext::$response->message_subject)!=''?'Re:'.rawurldecode(PageContext::$response->message_subject):'');?>"/><br/><br/>
	<label>Enter your message</label>
	<textarea rows="17" cols="40" id="message_detail" name="message_detail"></textarea>
	<input type="hidden" name="to_user_id" id="to_user_id" value="<?php echo PageContext::$response->to_user_id;?>"/><br/><br/>
	<input type="button" name="send_message" id="send_message"  value="Send Message" class="yellow_btt">
</div>