<script>
    $(document).ready(function(){
         magnificPopupGroupFn();
    });
</script>
<div class="joinMessage" id="joinMessage"></div>
<div class="jmessage" id="jmessage"></div>
<div class="container">
    <div class="row">
        {if PageContext::$response->result->community_name neq ''}
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="timelineheader">
                <div class="timelineheader_right">
                    <div class="cover-wrapper"
                         style="background-image: url('{PageContext::$response->userImagePath}{PageContext::$response->community_image}');">
                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <div class="timelineheader_left">
                                <div class="timeline_left_pic_nosec"></div>
                                <h3> {PageContext::$response->result->community_name}</h3>
                            </div>
                        </div>
                        <div class="menufloattop">
                            {if PageContext::$response->sess_user_id neq PageContext::$response->result->community_created_by }
                            {if PageContext::$response->sess_user_id neq PageContext::$response->result->community_created_by && PageContext::$response->sess_user_id neq PageContext::$response->checkmembers->cmember_id}
                            <a href="#"
                               onclick="{if PageContext::$response->result->community_type == 'PRIVATE'}joinMail({PageContext::$response->result->community_id}){else}joinCommunity({PageContext::$response->result->community_id},'join'){/if}">
                                <input type="button" name="invite" value="Join" class="fontupper">
                            </a>
                            {/if}
                            {if PageContext::$response->checkmembers neq ''}
                            <a href="#"
                               onclick="joinCommunity({PageContext::$response->result->community_id},'decline')">
                                <input type="button" name="invite" value="Leave Group" class="fontupper"
                                       >
                            </a>
                            {/if}
                            {/if}
                        </div>

                        {if PageContext::$response->sess_user_id eq PageContext::$response->result->community_created_by}
                        <div class="timelineimage">
                            <form name="frmtimelineimageupload">
                                <i class="fa fa-camera" id="jGrpCam"></i><span class="name"></span>
                                <input type="file" name="jgrptimelinepic" id="jgrptimelinepic" style="display:none;">
                                <input type="hidden" value="{PageContext::$response->result->community_id}" id="group_id" name="group_id">
                            </form>
                        </div>
                        {/if}

                    </div>
                    <div class="timeline_tabmenu">
                        <ul id="myTab" class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#" class="jtab general_tab" title="tab-Feed" data-toggle="tab">About</a>
                            </li>
                            {if PageContext::$response->sess_user_id>0}
                            {if PageContext::$response->sess_user_id eq
                            PageContext::$response->result->community_created_by || PageContext::$response->sess_user_id
                            eq PageContext::$response->checkmembers->cmember_id}
                            <li class=""><a href="#" class="jtab discussion_tab" title="tab-Feed" data-toggle="tab"
                                            alias="{PageContext::$response->result->community_alias}">Discussions</a>
                            </li>

                            <li class=""><a href="#" alias="{PageContext::$response->result->community_id}"
                                            class="jtab photos_tab" title="tab-Feed" data-toggle="tab">Photos</a>
                            </li>
                            <li><a href="#" title="tab-Friends" data-toggle="tab" class="jtab jShowMembers"
                                   alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">Members({PageContext::$response->memberCount})</a>
                            </li>
                            {/if}
                            {/if}
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        {else}
        <br>
          <div class='whitebox'>No Data Found</div>
        {/if}
    </div>
</div>
<div class='clearfix'></div>
<div class='container'>
    <div class='row'>
   <div class="col-sm-4 col-md-5 col-lg-3">   
    {if count(PageContext::$response->friend_display_list) gt 0}            
            <div class="whitebox marg20top">
                <div class="hdsec">
                    <h3><i class="fa fa-user" aria-hidden="true"></i> Members <a  href="#" id="jMoreGroupphotos" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist">
                    {foreach from=PageContext::$response->friend_display_list key=id item=friendslist}
                      
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <a class="friendphotobox_blk" href="{PageContext::$response->baseUrl}timeline/{$friendslist->user_alias}">
                                {if $friendslist->file_path}
                                <div class="friendphotobox" style="background-image: url('{PageContext::$response->userImagePath}{$friendslist->file_path}');">
                                </div>
                                {else}
                                <div class="friendphotobox" style="background-image: url('{PageContext::$response->userImagePath}medium/member_noimg.jpg');">
                                </div>
                                {/if}
                                <h4>{$friendslist->user_firstname|ucfirst} {$friendslist->user_lastname|ucfirst}</h4>
                            </a>
                        </li>
                   {/foreach}
                </ul>
                </div>  
   {/if}
    {if count(PageContext::$response->gallery_display_list) gt 0}   
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-file-image-o" aria-hidden="true"></i> Photos <a href="#" id="jMoreGroupGallery" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist portfolio">
                    {foreach from=PageContext::$response->gallery_display_list  key=id item=gallerylist}
                      
                     
<!--                    <li class="display_table_cell wid32per">
                        <a class="friendphotobox_blk" href="#">
                            <div class="friendphotobox" style="background-image: url('{PageContext::$response->userImagePath}medium/{$gallerylist->news_feed_image_name}');">-->
                                
<!--                            <ul class="portfolio" class="clearfix">-->
                                    <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4 vert_align_top"><a class="friendphotobox_blk" href="{PageContext::$response->userImagePath}{$gallerylist->community_announcement_image_path}" title="{$gallerylist->community_announcement_content}"><img src="{PageContext::$response->userImagePath}{$gallerylist->community_announcement_image_path}" alt=""></a></li>
<!--                            </ul>-->
                                
<!--                            </div>
                        </a>
                    </li>-->
                    {/foreach} 
                </ul>
                <!--<div class="sidead" style="background-image: url('{PageContext::$response->ImagePath}sidead.jpg');"> </div>-->
            </div>  
    {/if}   
   
</div>
        
        <div class="col-sm-8 col-md-7 col-lg-9 marg20col jDivMain">
            <input type="hidden" id="community_alias" value="{PageContext::$response->result->community_alias}">
            <input type="hidden" id="community_id" value="{PageContext::$response->communityData->community_id}">
            <div id="jDivDisplayBusiness" class="tab-content">
                <div class="wid100p">
                </div>
                 {if PageContext::$response->result->community_name neq ''}
                <div id="myTabContent" class="tab-content whitebox">
                    <div class="tab-pane fade active in" id="tab-Feed">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <div class="wel_community">
                                    <div id="sticky-anchor"></div>
                                    <div id="sticky">
                                       
                                        <div class="mediapost marg0top profile-bnr welcome">
                                            <div class="welcome_head">
                                                <div class="groupwelcome welcome_left">
                                                    <div class="wid100per pad5top">
                                                        <h4>Welcome to <span>{PageContext::$response->result->community_name}</span>
                                                        </h4>
                                                    </div>
                                                    <div class="wid100per pad5top">
                                                        <div class="display_table wid100per">
                                                            {if PageContext::$response->org_grp eq 1}
                                                            <div class="display_table_row">
                                                                <div class="display_table_cell wid30per">
                                                                    <h5>Page</h5>
                                                                </div>
                                                                <div class="display_table_cell wid70per">
                                                                    <h5>
                                                                        <a {if PageContext::$response->result->business_alias neq ''} href="{PageContext::$response->baseUrl}page/{PageContext::$response->result->business_alias}" {/if}><span>{PageContext::$response->result->business_name}</span></a>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            {/if}
                                                            <div class="display_table_row">
                                                                <div class="display_table_cell wid30per">
                                                                    <h5>Owned By</h5>
                                                                </div>
                                                                <div class="display_table_cell wid70per">
                                                                    <h5>
                                                                        <a href="{PageContext::$response->baseUrl}timeline/{PageContext::$response->result->user_alias}"><span>{PageContext::$response->result->user_firstname} {PageContext::$response->result->user_lastname}</span></a>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="display_table_row">
                                                                <div class="display_table_cell wid30per">
                                                                    <h5>Type</h5>
                                                                </div>
                                                                <div class="display_table_cell wid70per">
                                                                    <h5><span>{if PageContext::$response->result->community_type eq 'PRIVATE'} Private {else} Public {/if}</span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="display_table_row">
                                                                <div class="display_table_cell wid30per">
                                                                    <h5>Description</h5>
                                                                </div>
                                                                <div class="display_table_cell wid70per">
                                                                    <h5 class="welcomerighttext"><span>{if PageContext::$response->result->community_description neq ''} {stripslashes(PageContext::$response->result->community_description)|substr:0:200}....{/if}</span>
                                                                    </h5>
                                                                </div>
                                                            </div>

                                                            <input type="hidden"
                                                                   value="{PageContext::$response->sess_user_id}"
                                                                   class="login_status" name="" id="login_status">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="welcome_right">
                                                    <div class="wid100per pad5top">
                                                        {if PageContext::$response->result->community_status =='A'}
                                                        {php}PageContext::renderRegisteredPostActions('rating');{/php}
                                                        {/if}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                {if PageContext::$response->sess_user_id eq PageContext::$response->result->community_created_by}    
                                    <div class="right">
                                    <a class="edititem left marg5right" href="{php} echo PageContext::$response->baseUrl;{/php}user/edit_group/{PageContext::$response->result->community_id}"><i class="fa fa-pencil"></i></a>        
                                    <a  href="{PageContext::$response->baseUrl}index/delete_community/{PageContext::$response->result->community_id}" onclick="return deleteConfirm();"
                                               class="deleteitem left"><i class="fa fa-trash"></i></a>
                                    </div>
                                {/if}
                            </div>
                            <div class="clearfix"></div>
                            
                        </div>
                    </div>
                </div>
                     {/if}
            </div>
        </div>
     