<div class="container">
    <div class="row"> 
<div class="col-sm-12 col-md-12 col-lg-12">
<div id="o-wrapper" class="o-wrapper">
<!---Menu Link----------------- -->
{if PageContext::$response->sess_user_id >0}
<div class="main">
<div class="header-main">
     <main class="o-content">
        <div class="o-container">
          
          <div class="c-buttons">
            <button id="c-button--slide-right" class="button-nav-toggle"><span class="icon">&#9776;</span></button>
          </div>
          
          <div id="github-icons"></div>

        </div><!-- /o-container -->
      </main><!-- /o-content -->    
</div>
</div>  
{/if}
</div>
<!---EOF Menu Link----------------- -->
            <h3 class="heading_h3">
				<span>Invitation</span><span class="round-search">{PageContext::$response->totalrecords}</span>
          		<div class="clear"></div>
            </h3>
            {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
              <div class="businesslist_search_blk">
              <div class="row">

             <div class="col-md-3  col-md-offset-1 m-b-xs">
                        <div class="display_table">
                            <div class="display_table_cell wid40per">Status</div>
                            <div class="display_table_cell wid60per">
                            <select class="form-control input-s-sm inline" name="filterstatus" onchange="submit();">
                                <option></option>
                                <option value='' {if PageContext::$response->filterstatus eq ''}selected="selected"{/if} >All</option>
                                <option value='A' {if PageContext::$response->filterstatus eq 'A'}selected="selected"{/if} >Accepted</option>
                                <option value='P' {if PageContext::$response->filterstatus eq 'P'}selected="selected"{/if} >Pending</option>
                                <option value='R' {if PageContext::$response->filterstatus eq 'R'}selected="selected"{/if} >Rejected</option>
                            </select>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="display_table">
                            <div class="display_table_cell wid40per">Invitation Type</div>
                            <div class="display_table_cell wid60per">
                                <select class="form-control input-s-sm inline" name="filtertype" onchange="submit();">
                                    <option value='' {if PageContext::$response->filtertype eq ''}selected="selected"{/if} >All</option>
                                    <option value='C' {if PageContext::$response->filtertype eq 'C'}selected="selected"{/if} >Community Invite</option>
                                    <option value='E' {if PageContext::$response->filtertype eq 'E'}selected="selected"{/if} >Email</option>
                                    <option value='U' {if PageContext::$response->filtertype eq 'U'}selected="selected"{/if} >Friend Request</option>
                                </select>
                            </div>
                        </div>
                    </div>
               
              <div class="col-md-4">
                    {php}PageContext::renderRegisteredPostActions('searchbox');{/php}
<!--                    <div class="input-group min_width252px"><input type="text" placeholder="Search" class="input-sm form-control"> 
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-sm btn-primary"> <i class="fa fa-search"></i></button> 
                    </span>
                    </div>-->
                </div>
                </div>
        </div>
        </div>

       
        <div class="col-sm-12 col-md-12 col-lg-12">

            <form name="frmfilter" method="get" action="{PageContext::$response->search['action']}">
            <div class="ibox-content">
    <!--             <div class="row">
                    <div class="col-sm-3 m-b-xs">
                        <div class="display_table">
                            <div class="display_table_cell wid20per">Status</div>
                            <div class="display_table_cell wid80per">
                            <select class="input-sm form-control input-s-sm inline" name="filterstatus" onchange="submit();">
                                <option></option>
                                <option value='' {if PageContext::$response->filterstatus eq ''}selected="selected"{/if} >All</option>
                                <option value='A' {if PageContext::$response->filterstatus eq 'A'}selected="selected"{/if} >Accepted</option>
                                <option value='P' {if PageContext::$response->filterstatus eq 'P'}selected="selected"{/if} >Pending</option>
                                <option value='R' {if PageContext::$response->filterstatus eq 'R'}selected="selected"{/if} >Rejected</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 m-b-xs"></div>
                    <div class="col-sm-4">
                        <div class="display_table">
                            <div class="display_table_cell wid30per">Invitation Type</div>
                            <div class="display_table_cell wid70per">
                                <select class="input-sm form-control input-s-sm inline" name="filtertype" onchange="submit();">
                                    <option value='' {if PageContext::$response->filtertype eq ''}selected="selected"{/if} >All</option>
                                    <option value='C' {if PageContext::$response->filtertype eq 'C'}selected="selected"{/if} >Community Invite</option>
                                    <option value='E' {if PageContext::$response->filtertype eq 'E'}selected="selected"{/if} >Email</option>
                                    <option value='U' {if PageContext::$response->filtertype eq 'U'}selected="selected"{/if} >Friend Request</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div> -->
				
				
				
                <div class="table-responsive marg20col">
                    <table class="table table-border">
                        <tr>
                            <th>Name <a href="{php} echo PageContext::$response->baseUrl; {/php}invitation-sent?sort=user_firstname&sortby={php} echo PageContext::$response->order; {/php}&searchtext={php} echo PageContext::$response->searchtext; {/php}&searchtype={php} echo PageContext::$response->searchtype; {/php}&page={php} echo PageContext::$response->page; {/php}&filterstatus={php} echo PageContext::$response->filterstatus; {/php}&filtertype={php} echo PageContext::$response->filtertype; {/php}">{PageContext::$response->user_firstname_sortorder}</a></th>
                            <th>Email </th>
                            <th>Status <a href="{php} echo PageContext::$response->baseUrl; {/php}invitation-sent?sort=invitation_status&sortby={php} echo PageContext::$response->order; {/php}&searchtext={php} echo PageContext::$response->searchtext; {/php}&searchtype={php} echo PageContext::$response->searchtype; {/php}&page={php} echo PageContext::$response->page; {/php}&filterstatus={php} echo PageContext::$response->filterstatus; {/php}&filtertype={php} echo PageContext::$response->filtertype; {/php}">{PageContext::$response->invitation_status_sortorder}</a></th>
                            <th>Sent On <a href="{php} echo PageContext::$response->baseUrl; {/php}invitation-sent?sort=invitation_created_on&sortby={php} echo PageContext::$response->order; {/php}&searchtext={php} echo PageContext::$response->searchtext; {/php}&searchtype={php} echo PageContext::$response->searchtype; {/php}&page={php} echo PageContext::$response->page; {/php}&filterstatus={php} echo PageContext::$response->filterstatus; {/php}&filtertype={php} echo PageContext::$response->filtertype; {/php}">{PageContext::$response->invitation_created_on_sortorder}</a></th>
                            <th>Invitation Type</th>
                        </tr>
                        {if PageContext::$response->totalrecords > 0}
                        {assign var="i" value=PageContext::$response->slno}
                        {foreach from=PageContext::$response->leads key=id item=lead}
                        <tr>
                            <td>{if $lead->invitation_receiver_id eq 0}-{else}{$lead->user_firstname} {$lead->user_lastname}{/if}</td>
                            <td>{if $lead->invitation_receiver_email neq ''}{$lead->invitation_receiver_email}{else}{$lead->user_email}{/if}</td>
                            <td>{if $lead->invitation_status == 'A'}Accepted{elseif $lead->invitation_status == 'R'}Rejected{else}Pending{/if} </td>
                            <td>{date("m/d/Y",strtotime($lead->invitation_created_on))}</td>
                            <td>{if $lead->invitation_entity == 'U'}{if $lead->invitation_type == 'U'}Friend Request{else}Email{/if}{else}Community Invite{/if}</td>
                        </tr>
                        {assign var="i" value=$i+1}
                        {/foreach} 
                        {else}
                        <tr><td colspan="5">{if PageContext::$response->filtertype eq '' && PageContext::$response->filterstatus eq '' && PageContext::$response->searchtext eq ''}No Invitations have been sent.{else}No matches found.{/if}</td></tr>
                        {/if}       
                    </table>
                </div>

            </div>
            </form>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12">
            {if PageContext::$response->totalrecords gt PageContext::$response->itemperpage}
            <div>{PageContext::$response->pagination} </div>
            {/if}
        </div>
    </div>
</div>

