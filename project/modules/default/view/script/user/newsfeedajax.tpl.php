<script type="text/javascript" src="<?php echo BASE_URL ?>project/js/common.js"></script>

<?php foreach (PageContext::$response->news_feed as $key => $feed) {

    if ($feed['community_announcement_id']) {
        ?>
        <div class="whitebox timelinebox" id="group_feed_<?php echo $feed['community_announcement_id']; ?>">
            <div class="wid100per">
                <div class="postheadtablediv">
                    <div class="post_left">
                <span class="mediapost_pic">
                    <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $feed['user_alias']; ?>">
                    <?php if ($feed['file_path']) { ?>
                        <img
                            src="<?php echo PageContext::$response->userImagePath; ?>medium/<?php echo $feed['file_path']; ?>">
                    <?php } else { ?>
                        <img src="<?php echo PageContext::$response->userImagePath; ?>medium/member_noimg.jpg">
                    <?php } ?>
                     </a>
                </span>
                    </div>
                    <div class="post_right">
                        <div class="posthead"><h4 class="media-heading"><a
                                    href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $feed['user_alias']; ?>"><?php echo $feed['user_firstname'] . " " . $feed['user_lastname']; ?></a>
                                <a href="<?php echo PageContext::$response->baseUrl; ?>group/<?php echo $feed['community_alias']; ?>"><i
                                        class="fa fa-play"
                                        aria-hidden="true"></i> <?php echo $feed['community_name']; ?>
                                </a></h4></div>
                        <div class="postsubhead">
                            <div class="postsubhead_right">
                        <span><?php if ($feed['days'] > 0) {
                                echo $feed['days'] . " days ago";
                            } else {
                                echo "Today";
                            } ?>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="postsubhead_left">
                <?php
                $reg_exUrl = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/';
                if(preg_match($reg_exUrl, $feed['community_announcement_content'],$url)){ ?>
                  
                <div class="jEmojiable"><?php echo preg_replace($reg_exUrl, "<a href='$url[0]'>$url[0]</a>" , $feed['community_announcement_content']); ?></div>

                <?php } else{ ?>

                <div class="jEmojiable"><?php echo $feed['community_announcement_content'];?></div>

                <?php } ?>
                    
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>
            <?php if ($feed['community_announcement_image_path']) { ?>
                <div class="postpic">
                    <!--                <img src="{PageContext::$response->userImagePath}{$feed['community_announcement_image_path']}" class="fancybox">-->
                    <ul class="portfolio" class="clearfix">
                        <li>
                            <a href="{PageContext::$response->userImagePath}{$feed['community_announcement_image_path']}"
                               title=""><img
                                    src="{PageContext::$response->userImagePath}{$feed['community_announcement_image_path']}"
                                    alt=""></a></li>
                    </ul>
                </div>
            <?php } ?>
            <div class="clearfix"></div>

            <div class="mediapost_user-side">
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-5">
                        <div class="likesec">
                            <div class="display_table_cell pad10_right">
                                <a href="#"
                                   class=" colorgrey_text jLikeFeedcommunity <?php if ($feed->LIKE_ID > 0) { ?>  liked <?php } ?>"
                                   id="jlike_<?php echo $feed['community_announcement_id']; ?>"
                                   aid="<?php echo $feed['community_announcement_id']; ?>"
                                   cid="<?php echo $feed['community_id']; ?>">
                                    <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                    <span
                                        id="jlikedisplay_<?php echo $feed['community_announcement_id']; ?>">Like </span></a>
                            </div>
                            <div class="display_table_cell pad10_right">
                                <i class="fa fa-comments-o icn-fnt-size"></i>
                                <a href="#" id="jCommentButton_<?php echo $feed['community_announcement_id']; ?>"
                                   aid="<?php echo $feed['community_announcement_id']; ?>"
                                   class="jCommentButton colorgrey_text">Comment</a>
                            </div>
                            <?php if ($feed['community_announcement_user_id'] == PageContext::$response->sess_user_id) { ?>
                                <div class="display_table_cell pad10_right">
                                    <i class="fa fa-trash-o icn-fnt-size"></i>
                                    <a href="#" id="jDeleteButton_<?php echo $feed['community_announcement_id']; ?>"
                                       aid="<?php echo $feed['community_announcement_id']; ?>"
                                       class="jGroupDeletetButton colorgrey_text">Delete</a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-8 col-lg-7">
                        <div class="sharefnt">
                    <div id="group_like_users_<?php echo $feed['community_announcement_id'];?>" class="like_users_div" style=" display: none;">
                      <?php foreach(PageContext::$response->newsFeedLikeUsers[$feed['community_announcement_id']] as $key=>$val ){ ?>
                      <span><?php echo $val->user_name;?><br></span>
                      <?php } ?>  
                    </div>
                            <span class="mediapost_links jShowGroupLikeUsers"
                                  id="jcountlike_<?php echo $feed['community_announcement_id']; ?>"><?php if ($feed['community_announcement_num_likes'] > 0) {
                                    echo $feed['community_announcement_num_likes'] . " Likes";
                                } ?></span>
                    <span class="mediapost_links jCommentButton count_class"
                          aid="<?php echo $feed['community_announcement_id']; ?>"
                          id="jcountcomment_<?php echo $feed['community_announcement_id']; ?>"><?php if ($feed['community_announcement_num_comments'] > 0) {
                            echo $feed['community_announcement_num_comments'] .
                                "Comments";
                        } ?>
                    </span>
                            <div class="fb-share-button"
                                 id="share_button_<?php echo $feed['community_announcement_id']; ?>"
                                 data-href="<?php echo PageContext::$response->baseUrl; ?>announcement-detail/<?php echo $feed['community_announcement_alias']; ?>"
                                 data-title="Shared on facebook" data-layout="button_count"
                                 data-desc="You can share data"
                                 data-url="<?php echo PageContext::$response->baseUrl; ?>newsfeed-detail/<?php echo $feed['community_announcement_alias']; ?>"></div>
                            <a href="<?php echo PageContext::$response->baseUrl; ?>announcement-detail/<?php echo $feed['community_announcement_alias']; ?>"
                               title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="mediapost_user-side-top clear jCommentDisplayDiv"
                 id="jCommentBoxDisplayDiv_<?php echo $feed['community_announcement_id']; ?>" style="display:none">
                <p class="lead emoji-picker-container position_relatv">
                    <div class="emotionsbtn_textblk">
                        <textarea class="form-control textarea-control jEmojiTextarea"
                                  id="watermark_<?php echo $feed['community_announcement_id']; ?>" rows="3"
                                  placeholder="Write your comment here..."></textarea>
                        <a class="upload_cambtn" id="<?php echo $feed['community_announcement_id']; ?>">
                            <label for="file_<?php echo $feed['community_announcement_id']; ?>"><i
                                    class="fa fa-camera feed-camera"></i></label></a>
                        <input type="file" id="file_<?php echo $feed['community_announcement_id']; ?>"
                               aid="<?php echo $feed['community_announcement_id']; ?>" class="jimage_file"
                               style="cursor: pointer;  display: none"/>
                        <input type="hidden" id="announcement_id" value="<?php echo $feed['community_announcement_id']; ?>">
                    </div>
                </p>
                <a id="postButton" cid="<?php echo $feed['community_id']; ?>"
                   cmid="<?php echo $feed['announcement_comment_id']; ?>"
                   aid="<?php echo $feed['community_announcement_id']; ?>"
                   class="btn yellow_btn fontupper pull-right jPostCommentButtonCommunity"> Post</a>
            </div>
            <div class="loading" id="loading_<?php echo $feedlist['community_announcement_id']; ?>"
                 style="display: none;">
                <img src="{PageContext::$response->userImagePath}default/loader.gif"/>
            </div>
            <div id="juploadimagepreview_<?php echo $feed['community_announcement_id']; ?>"></div>
            <div class="clear commentblk jCommentDisplayDiv"
                 id="jCommentDisplayDiv_<?php echo $feed['community_announcement_id']; ?>" style="display:none">
                <div id="posting_<?php echo $feed['community_announcement_id']; ?>">
                    <!------------------------Comments-------------------------------->
                    <?php $val = 0;
                    foreach (PageContext::$response->newsFeed[$feed['community_announcement_id']] as $key => $comments) {
                        $val++; 
                        if($comments->announcement_comment_id){
                        ?>
                        <input type="hidden" id="val_<?php echo $feed['community_announcement_id']; ?>"
                               value="<?php echo count(PageContext::$response->newsFeed[$feed['community_announcement_id']]); ?>">

                        <input type="text" id="hid_announcement_id_<?php echo $comments->announcement_comment_id; ?>"
                               class="hid_announcement_id" value="<?php echo $feed['community_announcement_id']; ?>"
                               style="display: none;">
                        <input type="text" id="hid_<?php echo $comments->announcement_comment_id; ?>"
                               value="<?php echo $val; ?>" style="display: none;">
                        <div class="col-md-12 btm-mrg pad10p"
                             id="jdivComment_<?php echo $comments->announcement_comment_id; ?>">
                            <div class="row">
                                <div class="col-md-1">
                                    <span class="mediapost_pic">
                                        <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $comments->user_alias; ?>">
                                        <?php if ($comments->user_image != '') { ?>
                                            <img class="ryt-mrg"
                                                 src="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->user_image; ?>">
                                        <?php } else { ?>
                                            <img class="ryt-mrg"
                                                 src="<?php echo PageContext::$response->userImagePath; ?>medium/member_noimg.jpg">
                                        <?php } ?>
                                        </a>
                                    </span>
                                </div>
                                <div class="col-md-11">
                                    <span
                                        class="name">
                                             <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $comments->user_alias; ?>"><?php echo $comments->Username; ?></a></span>
                                    <span class="jEmojiable"><?php echo $comments->comment_content; ?></span>
                                    <div class="clearfix"></div>
                                        <span class="new-text">

                                        <?php echo $comments->commentDate; ?>
                                    </span>
                                    <div class="commentimg_box">
                                        <?php if ($comments->file_path != '') { ?>
                                            <!--                                        <img class="fancybox img-responsive center" src="<?php //echo PageContext::$response->userImagePath;?>medium/<?php //echo $comments->file_path;?>">-->
                                            <ul class="portfolio" class="clearfix">
                                                <li>
                                                    <a href="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->file_path; ?>"
                                                       title=""><img
                                                            src="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->file_path; ?>"
                                                            alt=""></a></li>
                                            </ul>

                                        <?php } ?>
                                    </div>
                                    <div class="mediapost_user-side1">
                                        <span rel="tooltip" title="<?php echo $comments->users;?> liked this comment" class="mediapost_links"
                                              id="jcountcommentlike_<?php echo $comments->announcement_comment_id; ?>"><?php if ($comments->num_comment_likes > 0) {
                                                echo $comments->num_comment_likes . " Likes";
                                            } ?></span>
                                        <input type="hidden"
                                               id="reply_<?php echo $comments->announcement_comment_id; ?>"
                                               value="<?php if ($comments->num_replies > 0) {
                                                   echo $comments->num_replies;
                                               } else {
                                                   echo "0";
                                               } ?>">
                                        <span class="mediapost_links "
                                              cid="<?php echo $comments->announcement_comment_id; ?>"
                                              id="<?php echo $comments->announcement_comment_id; ?>"><a href="#"
                                                                                                        class="jShowReply"
                                                                                                        id="<?php echo $comments->announcement_comment_id; ?>"><span
                                                    id="jcountreply_<?php echo $comments->announcement_comment_id; ?>"><?php if ($comments->num_replies > 0) {
                                                        echo $comments->num_replies . " Replies";
                                                    } ?></span></a></span>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="comment_sharelike_sec">
                                        <?php if ($comments->user_id == PageContext::$response->sess_user_id || $comments->community_created_by == PageContext::$response->sess_user_id) { ?>
                                            <a href="#" class="jFeedCommentDelete marg10right"
                                               aid="<?php echo $feed['community_announcement_id']; ?>"
                                               id="<?php echo $comments->announcement_comment_id; ?>"><i
                                                    class="fa fa-times"></i> Delete</a>
                                        <?php } ?>
                                        <a href="#" class="jFeedCommentReply marg10right"
                                           aid="<?php echo $feed['community_announcement_id']; ?>"
                                           cid="<?php echo $comments->announcement_comment_id; ?>"
                                           id="<?php echo $comments->announcement_comment_id; ?>"><i
                                                class="fa fa-reply"></i> Reply</a>
                                        <a href="#"
                                           class="jFeedCommentLike <?php if ($comments->LIKE_ID > 0) { ?> liked <?php } ?>"
                                           id="jlikeComment_<?php echo $comments->announcement_comment_id; ?>"
                                           cid="<?php echo $comments->announcement_comment_id; ?>"
                                           aid="<?php echo $feed['community_announcement_id']; ?>">

                                            <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                            <span
                                                id="jlikedisplaycomment_<?php echo $comments->announcement_comment_id; ?>">Like </span></a>
                                    </div>
                                    </span>
                                    <div id="jdivReply_<?php echo $comments->announcement_comment_id; ?>"
                                         class="jDisplayReply">
                                        <p class="lead emoji-picker-container position_relatv">
                                            <div class="emotionsbtn_textblk">
                                                <textarea placeholder="Write your reply here..."
                                                          class="form-control textarea-control jEmojiTextarea"
                                                          style="height:35px;"
                                                          id="watermark_<?php echo $feed['community_announcement_id']; ?>_<?php echo $comments->announcement_comment_id; ?>"
                                                          class="watermark" name="watermark"></textarea>

                                                <a class="upload_cambtn"
                                                   id="<?php echo $feed['community_announcement_id']; ?>">
                                                    <label
                                                        for="imagereply_file_<?php echo $comments->announcement_comment_id; ?>"><i
                                                            class="fa fa-camera feed-reply-camera"></i></label>
                                                    <div class="file_button"><input type="file"
                                                                                    id="imagereply_file_<?php echo $comments->announcement_comment_id; ?>"
                                                                                    cid="<?php echo $comments->announcement_comment_id; ?>"
                                                                                    class="jcommunityimagereply_file"
                                                                                    style="display:none;"/></div>
                                                </a>
                                            </div>
                                        </p>
                                            <a id="shareButton" cid="<?php echo $feed['community_id']; ?>"
                                           cmid="<?php echo $comments->announcement_comment_id; ?>"
                                           aid="<?php echo $feed['community_announcement_id']; ?>"
                                           class="btn yellow_btn fontupper pull-right jPostCommentButtonCommunity">
                                            Post</a>
                                    </div>
                                    <div class="loader"
                                         id="imagereply_loader_<?php echo $comments->announcement_comment_id; ?>">
                                        <img
                                            src="<?php echo PageContext::$response->userImagePath; ?>default/loader.gif"/>
                                    </div>
                                    <div
                                        id="jDivReplyImagePreview_<?php echo $comments->announcement_comment_id; ?>"></div>
                                    <div id="postingReply_<?php echo $comments->announcement_comment_id; ?>"></div>
                                </div>
                            </div>
                        </div>

                    <?php }} ?>
                    <?php if (!empty(PageContext::$response->Comments[$announcement->community_announcement_id])) {
                        if (PageContext::$response->Page[$announcement->community_announcement_id]['totpages'] != PageContext::$response->Page[$announcement->community_announcement_id]['currentpage']) {
                            ?>
                            <div id="jDisplayMoreComments_<?php echo $announcement->community_announcement_id; ?>"><a
                                    href="#" id="jMorecommentsButton"
                                    pid="<?php echo PageContext::$response->Page[$announcement->community_announcement_id]['currentpage']; ?>"
                                    aid="<?php echo $announcement->community_announcement_id; ?>">Load more comments</a>
                            </div>
                        <?php }

                    } ?>
                    <!------------------------EOF Comments-------------------------------->
                </div>
            </div>
        </div>
    <?php } ?>


    <?php if ($feed['organization_announcement_id']) { ?>

        <div class="whitebox" id="org_feed_<?php echo $feed['organization_announcement_id']; ?>">
            <div class="wid100per">
                <div class="postheadtablediv">
                    <div class="post_left">
                    <span class="mediapost_pic">
                     <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $feed['user_alias']; ?>">
                    <?php if ($feed['file_path']) { ?>
                        <img
                            src="<?php echo PageContext::$response->userImagePath; ?>medium/<?php echo $feed['file_path']; ?>">
                    <?php } else { ?>
                        <img src="<?php echo PageContext::$response->userImagePath; ?>medium/member_noimg.jpg">
                    <?php } ?>
                     </a>
                    </span>
                    </div>
                    <div class="post_right">
                        <div class="posthead"><h4 class="media-heading"><a
                                    href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $feed['user_alias']; ?>"><?php echo $feed['user_firstname'] . " " . $feed['user_lastname']; ?></a>
                                <a href="<?php echo PageContext::$response->baseUrl; ?>page/<?php echo $feed['business_alias']; ?>"><i
                                        class="fa fa-play"
                                        aria-hidden="true"></i> <?php echo $feed['business_name']; ?></a></h4>
                        </div>
                        <div class="postsubhead">
                            <div class="postsubhead_right">
                            <span><?php if ($feed['days'] > 0) {
                                    echo $feed['days'] . " days ago";
                                } else {
                                    echo "Today";
                                }
                                ?>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="postsubhead_left">
                    <?php
                $reg_exUrl = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/';
                if(preg_match($reg_exUrl, $feed['organization_announcement_content'],$url)){ ?>
                  
                <div class="jEmojiable"><?php echo preg_replace($reg_exUrl, "<a href='$url[0]'>$url[0]</a>" , $feed['organization_announcement_content']);?></div>

                <?php }else{ ?>

                <div class="jEmojiable"><?php echo $feed['organization_announcement_content'];?></div>

                <?php } ?>
                    
                </div>
                <div class="clearfix"></div>


                <?php if ($feed['organization_announcement_image_path']) { ?>
                    <div class="postpic">
                        <!--                                                                <img src="<?php //echo PageContext::$response->userImagePath;?>medium/<?php //echo $feed['organization_announcement_image_path'];?>" class="fancybox">-->
                        <ul class="portfolio" class="clearfix">
                            <li>
                                <a href="<?php echo PageContext::$response->userImagePath; ?>medium/<?php echo $feed['organization_announcement_image_path']; ?>"
                                   title=""><img
                                        src="<?php echo PageContext::$response->userImagePath; ?>medium/<?php echo $feed['organization_announcement_image_path']; ?>"
                                        alt=""></a></li>
                        </ul>

                    </div>
                <?php } ?>
                <div class="clearfix"></div>

                <div class="mediapost_user-side">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-lg-5">
                            <div class="likesec">
                                <div class="display_table_cell pad10_right">
                                    <a href="#"
                                       class=" colorgrey_text jLikeFeedOrganization <?php if ($feed['LIKE_ID'] > 0) { ?> liked <?php } ?>"
                                       id="jlike_<?php echo $feed['organization_announcement_id']; ?>"
                                       aid="<?php echo $feed['organization_announcement_id']; ?>"
                                       cid="<?php echo $feed['organization_id']; ?>">
                                        <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                        <span id="jlikedisplay_<?php echo $feed['organization_announcement_id']; ?>">Like </span></a>
                                </div>
                                <div class="display_table_cell pad10_right">
                                    <i class="fa fa-comments-o icn-fnt-size"></i>
                                    <a href="#" id="jCommentButton_<?php echo $feed['organization_announcement_id']; ?>"
                                       aid="<?php echo $feed['organization_announcement_id']; ?>"
                                       class="jOrgCommentButton colorgrey_text">Comment</a>
                                </div>
                                <?php if ($feed['organization_announcement_user_id'] == PageContext::$response->sess_user_id) { ?>
                                    <div class="display_table_cell pad10_right">
                                        <i class="fa fa-trash-o icn-fnt-size"></i>
                                        <a href="#"
                                           id="jDeleteButton_<?php echo $feed['organization_announcement_id']; ?>"
                                           aid="<?php echo $feed['organization_announcement_id']; ?>"
                                           class="jOrgDeletetButton colorgrey_text">Delete</a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-8 col-lg-7">
                            <div class="sharefnt">
                       <div id="org_like_users_<?php echo $feed['organization_announcement_id'];?>" class="like_users_div" style=" display: none;">
                      <?php foreach(PageContext::$response->newsFeedLikeUsers[$feed['organization_announcement_id']] as $key=>$val) {?>
                      <span><?php echo $val->user_name;?><br></span>
                      <?php } ?>  
                    </div>
                                <span class="mediapost_links jShowOrgLikeUsers"
                                      id="jcountlike_<?php echo $feed['organization_announcement_id']; ?>"><?php if ($feed['organization_announcement_num_likes'] > 0) {
                                        echo $feed['organization_announcement_num_likes'] . " Likes";
                                    } ?></span>
                    <span class="mediapost_links jOrgCommentButton count_class"
                          aid="<?php echo $feed['organization_announcement_id']; ?>"
                          id="jcountcomment_<?php echo $feed['organization_announcement_id']; ?>"><?php if ($feed['organization_announcement_num_comments'] > 0) {
                            echo $feed['organization_announcement_num_comments'] .
                                "Comments";
                        }
                        ?>
                    </span>
                                <div class="fb-share-button" id="share_button_<?php echo $feed['news_feed_id']; ?>"
                                     data-href="<?php echo PageContext::$response->baseUrl; ?>announcement-detail/<?php echo $feed['organization_announcement_alias']; ?>"
                                     data-title="Shared on facebook" data-layout="button_count"
                                     data-desc="You can share data"
                                     data-url="<?php echo PageContext::$response->baseUrl; ?>newsfeed-detail/<?php echo $feed['news_feed_alias']; ?>"></div>
                                <a href="<?php echo PageContext::$response->baseUrl; ?>announcement-detail/<?php echo $announcement['organization_announcement_alias']; ?>"
                                   title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="mediapost_user-side-top clear jCommentDisplayDiv"
                     id="jCommentBoxDisplayDiv_<?php echo $feed['organization_announcement_id']; ?>"
                     style="display:none">
                    <p class="lead emoji-picker-container position_relatv">\
                        <div class="emotionsbtn_textblk">
                            <textarea class="form-control textarea-control jEmojiTextarea"
                                      id="watermark_<?php echo $feed['organization_announcement_id']; ?>" rows="3"
                                      placeholder="Write your comment here..."></textarea>
                            <a class="upload_cambtn" id="<?php echo $feed['organization_announcement_id']; ?>">
                                <label for="file_<?php echo $feed['organization_announcement_id']; ?>"><i
                                        class="fa fa-camera feed-camera"></i></label></a>
                            <input type="file" id="file_<?php echo $feed['organization_announcement_id']; ?>"
                                   aid="<?php echo $feed['organization_announcement_id']; ?>" class="jorgimage_file"
                                   style="cursor: pointer;  display: none"/>
                            <input type="hidden" id="organization_announcement_id"
                                   value="<?php echo $feed['organization_announcement_id']; ?>">
                        </div>
                    </p>
                    <a id="postButton" cid="<?php echo $feed['organization_id']; ?>"
                       cmid="<?php echo $feed['announcement_comment_id']; ?>"
                       aid="<?php echo $feed['organization_announcement_id']; ?>"
                       class="btn yellow_btn fontupper pull-right jPostCommentButtonOrganization"> Post</a>
                </div>
                <div class="loading" id="loading_<?php echo $feedlist['community_announcement_id']; ?>"
                     style="display: none;">
                    <img src="<?php echo PageContext::$response->userImagePath; ?>default/loader.gif"/>
                </div>
                <div id="juploadimagepreview_<?php echo $feed['organization_announcement_id']; ?>"></div>
                <div class="clear commentblk jCommentDisplayDiv"
                     id="jCommentDisplayDiv_<?php echo $feed['organization_announcement_id']; ?>" style="display:none">
                    <div id="posting_<?php echo $feed['organization_announcement_id']; ?>">
                        <!------------------------Comments-------------------------------->
                        <?php $val = 0;

                        foreach (PageContext::$response->newsFeed[$feed['organization_announcement_id']] as $key => $comments) {
                            $val++; 
                            if($comments->organization_announcement_comment_id){
                            ?>
                            <input type="hidden" id="val_<?php echo $feed['organization_announcement_id']; ?>"
                                   value="<?php echo count(PageContext::$response->newsFeed[$feed['organization_announcement_id']]); ?>">

                            <input type="text"
                                   id="hid_announcement_id_<?php echo $comments->organization_announcement_comment_id; ?>"
                                   class="hid_announcement_id"
                                   value="<?php echo $feed['organization_announcement_id']; ?>" style="display: none;">
                            <input type="text" id="hid_<?php echo $comments->organization_announcement_comment_id; ?>"
                                   value="<?php echo $val; ?>" style="display: none;">
                            <div class="col-md-12 btm-mrg pad10p"
                                 id="jdivComment_<?php echo $comments->organization_announcement_comment_id; ?>">
                                <div class="row">
                                    <div class="col-md-1">
                                        <span class="mediapost_pic">
                                            <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $comments->user_alias; ?>">
                                            <?php if ($comments->user_image != '') { ?>
                                                <img class="ryt-mrg"
                                                     src="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->user_image; ?>">
                                            <?php } else { ?>
                                                <img class="ryt-mrg"
                                                     src="<?php echo PageContext::$response->userImagePath; ?>medium/member_noimg.jpg">
                                            <?php } ?>
                                            </a>
                                        </span>
                                    </div>
                                    <div class="col-md-11">
                                        <span
                                            class="name">
                                                 <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $comments->user_alias; ?>"><?php echo $comments->user_name; ?></a> </span>
                                        <span class="jEmojiable"><?php echo $comments->comment_content; ?></span>
                                        <div class="clearfix"></div>
                                            <span class="new-text">

                                            <?php echo $comments->commentDate; ?>
                                        </span>
                                        <div class="commentimg_box">
                                            <?php if ($comments->file_path != '') { ?>
                                                <!--                                            <img class="fancybox img-responsive center" src="<?php //echo PageContext::$response->userImagePath;?>medium/<?php //echo $comments->file_path;?>">-->
                                                <ul class="portfolio" class="clearfix">
                                                    <li>
                                                        <a href="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->file_path; ?>"
                                                           title=""><img
                                                                src="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->file_path; ?>"
                                                                alt=""></a></li>
                                                </ul>


                                            <?php } ?>
                                        </div>
                                        <div class="mediapost_user-side1">
                                            <span rel="tooltip" title="<?php echo $comments->users;?> liked this comment" class="mediapost_links"
                                                  id="jcountcommentlike_<?php echo $comments->organization_announcement_comment_id; ?>"><?php if ($comments->num_comment_likes > 0) {
                                                    echo $comments->num_comment_likes . " Likes";
                                                } ?></span>
                                            <input type="hidden"
                                                   id="reply_<?php echo $comments->organization_announcement_comment_id; ?>"
                                                   value="<?php if ($comments->num_replies > 0) {
                                                       echo $comments->num_replies;
                                                   } else {
                                                       echo "0";
                                                   } ?>">
                                            <div class="mediapost_links "
                                                 cid="<?php echo $comments->organization_announcement_comment_id; ?>"
                                                 id="<?php echo $comments->organization_announcement_comment_id; ?>">
                                                <a href="#" class="jOrgShowReply"
                                                   id="<?php echo $comments->organization_announcement_comment_id; ?>">
                                                    <span
                                                        id="jcountreply_<?php echo $comments->organization_announcement_comment_id; ?>"><?php if ($comments->num_replies > 0) {
                                                            echo $comments->num_replies . " Replies";
                                                        } ?></span>
                                                </a>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="comment_sharelike_sec">
                                            <?php if ($comments->user_id == PageContext::$response->sess_user_id || $comments->community_created_by == PageContext::$response->sess_user_id) { ?>
                                                <a href="#" class="jOrgFeedCommentDelete marg10right"
                                                   aid="<?php echo $feed['organization_announcement_id']; ?>"
                                                   id="<?php echo $comments->organization_announcement_comment_id; ?>"><i
                                                        class="fa fa-times"></i> Delete</a>
                                            <?php } ?>
                                            <a href="#" class="jFeedCommentReply marg10right"
                                               aid="<?php echo $feed['organization_announcement_id']; ?>"
                                               cid="<?php echo $comments->organization_announcement_comment_id; ?>"
                                               id="<?php echo $comments->organization_announcement_comment_id; ?>"><i
                                                    class="fa fa-reply"></i> Reply</a>
                                            <a href="#"
                                               class="jOrgFeedCommentLike <?php if ($comments->LIKE_ID > 0) { ?>  liked <?php } ?>"
                                               id="jlikeComment_<?php echo $comments->organization_announcement_comment_id; ?>"
                                               cid="<?php echo $comments->organization_announcement_comment_id; ?>"
                                               aid="<?php echo $feed['organization_announcement_id']; ?>">

                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                                <span
                                                    id="jlikedisplaycomment_<?php echo $comments->organization_announcement_comment_id; ?>">Like </span></a>
                                        </div>
                                        </span>
                                        <div
                                            id="jdivReply_<?php echo $comments->organization_announcement_comment_id; ?>"
                                            class="jDisplayReply">
                                            <p class="lead emoji-picker-container position_relatv">
                                                <div class="emotionsbtn_textblk">
                                                    <textarea placeholder="Write your reply here..."
                                                              class="form-control textarea-control jEmojiTextarea"
                                                              style="height:35px;"
                                                              id="watermark_<?php echo $feed['organization_announcement_id']; ?>_<?php echo $comments->organization_announcement_comment_id; ?>"
                                                              class="watermark" name="watermark"></textarea>

                                                    <a class="upload_cambtn"
                                                       id="<?php echo $feed['organization_announcement_id']; ?>">
                                                        <label
                                                            for="imagereply_file_<?php echo $comments->organization_announcement_comment_id; ?>"><i
                                                                class="fa fa-camera feed-reply-camera"></i></label>
                                                        <div class="file_button"><input type="file"
                                                                                        id="imagereply_file_<?php echo $comments->organization_announcement_comment_id; ?>"
                                                                                        cid="<?php echo $comments->organization_announcement_comment_id; ?>"
                                                                                        class="jorgimagereply_file"
                                                                                        style="display:none;"/></div>
                                                    </a>
                                                </div>
                                            </p>
                                                <a id="shareButton" cid="<?php echo $feed['community_id']; ?>"
                                               cmid="<?php echo $comments->organization_announcement_comment_id; ?>"
                                               aid="<?php echo $feed['organization_announcement_id']; ?>"
                                               class="btn yellow_btn fontupper pull-right jPostCommentButtonOrganization">
                                                Post</a>
                                        </div>
                                        <div class="loader"
                                             id="imagereply_loader_<?php echo $comments->organization_announcement_comment_id; ?>">
                                            <img
                                                src="<?php echo PageContext::$response->userImagePath; ?>default/loader.gif"/>
                                        </div>
                                        <div
                                            id="jDivReplyImagePreview_<?php echo $comments->organization_announcement_comment_id; ?>"></div>
                                        <div
                                            id="postingReply_<?php echo $comments->organization_announcement_comment_id; ?>"></div>
                                    </div>
                                </div>
                            </div>
                        <?php }} ?>


                        <?php if (!empty(PageContext::$response->Comments[$announcement['organization_announcement_id']])) {
                            if (PageContext::$response->Page[$announcement['organization_announcement_id']]['totpages'] != PageContext::$response->Page[$announcement['organization_announcement_id']]['currentpage']) {
                                ?>
                                <div
                                    id="jDisplayMoreComments_<?php echo $announcement['organization_announcement_id']; ?>">
                                    <a href="#" id="jMorecommentsButton"
                                       pid="<?php echo PageContext::$response->Page[$announcement['organization_announcement_id']]['currentpage']; ?>"
                                       aid="<?php echo $announcement['organization_announcement_id']; ?>">Load more
                                        comments</a></div>
                            <?php }

                        } ?>
                        <!------------------------EOF Comments-------------------------------->
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if ($feed['news_feed_id']) { ?>

        <div class="whitebox" id="news_feed_<?php echo $feed['news_feed_id']; ?>">
            <div class="wid100per">
                <div class="postheadtablediv">
                    <div class="post_left">
                <span class="mediapost_pic">
                    <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $feed['user_alias']; ?>">
                    <?php if ($feed['file_path'] != '') { ?>
                        <img
                            src="<?php echo PageContext::$response->userImagePath; ?><?php echo $feed['file_path']; ?>">
                    <?php } else { ?>
                        <img src="<?php echo PageContext::$response->userImagePath; ?>/small/member_noimg.jpg">
                    <?php } ?>
                     </a>
                </span>
                    </div>
                    <div class="post_right">
                        <div class="posthead"><h4 class="media-heading"><a
                                    href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $feed['user_alias']; ?>"><?php echo $feed['Username']; ?></a>
                                <span></span></h4></div>
                        <div class="postsubhead">
                            <div class="postsubhead_right"><span>
                            <?php
                            if ($feed['days'] > 0) {
                                echo $feed['days'] . " days ago";
                            } else {
                                $timesplit = explode(":", $feed['output_time']);
                                if ($timesplit[0] > 0) {
                                    echo ltrim($timesplit[0], '0') . " hrs ago";
                                } else {
                                    if ($timesplit[1] > 0) {
                                        echo ltrim($timesplit[1], '0');
                                        if ($timesplit[1] == 1) {
                                            echo "min";
                                        } else {
                                            echo "mins";
                                        }
                                        echo "ago";
                                    } else {
                                        echo "Just Now";
                                    }
                                }
                            }
                            ?>
                        </span></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="postsubhead_left">
                        <?php
                $reg_exUrl = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/';
                if(preg_match($reg_exUrl, $feed['news_feed_comment'],$url)){ ?>
                  
                <div class="jEmojiable"><?php echo preg_replace($reg_exUrl, "<a href='$url[0]'>$url[0]</a>" , $feed['news_feed_comment']);?></div>

                <?php 
                
                }else { ?>

                <div class="jEmojiable"><?php echo $feed['news_feed_comment'];?></div>

                <?php } ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <a href="#">
                <?php if ($feed['news_feed_image_name']) {
                    $img = $feed['news_feed_image_name'];
                    ?>
                    <div class="postpic">
                        <!--                <img src="<?php //echo PageContext::$response->userImagePath;?>medium/<?php //echo $img;?>"  class="fancybox img-responsive">-->
                        <ul class="portfolio" class="clearfix">
                            <li><a href="<?php echo PageContext::$response->userImagePath; ?>medium/<?php echo $img; ?>"
                                   title=""><img
                                        src="<?php echo PageContext::$response->userImagePath; ?>medium/<?php echo $img; ?>"
                                        alt=""></a></li>
                        </ul>


                    </div>
                <?php } ?>
            </a>
            <!----------------Statistics ------------------------>
            <div class="mediapost_user-side">
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-5">
                        <div class="likesec">
                            <div class="display_table_cell pad10_right">
                                <a href="#"
                                   class=" colorgrey_text jLikeNewsFeed <?php if ($feed['news_feed_num_like'] > 0) { ?> liked <?php } ?>"
                                   id="jlike_<?php echo $feed['news_feed_id']; ?>"
                                   cid="<?php echo $feed['news_feed_id']; ?>">
                                    <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                    <span id="jlikedisplay_<?php echo $feed['news_feed_id']; ?>">Like </span></a>
                            </div>
                            <div class="display_table_cell pad10_right">
                                <i class="fa fa-comments-o icn-fnt-size"></i>
                                <a href="#" id="jCommentButton_<?php echo $feed['news_feed_id']; ?>"
                                   aid="<?php echo $feed['news_feed_id']; ?>"
                                   class="jNewsfeedCommentButton colorgrey_text">Comment</a>
                            </div>
                            <?php if ($feed['news_feed_user_id'] == PageContext::$response->sess_user_id) { ?>
                                <div class="display_table_cell pad10_right">
                                    <i class="fa fa-trash-o icn-fnt-size"></i>
                                    <a href="#" id="jDeleteButton_<?php echo $feed['news_feed_id']; ?>"
                                       aid="<?php echo $feed['news_feed_id']; ?>"
                                       class="jNewsfeedDeletetButton colorgrey_text">Delete</a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-8 col-lg-7">
                        <div class="sharefnt">
                       <div id="feed_like_users_<?php echo $feed['news_feed_id'];?>" class="like_users_div" style=" display: none;">
                      <?php foreach(PageContext::$response->newsFeedLikeUsers[$feed['news_feed_id']] as  $key=>$val){?> 
                      <span><?php echo $val->news_feed_like_user_name;?><br></span>
                      <?php } ?> 
                    </div>
                            <span class="mediapost_links jShowFeedLikeUsers"
                                  id="jcountlike_<?php echo $feed['news_feed_id'];?>"><?php if ($feed['news_feed_num_like'] > 0) {
                                    echo $feed['news_feed_num_like']; ?> Likes <?php } ?></span>
                                                <span class="mediapost_links jNewsfeedCommentButton count_class"
                                                      aid="<?php echo $feed['news_feed_id']; ?>"
                                                      id="jcountcomment_<?php echo $feed['news_feed_id']; ?>"><?php if ($feed['news_feed_num_comments'] > 0) {
                                                        echo $feed['news_feed_num_comments']; ?>
                                                        Comments
                                                    <?php } ?>
                                                </span>
                            <div class="fb-share-button" id="share_button_<?php echo $feed['news_feed_id']; ?>"
                                 data-href="<?php echo PageContext::$response->baseUrl; ?>newsfeed-detail/<?php echo $feed['news_feed_alias']; ?>"
                                 data-title="Shared on facebook" data-layout="button_count"
                                 data-desc="You can share data"
                                 data-url="<?php echo PageContext::$response->baseUrl; ?>newsfeed-detail/<?php echo $feed['news_feed_alias']; ?>">
                                Share
                            </div>

                            <a href="<?php echo PageContext::$response->baseUrl; ?>newsfeed-detail/<?php echo $feed['news_feed_alias']; ?>"
                               title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!----------------End of  Statistics ------------------------>
            <!---------------Share buttons-------------------------------->

            <!---------------End of Share buttons-------------------------------->
            <div class="mediapost_user-side-top clear jCommentDisplayDiv"
                 id="jNewsfeedCommentBoxDisplayDiv_<?php echo $feed['news_feed_id']; ?>" style="display:none">
                <p class="lead emoji-picker-container">
                    <div class="emotionsbtn_textblk">
                        <textarea cid="<?php echo $feedlist['community_id']; ?>" cmid=""
                                  aid="<?php echo $feed['news_feed_id']; ?>"
                                  class="form-control textarea-control jtxtSearchProdAssign jEmojiTextarea"
                                  id="watermark_<?php echo $feed['news_feed_id']; ?>" rows="3"
                                  placeholder="Write your comment here..."></textarea>
                        <a class="upload_cambtn" id="<?php echo $feedlist['community_announcement_id']; ?>">
                            <label for="file_<?php echo $feed['news_feed_id']; ?>"><i class="fa fa-camera feed-camera"></i></label></a>
                        <input type="file" id="file_<?php echo $feed['news_feed_id']; ?>"
                               aid="<?php echo $feed['news_feed_id']; ?>"
                               cmid="<?php echo $feed['news_feed_comment_id']; ?>" class="jfeedimageimage_file"
                               style="cursor: pointer;  display: none"/>
                        <input type="hidden" id="announcement_id"
                               value="<?php echo $feedlist['community_announcement_id']; ?>">
                    </div>
                </p>
                <div class="display_image_div">
                    <div id="juploadcommentfeedimagepreview_<?php echo $feed['news_feed_id']; ?>"></div>
                </div>
                <div class="msg_display" id="msg_display_<?php echo $feed['news_feed_id']; ?>"></div>
                <div class="clearfix"></div>
                <!--                                         {PageContext::$response->Comments|@print_r}-->
                <a id="postButton" cid="<?php echo $feedlist['community_id']; ?>" cmid=""
                   aid="<?php echo $feed['news_feed_id']; ?>"
                   class="btn yellow_btn fontupper pull-left jPostNewsfeedCommentButton"> Post</a>
            </div>
            <div class="clear commentblk jCommentDisplayDiv"
                 id="jNewsfeedCommentDisplayDiv_<?php echo $feed['news_feed_id']; ?>" style="display:none">
                <div id="posting_<?php echo $feed['news_feed_id']; ?>">
                    <!------------------------Comments-------------------------------->

                    <?php foreach (PageContext::$response->newsFeed[$feed['news_feed_id']] as $key => $comments) {
                        if ($comments->news_feed_comments_id) {
                            ?>
                            <input type="hidden" id="val_<?php echo $feed['news_feed_id']; ?>"
                                   value="<?php echo count(PageContext::$response->newsFeed[$feed['news_feed_id']]); ?>">

                            <div class="col-md-12 btm-mrg pad10p" id="jdivComment_<?php echo $comments->news_feed_comments_id;?>">
                                <div class="row">
                                    <div class="col-md-1">
                                        <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $comments->user_alias; ?>">
                                            <?php if ($comments->user_image != '') { ?>
                                                <span class="mediapost_pic">
                                                                            <img class="ryt-mrg"
                                                                                 src="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->user_image; ?>">
                                                                        </span>
                                            <?php } ?>
                                            <?php if ($comments->user_image == '') { ?>
                                                <span class="mediapost_pic">
                                                                            <img class="ryt-mrg"
                                                                                 src="<?php echo PageContext::$response->userImagePath; ?>medium/member_noimg.jpg">
                                                                        </span>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="col-md-11">
                                        <span
                                            class="name">
                                                 <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $comments->user_alias; ?>"><?php echo $comments->Username; ?></a> </span>
                                        <span class="jEmojiable"><?php echo $comments->news_feed_comment_content; ?></span>
                                        <br>
                                        <span class="new-text">
                                            <?php echo $comments->commentDate; ?>
                                        </span>
                                        <div class="commentimg_box">
                                            <?php if ($comments->file_path != '') { ?>
                                                <!--                                                                          <img class="fancybox" src="<?php //echo PageContext::$response->userImagePath;?>medium/<?php //echo $comments->file_path;?>">-->
                                                <ul class="portfolio" class="clearfix">
                                                    <li>
                                                        <a href="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->file_path; ?>"
                                                           title=""><img
                                                                src="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->file_path; ?>"
                                                                alt=""></a></li>
                                                </ul>
                                            <?php } ?>
                                        </div>
                                        <div class="mediapost_user-side1">
                                            <span rel="tooltip" title="<?php echo $comments->users;?> liked this comment" class="mediapost_links"
                                                  id="jcountfeedcommentlike_<?php echo $comments->news_feed_comments_id; ?>"><?php if ($comments->num_comment_likes > 0) {
                                                    echo $comments->num_comment_likes; ?> Likes <?php } ?></span>
                                            <input type="hidden"
                                                   id="reply_<?php echo $comments->news_feed_comments_id; ?>"
                                                   value="<?php
                                                       if(PageContext::$response->newsFeed[$feed['news_feed_id']][$comments->news_feed_comments_id] > 0) {
                                                                                        echo PageContext::$response->newsFeed[$feed['news_feed_id']][$comments->news_feed_comments_id];
                                                         }
                                                       else
                                                       {
                                                       echo "0";
                                                       }?>
                                                       ">
                                            <div class="mediapost_links jShowFeedReply"
                                                 cid="<?php echo $comments->announcement_comment_id; ?>"
                                                 id="jcountreply_<?php echo $comments->news_feed_comments_id; ?>">
                                                <a href="#" class="jShowFeedReply"
                                                   id="<?php echo $comments->news_feed_comments_id; ?>">
                                                                                <span
                                                                                    id="jcountreply_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>"><?php if (PageContext::$response->newsFeed[$feed['news_feed_id']][$comments->news_feed_comments_id] > 0) {
                                                                                        echo PageContext::$response->newsFeed[$feed['news_feed_id']][$comments->news_feed_comments_id] . " Replies";
                                                                                    } ?>
                                                                                </span>
                                                </a>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="comment_sharelike_sec">
                                            <?php if ($comments->user_id == PageContext::$response->sess_user_id || $comments->community_created_by == PageContext::$response->sess_user_id) { ?>
                                                <a href="#" class="jNewsFeedCommentDelete marg10right"
                                                   aid="<?php echo $comments->news_feed_id; ?>"
                                                   id="<?php echo $comments->news_feed_comments_id; ?>"><i
                                                        class="fa fa-times"></i> Delete</a>
                                            <?php } ?>
                                            <a href="#" class="jFeedCommentReply marg10right"
                                               aid="<?php echo $comments->news_feed_id; ?>"
                                               cid="<?php echo $comments->news_feed_comments_id; ?>"
                                               id="<?php echo $comments->news_feed_comments_id; ?>"><i
                                                    class="fa fa-reply"></i> Reply</a>
                                            <!--<a href="#" class="jFeedCommentLike" aid="{$feedlist['community_announcement_id']}" id="{$comments->announcement_comment_id}">Like</a>-->
                                            <a href="#"
                                               class="jNewsFeedCommentLike <?php if ($comments->num_comment_likes > 0) { ?> liked <?php } ?>"
                                               id="jlikeComment_<?php echo $comments->news_feed_comments_id; ?>"
                                               cid="<?php echo $comments->news_feed_comments_id; ?>"
                                               aid="<?php echo $comments->news_feed_id; ?>">

                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                                <span
                                                    id="jlikedisplaycomment_<?php echo $comments->announcement_comment_id; ?>">Like </span></a>
                                        </div>
                                        </span>
                                        <div id="jdivReply_<?php echo $comments->news_feed_comments_id; ?>"
                                             class="jDisplayReply">
                                            <p class="lead emoji-picker-container">
                                                <div class="emotionsbtn_textblk">
                                                    <textarea placeholder="Write your reply here..."
                                                              class="form-control textarea-control jEmojiTextarea"
                                                              style="height:35px;"
                                                              id="watermark_<?php echo $comments->news_feed_id; ?>_<?php echo $comments->news_feed_comments_id; ?>"
                                                              class="watermark" name="watermark"></textarea>

                                                    <a class="upload_cambtn"
                                                       id="<?php echo $feedlist['community_announcement_id']; ?>">
                                                        <label
                                                            for="imagereply_file_<?php echo $comments->news_feed_comments_id; ?>"><i
                                                                class="fa fa-camera feed-reply-camera"></i></label>
                                                        <div class="file_button"><input type="file"
                                                                                        id="imagereply_file_<?php echo $comments->news_feed_comments_id; ?>"
                                                                                        cid="<?php echo $comments->news_feed_comments_id; ?>"
                                                                                        class="jfeedimagereply_file"
                                                                                        style="display:none;"/></div>
                                                    </a>
                                                </div>
                                            </p>
                                            <div class="msg_display"
                                                 id="msg_display_<?php echo $comments->news_feed_comments_id; ?>"></div>
                                            <div class="clearfix"></div>
                                            <a id="shareButton" cid="<?php echo $feedlist['community_id']; ?>"
                                               cmid="<?php echo $comments->news_feed_comments_id; ?>"
                                               aid="<?php echo $comments->news_feed_id; ?>"
                                               class="btn yellow_btn fontupper pull-right jPostNewsfeedCommentButton">
                                                Post</a>
                                        </div>
                                        <div class="loader"
                                             id="imagereply_loader_<?php echo $comments->announcement_comment_id; ?>">
                                            <img
                                                src="<?php echo PageContext::$response->userImagePath; ?>default/loader.gif"/>
                                        </div>
                                        <div
                                            id="jDivReplyImagePreview_<?php echo $comments->news_feed_comments_id; ?>"></div>
                                        <div id="postingReply_<?php echo $comments->news_feed_comments_id; ?>"></div>
                                    </div>
                                </div>

                            </div>
                        <?php }
                    } ?>

                    <?php if (!empty(PageContext::$response->newsFeed[$feed['news_feed_id']])) {
                        if (PageContext::$response->Page[$feed['news_feed_id']]['totpages'] != PageContext::$response->Page[$feed['news_feed_id']]['currentpage']) { ?>
                            <div id="jDisplayMoreComments_<?php echo $feed['news_feed_id']; ?>"><a href="#"
                                                                                                   id="jMorecommentsButton"
                                                                                                   pid="<?php echo PageContext::$response->Page[$feed['news_feed_id']]['currentpage']; ?>"
                                                                                                   aid="<?php echo $feed['news_feed_id']; ?>">Load
                                    more comments</a></div>
                        <?php }
                    } ?>

                    <!------------------------EOF Comments-------------------------------->
                </div>
            </div>


        </div>
    <?php } ?>
<?php } ?>