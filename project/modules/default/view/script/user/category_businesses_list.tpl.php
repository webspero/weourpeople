<div class="container">
    <div class="row"> 
        <div class="col-sm-12 col-md-12 col-lg-12">

            <h3>Categories 
                <span class="round-search">{PageContext::$response->total}</span>
            {if PageContext::$response->sess_user_id neq 0}
            <span class="addbusiness"><a href="{php} echo PageContext::$response->baseUrl;{/php}add-directory">Add BizDirectory</a></span>
            {/if}
            </h3>
<!--                        <div class="col-sm-6 col-md-8 col-lg-8">
                            <input type="hidden" value="{PageContext::$response->sess_user_id}" class="login_status" name="" id="login_status">
                            Sort by : A-Z <a href="{php} echo PageContext::$response->baseUrl; {/php}businesses?sort=business_name&sortby={PageContext::$response->order}&searchtext={php} echo PageContext::$response->searchtext; {/php}&searchtype={php} echo PageContext::$response->searchtype; {/php}&page={php} echo PageContext::$response->page; {/php}">{PageContext::$response->business_name_sortorder}</a>
                        </div>-->
                        <div class="businesslist_search_blk">
                            <div class="row"> 
                                <div class="col-sm-6 col-md-4 col-lg-4 pull-right">
                                  {php}PageContext::renderRegisteredPostActions('searchbox');{/php}
                                </div>
                            </div>
                        </div>
        </div>
        <section class="cls_section">
            {if PageContext::$response->total > 0}
            {foreach from=PageContext::$response->category_business key=id item=category} 

            <div class="col-sm-4 col-md-4 col-lg-4">
                <div class="cateforylist_items">
                    <a>{$category->category_name}</a>
                    <a href="{PageContext::$response->baseUrl}businesses/{$category->category_id}"><div class="categ_count">{$category->cnt}</div></a>
                </div>

            </div>

            {/foreach}   
            {else}
            
            <div class="categorylist_row rownoborder">
            <div class="no-item">{if PageContext::$response->searchtype eq ''}No categories added.{else}No matches found.{/if}
             </div>
            </div>
            {/if}
        </section>
    </div>
</div>