            <?php if(PageContext::$response->totalrecords > 0){
//	        {assign var="i" value=PageContext::$response->slno}
	        foreach(PageContext::$response->leads as $id=>$lead){?>
            <div class="item">
                
                <div class="mediapost well">
                    <div class="wid100per">
                        <span class="mediapost_pic">
                            <a href="<?php echo PageContext::$response->baseUrl;?>directory/<?php echo $lead->business_alias;?>"><img class="business_profile_desc_colside_pic" alt="<?php echo $lead->file_orig_name;?>" src="<?php echo PageContext::$response->userImagePath;?><?php if($lead->file_path != ''){?><?php echo $lead->file_path;}elseif($lead->file_path == ''){?>default/no_image_bbf.jpg<?php } ?>"> </a>
                        </span>
                    </div>

                    <div class="wid100per pad5p  media-body">
                        <h4 class="media-heading"><span><a href="<?php echo PageContext::$response->baseUrl;?>directory/<?php echo $lead->business_alias;?>"><?php echo $lead->business_name;?></a></span></h4>
                        <p class="owncaption"><span>Owned By</span> : <?php echo $lead->owner_name;?></p>
                        <?php if($lead->community_status =='I'){?>   
                        <span class="status clserror" >Pending Approval</span> 
                        <?php } else{ ?>
                        <div class="display_table">
                            <div class="display_table_cell wid60per">
                                <div id="starring">
                                    <div class="rateit" id="rateit_<?php echo $lead->ratetype_id;?>" data-rateit-value="<?php echo $lead->ratevalue;?>" data-rateit-ispreset="true" data-rateit-id="<?php echo $lead->rateuser_id;?>" data-rateit-type="<?php echo $lead->ratetype;?>" data-rateit-typeid="<?php echo $lead->ratetype_id;?>" data-rateit-flag="<?php echo $lead->rateflag;?>" data-rateit-step=1 data-rateit-resetable="false">
                                    </div>
                                    <div class="right">
                                        <?php if(PageContext::$response->sess_user_id == $lead->business_created_by){ ?>
                                        <a href="<?php echo PageContext::$response->baseUrl;?>edit-directory/<?php echo $lead->business_id;?>" class="edititem left marg5right"><i class="fa fa-pencil"></i></a>
                                        <a href="<?php echo PageContext::$response->baseUrl;?>index/delete_business/<?php echo $lead->business_id;?>" class="deleteitem left" onclick="return deleteConfirm();"><i class="fa fa-trash"></i></a> 
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <?php } ?>
                        <?php if($lead->business_address != ''){?><div class="business_profile_desc_colmiddle_location"><span><?php echo $lead->business_address;?></span></div><?php } ?>
                        <p>
                            <?php echo substr($lead->business_description,0,200)."...";?>
                        </p>
                    </div>
                    <!--<div class="clearfix"></div>-->
                
                </div>
            </div>
            <!--{assign var="i" value=$i+1}-->
            <?php } } ?> 	
<!--            {else}
            <div class="business_profilelist_detailsarea">{if PageContext::$response->searchtype eq ''}No communities added/joined yet.{else}No matches found.{/if} Click to <a href="{php} echo PageContext::$response->baseUrl;{/php}add-directory">create a business</a>.</div>
            {/if} 
            -->