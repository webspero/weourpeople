<div class="container">
    <div class="row"> 
        <div class="col-sm-12 col-md-12 col-lg-12">
<div id="o-wrapper" class="o-wrapper">
<!---Menu Link----------------- -->
{if PageContext::$response->sess_user_id >0}
<div class="main">
<div class="header-main">
     <main class="o-content">
        <div class="o-container">
          
          <div class="c-buttons">
            <button id="c-button--slide-right" class="button-nav-toggle"><span class="icon">&#9776;</span></button>
          </div>
          
          <div id="github-icons"></div>

        </div><!-- /o-container -->
      </main><!-- /o-content -->    
</div>
</div>  
{/if}
</div>
<!---EOF Menu Link----------------- --> 
            <h3>Oraganisations <span class="round-search">{PageContext::$response->totalrecords}</span> {if PageContext::$response->sess_user_id neq 0} <span class="addbusiness"><a href="{php} echo PageContext::$response->baseUrl;{/php}add-directory">Add BizDirectory</a></span> {/if}</h3>
        </div>
        <section class="searchbar">

            <div class="col-sm-12 col-md-12 col-lg-12">

                <div class="businesslist_search_blk">
                    <div class="row"> 
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <input type="hidden" value="{PageContext::$response->sess_user_id}" class="login_status" name="" id="login_status">
                            Sort by : A-Z <a href="{php} echo PageContext::$response->baseUrl; {/php}page-list?sort=business_name&sortby={php} echo PageContext::$response->order; {/php}&searchtext={php} echo PageContext::$response->searchtext; {/php}&searchtype={php} echo PageContext::$response->searchtype; {/php}&page={php} echo PageContext::$response->page; {/php}">{PageContext::$response->business_name_sortorder}</a>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4 pull-right">
                            {php}PageContext::renderRegisteredPostActions('searchbox');{/php}
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-4">
                        <select class="form-control" placeholder="Category" name="business_category" id="business_category" value="" >
	                                    {foreach from=PageContext::$response->categories key=category item=id}
	                                        <option value="{$id}" {if PageContext::$response->cat_id eq $id} selected="selected" {/if}>{$category}</option>
	                                    {/foreach}

	                </select>
                        </div>    
                    </div>
                </div>
                {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
            </div>
        </section>
    </div>
    <div class="clearfix"></div>
    <div class="row masonry"> 
        <div class="col-sm-12 col-md-12 col-lg-12">
        <section class="bizcomlisting">
            {if PageContext::$response->totalrecords > 0}
	        {assign var="i" value=PageContext::$response->slno}
	        {foreach from=PageContext::$response->leads key=id item=lead}
            <div class="item">
                
                <div class="mediapost well business_list_outer">
                    <div class="wid100per">
                        <span class="mediapost_pic">
                            <a href="{php} echo PageContext::$response->baseUrl;{/php}page/{$lead->business_alias}"><img class="business_profile_desc_colside_pic" alt="{$lead->file_orig_name}" src="{php} echo PageContext::$response->userImagePath;{/php}{if $lead->file_path neq ''}{$lead->file_path}{elseif $lead->file_path eq ''}default/no_image_bbf.jpg{/if}"> </a>
                        </span>
                    </div>

                    <div class="wid100per pad5p ">
                        <h4 class="media-heading"><span><a href="{php} echo PageContext::$response->baseUrl;{/php}page/{$lead->business_alias}">{$lead->business_name}</a></span></h4>
                        <p class="owncaption"><span>Owned By</span> : {$lead->owner_name}</p>
<!--                        <p class="owncaption"><span>Category</span> : {$lead->category_name}</p>-->
                        {if $lead->community_status =='I'}   
                        <span class="status clserror" >Pending Approval</span> 
                        {else}
                        <div class="display_table">
                            <div class="display_table_cell wid60per">
							
							
                                <div id="starring">
                                    <div class="rateit" id="rateit_{$lead->ratetype_id}" data-rateit-value="{$lead->ratevalue}" data-rateit-ispreset="true" data-rateit-id="{$lead->rateuser_id}" data-rateit-type="{$lead->ratetype}" data-rateit-typeid="{$lead->ratetype_id}" data-rateit-flag="{$lead->rateflag}" data-rateit-step=1 data-rateit-resetable="false">
                                    </div>
                                    <div class="right">
                                        {if PageContext::$response->sess_user_id eq $lead->business_created_by} 
                                        <a href="{php} echo PageContext::$response->baseUrl;{/php}edit-page/{$lead->business_id}" class="edititem left marg5right"><i class="fa fa-pencil"></i></a>
                                        <a href="{php} echo PageContext::$response->baseUrl;{/php}index/delete_organization/{$lead->business_id}" class="deleteitem left" onclick="return deleteConfirm();"><i class="fa fa-trash"></i></a> 
                                        {/if}
                                    </div>
                                </div>
								
								
                                <!--<div id="starring">
                                    <div class="rateit" id="rateit_{$lead->ratetype_id}" data-rateit-value="{$lead->ratevalue}" data-rateit-ispreset="true" data-rateit-id="{$lead->rateuser_id}" data-rateit-type="{$lead->ratetype}" data-rateit-typeid="{$lead->ratetype_id}" data-rateit-flag="{$lead->rateflag}" data-rateit-step=1 data-rateit-resetable="false">
                                    </div>
                                    <div class="right">
                                        {if PageContext::$response->sess_user_id eq $lead->business_created_by} 
                                        <a href="{php} echo PageContext::$response->baseUrl;{/php}edit-directory/{$lead->business_id}" class="edititem left marg5right"><i class="fa fa-pencil"></i></a>
                                        <a href="{php} echo PageContext::$response->baseUrl;{/php}index/delete_business/{$lead->business_id}" class="deleteitem left" onclick="return deleteConfirm();"><i class="fa fa-trash"></i></a> 
                                        {/if}
                                    </div>
                                </div>-->
                            </div>

                        </div>
                        {/if}
                        {if $lead->business_address neq ''}<div class="business_profile_desc_colmiddle_location"><span>{$lead->business_address}</span></div>{/if}
                        <p>
                            {stripslashes($lead->business_description)|substr:0:200}....
                        </p>
                    </div>
                    <div class="clearfix"></div>
                
                </div>
            </div>
            {assign var="i" value=$i+1}
            {/foreach} 	
            {else}
             <div class="clearfix"></div>
            <div class="col-sm-12 col-md-12 col-lg-12">{if PageContext::$response->searchtype eq ''}No communities added/joined yet.{else}No matches found.{/if} Click to <a href="{php} echo PageContext::$response->baseUrl;{/php}add-directory">create a business</a>.</div>
            {/if} 
           <div class="clear"></div> 
 </section>
       
        </div>

    </div>
    
	<div class="clear"></div>
    {if PageContext::$response->totalrecords gt PageContext::$response->itemperpage}
    <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">{PageContext::$response->pagination} </div>
    </div>
    {/if}

		





    </div>
</div>



				
</div>