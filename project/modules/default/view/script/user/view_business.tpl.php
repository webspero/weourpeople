
    <div class="business_profile_detailsarea" id="jDivDisplayBusiness">
    <?php PageContext::renderRegisteredPostActions('messagebox');?>
    <div class="business_profile_desc">
        <div class="business_profile_desc_col1">
            <?php $path=(PageContext::$response->resultSet->file_path != '')? PageContext::$response->userImagePath.'medium/'.PageContext::$response->resultSet->file_path : 'default/no_image_bbf.jpg';?>
            <img class="business_profile_desc_pic" alt="<?php echo PageContext::$response->resultSet->file_orig_name;?>" src="<?php echo $path;?>">
        </div>
        <div class="business_profile_desc_col1 col1wide">
            <h3><?php echo PageContext::$response->resultSet->business_name;?>
                <?php if(PageContext::$response->resultSet->business_status == 'A'){?>
                <div class="business_profile_desc_starring">
                    <?php PageContext::renderRegisteredPostActions('rating');?>
                </div>
                <?php 
                }?>
            </h3> 
            <div class="business_profile_desc_location"><?php if(PageContext::$response->resultSet->business_address != ''){?><div><?php echo PageContext::$response->resultSet->business_address;?></div> <?php }?>                
                <?php if(PageContext::$response->sess_user_id == PageContext::$response->resultSet->business_created_by){?>
                <a class="delete_link_business" href="<?php  echo PageContext::$response->baseUrl;?>user/edit_organization/<?php  echo PageContext::$response->resultSet->business_id;?>"> Edit</a><br clear="all"/>
                <?php }?>
                <p>
                    <?php echo PageContext::$response->resultSet->business_description;?>	
                </p>
            </div>
            <?php if(PageContext::$response->resultSet->business_status =='A'){?>
            <div class="inquire_ratingblk">
                <div class="business_profile_desc_action">
                    <input type="hidden" value="<?php echo PageContext::$response->sess_user_id;?>" class="login_status" name="" id="login_status">

                    <input type="button" value="INQUIRE" class="inquire_btn" name="" onclick="return checkLoginInquire('B','<?php echo PageContext::$response->resultSet->business_id;?>');">
                    <?php if(PageContext::$response->sess_user_status != 'I'){?>
                    <input type="button" value="COMMENT" class="comment_btn" name="" onclick=" return checkLogin('B','<?php echo PageContext::$response->resultSet->business_id;?>');">
                    <?php }?>
                </div>
            </div>
           <?php
            }?>
        </div>
    </div>
    <div class="clear"></div>
     <div class="business_profile_addressBlk" >
        <?php PageContext::renderRegisteredPostActions('googlemap'); ?>
        <div class="business_profile_addressdiv">
            <?php if (PageContext::$response->resultSet->business_address != ''){?>
            <div class="business_profile_addressrow">
                <span class="col1">Address</span>
                <span class="col2"></span>
                <span class="col3"><?php echo PageContext::$response->resultSet->business_address;?></span>
                <div class="clear"></div>
            </div>
            <?php }?>
           <?php if (PageContext::$response->resultSet->business_city != ''){?>
            <div class="business_profile_addressrow">
                <span class="col1">City</span>
                <span class="col2"></span>
                <span class="col3"><?php echo PageContext::$response->resultSet->business_city;?></span>
                <div class="clear"></div>
            </div>
            <?php }
            if(PageContext::$response->resultSet->business_state != ''){?>
            <div class="business_profile_addressrow">
                <span class="col1">State</span>
                <span class="col2"></span>
                <span class="col3"><?php echo PageContext::$response->resultSet->State?></span>
                <div class="clear"></div>
            </div>
            <?php
            }
            if(PageContext::$response->resultSet->business_country != ''){?>
            <div class="business_profile_addressrow">
                <span class="col1">Country</span>
                <span class="col2"></span>
                <span class="col3"><?php echo PageContext::$response->resultSet->Country?></span>
                <div class="clear"></div>
            </div>
            <?php
            }?>
            <?php if(PageContext::$response->resultSet->business_zipcode != ''){?>
            <div class="business_profile_addressrow">
                <span class="col1">Zip</span>
                <span class="col2"></span>
                <span class="col3"><?php echo PageContext::$response->resultSet->business_zipcode?></span>
                <div class="clear"></div>
            </div>
           <?php
            }
            if(PageContext::$response->resultSet->business_phone != ''){?>
            <div class="business_profile_addressrow">
                <span class="col1">Phone</span>
                <span class="col2"></span>
                <span class="col3"><?php echo PageContext::$response->resultSet->business_phone?></span>
                <div class="clear"></div>
            </div>
            <?php }?>
            <?php if(PageContext::$response->resultSet->business_email != ''){?>
            <div class="business_profile_addressrow">
                <span class="col1">Email</span>
                <span class="col2"></span>
                <span class="col3"><a href="mailto:<?php echo PageContext::$response->resultSet->business_email;?>"><?php echo PageContext::$response->resultSet->business_email;?></a></span>
                <div class="clear"></div>
            </div>
           <?php
            }?>
            <?php 
            if(PageContext::$response->resultSet->business_url != ''){?>
            <div class="business_profile_addressrow">
                <span class="col1">Website</span>
                <span class="col2"></span>
                <span class="col3"> <a href="{if strpos(PageContext::$response->resultSet->business_url, 'http://') === false}http://{/if}{PageContext::$response->resultSet->business_url}" target="_new"><?php echo PageContext::$response->resultSet->business_url;?></a></span>
                <div class="clear"></div>
            </div>
            <?php
            }?>										
        </div>

    </div>
    <div class="clear"></div>
    <h3> People Associated with {PageContext::$response->resultSet->business_name}</h3>
     <div class="business_profile_addressBlk" id="tagged_people"> 
        <div class="frnd_reqst_blk_content">
            {if PageContext::$response->associates|@sizeof eq 0}
            <div class="col-sm-12 col-md-12 col-lg-12">No Users Associated So far</div>
            {else}
            {foreach from=PageContext::$response->associates key=id item=user}
            <div class="frnds_list_blk_tag">
                <div class="frnd_reqst_img">
                    <a  href="{PageContext::$response->baseUrl}profile/{$user->user_alias}"><img src="{if $user->file_path eq ''}{PageContext::$response->userImagePath}thumb/member_noimg.jpg{else}{PageContext::$response->userImagePath}thumb/{$user->file_path}{/if}"></a>
                </div>
                <div class="frnd_content">
                    <h6><a  href="{PageContext::$response->baseUrl}profile/{$user->user_alias}">{$user->user_firstname} {$user->user_lastname}</a></h6>
                    <div class="frnd_content_accept" style="width: 100px!important">
                        <span>{$user->role}</span>
                    </div>
<!--                    <p><div class="affliate_content_add" id="affiliate_{$user->user_id}">
                        {if in_array($user->user_id,PageContext::$response->affliatesUsersList)}
                            Affilate <a uid="{$user->user_id}" bid="{PageContext::$response->resultSet->business_id}" class="jremoveaffiliate">x Remove</a>
                        {else}
                        +<span><a uid="{$user->user_id}" bid="{PageContext::$response->resultSet->business_id}" class="jaddaffiliate">Add as affiliate</a></span>
                        {/if}
                    </div></p>-->
               </div>
            </div>
            {/foreach}
            <div>{PageContext::$response->paginationAffilate} </div>
            {/if}
            <div class="clear"></div>
            <div class="clear"></div>
        </div>
   </div>
    <div class="clear"></div>
    <h3>Affiliate Request</h3>
     <div class="business_profile_addressBlk" id="tagged_people"> 
        <div class="frnd_reqst_blk_content">
            {if PageContext::$response->associatesRequest|@sizeof eq 0}
            <div>No request received</div>
            {else}
            {foreach from=PageContext::$response->associatesRequest key=id item=pendingusers}
            <div class="frnds_list_blk_tag">
                <div class="frnd_reqst_img">
                    <a  href="{PageContext::$response->baseUrl}profile/{$pendingusers->user_alias}"><img src="{if $pendingusers->file_path eq ''}{PageContext::$response->userImagePath}thumb/member_noimg.jpg{else}{PageContext::$response->userImagePath}thumb/{$pendingusers->file_path}{/if}"></a>
                </div>
                <div class="frnd_content">
                    <h6><a  href="{PageContext::$response->baseUrl}profile/{$pendingusers->user_alias}">{$pendingusers->user_firstname} {$pendingusers->user_lastname}</a></h6>
                    <div class="frnd_content_accept" style="width: 100px!important">
                        <span>{$pendingusers->role}</span>
                    </div>
                    <p><div class="affliate_content_add" id="affiliate_request_{$pendingusers->user_id}">
                       <a uid="{$pendingusers->user_id}" bid="{PageContext::$response->resultSet->business_id}" class="jAcceptAffilaite">Accept</a>
                        
                        +<span><a uid="{$pendingusers->user_id}" bid="{PageContext::$response->resultSet->business_id}" class="jRejectAffilaite">Reject</a></span>
                       
                    </div></p>
               </div>
            </div>
            {/foreach}
            <div>{PageContext::$response->paginationAffilate} </div>
            {/if}
            <div class="clear"></div>
            <div class="clear"></div>
        </div>
   </div>
    <div class="clear"></div>
    {if PageContext::$response->resultSet->business_status eq 'A' and PageContext::$response->sess_user_status neq 'I'}
    <div class="business_profile_commentBlk">
        <h3>Testimonials <span> <a href="javascript:void(0)" onclick="return checkLogin('B','{PageContext::$response->resultSet->business_id}');">Add testimonial </a>  </span></h3>
        {assign var="i" value=PageContext::$response->slno}
        {foreach from=PageContext::$response->comments key=id item=lead}
        <div class="business_profile_commentrow"  id="comment_{$lead->comment_id}">
            <img class="business_profile_commentBlk_pic" src="{php} echo PageContext::$response->userImagePath;{/php}{if $lead->file_path neq ''}small/{$lead->file_path}{elseif $lead->file_path eq ''}small/member_noimg.jpg{/if}">
            <h5><a  href="{PageContext::$response->baseUrl}profile/{$lead->user_alias}">{if $lead->user_firstname eq ''}{$lead->user_email}{else}{$lead->user_firstname} {$lead->user_lastname}{/if}</a><span class="datepaosted">  {time_elapsed_string($lead->comment_created_on)} </span></h5>
            <span class="comment_content_{$lead->comment_id}">{$lead->comment_content}</span>
            <span class="edit_delete">{if PageContext::$response->sess_user_id eq $lead->user_id}<a class="delete_link_business" href="{php} echo PageContext::$response->baseUrl;{/php}index/delete_comment/{$lead->comment_id}"  onclick="return deleteConfirm();">Delete</a><a class="edit_link_business jedit_link_business" href="#" cid="{$lead->comment_id}" id="{$lead->comment_id}">Edit</a>{/if}</span>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        {assign var="i" value=$i+1}
        {/foreach} 	
        <div>{PageContext::$response->pagination} </div>
    </div>
    {if PageContext::$response->sess_user_status neq 'I'}     
    <div>
  </div>
    {/if}
    {/if}               
    <div class="clear"></div>
</div>

<div class="clear"></div>
</div>

