<script>
    var TOTAL_PAGES = <?php echo PageContext::$response->count?>;
    $(document).ready(function () {
        var page = 1;
        var _throttleTimer = null;
        var _throttleDelay = 100;
        $(window).scroll(function () {
            clearTimeout(_throttleTimer);
            _throttleTimer = setTimeout(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                    if (page < TOTAL_PAGES) {
                        page++;
                        $.blockUI({message: ''});
                        var community_id = $("#community_alias").val();
                        $.ajax({
                            type: 'Post',
                            url: mainUrl + 'user/listmembersajax/' + page + '/' + community_id,
                            data: 'community_alias=' + community_id,
                            success: function (data) {
                                console.log(data);
                                $('#member_container_div').append(data);
                                $('.listitem').fadeIn(3000);
                                $('.listitem').hide();
                            }
                        })
                    }
                }
            }, _throttleDelay);
        });
    });
    
    $('.thumbnail').click(function(){
    $('.modal-body').empty();
    var title = $(this).parent('a').attr("title");
    $('.modal-title').html(title);
    $($(this).parents('div').html()).appendTo('.modal-body');
    $('#myModal').modal({show:true});
});
</script>
<?php if (PageContext::$response->sess_user_id > 0 && PageContext::$response->sess_user_id == PageContext::$response->communityDetails->community_created_by) {
    if (PageContext::$response->communityDetails->community_status == 'A' and PageContext::$response->sess_user_status != 'I') {
        ?>
        <h3>Pending Requests(<?php echo PageContext::$response->pendingcount; ?>)</h3>
        <div class="row">
        <?php
        if (count(PageContext::$response->pendingMembers) > 0) {
            foreach (PageContext::$response->pendingMembers As $members) {
                ?>

                <div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="mediapost">
                        <div class="mediapost_left pull-left">
                            <span class="mediapost_pic">
                                <img src="<?php if ($members->file_path == '') {
                                    echo PageContext::$response->userImagePath . 'small/member_noimg.jpg';
                                } else {
                                    echo PageContext::$response->userImagePath . 'small/' . $members->file_path;
                                } ?>">
                            </span>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl . 'timeline/' . $members->user_alias; ?>"><?php echo ($members->user_firstname != '') ? $members->user_firstname . " " . $members->user_lastname : $members->user_email; ?> </a></h4>
                            <p><?php echo $members->user_friends_count; ?>  friends</p>
                            <p>
                                <span class="accept">+ <a href="javascript:void(0)" onclick="changeCommunitMemberStatus(<?php echo $members->community_id; ?>,<?php echo $members->user_id; ?>,'accept')">Accept</a></span> 
                                <span class="reject">x <a href="javascript:void(0)" onclick="changeCommunitMemberStatus(<?php echo $members->community_id; ?>,<?php echo $members->user_id ?>,'decline')">Decline</a></span>

                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>





                
                <?php
            }
        } else {
            ?>
            <div class="frndslisting_blk_content" style="padding: 10px;">
                No Pending Request yet
            </div>
            <?php
        }?>
        </div>
        <?php
    }
}
?>
<div class="whitebox">
<div class="frnds_blk memberslist">
<h3>Members<span class="round-search"><?php echo PageContext::$response->count; ?></span> <input type="text" name="searchfriend" id="searchfriend" class="filter_feild" placeholder="Search Members" ></h3>             
<div class="clear"></div>
</div>
<div id="community_member_holder">
<div class="row" id="member_container_div">
<?php
if (count(PageContext::$response->members) > 0) {
    foreach (PageContext::$response->members As $members) {
        ?>

            <div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="mediapost <?php if($members->cmember_active_status =='A'){?> active_user_box <?php }?>">
                        <div class="picpost_left pull-left">
                            <span class="picpost_left_pic">
                                <img src="<?php if ($members->file_path == '') {
            echo PageContext::$response->userImagePath . 'small/member_noimg.jpg';
        } else {
            echo PageContext::$response->userImagePath .  $members->file_path;
        } ?>">
                            </span>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl . 'timeline/' . $members->user_alias; ?>"><?php echo ($members->user_firstname != '') ? $members->user_firstname . " " . $members->user_lastname : $members->user_email; ?> </a></h4>
                            <p><?php echo $members->user_friends_count; ?>  friends</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            
            <?php
        }
    } else {
        ?>
        <div class="frndslisting_blk_content" style="padding: 10px;">
            No Members Joined yet
        </div>
    <?php
}
?>
</div>
</div>
<input type="hidden" id="community_alias" value="<?php echo PageContext::$response->communityAlias; ?>">
<div class="clear"></div>
</div>