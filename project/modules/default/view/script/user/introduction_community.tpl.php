<div class="content_left pad_cont_right">
    <h3>Choose a BizCom to Initiate an Introduction </h3>
    {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
    <div class="businesslist_search_blk">
        <div class="businesslist_search_blk_col1">
            <input type="hidden" value="{PageContext::$response->sess_user_id}" class="login_status" name="" id="login_status">
            <div class="businesslist_search_business">

            </div>
        </div>
        <div class="businesslist_search_blk_col2">
            Sort by : A-Z <a href="{php} echo PageContext::$response->baseUrl; {/php}introduction?sort=community_name&sortby={php} echo PageContext::$response->order; {/php}&searchtext={php} echo PageContext::$response->searchtext; {/php}&searchtype={php} echo PageContext::$response->searchtype; {/php}&page={php} echo PageContext::$response->page; {/php}">{PageContext::$response->community_name_sortorder}</a>
        </div>

        <div class="businesslist_search_blk_col3">

            {php}PageContext::renderRegisteredPostActions('searchbox');{/php}
        </div>
    </div>
    {if PageContext::$response->totalrecords > 0}
    {assign var="i" value=PageContext::$response->slno}
    {foreach from=PageContext::$response->leads key=id item=lead}


    <div class="business_profilelist_detailsarea">
        <div class="business_profile_desc_colside">
            <img class="business_profile_desc_colside_pic" alt="{$lead->file_orig_name}" src="{php} echo PageContext::$response->userImagePath;{/php}{if $lead->file_path neq ''}{$lead->file_path}{elseif $lead->file_path eq ''}default/no_preview_med.jpg{/if}"> 
            <div class="clear"></div>
        </div>
        <div class="business_profile_desc_colmiddle">
            <h3><span>{$lead->community_name}</span></h3> 
            {if $lead->community_status =='I'}   
            <span class="status clserror" >Pending Approval</span> 
            {else}
            <div class="business_profile_desc_colside_list">
                <div id="starring">
                    <div class="rateit" id="rateit_{$lead->ratetype_id}" data-rateit-value="{$lead->ratevalue}" data-rateit-ispreset="true" data-rateit-id="{$lead->rateuser_id}" data-rateit-type="{$lead->ratetype}" data-rateit-typeid="{$lead->ratetype_id}" data-rateit-flag="{$lead->rateflag}" data-rateit-step=1 data-rateit-resetable="false">
                    </div>

                    
                </div>
<!-- 						<img src="{PageContext::$response->ImagePath}starring_inner.jpg">  -->
            </div> 
            {/if}
            <p>{$lead->community_description|substr:0:200}....</p>
            <p><br><a class="readmore" alias="{$lead->community_alias}" id="jRedirectToInitiate">Click Here To Initiate Introduction</a></p>
        </div>
        <!-- <div class="business_profile_desc_colside">
                <img src="{PageContext::$response->ImagePath}starring_inner.jpg">
        </div> -->

        <div class="clear"></div>
    </div>
    {assign var="i" value=$i+1}
    {/foreach} 	
    {else}
    <div class="business_profilelist_detailsarea">{if PageContext::$response->searchtype eq ''}No communities added yet.{else}No matches found.{/if} Click to <a href="{php} echo PageContext::$response->baseUrl;{/php}add-community">create a community</a>.</div>
    {/if} 
    <div class="clear"></div>
    {if PageContext::$response->totalrecords gt PageContext::$response->itemperpage}
    <div>{PageContext::$response->pagination} </div>
    {/if}
</div>
