<div class="container">
    <div class="row"> 
        <div class="col-sm-3 col-md-3 col-lg-3 "></div>
        <div class="col-sm-6 col-md-6 col-lg-6 ">

            <div class="whitebox marg40col pad25px_left pad25px_right">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Edit Page</h3>
                    </div>
                </div>
                <div class="form-bottom signup_form">
                    <div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
                    <form enctype="multipart/form-data" class="" method="post" id="frmBizdirectoryEdit" name="frmBizdirectoryEdit">
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="text" value="{PageContext::$response->data['business_name']}" name="business_name" class="form-control" placeholder="Page Name">
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_bnameadd" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="business_name" class="error"></label>
                        </div>
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="text" value="{PageContext::$response->data['business_email']}" name="business_email" class="form-control" placeholder="Page Email">
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_bemailadd" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="business_email" class="error"></label>
                        </div>
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="text" value="{PageContext::$response->data['business_url']}" name="business_url" class="form-control" placeholder="Page Url">
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_burladd" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="business_url" class="error"></label>
                        </div>

                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <textarea class="form-control" name="business_description" placeholder="Page Description">{PageContext::$response->data['business_description']|stripslashes}
                                    </textarea>
                                </div>
                            </div>
                            <label generated="true" for="business_description" class="error"></label>
                        </div>

  
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <textarea class="form-control" name="business_address" placeholder="Page Address">{PageContext::$response->data['business_address']}</textarea>
                                </div>
                            </div>
                            <label generated="true" for="business_description" class="error"></label>
                        </div>
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="text" value="{PageContext::$response->data['business_city']}" name="business_city" class="form-control" placeholder="Page City">
                                </div>
                            </div>
                        </div>
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <select class="form-control" placeholder="Page Country" selected="Array" name="business_country" id="business_country" type="select">
                                        {foreach from=PageContext::$response->countries key=country item=id}
                                            <option value="{$id}" {if PageContext::$response->data['business_country'] eq $id} selected="selected" {/if}>{$country}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="text" class="form-control" name="business_state" id="business_state">
                                </div>
                            </div>
                        </div>
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="text" value="{PageContext::$response->data['business_phone']}" name="business_phone" class="form-control" placeholder="Telephone" size="10">
                                </div>
                            </div>
                        </div>
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="text" value="{PageContext::$response->data['business_zipcode']}" name="business_zipcode" class="form-control" placeholder="Zip Code" size="10">
                                </div>
                            </div>
                        </div>
                        <div class="form-group relative">
                            <input type="file" class="  left" placeholder="Upload File" size="10" value="" id="business_image_id" name="business_image_id" onchange="upload(150,150,'business_image_id');">
                           <!-- <img class="masterTooltip" title="Business Image(Supported format:jpg,jpeg,gif,png)" src="{PageContext::$response->baseUrl}modules/cms/images/help_icon.png">-->
                            <span id="spn_support_imgedit">(Supported format:jpg,jpeg,gif,png)</span>
                            <label generated="true" id="label_image_id" for="business_image_id" class="error"></label>
                           
                        </div>
                         <span><img src="{PageContext::$response->userImagePath}{PageContext::$response->data['file_path']}"></span>
<!--                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    Upload Logo
                            <input type="file" class="left" placeholder="Upload File" size="10" value="" name="business_logo_id">
                             <img class="masterTooltip" src="{PageContext::$response->baseUrl}modules/cms/images/help_icon.png" title="Community Image(Supported format:jpg,jpeg,gif,png)"> 
                             <span id="spn_support_logoedit">(Supported format:jpg,jpeg,gif,png)</span>
                             </div>
                                
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    
                                </div>
                               
                            </div>
                            <span><img src="{PageContext::$response->userImagePath}thumb/{PageContext::$response->data['business_logo_name']}"></span>
                        </div>    -->
                        <div class="form-group relative">
                            <input type="hidden" id="business_id" name="business_id" value="{PageContext::$response->data['business_id']}">
                            <input type="submit" class="btn btn-primary yellow_btn2" value="Save" name="btnSubmit" ondblclick="this.disabled=true;">
                            <input type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary yellow_btn2" value="Cancel" name="btnCancel">
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3 col-lg-3 "></div>
    </div>
</div>



