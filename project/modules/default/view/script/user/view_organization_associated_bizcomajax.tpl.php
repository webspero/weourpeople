<link rel="stylesheet" href="<?php echo BASE_URL ?>project/styles/rateit.css" type="text/css"/>
<script src="<?php echo BASE_URL ?>project/js/jquery.rateit.js" type="text/javascript"></script>
<script src="<?php echo BASE_URL ?>project/js/ratinglist.js" type="text/javascript"></script>
<?php

foreach (PageContext::$response->associatedBizcom AS $id => $user) {
    ?>
    <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="mediapost">
            <div class="picpost_left pull-left">
                    <span class="picpost_left_pic">
                        <a href="<?php echo PageContext::$response->baseUrl . 'group/' . $user->community_alias ?>"><img
                                src="<?php echo ($user->file_orig_name == '') ? PageContext::$response->userImagePath . 'medium/member_noimg.jpg' : PageContext::$response->userImagePath . 'medium/' . $user->file_path; ?>"></a>
                    </span>
            </div>
            <div class="media-body">
                <h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl . 'group/' . $user->community_alias ?>"><?php echo $user->community_alias ?>
                        (<?php echo $user->member_count ?>)</a></h4>
            </div>
            <p>
                <?php echo substr($user->community_description, 0, 200) . "..."; ?>
            </p>
            <div class="clearfix"></div>
        </div>
    </div>
    <?php ?>
    <!--  <div class="clear"></div>-->
    <!--   <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                        <div><?php echo PageContext::$response->paginationAffilate; ?> </div>-->
<?php } ?>
<!--         </div>
     </div>-->
<div class="clear"></div>