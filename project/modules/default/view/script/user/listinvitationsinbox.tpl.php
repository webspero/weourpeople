

<div class="row tab-pane fade active in" id="invitations">
<?php 
if(sizeof(PageContext::$response->usersList) == 0){ ?>

No Pending Invitations Found
<?php }else{ 
    
	foreach (PageContext::$response->usersList as $user){
//           echopre($user->user_alias);
		if($user->community_id == ''){?>
            <div class="col-sm-4 col-md-4 col-lg-4">
            <div class="mediapost">
                <div class="mediapost_left pull-left">
                    <span class="mediapost_pic marg10top">
                        <a href="<?php echo PageContext::$response->baseUrl.'profile/'.$user->user_alias;?>"><img src="<?php if($user->file_path == ''){echo PageContext::$response->userImagePath;?>small/member_noimg.jpg<?php }else{ echo PageContext::$response->userImagePath;?>small/<?php echo $user->file_path;}?>"></a>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl.'profile/'.$user->user_alias;?>"><?php echo $user->user_firstname.' '.$user->user_lastname;?></a></h4>
                    <p><?php echo $user->user_friends_count;?> Friends</p>
                    <p>
                    	<?php if(PageContext::$response->sess_user_id > 0){ ?>
                        <a uid="<?php echo $user->user_id;?>" href="<?php echo PageContext::$response->baseUrl;?>accept-invitation/<?php echo $user->invitation_id;?>/friend/0/inbox"><span class="accept"><b>+</b> Accept</span></a>
                        <a uid="<?php echo $user->user_id;?>" href="<?php PageContext::$response->baseUrl;?>accept-invitation/<?php echo $user->invitation_id;?>/reject/0/inbox"><span class="reject"><i class="fa fa-times"></i> Reject</span></a>
                    	<?php }?>
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
            </div>
<!--
<div class="row">
<div class="colside1">
<div class="colside1_pic">
<a href="<?php echo PageContext::$response->baseUrl;?>profile/<?php echo $user->user_alias;?>"><img src="<?php if($user->file_path == ''){echo PageContext::$response->userImagePath;?>small/member_noimg.jpg<?php }else{ echo PageContext::$response->userImagePath;?>small/<?php echo $user->file_path;}?>"></a>
</div>
<div class="clear"></div>
</div>
<div class="colmiddle">
<h6><a href="<?php echo PageContext::$response->baseUrl;?>profile/<?php $user->user_alias;?>"><?php echo $user->user_firstname.' '.$user->user_lastname;?></a></h6>
<p>
<?php echo $user->user_friends_count;?> Friends
</p>
</div>
<div class="colside3">
<?php if(PageContext::$response->sess_user_id > 0){ ?>
<a class="friends_btn fright" uid="<?php echo $user->user_id;?>" href="<?php PageContext::$response->baseUrl;?>accept-invitation/<?php echo $user->invitation_id;?>/reject/0/inbox" style="float:left; ">
 
<span class="declinefriend_icon"></span>
Decline
</a>
<a class="friends_btn fright" uid="<?php echo $user->user_id;?>" href="<?php echo PageContext::$response->baseUrl;?>accept-invitation/<?php echo $user->invitation_id;?>/friend/0/inbox" style="float:left; ">
 
<span class="tik_icon"></span>
Accept
</a>

<?php }?>

</div>
</div>
-->
<?php }else{?>


            <div class="col-sm-4 col-md-4 col-lg-4">
            <div class="mediapost">
                <div class="mediapost_left pull-left">
                    <span class="mediapost_pic marg10top">
                        <a href="<?php echo PageContext::$response->baseUrl;?>community/<?php echo $user->community_alias;?>"><img src="<?php if($user->file_path == ''){echo PageContext::$response->userImagePath;?>small/noimage.jpg<?php }else{echo PageContext::$response->userImagePath;?>small/<?php echo $user->file_path;}?>"></a>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl;?>profile/<?php $user->user_alias;?>"><?php echo $user->user_firstname.' '.$user->user_lastname;?></a></h4>
                    <p><?php echo $user->user_friends_count;?> Friends</p>
                    <p>
                    	<?php if(PageContext::$response->sess_user_id > 0){ ?>
                        <a uid="<?php echo $user->user_id;?>" href="<?php echo PageContext::$response->baseUrl;?>accept-invitation/<?php echo $user->invitation_id;?>/community/0/inbox"><span class="accept"><b>+</b> Accept</span></a>
                        <a uid="<?php echo $user->user_id;?>" href="<?php echo PageContext::$response->baseUrl;?>accept-invitation/<?php echo $user->invitation_id;?>/reject/0/inbox"><span class="reject"><i class="fa fa-times"></i> Reject</span></a>
                    	<?php }?>
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
            </div>

<!--
<div class="row">
<div class="colside1">
<div class="colside1_pic">
<a href="<?php echo PageContext::$response->baseUrl;?>community/<?php echo $user->community_alias;?>"><img src="<?php if($user->file_path == ''){echo PageContext::$response->userImagePath;?>small/noimage.jpg<?php }else{echo PageContext::$response->userImagePath;?>small/<?php echo $user->file_path;}?>"></a>
</div>
<div class="clear"></div>
</div>
<div class="colmiddle">
<h6><a href="<?php echo PageContext::$response->baseUrl;?>profile/<?php echo $user->user_alias;?>"><?php echo $user->user_firstname.' '.$user->user_lastname;?></a> </h6>has Invited you to Join   <h6><a href="<?php echo PageContext::$response->baseUrl;?>community/<?php echo $user->community_alias;?>"><?php echo $user->community_name; ?></a> </h6>
 
</div>
<div class="colside3">
<?php if(PageContext::$response->sess_user_id > 0){ ?>
<a class="friends_btn fright" uid="<?php echo $user->user_id;?>" href="<?php echo PageContext::$response->baseUrl;?>accept-invitation/<?php echo $user->invitation_id;?>/reject/0/inbox" style="float:left; ">
 
<span class="declinefriend_icon"></span>
Decline
</a>
<a class="friends_btn fright" uid="<?php echo $user->user_id;?>" href="<?php echo PageContext::$response->baseUrl;?>accept-invitation/<?php echo $user->invitation_id;?>/community/0/inbox" style="float:left; ">
 
<span class="tik_icon"></span>
Accept
</a>

	

 

 
<?php }?>

</div>
</div>
-->
<?php }}}?>
</div>
</div>
</section>
</div>
</div>