<style>
    .announcerrmsg
    {
        display: none;
        color: red;
     font-weight: bold;
    margin-left: 27%;
    }
</style>
<div class="" id="loadingannc" style="display:none;">
            <img src="<?php echo PageContext::$response->userImagePath.'/default/loader.gif'; ?>" />&ensp;Processing...
        </div>
<div class="row">
<div id="annsection"> 
<?php 
        if (PageContext::$response->community_created == $_SESSION['default_user_id'] ) { ?>
    <div class="col-sm-12 col-md-12 col-lg-12">
      <span id="annsuccmsg"  style="margin:10px 0 0 0; float:left; color: green;"><?php // if (count(PageContext::$response->communityDetails) <= 0) { echo "No records found!"; } ?></span>
      <a id="add_announcement" class="add_btt" style="margin-right:10px;">Add</a>
    </div> 
    <?php } ?>

    





    <div id="anouncement_add_sec"  class="annsection_cont_outer" style="display:none;" >
        <input type="hidden" name="announcement_id" id="announcement_id" value=""/>
        <div class="ibox-content bord0">
            <div class="row">
<!--            <form>-->
                <div class="form-group">
                    <label class="col-lg-3 control-label">Title <span>*</span></label>
                    <div class="col-lg-9">
                        <input name="announcement_title" id="announcement_title" type="text" class="form-control" >
                    </div>
                    <div class="clearfix"></div>
                    <span id="errtitle" class="announcerrmsg"></span>
                    
                </div>
                <div class="form-group"><label class="col-lg-3 control-label">Description <span>*</span></label>
                    <div class="col-lg-9">
                        <textarea name="announcement_desc" id="announcement_desc"  class="form-control"> </textarea>
                        <div class="clear"></div>
                    </div>
                    <div class="clearfix"></div>
                    <span id="errdesc" class="announcerrmsg"></span>
                </div>
                <div class="form-group"><label class="col-lg-3 control-label">Status</label>
                    <div class="col-lg-9">
                        <input type="radio" value="A" name="announcement_status" checked="checked"/> <span>Active</span> &nbsp; 
                        <input type="radio" name="announcement_status" value="I"/> <span>Inactive</span>
                        <input type="hidden" name ="hiddate" id="hiddate" value="<?php echo date("m/d/Y"); ?> ">
                        <input type="hidden" name="hidcommunity_id" id="hidcommunity_id" value="<?php echo PageContext::$response->communityId;?>">
                    </div>
                    <div class="clearfix"></div>
                    <span id="errdb" class="announcerrmsg"></span>
                </div>
                <div class="form-group"><label class="col-lg-3 control-label">Select Picture </label>
<!--                   // <div class="col-lg-9">-->
                    <!--<label for="file"></label>-->
                    <div class="col-lg-9">
                    <input type="file" id="announcement_file" name="announcement_file"/>  
                    </div>   
                    <div class="clear"></div>
<!--                  //  </div>-->
                    <div class="clearfix"></div>
                    <!--<span id="errdesc" class="announcerrmsg"></span>-->
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <button id="jadd_announcement" class="yellow_btn marg5right">ADD</button><button id="can_anno" class="yellow_btn">CANCEL</button>
                        <input type="hidden" name="tagg_id" value="0" id="tagg_id">
                    </div>
                </div>
            <!--</form>-->
            </div>
        </div>
                
    </div>
    <div id="jannouncementsDisplay">
        <input type="hidden" value="<?php echo count(PageContext::$response->communityDetails);?>" id="announcement_count">
     <?php 
        if (count(PageContext::$response->communityDetails) > 0) {
            foreach (PageContext::$response->communityDetails As $announcements) {
                ?>

              <div class="col-sm-6 col-md-6 col-lg-6">
                  <div class="mediapost announcementblk" id="announcementdiv_<?php echo $announcements->community_announcement_id ?>">
                      <div class="media-body">
                          <h4 class="media-heading">
                            <span id="announctitle_<?php echo $announcements->community_announcement_id ?>">
                                <a href="<?php echo PageContext::$response->baseUrl;?>announcement-detail/<?php echo $announcements->community_announcement_alias;?>" ><?php echo stripslashes($announcements->community_announcement_title); ?></a>
                            </span> 
                            <span class="commentsdate listannounce_date"><div id="announcdate_<?php echo $announcements->community_announcement_id ?>"><?php echo date('m/d/Y',strtotime($announcements->community_announcement_date)); ?>
                      </div></span>
                          </h4>
                          <p id="announcdesc_<?php echo $announcements->community_announcement_id ?>"><?php echo stripslashes($announcements->community_announcement_content)."" ?></p>
						  </div>
                         <?php if($announcements->file_path != ''){ ?>
						 <div class="clear"></div>
						 <div class="profile-bnr">
                          <img id="img_<?php echo $announcements->community_announcement_id ?>" src="<?php echo PageContext::$response->userImagePath; ?><?php echo $announcements->file_path;?>" class="img-responsive">
						  </div>
						 <div class="clear"></div>
                         <?php } ?>
                          <div class="listannouncements_img_bottom">
                          <?php if($announcements->community_announcement_created_by == $_SESSION['default_user_id']) {?>
                      <a class="delete_link_business" style="cursor:pointer;" onclick="return deleteConfirm(<?php echo $announcements->community_announcement_id ?>);">Delete</a> <a class="edit_link_business" style="cursor:pointer;" onclick="return editAnnonc(<?php echo $announcements->community_announcement_id ?>);">Edit</a>
                    <?php } ?>  
                          </div>
                      
                      <div class="clearfix"></div>
                  </div>
              </div>
    

    
    <?php
            }
            
            }
            else{?>
    <div class="col-sm-12 col-md-12 col-lg-12 no_announcement">No announcements added</div>
                
          <?php  }
           
            
    ?>
    </div>
</div>
</div>
<script>
    
$(".readmore").live('click',function(){
    
    var str = $(this).attr('id');
    var id = str.split("_");
    
    if($("#announcements_"+id[1]).css('height') == "54px")
        {
    $("#announcements_"+id[1]).css({'height':'auto'});
    $("#readmore_"+id[1]).html("Hide");
        }
    else
        {
    $("#announcements_"+id[1]).css({'height':'54px'});
    $("#readmore_"+id[1]).html("Read More ");
        }
    
}); 

$("#add_announcement").click(function(){
    $("#jadd_announcement").html('ADD');
    $("#annsuccmsg").html("");
    $("#announcement_id").val("");
    $("#announcement_title").val("");
    $("#announcement_desc").val("");
    $('input[name=announcement_status]').filter('[value=A]').prop('checked', true);
    $("#anouncement_add_sec").show();
    $(".business_profilelist_detailsarea").hide();
    $("#norec").hide();
});



$("#can_anno").click(function(){
    
    $("#annsuccmsg").html("");
    $("#anouncement_add_sec").hide();

//    if($(".business_profilelist_detailsarea").length == 0)
//       $("#annsuccmsg").html("No records found!");
   
    $(".business_profilelist_detailsarea").show();
});

function deleteConfirm(val)
{
         var commid  = "<?php echo PageContext::$response->communityId; ?>";
         $("#annsuccmsg").html("");
         res = confirm("Do you want to delete the record!");
         
         if(res)
             {
                var count = $("#announcement_count").val();
                count= count-1;
                $("#annsection").hide();
                $("#loadingannc").show(); 
                $.ajax({
    		 type    : "POST",
    		 url     : mainUrl+"user/deleteannouncements",
    		 data    : 'announcement_id='+val+'&community_id='+commid,
    		 cache   : false,
    		 success : function(data){
                   //  alert(data);exit;
                                    $("#announcement_setting").html("Announcements ("+count+")");
                                    $("#annsection").show();
                                    $("#loadingannc").hide();
                                    $("#announcementdiv_"+val).remove();
                                    $("#annsuccmsg").html("Announcement deleted successfully!");
                                    $("#announcement_count").val(count);
                                    
//                                    if($(".business_profilelist_detailsarea").length == 0)
//                                        $("#annsuccmsg").html("No records found!");
                               
    		 }
    	 }); 
             }
         else
             return false;
}
String.prototype.stripSlashes = function(){
    return this.replace(/\\(.)/mg, "$1");
}
function editAnnonc(val)
{
    $(".announcerrmsg").html("");
    $("#annsection").hide();
    $("#loadingannc").show();
    $("#annsuccmsg").html("");
    $(".business_profilelist_detailsarea").hide();
    $.ajax({
    		 type    : "POST",
    		 url     : mainUrl+"user/editannouncements",
    		 data    : 'announcement_id='+val,
                 dataType: "json",
    		 cache   : false,
    		 success : function(data){ 
                     $("#annsection").show();
                     $("#loadingannc").hide();
                     $("#anouncement_add_sec").show();
                     $("#announcement_id").val(data.community_announcement_id);
                     $("#jadd_announcement").html('UPDATE');
                     $("#announcement_title").val(data.community_announcement_title.stripSlashes());
                     $("#announcement_desc").val(data.community_announcement_content.stripSlashes());
                     //$("#announcement_date").val(data.community_announcement_date);
                     $('input[name=announcement_status]').filter('[value='+data.community_announcement_status+']').prop('checked', true);
                     
                     $('html,body').animate({
        scrollTop: $("#anouncement_add_sec").offset().top},
        'slow');
        //$(".business_profilelist_detailsarea").show();
    		 }
    	 }); 
}
</script>