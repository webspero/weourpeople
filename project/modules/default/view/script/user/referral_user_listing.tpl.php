<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
<div id="o-wrapper" class="o-wrapper">
<!---Menu Link----------------- -->
{if PageContext::$response->sess_user_id >0}
<div class="main">
<div class="header-main">
     <main class="o-content">
        <div class="o-container">
          
          <div class="c-buttons">
            <button id="c-button--slide-right" class="button-nav-toggle"><span class="icon">&#9776;</span></button>
          </div>
          
          <div id="github-icons"></div>

        </div><!-- /o-container -->
      </main><!-- /o-content -->    
</div>
</div>  
{/if}
</div>
<!---EOF Menu Link----------------- -->
            <h3>Referral Request To Join My Business Community</h3>
            <div class="" id="msg"></div>
            <div class="clear"><br></div>
            <div class="{PageContext::$response->message['msgClass']}">{PageContext::$response->message['msg']}</div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div class="bs-wizard" style="border-bottom:0;">
            <div class="col-xs-3 bs-wizard-step complete">
              <div class="text-center bs-wizard-stepnum">Step 1</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a title="Set the most appropriate campaign type(Goodwill, cash, voucher or charity) and the associated incentive levels for The referral stages" href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">
                    <p title="Set the most appropriate campaign type(Goodwill, cash, voucher or charity) and the associated incentive levels for The referral stages">Bizcom</p>
                    <span title="Set the most appropriate campaign type(Goodwill, cash, voucher or charity) and the associated incentive levels for The referral stages">Choose Bizcom</span>
<!--                    <div style="font-weight: bold; font-size: 12px; text-align: left;"> Set the most appropriate campaign type(Goodwill, cash, voucher or charity) and the associated incentive levels for The referral stages </div>-->
              </div>
            </div>
            
            <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
              <div class="text-center bs-wizard-stepnum">Step 2</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a title="You choose one of your BizCom's to which you want to drive new membership/customers" href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">
                <p title="You choose one of your BizCom's to which you want to drive new membership/customers">Campaign</p>
                <span title="You choose one of your BizCom's to which you want to drive new membership/customers">Campaign Details</span>
<!--                <div style="font-weight: bold; font-size: 12px; text-align: left;"> You choose one of your BizCom's to which you want to drive new membership/customers </div>-->
              </div>
            </div>
            
            <div class="col-xs-3 bs-wizard-step active"><!-- complete -->
              <div class="text-center bs-wizard-stepnum">Step 3</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a title="Decide who you want to approach & how many recipients and manage your marketing budget parameters accordingly" href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">
                  <p title="Decide who you want to approach & how many recipients and manage your marketing budget parameters accordingly">Recipients</p>
                <span title="Decide who you want to approach & how many recipients and manage your marketing budget parameters accordingly">Choose recipients</span>
<!--                 <div style="font-weight: bold; font-size: 12px; text-align: left;"> Decide who you want to approach & how many recipients and manage your marketing budget parameters accordingly </div>-->
              </div>
            </div>
            
            <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
              <div class="text-center bs-wizard-stepnum">Step 4</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a title="Specify any guidelines of your own and add any special messaging that you want included in your request-then initiate" href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">
                <p title="Specify any guidelines of your own and add any special messaging that you want included in your request-then initiate">Message</p>
                <span title="Specify any guidelines of your own and add any special messaging that you want included in your request-then initiate">Personal Message</span>
<!--                <div style="font-weight: bold; font-size: 12px; text-align: left;"> Specify any guidelines of your own and add any special messaging that you want included in your request-then initiate. </div>-->
              </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <form  id="frmUserListing" method="Post" class="marg20col border_top1px pad15top">
                    <div id="jDivUserDetails">
                        <div class="intro_list" id="divDisplayGoodFriendIntro">
                            <h4>Specify Contacts-if Any</h4>
                            <!--If you have a specific person in mind that you asking your contact to refer you to then enter their details below (include name, email and business details if you have them) – if you dont have a specific person in mind then leave this blank-->
                            If you have specific contacts in mind to whom would like your referrer to make an introduction for you, enter their name or email details here. If you don't then leave this blank.
                            <div class="member_search">
                                <input type="text" placeholder="Enter name,email or business details" value="{PageContext::$response->campaignDetails->refferalcampaign_mail_specified_contact}" name="txtSpecifiedContact" id="txtSpecifiedContact" class="form-control" >
                            </div>
                            <div class="clear"></div>
                            <div id="displayusererror" style="display:none;color:#FF3A05;padding-left:12px;"></div>
                        </div>

                        <div class="intro_list">
                            <div  id="divDisplayGoodFriendIntro" >
                                <div>
                                    <h4>Please select one of the following</h4>
                                </div>
                                {foreach from=PageContext::$response->invitationType key=id item=user}
                                <div>
                                    <input type="radio" {if PageContext::$response->campaignDetails->refferalcampaign_member_type eq $id}checked{/if} name="user_type" class="jUserType" value="{$id}">&nbsp;{$user}
                                </div>
                                {/foreach}
                                <div id="displayusererror" style="display:none;color:#FF3A05;padding-left:12px;"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="marg10col">
                            <input type="hidden" id="back_value" value="{pagecontext::$response->saveStepFlag}">
                        <div id="divDisplayIntroductionMemberDetails" >
                            {if PageContext::$response->displayType eq 'bizcom_members'}
                            <h4>Choose the members whom you wish to send the introduction </h4>
                            <!--<h4>Enter the email details of the member who sent you the referral offer request</h4>-->
                            <a href="referral_campaign_details.tpl.php"></a>
                            <div class="member_search">
                                <div class="wid100per">
                                    <input type="text" name="searchmembers" id="searchmembers" class="form-control" placeholder="Search Members">
                                </div>
                                <div class="clear"></div>
                                 {if PageContext::$response->campaignDetails->refferalcampaign_member_type < 4 AND PageContext::$response->numRecipients > 0}
                                <div class="wid100per"><b>Number of recipients chosen</b> - {PageContext::$response->numRecipients}</div>
                                 {/if}
                                <div class="clear"></div>
                            </div>
                            <div><label class="error" for="txtmembers" generated="true"></label></div>
                            <!--<div id="jLoaderMember" style="display:none"><img src="<?php echo BASE_URL; ?>project/themes/default/images/loadinfo.gif"></div>-->
                            <input type="hidden" name="hidSelectedMembers" id="hidSelectedMembers" value="{PageContext::$response->recipients}" >
                            <input type="hidden" name="hidCommunityAlias" value="{PageContext::$response->communityAlias}" id="hidCommunityAlias" >
                            <input type="hidden" name="hidTotalPages" value="{PageContext::$response->totpages}" id="hidTotalPages" >
                            <input type="hidden" name="hidSelectedMembersCount" id="hidSelectedMembersCount" value="{PageContext::$response->refferalcampaign_max_introductions}">

                            <div id="frndslisting_blk_content" class="row">
                                {if PageContext::$response->members}
                                {foreach from=PageContext::$response->members key=key item=val}

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="mediapost">
                                        <div class="mediapost_left pull-left">
                                            <span class="mediapost_pic">
                                                {if $val->file_path}
                                                {assign 'file_path' $val->file_path}
                                                {else}
                                                {assign 'file_path' 'member_noimg.jpg'}
                                                {/if}
                                                <img src="{PageContext::$response->userImagePath}thumb/{$file_path}">
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <div class="display_table">
                                                <div class="display_table_cell wid90per">
                                                    {if $val->user_firstname}
                                                    {assign 'uname'  $val->user_firstname|cat:' '|cat:$val->user_lastname}
                                                    {else}
                                                    {assign 'uname' $val->user_email}
                                                    {/if}
                                                    <h3 class="marg20col">{$uname}</h3>
                                                </div>
                                                <div class="display_table_cell wid10per">
                                                    <input type="hidden" name="inv_check[{$val->user_id}]" value="0">
                                                    <div class="loading_invite" id="loading_invite{$key}"></div>
                                                    <input type="checkbox" name="txtmembers" id="{$val->user_id}" {if in_array($val->user_id,PageContext::$response->recipientsId)}checked{/if} value="{$val->user_id}" />

                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                {/foreach}
                                <div class="clear"></div>
                                {/if}
                                {if PageContext::$response->totpages > PageContext::$response->currentpage}
                                <div id="div_display_more_friends">
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <a id="load_more_user_bizcom_button" class="linkstyle2" tot="{PageContext::$response->totpages}">Load More</a>
                                    </div>
                                </div>
                                {/if}
                            </div>
                            {elseif PageContext::$response->displayType eq 'friends'}

                            <h4>Choose the members whom you wish to send the introduction </h4>
                            <div class="member_search">
                                <div class="wid100per">
                                    <input type="text" name="searchintrofriends" id="searchintrofriends" class="form-control" placeholder="Search Members">
                                </div>
                                <div class="clear"></div>
                                {if PageContext::$response->campaignDetails->refferalcampaign_member_type < 4 AND PageContext::$response->numRecipients > 0}
                                <div class="wid100per"><b>Number of recipients chosen</b> - {PageContext::$response->numRecipients}</div>
                                 {/if}
                                <div class="clear"></div>
                            </div>
                            <!--<div id="jLoaderMember" style="display:none"><img src="<?php echo BASE_URL; ?>project/themes/default/images/loadinfo.gif"></div>-->
                            <input type="hidden" name="hidSelectedMembers" id="hidSelectedMembers" value="{PageContext::$response->recipients}" >
                            <input type="hidden" name="hidUserAlias" value="{PageContext::$response->userAlias}" id="hidUserAlias" >
                            <input type="hidden" name="hidTotalPages" value="{PageContext::$response->totpages}" id="hidTotalPages" >
                            <div id="frndslisting_blk_content" class="row">
                                {if PageContext::$response->members}
                                {foreach from=PageContext::$response->members key=key item=val}

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="mediapost">
                                        <div class="mediapost_left pull-left">
                                            <span class="mediapost_pic">
                                                {if $val->file_path}
                                                {assign 'file_path' $val->file_path}
                                                {else}
                                                {assign 'file_path' 'member_noimg.jpg'}
                                                {/if}
                                                <img src="{PageContext::$response->userImagePath}thumb/{$file_path}">
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <div class="display_table">
                                                <div class="display_table_cell wid90per">
                                                    {if $val->user_firstname}
                                                    {assign 'uname'  $val->user_firstname|cat:' '|cat:$val->user_lastname}
                                                    {else}
                                                    {assign 'uname' $val->user_email}
                                                    {/if}
                                                    <h3 class="marg20col">{$uname}</h3>
                                                </div>
                                                <div class="display_table_cell wid10per" id="{$key}">
                                                    <input type="hidden" name="inv_check[{$val->user_id}]" value="0">
                                                    <div class="loading_invite" id="loading_invite{$key}"></div>
                                                    <input type="checkbox" name="inv_check[{$val->user_id}]" id="{$val->user_id}" value="{$val->user_id}" {if in_array($val->user_id,PageContext::$response->recipientsId)}checked{/if} />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                {/foreach}
                                <div class="clear"></div>
                                {/if}
                                {if PageContext::$response->totpages > PageContext::$response->currentpage}
                                <div id="div_display_more_friends">
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <a id="load_more_friends_button" class="linkstyle2" tot="{PageContext::$response->totpages}" alias="{PageContext::$response->userAlias}">Load More</a>
                                    </div>
                                </div>
                                {/if}
                            </div>
                            
                            {elseif PageContext::$response->displayType eq 'non_bizcom_members'}
                            <h4>Choose the members whom you wish to send the introduction </h4>
                            <div class="member_search">
                                <div class="wid100per">
                                    <input type="text" name="searchNonBizcomMembers" id="searchNonBizcomMembers" class="form-control" placeholder="Search Members">
                                </div>
                                <div class="clear"></div>
                                {if PageContext::$response->campaignDetails->refferalcampaign_member_type < 4 AND PageContext::$response->numRecipients > 0}
                                <div class="wid100per"><b>Number of recipients chosen</b> - {PageContext::$response->numRecipients}</div>
                                 {/if}
                                <div class="clear"></div>
                            </div>
                            <!--<div id="jLoaderMember" style="display:none"><img src="<?php echo BASE_URL; ?>project/themes/default/images/loadinfo.gif"></div>-->
                            <input type="hidden" name="hidSelectedMembers" id="hidSelectedMembers" value="{PageContext::$response->recipients}" >
                            <input type="hidden" name="hidCommunityAlias" value="{PageContext::$response->communityAlias}" id="hidCommunityAlias" >
                            <input type="hidden" name="hidTotalPages" value="{PageContext::$response->totpages}" id="hidTotalPages" >
                            <div id="frndslisting_blk_content" class="row">
                                {if PageContext::$response->members}
                                {foreach from=PageContext::$response->members key=key item=val}

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="mediapost">
                                        <div class="mediapost_left pull-left">
                                            <span class="mediapost_pic">
                                                {if $val->file_path}
                                                {assign 'file_path' $val->file_path}
                                                {else}
                                                {assign 'file_path' 'member_noimg.jpg'}
                                                {/if}
                                                <img src="{PageContext::$response->userImagePath}thumb/{$file_path}">
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <div class="display_table">
                                                <div class="display_table_cell wid90per">
                                                    {if $val->user_firstname}
                                                    {assign 'uname'  $val->user_firstname|cat:' '|cat:$val->user_lastname}
                                                    {else}
                                                    {assign 'uname' $val->user_email}
                                                    {/if}
                                                    <h3 class="marg20col">{$uname}</h3>
                                                </div>
                                                <div class="display_table_cell wid10per" id="{$key}">

                                                    <input type="hidden" name="inv_check[{$val->user_id}]" value="0">
                                                    <div class="loading_invite" id="loading_invite{$key}"></div>
                                                    <input type="checkbox" name="inv_check[{$val->user_id}]" id="{$val->user_id}" value="{$val->user_id}" {if in_array($val->user_id,PageContext::$response->recipientsId)}checked{/if}  />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                {/foreach}
                                <div class="clear"></div>
                                {/if}
                                {if PageContext::$response->totpages > PageContext::$response->currentpage}
                                <div id="div_display_more_friends">
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <a id="load_more_bizcom_button" class="linkstyle2" tot="{PageContext::$response->totpages}" alias="{PageContext::$response->communityAlias}">Load More</a>
                                    </div>
                                </div>
                                {/if}
                            </div>
                            {elseif PageContext::$response->displayType eq 'bizcom'}
                            <h4>Choose the members whom you wish to send the introduction</h4>
                            <div class="member_search">
                                <input type="text" name="searchbizcom" id="searchbizcom" class="form-control" placeholder="Search Members">
                                <div class="clear"></div>
                                {if PageContext::$response->pifstatus eq 'PIF'}
                                <div class="wid100per"<input class="yellow_btn" type="button" id="{$nextId}" value="Next" name="">/div>
                                 {/if}
                                <div class="clear"></div>
                            </div>
                            <!--<div id="jLoaderMember" style="display:none"><img src="<?php echo BASE_URL; ?>project/themes/default/images/loadinfo.gif"></div>-->
                            <input type="hidden" name="hidSelectedMembers" id="hidSelectedMembers" value="{PageContext::$response->recipients}" >
                            <div id="frndslisting_blk_content" class="row">
                                {if PageContext::$response->members}
                                {foreach from=PageContext::$response->members  key=key item=val}

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="mediapost">
                                        <div class="mediapost_left pull-left">
                                            <span class="mediapost_pic">
                                                {if $val->file_path}
                                                {assign 'file_path' $val->file_path}
                                                {else}
                                                {assign 'file_path' 'member_noimg.jpg'}
                                                {/if}
                                                <img src="{PageContext::$response->userImagePath}thumb/{$file_path}">
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <div class="display_table">
                                                <div class="display_table_cell wid90per">
                                                    <h3 class="marg20col">{$val->community_name}</h3>
                                                </div>
                                                <div class="display_table_cell wid10per" id="{$key}">
                                                    <input type="hidden" name="inv_check[{$val->user_id}]" value="0">
                                                    <div class="loading_invite" id="loading_invite{$key}"></div>
                                                    <input type="radio" name="rad_bizcom" mcount="{$val->memberCount}" value="{$val->community_id}" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                {/foreach}
                                <div class="clear"></div>
                                {/if}
                                {if PageContext::$response->totpages > PageContext::$response->currentpage}
                                <div id="div_display_more_friends">
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <a id="load_more_bizcom_button" class="linkstyle2" tot="{PageContext::$response->totpages}" alias="{PageContext::$response->communityAlias}">Load More</a>
                                    </div>
                                </div>
                                {/if}
                            </div>
                            
                            {elseif PageContext::$response->displayType eq 'non_bbf_member'}
                            <h4>Enter the email address </h4>
                            <!--<div id="jLoaderMember" style="display:none"><img src="<?php echo BASE_URL; ?>project/themes/default/images/loadinfo.gif"></div>-->    

                            <div class="wid100per">
                                <textarea name="txtMailIds" id="txtMailIds" style="height: 98px;">{PageContext::$response->recipientsMail}</textarea>
                                <div class="clear"></div>
                            </div>
                            <div class="member_search">

                                <div class="clear"></div>
                            </div>
                            {/if}
                        </div>
                    </div>
                    </div>
                    <div class="wid100per">
                        <div class="col-sm-8 col-md-offset-4">
                            <div class="biz_step_btns_group">
                                <div class="bizcom_back"><a href="{PageContext::$response->baseUrl}refferal-campign-details/back" id="jrefferalRequestDisplay" class="grey_btn jCampaign">Back</a></div>
                                <div class="bizcom_next">
                                    <button type="submit" name="saveThirdStep" id="" class="yellow_btn3 jUserList jCampaign" value="Save" >Save</button>
                                </div>
                                <div class="bizcom_next">
                                <button type="submit" name="thirdStep" id="" class="yellow_btn3 jUserList jCampaign" value="Next" >Save and Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </form>
        </div>
        <div class="clearfix"></div>
    </div>
</div>






