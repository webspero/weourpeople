<?php foreach (PageContext::$response->Comments AS $comments) {
    //echopre($comments);
    ?>
    <div class="wid100p pad10p border_left1px" id="jdivComment_<?php echo $comments->news_feed_comments_id; ?>">
        <div class="row">
            <div class="col-md-1 pad_left_0">
                <a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $comments->user_alias; ?>">
                    <?php if ($comments->user_image != '') { ?>
                        <span class="mediapost_pic">
                    <img class="ryt-mrg"
                         src="<?php echo PageContext::$response->userImagePath; ?><?php echo $comments->user_image; ?>">
                </span>
                    <?php } else { ?>
                        <span class="mediapost_pic">
                    <img class="ryt-mrg"
                         src="<?php echo PageContext::$response->userImagePath; ?>medium/member_noimg.jpg">
                </span>
                    <?php } ?>
                </a>
            </div>
            <div class="col-md-11">
                <?php if ($comments->file_path) { ?>
                    <img class="fancybox"
                         src="<?php echo PageContext::$response->userImagePath; ?>medium/<?php echo $comments->file_path; ?>">
                    <br>
                <?php } ?>
                <span
                    class="name"><a href="<?php echo PageContext::$response->baseUrl; ?>timeline/<?php echo $comments->user_alias; ?>"><?php echo $comments->Username; ?></a>
                </span>
                <span class="jEmojiable">
                    <?php echo $comments->news_feed_comment_content; ?>
                </span>
                <br>
                <span class="new-text">
                    <?php echo $comments->commentDate; ?></span>
                <?php if ($comments->news_feed_comment_user_id == PageContext::$response->sess_user_id) { ?>
                    <a href="#" class="jNewsFeedReplyDelete right" aid="<?php echo $comments->parent_comment_id; ?>"
                       id="<?php echo $comments->news_feed_comments_id; ?>"><i class="fa fa-times"></i> Delete</a>
                <?php } ?>
                <div class="mediapost_user-side">
                    <span class="mediapost_links"
                          id="jcountcommentlike_<?php echo $comments->news_feed_comments_id; ?>"></span>
                </div>
            </div>
        </div>
    </div>
<?php }
if (PageContext::$response->TotalPage != PageContext::$response->CurrentPage) {
    ?>
    <div id="jDisplayMoreComments_<?php echo $comments->news_feed_comments_id; ?>">
        <a href="#" id="jMorecommentsButton"
          pid="<?php echo PageContext::$response->CurrentPage; ?>"
          aid="<?php echo $comments->news_feed_comments_id; ?>">
            Load more comments</a>
    </div>
<?php }
?>


