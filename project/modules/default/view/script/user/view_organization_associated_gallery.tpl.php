<script>

    var TOTAL_PAGES = <?php echo PageContext::$response->totalPages?>;
    console.info('TOTAL_PAGES '+TOTAL_PAGES );
    $(document).ready(function () {

        magnificPopupFn();
        magnificPopupGroupFn();
        
        var page = 1;
        var _throttleTimer = null;
        var _throttleDelay = 100;
        $(window).scroll(function () {
            clearTimeout(_throttleTimer);
            _throttleTimer = setTimeout(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                    if (page < TOTAL_PAGES) {
                        page++;
                        $.blockUI({message: ''});
                        var business_id = $("#business_id").val();
                        $.ajax({
                            type: 'Post',
                            url: mainUrl + 'user/view_organization_associated_galleryajax/' + page + '/' + business_id,
                            data: 'business_id=' + business_id,
                            success: function (data) {
                                $('.main_gallary').append(data);
                                magnificPopupFn();
                                $('.listitem').fadeIn(3000);
                                $('.listitem').hide();
                            }
                        })
                    }
                }
            }, _throttleDelay);
        });
    });

</script>
<div class="col-sm-4 col-md-5 col-lg-3">   
     <?php if(count(PageContext::$response->friend_display_list) > 0){?>            
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-user" aria-hidden="true"></i> Followers <a  href="#" id="jMoreOrgphotos" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist">
                    <?php foreach(PageContext::$response->friend_display_list as  $key=>$friendslist){
                       // echopre($friendslist);?>
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <a class="friendphotobox_blk" href="<?php echo PageContext::$response->baseUrl;?>timeline/<?php echo $friendslist->user_alias;?>">
                                <?php if($friendslist->file_path) {?>
                                <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?><?php echo $friendslist->file_path;?>');">
                                </div>
                                <?php }else{ ?>
                                 <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>medium/member_noimg.jpg');">
                                </div>
                                <?php } ?>
                                <h4><?php echo ucfirst($friendslist->user_firstname)." ".ucfirst($friendslist->user_lastname);?></h4>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
                </div>  
   <?php } ?>
    <?php if(count(PageContext::$response->gallery_display_list) > 0){   ?>
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-file-image-o" aria-hidden="true"></i> Photos <a href="#" id="jMoreOrgGallery" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist portfolio">
                     <?php foreach(PageContext::$response->gallery_display_list as  $key=>$gallerylist){
                        ?>
                     
<!--                    <li class="display_table_cell wid32per">
                        <a class="friendphotobox_blk" href="#">
                            <div class="friendphotobox" style="background-image: url('{PageContext::$response->userImagePath}medium/{$gallerylist->news_feed_image_name}');">-->
                                
<!--                            <ul class="portfolio" class="clearfix">-->
                                    <li class="pull-left wid32per vert_align_top"><a class="friendphotobox_blk" href="<?php echo PageContext::$response->userImagePath;?><?php echo $gallerylist->organization_announcement_image_path;?>" title=""><img src="<?php echo PageContext::$response->userImagePath;?><?php echo $gallerylist->organization_announcement_image_path;?>" alt=""></a></li>
<!--                            </ul>-->
                                
<!--                            </div>
                        </a>
                    </li>-->
                     <?php } ?> 
                </ul>
                <!--<div class="sidead" style="background-image: url('{PageContext::$response->ImagePath}sidead.jpg');"> </div>-->
            </div>  
    <?php } ?>  
</div>
<div class="col-sm-8 col-md-7 col-lg-9">
<div class="whitebox">
    <div class="row">
        <?php
        if (PageContext::$response->imageDetails) { ?>
            <br>
            <div class="clearfix"></div>
            <ul id="portfolio" class="clearfix main_gallary">
                <?php foreach (PageContext::$response->imageDetails As $images) {?>
                    <li class='col-xs-6 col-sm-4 col-md-4 col-lg-4'><a href="<?php echo PageContext::$response->userImagePath.$images->organization_announcement_image_path;?>" title="<?php echo $images->organization_announcement_content;?>"><img src="<?php echo PageContext::$response->userImagePath."medium/".$images->organization_announcement_image_path;?>" alt="<?php echo $images->organization_announcement_content;?>"></a></li>
                <?php } ?>
            </ul>
        <?php } else { ?>
            <div class="frndslisting_blk_content">
                <span class="margin_l_20">No Images Found</span>
            </div>
        <?php } ?>
    </div>
</div>
</div>
