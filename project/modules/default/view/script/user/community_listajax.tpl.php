<link rel="stylesheet" href="<?php echo BASE_URL?>project/styles/rateit.css" type="text/css" />
<script src="<?php echo BASE_URL?>project/js/jquery.rateit.js" type="text/javascript"></script>
<script src="<?php echo BASE_URL?>project/js/ratinglist.js" type="text/javascript"></script>
<?php if(PageContext::$response->totalrecords > 0){
//            {assign var="i" value=PageContext::$response->slno}
            foreach(PageContext::$response->leads as $id=>$lead){?>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="mediapost">
                    <div class="mediapost_left pull-left">
                        <span class="mediapost_pic">

                            <a href="<?php echo PageContext::$response->baseUrl;?>community/<?php echo $lead->community_alias;?>">
                                <img class="business_profile_desc_colside_pic" alt="<?php echo $lead->file_orig_name;?>" src="<?php echo PageContext::$response->userImagePath;if($lead->file_path != ''){?><?php echo $lead->file_path;}?>"
<!--                                  <?php // elseif($lead->file_path == ''){?>default/no_image_bbf.jpg"<?php // }?> > -->
                            </a>
                        </span>
                    </div>

                    <div class="media-body">
                        <h4 class="media-heading"><span><a href="<?php echo PageContext::$response->baseUrl;?>community/<?php echo $lead->community_alias;?>"><?php echo $lead->community_name;?></a></span></h4>
                        <?php if($lead->community_status =='I'){   ?>
                        <span class="status clserror" >Pending Approval</span> 
                        <?php } else{?>
                        <div class="display_table">
                            <div class="display_table_cell wid60per">
                                <div id="starring">
                                    <div class="rateit" id="rateit_<?php echo $lead->ratetype_id;?>" data-rateit-value="<?php echo $lead->ratevalue;?>" data-rateit-ispreset="true" data-rateit-id="<?php echo $lead->rateuser_id;?>" data-rateit-type="<?php echo $lead->ratetype;?>" data-rateit-typeid="<?php echo $lead->ratetype_id;?>" data-rateit-flag="<?php echo $lead->rateflag;?>" data-rateit-step=1 data-rateit-resetable="false">
                                    </div>
                                    <div class="right">
                                        <?php if(PageContext::$response->sess_user_id == $lead->community_created_by){?>
                                        <a href="<?php echo PageContext::$response->baseUrl;?>edit-community/<?php echo $lead->community_id;?>" class="edititem left marg5right"><i class="fa fa-pencil"></i></a>
                                        <a href="<?php echo PageContext::$response->baseUrl;?>index/delete_community/<?php echo $lead->community_id;?>" class="deleteitem left"><i class="fa fa-trash"></i></a> 
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <?php } ?>
                        <p>
                            <?php echo substr($lead->community_description,0,200)."...";?>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
<?php }} ?>
<!--            {else}
            <div class="business_profilelist_detailsarea">{if PageContext::$response->searchtype eq ''}No communities are available.{else}No matches found.{/if}
        {if PageContext::$response->sess_user_id neq 0 and PageContext::$response->sess_user_status neq 'I'}Click to <a href="{php} echo PageContext::$response->baseUrl;{/php}add-community">create a community</a>. {/if}
    </div>
    {/if}   
            <div class="clear"></div>
            {if PageContext::$response->totalrecords gt PageContext::$response->itemperpage}
    <div>{PageContext::$response->pagination} </div>
    {/if}-->
