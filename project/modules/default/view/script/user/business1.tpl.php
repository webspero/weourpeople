<div class="container">
    <div class="row">
        {if PageContext::$response->resultSet->business_name neq ''}
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="timelineheader">
                <div class="timelineheader_right">
                    <div class="cover-wrapper"
                         style="background-image: url('{PageContext::$response->userImagePath}{PageContext::$response->resultSet->business_timeline_image_name}');">
                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <div class="timelineheader_left profilepic_business1blk">
                                <input type="hidden" name="business_id" id="business_id"
                                       value="{PageContext::$response->business_id}">
                                 <input type="hidden" name="user_id" id="user_id"
                                       value="{PageContext::$response->sess_user_id}">
                                <!--{if PageContext::$response->user_image neq ''}
                                     <div class="timeline_left_pic" style="background-image: url('{PageContext::$response->userImagePath}{PageContext::$response->user_image}');"></div>
                                     {else}-->
                                <div class="timeline_left_pic"
                                     style="background-image: url('{PageContext::$response->userImagePath}{PageContext::$response->resultSet->file_path}');">
                                </div>

                                <!--{/if}-->
                                <h3><a href="#" target="_blank">{PageContext::$response->user_name}</a></h3>
                                {if PageContext::$response->resultSet->business_created_by eq PageContext::$response->sess_user_id}
                                <form name="orgtimelineimageupload" class="upload">
                                    <i class="fa fa-camera" id="jOrgCamProfile"></i><span class="name"></span>
                                    <input type="file" name="timelinepic" id="jorgprofilepic" style="display:none;">
                                </form>
                                {/if}
                                <h3>{PageContext::$response->resultSet->business_name}</h3>
                            </div>
                              
                           
                        </div>
                        
                        {if PageContext::$response->resultSet->business_created_by eq
                        PageContext::$response->sess_user_id}
                        <div class="timelineimage">
                            <form name="frmtimelineimageupload" id="jfrmtimelineimageupload"
                                  action="{PageContext::$response->baseUrl}user/timelineimageresize"
                                  enctype="multipart/form-data" method="post">
                                <i class="fa fa-camera" id="jorgcam"></i><span class="name"></span>
                                <input type="file" name="jorgtimelinepic" id="jorgtimelinepic" style="display:none;">
                            </form>
                        </div>
                        {/if}
                        <div class="menufloattop">
                            {if PageContext::$response->sess_user_id neq PageContext::$response->resultSet->business_created_by && PageContext::$response->sess_user_id gt 0}
                              {if in_array(PageContext::$response->sess_user_id,PageContext::$response->tagged)}
                              <a id="" class="jUnfollowPage" aid="{PageContext::$response->resultSet->business_id}" href="javascript:void(0)" bid="{PageContext::$response->resultSet->tagg_id}"><i class="fa fa-user-times"></i> Unfollow</a>
                                {else}
                                <a id="jFollowPage" aid="{PageContext::$response->resultSet->business_id}" href="javascript:void(0)"><i class="fa fa-user-plus"></i> Follow</a>
                              {/if}
                            {/if}
                            </a></div>
                    </div>
                    <div class="timeline_tabmenu buztimelineblock">
                            <ul id="myTab" class="nav nav-tabs nav-justified">

                                <!-- Organization Details  tab -->
                                <li class="active"><a href="#" id="jViewBusiness" class="jtab" data-toggle="tab">About
                                        </a>
                                </li>

                                <!-- Discussion tab -->
                                <li class=""><a id="jViewDiscussion" alias="{PageContext::$response->rate_entity_alias}"
                                                href="#jViewDiscussion" class="jtab" data-toggle="tab">Discussions</a>
                                </li>

                                <!-- Associated People tab -->
                                <li class="jShowProfile" alias="{PageContext::$response->rate_entity_alias}"><a alias="{PageContext::$response->rate_entity_alias}"
                                        id="jViewAssociatedPeople" bid="{PageContext::$response->business_id}"
                                        bname="{PageContext::$response->resultSet->business_name}" class="jtab"
                                        href="#jViewAssociatedPeople" data-toggle="tab">Followers</a>
                                </li>

                                <!-- Associated Group tab -->
                                {if PageContext::$response->org_grp eq 1}
                                <li class="jShowFriends" alias="{PageContext::$response->rate_entity_alias}"><a alias="{PageContext::$response->rate_entity_alias}"
                                        bid="{PageContext::$response->business_id}"
                                        bname="{PageContext::$response->resultSet->business_name}" class="jtab "
                                        id="jViewAssociatedBizcom" href="#jViewAssociatedBizcom" data-toggle="tab"
                                        >Groups</a>
                                </li>
                                {/if}

                                <!-- Testimonials tab -->
                                <li class="jShowFriends" alias="{PageContext::$response->rate_entity_alias}"><a
                                        bid="{PageContext::$response->business_id}"
                                        bname="{PageContext::$response->resultSet->business_name}" class="jtab "
                                        id="jViewAssociatedTestimonials" href="#jViewAssociatedTestimonials" data-toggle="tab"
                                        >Reviews</a>
                                </li>

                                <!-- Gallery tab -->
                                <li class="jShowFriends" alias="{PageContext::$response->rate_entity_alias}"><a
                                        bid="{PageContext::$response->business_id}"
                                        bname="{PageContext::$response->resultSet->business_name}" class="jtab "
                                        id="jViewAssociatedGallery" href="#jViewAssociatedGallery" data-toggle="tab"
                                    >Photos</a>
                                </li>

                            </ul>
                        </div>
                </div>
            </div>
        </div>
        {else}
        <br>
        {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
        {/if}
    </div>
</div>
<div class='clearfix'></div>
<div class='container'>
    <div class='row'>
         {if PageContext::$response->resultSet->business_name neq ''}
        <section class="marg15col">
        <div id="jDivDisplayBusiness" class="tab-content">
            <div class="loader" id="loader">
                <img src="{PageContext::$response->userImagePath}default/loader.gif"/>
            </div>
            <div class='col-sm-4 col-md-5 col-lg-3'>   
           {if count(PageContext::$response->friend_display_list) gt 0}            
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-user" aria-hidden="true"></i> Followers <a  href="#" id="jMoreOrgphotos" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist">
                    {foreach from=PageContext::$response->friend_display_list key=id item=friendslist}
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <a class="friendphotobox_blk" href="{PageContext::$response->baseUrl}timeline/{$friendslist->user_alias}">
                                {if $friendslist->file_path}
                                <div class="friendphotobox" style="background-image: url('{PageContext::$response->userImagePath}{$friendslist->file_path}');">
                                </div>
                                {else}
                                <div class="friendphotobox" style="background-image: url('{PageContext::$response->userImagePath}medium/member_noimg.jpg');">
                                </div>
                                {/if}
                                <h4>{$friendslist->user_firstname|ucfirst} {$friendslist->user_lastname|ucfirst}</h4>
                            </a>
                        </li>
                   {/foreach}
                </ul>
                </div>  
            {/if}
   {if count(PageContext::$response->gallery_display_list) gt 0}
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-file-image-o" aria-hidden="true"></i> Photos <a href="#" id="jMoreOrgGallery" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist portfolio">
                    {foreach from=PageContext::$response->gallery_display_list key=id item=gallerylist}
                     
                     
<!--                    <li class="display_table_cell wid32per">
                        <a class="friendphotobox_blk" href="#">
                            <div class="friendphotobox" style="background-image: url('{PageContext::$response->userImagePath}medium/{$gallerylist->news_feed_image_name}');">-->
                                
<!--                            <ul class="portfolio" class="clearfix">-->
                                    <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4 vert_align_top"><a class="friendphotobox_blk" href="{PageContext::$response->userImagePath}{$gallerylist->organization_announcement_image_path}" title="{$gallerylist->organization_announcement_content}"><img src="{PageContext::$response->userImagePath}{$gallerylist->organization_announcement_image_path}" alt=""></a></li>
<!--                            </ul>-->
                                
<!--                            </div>
                        </a>
                    </li>-->
                    {/foreach}
                </ul>
                <!--<div class="sidead" style="background-image: url('{PageContext::$response->ImagePath}sidead.jpg');"> </div>-->
            </div>  
    {/if}  
            </div>
            <div class="col-sm-8 col-md-7 col-lg-9">
            {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
             {if PageContext::$response->resultSet->business_name neq ''}
            <div class="wid100per">
                <section class="business_profile_desc marg10col marg0top whitebox">
                    <h3>{PageContext::$response->resultSet->business_name}
                        {if PageContext::$response->resultSet->business_status eq 'A'}
                        <div class="business_profile_desc_starring">
                            {php}PageContext::renderRegisteredPostActions('rating');{/php}
                        </div>
                        {/if}
                    </h3>
                    {if PageContext::$response->resultSet->user_firstname neq ''}
                    <p class="owncaption"><span>Owned By</span> : <a
                            href="{PageContext::$response->baseUrl}timeline/{PageContext::$response->resultSet->user_alias}">{PageContext::$response->resultSet->user_firstname}
                            {PageContext::$response->resultSet->user_lastname}</a></p>
                            {/if}
                    <!--<p class="owncaption"><span>Category</span> : {PageContext::$response->resultSet->category_name}</p>-->

                    <div class="business_profile_desc_location">
                        <div class="wid100per"> {if PageContext::$response->resultSet->business_address neq ''}
                            <div class="left">{PageContext::$response->resultSet->business_address}</div>
                            {/if}
                            {if PageContext::$response->sess_user_id eq
                            PageContext::$response->resultSet->business_created_by}
                            <div class="right">
                                <a class="edititem left marg5right"
                                   href="{php} echo PageContext::$response->baseUrl;{/php}edit-page/{PageContext::$response->resultSet->business_id}"><i
                                        class="fa fa-pencil"></i></a>
                                <a class="deleteitem left" onclick="return deleteConfirm();"
                                   href="{php} echo PageContext::$response->baseUrl;{/php}index/delete_organization/{PageContext::$response->resultSet->business_id}"><i
                                        class="fa fa-trash"></i></a>
                            </div>
                            {/if}
                            <div class="clearfix"></div>
                        </div>
                        <p>
                            {PageContext::$response->resultSet->business_description|stripslashes}
                        </p>
                    </div>
                            {if count(PageContext::$response->resultSet) gt 1}
                    {if PageContext::$response->resultSet->business_status eq 'A'}
                    <div class="inquire_ratingblk">
                        <div class="business_profile_desc_action">
                            <input type="hidden" value="{PageContext::$response->sess_user_id}" class="login_status"
                                   name="" id="login_status">
                            {if PageContext::$response->sess_user_id eq
                            PageContext::$response->resultSet->business_created_by}
                            {else}
                            <!--<input type="button" value="INQUIRE" class="yellow_btn" name="" onclick="return checkLoginInquire('B','{PageContext::$response->resultSet->business_id}','Inquire');">-->
<!--                            {if PageContext::$response->sess_user_status neq 'I'}
                            <input type="button" value="POST TESTIMONIAL" class="yellow_btn" name=""
                                   onclick=" return checkLogin('B','{PageContext::$response->resultSet->business_id}','Tstimonial');">
                            {/if}-->
                            {/if}
                        </div>
                    </div>

                    {else}
                    <div class="inquire_ratingblk" style="color:red">
                        Pending approval from administrator
                    </div>
                    {/if}
                    {/if}
                </section>
            </div>
             {/if}
            <!--<div class="col-sm-4 col-md-4 col-lg-4">
                {php} PageContext::renderRegisteredPostActions('googlemap'); {/php}
            </div>-->
             {if PageContext::$response->resultSet->business_name neq ''}
            <div class="wid100per">
                <section class="whitebox">
                    <div class="display_table addresstbl">
                        {if PageContext::$response->resultSet->business_address neq ''}
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">Address</span>
                            <span class="display_table_cell wid70per">{PageContext::$response->resultSet->business_address}</span>
                        </div>
                        {/if}
                        {if PageContext::$response->resultSet->business_city neq ''}
                        <div class="display_table_row">
                            <div class="display_table_cell wid30per hdleft">City</div>
                            <span
                                class="display_table_cell wid70per">{PageContext::$response->resultSet->business_city}</span>
                        </div>
                        {/if}
                        {if PageContext::$response->resultSet->business_state neq ''}
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">State</span>
                            <span class="display_table_cell wid70per">        {if PageContext::$response->resultSet->State }{PageContext::$response->resultSet->State}{else}{if PageContext::$response->resultSet->business_state}{PageContext::$response->resultSet->business_state}{/if}{/if}</span>
                        </div>
                        {/if}
                        {if PageContext::$response->resultSet->business_country neq ''}
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">Country</span>
                            <span
                                class="display_table_cell wid70per">{PageContext::$response->resultSet->Country}</span>
                        </div>
                        {/if}
                        {if PageContext::$response->resultSet->business_zipcode neq ''}
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">Zip</span>
                            <span class="display_table_cell wid70per">{PageContext::$response->resultSet->business_zipcode}</span>
                        </div>
                        {/if}
                        {if PageContext::$response->resultSet->business_phone neq ''}
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">Phone</span>
                            <span class="display_table_cell wid70per">{PageContext::$response->resultSet->business_phone}</span>
                        </div>
                        {/if}
                        {if PageContext::$response->resultSet->business_email neq ''}
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">Email</span>
                            <span class="display_table_cell wid70per"><a
                                    href="mailto:{PageContext::$response->resultSet->business_email}">{PageContext::$response->resultSet->business_email}</a></span>
                        </div>
                        {/if}
                        {if PageContext::$response->resultSet->business_url neq ''}
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">Website</span>
                            <span class="display_table_cell wid70per">
                               <a
                                    href="{PageContext::$response->resultSet->business_url}"
                                    target="_new">{PageContext::$response->resultSet->business_url}</a></span>
                        </div>
                        {/if}
                    </div>
                    <div class="clear"></div>
                </section>
            </div>
{/if}
            <div class="clear"></div>
            </div>

            <div class="listitem loader">
                <img src="{PageContext::$response->userImagePath}default/loader.gif"/> Loading....
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        </section>
         {/if}
    </div>
</div>

