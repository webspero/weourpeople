<?php foreach (PageContext::$response->testimonials AS $id => $lead){ ?>
    <div class="margin_l_1">
        <div class="mediapost" id="<?php echo 'comment_' . $lead->testimonial_id ?>">
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-2">
                    <div class="mediapost_left">
                        <div class="mediapost_pic">
                                <img src="<?php echo PageContext::$response->userImagePath;
                                if ($lead->file_path != '') {
                                    echo "small/" . $lead->file_path;
                                } elseif ($lead->file_path == '') {
                                    echo "small/member_noimg.jpg";
                                } ?>">
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 col-md-7 col-lg-8">
                    <div class="media-body">
                        <div class="<?php echo 'testi_icon testimonial_content_' . $lead->testimonial_id ?>">
                            <span id="jtestimonial_content_<?php echo $lead->testimonial_id; ?>">
                                <?php echo $lead->testimonial_content ?>
                            </span>
                        </div>
                        <div class="clear"></div>
                        <div class="wid100per">
                            <h4 class="media-heading">
                                <a class="yellow_text"
                                   href="<?php echo PageContext::$response->baseUrl . 'timeline/' . $lead->user_alias ?>">
                                    <?php if ($lead->user_firstname == '') {
                                        echo $lead->user_email;
                                    } else {
                                        echo $lead->user_firstname;
                                        echo $lead->user_lastname;
                                    } ?></a>
                            </h4>
                        </div>
<!--                        <div class="wid100per pad5top">
                            <span><?php echo $lead->user_company . ',' . $lead->user_designation ?></span>
                        </div>-->
                    </div>
                </div>
                <div class="col-sm-12 col-md-2 col-lg-2">
                    <div class="pad5p">
                        <div class="datepaosted wid100per">
                            <div class="align-right">
                                <?php echo date('m-d-Y H:i:s', strtotime($lead->tetimonial_date)); ?>
                            </div>
                        </div>
                        <div class="wid100per pad15top">
                            <div class="right">
                                <?php if (PageContext::$response->sess_user_id == $lead->user_id) { ?>
                                    <a class="edititem left marg5right jedit_link_testimonial" href="#"
                                       cid="<?php echo $lead->testimonial_id ?>"
                                       id="<?php echo $lead->testimonial_id ?>"><i
                                            class="fa fa-pencil"></i></a>
                                    <a class="deleteitem left jdeleteTestimonial"
                                       href="javascript:void(0)"
                                        aid="<?php echo $lead->testimonial_id; ?>"><i class="fa fa-trash"></i></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clear"></div>
    </div>
<?php }?>