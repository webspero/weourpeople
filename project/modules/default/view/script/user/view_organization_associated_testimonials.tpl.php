<script>
    var TOTAL_PAGES = <?php echo PageContext::$response->totalPages?>;
    $(document).ready(function () {
        var page = 1;
        var _throttleTimer = null;
        var _throttleDelay = 100;
        $(window).scroll(function () {
            clearTimeout(_throttleTimer);
            _throttleTimer = setTimeout(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                    if (page < TOTAL_PAGES) {
                        page++;
                        $.blockUI({message: ''});
                        var business_id = $("#business_id").val();
                        $.ajax({
                            type: 'Post',
                            url: mainUrl + 'user/view_organization_associated_testimonialsajax/' + page + '/' + business_id,
                            data: 'business_id=' + business_id,
                            success: function (data) {
                                $('#jTestimonials').append(data);
                                $('.listitem').fadeIn(3000);
                                $('.listitem').hide();
                            }
                        })
                    }
                }
            }, _throttleDelay);
        });
        magnificPopupGroupFn();
    });
    
    $(document).ready(function(){
        $('#jPostTestimonial').bind('click',function(){
            var testimonialContent = $('#txtTestimonial').val();
            //alert(testimonialContent);exit;
            var business_id = $("#business_id").val();
            if($.trim(testimonialContent)!=''){
                $.ajax({
                    type : 'Post',
                    url  : mainUrl + 'index/add_testimonial',
                    data : 'business_id=' + business_id+'&testimonialContent=' + testimonialContent,
                    success: function (data) {
                        $(".business_profile_commentrow").html('');
                        $("#txtTestimonial").val('');
                        $('#jTestimonials').prepend(data);
                        $('.listitem').fadeIn(3000);
                        $('.listitem').hide();
                    }
                })  
            }
            else{
                $("#jdivErrorDisplay").html("<font color='red'>Please enter review</font>");
            }
        })
    })
    
    
</script>
<div class="col-sm-4 col-md-5 col-lg-3">   
    <?php if(count(PageContext::$response->friend_display_list) > 0){?>            
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-user" aria-hidden="true"></i> Followers <a  href="#" id="jMoreOrgphotos" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist">
                    <?php foreach(PageContext::$response->friend_display_list as  $key=>$friendslist){
                       // echopre($friendslist);?>
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <a class="friendphotobox_blk" href="<?php echo PageContext::$response->baseUrl;?>timeline/<?php echo $friendslist->user_alias;?>">
                                <?php if($friendslist->file_path) {?>
                                <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?><?php echo $friendslist->file_path;?>');">
                                </div>
                                <?php }else{ ?>
                                 <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>medium/member_noimg.jpg');">
                                </div>
                                <?php } ?>
                                <h4><?php echo ucfirst($friendslist->user_firstname).' '.ucfirst($friendslist->user_lastname);?></h4>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
                </div>  
   <?php } ?>
    <?php if(count(PageContext::$response->gallery_display_list) > 0){   ?>
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-file-image-o" aria-hidden="true"></i> Photos <a href="#" id="jMoreOrgGallery" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist portfolio">
                     <?php foreach(PageContext::$response->gallery_display_list as  $key=>$gallerylist){
                        ?>
                     
<!--                    <li class="display_table_cell wid32per">
                        <a class="friendphotobox_blk" href="#">
                            <div class="friendphotobox" style="background-image: url('{PageContext::$response->userImagePath}medium/{$gallerylist->news_feed_image_name}');">-->
                                
<!--                            <ul class="portfolio" class="clearfix">-->
                                    <li class="pull-left wid32per vert_align_top"><a class="friendphotobox_blk" href="<?php echo PageContext::$response->userImagePath;?><?php echo $gallerylist->organization_announcement_image_path;?>" title=""><img src="<?php echo PageContext::$response->userImagePath;?><?php echo $gallerylist->organization_announcement_image_path;?>" alt=""></a></li>
<!--                            </ul>-->
                                
<!--                            </div>
                        </a>
                    </li>-->
                     <?php } ?> 
                </ul>
                <!--<div class="sidead" style="background-image: url('{PageContext::$response->ImagePath}sidead.jpg');"> </div>-->
            </div>  
    <?php } ?>    
    
</div>
<div class="col-sm-8 col-md-7 col-lg-9">
    <?php if (PageContext::$response->business->status == 'A' && PageContext::$response->sess_user_status != 'I') { ?>
        <section class="whitebox">
            <div class="business_profile_commentBlk">
                <h3>Reviews
                
                    <?php if (PageContext::$response->is_associate) { ?>
                       <span> <a href="javascript:void(0)"
                            id="jaddTestimonial"><i class="fa fa-comments-o" aria-hidden="true"></i> Add Review </a>
                    </span></h3>
                    <div class="jtestimonial_section" style="display: none;">
                    <textarea name="txtTestimonial" id="txtTestimonial" placeholder="Type your reviews.."></textarea>
                    <div id="jdivErrorDisplay"></div>
                    <input type="button" id="jPostTestimonial" value="Post Review" class="btn primary post_btn marg7col">
                    
                </div>
                    
                    <?php } ?>
                
                
                <div id="jTestimonials">
                
                <?php if (empty(PageContext::$response->testimonials)){ ?>
                    <div class="business_profile_commentrow">
                        <font color="grey" class="fontsmall">No reviews added.</font>
                    </div>
                <?php } else { ?>
                
                    <?php foreach (PageContext::$response->testimonials AS $id => $lead) { ?>
                        <div class="margin_l_1 ">
                            <div class="mediapost" id="<?php echo 'comment_' . $lead->testimonial_id ?>">
                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-2">
                                        <div class="mediapost_left">
                                <div class="mediapost_pic">
                                        <img src="<?php echo PageContext::$response->userImagePath;
                                        if ($lead->file_path != '') {
                                            echo "small/" . $lead->file_path;
                                        } elseif ($lead->file_path == '') {
                                            echo "small/member_noimg.jpg";
                                        } ?>">
                                </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 col-md-7 col-lg-8">
                                        <div class="media-body">
                                            <div class="<?php echo 'testi_icon testimonial_content_' . $lead->testimonial_id ?>">
                                                <span id="jtestimonial_content_<?php echo $lead->testimonial_id; ?>">
                                                    <?php echo $lead->testimonial_content ?>
                                                </span>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="wid100per">
                                                <h4 class="media-heading">
                                                    <a class="yellow_text"
                                                       href="<?php echo PageContext::$response->baseUrl . 'timeline/' . $lead->user_alias ?>">
                                                        <?php if ($lead->user_firstname == '') {
                                                            echo $lead->user_email;
                                                        } else {
                                                            echo $lead->user_firstname.' ';
                                                            echo $lead->user_lastname;
                                                        } ?></a>
                                                </h4>
                                            </div>
<!--                                            <div class="wid100per pad5top">
                                                <span><?php echo $lead->user_company . ',' . $lead->user_designation ?></span>
                                            </div>-->
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2 col-lg-2">
                                        <div class="pad5p">
                                            <div class="datepaosted wid100per">
                                                <div class="align-right">
                                                    <?php echo date('m-d-Y H:i:s', strtotime($lead->tetimonial_date)); ?>
                                                </div>
                                            </div>
                                            <div class="wid100per pad15top">
                                                <div class="right">
                                                    <?php if (PageContext::$response->sess_user_id == $lead->user_id) { ?>
                                                        <a class="edititem left marg5right jedit_link_testimonial"
                                                           href="#"
                                                           cid="<?php echo $lead->testimonial_id ?>"
                                                           id="<?php echo $lead->testimonial_id ?>"><i
                                                                class="fa fa-pencil"></i>
                                                        </a>
                                                        <a class="deleteitem left jdeleteTestimonial"
                                                           href="javascript:void(0)"
                                                            aid = "<?php echo $lead->testimonial_id ?>"><i class="fa fa-trash"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="clear"></div>
                    <?php } ?>
                    <?php } ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </section>
    <?php } ?>
    <!-- {else} business status is inactive || user status is inactive  -->
</div>