<div id="fb-root1"></div>
<script>
    
$(function(){
  $("#profile_image").popImg();
  $(".image_zoom").popImg();
});
$('#share_button').click(function(e){
e.preventDefault();
FB.ui(
{
method: 'feed',
name: 'This is the content of the "name" field.',
link: ' https://www.iscriptsdemo.com/bbf/',
picture: 'http://www.hyperarts.com/external-xfbml/share-image.gif',
caption: 'This is the content of the "caption" field.',
description: 'This is the content of the "description" field, below the caption.',
message: ''
},
function(response) {
     if (response && response.post_id) {
     //  alert('Post was published.');
     } else {
      // alert('Post was not published.');
     }
   }
);
});

$('a.twitter-timeline').click(function(e){

  //We tell our browser not to follow that link
  e.preventDefault();

  //We get the URL of the link
  var loc = $(this).attr('href');

  //We get the title of the link
  var title  = encodeURIComponent($(this).attr('title'));

  //We trigger a new window with the Twitter dialog, in the middle of the page
  window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
 // alert('Post was published.');
});
</script>
<?php 
$i=1;
foreach(PageContext::$response->feeds AS $feeds){ 
    $i++;?>
<div class="mediapost profile-bnr">
    <div class="mediapost_head">
        <h4>
            <a href="<?php echo PageContext::$response->baseUrl?>community/<?php echo $feeds->community_alias;?>"><?php echo $feeds->community_name;?></a>
             <?php if($feeds->community_type == 'PRIVATE'){ ?>
                <div class='pull-right'>
                    <a title='private' class="normal_txt"><img src="<?php echo PageContext::$response->ImagePath;?>/private-icon.png"> Private</a>
                </div>
             <?php } ?>
        </h4>
    </div>
    <div class="mediapost_left pull-left">
        <span class="mediapost_pic">
            <img src="<?php echo PageContext::$response->userImagePath ?>medium/<?php echo $feeds->file_path;?>">
        </span>
    </div> 
    <div class="media-body">
        <div class="col-md-8">
        <h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl?>community/<?php echo $feeds->community_alias;?>#announcement" ><?php echo $feeds->community_announcement_title;?></a></h4>
        <p><?php echo $feeds->community_announcement_content?></p>
        </div>
		
		
        <div class="col-md-4 new-text text-right">
            <?php if($feeds->days > 0){
                echo $feeds->days."days ago"; 
            }else{ 
                "Today";
            } ?>
        </div>
		
        </div> 
        <!--<div class="commentimg_box">-->
            <?php if($feeds->file_path != '') {?>
				<div class="profile-bnr">
	              <img class="image_zoom img-responsive center" src="<?php echo PageContext::$response->userImagePath;?><?php echo $feeds->file_path;?>">
				</div>
            <?php } ?>

    <div class="mediapost_user-side  no-top-mrg">
        <!--<div data-url="http://localhost/bbf/announcement-detail/" data-desc="You can share data" data-layout="button_count" data-title="Shared on facebook" data-href="http://localhost/bbf/announcement-detail/" id="share_button_6" class="fb-share-button fb_iframe_widget" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=1015953898468281&amp;container_width=496&amp;href=http%3A%2F%2Flocalhost%2Fbbf%2Fannouncement-detail%2F&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey"><span style="vertical-align: bottom; width: 88px; height: 20px;"><iframe height="1000px" frameborder="0" width="1000px" name="f2edef3ea149996" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:share_button Facebook Social Plugin" style="border: medium none; visibility: visible; width: 88px; height: 20px;" src="http://www.facebook.com/v2.5/plugins/share_button.php?app_id=1015953898468281&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D42%23cb%3Df3d802f900f3978%26domain%3Dlocalhost%26origin%3Dhttp%253A%252F%252Flocalhost%252Ff1f173ed2302a06%26relation%3Dparent.parent&amp;container_width=496&amp;href=http%3A%2F%2Flocalhost%2Fbbf%2Fannouncement-detail%2F&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey" class=""></iframe></span></div>-->
         <div class="fb-share-button" id="share_button_<?php echo $feeds->community_announcement_id;?>" data-href="<?php PageContext::$response->baseUrl?>announcement-detail/<?php echo $feeds->community_announcement_alias;?>" data-title ="Shared on facebook" data-layout="button_count" data-desc="You can share data" data-url="<?php echo PageContext::$response->baseUrl;?>announcement-detail/<?php echo $feeds->community_announcement_alias;?>"></div>
         <a href="<?php echo PageContext::$response->baseUrl;?>announcement-detail/<?php echo $feeds->community_announcement_alias;?>" title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>
        <span class="mediapost_links" id="jcountlike_<?php echo $feeds->community_announcement_id?>"><?php if($feeds->community_announcement_num_likes > 0){ echo $feeds->community_announcement_num_likes; ?>Likes</span> <?php }?>
        <span class="mediapost_links jCommentButton " id="jcountcomment_<?php echo $feeds->community_announcement_id?>" aid="<?php echo $feeds->community_announcement_id?>"><?php if($feeds->community_announcement_num_comments > 0) echo $feeds->community_announcement_num_comments; ?> Comments</span><?php ?>
         <span  class="mediapost_links" id="jcountshare_<?php echo $feeds->community_announcement_id?>"><?php if ($feeds->community_announcement_num_shares > 0)echo $feeds->community_announcement_num_shares;?> Shares</span>   
     </div>

<!--    <table cellpadding="10" cellspacing="5">
        <tr>
            <td><a href="#" class="jLikeFeed <?php if($feedlist['LIKE_ID']>0){ ?> liked <?php }?>" id="jlike_<?php echo $feeds->community_announcement_id; ?>" aid="<?php echo $feeds->community_announcement_id; ?>" cid="<?php echo $feeds->community_id;?>"> <?php if ($feeds->LIKE_ID>0){?><span id="jlikedisplay_<?php echo $feeds->community_announcement_id ?>" class="liked"><?php }else{ ?><span id="jlikedisplay_<?php echo $feeds->community_announcement_id; ?>"><?php }?>Like</span></a>&nbsp;&nbsp;</td>
            <td>Comment&nbsp;&nbsp;</td>
            <td><a href="#" class="jShareFeed" id="jshare_<?php echo $feeds->community_announcement_id ?>" aid="<?php echo $feeds->community_announcement_id ?>" cid="<?php echo $feeds->community_id;?>">Share</a>&nbsp;&nbsp;</td>
            <td><a href="#" class="jShowFeedReferralOptions referral_btn" aid="<?php echo $feeds->community_announcement_id ?>" bid="<?php echo $feeds->community_id?>">Referral offer&nbsp;&nbsp;</a></td>
        </tr>
    </table>-->
    <div class="mediapost_user-side-top clear">
        <div class="display_table_cell pad10_right">
            <a href="#" class="colorgrey_text jLikeFeed <?php if($feeds->LIKE_ID>0){ ?> liked <?php }?>" id="jlike_<?php echo $feeds->community_announcement_id; ?>" aid="<?php echo $feeds->community_announcement_id; ?>" cid="<?php echo $feeds->community_id;?>"> 
           

                <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                    <span id="jlikedisplay_<?php echo $feeds->community_announcement_id;?>">Like </span></a>

            </div>
             <div class="display_table_cell pad10_right">
            <i class="fa fa-comments-o icn-fnt-size"></i>
            <a href="#" id="jCommentButton_<?php echo $feeds->community_announcement_id;?>" aid="<?php echo $feeds->community_announcement_id;?>" class="jCommentButton colorgrey_text">Comment</a>
            </div>
              <div class="display_table_cell pad10_right">
            <i class="fa fa-share-alt icn-fnt-size"></i>
            <a href="#" class="jShareFeed colorgrey_text" id="jshare_<?php echo $feeds->community_announcement_id;?>" aid="<?php echo $feeds->community_announcement_id?>" cid="<?php echo $feeds->community_id?>">Share</a>
            </div>
              <div class="display_table_cell pad10_right">
                  <i class="farefer"></i>
            <a href="#" class="jShowFeedReferralOptions referral_btn-new colorgrey_text" aid="<?php echo $feeds->community_announcement_id?>" bid="<?php echo $feeds->community_id?>">Referral offer</a>
            </div>
            <div class="display_table_cell pad10_right">
                <!--<i class="farefer"></i>-->
                <a href="#" class="view_other_bizcom" id="suggestheaddiv_suggest_<?php echo $feeds->community_id;?>_<?php echo $feeds->community_business_id;?>_<?php echo $i;?>_<?php echo $i;?>">More BizComs</a>
            </div>
        <div class="display_table_cell pad10_right">
             <div class="loading" id="loading_suggestheaddiv_suggest_<?php echo $feeds->community_id;?>_<?php echo $feeds->community_business_id;?>_<?php echo $i;?>_<?php echo $i;?>" style="display: none;">
                                        <img src="<?php echo PageContext::$response->userImagePath;?>default/loader.gif" />
             </div>
            </div> 
    	</div>
    
    
    <div id="jRefDiv_<?php echo $feeds->community_id ?>_<?php echo $feeds->community_announcement_id ?>" class="fleft"></div>
    <div class="jReferralOptionsClass referraltype_blk" id="jDivdisplayReferralOptions_<?php echo $feeds->community_id ?>_<?php echo $feeds->community_announcement_id ?>" style="display:none;">
        <h4>Please choose the referral type</h4>
        <ul>
            <li><input type="radio" name="RefferalType_<?php echo $feeds->community_id ?>" value="1" > Free Referral</li>
            <li><input type="radio" name="RefferalType_<?php echo $feeds->community_id ?>" value="2" > Financial Incentive Based Referral</li>
            <li><input type="radio" name="RefferalType_<?php echo $feeds->community_id ?>"  value="3"> Charitable Donation Based Referral</li>
            <li><input type="radio" name="RefferalType_<?php echo $feeds->community_id ?>" value="4" > Voucher or Product Based Referral</li>
            <li><input type="button" class="jClickFeedReferralOffer sendref_offr_btn" aid="<?php echo $feeds->community_announcement_id ?>"  bid="<?php echo $feeds->community_id ?>" uid="<?php echo $feeds->community_created_by ?>" value="Send Refferal Offer"></li>
        </ul>
    </div>
    
    <div class="mediapost_user-side-top clear jCommentDisplayDiv" id="jCommentBoxDisplayDiv_<?php echo $feeds->community_announcement_id;?>" style="display:none">
        <textarea placeholder="Write your comment here..." class="marg7col form-control new-height" id="watermark_<?php echo $feeds->community_announcement_id;?>" class="watermark" name="watermark" data-emojiable="true" ></textarea>  
        <a class="upload_cambtn" id="<?php echo $feedlist['community_announcement_id'];?>" onclick="uploadimage(<?php echo $feedlist['community_announcement_id'];?>);">
                                             <label for="file"><i class="fa fa-camera"></i></label></a>
                                                  <input type="file" id="file" class="image_file" style="cursor: pointer;  display: none"/></a>
       
        <a id="shareButton" cid="<?php echo $feeds->community_id?>" aid="<?php echo $feeds->community_announcement_id?>" style="float:left"   class="btn yellow_btn fontupper pull-right jPostCommentButton"> Post</a>
    </div>
    <!--<table>-->
<!--        <tr>
            <td colspan="3"><textarea placeholder="Write your comment here..." class="input" id="watermark_<?php echo $feeds->community_announcement_id;?>" class="watermark" name="watermark" style="height:30px" cols="60" ></textarea></td>
            <td><a id="shareButton" cid="<?php echo $feeds->community_id?>" aid="<?php echo $feeds->community_announcement_id?>" style="float:left" class="jshareButton"> Post</a></td>
        </tr>-->
        
         <div class="mediapost_user-side-top clear jCommentDisplayDiv" id="jCommentDisplayDiv_<?php echo $feeds->community_announcement_id?>" style="display:none">
            <div id="posting_<?php echo $feeds->community_announcement_id?>">
                    <?php 
                        foreach (PageContext::$response->Comments[$feeds->community_announcement_id] AS $comments){?>
                     <div class="col-md-12 btm-mrg" id="jdivComment_{$comments->announcement_comment_id}">
                         <div class="row">
                             <?php if($comments->file_path !=''){ ?>
                             <div class="col-md-1">
                                 <img width="30px" height="30px" class="ryt-mrg" src="<?php echo PageContext::$response->userImagePath ?><?php echo $comments->file_path ?>">
                             </div>
                             <?php } ?>
                             <div class="col-md-11">
                                 <span class="name"><?php echo $comments->user_name?> </span><?php echo $comments->comment_content ?>
                                 <br>
                                 <span class="new-text">
                                    <?php echo $comments->comment_date ?>  <a href="#" class="jFeedCommentDelete" aid="<?php echo $feeds->community_announcement_id ?>" id="<?php echo $comments->announcement_comment_id ?>">Delete</a>
                                 </span>
                             </div>
                         </div> 
                     </div>
                     <?php }?>
                 </div>
             </div>
<!--        <tr>
            <td colspan="4">
                <div id="posting_<?php echo $feeds->community_announcement_id?>" align="center">
                    <table>
                        <?php 
                        foreach (PageContext::$response->Comments[$feeds->community_announcement_id] AS $comments){?>

                        <tr>
                            <td><img width="50px" height="50px" src="<?php echo PageContext::$response->userImagePath ?>medium/<?php echo $comments->user_image_name ?>"></td>
                            <td><b><?php echo $comments->user_name?></b>&nbsp;<?php echo $comments->comment_content ?></td>
                        </tr>

                        <?php }?>
                    </table>
                </div>
            </td>
        </tr>
    </table>-->
    
 <div class="jslideSuggestion" id = "suggestheaddiv_suggest_<?php echo $feeds->community_id;?>_<?php echo $feeds->community_business_id;?>_<?php echo $i;?>_<?php echo $i;?>_slide"></div></div>

    <div class="clearfix"></div>
</div>
<?php 
}?>
