<div class="container">
<section class="inbox_section whitebox">
    <div class="row"> 
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h3>Group<span class="round-search"><span class="count_bg">{PageContext::$response->totalrecords}</span></span> {if PageContext::$response->sess_user_id neq 0} <span class="addbusiness"><a href="{php} echo PageContext::$response->baseUrl;{/php}add-group">Add Group</a></span> {/if}</h3>
        </div>
        <section class="searchbar">

            <div class="col-sm-12 col-md-12 col-lg-12">

                <div class="businesslist_search_blk">
                    <div class="row"> 
                        <div class="col-sm-6 col-md-8 col-lg-8">
                            <input type="hidden" value="{PageContext::$response->sess_user_id}" class="login_status" name="" id="login_status">
                            Sort by : A-Z <a href="{php} echo PageContext::$response->baseUrl; {/php}org-groups/{php} echo PageContext::$response->leads[0]->community_business_id; {/php}?sort=community_name&sortby={php} echo PageContext::$response->order; {/php}&searchtext={php} echo PageContext::$response->searchtext; {/php}&searchtype={php} echo PageContext::$response->searchtype; {/php}&page={php} echo PageContext::$response->page; {/php}">{PageContext::$response->community_name_sortorder}</a>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            {php}PageContext::renderRegisteredPostActions('searchbox');{/php}
                        </div>
                    </div>
                </div>
                {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
            </div>
        </section>
    </div>
    <div class="clearfix"></div>
     {if PageContext::$response->totalrecords > 0}
            {assign var="i" value=PageContext::$response->slno}
    <div class="row masonry"> 
        <div class="col-sm-12 col-md-12 col-lg-12">
            
        <section class="bizcomlisting subscribe_listing">
           
            {foreach from=PageContext::$response->leads key=id item=lead}
            <div class="item">
                
                <div class="mediapost well">
                    <div class="wid100per">
                        <span class="mediapost_pic">
                            <a href="{php} echo PageContext::$response->baseUrl;{/php}group/{$lead->community_alias}"><img class="business_profile_desc_colside_pic" alt="{$lead->file_orig_name}" src="{php} echo PageContext::$response->userImagePath;{/php}{if $lead->file_path neq ''}medium/{$lead->file_path}{elseif $lead->file_path eq ''}default/no_preview_med.jpg{/if}"> </a>
                        </span>
                    </div>

                    <div class="wid100per pad5p  media-body">
                        <h4 class="media-heading"><span><a href="{php} echo PageContext::$response->baseUrl;{/php}group/{$lead->community_alias}">{$lead->community_name}({$lead->business_name})</a></span></h4>
                        {if $lead->community_status =='I'}   
                        <span class="status clserror">Pending Approval</span> 
                        {else}
                        <div class="display_table">
                            <div class="display_table_cell wid60per">
                                <div id="starring">
                                    {if PageContext::$response->sess_user_id neq $lead->community_created_by}
                                    <div class="rateit" id="rateit_{$lead->ratetype_id}" data-rateit-value="{$lead->ratevalue}" data-rateit-ispreset="true" data-rateit-id="{$lead->rateuser_id}" data-rateit-type="{$lead->ratetype}" data-rateit-typeid="{$lead->ratetype_id}" data-rateit-flag="{$lead->rateflag}" data-rateit-step=1 data-rateit-resetable="false">
                                    </div>
                                    {/if}
                                    <div class="right">
                                        {if PageContext::$response->sess_user_id eq $lead->community_created_by} 
                                        <a href="{php} echo PageContext::$response->baseUrl;{/php}edit-group/{$lead->community_id}" class="edititem left marg5right"><i class="fa fa-pencil"></i></a>
                                        <a href="{php} echo PageContext::$response->baseUrl;{/php}index/delete_community/{$lead->community_id}" class="deleteitem left"><i class="fa fa-trash"></i></a> 
                                        {/if}
                                    </div>
                                </div>
                            </div>

                        </div>
                        {/if}
                        <p>
                            {$lead->community_description|substr:0:200}....
                        </p>
                    </div>
                    <div class="clearfix"></div>
                
                </div>
            </div>
            {assign var="i" value=$i+1}
            {/foreach}  
            
            

        </section>
        </div>

    </div>
     {else}
    <!--<div class="clear"></div>-->
    
   
    <div class="business_profilelist_detailsarea" style='border: none;'>{if PageContext::$response->searchtype eq ''}No groups added/joined yet.{else}No matches found.{/if} Click to <a href="{php} echo PageContext::$response->baseUrl;{/php}add-group">create a group</a>.</div>
    {/if} 
    {if PageContext::$response->totalrecords gt PageContext::$response->itemperpage}
    <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">{PageContext::$response->pagination} </div>
    </div>
    {/if}

    </div>

    </section>



<script type ="text/javascript">
    $('#starring .rateit').bind('rated', function (e) {
        var ri = $(this);
        var rateField = $(this).attr("id");
        var status = true;
        var ratetype = ri.rateit('type');
        var ratetypeid = ri.rateit('typeid');     
        var rateflag = ri.rateit('flag');
        //if the use pressed reset, it will get value: 0 (to be compatible with the HTML range control), we could check if e.type == 'reset', and then set the value to  null .
        var ratevalue = ri.rateit('value');
        // alert("hi "+ratevalue);
        var userid = ri.rateit('id'); // if the product id was in some hidden field: ri.closest('li').find('input[name="productid"]').val()
        if(rateflag == 1 && userid <= 0){ //alert("2");
            status = checkLogin2(ratetype,ratetypeid,'rate',rateField);	    	
        }	 
        if(status != false){
            // alert("here");
            checkRated1(rateField);	    
        }
	 
    });

</script>
</div>