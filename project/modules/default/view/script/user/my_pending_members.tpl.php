<script type="text/javascript">
    $(document).ready(function(){
       $(".notifi_no_request").html(0);
    });
</script>
<div class="container">
    <section class="whitebox marg40col">
    <div class="row"> 
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h3>Pending approvals to join your Group<span class="round-search">{PageContext::$response->totalrecords}</span></h3>
            {PageContext::renderRegisteredPostActions('messagebox')}
              <div class="businesslist_search_blk">
                <div class="row"> 
                        <div class="col-sm-6 col-md-4 col-lg-4 pull-right">
                            {php}PageContext::renderRegisteredPostActions('searchbox');{/php}
                        </div>
                </div>
              </div>
        </div>
        <div class="clearfix"></div>
        <section class="bizcomlisting listin">
            {if PageContext::$response->usersList|@sizeof eq 0}
            <div class="col-sm-12 col-md-12 col-lg-12 ">
            <h3 class="new-titile">
            No Pending Requests Found
            </h3>
            </div>
            {else}
            {foreach from=PageContext::$response->usersList key=id item=user}
            <div class="col-sm-4 col-md-4 col-lg-4">
            <div class="mediapost pendappr display_table wid100per">
                <div class="picpost_left display_table_cell wid30per">
                    <span class="picpost_left_pic">
                        <a href="{PageContext::$response->baseUrl}group/{$user->community_alias}"><img src="{if $user->file_path eq ''}{PageContext::$response->userImagePath}small/noimage.jpg{else}{PageContext::$response->userImagePath}{$user->file_path}{/if}"></a>
                    </span>
                </div>
                <div class="media-body frndslist_media display_table_cell wid70per vert_align_middle">
                    <h4 class="media-heading"><a href="{PageContext::$response->baseUrl}timeline/{$user->user_alias}">{$user->user_firstname} {$user->user_lastname}</a></h4>
                    <p>membership request is pending for </p>
                    <p><a class="req_pend_community" href="{PageContext::$response->baseUrl}group/{$user->community_alias}">{$user->community_name}</a> </p>
                    <p>
                        {if PageContext::$response->sess_user_id > 0}
                        <a uid="{$user->user_id}" href="{PageContext::$response->baseUrl}private-membership-request/{$user->user_id}/{$user->community_id}/accept"><span class="accept"><b>+</b> Accept</span></a>
                        <a uid="{$user->user_id}" href="{PageContext::$response->baseUrl}private-membership-request/{$user->user_id}/{$user->community_id}/decline"><span class="reject"><i class="fa fa-times"></i> Reject</span></a>
                        {/if}
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
            </div>
            {/foreach}
             
            {/if}
            <div class="clear"></div>
            <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="listitem">
                <img src="{PageContext::$response->userImagePath}default/loader.gif" /> Loading....
            </div>
            </div>
        </section>
        {if PageContext::$response->totalrecords gt PageContext::$response->itemperpage}
        <div>{PageContext::$response->pagination} </div>
        {/if}
    </div>
    </section>
</div>








<!--
<div class="content_left">
<div class="content_left pad_cont_right findfriend">
    <h3>Pending BizCom Members</h3>
    {PageContext::renderRegisteredPostActions('messagebox')}
    <div class="find_friends_list_outer">
        <div class="find_friends_list_blk">
            {if PageContext::$response->usersList|@sizeof eq 0}
            No Pending Requests Found
            {else}
            {foreach from=PageContext::$response->usersList key=id item=user}
            <div class="row">
                <div class="colside1">
                    <div class="colside1_pic">
                        <a href="{PageContext::$response->baseUrl}community/{$user->community_alias}"><img src="{if $user->file_path eq ''}{PageContext::$response->userImagePath}small/noimage.jpg{else}{PageContext::$response->userImagePath}small/{$user->file_path}{/if}"></a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="colmiddle">
                    <h6><a href="{PageContext::$response->baseUrl}profile/{$user->user_alias}">{$user->user_firstname} {$user->user_lastname}</a> </h6>membership request is pending for  <h6><a href="{PageContext::$response->baseUrl}community/{$user->community_alias}">{$user->community_name}</a> </h6>
                </div>
                <div class="colside3">
                    {if PageContext::$response->sess_user_id > 0}
                    <a class="friends_btn fright" uid="{$user->user_id}" href="{PageContext::$response->baseUrl}bizcom-membership-request/{$user->user_id}/{$user->community_id}/decline" style="float:left; ">
                        <span class="declinefriend_icon"></span>
                        Decline
                    </a>
                    <a class="friends_btn fright" uid="{$user->user_id}" href="{PageContext::$response->baseUrl}bizcom-membership-request/{$user->user_id}/{$user->community_id}/accept" style="float:left; ">
                        <span class="tik_icon"></span>
                        Accept
                    </a>
                    {/if}
                </div>
            </div>
            {/foreach}
            {/if}
        </div>
        <div class="clear"></div>
        <div class="listitem">
            <img src="{PageContext::$response->userImagePath}default/loader.gif" /> Loading....
        </div>
    </div>
    <div class="clear"></div>
</div>
</div>
-->
