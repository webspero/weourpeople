<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
        <div id="o-wrapper" class="o-wrapper">
        <!---Menu Link-->
        {if PageContext::$response->sess_user_id >0}
            <div class="main">
            <div class="header-main">
                 <main class="o-content">
                    <div class="o-container">

                      <div class="c-buttons">
                        <button id="c-button--slide-right" class="button-nav-toggle"><span class="icon">&#9776;</span></button>
                      </div>

                      <div id="github-icons"></div>

                    </div><!-- /o-container -->
                  </main><!-- /o-content -->    
            </div>
            </div>  
        {/if}
        <!---EOF Menu Link-->
        </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
            <!---Menu Link----------------- -->
            <div class="main">
            <div class="header-main">
                <div class="{PageContext::$response->messagebox['msgClass']}">{PageContext::$response->messagebox['msg']}</div>


            </div>
            </div>  
            <h3>My Introduction Invitation</h3>
            <div class="clear"></div>
        </div>
    <div class="col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
    <div class="introdiv refferal_request_outer" id="tab-InitiateIntroduction">
        <div class="content_login">
            <div class="pad5_10p">
                <h3>
                 {if PageContext::$response->campaignDetails->refferalcampaign_payment_mode eq 'P' and strtotime(PageContext::$response->campaignDetails->refferalcampaign_expiry_date) < strtotime(Date('Y-m-d'))}
                      <span class="expiry"><img src="{PageContext::$response->userImagePath}default/exp_small_logo.jpg" /></span>
                {/if}
                </h3>
                <div class="clear"></div>
                <!--{PageContext::renderRegisteredPostActions('messagebox')}-->
                <div class="clear"></div>
                <em>This is a referral Request from a friend, colleague or somebody that you have done business with, they have been asked to refer you to others that they may know. Your details have not been shared with the originators of this request and will not be shared unless you agree to this request.</em>
            </div>
            <div class="wid100per">
                <div id="jDisplayCommonIntroDiv">
					<!-- <fieldset>
							<legend> <h4><label for="intro_title">Title</label></h4></legend>
						<div>
							<label for="intro_title">{PageContext::$response->campaignDetails->refferalcampaign_title}</label>
						</div>
					</fieldset>-->
                    <div class="intro_list display_table wid100per">
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <h4><label for="intro_title">Title</label></h4>
                        </div>
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <label for="intro_title">{PageContext::$response->campaignDetails->refferalcampaign_title}</label>
                        </div>
                    </div> 
                    <div class="intro_list display_table wid100per">
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <h4><label for="intro_title">Owned By</label></h4>
                        </div>
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <label for="intro_title">{PageContext::$response->campaignDetails->refferalcampaign_creator_name}</label>
                        </div>
                    </div> 
                    <div class="intro_list gray display_table wid100per">
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <h4><label for="intro_title">Sender</label></h4>
                        </div>
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <span class="mediapost_pic">
                                <a target="_blank" href="{PageContext::$response->baseUrl}profile/{PageContext::$response->useralias}"><img src="{if PageContext::$response->userdetails->file_path eq ''}{PageContext::$response->userImagePath}small/member_noimg.jpg{else}{PageContext::$response->userImagePath}small/{PageContext::$response->userdetails->file_path}{/if}"></a>
                            </span>
                            <a target="_blank"href="{PageContext::$response->baseUrl}profile/{PageContext::$response->useralias}"><label for="intro_title">{PageContext::$response->bizComOwner}</label></a>
                        </div>
                    </div> 
                    <div class="intro_list display_table wid100per">
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <h4><label for="intro_title">Bizcom</label></h4>
                        </div>
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <!-- <div class="colside1_pic11">
                                <img src="{if PageContext::$response->userdetails->user_image_name eq ''}{PageContext::$response->userImagePath}small/member_noimg.jpg{else}{PageContext::$response->userImagePath}small/{PageContext::$response->userdetails->user_image_name}{/if}"></a>
                            </div>-->
                            <a target="_blank" href="{PageContext::$response->baseUrl}community/{PageContext::$response->campaignDetails->introduction_community_alias}"><label for="intro_title">{PageContext::$response->bizcom}</label></a>
                        </div>
                    </div> 
                    {if PageContext::$response->campaignDetails->refferalcampaign_paidrefferalmode eq 2 && PageContext::$response->campaignDetails->charity_id >0}<!-- Charity Campaign-->
                    <div class="intro_list gray display_table wid100per">
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <h4><label for="intro_title">Charity Organization</label></h4>
                        </div>
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <span class="mediapost_pic">
                                <a target="_blank" href=""><img src="{PageContext::$response->campaignDetails->charity_logo}"</a>
                            </span>
                            <a target="_blank" href="{PageContext::$response->campaignDetails->charity_url}"><label for="intro_title">{PageContext::$response->campaignDetails->charity_name}</label></a>
                        </div>
                    </div> 
                    {/if}
                    {if PageContext::$response->campaignDetails->refferalcampaign_mail_specified_contact neq ''}
                    <div class="intro_list display_table wid100per">
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <h4><label for="intro_title">Specified contacts</label></h4>
                        </div>
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <label for="intro_title">{PageContext::$response->campaignDetails->refferalcampaign_mail_specified_contact}</label>
                        </div>
                    </div>
                    {/if}
                   
                    {if PageContext::$response->campaignDetails->refferalcampaign_payment_mode eq 'P'}
                    <div class="wid100per">
                    <div class="intro_list gray display_table wid100per pad5_10p">
                            <h4><label for="intro_title">
                                    {if PageContext::$response->campaignDetails->refferalcampaign_paidrefferalmode eq 1}
                                      Incentives Summary
                                    {else if PageContext::$response->campaignDetails->refferalcampaign_paidrefferalmode eq 2}
                                     Charity Donation Summary
                                     {else if PageContext::$response->campaignDetails->refferalcampaign_paidrefferalmode eq 3}
                                         Voucher Details
                                      {/if}
                             </label></h4>
                        </div>
                                        {if PageContext::$response->campaignDetails->refferalcampaign_max_pif neq 0}
						
                            <div class="intro_list display_table wid100per">
								<div class="display_table_cell wid50per vlaign_middle pad5_10p"><label for="intro_title">Maximum Pass It Forwards : </label></div>
								<div class="display_table_cell wid50per vlaign_middle pad5_10p">{PageContext::$response->campaignDetails->refferalcampaign_max_pif}</div>
							</div>
							{/if}
							{if PageContext::$response->campaignDetails->refferalcampaign_pif_buget neq 0}
							<div class="intro_list display_table wid100per">
                                                              {if PageContext::$response->campaignDetails->refferalcampaign_paidrefferalmode eq 3 }
                                                              <div class="display_table_cell wid50per vlaign_middle pad5_10p"><label for="intro_title">Discount Amount For A Pass It Forward :</label></div>
                                                               {else}
								<div class="display_table_cell wid50per vlaign_middle pad5_10p"><label for="intro_title">Incentives for a Pass It Forward :</label></div>
                                                               {/if}
								<div class="display_table_cell wid50per vlaign_middle pad5_10p">&pound{PageContext::$response->campaignDetails->refferalcampaign_pif_buget}</div>
							</div>
							{/if}
							{if PageContext::$response->campaignDetails->refferalcampaign_max_joins neq 0}
							
							<div class="intro_list display_table wid100per">
                                                            {if PageContext::$response->campaignDetails->refferalcampaign_paidrefferalmode eq 3 }
                                                                <div class="display_table_cell wid50per vlaign_middle pad5_10p"><label for="intro_title">Discount Amount For A Accept & Join :</label></div>
                                                            {else}
								<div class="display_table_cell wid50per vlaign_middle pad5_10p"><label for="intro_title">Incentives for a Accept and Join :</label></div>
                                                            {/if}
								<div class="display_table_cell wid50per vlaign_middle pad5_10p">&pound{PageContext::$response->campaignDetails->refferalcampaign_max_joins}</div>
							</div>
							{/if}
                                                        {if PageContext::$response->campaignDetails->refferalcampaign_paidrefferalmode eq 3 }
                                                            <div class="intro_list display_table wid100per">
								<div class="display_table_cell wid50per vlaign_middle pad5_10p"><label for="intro_title">Discount Amount For New Joinees :</label></div>
								<div class="display_table_cell wid50per vlaign_middle pad5_10p">&pound{PageContext::$response->campaignDetails->referralcampaign_new_joinee_incentive}</div>
                                                            </div>
                                                        {/if}
							{if PageContext::$response->campaignDetails->refferalcampaign_business_bonus neq 0}
							
<!--							<div class="intro_list">
								<div class="split_L"><label for="intro_title">Business bonus </label></div>
								<div class="split_R">:&pound{PageContext::$response->campaignDetails->refferalcampaign_business_bonus}</div>
							</div>-->
							{/if}
					</div>
                    {/if}
                    
                    {if PageContext::$response->campaignDetails->refferalcampaign_payment_mode eq 'P' and PageContext::$response->campaignDetails->refferalcampaign_expiry_date neq '1970-01-01'}
                    <div class="intro_list gray display_table wid100per">
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <h4><label for="intro_title">Campaign Expiry</label></h4>
                        </div>
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <label for="intro_title">{date('d-m-Y',strtotime(PageContext::$response->campaignDetails->refferalcampaign_expiry_date))}</label>
                        </div>
                    </div>
                    {/if}
                    
                    <div class="intro_list gray display_table wid100per">
                        <div class="display_table_cell wid100per vlaign_middle pad5_10p">
                            <h4><label class="hding" for="intro_title">Message</label></h4>
                        </div>
                    </div>
                    <div class="intro_list display_table wid100per">
                        <div class="display_table_cell wid100per vlaign_middle pad5_10p">
                            <label for="intro_title">
                                Hi {PageContext::$response->memberName},<br>
                                You have recieved {PageContext::$response->campaignType} referral request from {PageContext::$response->bizComOwner} who would like to introduce you to {PageContext::$response->bizcom}
                            </label>
                        </div>
                    </div>
                     {if PageContext::$response->campaignDetails->refferalcampaign_message neq ''}
                    <div class="intro_list gray display_table wid100per">
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <h4><label for="intro_title">Personal Message</label></h4>
                        </div>
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <label for="intro_title">{PageContext::$response->campaignDetails->refferalcampaign_message}</label>
                        </div>
                    </div>
                    {/if}
                    {if PageContext::$response->campaignDetails->refferalcampaign_terms_n_conditions neq ''}
                    <div class="intro_list gray display_table wid100per">
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <h4><label for="intro_title">Terms and Conditions</label></h4>
                        </div>
                        <div class="display_table_cell wid50per vlaign_middle pad5_10p">
                            <label for="intro_title">{PageContext::$response->campaignDetails->refferalcampaign_terms_n_conditions}</label>
                        </div>
                    </div>
                    {/if}
                    
                    {if PageContext::$response->campaignDetails->refferalcampaign_paidrefferalmode eq 2 && PageContext::$response->campaignDetails->charity_id eq 0}<!-- Charity Campaign-->
                    <div>
                            <h4><label for="intro_title">Choose charity organization of your choice</label></h4>
                        </div>
                    <div id="divMsg" style="color: red"></div>
                    <div style="float: left;">
                                        <input type="text" name="txtCharitySearch" id="txtSearchCharity">
                                        <a href="#" id="jCharitySearch">Search</a>
                                    </div>
                                    <div>
                                        <select name="charity_country" id="jcharity_country">
                                            <option value="">Select Country</option>
                                            <option value="US">United States</option>
                                            <option value="GB">United Kingdom</option>
                                            <option value="IN">India</option>
                                            
                                        </select>
                                        
                                    </div>
                                    <div class="clear_both"></div>
                    <div class="" id="jbizCharityContent" >
                        {assign var="i" value="1"} 
                        {foreach from=PageContext::$response->charities key=id item=charity}
<!--                        <div class="wid100p">
                            <div class="mediapost">
                            <div class="col-sm-4 col-md-3 col-lg-3">    
                            <div class="bigimg">
                                <span class="mediapost_pic">
                                    <a>
                                    <div class="list_img_blk" style="background-image: url('{$charity->organization_logourl}');"> </div>
                                    </a>
                                </span>
                            </div>
                            </div>
                            <div class="col-sm-8 col-md-9 col-lg-9">
                                <div class="media-body charity_sec_content">
                                <div class="display_table">
                                <div class="display_table_cell wid90per">
                                <a href="{$charity->organization_url}" target="_blank">{$charity->organization_name}</a><p>
                                <i class="fa fa-globe"></i>
                                {$charity->organization_url}</p>
                                <p>
                                <i class="fa fa-map-marker"></i>
                                {$charity->organization_country}
                                </p>
                                 <p>{stripslashes($charity->organization_mission)|substr:0:100} <a class="" href="{$charity->organization_url}" target="_blank">...more</a></p>
       
                                </div>
                               
                          
                            <div class="display_table_cell wid10per">
                                <input  name="txtCharity" id="txtCharity"  type="radio" value="{$charity->organization_id}" class="bizcom_ckbox" {if PageContext::$response->campaignDetails->refferalcampaign_charity_id eq $charity->organization_id} checked{/if} >
                            </div>
                            </div>
                            </div>
                            <div class="clear"></div>
                            </div>
                        </div>
                       </div>-->
              <div class="wid100p">
                                            <div class="mediapost">
                                                <div class="col-sm-4 col-md-3 col-lg-3">
                                                        <div class="bigimg"> 
                                                            <span class="mediapost_pic">
                                                                <a>
                                                                    <div class="list_img_blk" style="background-image: url('{$charity->organization_logourl}');">                                             
                                        </div>
                                                                </a>
                                                            </span>
                                                        </div>
                                                </div>
<!--                                            <div class="charitylist_image">
                                                <img src="{$charity->logoUrl}" >
                                            </div>-->
                                            <div class="col-sm-8 col-md-9 col-lg-9">
                                            <div class="media-body charity_sec_content">
                                                <div class="display_table">
                                                    <div class="display_table_cell wid90per">
                                                        <a class="charity_title" href="{$charity->organization_url}" target="_blank">{$charity->organization_name}</a>
                                                        <p><i class="fa fa-globe"></i> {$charity->organization_url}</p>
                                                        <p><i class="fa fa-map-marker"></i> {$charity->organization_country}</p>
                                                        <p>{stripslashes($charity->organization_mission)|substr:0:100} <a class="" href="{$charity->organization_url}" target="_blank">...more</a></p>
                                                    </div>
                                                    <div class="display_table_cell wid10per">
                                                        <input  name="txtCharity" id="txtCharity"  type="radio" value="{$charity->organization_id}" class="bizcom_ckbox" {if PageContext::$response->campaignDetails->refferalcampaign_charity_id eq $charity->organization_id} checked{/if} >
                                                    </div>
                                                </div>     
                                            </div>
                                            </div>
<!--                                            <div class="bizcom_detail">
                                                <a href="{$charity->url}" target="_blank">{$charity->name}</a><br>
                                                {$charity->url}<br>
                                                {$charity->country}
                                            </div>-->
<!--                                            <div class="display_table_cell wid10per">
                                                <input  name="txtCharity" id="txtCharity"  type="radio" value="{$charity->id}" class="bizcom_ckbox" {if PageContext::$response->campaignDetails->refferalcampaign_charity_id eq $charity->id} checked{/if} >
                                            </div>-->
<!--                                            <div class="bizcomoption">
                                                <input  name="txtCharity" id="txtCharity"  type="radio" value="{$charity->id}" class="bizcom_ckbox" {if PageContext::$response->campaignDetails->refferalcampaign_charity_id eq $charity->id} checked{/if} >
                                            </div>-->
                                            <div class="clear"></div>
                                            </div>
                                        </div>
                        {assign var="i" value=$i+1} 
                        {/foreach}
                        <div class="clear"></div>
                        {if PageContext::$response->charity_has_next gt PageContext::$response->charity_next_orgId}
                        <div id="div_display_more_friends">
                            <a  bid="{PageContext::$response->charity_next_orgId}" class="linkstyle2 load_more_charity_button" >Load More</a>
                        </div>
                        {/if}
         
                    {/if}
                </div>
                <div class="clear">&nbsp;</div>

                <!--                <form action="">-->
                {if PageContext::$response->campaignDetails->refferalcampaign_payment_mode =='F'}
                <div class="bizcom_accept">
                    <a href="{PageContext::$response->baseUrl}user/update_introduction/{PageContext::$response->id}/accept" ><span class="accept"><b>+</b> Accept</span></a>
                </div>
                <div class="bizcom_decline">
                    <a href="{PageContext::$response->baseUrl}user/update_introduction/{PageContext::$response->id}/reject"><span class="reject"><i class="fa fa-times"></i> Decline</span></a>
                </div>
                {else}
                {if strtotime(PageContext::$response->campaignDetails->refferalcampaign_expiry_date) >= strtotime(Date('Y-m-d'))}
                              <div><input type="checkbox" class="" title="I agree to the &lt;a href='' onclick='load_home'&gt;Terms of Service&lt;/a&gt;." placeholder="" size="10" value="1" name="special_terms_conditions" id="special_terms_conditions"> I agree to the <a class ="jCampaign" onclick="load_home();">Terms of Service</a>.<div class="clserror"><label generated="true" for="special_terms_conditions" class="error"></label></div></div>
                              <div id="error_message"></div>

                                <div id="content" class="terms-new-paypal" style="display: none;">{PageContext::$response->special_terms}</div>
                              <input type="hidden" name="hid_paypal_payment" id="hid_paypal_payment">
                              <div class="form-group relative marg0btm">
                            <input required type="email" placeholder="Paypal Email Id" class="form-control" id="paypal_email" name="paypal_email" value="{PageContext::$response->paypalEmail}">
                            <label for="paypal_email" class="error" generated="true"></label>
                        </div>
                <div class="bizcom_accept">
                     {if PageContext::$response->campaignDetails->refferalcampaign_paidrefferalmode eq 2 && PageContext::$response->campaignDetails->charity_id eq 0}<!-- Charity Campaign-->
                        <a href="#" id="jAcceptCharity" cid="{PageContext::$response->id}"><span class="accept"><b>+</b> Accept</span></a>
                     {else}
       
                        <a id="jaccept" cid="{PageContext::$response->id}" href="{PageContext::$response->baseUrl}user/update_introduction/{PageContext::$response->id}/accept"><span class="accept"><b>+</b> Accept</span></a>
                     {/if}   
    <!--                    <input id="" class="invite_btn" type="submit" value="Accept">-->
                </div>
                <div class="bizcom_decline">
                    <a href="{PageContext::$response->baseUrl}user/update_introduction/{PageContext::$response->id}/reject"><span class="reject"><i class="fa fa-times"></i> Decline</span></a>
                </div>
                {else}
                <div class="bizcom_decline">
                    <a href="{PageContext::$response->baseUrl}user/notify_owner/{PageContext::$response->id}/1" class="yellow_btn4"><span class="accept">Notify owner</span></a>
                </div>
                {/if}
                {/if}
<!--                    <input type="hidden" name="introduction" id="userId" value="{PageContext::$response->userId}">-->
                <!--                </form>-->
            </div>  
            <div class="clear"></div>

        </div>
        </div>
    </div>
</div>
</div>