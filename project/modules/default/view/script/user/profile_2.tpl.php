<div id="fb-root"></div>
<div class="container" id="div_container">
<div class="marg20col"> 
<div class="row"> 
    <div id='msg'></div>
<div class="col-sm-3 col-md-3 col-lg-3">
<div class="profilesec_left">
    <div id="profilepic" >

        <a href="#" title="tab-Profile"  id="jProfile" class="jtab profile_pic " alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">
            <img  id="profile_image" src="{PageContext::$response->userImagePath}medium/{PageContext::$response->image}">
        </a>
        <h3 class="profile_name"><a href="#" title="tab-Profile" id="jProfile" class="jtab " alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">
                    {PageContext::$response->username}</a></h3>
         {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
        <a href="#" class="changepic" onclick="showProfileChangeBox(1)" ><i class="fa fa-pencil"></i>
        {/if}
    </a>
           
    </div>
</div>
<div class="profile_pic_sec">

    <div id="profilepic" >

        <a href="#" title="tab-Profile"  id="jProfile" class="jtab profile_pic " alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">
            <img  id="profile_image" src="{PageContext::$response->userImagePath}medium/{PageContext::$response->image}">
        </a>
      
         {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
        <a href="#" class="changepic" onclick="showProfileChangeBox(1)" ><i class="fa fa-pencil"></i>
        {/if}
    </a>
           
    </div>
    <div id='mydiv'></div>
    <h3 class="profile_name"><a href="#" title="tab-Profile" id="jProfile" class="jtab " alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">
                    {PageContext::$response->username}</a></h3>
      {if PageContext::$response->sess_user_id > 0}     
    <div class="profile_pic_actions">
        {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
            <h5>Last logged in <span>{time_elapsed_string(PageContext::$response->sess_user_last_login)}</span></h5>
        {/if}
        <div class="wid100per">
            {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
            <span>
                <select alias="{PageContext::$response->sess_user_alias}" name="business_community_type" id="jbusiness_community_type">
                    <option value="All"{if PageContext::$response->business_type eq "All"} selected{/if}>All</option>
                    <option value="Business"{if PageContext::$response->business_type eq "Business"} selected{/if}>Business</option>
                    <option value="Personal"{if PageContext::$response->business_type eq "Personal"} selected{/if}>Personal</option>
                </select>
            </span>
            {/if}
            <div class="clear"></div>
        </div>
        <div class="wid100per pad5col">
            {if PageContext::$response->alias neq PageContext::$response->sess_user_alias &&  PageContext::$response->sess_user_id > 0}
                <a href="#" name="reply_message" class="replymessage margtop5" id="" onclick="replay_message({PageContext::$response->friend_to},'');" title="Send Message"  src="#">
                    <span class="accept"><i class="fa fa-envelope-o"></i> Send Message</span> 
                </a>
            {if PageContext::$response->userDetails['user_friends_privacy']=='N' && PageContext::$response->sess_user_id > 0}
                 <a href="#" name="mutual_friends" class="margtop5" id="" onclick="mutual_friends({PageContext::$response->id},{PageContext::$response->sess_user_id});">
                    <span class="accept margtop5 pull-right"> Mutual Friends({PageContext::$response->mutual_friend_count})</span> 
                </a>
            {/if}
            {/if}
        </div>
        <div class="clear"></div>
         <div class="wid100per pad5col dishid">
        {if PageContext::$response->sess_user_id > 0 && PageContext::$response->id neq PageContext::$response->sess_user_id && PageContext::$response->sess_user_status != 'I'}
        {if in_array(PageContext::$response->id, PageContext::$response->myfriends)}
       
        <a class=" jUnfriend" uid="{PageContext::$response->friend_to}" href="#" id="jUnfriend_{PageContext::$response->friend_to}">
            <span class="reject margtop5"><i class="fa fa-times"></i> Unfriend</span>
        </a>
<!--        <a class="friends_btn fright friend" id="jshowfriendstatus" uid="{PageContext::$response->id}" href="#" >
            <span class="tik_icon"></span>
            Friends</a>-->
<!--        <div class="jfriendstatus" id="friend_status_{PageContext::$response->id}" style="display: none; background-color: rgb(247, 247, 247);
                        border: thin double rgb(255, 210, 41);
                        height: 60px;
                        left: 127px;
                        padding: 3px;
                        position: absolute;
                        text-align: center;
                        top: 88px;
                        width: 155px;">
                            
                            <a class="jChangetopersonal jChangetopersonal_{PageContext::$response->id}" id="{PageContext::$response->id}">{if PageContext::$response->myfriendsstatus[{PageContext::$response->id}] eq 'P'} * {/if}Personal</a><br>
                            <a class="jChangetoBusiness jChangetoBusiness_{PageContext::$response->id}" id="{PageContext::$response->id}">{if PageContext::$response->myfriendsstatus[{PageContext::$response->id}] eq 'B'} * {/if}Business</a>
                        </div>-->
                        <p>
                            <div class="dropdown">
                              <button class="btn btn-default right dropdown-toggle jshowfriendstatus" type="button" id="{PageContext::$response->id}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <span class="tik_icon"></span>
                                Friends
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="{PageContext::$response->id}">
                                <li><a class="jChangetopersonal jChangetopersonal_{PageContext::$response->id}" id="{PageContext::$response->id}">{if PageContext::$response->myfriendsstatus[{PageContext::$response->id}] eq 'P'} <span class='tik_icon'></span> {/if}Personal</a></li>
                                <li><a class="jChangetoBusiness jChangetoBusiness_{PageContext::$response->id}" id="{PageContext::$response->id}">{if PageContext::$response->myfriendsstatus[{PageContext::$response->id}] eq 'B'} <span class='tik_icon'></span> {/if}Business</a></li>
                              </ul>
                            </div>
                            </p>
            {else if in_array(PageContext::$response->id,PageContext::$response->myinvitedfriends)}
            <a class="friends_btn fright friend " uid="{PageContext::$response->id}" href="#" >
                <span class="addfriend_icon jaddfriend"></span>
                <span id="jFriend_{PageContext::$response->id}">Friend request sent</span>
            </a>
                {else if in_array(PageContext::$response->id,PageContext::$response->myPendingRequests)}
                <a class="friends_btn fright friend jaddfriend1" uid="{PageContext::$response->id}" href="#"  inviteid="{PageContext::$response->inviteId}" >
                    <span class="addfriend_icon "></span>
                    <span id="jFriend_{PageContext::$response->id}">Accept Friend</span>
                </a>
                    {else}
                    <a class="friends_btn fright friend jaddfriend" uid="{PageContext::$response->id}" href="#" >
                        <span class="addfriend_icon jaddfriend"></span>
                        <span id="jFriend_{PageContext::$response->id}">Add Friend</span>
                        
                    </a>
                    
                    <div class="loading right" id="loading_{PageContext::$response->id}" style="display: none;">
                       <img src="{PageContext::$response->userImagePath}default/loader.gif" />
                    </div>
                    
                    {/if}
                    {/if}
                     </div>
                    <div class="clear"></div>
    </div>
      {/if}
    
</div>
<div class="profile_leftlinks">
    <ul>
        <!--{PageContext::$response->alias}----{PageContext::$response->sess_user_alias}-----{PageContext::$response->userDetails['user_friends_privacy']}-->
        {if PageContext::$response->sess_user_id eq 0}
        <li><div class="display_table"><div class="profile_leftlinks_left"><i class="fa fa-users"></i>{if PageContext::$response->alias neq PageContext::$response->sess_user_alias && PageContext::$response->userDetails['user_friends_privacy']=='Y'} Friends {else}<a href="#" title="tab-Friends" id="" class="jtab jShowFriends" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">Friends</a>{/if} </div><div class="profile_leftlinks_right">{if PageContext::$response->alias neq PageContext::$response->sess_user_alias && PageContext::$response->userDetails['user_friends_privacy']=='Y'}<span title="tab-Friends" id="" class="jtab profile_leftlinks_right" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">{if PageContext::$response->userDetails['user_friends_count']>0}{PageContext::$response->userDetails['user_friends_count']}{else}0{/if}</span>{else}<span><a href="#" title="tab-Friends" id="" class="jtab jShowFriends" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}">{if PageContext::$response->userDetails['user_friends_count']>0}{PageContext::$response->userDetails['user_friends_count']}{else}0{/if}</a></span>{/if}</div></div></li>
        {/if}
        {if PageContext::$response->sess_user_id > 0}
        <li><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-user" aria-hidden="true"></i></div><div class="profile_leftlinks_left"><a>New Campaign Offers Received</a></div><div class="profile_leftlinks_right"></div></div>
            <ul>
                <li class="profile_left_sublinks"><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-angle-right" aria-hidden="true"></i></div><div class="profile_leftlinks_left"><a href="{PageContext::$response->baseUrl}incentivised-campaigns">Cash</a></div><div class="profile_leftlinks_right"><span><a href="{PageContext::$response->baseUrl}incentivised-campaigns">{if PageContext::$response->offers_cash_count > 0}{PageContext::$response->offers_cash_count}{else}0{/if}</a></span></div></div></li>
                <li class="profile_left_sublinks"><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-angle-right" aria-hidden="true"></i></div><div class="profile_leftlinks_left"><a href="{PageContext::$response->baseUrl}charitable-campaigns">Charitable</a></div><div class="profile_leftlinks_right"><span><a href="{PageContext::$response->baseUrl}charitable-campaigns">{if PageContext::$response->offers_charitable_count > 0}{PageContext::$response->offers_charitable_count}{else}0{/if}</a></span></div></div></li>
                <li class="profile_left_sublinks"><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-angle-right" aria-hidden="true"></i></div><div class="profile_leftlinks_left"><a href="{PageContext::$response->baseUrl}voucher-campaigns">Vouchers</a></div><div class="profile_leftlinks_right"><span><a href="{PageContext::$response->baseUrl}voucher-campaigns">{if PageContext::$response->offers_voucher_count > 0}{PageContext::$response->offers_voucher_count}{else}0{/if}</a></span></div></div></li>
                <li class="profile_left_sublinks"><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-angle-right" aria-hidden="true"></i></div><div class="profile_leftlinks_left"><a href="{PageContext::$response->baseUrl}goodwill-campaigns">Goodwill</a></div><div class="profile_leftlinks_right"><span><a href="{PageContext::$response->baseUrl}goodwill-campaigns">{if PageContext::$response->offers_goodwill_count > 0}{PageContext::$response->offers_goodwill_count}{else}0{/if}</a></span></div></div></li>
            </ul>
        </li>
        {/if}
        {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
        <li><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-gift" aria-hidden="true"></i></div><div class="profile_leftlinks_left"><a>Referral Offers I have made</a></div><div class="profile_leftlinks_right"><span><a>{pagecontext::$response->refferalcountsent}</a></span></div></div></li>
        {/if}
        {if PageContext::$response->sess_user_id > 0}
        <li><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-dot-circle-o" aria-hidden="true"></i></div><div class="profile_leftlinks_left"><a href="{PageContext::$response->baseUrl}my-active-campaigns">My Active Campaigns</a></div><div class="profile_leftlinks_right"><span><a href="{PageContext::$response->baseUrl}my-active-campaigns">{if PageContext::$response->active_campaign_count > 0}{PageContext::$response->active_campaign_count}{else}0{/if}</a></span></div></div></li>
        <li><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-users"></i></div><div class="profile_leftlinks_left"><a href="#" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}" groupby="Business" class="jtab jShowFriends">My Business Friends</a></div><div class="profile_leftlinks_right"><span><a href="#" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}" groupby="Business" class="jtab jShowFriends">{if PageContext::$response->business_friend > 0}{PageContext::$response->business_friend}{else}0{/if}</a></span></div></div></li>
        <li><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-users"></i></div><div class="profile_leftlinks_left"><a href="#" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}" groupby="Personal" class="jtab jShowFriends">My Personal Friends</a></div><div class="profile_leftlinks_right"><span><a href="#" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}" groupby="Personal" class="jtab jShowFriends">{if PageContext::$response->personal_friend > 0}{PageContext::$response->personal_friend}{else}0{/if}</a></span></div></div></li>
        {/if}
        {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
        <li><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-exclamation-circle"></i></div><div class="profile_leftlinks_left"><a href="{PageContext::$response->baseUrl}pending-request">Pending Friend Requests</a></div><div class="profile_leftlinks_right"><span><a href="{PageContext::$response->baseUrl}pending-request">{PageContext::$response->pendingRequestCount}</a></span></div></div></li>
        {/if}
        <li><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-briefcase"></i></div><div class="profile_leftlinks_left">{if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }<a href="{PageContext::$response->baseUrl}my-community">My BizComs</a>{else} BizComs {/if}</div><div class="profile_leftlinks_right">{if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }<span><a href="{PageContext::$response->baseUrl}my-community">{PageContext::$response->community_count}</a></span>{else}<span>{PageContext::$response->userDetails['user_community_count']}{/if}</span></div></div></li>
        {if PageContext::$response->sess_user_id > 0}
        <li><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-cube"></i></div><div class="profile_leftlinks_left">{if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }<a href="{PageContext::$response->baseUrl}my-subscribed-community">My BizCom Subscriptions</a>{else} BizCom Subscriptions{/if}</div><div class="profile_leftlinks_right">{if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }<span><a href="{PageContext::$response->baseUrl}my-subscribed-community">{PageContext::$response->Subscribed_community_count}</a></span>{else}<span>{PageContext::$response->userDetails['user_community_count']}{/if}</span></div></div></li>
        {/if}
        <li><div class="display_table"><div class="profile_lefticons_left"><i class="fa fa-briefcase"></i></div><div class="profile_leftlinks_left">{if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }<a href="{PageContext::$response->baseUrl}businesses">My BizDirectories</a> {else} BizDirectories{/if}</div><div class="profile_leftlinks_right">{if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }<span><a href="{PageContext::$response->baseUrl}businesses">{PageContext::$response->userDetails['user_business_count']}</a></span>{else}<span>{PageContext::$response->userDetails['user_business_count']}</span>{/if}</div></div></li>
</ul>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
        
<!--        {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
        <li><div class="display_table"><div class="profile_leftlinks_left"><i class="fa fa-exclamation-circle"></i><a href="{PageContext::$response->baseUrl}pending-request">Outstanding Refferal Offer Recieved</a></div><div class="profile_leftlinks_right"><span><a href="{PageContext::$response->baseUrl}pending-referral-offer">{pagecontext::$response->refferalcount}</a></span></div></div></li>
        {/if}-->
        
    




<div class="col-sm-9 col-md-9 col-lg-9 marg20col">  
<div id="o-wrapper" class="o-wrapper">
<!---Menu Link----------------- -->
{if PageContext::$response->sess_user_id >0}
<div class="main">
<div class="header-main">
     <main class="o-content">
        <div class="o-container">
          
          <div class="c-buttons">
            <button id="c-button--slide-right" class="button-nav-toggle"><span class="icon">&#9776;</span></button>
          </div>
          
          <div id="github-icons"></div>

        </div><!-- /o-container -->
      </main><!-- /o-content -->    
</div>
</div>  
{/if}
</div>
<!---EOF Menu Link----------------- -->


{if PageContext::$response->alias eq PageContext::$response->sess_user_alias OR PageContext::$response->userDetails['user_friends_privacy']=='N'}
            <div class="tab_container">
                <div class="row">
                <div class="col-sm-8 col-md-8 col-lg-8">
                <ul id="myTab" class="nav nav-tabs nav-justified">
                    <!--{if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }-->
                    <li class="active jShowFeeds"><a href="#newsfeed" data-toggle="tab">News Feed</a>
                    </li>
                    <!--{/if}-->
                    <li class="jShowProfile"  alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}"><a href="#myprofile" data-toggle="tab">{if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }My Profile{else}{if PageContext::$response->alias eq PageContext::$response->sess_user_alias OR PageContext::$response->userDetails['user_friends_privacy']=='N'} Profile{/if}{/if}</a>
                    </li>
                    {if PageContext::$response->alias eq PageContext::$response->sess_user_alias OR PageContext::$response->userDetails['user_friends_privacy']=='N'}
                    <li class="jShowFriends jShowFriends_tab" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}"><a href="#tab-Friends" data-toggle="tab">Friends({if PageContext::$response->userDetails['user_friends_count']>0}{PageContext::$response->userDetails['user_friends_count']}{else}0{/if})</a>
                    </li>
                    {/if}
                     <li style="display: none;" class="Jtabfriends_data" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}"><a onclick="mutual_friends(185,1);" href="#tab-Friends" data-toggle="tab">Mutual Friends</a>
                    </li>
                </ul>
                </div>
				<div class="col-sm-4 col-md-4 col-lg-4">
                 <div class="btn-group"> 
                   
                                {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
                                    <select name="txtFilter" alias="{PageContext::$response->sess_user_alias}" id="jFeedSearch" class="form-control">
                                        <option value="" {if PageContext::$response->search eq ""} selected{/if}>All</option>
                                        <option value="A" {if PageContext::$response->search eq A} selected{/if}>Announcements</option>
                                        <option value="S" {if PageContext::$response->search eq S} selected{/if}>Shared Feeds</option>
                                        <option value="F" {if PageContext::$response->search eq F} selected{/if}>Pending Friend Request</option>
                                        <option value="B" {if PageContext::$response->search eq B} selected{/if}>Pending Bizcom Members</option>
                                        {foreach from=PageContext::$response->communities key=id item=community} 
                                        <option value="{$community->community_id}" {if PageContext::$response->search eq $community->community_id} selected{/if}>{$community->community_name}</option>
                                        {/foreach}
                                    </select>
                                    {/if}
                              
            
                                </div>
                   <!-- {if $flag > 0 || PageContext::$response->search neq ''}     
                                {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
                                <div class="btn-group"> 
                                    <select name="txtFilter" alias="{PageContext::$response->sess_user_alias}" id="jFeedSearch" class="form-control">
                                        <option value="" {if PageContext::$response->search eq ""} selected{/if}>All</option>
                                        <option value="A" {if PageContext::$response->search eq A} selected{/if}>Announcements</option>
                                        <option value="S" {if PageContext::$response->search eq S} selected{/if}>Shared Feeds</option>
                                        <option value="F" {if PageContext::$response->search eq F} selected{/if}>Pending Friend Request</option>
                                        <option value="B" {if PageContext::$response->search eq B} selected{/if}>Pending Bizcom Members</option>
                                                                    {foreach from=PageContext::$response->communities key=id item=community} 
                                        <option value="{$community->community_id}" {if PageContext::$response->search eq $community->community_id} selected{/if}>{$community->community_name}</option>
                                        {/foreach}
                                    </select>
            
                                </div>
                                                                <div class="clearfix"></div>
                                {/if}
                                                                  {/if}-->
                </div>
                </div>
            </div>
{/if}
<!--<div class="mediapost">
       <h3>Invite Friends</h3>
        <input type="button" id="jInviteFriends" title="invite-block" class="invite_btn" value="Invite" name="invite">
        <div class="" id="msgDisplay1"></div>
        <div id="jDisplayInviteGrp" style="display:none">
        {PageContext::renderRegisteredPostActions('importcontacts')}
        <input type="button" id="jdisplayEmailOption" title="invite-block" name="cs_import" class="invite_btn" value="Enter mail ids to invite" name="invite">
        </div>
      <div id="jDivEmail" style="display:none">
                                                    <textarea style="height:80px;width:370px;" placeholder="Enter email id's" id="txtmailids" name="txtmailids"></textarea>
                                                    <div class="clear"></div>
                                                    <input class="invite_btn" type="button" uid="{PageContext::$response->sess_user_id}" id="jEmailFriendRequest" Value="Send">
        </div>
        <div class="clearfix"></div>
    </div>-->
<div id ="ajax"></div>
                <div id="myTabContent" class="tab-content">
                    {php}PageContext::renderRegisteredPostActions('messagebox');{/php}
                     {if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
                    <div class="tab-pane fade active in" id="newsfeed">
                        <div class="row">
                            <!-------------Feeds---------------->
							
<!--							<div class="col-sm-4 col-md-4 col-lg-3 filter_outer filter_outer_T">
								{if PageContext::$response->alias eq PageContext::$response->sess_user_alias && PageContext::$response->sess_user_alias != '' }
								<div class="btn-group pull-right"> 
									<select name="txtFilter" alias="{PageContext::$response->sess_user_alias}" id="jFeedSearch" class="form-control">
										<option value="" {if PageContext::$response->search eq ""} selected{/if}>All</option>
										<option value="A" {if PageContext::$response->search eq A} selected{/if}>Announcements</option>
										<option value="S" {if PageContext::$response->search eq S} selected{/if}>Shared Feeds</option>
										<option value="F" {if PageContext::$response->search eq F} selected{/if}>Pending Friend Request</option>
										<option value="B" {if PageContext::$response->search eq B} selected{/if}>Pending Bizcom Members</option>
										 {foreach from=PageContext::$response->communities key=id item=community} 
										<option value="{$community->community_id}" {if PageContext::$response->search eq $community->community_id} selected{/if}>{$community->community_name}</option>
										{/foreach}
									</select>
							
								</div>
								{/if}
							<div class="clear"></div>
							</div>-->
				{assign var=flag value=0}			
					<div class="col-sm-12 col-md-12 col-lg-8 jfeeddisplay">
                                <div class="profile-bnr mediapost news_feed">
                                    <div class="post_comment_div">
                                        <span>
                                            <input name="newsfeed_comment" id="newsfeed_comment" type="text" placeholder="Write Your Comments">
                                        </span>
                                        <div class="display_image_div">
                                             <div id="juploadimagepreview"></div>
                                        </div>
                                    </div>
                                    <div class="newsfeed_icons_div">
                                        <a> <label for="file_feed" ><i class="fa fa-camera" aria-hidden="true"></i></label></a>
                                         <input type="file" id="file_feed" class="jimage_feed" style="cursor: pointer;  display: none"/>
                                         <div class="pull-right"><button value="Post" onclick="postNewsFeed()">Post</button></div>
                                    </div>
                                </div>
                                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-8 jfeeddisplay_div">
                                  {foreach from=PageContext::$response->news_feed key=id item=feed}
                                {assign var=flag value={$flag}+1}
                                    <div class="profile-bnr mediapost news_feed_div">
                                        <div>
                                            <div class="mediapost_head">
                                            </div>

                                            <div class="col-md-8">
                                                    <p class="normal_txt_profile">{$feed->news_feed_comment}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="new-text text-right right pad15top">
                                                    {if $feed->days gt 0} {$feed->days} days ago 
                                                    {else} Today
                                                    {/if}
                                                </span>
                                            </div>
                                        </div>
                                    
									{if $feed->news_feed_image_name}
                                        <br>
                                        <img class="image_zoom img-responsive center" src="{PageContext::$response->userImagePath}{$feed->news_feed_image_name}">
                                        {/if}
                                        <div class="clearfix"></div>
                                        <div class="mediapost_user-side no-top-mrg">
                                            <div class="fb-share-button" id="share_button_{$feed->news_feed_id}" data-href="{PageContext::$response->baseUrl}newsfeed-detail/{$feed->news_feed_alias}" data-title ="Shared on facebook" data-layout="button_count" data-desc="You can share data" data-url="{PageContext::$response->baseUrl}newsfeed-detail/{$feed->news_feed_alias}"></div>
                                            <a href="{PageContext::$response->baseUrl}newsfeed-detail/{$feed->news_feed_alias}" title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                                        <span class="mediapost_links" id="jcountlike_{$feed->news_feed_id}">{if $feed->news_feed_num_like > 0}{$feed->news_feed_num_like} Likes {/if}</span>
                                        <span class="mediapost_links jNewsfeedCommentButton count_class"  aid ="{$feed->news_feed_id}" id="jcountcomment_{$feed->news_feed_id}">{if $feed->news_feed_num_comments > 0}{$feed->news_feed_num_comments}
                                            Comments 
                                            {/if}
                                        </span>
                                        <!--<span class="mediapost_links" id="jcountshare_{$feedlist['community_announcement_id']}">{if $feedlist['community_announcement_num_shares'] > 0}{$feedlist['community_announcement_num_shares']} Shares{/if}</span>-->
                                        </div>
                                         <!---------------Share buttons-------------------------------->
                                   <div class="mediapost_user-side-top clear">
                                        <div class="display_table_cell pad10_right">
                                            <a href="#" class=" colorgrey_text jLikeNewsFeed {if $feedlist['LIKE_ID']>0} liked {/if}" id="jlike_{$feed->news_feed_id}" cid="{$feed->news_feed_id}">
                                            <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                            <span id="jlikedisplay_{$feedlist['community_announcement_id']}">Like </span></a>
                                        </div>
                                         <div class="display_table_cell pad10_right">
                                            <i class="fa fa-comments-o icn-fnt-size"></i>
                                            <a href="#" id="jCommentButton_{$feed->news_feed_id}" aid="{$feed->news_feed_id}" class="jNewsfeedCommentButton colorgrey_text">Comment</a>
                                        </div>
<!--                                        <div class="display_table_cell pad10_right">
                                            <i class="fa fa-share-alt icn-fnt-size"></i>
                                            <a href="#" class="jShareFeed colorgrey_text" id="jshare_{$feedlist['community_announcement_id']}" aid="{$feedlist['community_announcement_id']}" cid="{$feedlist['community_id']}">Share</a>
                                        </div>-->
                                         <div class="display_table_cell pad10_right">
                                        <div class="loading" id="loading_suggestheaddiv_suggest_{$feedlist['community_id']}_{$feedlist['community_business_id']}_{$val1}" style="display: none;">
                                        <img src="{PageContext::$response->userImagePath}default/loader.gif" />
                                        </div>
                                        </div>
                                       <br>
                                    </div>
                                    <div class="mediapost_user-side-top clear jCommentDisplayDiv" id="jNewsfeedCommentBoxDisplayDiv_{$feed->news_feed_id}" style="display:none">
                                         <p class="lead emoji-picker-container">
                                            <textarea class="form-control textarea-control" id="watermark_{$feed->news_feed_id}" rows="3" placeholder="Write your comment here..." data-emojiable="true">          

                                            </textarea>
                                             <a class="upload_cambtn" id="{$feedlist['community_announcement_id']}">
                                             <label for="file_{$feed->news_feed_id}" ><i class="fa fa-camera feed-camera"></i></label></a>
                                                  <input type="file" id="file_{$feed->news_feed_id}" aid="{$feed->news_feed_id}" cmid="{$feed->news_feed_comment_id}" class="jfeedimageimage_file" style="cursor: pointer;  display: none"/>
                                              <input type="hidden" id="announcement_id" value="{$feedlist['community_announcement_id']}">

                                         
                                         </p>
                                         <div class="display_image_div">
                                             <div id="juploadcommentfeedimagepreview_{$feed->news_feed_id}"></div>
                                        </div>
<!--                                         {PageContext::$response->Comments|@print_r}-->
                                         <a id="postButton" cid="{$feedlist['community_id']}" cmid="" aid="{$feed->news_feed_id}"  class="btn yellow_btn fontupper pull-right jPostNewsfeedCommentButton"> Post</a>
                                       </div>
                                           <div class="clear commentblk jCommentDisplayDiv" id="jNewsfeedCommentDisplayDiv_{$feed->news_feed_id}" style="display:none">
                                                <div id="posting_{$feed->news_feed_id}">
                                                   <!------------------------Comments-------------------------------->
                                                   {assign var=val value=0}
                                                    {foreach from=PageContext::$response->newsFeed[{$feed->news_feed_id}] key=id item=comments}
                                                    <!--{$id|@print_r}-->
<!--                                                     {assign var=flag value={$flag}+1}
                                                     {assign var=val value=$val+1}
                                                        
                                                        <input type="text" id="hid_announcement_id_{$comments->announcement_comment_id}" class="hid_announcement_id" value="{$feedlist['community_announcement_id']}" style="display: none;" >   
                                                        <input type="text" id="hid_{$comments->announcement_comment_id}" value="{$val}" style="display: none;">-->
                                                        <div class="col-md-12 btm-mrg pad10p">
                                                            <div class="row">
                                                                <div class="col-md-1">
                                                                    <!--{PageContext::$response->userImagePath}medium/{$comments->user_image_name}-->
                                                                    <!--{assign var="printad" value="{PageContext::$response->userImagePath}medium/{$comments->user_image_name}"}--> 
                                                                    {if $comments->file_path neq ''}
                                                                        <span class="mediapost_pic">
                                                                            <img class="ryt-mrg" src="{PageContext::$response->userImagePath}medium/{$comments->file_path}">
                                                                        </span>
                                                                    {/if} 
                                                                     {if $comments->file_path eq ''}
                                                                        <span class="mediapost_pic">
                                                                            <img class="ryt-mrg" src="{PageContext::$response->userImagePath}medium/member_noimg.jpg">
                                                                        </span>
                                                                     {/if}
                                                                    
                                                                </div>
                                                                <div class="col-md-11">
                                                                    <span class="name">{$comments->news_feed_comment_user_name} </span>{$comments->news_feed_comment_content}
                                                                    <br>
                                                                    <span class="new-text">
                                                                        
                                                                        {$comments->commentDate} 
                                                                    </span>
                                                                    <div class="commentimg_box">
                                                                        {if $comments->file_path neq ''}
                                                                          <img class="image_zoom" src="{PageContext::$response->userImagePath}medium/{$comments->file_path}">
                                                                        {/if}
                                                                    </div>
                                                                    <div class="mediapost_user-side1">
                                                                        <span class="mediapost_links" id="jcountfeedcommentlike_{$comments->news_feed_comments_id}">{if $comments->num_comment_likes > 0}{$comments->num_comment_likes} Likes {/if}</span>
                                                                        <input type="hidden" id="reply_{$comments->news_feed_comments_id}" value="{if $comments->num_replies > 0}{$comments->num_replies}{else}0{/if}">
                                                                        <span class="mediapost_links " cid="{$comments->announcement_comment_id}" id="jcountreply_{$comments->news_feed_comments_id}"><a href="#" class="jShowFeedReply" id="{$comments->news_feed_comments_id}"><span id="jcountreply_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>">Replies</span></a></span>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                        <div class="comment_sharelike_sec">
                                                                        {if $comments->user_id eq PageContext::$response->sess_user_id OR $comments->community_created_by eq PageContext::$response->sess_user_id}
                                                                        <a href="#" class="jFeedCommentDelete marg10right" aid="{$feedlist['community_announcement_id']}" id="{$comments->announcement_comment_id}"><i class="fa fa-times"></i> Delete</a>
                                                                        {/if}
                                                                        <a href="#" class="jFeedCommentReply marg10right" aid="{$feedlist['community_announcement_id']}" cid="{$comments->announcement_comment_id}" id="{$comments->announcement_comment_id}"><i class="fa fa-reply"></i> Reply</a>
                                                                        <!--<a href="#" class="jFeedCommentLike" aid="{$feedlist['community_announcement_id']}" id="{$comments->announcement_comment_id}">Like</a>-->
                                                                         <a href="#" class="jNewsFeedCommentLike {if $comments->LIKE_ID>0} liked {/if}" id="jlikeComment_{$comments->announcement_comment_id}" cid="{$comments->news_feed_comments_id}" aid="{$comments->news_feed_id}" >
                                                
                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                                    <span id="jlikedisplaycomment_{$comments->announcement_comment_id}">Like </span></a>
                                                                        </div>
                                                                    </span>
                                                                    <div id="jdivReply_{$comments->announcement_comment_id}" class="jDisplayReply">
                                                                        <p class="lead emoji-picker-container">
                                                                            <textarea data-emojiable="true" placeholder="Write your reply here..."  class="form-control textarea-control" style="height:35px;" id="watermark_{$comments->news_feed_id}_{$comments->news_feed_comments_id}" class="watermark" name="watermark"  >
                                                                              
                                                                            </textarea>
                                                                        </p>
                                                                        <a class="upload_cambtn" id="{$feedlist['community_announcement_id']}" >
                                                                                <label for="imagereply_file_{$comments->news_feed_comments_id}"><i class="fa fa-camera feed-reply-camera"></i></label>
                                                                               <div class="file_button"> <input type="file" id="imagereply_file_{$comments->news_feed_comments_id}" cid="{$comments->news_feed_comments_id}" class="jfeedimagereply_file" style="display:none;" /></div></a>
                                                                        <!--<input type="hidden" id="reply_comment_id" value="{$comments->announcement_comment_id}">-->
                                                                        <!--<a id="shareButton" cid="{$feedlist['community_id']}" aid="{$feedlist['community_announcement_id']}"  class="btn yellow_btn fontupper pull-right jshareCommentButton"> Post</a>-->
                                                                        <a id="shareButton" cid="{$feedlist['community_id']}" cmid="{$comments->news_feed_comments_id}" aid="{$comments->news_feed_id}"  class="btn yellow_btn fontupper pull-right jPostNewsfeedCommentButton"> Post</a>
                                                                    </div>
                                                                     <div class="loader" id="imagereply_loader_{$comments->announcement_comment_id}">
                                                                        <img src="{PageContext::$response->userImagePath}default/loader.gif" />
                                                                    </div>
                                                                    <div id="jDivReplyImagePreview_{$comments->news_feed_comments_id}"></div>
                                                                    <div id="postingReply_{$comments->news_feed_comments_id}"></div>
                                                                </div>
                                                            </div> 
                                                            
                                                        </div>
                                                    {/foreach}
<!--                                                    {literal}
                                                            <script>
                                                             {/literal}
                                                                var y = $("#hid_{$comments->announcement_comment_id}").val();
                                                                $("#jcountcomment_{$feedlist['community_announcement_id']}").html('{$val} Comment')
                                                             {literal}
                                                            </script>
                                                            {/literal}-->
                                                    {if !empty(PageContext::$response->Comments[$feedlist['community_announcement_id']])}
                                                    {if PageContext::$response->Page[$feedlist['community_announcement_id']]['totpages'] != PageContext::$response->Page[$feedlist['community_announcement_id']]['currentpage']}
                                                    <div id="jDisplayMoreComments_{$feedlist['community_announcement_id']}" ><a href="#" id="jMorecommentsButton" pid="{PageContext::$response->Page[$feedlist['community_announcement_id']]['currentpage']}" aid="{$feedlist['community_announcement_id']}">Load more comments</a></div>
                                                    {/if}
                                                    {/if}
                                                    
                                                    <!------------------------EOF Comments-------------------------------->
                                                </div>
                                           
                                       
                                   </div>
                                    </div>     
                                	{/foreach}
                                
<!--                                {foreach from=PageContext::$response->adminannouncement key=id item=announcement}
                                {assign var=flag value={$flag}+1}
                                    <div class="profile-bnr mediapost admin_announcement">
                                    <div>
										<div class="mediapost_head">
                                            <h4>
                                                <a href="#" class="{if $announcement->admin_announcement_description neq ''}jworkflow_div{/if}" id="{$announcement->admin_announcement_id}">{$announcement->admin_announcement_title}</a>
                                                <a href="#" class="jworkflow_div" id="{$announcement->admin_announcement_id}">Working with BizBfriend</a>
                                                <div class="pull-right">
                                                    <a class="normal_txt" title="private"><span class="posticon"><i class="fa fa-user" aria-hidden="true"></i></span> From the <b>BizBfriend Team</b></a>
                                                </div>
                                            </h4>
										</div>
                                        
										<div class="col-md-8">
                                                                                     <h5>
                                                <a>{$announcement->admin_announcement_title}</a></h5>
											<p class="normal_txt_profile">{$announcement->admin_announcement_content}</p>
										</div>
                                        <div class="col-md-4">
                                            <span class="new-text text-right right pad15top">
                                                {if $announcement->days gt 0} {$announcement->days} days ago 
                                                {else} Today
                                                {/if}
                                            </span>
                                        </div>
                                    </div>
                                    
									{if $announcement->file_path}
                                        <br>
                                        <img class="image_zoom img-responsive center" src="{PageContext::$response->userImagePath}{$announcement->file_path}">
                                        {/if}
                                      </div>     
                                	{/foreach}-->
                                
                                 {assign var=val value=1}
                                 {if PageContext::$response->search eq "A" OR PageContext::$response->search eq "" OR PageContext::$response->search eq "S"}
                                {foreach from=PageContext::$response->feeds key=id item=feed}
                                {foreach from=$feed key=id item=feedlist}
                                                                   <!--{$feedlist|print_r}-->
                                  {assign var=flag value={$flag}+1}
                                  {assign var=val value={$val}+1}
                                 {assign var=val1 value={$val1}+1}  
                                 
                           
                                    {php}  $content = Communities::getCommunitySuggestionsall({$feedlist['community_business_id']},{$feedlist['community_id']});{/php}
                                     {assign var=suggest value=Communities::getCommunitySuggestionsall({$feedlist['community_business_id']},{$feedlist['community_id']})}
                                     <!--{$suggest|print_r}-->
                                     
                                <div class="mediapost " id="suggestdiv_suggest_{$feedlist['community_id']}_{$feedlist['community_business_id']}_{$val}">
                                    <div class="suggest" id="suggestheaddiv_suggest_{$feedlist['community_id']}_{$feedlist['community_business_id']}_{$val}">
                                    <div class="mediapost_head">
                                        <h4>
                                            {if $feedlist['announcement_share_id'] >0}
                                                {$feedlist['user_name']} shared <a href="{PageContext::$response->baseUrl}community/{$feedlist['community_alias']}">{$feedlist['community_name']}'s</a> announcement
                                                <span id="jDisplayCommunityJoin"><a  class="jFeedCommunityJoin" cid="{$feedlist['community_id']}" style="color: #0000FF;" href="#">
                                                <input class="btn yellow_btn fontupper right" type="button" title="invite-block" value="Join" name="invite">
                                                </a></span>
                                            {else}
                                            <a class="" id="suggest_{$feedlist['community_id']}_{$feedlist['community_business_id']}" href="{PageContext::$response->baseUrl}community/{$feedlist['community_alias']}">{$feedlist['community_name']}</a>
                                            {/if}
                                            {if $feedlist['community_type'] eq 'PRIVATE'}
                                            <div class="pull-right">
                                                <a class="normal_txt" title="private"><span class="privateicon"><i class="fa fa-lock" aria-hidden="true"></i></span> Private</a>
                                            </div>
                                            
                                            {/if}
                                      {if $feedlist['community_logo_name'] neq ''}
                                         <div class="pull-right">
                                                    <a class="normal_txt" title="private"><img src="{PageContext::$response->userImagePath}thumb/{$feedlist['community_logo_name']}"></a>
                                         </div>
                                      {/if}
                                        </h4>
                                    </div>
                                    <div class="mediapost_left pull-left">
                                        {if $feedlist['file_path'] neq ''}
                                        <span class="mediapost_pic">
                                            <img src="{PageContext::$response->userImagePath}medium/{$feedlist['file_path']}">
                                        </span>
                                        {else}
                                        <span class="mediapost_pic">
                                            <img src="{PageContext::$response->userImagePath}medium/member_noimg.jpg">
                                        </span>
                                        {/if}
                                    </div>
                                    <div class="media-body">
                                    <div class="col-md-8">
                                        <h4 class="media-heading"><a href="{PageContext::$response->baseUrl}announcement-detail/{$feedlist['community_announcement_alias']}" >{$feedlist['community_announcement_title']}</a></h4>
                                        <p>{$feedlist['community_announcement_content']}</p>
                                     <br>
                                          
                                    </div>
                                       <div class="col-md-4 new-text text-right">
                                   {if $feedlist['days'] gt 0} {$feedlist['days']} days ago 
                                        {else} Today
                                        {/if}
                                    </div>
                                     </div>
                                        <div class="profile-bnr">

                                     {if $feedlist['community_announcement_image_path']}
                                           <img class="image_zoom img-responsive center" src="{PageContext::$response->userImagePath}{$feedlist['community_announcement_image_path']}"> 
                                        {/if} 
                                       </div>
                                        </div>
                                    <!----------------Statistics ------------------------>
                                    <div class="mediapost_user-side no-top-mrg">
                                            <div class="fb-share-button" id="share_button_{$feedlist['community_announcement_id']}" data-href="{PageContext::$response->baseUrl}announcement-detail/{$feedlist['community_announcement_alias']}" data-title ="Shared on facebook" data-layout="button_count" data-desc="You can share data" data-url="{PageContext::$response->baseUrl}announcement-detail/{$feedlist['community_announcement_alias']}"></div>
                                            <a href="{PageContext::$response->baseUrl}announcement-detail/{$feedlist['community_announcement_alias']}" title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                                        <span class="mediapost_links" id="jcountlike_{$feedlist['community_announcement_id']}">{if $feedlist['Count'] > 0}{$feedlist['Count']} Likes {/if}</span>
                                        <span class="mediapost_links jCommentButton count_class"  aid ="{$feedlist['community_announcement_id']}" id="jcountcomment_{$feedlist['community_announcement_id']}">{if $feedlist['community_announcement_num_comments'] > 0}{$feedlist['community_announcement_num_comments']}
                                            Comments 
                                            {/if}
                                        </span>
                                        <span class="mediapost_links" id="jcountshare_{$feedlist['community_announcement_id']}">{if $feedlist['community_announcement_num_shares'] > 0}{$feedlist['community_announcement_num_shares']} Shares{/if}</span>
                                   </div>
                                    <!----------------End of  Statistics ------------------------>
                                    <!---------------Share buttons-------------------------------->
                                   <div class="mediapost_user-side-top clear">
                                        <div class="display_table_cell pad10_right">
                                            <a href="#" class=" colorgrey_text jLikeFeed {if $feedlist['LIKE_ID']>0} liked {/if}" id="jlike_{$feedlist['community_announcement_id']}" aid="{$feedlist['community_announcement_id']}" cid="{$feedlist['community_id']}">
                                            <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                            <span id="jlikedisplay_{$feedlist['community_announcement_id']}">Like </span></a>
                                        </div>
                                         <div class="display_table_cell pad10_right">
                                            <i class="fa fa-comments-o icn-fnt-size"></i>
                                            <a href="#" id="jCommentButton_{{$feedlist['community_announcement_id']}}" aid="{$feedlist['community_announcement_id']}" class="jCommentButton colorgrey_text">Comment</a>
                                        </div>
                                        <div class="display_table_cell pad10_right">
                                            <i class="fa fa-share-alt icn-fnt-size"></i>
                                            <a href="#" class="jShareFeed colorgrey_text" id="jshare_{$feedlist['community_announcement_id']}" aid="{$feedlist['community_announcement_id']}" cid="{$feedlist['community_id']}">Share</a>
                                        </div>
                                         <div class="display_table_cell pad10_right">
                                            <i class="farefer"></i>
                                            <a href="#" class="jShowFeedReferralOptions referral_btn-new" aid="{$feedlist['community_announcement_id']}" bid="{$feedlist['community_id']}">Referral offer</a>
                                        </div>
                                        <div class="display_table_cell pad10_right">
                                            <!--<i class="farefer"></i>-->
                                            <a href="#" class="view_other_bizcom" id="suggestheaddiv_suggest_{$feedlist['community_id']}_{$feedlist['community_business_id']}_{$val1}">More BizComs</a>
                                        </div>
                                         <div class="display_table_cell pad10_right">
                                        <div class="loading" id="loading_suggestheaddiv_suggest_{$feedlist['community_id']}_{$feedlist['community_business_id']}_{$val1}" style="display: none;">
                                        <img src="{PageContext::$response->userImagePath}default/loader.gif" />
                                        </div>
                                        </div>
                                       <br>
                                    </div>
                                    <!---------------End of Share buttons-------------------------------->
                                     <div id="jRefDiv_{$feedlist['community_id']}_{$feedlist['community_announcement_id']}" class="fleft"></div>
                                     <!---------------Referral offer-------------------------------->
                                     <div class="jReferralOptionsClass referraltype_blk" id="jDivdisplayReferralOptions_{$feedlist['community_id']}_{$feedlist['community_announcement_id']}" style="display:none;">
                                         <h4>Please choose the preferred referral type</h4>
                                         <ul>
                                             <li><input type="radio" name="RefferalType_{$feedlist['community_id']}" value="1" > Free Referral</li>
                                             <li><input type="radio" name="RefferalType_{$feedlist['community_id']}" value="2" > Financial Incentive Based Referral</li>
                                             <li><input type="radio" name="RefferalType_{$feedlist['community_id']}"  value="3"> Charitable Donation Based Referral</li>
                                             <li><input type="radio" name="RefferalType_{$feedlist['community_id']}" value="4" > Voucher or Product Based Referral</li>
                                             <li><input type="button" class="jClickFeedReferralOffer sendref_offr_btn" aid="{$feedlist['community_announcement_id']}"  bid="{$feedlist['community_id']}" uid="{$feedlist['community_created_by']}" value="Send Referral Offer"></li>
                                         </ul>
                                     </div>
                                     <!---------------End of Referral offer-------------------------------->

                                       <div class="mediapost_user-side-top clear jCommentDisplayDiv" id="jCommentBoxDisplayDiv_{$feedlist['community_announcement_id']}" style="display:none">
                                         <p class="lead emoji-picker-container">
                                             <textarea class="form-control textarea-control" id="watermark_{$feedlist['community_announcement_id']}" rows="3" placeholder="Write your comment here..." data-emojiable="true">          

</textarea>
                                             <a class="upload_cambtn" id="{$feedlist['community_announcement_id']}">
                                             <label for="file_{$feedlist['community_announcement_id']}" ><i class="fa fa-camera feed-camera"></i></label></a>
                                                  <input type="file" id="file_{$feedlist['community_announcement_id']}" aid="{$feedlist['community_announcement_id']}" class="jimage_file" style="cursor: pointer;  display: none"/>
                                              <input type="hidden" id="announcement_id" value="{$feedlist['community_announcement_id']}">

                                         
                                         </p>
<!--                                         {PageContext::$response->Comments|@print_r}-->
                                         <a id="postButton" cid="{$feedlist['community_id']}" cmid="" aid="{$feedlist['community_announcement_id']}"  class="btn yellow_btn fontupper pull-right jPostCommentButton"> Post</a>
                                       </div>
                                     <div class="loading" id="loading_{$feedlist['community_announcement_id']}" style="display: none;">
                                        <img src="{PageContext::$response->userImagePath}default/loader.gif" />
                                    </div>
                                     <div id="juploadimagepreview_{$feedlist['community_announcement_id']}"></div>
                                     <!--<div id='jCommentImagePreview_+{$feedlist['community_announcement_id']}' class="jCommentImagePreview"></div>-->
                                     <div class="clear commentblk jCommentDisplayDiv" id="jCommentDisplayDiv_{$feedlist['community_announcement_id']}" style="display:none">
                                                <div id="posting_{$feedlist['community_announcement_id']}">
                                                   <!------------------------Comments-------------------------------->
                                                   {assign var=val value=0}
                                                    {foreach from=PageContext::$response->Comments[$feedlist['community_announcement_id']] key=id item=comments}
                                                    <!--{$comments|@print_r}-->
                                                     {assign var=flag value={$flag}+1}
                                                     {assign var=val value=$val+1}
                                                        {if $feedlist['community_type'] eq 'PUBLIC' OR $feedlist['community_created_by'] eq PageContext::$response->sess_user_id OR PageContext::$response->sess_user_id eq $comments->user_id}     
                                                        <input type="text" id="hid_announcement_id_{$comments->announcement_comment_id}" class="hid_announcement_id" value="{$feedlist['community_announcement_id']}" style="display: none;" >   
                                                        <input type="text" id="hid_{$comments->announcement_comment_id}" value="{$val}" style="display: none;">
                                                        <div class="col-md-12 btm-mrg pad10p" id="jdivComment_{$comments->announcement_comment_id}">
                                                            <div class="row">
                                                                <div class="col-md-1">
                                                                    <!--{PageContext::$response->userImagePath}medium/{$comments->user_image_name}-->
                                                                    <!--{assign var="printad" value="{PageContext::$response->userImagePath}medium/{$comments->user_image_name}"}--> 
                                                                    {if $comments->file_path neq ''}
                                                                        <span class="mediapost_pic">
                                                                            <img class="ryt-mrg" src="{PageContext::$response->userImagePath}{$comments->file_path}">
                                                                        </span>
                                                                    {/if} 
                                                                     {if $comments->file_path eq ''}
                                                                        <span class="mediapost_pic">
                                                                            <img class="ryt-mrg" src="{PageContext::$response->userImagePath}medium/member_noimg.jpg">
                                                                        </span>
                                                                     {/if}
                                                                    
                                                                </div>
                                                                <div class="col-md-11">
                                                                    <span class="name">{$comments->user_name} </span>{$comments->comment_content}
                                                                    <br>
                                                                    <span class="new-text">
                                                                        
                                                                        {$comments->commentDate} 
                                                                    </span>
                                                                    <div class="commentimg_box">
                                                                        {if $comments->file_path neq ''}
                                                                          <img class="image_zoom" src="{PageContext::$response->userImagePath}medium/{$comments->file_path}">
                                                                        {/if}
                                                                    </div>
                                                                    <div class="mediapost_user-side1">
                                                                        <span class="mediapost_links" id="jcountcommentlike_{$comments->announcement_comment_id}">{if $comments->num_comment_likes > 0}{$comments->num_comment_likes} Likes {/if}</span>
                                                                        <input type="hidden" id="reply_{$comments->announcement_comment_id}" value="{if $comments->num_replies > 0}{$comments->num_replies}{else}0{/if}">
                                                                        <span class="mediapost_links " cid="{$comments->announcement_comment_id}" id="jcountreply_{$comments->announcement_comment_id}"><a href="#" class="jShowReply" id="{$comments->announcement_comment_id}"><span id="jcountreply_<?php echo PageContext::$response->comments['comment']['comment_id']; ?>">{if $comments->num_replies > 0}{$comments->num_replies} Replies{/if}</span></a></span>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                        <div class="comment_sharelike_sec">
                                                                        {if $comments->user_id eq PageContext::$response->sess_user_id OR $comments->community_created_by eq PageContext::$response->sess_user_id}
                                                                        <a href="#" class="jFeedCommentDelete marg10right" aid="{$feedlist['community_announcement_id']}" id="{$comments->announcement_comment_id}"><i class="fa fa-times"></i> Delete</a>
                                                                        {/if}
                                                                        <a href="#" class="jFeedCommentReply marg10right" aid="{$feedlist['community_announcement_id']}" cid="{$comments->announcement_comment_id}" id="{$comments->announcement_comment_id}"><i class="fa fa-reply"></i> Reply</a>
                                                                        <!--<a href="#" class="jFeedCommentLike" aid="{$feedlist['community_announcement_id']}" id="{$comments->announcement_comment_id}">Like</a>-->
                                                                         <a href="#" class="jFeedCommentLike {if $comments->LIKE_ID>0} liked {/if}" id="jlikeComment_{$comments->announcement_comment_id}" cid="{$comments->announcement_comment_id}" aid="{$feedlist['community_announcement_id']}" >
                                                
                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                                    <span id="jlikedisplaycomment_{$comments->announcement_comment_id}">Like </span></a>
                                                                        </div>
                                                                    </span>
                                                                    <div id="jdivReply_{$comments->announcement_comment_id}" class="jDisplayReply">
                                                                        <p class="lead emoji-picker-container">
                                                                            <textarea data-emojiable="true" placeholder="Write your reply here..."  class="form-control textarea-control" style="height:35px;" id="watermark_{$feedlist['community_announcement_id']}_{$comments->announcement_comment_id}" class="watermark" name="watermark"  >
                                                                              
                                                                            </textarea>
                                                                        </p>
                                                                        <a class="upload_cambtn" id="{$feedlist['community_announcement_id']}" >
                                                                                <label for="imagereply_file_{$comments->announcement_comment_id}"><i class="fa fa-camera feed-reply-camera"></i></label>
                                                                               <div class="file_button"> <input type="file" id="imagereply_file_{$comments->announcement_comment_id}" cid="{$comments->announcement_comment_id}" class="jimagereply_file" style="display:none;" /></div></a>
                                                                        <!--<input type="hidden" id="reply_comment_id" value="{$comments->announcement_comment_id}">-->
                                                                        <!--<a id="shareButton" cid="{$feedlist['community_id']}" aid="{$feedlist['community_announcement_id']}"  class="btn yellow_btn fontupper pull-right jshareCommentButton"> Post</a>-->
                                                                        <a id="shareButton" cid="{$feedlist['community_id']}" cmid="{$comments->announcement_comment_id}" aid="{$feedlist['community_announcement_id']}"  class="btn yellow_btn fontupper pull-right jPostCommentButton"> Post</a>
                                                                    </div>
                                                                     <div class="loader" id="imagereply_loader_{$comments->announcement_comment_id}">
                                                                        <img src="{PageContext::$response->userImagePath}default/loader.gif" />
                                                                    </div>
                                                                    <div id="jDivReplyImagePreview_{$comments->announcement_comment_id}"></div>
                                                                    <div id="postingReply_{$comments->announcement_comment_id}"></div>
                                                                </div>
                                                            </div> 
                                                            
                                                        </div>
                                                                {/if}
                                                    {/foreach}
<!--                                                    {literal}
                                                            <script>
                                                             {/literal}
                                                                var y = $("#hid_{$comments->announcement_comment_id}").val();
                                                                $("#jcountcomment_{$feedlist['community_announcement_id']}").html('{$val} Comment')
                                                             {literal}
                                                            </script>
                                                            {/literal}-->
                                                    {if !empty(PageContext::$response->Comments[$feedlist['community_announcement_id']])}
                                                    {if PageContext::$response->Page[$feedlist['community_announcement_id']]['totpages'] != PageContext::$response->Page[$feedlist['community_announcement_id']]['currentpage']}
                                                    <div id="jDisplayMoreComments_{$feedlist['community_announcement_id']}" ><a href="#" id="jMorecommentsButton" pid="{PageContext::$response->Page[$feedlist['community_announcement_id']]['currentpage']}" aid="{$feedlist['community_announcement_id']}">Load more comments</a></div>
                                                    {/if}
                                                    {/if}
                                                    
                                                    <!------------------------EOF Comments-------------------------------->
                                                </div>
                                           
                                       
                                   </div>
                                   
                                    
                                    <div class="clearfix"></div>
                                    <div id = "suggestheaddiv_suggest_{$feedlist['community_id']}_{$feedlist['community_business_id']}_{$val1}_slide" class="jslideSuggestion"></div>
                                </div>
                                 
                                                     <div class="clearfix"></div>
                           
<!--                                                       <div id="suggestiondiv_{$feedlist['community_id']}_{$feedlist['community_business_id']}_{$val}" class="col-sm-4 col-md-4 col-lg-4 suggestion" style="margin-left:90%;width:50%;position: absolute;">
                                 <div class="mediapost">
                                    <div class="mediapost_head">
                                        <h4>BizCom Suggestion</h4>
                                    </div>
                                  {assign var=vals value=0}
                                  {foreach $suggest as $suggestion}
                                     {if $vals gt 2}{break}{/if}
                                    <!--{$suggest[5]|print_r}
                                     <div class="marg5col">
                                    <div class="mediapost_left pull-left bizcomsugg">
                                        <span class="mediapost_pic">
                                            <a href="{PageContext::$response->baseUrl}community/{$suggest[{$vals}]->community_alias}"><img src="{PageContext::$response->userImagePath}medium/{$suggest[{$vals}]->file_path}"></a>
                                        </span>
                                    </div>
                                    <div class="media-body pad15top">
                                        <div class="suggbody">
                                            <div class="suggbody_left">
                                                <h4 class="media-heading"><a href="{PageContext::$response->baseUrl}community/{$suggest[{$vals}]->community_alias}">{$suggest[{$vals}]->community_name}</a></h4>
                                            </div>
                                            <div class="suggbody_right">
                                                <button onclick="joinCommunity({$suggest[{$vals}]->community_id},'join')" class="btn yellow_btn fontupper">Join</button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    </div>-->
<!--                                         {assign var=vals value=$vals+1}
                         {/foreach} 
                         {if $vals eq 0}
                         No Suggestions
                         {/if}
                         {if $vals gt 2}
                         <div class="viewall_suggestion_link" id=""><a href="{PageContext::$response->baseUrl}suggest/{$feedlist['community_id']}/{$feedlist['community_business_id']}" target="_blank">View All</a></div>
                         {/if}

                                   
                                    <div class="clearfix"></div>
                                </div>
                                                    <div class="clearfix"></div>-->
                                {/foreach}
                                {/foreach}
                                {/if}
                                <!---------------------Pending friend request-------------------->
                                {if PageContext::$response->search eq "F" OR PageContext::$response->search eq ""}
                                    {if PageContext::$response->NumFriendRequest>0}
                                     {assign var=flag value={$flag}+1}
                                        <div class="mediapost">
                                        <div class="mediapost_head">
                                            <h4><a href="{PageContext::$response->baseUrl}pending-request">Pending Friend Request</a></h4>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="{PageContext::$response->baseUrl}pending-request" >You have {PageContext::$response->NumFriendRequest} pending friend request</a></h4>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    {/if}
                                  {/if}
                                   <!---------------------Pending bizcom members-------------------->
                                  {if PageContext::$response->search eq "B" OR PageContext::$response->search eq ""}
                                    {if PageContext::$response->NumPendingBizcomMembers>0}
                                     {assign var=flag value={$flag}+1}
                                        <div class="mediapost">
                                        <div class="mediapost_head">
                                            <h4><a href="{PageContext::$response->baseUrl}pending-bizcom-members">Pending Bizcom Members</a></h4>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="{PageContext::$response->baseUrl}pending-bizcom-members" >You have {PageContext::$response->NumPendingBizcomMembers} pending bizcom members</a></h4>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    {/if}
                                  {/if}
                            </div>
						
							<div class="col-sm-12 col-md-12 col-lg-4 pull-right">
                                                            
                                <div id="slider1_container" class="sidebanner_right">
                                    <div u="slides" style="cursor: move; left: 15px; top: 0px; width: 252px; height: 662px;">
                                        <div><img src="{PageContext::$response->ImagePath}rtsidebanner.jpg" class="img-responsive"></div>
                                        <div><img src="{PageContext::$response->ImagePath}bbf_rightbanner2.jpg" class="img-responsive"/></div>
                                        <div><img src="{PageContext::$response->ImagePath}bbf_rightbanner3.jpg" class="img-responsive"/></div>
                                        <div><img src="{PageContext::$response->ImagePath}bbf_rightbanner4.jpg" class="img-responsive"/></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
<!--<br>
                                <div id="slider1_container" class="sidebanner_right">
                                    
                                    <div u="slides" style="cursor: move; position: absolute; overflow: hidden; left: 0px; top: 0px; width: 252px; height: 662px;">
                                        <div><img src="{PageContext::$response->ImagePath}rtsidebanner.jpg" class="img-responsive"></div>
                                        <div><img src="{PageContext::$response->ImagePath}bbf_rightbanner2.jpg" class="img-responsive"/></div>
                                        <div><img src="{PageContext::$response->ImagePath}bbf_rightbanner3.jpg" class="img-responsive"/></div>
                                        <div><img src="{PageContext::$response->ImagePath}bbf_rightbanner4.jpg" class="img-responsive"/></div>
                                                         
                                    </div>
                                </div>-->
<!--                                                <div class="blueberry sidebanner_right" id="slider1_container">
                                                    <ul class="slides">
                                                      <li><img src="{PageContext::$response->userImagePath}rtsidebanner.jpg" class="img-responsive"></li>
                                                      <li><img src="{PageContext::$response->userImagePath}bbf_rightbanner2.jpg" class="img-responsive"/></li>
                                                      <li><img src="{PageContext::$response->userImagePath}bbf_rightbanner3.jpg" /></li>
                                                      <li><img src="{PageContext::$response->userImagePath}bbf_rightbanner4.jpg" /></li>
                                                    </ul>
                                                  </div>-->

							</div>
                                                <!--<div class="col-sm-4 col-md-4 col-lg-4 pull-right filter_outer">-->
                                                   
                                                <!--</div>-->
                                                
                                              
                            
                           
                            <!----------------End of Feeds-------------------->
                            <!------------------Bizcom suggestion-------------->
<!--                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="mediapost">
                                    <div class="mediapost_head">
                                        <h4>BizCom Suggestion</h4>
                                    </div>
                                    <div class="marg5col">
                                    <div class="mediapost_left pull-left bizcomsugg">
                                        <span class="mediapost_pic">
                                            <img src="{PageContext::$response->userImagePath}medium/{PageContext::$response->image}">
                                        </span>
                                    </div>
                                    <div class="media-body pad15top">
                                        <div class="suggbody">
                                            <div class="suggbody_left">
                                                <h4 class="req_hd">Sally Dona</h4>
                                            </div>
                                            <div class="suggbody_right">
                                                <button class="btn yellow_btn fontupper">Join</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    </div>
                                    <div class="marg5col">
                                    <div class="mediapost_left pull-left bizcomsugg">
                                        <span class="mediapost_pic">
                                            <img src="{PageContext::$response->userImagePath}medium/{PageContext::$response->image}">
                                        </span>
                                    </div>
                                    <div class="media-body pad15top">
                                        <div class="suggbody">
                                            <div class="suggbody_left">
                                                <h4 class="media-heading">Sally Dona</h4>
                                            </div>
                                            <div class="suggbody_right">
                                                <button class="btn yellow_btn fontupper">Join</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    </div>
                                    <div class="marg5col">
                                    <div class="mediapost_left pull-left bizcomsugg">
                                        <span class="mediapost_pic">
                                            <img src="{PageContext::$response->userImagePath}medium/{PageContext::$response->image}">
                                        </span>
                                    </div>
                                    <div class="media-body pad15top">
                                        <div class="suggbody">
                                            <div class="suggbody_left">
                                                <h4 class="media-heading">Sally Dona</h4>
                                            </div>
                                            <div class="suggbody_right">
                                                <button class="btn yellow_btn fontupper">Join</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    </div>
                                    <div class="viewall_link"><a href="#">View All</a></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>-->
                            <!----------------End of bizcom sugestion-------------->
                        </div>
                    </div>
                     {/if}
                    <div class="tab-pane fade" id="myprofile">
                        
                        <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit, accusantium expedita debitis impedit rerum totam id. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus quibusdam recusandae illum, nesciunt, architecto, saepe facere, voluptas eum incidunt dolores magni itaque autem neque velit in. At quia quaerat asperiores.</p>-->
                        <div id="msgDisplay"></div> 
                                <div id="jMyProfile">
                                    <img src="{PageContext::$response->userImagePath}default/loader.gif" /> Loading....
                                </div>
                                <div class="clear"></div>

                                <div class="row">
                                <div class="col-lg-8">
                                    <div class="ibox float-e-margins pad5top">
                                        <div class="ibox-content bordertop_0"  id="jDivDisplayEdit" style="display:none">
                                            <form id="frmprofile_edit">
                                                <div class="form-group"><label class="col-lg-3 control-label">First Name</label>
                                                    <div class="col-lg-9">
                                                        <div class="display_table wid100p">
                                                            <div class="display_table_cell wid98per">
                                                                <input type="text" id="user_firstname" class="form-control valid" value="{PageContext::$response->userDetails['user_firstname']}" placeholder="First Name" name="user_firstname" required="true"> 
                                                            </div>
                                                            <div class="display_table_cell wid2per vlaign_middle align-right">
                                                                <span id="spn_asterisk_cnameadd" style="color:red"> *</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <span class="spn_asterisk" id="spn_asterisk_cnamemsg" style="color:red"></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-3 control-label">Last Name</label>

                                                    <div class="col-lg-9">
                                                        <div class="display_table wid100p">
                                                            <div class="display_table_cell wid98per">
                                                                <input type="text" class="form-control" placeholder="Last Name" size="10" value="{PageContext::$response->userDetails['user_lastname']}" id="user_lastname" name="user_lastname" required="true">
                                                            </div>
                                                            <div class="display_table_cell wid2per vlaign_middle align-right">
                                                                <span id="spn_asterisk_cnameadd" style="color:red"> *</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <span class="spn_asterisk" id="spn_asterisk_clnamemsg" style="color:red"></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-3 control-label">Email </label>

                                                    <div class="col-lg-9">
                                                        <div class="display_table wid100p">
                                                            <div class="display_table_cell wid98per">
                                                                <input type="email" class="form-control" placeholder="Your Email" size="10" value="{PageContext::$response->userDetails['Email']}" id="user_email" name="user_email" required="true">
                                                            </div>
                                                            <div class="display_table_cell wid2per vlaign_middle align-right">
                                                                <span id="spn_asterisk_cnameadd" style="color:red"> *</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                                    
                                                    <span class="spn_asterisk" id="spn_asterisk_emailmsg" style="color:red"></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-3 control-label">Date of Birth</label>

                                                    <div class="col-lg-9">
                                                        <div class="display_table wid100p">
                                                            <div class="display_table_cell wid98per">
                                                               <input type="text" class="form-control " placeholder="Date of Birth" size="10" value="{PageContext::$response->userDetails['Date_of_Birth']}" id="user_date_of_birth" name="user_date_of_birth">
                                                            </div>
                                                            <div class="display_table_cell wid2per vlaign_middle align-right">
                                                                <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-3 control-label">Gender</label>

                                                    <div class="col-lg-9">
                                                        
                                                        <div class="display_table wid100p">
                                                            <div class="display_table_cell wid98per">
                                                               <select class="form-control valid" placeholder="Gender" selected="Array" id="user_gender" name="user_gender" type="select" required="true"><option value="">Select</option><option value="M" {if PageContext::$response->userDetails['Gender'] eq 'M'}selected{/if} >Male</option><option value="F" {if PageContext::$response->userDetails['Gender'] eq 'F'}selected{/if}>Female</option></select>
                                                            </div>
                                                            <div class="display_table_cell wid2per vlaign_middle align-right">
                                                                <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-3 control-label">Address</label>
                                                    <div class="col-lg-9">
                                                        <div class="display_table wid100p">
                                                            <div class="display_table_cell wid98per">
                                                               <textarea class="form-control" placeholder="Address" id="user_address" name="user_address" type="textarea">{PageContext::$response->userDetails['Address']}</textarea>
                                                            </div>
                                                            <div class="display_table_cell wid2per vlaign_middle align-right">
                                                                <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-3 control-label">City</label>
                                                    <div class="col-lg-9">
                                                        <div class="display_table wid100p">
                                                            <div class="display_table_cell wid98per">
                                                               <input type="text" class="form-control" placeholder=" City" size="10" value="{PageContext::$response->userDetails['City']}" id="user_city" name="user_city">
                                                            </div>
                                                            <div class="display_table_cell wid2per vlaign_middle align-right">
                                                                <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-3 control-label">Country</label>
                                                    <div class="col-lg-9">
                                                        <div class="display_table wid100p">
                                                            <div class="display_table_cell wid98per">
                                                               <select class="form-control user_country valid" placeholder="Business Country" selected="Array" id="user_country" name="user_country" type="select" required="true">
                                                                   {foreach from=PageContext::$response->countrydropdown key=id item=country}
                                                                         <option value="{$country}" {if PageContext::$response->userDetails['user_country'] eq $country}selected{/if}>{$id}</option>
                                                                    {/foreach}
                                                                    <!--<option value="AF">Afghanistan</option><option value="AL">Albania</option><option value="DZ">Algeria</option>-->
                                                                </select>
                                                            </div>
                                                            <div class="display_table_cell wid2per vlaign_middle align-right">
                                                                <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-3 control-label">State</label>
                                                    <div class="col-lg-9">
                                                        <div class="display_table wid100p">
                                                            <div class="display_table_cell wid98per">
                                                               <select class="form-control user_state valid" id="user_state" placeholder="Business Country" selected="Array" name="user_state" type="select" required="true">
                                                                </select>
                                                            </div>
                                                            <div class="display_table_cell wid2per vlaign_middle align-right">
                                                                <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-3 control-label">Phone</label>
                                                    <div class="col-lg-9">
                                                        <div class="display_table wid100p">
                                                            <div class="display_table_cell wid98per">
                                                              <input type="text" class="form-control" placeholder="Telephone" size="10" value="{PageContext::$response->userDetails['Phone']}" id="user_phone" name="user_phone">
                                                            </div>
                                                            <div class="display_table_cell wid2per vlaign_middle align-right">
                                                                <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-3 control-label">Zipcode</label>
                                                    <div class="col-lg-9">
                                                        <div class="display_table wid100p">
                                                            <div class="display_table_cell wid98per">
                                                              <input type="text" class="form-control" placeholder="Zip Code" size="10" value="{PageContext::$response->userDetails['Zip_Code']}" id="user_zipcode" name="user_zipcode" required="true">
                                                            </div>
                                                            <div class="display_table_cell wid2per vlaign_middle align-right">
                                                                <span id="spn_asterisk_cnameadd" style="color:red">*</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <span class="spn_asterisk" id="spn_asterisk_zipmsg" style="color:red"></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-3 control-label">Profile View</label>
                                                    <div class="col-lg-9">
                                                        <div class="display_table wid100p">
                                                            <div class="display_table_cell wid98per">
                                                              <select class="form-control user_privacy valid" placeholder="Privacy" selected="Array" id="user_privacy" name="user_privacy" type="select" required="true"><option value="selected">Select</option><option value="SA" {if PageContext::$response->userDetails['User_Privacy'] eq 'SA'}selected{/if}>Show to All</option><option value="H" {if PageContext::$response->userDetails['User_Privacy'] eq 'H'}selected{/if}>Only to Me</option><option value="SM" {if PageContext::$response->userDetails['User_Privacy'] eq 'SM'}selected{/if}>Only to BBF Memebers</option></select>
                                                            </div>
                                                            <div class="display_table_cell wid2per vlaign_middle align-right">
                                                                <span id="spn_asterisk_cnameadd" style="color:red">*</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-3 col-lg-9">
                                                        <input id="user_alias" class=" " type="hidden" placeholder="" size="10" value="{PageContext::$response->alias}" name="user_alias">
                                                        <input type="button" class="btn btn-sm yellow_btn3 marg5right left" value="Update" id="jUpdate" name="btnSubmit"> <input type="button" class="btn btn-sm grey_btn jShowProfile left" alias="{PageContext::$response->alias}" uid="{PageContext::$response->sess_user_id}" value="Cancel" name="btnCancel">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4"></div>
                                </div>




<!--                                <div id="jDivDisplayEdit" style="display:none">
                                    <div class="member_prof_abt">
                                        <div class="member_prof_abt_edit_table">
                                            {PageContext::$response->objForm}
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>-->
                                <div id="tagg-business" style="display:none">
                                <div id="tagform"> 
                                    <h3>BizDirectory</h3>    
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox-content">
                                                <div class="row">
                                                <form name="serachbuisnessform" id="serachbuisnessform" action="" method="GET">
                                                    <div class="form-group"><label class="col-lg-3 control-label">Select BizDirectory</label>
                                                        <div class="col-lg-9">
                                                            <input name="searchtext" id="serachbuisness" type="text" placeholder="Business Name" class="busname form-control" required="true" >
                                                           
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-group"><label class="col-lg-3 control-label">Role</label>
                                                        <div class="col-lg-9">
                                                            <input name="role" id="role" class="rolename form-control" type="text" placeholder="Role">
                                                           
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-group"><label class="col-lg-3 control-label">Email</label>
                                                        <div class="col-lg-9">
                                                            <input name="email" id="email" class="form-control" type="text" placeholder="Email">
                                                            <input type="hidden" name="search_selected_activity_buisness" id="search_selected_activity_buisness" >
                                                            <input type="hidden" name="search_selected_activity_buisness_name" id="search_selected_activity_buisness_name" >
                                                            <input type="hidden" name="search_selected_activity_buisness_alias" id="search_selected_activity_buisness_alias" >
                                                             <input type="hidden" name="search_selected_activity_buisness_image" id="search_selected_activity_buisness_image" >
                                                               <input type="hidden" name="user_image_path" id="user_image_path" value="{php} echo PageContext::$response->userImagePath;{/php}" >
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-lg-offset-3 col-lg-9">
                                                            <input type="submit" name="add" value="Add" class="invite_btn btn btn-sm yellow_btn3" title="invite-block" style="margin-top:0px!important;">
                                                            <input type="hidden" name="tagg_id" value="0" id="tagg_id">
                                                        </div>
                                                    </div>
                                                </form>
                                                </div>
                                            </div>
                                        <section class="businesseditlists">
                                            <div class="row" id="tagList">
                                            {foreach from=PageContext::$response->tagDetails key=id item=lead}
                                            <div class="col-sm-12 col-md-12 col-lg-12" id="tag_list_{$lead->tagg_id}">
                                                <div class="mediapost">
                                                    <div class="mediapost_left pull-left">
                                                        <span class="mediapost_pic">

                                                            <a id="image_link_{$lead->tagg_id}" href="{PageContext::$response->baseUrl}business/{$lead->business_alias}">
                                                                <img src="{php} echo PageContext::$response->userImagePath;{/php}{if $lead->file_path neq ''}thumb/{$lead->file_path}{elseif $lead->file_path eq ''}thumb/noimage.jpg{/if}" alt="" id="image_{$lead->tagg_id}" class="business_profile_desc_colside_pic"> 
                                                            </a>
                                                        </span>
                                                    </div>

                                                    <div class="media-body">
                                                        <h4 class="media-heading"><span><a id="business_{$lead->tagg_id}" href="{PageContext::$response->baseUrl}directory/{$lead->business_alias}">{$lead->business_name}</a></span></h4>
                                                            <div class="display_table">
                                                            <div class="display_table_cell wid70per">
                                                                <p><span id="role_{$lead->tagg_id}">{$lead->role}</span></p>
                                                            </div>
                                                            <div class="display_table_cell wid30per">
                                                                <div class="right">
                                                                    <a class="edititem left marg5right" href="javascript:void(0)" title="Edit" onclick="updateTags('{$lead->tagg_id}','{$lead->business_id}')"><i class="fa fa-pencil"></i></a>
                                                                    <a class="deleteitem left" href="javascript:void(0)"  title="Delete" onclick="deleteTags('{$lead->tagg_id}','{$lead->business_id}')"><i class="fa fa-trash"></i></a> 
                                                                </div>
                                                            </div>
                                                            </div>
                                                            <p><span id="email_id_{$lead->tagg_id}">{$lead->tag_buisness_email}</span></p>
                                                        </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            {/foreach}
                                            </div>
                                        </section>
                                        <div class="clearfix"></div>
<!--
										<div class="bus_prof_outer">
											<form name="serachbuisnessform" id="serachbuisnessform" action="" method="GET">
												<div class="bus_prof_inner">
													<label>Select Business</label>
													<input name="searchtext" id="serachbuisness" class="busname" type="text" placeholder="Business Name">
												</div>
												<div class="bus_prof_inner">
													<label>Role</label>
													<input name="role" id="role" class="rolename" type="text" placeholder="Role">
												</div>
												<div class="bus_prof_inner_1">
													<label>Email</label>
													<input name="email" id="email" class="" type="text" placeholder="Email">
													<input type="hidden" name="search_selected_activity_buisness" id="search_selected_activity_buisness" >
													<input type="submit" name="add" value="Add" class="invite_btn" title="invite-block" style="margin-top:0px!important;">
												</div>
												<input type="hidden" name="tagg_id" value="0" id="tagg_id">
											</form>
                                            <div class="clearfix"></div>
										</div>
-->                                        
                                        </div>
                                    </div>  
                                </div>  
<!--                                
                                <div id="tagList">       
                                    {foreach from=PageContext::$response->tagDetails key=id item=lead}
                                    <div class="list_item_container" id="tag_list_{$lead->tagg_id}">
                                        <div class="bus_profile_blk business_profile_info">
                                            <div class="image"><a href="{PageContext::$response->baseUrl}business/{$lead->business_alias}">
                                                    <img src="{php} echo PageContext::$response->userImagePath;{/php}{if $lead->file_path neq ''}thumb/{$lead->file_path}{elseif $lead->file_path eq ''}thumb/noimage.jpg{/if}">
                                                </a>
                                            </div>
                                            <div class="label"> 
                                                <div class="bus_name_profile ">
                                                    <a id="business_{$lead->tagg_id}" href="{PageContext::$response->baseUrl}business/{$lead->business_alias}">{$lead->business_name}</a>
                                                    <br><span id="role_{$lead->tagg_id}">{$lead->role}</span>
                                                    <br><span id="email_id_{$lead->tagg_id}">{$lead->tag_buisness_email}</span> 
                                                    <div class="clear"></div>
                                                </div>

                                                <div class="left">
                                                    <a href="javascript:void(0)" class="delete_link_business" title="Delete" onclick="deleteTags('{$lead->tagg_id}','{$lead->business_id}')">Delete</a> 
                                                    <a href="javascript:void(0)" class="edit_link_businessprofile" title="Edit" onclick="updateTags('{$lead->tagg_id}','{$lead->business_id}')" style="margin-top:0px; ">Edit</a> 

                                                </div>

                                            </div>  
                                        </div>     
                                    </div>
                                    {/foreach} 
                                </div>
-->                                
                                <div class="clear"></div> 
                            </div>
                            <!--</div>-->
                    </div>
                    <div id="tab-Friends" class="tab-pane fade active in">
                        <div class="row" style="display:none">
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="mediapost">
                                    <div class="mediapost_left pull-left">
                                        <span class="mediapost_pic">
                                            <img src="{PageContext::$response->userImagePath}medium/{PageContext::$response->image}">
                                        </span>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Abel Andrews</h4>
                                        <p>8 Friends</p>
                                        <p><span class="accept"><a href="#"><i class="fa fa-envelope-o"></i> Send message</a></span> <span class="reject"><i class="fa fa-times"></i> <a href="#">Unfriend</a></span></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="mediapost">
                                    <div class="mediapost_left pull-left">
                                        <span class="mediapost_pic">
                                            <img src="{PageContext::$response->userImagePath}medium/{PageContext::$response->image}">
                                        </span>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Abel Andrews</h4>
                                        <p>8 Friends</p>
                                        <p><span class="accept"><a href="#"><i class="fa fa-envelope-o"></i> Send message</a></span> <span class="reject"><i class="fa fa-times"></i> <a href="#">Unfriend</a></span></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="mediapost">
                                    <div class="mediapost_left pull-left">
                                        <span class="mediapost_pic">
                                            <img src="{PageContext::$response->userImagePath}medium/{PageContext::$response->image}">
                                        </span>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Abel Andrews</h4>
                                        <p>8 Friends</p>
                                        <p><span class="accept"><a href="#"><i class="fa fa-envelope-o"></i> Send message</a></span> <span class="reject"><i class="fa fa-times"></i> <a href="#">Unfriend</a></span></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>


<!--                    
            <div class="frnds_blk content" id="tagg-business" style="display:none">
                <div id="tagform"> 
                    <h3>Business</h3>    
                    <div class="bus_catprofileform">
						<div class="bus_prof_outer">
							<form name="serachbuisnessform" id="serachbuisnessform" action="" method="GET">
								<div class="bus_prof_inner">
									<label>Select Business</label>
									<input name="searchtext" id="serachbuisness" class="busname" type="text" placeholder="Business Name">
								</div>
								<div class="bus_prof_inner">
									<label>Role</label>
									<input name="role" id="role" class="rolename" type="text" placeholder="Role">
								</div>
								<div class="bus_prof_inner_1">
									<label>Email</label>
									<input name="email" id="email" class="" type="text" placeholder="Email">
									<input type="hidden" name="search_selected_activity_buisness" id="search_selected_activity_buisness" >
									<input type="submit" name="add" value="Add" class="invite_btn" title="invite-block" style="margin-top:0px!important;">
								</div>
								<input type="hidden" name="tagg_id" value="0" id="tagg_id">
							</form>
						</div>
                    </div>  
                </div>  
                <div id="tagList">       
                    {foreach from=PageContext::$response->tagDetails key=id item=lead}
                    <div class="list_item_container" id="tag_list_{$lead->tagg_id}">
                        <div class="bus_profile_blk business_profile_info">
                            <div class="image"><a href="{PageContext::$response->baseUrl}business/{$lead->business_alias}">
                                    <img src="{php} echo PageContext::$response->userImagePath;{/php}{if $lead->file_path neq ''}thumb/{$lead->file_path}{elseif $lead->file_path eq ''}thumb/noimage.jpg{/if}">
                                </a>
                            </div>
                            <div class="label"> 
                                <div class="bus_name_profile ">
                                    <a id="business_{$lead->tagg_id}" href="{PageContext::$response->baseUrl}business/{$lead->business_alias}">{$lead->business_name}</a>
                                    <br><span id="role_{$lead->tagg_id}">{$lead->role}</span>
                                    <br><span id="email_id_{$lead->tagg_id}">{$lead->tag_buisness_email}</span> 
                                    <div class="clear"></div>
                                </div>

                                <div class="left">
                                    <a href="javascript:void(0)" class="delete_link_business" title="Delete" onclick="deleteTags('{$lead->tagg_id}','{$lead->business_id}')">Delete</a> 
                                    <a href="javascript:void(0)" class="edit_link_businessprofile" title="Edit" onclick="updateTags('{$lead->tagg_id}','{$lead->business_id}')" style="margin-top:0px; ">Edit</a> 

                                </div>

                            </div>  
                        </div>     
                    </div>
                    {/foreach} 
                </div>
                <div class="clear"></div> 
            </div>
-->
            <div class="clear"></div>
            <div class="listitem loader">
                <img src="{PageContext::$response->userImagePath}default/loader.gif" /> Loading....
            </div>
            </div>
        </div>
    </div>
</div>
</div>

                            <!-----------------------Friends end--------------------------->
                            </div>
                            <div class="clear"></div>
                            

                            </div>
                