<link type="text/css" rel="stylesheet" href="<?php echo BASE_URL?>project/styles/popup.css">

<link type="text/css" rel="stylesheet" href="<?php echo BASE_URL?>project/styles/style.css">

<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery-1.8.0.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>bbf/public/js/jquery-ui-1.8.23.custom.min.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/jquery.blockUI.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>public/js/fw.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.metadata.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.validate.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/app.js"> </script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/jquery.colorbox.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/comment_popup.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL?>project/js/community.js"></script>

<div class="show_message_outer">
	<h1><?php echo stripslashes(PageContext::$response->message_subject);?></h1>
	<p><?php echo stripslashes(PageContext::$response->message_detail);?></p>
</div>