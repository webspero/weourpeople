<script>
    $(document).ready(function() {
        console.info('TOTAL_PAG '+TOTAL_PAG);
        var page = 1;
       	var _throttleTimer = null;
	    var _throttleDelay = 100;
        $(window ).scroll(function() {
            clearTimeout(_throttleTimer);
            _throttleTimer = setTimeout(function () {
            if($(window).scrollTop() + $(window).height() > $(document).height()-100) {
                if(page < TOTAL_PAG){
                    console.info('Current Page '+page);
                    page++;
                    console.info('Next Page '+page);
                    $.blockUI({ message: '' });
                    $.ajax({
                        type    : 'Post',
                        url     : mainUrl+'user/organisations_listajax/'+page,
                        cache   : false,
                        success :function(data){
                            console.info('HTML Data');
                            console.log(data);
                            $('.cls_section').append(data);
                            $('.listitem').fadeIn(3000);
                            $('.listitem').hide();
                        }
                    })
                }
            }
            },_throttleDelay);
	    });
    });
</script>

<div class="container">
    <section class="inbox_section whitebox">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">

                <h3>Pages <span class="round-search">{PageContext::$response->total}</span>
                    {if PageContext::$response->sess_user_id neq 0}
                    <span class="addbusiness"><a
                            href="{php} echo PageContext::$response->baseUrl;{/php}add-page">Add Page</a></span>
                    {/if}
                </h3>
                <div class="businesslist_search_blk">
                    <div class="row">
                        <div class="col-sm-6 col-md-8 col-lg-8">
                            <input type="hidden" value="{PageContext::$response->sess_user_id}" class="login_status"
                                   name="" id="login_status">
                            Sort by : A-Z <a
                                href="{php} echo PageContext::$response->baseUrl; {/php}pages/{PageContext::$response->category_id}?sort=business_name&sortby={PageContext::$response->order}&searchtext={php} echo PageContext::$response->searchtext; {/php}&searchtype={php} echo PageContext::$response->searchtype; {/php}&page={php} echo PageContext::$response->page; {/php}">{PageContext::$response->business_name_sortorder}</a>
                           
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            {php}PageContext::renderRegisteredPostActions('searchbox');{/php}
                        </div>
                    </div>
                </div>
            </div>
            <section class="cls_section">
                {if PageContext::$response->total > 0}
                {foreach from=PageContext::$response->list key=id item=category}

                <div class="col-sm-6 col-md-6 col-lg-3">
                    <div class="cateforylist_items">
                        <div class="wid100p position_relatv">
                            <div class="pageslisting_img" style="background-image: url('{PageContext::$response->userImagePath}{$category->file_path}');">  </div>
                            {if $category->business_status =='I'}    
                        <span class="status clserror" >Pending Approval</span> 
                            {/if}
                            <div class="pageslisting_actions">
                                    {if $category->business_created_by eq PageContext::$response->sess_user_id}
                                    <span class="pull-left">
                                    <a class="edititem left marg5right"
                                       href="{php} echo PageContext::$response->baseUrl;{/php}edit-page/{$category->business_id}"><i
                                            class="fa fa-pencil"></i></a>
                                    <a class="deleteitem left" onclick="return deleteConfirm();"
                                       href="{php} echo PageContext::$response->baseUrl;{/php}index/delete_organization/{$category->business_id}"><i
                                            class="fa fa-trash"></i></a>
                                    </span>
                                    {/if}
                                    <div class="clearfix"></div>
                            </div>
                        </div>
                            <div class="wid100per display_block">
                                <a href="{PageContext::$response->baseUrl}page/{$category->business_alias}">{$category->business_name}</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                    </div>
                </div>

                {/foreach}
                {else}
                <div class="categorylist_row rownoborder">
                    <div class="col-sm-12 col-md-12 col-lg-12">{if PageContext::$response->searchtype eq ''}No
                        pages added.{else}No matches found.{/if}
                        {if PageContext::$response->sess_user_id neq 0 and PageContext::$response->sess_user_status neq
                        'I'}Click to <a href="{php} echo PageContext::$response->baseUrl;{/php}add-page">add a
                            page</a>. {/if}
                    </div>
                </div>
                {/if}
            </section>
        </div>
    </section>
</div>





