<script>
    $(document).ready(function () {
        var page = 1;
        var _throttleTimer = null;
        var _throttleDelay = 100;
        $(window).scroll(function () {
            clearTimeout(_throttleTimer);
            _throttleTimer = setTimeout(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                    if (page < TOTAL_PAGES) {
                        page++;
                        $.blockUI({message: ''});
                        var business_id = $("#business_id").val();
                        $.ajax({
                            type: 'Post',
                            url: mainUrl + 'user/view_organization_associated_peopleajax/' + page + '/' + business_id,
                            data: 'business_id=' + business_id,
                            cache: false,
                            success: function (data) {
                                $('.margin_l_1').append(data);
                                $('.listitem').fadeIn(3000);
                                $('.listitem').hide();
                            }
                        })
                    }
                }
            }, _throttleDelay);
        });
        magnificPopupGroupFn();
    });
</script>
<div class="col-sm-4 col-md-5 col-lg-3">   
    <?php if(count(PageContext::$response->friend_display_list) > 0){?>            
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-user" aria-hidden="true"></i> Followers <a  href="#" id="jMoreOrgphotos" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist">
                    <?php foreach(PageContext::$response->friend_display_list as  $key=>$friendslist){
                       // echopre($friendslist);?>
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <a class="friendphotobox_blk" href="<?php echo PageContext::$response->baseUrl;?>timeline/<?php echo $friendslist->user_alias;?>">
                                <?php if($friendslist->file_path) {?>
                                <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?><?php echo $friendslist->file_path;?>');">
                                </div>
                                <?php }else{ ?>
                                 <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>medium/member_noimg.jpg');">
                                </div>
                                <?php } ?>
                                <h4><?php echo ucfirst($friendslist->user_firstname)." ".ucfirst($friendslist->user_lastname);?></h4>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
                </div>  
   <?php } ?>
    <?php if(count(PageContext::$response->gallery_display_list) > 0){   ?>
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-file-image-o" aria-hidden="true"></i> Photos <a href="#" id="jMoreOrgGallery" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist portfolio">
                     <?php foreach(PageContext::$response->gallery_display_list as  $key=>$gallerylist){
                        ?>
                     
<!--                    <li class="display_table_cell wid32per">
                        <a class="friendphotobox_blk" href="#">
                            <div class="friendphotobox" style="background-image: url('{PageContext::$response->userImagePath}medium/{$gallerylist->news_feed_image_name}');">-->
                                
<!--                            <ul class="portfolio" class="clearfix">-->
                                    <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4 vert_align_top"><a class="friendphotobox_blk" href="<?php echo PageContext::$response->userImagePath;?><?php echo $gallerylist->organization_announcement_image_path;?>" title=""><img src="<?php echo PageContext::$response->userImagePath;?><?php echo $gallerylist->organization_announcement_image_path;?>" alt=""></a></li>
<!--                            </ul>-->
                                
<!--                            </div>
                        </a>
                    </li>-->
                     <?php } ?> 
                </ul>
                <!--<div class="sidead" style="background-image: url('{PageContext::$response->ImagePath}sidead.jpg');"> </div>-->
            </div>  
    <?php } ?>   
    
</div>
<div class="col-sm-8 col-md-7 col-lg-9">
<section class="whitebox">
<div class="bizcom_h3">
<h3> People Associated with <?php echo PageContext::$response->business_name;?></h3>
</div>
<input type="hidden" id="business_id" value="<?php echo PageContext::$response->business_id;?>">
     <div id="tagged_people"> 
        <div class="row">
            <?php if (sizeof(PageContext::$response->associates)== 0){?>
            <div class="col-sm-12 col-md-12 col-lg-12">
                No Users Associated So Far
            </div>
            <?php
            }
            else{
            foreach(PageContext::$response->associates AS $id=>$user){?>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="mediapost">
                                <div class="picpost_left pull-left">
                                        <span class="picpost_left_pic">
                                            <a href="<?php echo PageContext::$response->baseUrl . 'timeline/' . $user->user_alias ?>">
                                                <img
                                                    src="<?php echo ($user->file_path == '') ? PageContext::$response->userImagePath . 'thumb/member_noimg.jpg' : PageContext::$response->userImagePath .  $user->file_path; ?>">
                                            </a>                                        
                                        </span>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading"><a
                                            href="<?php echo PageContext::$response->baseUrl . 'timeline/' . $user->user_alias ?>"><?php echo $user->user_firstname . ' ' . $user->user_lastname ?></a>
                                    </h4>
                                    <p><?php echo $user->role ?></p>
                                    
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    <?php } ?>
                    <!--            <div class="clear"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">-->
                    <div><?php echo PageContext::$response->paginationAffilate; ?> </div>
                <?php } ?>
                <!--            </div>-->
            </div>
        </div>
        <!-- <div class="clear"></div>-->
    </section>
</div>