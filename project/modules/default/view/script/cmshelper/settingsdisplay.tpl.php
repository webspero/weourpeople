<?php
$tab = $_REQUEST['tab'];
if($tab=='') $tab = 'general';
?>
<script type="text/javascript">
    $(function(){
        var tab = '<?php echo $tab ?>';
        //$('#settingtab a:first').tab('show');
        $('#settingtab a[href="#'+tab+'"]').tab('show');
    });
</script>
<div class="section_list_view ">
    <div class="row have-margin">
        <span class="legend">Section : Settings </span>

        <?php if (!empty(PageContext::$response->message)) { ?>
        <div class="alert alert-<?php echo PageContext::$response->successError; ?>">
            <button class="close" data-dismiss="alert" type="button">x</button>
                <?php echo PageContext::$response->message; ?>
        </div>
            <?php } ?>

        <div class="input-append pull-right">

        </div>
    </div>
    <ul id="settingtab" class="nav nav-tabs">
        <li class=""><a data-toggle="tab" href="#general">General</a></li>
        <li class=""><a data-toggle="tab" href="#payment">Payment</a></li>
        <li class=""><a data-toggle="tab" href="#advanced">Advanced Settings</a></li>
        <!--li class=""><a data-toggle="tab" href="#social">Social Settings</a></li-->
        <?php if(PageContext::$response->advancedSettings['CancelType']->vLookUp_Value == '1') {
            $penaltyTabDisplay = 'inline';
        }  else {
            $penaltyTabDisplay = 'none';
        }?>
        <?php if(PageContext::$response->advancedSettings['AllowCancel']->vLookUp_Value == '1') { ?>
        <li class="" id="jqPenaltyTab" style="display:<?php echo $penaltyTabDisplay;?>"><a data-toggle="tab" href="#penalty">Penalty</a></li>
            <?php
        } ?>
        <li class=""><a data-toggle="tab" data-target="#password" href="#password">Update Password</a></li>
    </ul>
    <div class="tab-content">

        <div id="general" class="tab-pane">

            <?php      //  echo "<pre>";
            //print_r( PageContext::$response->genSettings);
            ?>
            <form name="generalForm" id="jqGeneralForm" method="post" enctype="multipart/form-data" class="form-horizontal" action="<?php echo BASE_URL; ?>cms?section=settings&tab=general">


                <div class="boxborder">
                    <div class="control-group">
                        <label for="sitename" class="control-label">License Key</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->genSettings['vLicenceKey']->vLookUp_Value; ?>"  name="licenseKey"  class="float-left">
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="The license key you recieve  after purchasing the product."></a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="sitename" class="control-label">Site Name</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->genSettings['sitename']->vLookUp_Value; ?>"  name="sitename"  class="float-left">
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Name your site. This name is the website title. This will be visible
                               to users within the browser tab. Try to use keywords describing your site to
                               help visitors understand your page quickly."></a>
                        </div>
                    </div>

                    <!--                <div class="control-group">-->
                    <!--                    <label class="control-label" for="siteTitle">Site Title</label>-->
                    <!--                    <div class="controls">-->
                    <!--                        <input type="text" value="<?php echo PageContext::$response->genSettings['metaTitle']->vLookUp_Value; ?>" name="siteTitle">-->
                    <!--                    </div>-->
                    <!--                </div>-->

                    <div class="control-group">
                        <label for="secureURL" class="control-label">Secure URL</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->genSettings['vsecure_server']->vLookUp_Value; ?>" name="vsecure_server" class="float-left"  ></div>
                        <a href="javascript:void(0)" class="tooltiplink help-icon" title="This is for SSL certificates only. This information is provided
                           by your hosting company."></a>
                    </div>

                    <div class="control-group">
                        <label for="vsite_url" class="control-label">Site URL</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->genSettings['vsite_url']->vLookUp_Value; ?>"  name="vsite_url" class="float-left"  ></div>
                        <a href="javascript:void(0)" class="tooltiplink help-icon" title="This is the URL of your site."></a>
                    </div>


                    <div class="control-group">
                        <label for="adminEmail" class="control-label">Admin Email</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->genSettings['vadmin_email']->vLookUp_Value; ?>"  name="vadmin_email" class="float-left">
                            <input type="hidden" name="addressfromemail">
                            <input type="hidden" name="addressfromemailname">
                            <input type="hidden" name="addressreplyemail" >
                            <input type="hidden" name="addressreplyemailname" ></div>
                        <a href="javascript:void(0)" class="tooltiplink help-icon" title="This is the email where all site notifications, alerts and
                           approvals will be sent."></a>
                    </div>


                    <div class="control-group">
                        <label for="metaKeywords" class="control-label">Meta Title</label>
                        <div class="controls">
                            <textarea name="metaTitle" class="float-left"><?php echo PageContext::$response->genSettings['metaTitle']->vLookUp_Value; ?></textarea></div>
                        <a href="javascript:void(0)" class="tooltiplink help-icon" title="The contents of this field will appear in several places including the
                           title of the website."></a>
                    </div>

                    <div class="control-group">
                        <label for="metaKeywords" class="control-label">Default Meta Keywords</label>
                        <div class="controls">
                            <textarea name="metatagKey" class="float-left"><?php echo PageContext::$response->genSettings['metatagKey']->vLookUp_Value; ?></textarea></div>
                        <a href="javascript:void(0)" class="tooltiplink help-icon" title="Meta Keywords, or tags, are additional copy included in
                           your website's HTML. (1) keep your list of keywords or keyword phrases
                           down to 10 - 15 unique words or phrases; (2) separate the words or
                           phrases using a comma (you do not need to leave a space between words
                           separated by commas); (3) do not repeat words or phrases; (4) put your
                           most important word or phrases at the beginning of your list."></a>
                    </div>

                    <div class="control-group">
                        <label for="metaDescription" class="control-label">Default Meta Description</label>
                        <div class="controls">
                            <textarea name="metatagDes" class="float-left"><?php echo PageContext::$response->genSettings['metatagDes']->vLookUp_Value; ?></textarea></div>
                        <a href="javascript:void(0)" class="tooltiplink help-icon" title="The meta description is a meta tag that is used to describe
                           the current Web page. It provides a short summary of the page so that
                           readers can decide whether the page is what they are looking for or looks
                           interesting."></a>
                    </div>
                </div>

                <div class="boxborder">
                    <?php
                    $checked = '';
                    if(PageContext::$response->genSettings['bookmark_enable']->vLookUp_Value == 'Yes') {
                        $checked = "checked='checked'";
                    }
                    ?>
                    <div class="control-group">
                        <label for="bookmark_enable" class="control-label">Enable Social Bookmark</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo $checked; ?>  name="bookmark_enable" class="jqToggle float-left" value="Yes" id="jqBookmark" ><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You can enable / disable social book mark in site."></a>

                        </div>
                    </div>
                    <div class="control-group jqBookmark">
                        <label for="bookmark_value" class="control-label">Bookmark Value</label>
                        <div class="controls">
                            <textarea name="bookmark_value" class="float-left"><?php echo htmlspecialchars(PageContext::$response->genSettings['bookmark_value']->vLookUp_Value); ?></textarea>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Enter the value for Bookmarking"></a>
                        </div>
                    </div>
                </div>

                <div class="boxborder">
                    <?php
                    $checked = '';
                    if(PageContext::$response->genSettings['recaptcha_enable']->vLookUp_Value == 'Y') {
                        $checked = "checked='checked'";
                    }
                    ?>
                    <div class="control-group">
                        <label for="recaptcha_enable" class="control-label">Enable Recaptcha</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo $checked; ?>  name="recaptcha_enable" class="jqToggle float-left" value="Y" id="jqRecaptcha" ><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You can enable / disable recaptcha in site."></a>

                        </div>
                    </div>
                    <div class="control-group jqRecaptcha">
                        <div class="control-group">
                            <label for="recaptcha_public_key" class="control-label">Recaptcha Public Key</label>
                            <div class="controls">
                                <textarea name="recaptcha_public_key" class="float-left"><?php echo htmlspecialchars(PageContext::$response->genSettings['recaptcha_public_key']->vLookUp_Value); ?></textarea>
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Enter Recaptcha Public Key"></a>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="recaptcha_private_key" class="control-label">Recaptcha Private Key</label>
                            <div class="controls">
                                <textarea name="recaptcha_private_key" id="jqAdsenseValue" validate="required:validateFieldsWithCheckbox('jqAdsense','jqAdsenseValue')" class="float-left"><?php echo htmlspecialchars(PageContext::$response->genSettings['recaptcha_private_key']->vLookUp_Value); ?></textarea>
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Enter Recaptcha Private Key"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="boxborder">
                    <?php
                    $checked = '';
                    if(PageContext::$response->genSettings['calendar_enable']->vLookUp_Value == 'Y') {
                        $checked = "checked='checked'";
                    }
                    ?>
                    <div class="control-group">
                        <label for="calendar_enable" class="control-label">Enable Calendar</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo $checked; ?>  name="calendar_enable" class="jqToggle float-left" value="Y" id="jqCalendar" ><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You can enable / disable calendar in site."></a>
                        </div>
                    </div>
                </div>

                <div class="boxborder">
                    <?php
                    $checked = '';
                    if(PageContext::$response->genSettings['social_share_enable']->vLookUp_Value == 'Y') {
                        $checked = "checked='checked'";
                    }
                    ?>
                    <div class="control-group">
                        <label for="social_share_enable" class="control-label">Enable Social Media Shares</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo $checked; ?>  name="social_share_enable" class="jqToggle float-left" value="Y" id="jqSocialShare" ><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You can enable / disable social media shares shows in the location details page in the site."></a>
                        </div>
                    </div>
                    <div class="control-group jqSocialShare">
                        <label for="fb_app_id" class="control-label">Facebook App ID</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->genSettings['fb_app_id']->vLookUp_Value; ?>" name="fb_app_id" class="float-left">
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="If you have a facebook app, enter the app id here for facebook shares which is in the location details page"></a>
                        </div>
                    </div>

                </div>


                <!--
 			 <div class="boxborder">
                <?php
                $checked = '';
                if(PageContext::$response->genSettings['affiliate_enable']->vLookUp_Value == 'Y') {
                    $checked = "checked='checked'";
                }
                ?>
                                    <div class="control-group">
                                        <label for="affilaiate_enable" class="control-label">Enable Affiliate</label>
                                        <div class="controls">
                                            <input type="checkbox" <?php echo $checked; ?>  name="affiliate_enable" class="jqToggle float-left" value="Y" id="jqAffiliate" ><span class="help-inline"></span>
                                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You can enable / disable Affiliates."></a>
                                        </div>
                                    </div>
                                    <div class="control-group jqAffiliate">
                                        <label class="control-label" for="siteTitle">Affiliate Commission</label>
                                        <div class="controls">
                                            <input type="text" value="<?php echo PageContext::$response->genSettings['affiliate_percentage']->vLookUp_Value; ?>" name="affiliate_percentage" class="float-left">

                                        </div>
                                    </div>

                                </div>

                -->








                <div class="boxborder">
                    <div class="control-group">
                        <label class="control-label" for="siteTitle">Number of records per page</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->genSettings['perpage']->vLookUp_Value; ?>" name="perpage" class="float-left">

                        </div>
                    </div>


                    <div class="control-group">
                        <label for="companyName" class="control-label">Company Name</label>
                        <div class="controls">
                            <input type="text" name="companyName"  class="float-left" value="<?php echo htmlspecialchars(PageContext::$response->genSettings['companyName']->vLookUp_Value); ?>">
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Name of your company"></a>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="admin_address" class="control-label">Company Address</label>
                        <div class="controls">
                            <textarea name="admin_address"  class="float-left"><?php echo htmlspecialchars(PageContext::$response->genSettings['admin_address']->vLookUp_Value); ?></textarea>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Address for offline payments like Check, Bank Transfer etc."></a>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="companyPhone" class="control-label">Company Phone No.</label>
                        <div class="controls">
                            <input type="text" name="companyPhone"  class="float-left" value="<?php echo htmlspecialchars(PageContext::$response->genSettings['companyPhone']->vLookUp_Value); ?>">
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Phone number that will be displayed in various sections like invoice"></a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="companySite" class="control-label">Company Website</label>
                        <div class="controls">
                            <input type="text" name="companySite"  class="float-left" value="<?php echo htmlspecialchars(PageContext::$response->genSettings['companySite']->vLookUp_Value); ?>">
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="URL of your website"></a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="siteTitle">Maximum Number of Images</label>
                        <div class="controls">
                            <input type="text" class="float-left" value="<?php echo PageContext::$response->genSettings['max_num_img']->vLookUp_Value; ?>" name="max_num_img">
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Maximum number of images displayed in Location page at a time"></a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="admin_address" class="control-label">Allowed Image Types(Comma Separated, no periods)</label>
                        <div class="controls">
                            <textarea name="allowedimagetypes"  class="float-left"><?php echo htmlspecialchars(PageContext::$response->genSettings['allowedimagetypes']->vLookUp_Value); ?></textarea>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Type of Images supported"></a>

                        </div>
                    </div>


                    <!--    <div class="control-group">
                                <label for="gcheck_server_type" class="control-label">User Approval Type </label>
                                <div class="controls">
                                    <select name="userapproval" class="float-left">
                                        <option value="AU" <?php echo (PageContext::$response->genSettings['userapproval']->vLookUp_Value=='AU')?'selected':'';?>>Admin Approval</option>
                                        <option value="A" <?php echo (PageContext::$response->genSettings['userapproval']->vLookUp_Value=='A')?'selected':'';?>>Automatic Approval</option>
                                        <option value="E" <?php echo (PageContext::$response->genSettings['userapproval']->vLookUp_Value=='E')?'selected':'';?>>Email Approval</option>
                                    </select>
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Based on the selcted value, Customer registration will be approved."></a>
                                </div>
                            </div>-->
                </div>

                <div class="boxborder">


                    <?php

                    $checked = '';
                    if(PageContext::$response->genSettings['GoogleMap']->vLookUp_Value == 'Y') {
                        $checked = "checked='checked'";
                    }
                    ?>
                    <div class="control-group">
                        <label for="GoogleMap" class="control-label"> Enable Google Map In User Location Details</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo $checked; ?>  name="GoogleMap" class="jqToggle" value="Y" id="JqGoogleMap"><span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group JqGoogleMap">
                        <label class="control-label" for="GoogleMapValue"> Google Map Value</label>
                        <div class="controls">
                            <textarea  name="GoogleMapValue" class="float-left"><?php echo PageContext::$response->genSettings['GoogleMapValue']->vLookUp_Value; ?></textarea>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="This is the Google Map API key obtained while registering the product.This is for rendering map functionality in the user side"></a>
                        </div>
                    </div>
                </div>

                <div class="boxborder">
                    <?php

                    $checked = '';
                    if(PageContext::$response->genSettings['googleaddemo']->vLookUp_Value == 'Y') {
                        $checked = "checked='checked'";
                    }
                    ?>
                    <div class="control-group">
                        <label for="googleaddemo" class="control-label"> Enable Google Advertisement</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo $checked; ?>  name="googleaddemo" class="jqToggle" value="Y" id="jqAdsense"><span class="help-inline"></span>
                        </div>
                    </div>

                    <div class="control-group jqAdsense">
                        <label for="googleadvalue" class="control-label">Invocation Code for Advertisement</label>
                        <div class="controls">
                            <textarea name="googleadvalue" class="float-left"><?php echo htmlspecialchars(PageContext::$response->genSettings['googleadvalue']->vLookUp_Value); ?></textarea>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You can add advertisements from third party networks
                               like Google Adsense,Adbrite,OpenX,Commission junction etc."></a>

                        </div>
                    </div>
                </div>


                <div class="boxborder">
                    <?php

                    $checked = '';
                    if(PageContext::$response->genSettings['feedname']->vLookUp_Value == 'Yes') {
                        $checked = "checked='checked'";
                    }
                    ?>
                    <div class="control-group">
                        <label for="bookmark_enable" class="control-label">Enable RSS Feed</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo $checked; ?>  name="feedname"   value="Yes" class="float-left"><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You can enable/ disable Rss feed in site"></a>

                        </div>
                    </div>


                    <div class="control-group">
                        <label for="gcheck_server_type" class="control-label">Outgoing Email Format</label>
                        <div class="controls">
                            <select name="email_type" class="float-left">
                                <option value="html" <?php echo (PageContext::$response->genSettings['email_type']->vLookUp_Value=='html')?'selected':'';?>>HTML</option>
                                <option value="text" <?php echo (PageContext::$response->genSettings['email_type']->vLookUp_Value=='text')?'selected':'';?>>Text</option>

                            </select>
                            <!--   <a href="javascript:void(0)" class="tooltiplink help-icon" title="Name your site. This name is the website title. This will be visible to users within the browser tab. Try to use keywords describing your site to help visitors understand your page quickly."></a>-->

                        </div>
                    </div>



                    <div class="control-group">
                        <label for="gcheck_server_type" class="control-label">Custom Message With Booking Email</label>
                        <div class="controls">
                            <textarea name="admin_booking_message"     class="float-left"><?php echo htmlspecialchars(PageContext::$response->genSettings['admin_booking_message']->vLookUp_Value); ?></textarea>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="This is the message that send along with the booking email. You can add details like checkin information, checkin timings etc."></a>

                        </div>
                    </div>







                    <div class="control-group">
                        <label for="sitestyle" class="control-label">Site Style</label>
                        <div class="controls">
                            <?php echo $styleBox = Cmshelper::ShowTemplates(PageContext::$response->genSettings['sitestyle']->vLookUp_Value); ?>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You may select the color scheme of your site here. Choose one of
                               the preloaded templates to change the look and feel of your site instantly."></a>
                        </div>
                    </div> 

                    <div class="control-group ">
                        <label for="streamsend_enable" class="control-label">Site Logo</label>
                        <div class="controls">
                            <input type="file" name="sitelogo" id="sitelogo" class="float-left"/>

                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="This is where you can change the logo of the site. Click Browse to
                               upload a file saved on your computer."></a>
                            <p class="float-left" style="width:800px">
                                <?php
                                echo PageContext::$response->genSettings['sitelogo']->vLookUp_Value;
                                //if(is_file(BASE_PATH . 'project/styles/images/' . PageContext::$response->pageContents[5]->value)){ ?>
                            </p>
                            <ul class="thumbnails">
                                <li class="span3">
                                    <a class="thumbnail" href="#">
                                        <img width="px" height="px" src="<?php echo BASE_URL . 'project/styles/images/' . PageContext::$response->genSettings['sitelogo']->vLookUp_Value; ?>" />
                                    </a>
                                </li>
                            </ul>
                            <span class="mandatory">For best display, size should be 283 x 67</span>
                            <?php //} ?>

                        </div>

                    </div>





                </div>
                <div class="boxborder">
                    <?php

                    $checked = '';
                    if(PageContext::$response->genSettings['expedia_enable']->vLookUp_Value == 'Y') {
                        $checked = "checked='checked'";
                    }
                    ?>
                    <div class="control-group">
                        <label for="expedia_enable" class="control-label"> Enable Expedia Search</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo $checked; ?>  name="expedia_enable" class="jqToggle float-left" value="Y" id="jqExpedia"   ><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You can enable/ disable Expedia search"></a>
                        </div>
                    </div>

                    <div class="control-group jqExpedia">
                        <div class="control-group">
                            <label for="expedia_version" class="control-label">Expedia Version</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->genSettings['expedia_version']->vLookUp_Value; ?>" name="expedia_version" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="The version of the expedia API application"></a>

                            </div>
                        </div>

                        <div class="control-group">
                            <label for="expedia_cid" class="control-label">Expedia CID Value</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->genSettings['expedia_cid']->vLookUp_Value; ?>" name="expedia_cid" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Expedia affiliate account CID "></a>

                            </div>
                        </div>
                        <div class="control-group">
                            <label for="expedia_apikey" class="control-label">Expedia API Key</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->genSettings['expedia_apikey']->vLookUp_Value; ?>" name="expedia_apikey" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Expedia API Key"></a>

                            </div>
                        </div>
                        <div class="control-group">
                            <label for="expedia_currency" class="control-label">Expedia Currency Code</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->genSettings['expedia_currency']->vLookUp_Value; ?>" name="expedia_currency" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Currency format for the API"></a>

                            </div>
                        </div>

                        <div class="control-group">
                            <label for="expedia_search_mode" class="control-label">Expedia Search Mode</label>
                            <div class="controls">
                                <select name="expedia_search_mode" class="float-left">
                                    <option value="1" <?php echo (PageContext::$response->genSettings['expedia_search_mode']->vLookUp_Value=='1')?'selected':'';?>>Enable search only if there is no location data available for a search</option>
                                    <option value="2" <?php echo (PageContext::$response->genSettings['expedia_search_mode']->vLookUp_Value=='2')?'selected':'';?>>Enable search data along with the location search results</option>
                                    <option value="3" <?php echo (PageContext::$response->genSettings['expedia_search_mode']->vLookUp_Value=='3')?'selected':'';?>>Show Expedia results only</option>

                                </select>
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Search mode of the Expedia API"></a>

                            </div>
                        </div>

                    </div>


                </div>













                <div class="boxborder">
                    <div class="control-group">
                        <label class="control-label">Follow Us Facebook Link</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->genSettings['followlink_facebook']->vLookUp_Value; ?>" name="followlink_facebook" class="float-left">
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Enter the full url of the facebook fan page"></a>

                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Follow Us Twitter Link</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->genSettings['followlink_twitter']->vLookUp_Value; ?>" name="followlink_twitter" class="float-left">
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Enter the full url of the Twitter account"></a>

                        </div>
                    </div>



                    <div class="control-group">
                        <label class="control-label">Follow Us LinkedIn Link</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->genSettings['followlink_linkedin']->vLookUp_Value; ?>" name="followlink_linkedin" class="float-left">
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Enter the full url of the LinkedIn profile"></a>

                        </div>
                    </div>
                </div>







































                <!--               need to add


                vadmin_user


                pemfolder
                vLicenceKey
                email_logo


                -->



                <?php
                /*
                $checked = '';
                if(PageContext::$response->pageContents[4]->value == 'Y'){
                    $checked = "checked='checked'";
                } */
                ?>
                <!--
                <div class="control-group">
                    <label for="streamsend_enable" class="control-label">Enable Site Banner</label>
                    <div class="controls">
                        <input type="checkbox" <?php //echo $checked; ?> name="enablesiteBanner" class="jqToggle" id="jqBanner" value="Y"><span class="help-inline"></span>
                    </div>
                </div>

                <div class="control-group jqBanner">
                    <label for="streamsend_enable" class="control-label">Banner Image</label>
                    <div class="controls">
                        <input type="file" name="siteBanner" id="siteBanner" />
                        <p/>
                <?php //if(is_file(BASE_PATH . 'project/styles/images/' . PageContext::$response->pageContents[5]->value)){ ?>
                            <img src="<?php //echo BASE_URL . 'project/styles/images/' . PageContext::$response->pageContents[5]->value; ?>" />
                <?php //} ?>
                    </div>

                </div>

                <div class="control-group jqBanner">
                    <label for="banner_link" class="control-label">Banner Link</label>
                    <div class="controls">
                        <input type="text" name="banner_link" id="jqBannerLink" value="<?php echo PageContext::$response->pageContents[14]->value; ?>" validate="required:validateFieldsWithCheckbox('jqBanner','jqBannerLink')" >
                    </div>
                </div>
                -->








                <div class="controls">
                    <input type="submit" name="submitBtn" value="Save" class="submitButton btn" />
                </div>
            </form>
        </div>


        <!-- Payment -->
        <div id="payment" class="tab-pane">
            <form name="paypalForm" id="jqPaypalForm" method="post" enctype="multipart/form-data"  class="form-horizontal" action="<?php echo BASE_URL; ?>cms?section=settings&tab=payment">






                <div class="boxborder">
                    <div class="control-group">
                        <label for="enablepaypal" class="control-label">Get A New PayPal Account</label>
                        <div class="controls">
                            <a target="_blank" href="https://www.paypal.com/us/mrb/pal=NCGEZ2VF6YL62"><img width="77" height="30" border="0" class="float-left" title="Start A New Account" alt="Start A New Account" src="<?php echo BASE_URL; ?>project/styles/images/paypal_small.jpg"></a><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="To start a new paypal account click on the paypal logo."></a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="enablepaypal" class="control-label">Enable Paypal</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo (PageContext::$response->paySettings['cenable_paypal']->vLookUp_Value == 'Y')?'checked':''; ?> name="cenable_paypal" value="Y" class="float-left jqToggle " id="jqEnablePaypal"><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Enable /disable paypal payment on your site."></a>
                        </div>
                    </div>
                    <div class="control-group jqEnablePaypal">
                        <div class="control-group">
                            <label class="control-label" for="enablepaypalsandbox">Payal Test Mode</label>
                            <div class="controls">
                                <input type="checkbox" <?php echo (PageContext::$response->paySettings['vpaypal_test_mode']->vLookUp_Value == 'YES')?'checked':'';?> name="vpaypal_test_mode" value="YES" class="float-left"><span class="help-inline"> Check To Make Paypal Sandbox/Test Mode (Uncheck To Make Paypal Live)</span>
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="switch between live and test mode."></a>

                            </div>
                        </div>
                        <!-- 
                        <div class="control-group">
                            <label for="vpaypalidentitytoken" class="control-label">Paypal Identity Token</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->paySettings['vpaypalidentitytoken']->vLookUp_Value; ?>" name="vpaypalidentitytoken" id="vpaypalidentitytoken" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Provide your paypal identity token generated from paypal.com."></a>

                            </div>

                        </div>
                        -->
                        <div class="control-group">
                            <label for="vpaypalemail" class="control-label">Paypal Email</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->paySettings['vpaypalemail']->vLookUp_Value; ?>" name="vpaypalemail" id="jqPaypalEmail" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Provide your paypal email id."></a>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="settings-note-style">
                                <p class="note-header"><b>Note:</b></p>
                                <p> While enabling PayPal, you need to set the return page in PayPal merchant Panel. You should also enable auto return and payment data transfer to generate identity token. Given below are the details of doing it.
                                <ul>
                                    <li>1. Click the Profile tab.</li>
                                    <li>2. Click the Website Payment Preferences link under Selling Preferences.</li>
                                    <li>3. Click the On radio button to enable Auto Return.</li>
                                    <li>4. Enter the Return URL.
                                        The return url is
                                        Success: https://www.iscripts.com/reservelogic/reservelogic/paynow.php?msg=s</li>
                                    <li> 5. Click on the radio button to enable Payment Data Transfer</li>
                                    <li> 6 Click save.</li>
                                    </p>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>




                <div class="boxborder">
                    <div class="control-group">
                        <label for="enablepaypal" class="control-label">Get A New Authorize.net Account</label>
                        <div class="controls">
                            <a target="_blank" href="http://www.authorize.net/"><img width="49" class="float-left" height="36" border="0" title="Start A New Account" alt="Start A New Account" src="<?php echo BASE_URL; ?>project/styles/images/authorize_small.jpg"></a>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="To start a new authorize.net account click on the authorize.net logo."></a>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="cenable_authorize" class="control-label">Enable Authorize.Net</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo (PageContext::$response->paySettings['cenable_authorize']->vLookUp_Value == 'Y')?'checked':''; ?> name="cenable_authorize" class="float-left jqToggle" value="Y" id="jqAuthorize" ><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Enable /disable Authorize.Net payment on your site."></a>

                        </div>
                    </div>
                    <div class="control-group jqAuthorize">
                        <div class="control-group">
                            <label for="authorize_test_mode" class="control-label">Authorize.Net Test Mode</label>
                            <div class="controls">
                                <input type="checkbox" <?php echo (PageContext::$response->paySettings['vauthorize_test_mode']->vLookUp_Value == 'Y')?'checked':''; ?> name="vauthorize_test_mode" value="Y" class="float-left"><span class="help-inline">Check To Make Authorize.Net Sandbox/Test Mode (Uncheck To Make Authorize.Net Live)</span>
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="switch between live and test mode."></a>

                            </div>
                        </div>
                        <div class="control-group">
                            <label for="vauthorize_loginid" class="control-label">Authorize.Net Login ID</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->paySettings['vauthorize_loginid']->vLookUp_Value; ?>" name="vauthorize_loginid" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Provide your login id  generated from Authorize.Net account"></a>

                            </div>
                        </div>
                        <div class="control-group">
                            <label for="vauthorize_transkey" class="control-label">Authorize.Net Transaction Key</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->paySettings['vauthorize_transkey']->vLookUp_Value; ?>" name="vauthorize_transkey" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Provide your Authorize.Net Transaction Key generated from Authorize.Net account"></a>

                            </div>
                        </div>
                        <div class="control-group">
                            <label for="vauthorize_email" class="control-label">Authorize.Net Email</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->paySettings['vauthorize_email']->vLookUp_Value; ?>" name="vauthorize_email" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Provide your Authorize.Net email id."></a>

                            </div>
                        </div>
                        <div class="control-group">
                            <label for="vauthorize_email" class="control-label">Type of Transaction </label>
                            <div class="controls">
                                <input type="radio" value="AUTH_CAPTURE" name="AuthorizeTransMode" class="float-left jqAuthoriseTransactionType" <?php echo PageContext::$response->paySettings['AuthorizeTransMode']->vLookUp_Value == 'AUTH_CAPTURE'?'Checked':''; ?>><label class="float-left"> &nbsp;AUTH and CAPTURE &nbsp;&nbsp;</label>
                                <input type="radio" value="AUTH_ONLY" name="AuthorizeTransMode" class="float-left jqAuthoriseTransactionType" <?php echo PageContext::$response->paySettings['AuthorizeTransMode']->vLookUp_Value == 'AUTH_ONLY'?'Checked':''; ?>><label class="float-left"> &nbsp;AUTH ONLY &nbsp;&nbsp;</label>
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Click to enable or disable transaction mode on your site."></a>

                            </div>
                        </div>
                        <?php if(PageContext::$response->paySettings['AuthorizeTransMode']->vLookUp_Value == 'AUTH_CAPTURE') {
                            $captureNpteDisplay = "inline";
                        }else {
                            $captureNpteDisplay = "none";
                        } ?>
                        <div class="control-group" style="display:<?php echo $captureNpteDisplay?>" id="jqCaptureNote" >

                            <div class="settings-note-style">
                                <p class="note-header"><b>Note :</b> </p>

                                <p><b><u>AUTH</u></b></p>
                                <p> This transaction type "reserves" funds on the customer's card for later CAPTURE. If the transaction is never followed up with a related CAPTURE then the funds are never removed from the customer's card and no charge occurs.</p>

                                <p><b><u>CAPTURE</u></b></p>
                                <p>This transaction type debits the funds, reserved earlier by an AUTH, and causes them to be transferred to the merchant's account. It's important to note that a successful AUTH guarantees a later CAPTURE -- if performed within the time limit.</p>

                                <p>This transaction is essentially an AUTH and a CAPTURE in one step and is the transaction type of choice for the majority of our merchants. </p>
                            </div>

                        </div>

                        <?php if(PageContext::$response->paySettings['AuthorizeTransMode']->vLookUp_Value == 'AUTH_ONLY') {
                            $authNoteDisplay = "inline";
                        }else {
                            $authNoteDisplay = "none";
                        } ?>
                        <div class="control-group" style="display:<?php echo $authNoteDisplay;?>" id="jqAuthoriseNote">

                            <div class="settings-note-style">
                                <p class="note-header"><b>Note :</b> </p>
                                <p>  This transaction type "reserves" funds on the customer's card for later CAPTURE. If the transaction is never followed up with a related CAPTURE then the funds are never removed from the customer's card and no charge occurs. You have roughly thirty days to CAPTURE an AUTH, although the exact time can vary from network to network. .</p>
                            </div>

                        </div>

                    </div>
                </div>






                <div class="boxborder">
                    <div class="control-group">
                        <label for="enablepaypal" class="control-label">Get A New Google Checkout Account</label>
                        <div class="controls">
                            <a target="_blank" href="http://checkout.google.com/sell?promo=searmiasystemsinc"><img width="114" height="20" border="0" title="Start A New Account" alt="Start A New Account" class="float-left" src="<?php echo BASE_URL; ?>project/styles/images/google_small.gif"></a><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="To start a new google checkout account click on the google checkout logo."></a>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="enable_googlecheckout" class="control-label">Enable Google Checkout</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo (PageContext::$response->paySettings['enablegoogle']->vLookUp_Value == 'Y')?'checked':''; ?> name="enablegoogle" class="jqToggle float-left" value="Y" id="jqGoogleCheckout"><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Enable /disable Google Checkout payment on your site."></a>

                        </div>
                    </div>
                    <div class="control-group jqGoogleCheckout">
                        <div class="control-group">
                            <label for="googleid" class="control-label">Google Checkout Merchant ID</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->paySettings['googleid']->vLookUp_Value; ?>" name="googleid" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Provide your Google Checkout Merchant ID generated from Google Checkout account"></a>

                            </div>
                        </div>
                        <div class="control-group">
                            <label for="googlekey" class="control-label"> Google Checkout Merchant Key</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->paySettings['googlekey']->vLookUp_Value; ?>" name="googlekey" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Provide your Google Checkout Merchant Key generated from Google Checkout account"></a>

                            </div>
                        </div>
                        <div class="control-group">
                            <label for="gcheck_server_type" class="control-label"> Google Checkout Server Type </label>
                            <div class="controls">
                                <select name="enablegooglesandbox" class="float-left">
                                    <option value="sandbox" <?php echo (PageContext::$response->paySettings['enablegooglesandbox']->vLookUp_Value=='sandbox')?'selected':'';?>>Sandbox</option>
                                    <option value="live" <?php echo (PageContext::$response->paySettings['enablegooglesandbox']->vLookUp_Value=='live')?'selected':'';?>>Live</option>
                                </select>
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Switch between Live and Test Mode"></a>

                            </div>
                        </div>
                        <div class="control-group">
                            <div class="settings-note-style">
                                <p class="note-header"><b>Note :</b></p>
                                <p>
                                    Please add the url "https://www.iscripts.com/reservelogic/reservelogic/response.php" to the "API callback URL" field in googlecheckout merchant panel. You can navigate to this in googlecheckout merchant panel screen as follows My Sales -> Settings -> Integration -> set API callback URL
                                </p>
                            </div>
                        </div>

                    </div>
                </div>






                <div class="boxborder">
                    <div class="control-group">
                        <label for="enablepaypal" class="control-label">Get A New YourPay Account</label>
                        <div class="controls">
                            <a target="_blank" href="http://www.firstdata.com/yourpay/"><img width="63" class="float-left" height="16" border="0" title="Get A New YourPay Account" alt="Get A New YourPay Account" src="<?php echo BASE_URL; ?>project/styles/images/yourpay_small.jpg"></a>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="To start a new yourpay account click on the yourpay logo."></a>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="enable_googlecheckout" class="control-label">Enable YourPay</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo (PageContext::$response->paySettings['enableyourpay']->vLookUp_Value == 'Y')?'checked':''; ?> name="enableyourpay" class="jqToggle" value="Y" id="jqYourPay"><span class="help-inline"></span>
                        </div>
                    </div>
                    <div class="control-group jqYourPay">
                        <div class="control-group">
                            <label for="urpaystoreid" class="control-label">YourPay Store ID</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->paySettings['urpaystoreid']->vLookUp_Value; ?>" name="urpaystoreid">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="pemfolder" class="control-label"> YourPay PEM File</label>
                            <div class="controls">
                                <input type="file" name="pemfolder" id="pemfolder" />
                                <p/>
                                <?php
                                echo PageContext::$response->paySettings['pemfolder']->vLookUp_Value;
                                ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="urpaydemo" class="control-label"> YourPay Demo Mode </label>
                            <div class="controls">
                                <select name="urpaydemo">
                                    <option value="YES" <?php echo (PageContext::$response->paySettings['urpaydemo']->vLookUp_Value=='YES')?'selected':'';?>>Yes</option>
                                    <option value="NO" <?php echo (PageContext::$response->paySettings['urpaydemo']->vLookUp_Value=='NO')?'selected':'';?>>No</option>
                                </select>
                            </div>
                        </div>

                    </div>

                </div>



                <!---------------------------------------------------------------------->
                <!----------------------   Worldpay  settings  ------------------------->
                <!---------------------------------------------------------------------->


                <div class="boxborder">

                    <div class="control-group">
                        <label for="enablepaypal" class="control-label">Get A New Worldpay Account</label>
                        <div class="controls">
                            <a target="_blank" href="http://www.worldpay.com/"><img width="63" class="float-left" height="16" border="0" title="Get A New World Account" alt="Get A New World Account" src="<?php echo BASE_URL; ?>project/styles/images/worldpay.jpg"></a>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="To start a new Worldpay account click on the Worldpay logo."></a>




                        </div>
                    </div>

                    <div class="control-group">
                        <div class="settings-note-style">
                            <p class="note-header"><b>Note :</b></p>
                            <p>
                                * While enabling Worldpay, you need to take account from http://worldpay.com/ ,set the return page in your site after the payment process is over, in the Worldpay Merchant Panel.<br />
                                Log into your WorldPay Customer Management System then click Configuration Options. Then:<br />
                                1) Enter the Callback URL as <?php echo BASE_URL;?>payments/worldPayIpnCall<br />
                                2) Tick the checkboxes for Callback Enabled and Use Callback Response<br />
                                3) Set your Callback Password to match exactly with the Callback Password you set in site - note that you need to enter this in 2 boxes<br />
                                4) Click Save Changes<br />
                            </p>
                        </div>

                    </div>

                    <div class="control-group">
                        <label for="enableworldpay" class="control-label">Enable Worldpay</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo (PageContext::$response->paySettings['enableworldpay']->vLookUp_Value == 'Y')?'checked':''; ?> name="enableworldpay" class="jqToggle" value="Y" id="jqWorldPay"><span class="help-inline"></span>
                        </div>
                    </div>

                    <div class="control-group jqWorldPay">

                        <div class="control-group">
                            <label for="worldpayid" class="control-label">Worldpay Installation ID</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->paySettings['worldpayid']->vLookUp_Value; ?>" name="worldpayid">
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="gcheck_server_type" class="control-label">Worldpay Demo Mode</label>
                            <div class="controls">
                                <select name="worldpaydemo">
                                    <option value="Y" <?php echo (PageContext::$response->paySettings['worldpaydemo']->vLookUp_Value=='Y')?'selected':'';?>>Yes</option>
                                    <option value="N" <?php echo (PageContext::$response->paySettings['worldpaydemo']->vLookUp_Value!='Y')?'selected':'';?>>No</option>
                                </select>
                            </div>
                        </div>

                    </div>

                </div>

                <!---------------------------------------------------------------------->
                <!----------------------  end ------------------------------------------->
                <!---------------------------------------------------------------------->













                <div class="boxborder">
                    <div class="control-group">
                        <label for="enable_other_payments" class="control-label">Enable Other Payments? </label>
                        <div class="controls">
                            <input type="checkbox" <?php echo (PageContext::$response->paySettings['enable_other_payments']->vLookUp_Value == 'Y')?'checked':''; ?> name="enable_other_payments" class="jqToggle float-left" value="Y" id="enable_other_payments"><span class="help-inline"></span>
                            <span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Click to enable/disable other payments(DD/Check) on your site."></a>
                        </div>
                    </div>


                </div>

                <div class="boxborder">
                    <div class="control-group">
                        <div class="settings-note-style">
                            <p class="note-header"><b>Note :</b></p>
                            <p>
                                Must fill the address for communication under general settings
                            </p>
                        </div>
                    </div>
                </div>

                <div class="controls">
                    <input type="submit" name="paymentSubmitBtn" value="Save" class="submitButton btn" />
                </div>
            </form>

        </div>
        <!-- Payment -->


        <!-- Domain Registrar -->
        <div id="advanced" class="tab-pane">
            <form name="DomainRegistrarForm" id="jqDomainRegistrarForm" method="post" class="form-horizontal" action="<?php echo BASE_URL; ?>cms?section=settings&tab=advanced">

                <div class="boxborder" >
                    <div class="control-group" style="display:none;">
                        <label class="control-label" for="godaddy_id">Enable Flash Header</label>
                        <div class="controls">
                            <input type="radio" name="flash_header" value="1" <?php echo (PageContext::$response->advancedSettings['flash_header']->vLookUp_Value == '1')?'checked="checked"':''; ?> class="float-left"><label class="float-left">&nbsp; Yes &nbsp;&nbsp;</label>
                            <input type="radio" name="flash_header" value="0" <?php echo (PageContext::$response->advancedSettings['flash_header']->vLookUp_Value == '0')?'checked="checked"':''; ?> class="float-left"><label class="float-left">&nbsp; No &nbsp;&nbsp;</label>
                            <!--  <a href="javascript:void(0)" class="tooltiplink help-icon" title="Name your site. This name is the website title. This will be visible to users within the browser tab. Try to use keywords describing your site to help visitors understand your page quickly."></a>-->
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="godaddy_id">Enable Affiliates</label>
                        <div class="controls"> 

                            <input type="radio" name="referral" value="1" <?php echo (PageContext::$response->advancedSettings['referral']->vLookUp_Value == '1')?'checked="checked"':''; ?> class="float-left"><label class="float-left">&nbsp; Yes &nbsp;&nbsp;</label>
                            <input type="radio" name="referral" value="0" <?php echo (PageContext::$response->advancedSettings['referral']->vLookUp_Value != '1')?'checked="checked"':''; ?> class="float-left"> <label class="float-left">&nbsp;No &nbsp;&nbsp;</label>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="To enable referral in the site."></a>

                        </div>
                    </div>
                    <div class="control-group jqAffiliate">
                        <label class="control-label" for="siteTitle">Affiliate Commission</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->genSettings['affiliate_percentage']->vLookUp_Value; ?>" name="affiliate_percentage" class="float-left"> &nbsp;%
                        </div>
                    </div>

                    <!-- Deault Property Commission -->
                    <div class="control-group">
                        <label class="control-label" for="default_property_commission">Default Property Commission</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->advancedSettings['default_property_commission']->vLookUp_Value; ?>" name="default_property_commission" class="float-left"> &nbsp;%
                        </div>
                    </div>
                    <!-- Deault Property Commission -->
                    
                    <div class="control-group">
                        <label class="control-label" for="godaddy_id">Operation Mode</label>
                        <div class="controls">

                            <input type="radio" name="operation_mode" value="1" <?php echo (PageContext::$response->advancedSettings['operation_mode']->vLookUp_Value == '1')?'checked="checked"':''; ?> >&nbsp; Single Destination Single Property <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Eg: Independent Resort, B&B, Motel, Hotel etc..]<br /> 
                            <input type="radio" name="operation_mode" value="2" <?php echo (PageContext::$response->advancedSettings['operation_mode']->vLookUp_Value == '2')?'checked="checked"':''; ?>>&nbsp; Single Destination Multiple Property<br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Eg: Hotel HolidayInn (Property) & Hotel Trident (Property)at Miami (destination)]<br />

                            <input type="radio" name="operation_mode" value="3" <?php echo (PageContext::$response->advancedSettings['operation_mode']->vLookUp_Value == '3')?'checked="checked"':''; ?>>&nbsp; Multiple Destination Multiple Property<br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Eg: Hotel HolidayInn (Property) at Washington(Property)] & Hotel Trident (location)at &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Miami(destination)]

                        </div>
                    </div>
                </div>


                <div class="boxborder">
                    <div class="control-group">
                        <label class="control-label" for="godaddy_id">Reservation Mode</label>
                        <div class="controls">

                            <input type="radio" name="ReserveMode" value="F" <?php echo (PageContext::$response->advancedSettings['ReserveMode']->vLookUp_Value == 'F')?'checked="checked"':''; ?> class="float-left jqToggleBox jqReserveModes" id="jqReserveMode"><label class="float-left">&nbsp; Free &nbsp;&nbsp;</label>
                            <input type="radio" name="ReserveMode" value="P" <?php echo (PageContext::$response->advancedSettings['ReserveMode']->vLookUp_Value == 'P')?'checked="checked"':''; ?> class="float-left jqToggleBox jqReserveModes" id="jqReserveMode"> <label class="float-left">&nbsp;Paid&nbsp;&nbsp;</label>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="If reservation mode is free, no payments is done while booking."></a>

                        </div>
                    </div>
                    <?php if(empty(PageContext::$response->advancedSettings['ReserveMode']->vLookUp_Value) || PageContext::$response->advancedSettings['ReservePayMode']->vLookUp_Value == 'F')
                        $noBookingAdvacneNoteDisplay = 'inline';
                    else {
                        $noBookingAdvacneNote = 'none';
                    }
                    if(PageContext::$response->advancedSettings['ReserveMode']->vLookUp_Value == 'P')
                        $BookingAdvacneNoteDisplay = 'inline';
                    else {
                        $BookingAdvacneNoteDisplay = 'none';
                    }?>
                    <div class="control-group" id="jqNoteNoBookingAdvance" style="display:<?php echo $noBookingAdvacneNote;?>">
                        <div class="settings-note-style">
                            <p class="note-header"><b>Note :</b></p>
                            <p>
                                No booking advance
                            </p>
                        </div>
                    </div>
                    <div class="control-group" id="jqNoteBookingAdvance" style="display:<?php echo $BookingAdvacneNoteDisplay;?>">
                        <div class="settings-note-style">
                            <p class="note-header"><b>Note :</b></p>
                            <p>
                                If Amount mode is Percentage, booking advance is percentage (is set in below) of total amount.
                                If Amount mode is Amount, that amount is taken as booking advance
                            </p>
                        </div>
                    </div>

                    <div class="control-group jqReserveMode" <?php echo (PageContext::$response->advancedSettings['ReserveMode']->vLookUp_Value == 'F')?'style="display:none"':''; ?>>
                        <div class="control-group">
                            <label class="control-label" for="enablepaypalsandbox">Amount Mode</label>
                            <div class="controls">

                                <input type="radio" class="float-left jqReserveAmountModeClass" name="ReservePayMode" value="Per" <?php echo (PageContext::$response->advancedSettings['ReservePayMode']->vLookUp_Value == 'Per')?'checked="checked"':''; ?> ><label class="float-left">&nbsp; Percentage&nbsp;&nbsp;</label>
                                <input type="radio" name="ReservePayMode" class="float-left jqReserveAmountModeClass" value="Amt" <?php echo (PageContext::$response->advancedSettings['ReservePayMode']->vLookUp_Value == 'Amt')?'checked="checked"':''; ?> ><label class="float-left"> &nbsp;Amount&nbsp;&nbsp;</label>
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Advance amount can either be percentage of Order Total or a fixed amount set by admin"></a>


                            </div>
                        </div>
                        <?php if(empty(PageContext::$response->advancedSettings['ReservePayMode']->vLookUp_Value) || PageContext::$response->advancedSettings['ReservePayMode']->vLookUp_Value == 'Amt')
                            $amountDisplay = 'inline';
                        else {
                            $amountDisplay = 'none';
                        }
                        if(PageContext::$response->advancedSettings['ReservePayMode']->vLookUp_Value == 'Per')
                            $percentDisplay = 'inline';
                        else {
                            $percentDisplay = 'none';
                        }?>

                        <div class="control-group">
                            <label class="control-label" for="enablepaypalsandbox">Reserve Mode Value</label>
                            <div class="controls" style="width:300px">

                                <span class="float-left" id="jqAmountModeSymbol" style="display:<?php echo $amountDisplay;?>">$&nbsp;&nbsp;</span>
                                <input type="text" value="<?php echo PageContext::$response->advancedSettings['ReserveModeAmount']->vLookUp_Value; ?>" name="ReserveModeAmount" class="float-left" style="width:50px;">
                                <span class="float-left" id="jqPercentModeSymbol" style="display:<?php echo $percentDisplay;?>">&nbsp;&nbsp;%</span>

                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Enter percentage or amount that user has to pay in advance while booking.Mode value can be percentage or amount based on  Amount Mode Setting"></a>

                            </div>
                        </div>
                    </div>
                </div>



                <div class="boxborder">
                    <div class="control-group">
                        <label for="AllowCancel" class="control-label">Allow Online Cancellation </label>
                        <div class="controls">
                            <input type="checkbox" <?php echo (PageContext::$response->advancedSettings['AllowCancel']->vLookUp_Value == '1')?'checked':''; ?> name="AllowCancel" value="1" class="jqToggle float-left" id="jqAllowCancel"><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Allow online cancellation by user after booking"></a>

                        </div>
                    </div>
                    <div class="control-group jqAllowCancel">
                        <div class="control-group">
                            <label class="control-label" for="enablepaypalsandbox">Cancel type</label>
                            <div class="controls">

                                <input  type="radio" name="CancelType" value="1" <?php echo (PageContext::$response->advancedSettings['CancelType']->vLookUp_Value == '1')?'checked="checked"':''; ?> class="float-left jqCancelType"> <label class="float-left">&nbsp;Penalty &nbsp;&nbsp;</label>
                                <input type="radio" name="CancelType" value="0" <?php echo (PageContext::$response->advancedSettings['CancelType']->vLookUp_Value == '0')?'checked="checked"':''; ?> class="float-left jqCancelType"> <label class="float-left ">&nbsp;Contact Admin&nbsp;&nbsp;</label>
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="If penalty is selected user has to pay an amount. If Contact Admin selected, user has to mail or contact admin."></a>


                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <?php if(PageContext::$response->advancedSettings['CancelType']->vLookUp_Value == '1') {
                                    $penaltyDisplay = "inline";
                                }else {
                                    $penaltyDisplay = "none";
                                }
                                ?>
                                <div ><a id="jqPenaltyLink" href="<?php echo BASE_URL?>cms?section=settings&tab=penalty" style="display:<?php echo $penaltyDisplay;?>">Manage Penalty</a></div>

                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="enablepaypalsandbox">Cancellation Terms</label>
                            <div class="controls">
                                <textarea name="cancellation_terms"  class="float-left"><?php echo htmlspecialchars(PageContext::$response->advancedSettings['cancellation_terms']->vLookUp_Value); ?></textarea>
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="Terms and Conditions for cancellation"></a>

                            </div>
                        </div>


                    </div>
                </div>



                <div class="boxborder">
                    <div class="control-group">
                        <label for="EnableSMTP" class="control-label">Enable SMTP </label>
                        <div class="controls">
                            <input type="checkbox" <?php echo (PageContext::$response->advancedSettings['EnableSMTP']->vLookUp_Value == 'Y')?'checked':''; ?> name="EnableSMTP" value="Y" class="jqToggle float-left" id="jqEnableSMTP"><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Allow mail to send using SMTP"></a>

                        </div>
                    </div>
                    <div class="control-group jqEnableSMTP">

                        <div class="control-group">
                            <label class="control-label" for="SMTPHost">SMTP Host</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->advancedSettings['SMTPHost']->vLookUp_Value; ?>" name="SMTPHost" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="SMTP Host/Server"></a>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="SMTPUsername">SMTP Username</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->advancedSettings['SMTPUsername']->vLookUp_Value; ?>" name="SMTPUsername" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="SMTP Account Username"></a>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="SMTPPassword">SMTP Password</label>
                            <div class="controls">
                                <input type="password" value="<?php echo PageContext::$response->advancedSettings['SMTPPassword']->vLookUp_Value; ?>" name="SMTPPassword" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="SMTP Account Password"></a>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="SMTPPort">SMTP Port</label>
                            <div class="controls">
                                <input type="text" value="<?php echo PageContext::$response->advancedSettings['SMTPPort']->vLookUp_Value; ?>" name="SMTPPort" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="SMTP Port"></a>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="SSLEnabled" class="control-label">SSL Enabled </label>
                            <div class="controls">
                                <input type="checkbox" <?php echo (PageContext::$response->advancedSettings['SSLEnabled']->vLookUp_Value == 'Y')?'checked':''; ?> name="SSLEnabled" value="Y" class="float-left">
                                <a href="javascript:void(0)" class="tooltiplink help-icon" title="SSL is enabled or not"></a>
                            </div>
                        </div>

                    </div>
                </div>

                

                <div class="controls">
                    <input type="submit" name="domainSubmitBtn" value="Save" class="submitButton btn" />
                </div>
            </form>
        </div>
        <!-- Domain Registrar -->

        <div id="penalty" class="tab-pane">


            <?php      PageContext::renderPostAction("managepenalty","cmshelper", "default"); ?>



        </div>




        <!-- Social Settings -->
        <div id="social" class="tab-pane">
            <form name="socialSettingsForm" id="jqsocialSettingsForm" method="post" class="form-horizontal" action="<?php echo BASE_URL; ?>cms?section=settings&tab=social">
                <div class="boxborder">
                    <?php
                    $checked = '';
                    if(PageContext::$response->socialSettings['enable_fb']->vLookUp_Value == 'Y') {
                        $checked = "checked='checked'";
                    }
                    ?>
                    <div class="control-group">
                        <label for="enable_fb" class="control-label">Enable Facebook</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo $checked; ?>  name="enable_fb" class="jqToggle float-left" value="Yes" id="jqFacebook" ><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You can enable / disable Facebook share from site."></a>
                        </div>
                    </div>

                    <div class="control-group jqFacebook">
                        <label for="fb_app_id" class="control-label">Facebook App ID</label>
                        <div class="controls">
                            <input type="text" value="<?php echo PageContext::$response->socialSettings['fb_app_id']->vLookUp_Value; ?>" name="fb_app_id" class="float-left">
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="Enter a Facebook App ID"></a>
                        </div>
                    </div>
                    <?php
                    $checked = '';
                    if(PageContext::$response->socialSettings['enable_twitter']->vLookUp_Value == 'Y') {
                        $checked = "checked='checked'";
                    }
                    ?>
                    <div class="control-group">
                        <label for="enable_twitter" class="control-label">Enable Twitter</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo $checked; ?>  name="enable_twitter" class="jqToggle float-left" value="Yes" id="jqTwitter" ><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You can enable / disable Twitter share from site."></a>
                        </div>
                    </div>
                    <?php
                    $checked = '';
                    if(PageContext::$response->socialSettings['enable_pinterest']->vLookUp_Value == 'Y') {
                        $checked = "checked='checked'";
                    }
                    ?>
                    <div class="control-group">
                        <label for="enable_pinterest" class="control-label">Enable Pinterest</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo $checked; ?>  name="enable_pinterest" class="jqToggle float-left" value="Yes" id="jqPinterest" ><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You can enable / disable Pinterest share from site."></a>
                        </div>
                    </div>
                    <?php
                    $checked = '';
                    if(PageContext::$response->socialSettings['enable_gplus']->vLookUp_Value == 'Y') {
                        $checked = "checked='checked'";
                    }
                    ?>
                    <div class="control-group">
                        <label for="enable_gplus" class="control-label">Enable GooglePlus</label>
                        <div class="controls">
                            <input type="checkbox" <?php echo $checked; ?>  name="enable_gplus" class="jqToggle float-left" value="Yes" id="jqGplus" ><span class="help-inline"></span>
                            <a href="javascript:void(0)" class="tooltiplink help-icon" title="You can enable / disable GooglePlus share from site."></a>
                        </div>
                    </div>
                </div>
                <div class="controls">
                    <input type="submit" name="submitBtn" value="Save" class="submitButton btn" />
                </div>
            </form>
        </div>
        <!-- Social Settings -->





        <!-- Update Password -->
        <div id="password" class="tab-pane">
            <form name="passwordForm" id="jqPasswordForm" method="post" class="form-horizontal" action="<?php echo BASE_URL; ?>cms?section=settings&tab=password">
                <div class="control-group jqStream">
                    <label class="control-label">Current Password</label>
                    <div class="controls">
                        <input type="password" value="" name="current_password" class="float-left">
                        <!-- <a href="javascript:void(0)" class="tooltiplink help-icon" title="Name your site. This name is the website title. This will be visible to users within the browser tab. Try to use keywords describing your site to help visitors understand your page quickly."></a>
                        -->
                    </div>
                </div>

                <div class="control-group jqStream">
                    <label class="control-label">New Password</label>
                    <div class="controls">
                        <input type="password" value="" name="new_password" class="float-left">
                        <!--   <a href="javascript:void(0)" class="tooltiplink help-icon" title="Name your site. This name is the website title. This will be visible to users within the browser tab. Try to use keywords describing your site to help visitors understand your page quickly."></a>
                        -->
                    </div>
                </div>

                <div class="control-group jqStream">
                    <label class="control-label">Re-Type Password</label>
                    <div class="controls">
                        <input type="password" value="" name="retype_password" class="float-left">
                        <!--         <a href="javascript:void(0)" class="tooltiplink help-icon" title="Name your site. This name is the website title. This will be visible to users within the browser tab. Try to use keywords describing your site to help visitors understand your page quickly."></a>
                        -->
                    </div>
                </div>

                <div class="controls">
                    <input type="submit" name="passwordSubmitBtn" value="Save" class="submitButton btn" />
                </div>
            </form>
        </div>
    </div>
</div>
<?php //print_r(PageContext::$response->pageContents); ?>