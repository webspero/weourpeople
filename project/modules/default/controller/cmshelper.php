<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ControllerCmshelper extends BaseController
{
    public function init(){
        parent::init();
               
     }

    //Function to change user status
    public function ajaxUserStatusChange(){
       print_r(PageContext::$request);
        $id    = PageContext::$request['id'];
        $value = PageContext::$request['value'];
        if($id>0){
            $updateVal = Cmshelper::updateUserStatus($id,$value);
        }
        exit();
    }

    //Function to change Category status
    public function ajaxCategoryStatusChange(){
    	 
    	$id    = PageContext::$request['id'];
    	$value = PageContext::$request['value'];
    	if($id>0){
    		$updateVal = Cmshelper::updateCategoryStatus($id,$value);
    	}
    	exit();
    }

    //Function to change industry status
    public function ajaxIndustryStatusChange(){
    
    	$id    = PageContext::$request['id'];
    	$value = PageContext::$request['value'];
    	if($id>0){
    		$updateVal = Cmshelper::updateIndustryStatus($id,$value);
    	}
    	exit();
    }
    
    //Function to change mail template status
    public function ajaxMailTemplateStatusChange(){
    	 
    	$id    = PageContext::$request['id'];
    	$value = PageContext::$request['value'];
    	
    	
	    	if($id>0){
	    		$updateVal = Cmshelper::updateMailTemplateStatus($id,$value);
	    	}
    	
    	exit();
    }    
    
    //Function to change business status
    public function ajaxBusinessStatusChange(){
       
        $id    = PageContext::$request['id'];
        $value = PageContext::$request['value'];
        if($id>0){
            $updateVal = Cmshelper::updateBusinessStatus($id,$value);
        }
        exit();
    }
    
    
 	//Function to change clasifieds status
    public function ajaxClassifiedsStatusChange(){
       
        $id    = PageContext::$request['id'];
        $value = PageContext::$request['value'];
        if($id>0){
            $updateVal = Cmshelper::updateClassifiedsStatus($id,$value);
        }
        exit();
    }
       
    public function ajaxCommunityStatusChange(){
       
        $id    = PageContext::$request['id'];
        $value = PageContext::$request['value'];
        if($id>0){
            $updateVal = Cmshelper::updateCommunityStatus($id,$value);
        }
        exit();
    }

    public function ajaxGetUserState($userId){
        $stateVal = User::getState($userId);//echo"1";exit;
        if($stateVal->user_state!=''){
            echo $stateVal->user_state;
        }else{
            echo '';
        }
        exit;
    }

    public function ajaxGetOrganizationState($userId){
    	$stateVal = Business::getState($userId);//echo"1";exit;
        //echopre1($stateVal);
//    	if($stateVal->business_state!=''){
//    		echo $stateVal->business_state;
//    	}else{
//    		echo '';
//    	}
        if($stateVal!=''){
    		echo $stateVal;
    	}else{
    		echo '';
    	}
    	exit;
    }
    
    public function ajaxCommentStatusChange(){
    	print_r(PageContext::$request);
    	$id    = PageContext::$request['id'];
    	$value = PageContext::$request['value'];
    	if($id>0){
    		$updateVal = Cmshelper::updateCommentStatus($id,$value);
    	}
    	exit();
    }
    
    public function ajaxGetStatesByCountryUser($countryCode,$userId=0,$mode=''){
    	//echo"3";exit;
        if($mode=='edit' && $userId > 0 ){ //echo"1";exit;
            $stateVal = User::getState($userId);
        } //echo"2";exit;
        $states = Utils::getStatesByCountry($countryCode);
        $stateContent = '';
        foreach($states AS $state){
            if($stateVal!='')
                $selected = ($stateVal==$state->ts_code)?'selected':'';
            $stateContent .= "<option value ='".$state->ts_code."' ".$selected." >".$state->ts_name."</option>";
        }
        print_r($stateContent);
        Logger::info($stateContent);
        exit;
    }

    public function ajaxGetStatesByCountryOrganization($countryCode,$userId=0,$mode=''){
    	//echo"3";exit;
    	if($mode=='edit' && $userId > 0 ){ //echo"1";exit;
    		$stateVal = Business::getState($userId);
    	} //echo"2";exit;
    	$states = Utils::getStatesByCountry($countryCode);
    	$stateContent = '';
    	foreach($states AS $state){
    		if($stateVal!='')
    			$selected = ($stateVal==$state->ts_code)?'selected':'';
    		$stateContent .= "<option value ='".$state->ts_code."' ".$selected." >".$state->ts_name."</option>";
    	}
    	print_r($stateContent);
    	Logger::info($stateContent);
    	exit;
    }

    //Function to change banner status
   public function ajaxBannerStatusChange(){
    	print_r(PageContext::$request);
    	$id    = PageContext::$request['id'];
    	$value = PageContext::$request['value'];
    	if($id>0){
    		$updateVal = Cmshelper::updateBannerStatus($id,$value);
    	}
    	exit();
    }
     //Function to change banner status
   public function ajaxReconcileStatusChange(){
    	print_r(PageContext::$request);
    	$id    = PageContext::$request['id'];
    	$value = PageContext::$request['value'];
    	if($id>0){
    		$updateVal = Cmshelper::updateReconcileStatus($id,$value);
               
    	}
    	exit();
    }
    
    public function setTheme() {
    	
        if(trim(CURRENT_THEME) == 'corporate')
           $selcor = "selected";
        if(trim(CURRENT_THEME) == 'default')
           $seldef = "selected";
        if(trim(CURRENT_THEME) == 'lifestyle_fashion')
           $selfas = "selected";
        if(trim(CURRENT_THEME) == 'foodie')
           $selfdl = "selected";
        

        if(trim(CURRENT_THEME) == 'theme5')
           $seltheme = "selected";

        if(trim(CURRENT_THEME) == 'ayurved')
           $selayurved = "selected";
        
        echo $var =  '<div class="control-group"><label class="control-label" for="sitestyle">Site Theme</label><div class="controls"><select class="float-left" id="themeselect">
             <option '.$selayurved.' value="ayurved">Ayurved</option>
           <option '.$selcor.' value="corporate">Corporate</option><option '.$seldef.' value="default">Default</option><option '.$selfas.' value="lifestyle_fashion">Life Style Fashion</option><option '.$selfdl.' value="foodie">Foodie</option><option '.$seltheme.' value="theme5">Theme5</option></select></div></div>';
   		exit();
    }
    
    
  
}
?>
