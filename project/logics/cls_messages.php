<?php 

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
// +----------------------------------------------------------------------+
// | File name : Utils.php                                         		  |
// | PHP version >= 5.2                                                   |
// +----------------------------------------------------------------------+
// | Author: ARUN SADASIVAN<arun.s@armiasystems.com>              		  |
// +----------------------------------------------------------------------+
// | Copyrights Armia Systems � 2010                                    |
// | All rights reserved                                                  |
// +----------------------------------------------------------------------+
// | This script may not be distributed, sold, given away for free to     |
// | third party, or used as a part of any internet services such as      |
// | webdesign etc.                                                       |
// +----------------------------------------------------------------------+
/*Class display all standard Messages*/
class Messages{
	
        public $status = SUCCESS;
        public $message;
       
    /*function to display messages*/
	public static function getMessage(){
	   return $this->message;
	}
        
         /*function to display messages*/
	public static function Message(){
	   return $this->message;
	}
        
        
         public static function setPageMessage($message,$class){
       $_SESSION['page_message']    =   $message;
       $_SESSION['message_class']    =   $class;
    }
    
    public static function getPageMessage($unsetMessage = true){
        
       $message =  array('msg'=> $_SESSION['page_message'],'msgClass'=> $_SESSION['message_class']);
       if($unsetMessage){
        $_SESSION['page_message']	='';
        $_SESSION['message_class']	='';
       }
        return $message;
        
        
    }
    
     public static function getMessageCount(){
        $db                             = new Db();
        $userId                         = Utils::getAuth('user_id');
        $tableName                      = Utils::setTablePrefix('messages');
        $query                          = "SELECT message_id FROM ".$tableName." WHERE message_to_id= '".$userId."' AND message_status=1";
        $objResultRow                   = $db->fetchAll($db->execute($query));
        return $objResultRow;
    }
    public static function getMessageCountByUser(){
        $db                             = new Db();
        $userId                         = Utils::getAuth('user_id');
        $tableName                      = Utils::setTablePrefix('users');
        $query                          = "SELECT user_message_count FROM ".$tableName." WHERE user_id= '".$userId."'";
        $objResultRow                   = $db->fetchAll($db->execute($query));
        return $objResultRow;
    }
    
    
    
    
    
        
}


?>