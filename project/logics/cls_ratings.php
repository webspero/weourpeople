<?php 
/*
 * All Ratingss Entity Logics should come here.
 */
class Ratings{
   /*
Function to Add a  new Ratings
*/
 public static  function addRatings($objRatingsVo)
 {         
            $objResult                      =   new Messages();
            $objResult->status              =   SUCCESS;
     
                if($objResult->status==SUCCESS) {
                 $db                             =   new Db();

                 $tableName                      =   Utils::setTablePrefix('rating');
                 
                 $excludeId                      =    '';
                 
               


                 $ratingId                     =   $db->insert($tableName, array(  'rating_value'     =>  $objRatingsVo->rating_value,
                                                                                    'rating_entity'      =>  $db->escapeString($objRatingsVo->rating_entity),
                                                                                    'rating_entity_id'  =>  $objRatingsVo->rating_entity_id,
                                                                                  //  'comment_created_by'  =>  $objRatingsVo->comment_created_by,
                                                                                    'rating_rated_by'   =>  $objRatingsVo->rating_rated_by,
                                                                                    'rating_rated_on'      =>  $objRatingsVo->rating_rated_on,
                                                                                    'rating_status' =>  $db->escapeString($objRatingsVo->rating_status)
                                                                                  ));
                 $objUserVo->rating_id              =   $ratingId;
                 $objResult->data                 =   $objRatingsVo;
          }
          Logger::info($objResult);
          return $objResult;
                
 }   
 
 
 
//  public static function getRatings($ratingEntityId,$ratingEntity)
//  {
//      
//      
//        $objResult                        =   new Messages();
//                $objResult->status                =   SUCCESS;
//                
//            if($ratingEntityId=='' || $ratingEntity == '') {  
//             
//                                 $objResult->status           =   "ERROR";
//                                 $objResult->message          =   GROUP_ID_NOT_EXIST; // User Id not  Exists
//                         
//         }        
//                
//             if ($objResult->status == SUCCESS) {
//
//            if ($ratingEntityId != '') {
//                $db = new Db();
//                $tableName = 'rating';
//                $selectCase = "  rating_entity ='" . $db->escapeString($ratingEntity) ." ' AND rating_entity_id = ".$db->escapeString($ratingEntityId);
//                Logger::info($selectCase);
////echo $selectCase;
//                $objResult1 = $db->selectResult($tableName, 'SUM(rating_value) as total,count(rating_id) as count', $selectCase);
////echopre($objResult1);
//                if ($objResult1)
//                    $objResult->data = $objResult1[0]; // Assign user record  to object
//                else {
//                    $objResult->status = "ERROR";
//                    $objResult->message = USER_ID_NOT_EXIST; // User not exists
//                }
//            }
//        }
//            Logger::info($objResult);
//            return $objResult;
//      
//      
//      
//      
//  }
 
 public static function getRatings($ratingEntityId,$ratingEntity)
  {
      
      
        $objResult                        =   new Messages();
                $objResult->status                =   SUCCESS;
                
            if($ratingEntityId=='' || $ratingEntity == '') {  
             
                                 $objResult->status           =   "ERROR";
                                 $objResult->message          =   GROUP_ID_NOT_EXIST; // User Id not  Exists
                         
         }        
                
             if ($objResult->status == SUCCESS) {

            if ($ratingEntityId != '') {
                $db = new Db();
                $tableName = Utils::setTablePrefix('rating');
                $tableName1 = Utils::setTablePrefix('users');
                
                $query = "SELECT SUM(rating_value) as total,count(rating_id) as count FROM $tableName AS r JOIN $tableName1 AS u ON u.user_id = r.rating_rated_by AND user_status = 'A' WHERE rating_entity ='" . $db->escapeString($ratingEntity) ." ' AND rating_entity_id = ".$db->escapeString($ratingEntityId);

               // $selectCase = "  rating_entity ='" . $db->escapeString($ratingEntity) ." ' AND rating_entity_id = ".$db->escapeString($ratingEntityId);
                Logger::info($selectCase);
//echo $selectCase;
               // $objResult1 = $db->selectResult($tableName, 'SUM(rating_value) as total,count(rating_id) as count', $selectCase);
//echopre($objResult1);
                 $objResult1=$db->fetchAll($db->execute($query));
                if ($objResult1)
                    $objResult->data = $objResult1[0]; // Assign user record  to object
                else {
                    $objResult->status = "ERROR";
                    $objResult->message = USER_ID_NOT_EXIST; // User not exists
                }
            }
        }
            Logger::info($objResult);
            return $objResult;
      
      
      
      
  }
 
//  public static function checkIfRated($ratingEntityId,$ratingEntity,$ratedBy)
//  {
//  
//  
//  	$objResult                        =   new Messages();
//  	$objResult->status                =   SUCCESS;
//  
//  	if($ratingEntityId=='' || $ratingEntity == '') {
//  		 
//  		$objResult->status           =   "ERROR";
//  		$objResult->message          =   GROUP_ID_NOT_EXIST; // User Id not  Exists
//  		 
//  	}
//  
//  	if ($objResult->status == SUCCESS) {
//  
//  		if ($ratingEntityId != '') {
//  			$db = new Db();
//  			$tableName = 'rating';
//  			$selectCase = "  rating_entity ='" . $db->escapeString($ratingEntity) ." ' AND rating_entity_id = ".$db->escapeString($ratingEntityId) . " AND rating_rated_by = ".$db->escapeString($ratedBy);
//  			Logger::info($selectCase);
//  			//echo $selectCase;
//  			$objResult1 = $db->selectResult($tableName, 'rating_id, count(rating_id) as count', $selectCase);
//  
//  			if ($objResult)
//  				$objResult->data = $objResult1[0]; // Assign user record  to object
//  			else {
//  				$objResult->status = "ERROR";
//  				$objResult->message = USER_ID_NOT_EXIST; // User not exists
//  			}
//  		}
//  	}
//  	Logger::info($objResult);
//  	return $objResult;
//  
//  
//  
//  
//  }
   public static function checkIfRated($ratingEntityId,$ratingEntity,$ratedBy)
  {
  
  
  	$objResult                        =   new Messages();
  	$objResult->status                =   SUCCESS;
  
  	if($ratingEntityId=='' || $ratingEntity == '') {
  		 
  		$objResult->status           =   "ERROR";
  		$objResult->message          =   GROUP_ID_NOT_EXIST; // User Id not  Exists
  		 
  	}
  
  	if ($objResult->status == SUCCESS) {
  
  		if ($ratingEntityId != '') {
  			$db = new Db();
  			$tableName = Utils::setTablePrefix('rating');
                        $tableName1 = Utils::setTablePrefix('users');
                        $tableName2 = Utils::setTablePrefix('communities');
                        $tableName3 = Utils::setTablePrefix('businesses');
                        $tableName4 = Utils::setTablePrefix('community_member');
                        if($ratingEntity == 'C'){
                            $query2 = "SELECT count(cmember_id) as community_member_count FROM $tableName4 AS m JOIN $tableName2 AS c ON c.community_id = m.cmember_community_id WHERE c.community_id = ".$db->escapeString($ratingEntityId)." AND m.cmember_status!='P' AND m.cmember_id = ".$db->escapeString($ratedBy);
                            $objResult2=$db->fetchAll($db->execute($query2));
                            
                            if($objResult2[0]->community_member_count == 0){
  				$objResult->status = "ERROR";
  				$objResult->message = "You are not a member of this group";// Assign user record  to object
                        }else{
                            $query1 = "SELECT count(community_name) as community_count FROM $tableName2 AS c JOIN $tableName1 AS u ON u.user_id = c.community_created_by WHERE c.community_created_by ='". $db->escapeString($ratedBy) ."  ' AND c.community_id = ".$db->escapeString($ratingEntityId);
                            $objResult1=$db->fetchAll($db->execute($query1));
                       // echopre($objResult1);
                       // echo $objResult1[0]->community_count;exit;
  			if($objResult1[0]->community_count > 0){
  				$objResult->status = "ERROR";
  				$objResult->message = "You can't rate your own group";// Assign user record  to object
                        }
                        else{
                        //}
                        //}
                        $query = "SELECT rating_id, count(rating_id) as count FROM $tableName AS r JOIN $tableName1 AS u ON u.user_id = r.rating_rated_by AND user_status = 'A' WHERE rating_entity ='" . $db->escapeString($ratingEntity) ." ' AND rating_entity_id = ".$db->escapeString($ratingEntityId) . " AND rating_rated_by = ".$db->escapeString($ratedBy);
  			//$selectCase = "  rating_entity ='" . $db->escapeString($ratingEntity) ." ' AND rating_entity_id = ".$db->escapeString($ratingEntityId) . " AND rating_rated_by = ".$db->escapeString($ratedBy);
  			//Logger::info($selectCase);
  			//echo $query;exit;
  			//$objResult1 = $db->selectResult($tableName, 'rating_id, count(rating_id) as count', $selectCase);
                        $objResult=$db->fetchAll($db->execute($query));
                       // echopre1($objResult);
  			if ($objResult)
  				$objResult->data = $objResult1[0]; // Assign user record  to object
  			else {
  				$objResult->status = "ERROR";
  				$objResult->message = USER_ID_NOT_EXIST; // User not exists
  			}
                        }
                        }  
                }
                else{
                   $query1 = "SELECT count(business_name) as business_count FROM $tableName3 AS b JOIN $tableName1 AS u ON u.user_id = b.business_created_by WHERE b.business_created_by ='". $db->escapeString($ratedBy) ."  ' AND b.business_id = ".$db->escapeString($ratingEntityId);
                            $objResult1=$db->fetchAll($db->execute($query1));
                       // echopre($objResult1);
                       // echo $objResult1[0]->community_count;exit;
  			if($objResult1[0]->business_count > 0){
  				$objResult->status = "ERROR";
  				$objResult->message = "You can't rate your own page";// Assign user record  to object
                        }
                        else{
                        //}
                        //}
                        $query = "SELECT rating_id, count(rating_id) as count FROM $tableName AS r JOIN $tableName1 AS u ON u.user_id = r.rating_rated_by AND user_status = 'A' WHERE rating_entity ='" . $db->escapeString($ratingEntity) ." ' AND rating_entity_id = ".$db->escapeString($ratingEntityId) . " AND rating_rated_by = ".$db->escapeString($ratedBy);
  			//$selectCase = "  rating_entity ='" . $db->escapeString($ratingEntity) ." ' AND rating_entity_id = ".$db->escapeString($ratingEntityId) . " AND rating_rated_by = ".$db->escapeString($ratedBy);
  			//Logger::info($selectCase);
  			//echo $query;exit;
  			//$objResult1 = $db->selectResult($tableName, 'rating_id, count(rating_id) as count', $selectCase);
                        $objResult=$db->fetchAll($db->execute($query));
                       // echopre1($objResult);
  			if ($objResult)
  				$objResult->data = $objResult1[0]; // Assign user record  to object
  			else {
  				$objResult->status = "ERROR";
  				$objResult->message = USER_ID_NOT_EXIST; // User not exists
  			}
                        } 
                }
                
  		}
  	}
  	Logger::info($objResult);
  	return $objResult;
  
  
  
  
  }
	
}
?>