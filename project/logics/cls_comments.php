<?php 
/*
 * All Comments Entity Logics should come here.
 */
class Comments{
   /*
Function to Register a  new Comment
*/
 public static  function createComment($objCommentVo)
 {         
            $objResult                      =   new Messages();
            $objResult->status              =   SUCCESS;
     
                if($objResult->status==SUCCESS) {
                 $db                             =   new Db();

                 $tableName                      =   Utils::setTablePrefix('comments');
                 
                 $excludeId                      =    '';
                 
               


                 $commentId                     =   $db->insert($tableName, Utils::objectToArray($objCommentVo));
                 $objCommentVo->comment_id              =   $comment_id;
                 $objResult->data                 =   $objCommentVo;
          }
          Logger::info($objResult);
          return $objResult;
                
 }   
 
 
 
  /*
Function to edit Comment
   * Parameter -  Comment Data
*/
  public  function editComment($commentId='',$objCommentVo)
 {
      
      
       $objResult                      =   new Messages();
            $objResult->status              =   SUCCESS;
          
          
                    if($commentId == '') {
                        $objResult->status          =   "ERROR";
                        $objResult->message         =   COMMENT_ID_NOT_EXIST; 
                    }

        
                    $objCommentVo1 = Utils::objectToArray($objCommentVo);
        // Update Comment data to  Comment table 
                 
                if($objResult->status==SUCCESS) {

                 $db                             =   new Db();

                 $tableName                      =   Utils::setTablePrefix('comments');
                 $where                          =   "comment_id = ".$db->escapeString($commentId)." ";
                 $customerId                     =   $db->update($tableName, $objCommentVo1,$where,$doAudit=false);
//                  $customerId                     =   $db->update($tableName, array(  'comment_content'     =>  $db->escapeString($objCommentVo->comment_content),
//                                                                                     'comment_parent'      =>  $objCommentVo->comment_parent,
//                                                                                     'comment_created_on'  =>  $objCommentVo->community_status,
//                                                                                     'comment_created_by'  =>  $objCommentVo->comment_created_by,
//                                                                                     'comment_entity_id'   =>  $objCommentVo->comment_entity_id,
//                                                                                     'comment_status'      =>  $db->escapeString($objCommentVo->comment_status),
//                                                                                     'comment_entity_type' =>  $db->escapeString($objCommentVo->comment_entity_type)
//                                                                                   ),$where,$doAudit=false);
                 $objCommentVo->comment_id         =   $commentId;
                 $objResult->data                 =   $objCommentVo;

          }

          Logger::info($objResult);

          return $objResult;
     
                
 }  
 
  

  /*
Function to check Customer Exists or not if exists re-direct to login 
   * Parameters  : Comment Id 
*/
 public static function changeCommentStatus($commentId,$objCommentVo='')
 {     
            $objResult                      =   new Messages();
            $objResult->status              =   SUCCESS;
         //  Check Entered Comment Id  exists in the system
         if($commentId=='') {  
             
                                 $objResult->status           =   "ERROR";
                                 $objResult->message          =   COMMENT_ID_NOT_EXIST; // Comment Id not  Exists
                         
         }         
        // Update Comment status to deleted  if comment  already exists
                if($objResult->status==SUCCESS) {

                 $db                             =   new Db();
                 $tableName                      =   Utils::setTablePrefix('comments');
                 $selectCase                     =   '';
              
                 $where                          =  "comment_id='".$db->escapeString($commentId)."' ";    
                 $resultRows                     =  $db->update($tableName, array( 'comment_status'     =>  $db->escapeString($objCommentVo->comment_status),
                                                                                  ),$where,$doAudit=false);
                 // Update  Comment Account with deleted status  and Set Corresponding Message
                 if($resultRows>0)
                 { 
                      $objResult->message        =   COMMENT_DELETED; // Comment Deleted Successfully
                 }
                 
          }

            Logger::info($objResult);
            return $objResult;

 }
 
 public static function changeTestimonialStatus($testimonialId,$objTestimonialVo='')
 {     
            $objResult                      =   new Messages();
            $objResult->status              =   SUCCESS;
         //  Check Entered Comment Id  exists in the system
         if($testimonialId=='') {  
             
                                 $objResult->status           =   "ERROR";
                                 $objResult->message          =   TESTIMONIAL_ID_NOT_EXIST; // Comment Id not  Exists
                         
         }         
        // Update Comment status to deleted  if comment  already exists
                if($objResult->status==SUCCESS) {

                 $db                             =   new Db();
                 $tableName                      =   Utils::setTablePrefix('business_testimonials');
                 $selectCase                     =   '';
              
                 $where                          =  "testimonial_id='".$db->escapeString($testimonialId)."' ";    
                 $resultRows                     =  $db->update($tableName, array( 'testimonial_status'     =>  $db->escapeString($objTestimonialVo->testimonial_status),
                                                                                  ),$where,$doAudit=false);
                 // Update  Comment Account with deleted status  and Set Corresponding Message
                 if($resultRows>0)
                 { 
                      $objResult->message        =   COMMENT_DELETED; // Comment Deleted Successfully
                 }
                 
          }

            Logger::info($objResult);
            return $objResult;

 }
 
 public  function listComment($objTableVo)
 {    
            if($objTableVo->coulmns=='')
            {
                $objTableVo->coulmns  = '*';
            }
            
             $objTableVo->tablename          =   'comments';
             $objResult                      =   new Messages();
             $objResult->status              =   SUCCESS;
             $db                             =   new Db();
             $objResultRows                  =   $db->getPagingData($objTableVo->coulmns,$objTableVo->tablename,$objTableVo->join,$objTableVo->criteria,$objTableVo->community_by,$objTableVo->sort_order,$objTableVo->sort_field,$objTableVo->limit);
            //Check Comment Details exist 
                  if($objResultRows)
              // Assign Comment details to object
                      $objResult->data             =    $objResultRows;          
                 else {
                        $objResult->status         =   "ERROR";
                        $objResult->message        =   USER_NOT_EXIST; // Store details not exists 
                     }
       
            Logger::info($objResult);
          
            return $objResult;
 } 
 /*
Function to get Comment details
   * Parameters  : Comment id
*/
 public static function getComment($commentId)
 {         
                $objResult                        =   new Messages();
                $objResult->status                =   SUCCESS;
         //Select Comment Record If Comment id is not null
                if($commentId!='') {
                 $db                             =   new Db();
                 $tableName                      =   'comments';
                 $selectCase                     =   '  comment_id ='.$db->escapeString($commentId).' ';
                 Logger::info($selectCase);
                 
                 $objResultRow                   =  $db->selectRecord($tableName,'*',$selectCase);
         // If comment exists then assign comment data to result object
                 if($objResultRow)
                      $objResult->data           =   $objResultRow; // Assign comment record  to object
                 else{
                     $objResult->status          =   "ERROR";
                     $objResult->message         =   USER_ID_NOT_EXIST; // Comment not exists
                  }
          }

            Logger::info($objResult);
            return $objResult;
 }	
 
 
 
  public static function getAllComments($orderfield='comment_id',$orderby='ASC',$pagenum=1,$itemperpage=10,$search='',$searchFieldArray,$entity_type='',$entity_id='') {
      
        $db = new Db();
        
         $files                      =   Utils::setTablePrefix('files');
        
         $users                      =   Utils::setTablePrefix('users');
        
         if($entity_type)
         {
             $userWhere = " AND c.comment_entity_type = '".$entity_type."' AND  c.comment_entity_id = '".$entity_id."' ";
         }else{
             $userWhere = '';
         }
         
         
         
        $objLeadData                = new stdClass();
        $objLeadData->table         = 'comments AS c';
        $objLeadData->key           = 'c.comment_id';
        $objLeadData->fields	    = 'c.*,f.*,u.*';
        $objLeadData->join	    = "LEFT JOIN $users AS u ON c.comment_created_by = u.user_id LEFT JOIN $files as f ON u.user_image_id = f.file_id";
        $objLeadData->where	    = "c.comment_status = 'A' $userWhere ";
     //   echo $objLeadData->where;
        
        if($search!=''){
            $objLeadData->where	  .=  " AND (";
            $fieldsize      =   sizeof($searchFieldArray);
            $i=1;
            foreach($searchFieldArray AS $searchField){
               $objLeadData->where	  .=  " $searchField LIKE '%$search%' "; 
                if($i==$fieldsize){
                   $objLeadData->where	  .= " )" ;
                }
                else{
                    $objLeadData->where	  .= " OR" ;
                }
               $i++;
            }
        }
        $objLeadData->groupbyfield  = '';
        $objLeadData->orderby	    = $orderby;
        $objLeadData->orderfield    = $orderfield;
        $objLeadData->itemperpage   = $itemperpage;
        $objLeadData->page	    = $pagenum;		// by default its 1
        $objLeadData->debug	    = true;
        $leads                      = $db->getData($objLeadData);
         // echopre($leads);
        //return $leads->records;
        return $leads;
    }
    
      public static  function createTestimonial($objCommentVo)
 {         
            $objResult                      =   new Messages();
            $objResult->status              =   SUCCESS;
     
                if($objResult->status==SUCCESS) {
                 $db                             =   new Db();

                 $tableName                      =   Utils::setTablePrefix('business_testimonials');
                 
                 $excludeId                      =    '';
                 
               
                 //echopre1($objCommentVo);

                 $commentId                     =   $db->insert($tableName, Utils::objectToArray($objCommentVo));
                 $objCommentVo->comment_id              =   $commentId;
                 $objResult->data                 =   $objCommentVo;
          }
          Logger::info($objResult);
          return $objResult;
                
 }
  public static function getAllTestimonials($orderfield='testimonial_id',$orderby='ASC',$pagenum=1,$itemperpage=10,$search='',$searchFieldArray,$business_id) {
      //echo $business_id;
        $db = new Db();
        $files                      =   Utils::setTablePrefix('files');
        $users                      =   Utils::setTablePrefix('users');
        $objLeadData                = new stdClass();
        $objLeadData->table         = 'business_testimonials AS b';
        $objLeadData->key           = 'b.testimonial_id';
        $objLeadData->fields	    = 'b.*,f.*,u.*';
        $objLeadData->join	    = "LEFT JOIN $users AS u ON b.user_id = u.user_id LEFT JOIN $files as f ON u.user_image_id = f.file_id";
        $objLeadData->where	    = "b.testimonial_status = 'A' AND business_id = ".$db->escapeString($business_id)." ";
     //   echo $objLeadData->where;
        
        if($search!=''){
            $objLeadData->where	  .=  " AND (";
            $fieldsize      =   sizeof($searchFieldArray);
            $i=1;
            foreach($searchFieldArray AS $searchField){
               $objLeadData->where	  .=  " $searchField LIKE '%$search%' "; 
                if($i==$fieldsize){
                   $objLeadData->where	  .= " )" ;
                }
                else{
                    $objLeadData->where	  .= " OR" ;
                }
               $i++;
            }
        }
        $objLeadData->groupbyfield  = '';
        $objLeadData->orderby	    = $orderby;
        $objLeadData->orderfield    = $orderfield;
        $objLeadData->itemperpage   = $itemperpage;
        $objLeadData->page	    = $pagenum;		// by default its 1
        $objLeadData->debug	    = true;
        $leads                      = $db->getData($objLeadData);
         // echopre($leads);
        //return $leads->records;
        return $leads;
    }
 
public  function editTestimonial($testimonialId='',$objTestimonialVo)
 {
      
      
       $objResult                      =   new Messages();
            $objResult->status              =   SUCCESS;
          
          
                    if($testimonialId == '') {
                        $objResult->status          =   "ERROR";
                        $objResult->message         =   TESTIMONIAL_ID_NOT_EXIST; 
                    }

        
                    $objTestimonialVo1 = Utils::objectToArray($objTestimonialVo);
        // Update Comment data to  Comment table 
                 
                if($objResult->status==SUCCESS) {

                 $db                             =   new Db();

                 $tableName                      =   Utils::setTablePrefix('business_testimonials');
                 $where                          =   "testimonial_id = ".$db->escapeString($testimonialId)." ";
                 $customerId                     =   $db->update($tableName, $objTestimonialVo1,$where,$doAudit=false);
//                  $customerId                     =   $db->update($tableName, array(  'comment_content'     =>  $db->escapeString($objCommentVo->comment_content),
//                                                                                     'comment_parent'      =>  $objCommentVo->comment_parent,
//                                                                                     'comment_created_on'  =>  $objCommentVo->community_status,
//                                                                                     'comment_created_by'  =>  $objCommentVo->comment_created_by,
//                                                                                     'comment_entity_id'   =>  $objCommentVo->comment_entity_id,
//                                                                                     'comment_status'      =>  $db->escapeString($objCommentVo->comment_status),
//                                                                                     'comment_entity_type' =>  $db->escapeString($objCommentVo->comment_entity_type)
//                                                                                   ),$where,$doAudit=false);
                 $objTestimonialVo->testimonial_id         =   $testimonialId;
                 $objResult->data                 =   $objTestimonialVo;

          }

          Logger::info($objResult);

          return $objResult;
     
                
 }  	
}
?>