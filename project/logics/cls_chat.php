<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

// +----------------------------------------------------------------------+
// | File name : Utils.php                                         		  |
// | PHP version >= 5.2                                                   |
// +----------------------------------------------------------------------+
// | Author: ARUN SADASIVAN<arun.s@armiasystems.com>              		  |
// +----------------------------------------------------------------------+
// | Copyrights Armia Systems � 2010                                    |
// | All rights reserved                                                  |
// +----------------------------------------------------------------------+
// | This script may not be distributed, sold, given away for free to     |
// | third party, or used as a part of any internet services such as      |
// | webdesign etc.                                                       |
// +----------------------------------------------------------------------+

class Chat {

    public static function addChatlog($from, $to, $message) {
        $db = new Db();
        $tableName = Utils::setTablePrefix('users_chat');

        $query = "INSERT into $tableName ({$tableName}.from, {$tableName}.to, message, sent) values ('" . $db->escapeString($from) . "', '" . $db->escapeString($to) . "','" . $db->escapeString($message) . "',NOW())";
        $db->execute($query);
    }

    public static function chatStatusUpdate($uid) {
        $db = new Db();
        $tableName = Utils::setTablePrefix('users_chat');

        $query = "UPDATE $tableName set recd = 1 where {$tableName}.to = '$uid' and recd = 0";
        $db->execute($query);
    }

    public static function getChatlog($toID) {
        $db = new Db();
        $tableName = Utils::setTablePrefix('users_chat');
        
        $query = "SELECT * from $tableName where ({$tableName}.to = '" . $db->escapeString($toID) . "' AND recd = 0) order by id ASC";
        $objResultRow = $db->fetchAll($db->execute($query));
        return $objResultRow;
    }
    
    public static function getUsersList() {
        $db = new Db();
        $tableName = Utils::setTablePrefix('users');
        
        $query = "SELECT * from $tableName";
        $objResultRow = $db->fetchAll($db->execute($query));
        return $objResultRow;
    }

}
?>