<?php 

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
// +----------------------------------------------------------------------+
// | File name : cls_mailer.php                                         		  |
// | PHP version >= 5.2                                                   |
// +----------------------------------------------------------------------+
// +----------------------------------------------------------------------+
// | Copyrights Armia Systems                                      |
// | All rights reserved                                                  |
// +----------------------------------------------------------------------+
// | This script may not be distributed, sold, given away for free to     |
// | third party, or used as a part of any internet services such as      |
// | webdesign etc.                                                       |
// +----------------------------------------------------------------------+

class Mailer{
	
	public $siteName;
	public $siteLogo;
	public $siteDate;
	public $siteCopyRight;
	public $mailSignature;
	 
	
	
	public function  __construct() {
		 
		// common parameters
		$this->siteName 		= SITE_NAME;
                $this->site_url                 = BASE_URL;
		$this->siteLogo 		='<img src="'.SITE_LOGO.'">';
		$this->siteCopyRight 	= '&copy; '.SITE_NAME.' '.date('Y').' All rights reserved';
		if(Utils::getAuth('user_country')=='UK'){
			date_default_timezone_set('America/Los_Angeles');
			$this->siteDate 		= date("m/d/Y");
		}else{
			$this->siteDate 		= date("d/m/Y");
			date_default_timezone_set('Europe/London');
		}
		$this->mailSignature 	= '<p style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#333333">Thank You,<br>
The '.SITE_NAME.' Team | '.BASE_URL.'</p>';
			
	}
    
	
    /*
     * function to send mail
     */
    public function sendMail($mailIds,$template,$replaceparams) {
    	PageContext::includePath('phpmailer');
    	$db                = new Db();
        // get the mail template
    	$mailTemplate      = $db->selectRecord("mail_template", "*", "mail_template_name='".$template."' AND mail_template_status=1");
    	$replaceparams['SIGNATURE'] 		= $this->mailSignature;
        $replaceparams['SITE_NAME'] 		= $this->siteName ;
        $replaceparams['SITE_URL'] 		= $this->site_url ;
        $replaceparams['SITE_LOGO'] 		= $this->siteLogo ;
    	
    	if(sizeof($replaceparams) > 0) { 
    		foreach($replaceparams as $key=>$parms) { 
     			$mailTemplate->mail_template_body 	= str_replace('['.$key.']',$parms, $mailTemplate->mail_template_body);
     			$mailTemplate->mail_template_sub 	= str_replace('['.$key.']',$parms, $mailTemplate->mail_template_sub);
    		}
    	}
    	  
    	$emailBody 				= Mailer::prepareMail($mailTemplate->mail_template_body);  
    	 
        
  //echo $emailBody;
        
//        $adfromemail          	= $db->selectRow("lookup","vLookUp_Value","vLookUp_Name='addressfromemail'");
//        $adfromemailname      	= $db->selectRow("lookup","vLookUp_Value","vLookUp_Name='addressfromemailname'");
//        $adreplyemail         	= $db->selectRow("lookup","vLookUp_Value","vLookUp_Name='addressreplyemail'");
//        $adreplyemailname     	= $db->selectRow("lookup","vLookUp_Value","vLookUp_Name='addressreplyemailname'");
          
		$mailBody   			= $emailBody;
								
        $mailSubject			= $mailTemplate->mail_template_sub;
 
        $mail                           = new PHPMailer();
        $mail->IsSMTP();
	    	$mail->Host             = SMTP_HOST; 		// SMTP server example
			//$mail->SMTPDebug  = 2;      // enables SMTP debug information (for testing)
			$mail->SMTPAuth   = true;                  		// enable SMTP authentication
			$mail->Port       = SMTP_PORT;       // set the SMTP port for the GMAIL server
			$mail->Username   = SMTP_USERNAME; 	// SMTP account username example
			$mail->Password   = SMTP_PASSWORD;   // SMTP account password example
			//$mail->SMTPSecure = 'ssl';
        $mail->AddReplyTo(ADMIN_EMAILS,SITE_NAME);
        $mail->SetFrom(ADMIN_EMAILS,SITE_NAME);
       
        foreach($mailIds as $key=>$name)
        	$mail->AddAddress($key, $name);
        $mail->Subject              = $mailSubject;
        $mail->AltBody              = ''; // Optional, comment out and test.
        
      
     
    	$mail->MsgHTML($mailBody);
        $mailBody = stripslashes($mailBody);
       // echopre1($mailBody);
    	if($mailTemplate->mail_template_status == 1){
        	$mailsent           = $mail->Send();
        	return true;
        	$flag = '1';
    	}else{
    		$flag = '0';
    		return $flag;
    	}
    	 
    	 
    } 
    
    public function getstatus($template){
    	PageContext::includePath('phpmailer');
    	$db                = new Db();
    	
    	// get the mail template
    	$mailTemplate      = $db->selectRecord("mail_template", "mail_template_status", "mail_template_name='".$template."'");
    	return $mailTemplate->mail_template_status;
    	
    }

   public function sendInquiryMail($businessObj,$from,$fromEmail,$message)
   {
       PageContext::includePath('phpmailer');
    	$db                = new Db();
    
        // get the mail template
    	$mailTemplate      = $db->selectRecord("mail_template", "*", "mail_template_name='".$template."' AND mail_template_status=1");
   
    	$replaceparams['SIGNATURE'] 		= $this->mailSignature;
        $replaceparams['SITE_NAME'] 		= $this->siteName ;
        $replaceparams['SITE_URL'] 		= $this->site_url ;
    	//$replaceparams['CURRENCY'] 			= DEFAULT_CURRENCY;
    	
//    	if(sizeof($replaceparams) > 0) { 
//    		foreach($replaceparams as $key=>$parms) { 
//     			$mailTemplate->mail_template_body 	= str_replace('['.$key.']',$parms, $mailTemplate->mail_template_body);
//     			$mailTemplate->mail_template_sub 	= str_replace('['.$key.']',$parms, $mailTemplate->mail_template_sub);
//    		}
//    	}
//    	  
//    	$emailBody 				= Mailer::prepareMail($mailTemplate->mail_template_body);  
//    	 
//        
//  //echo $emailBody;
//        
////        $adfromemail          	= $db->selectRow("lookup","vLookUp_Value","vLookUp_Name='addressfromemail'");
////        $adfromemailname      	= $db->selectRow("lookup","vLookUp_Value","vLookUp_Name='addressfromemailname'");
////        $adreplyemail         	= $db->selectRow("lookup","vLookUp_Value","vLookUp_Name='addressreplyemail'");
////        $adreplyemailname     	= $db->selectRow("lookup","vLookUp_Value","vLookUp_Name='addressreplyemailname'");
//          
//		$mailBody   			= $emailBody;
//		
//		
//			
//		
//        $mailSubject			= $mailTemplate->mail_template_sub;
 
        $mail               	= new PHPMailer();
        $mail->IsSMTP();
	    	$mail->Host       = SMTP_HOST; 		// SMTP server example
			//$mail->SMTPDebug  = 2;      // enables SMTP debug information (for testing)
			$mail->SMTPAuth   = true;                  		// enable SMTP authentication
			$mail->Port       = SMTP_PORT;       // set the SMTP port for the GMAIL server
			$mail->Username   = SMTP_USERNAME; 	// SMTP account username example
			$mail->Password   = SMTP_PASSWORD;   // SMTP account password example
			//$mail->SMTPSecure = 'ssl';
        $mail->AddReplyTo($fromEmail,$from);
        $mail->SetFrom(ADMIN_EMAILS,SITE_NAME);
       
        
        	$mail->AddAddress($businessObj->business_email, $businessObj->business_name);
        $mail->Subject              = "Inquiry From $from";
        $mail->AltBody              = ''; // Optional, comment out and test.
    	$mail->MsgHTML($message);
    	
        $mailsent           = $mail->Send();
       
        return true;
    	 
       
       
   }




   /*
     * function to add the mail template with mail body
     */
	public function prepareMail($mailBody) { 
 		$db                		= new Db();
		// get the mail body
    	$mailContainer      	= $db->selectRecord("mail_template", "*", "mail_template_name='mailcontainer' AND mail_template_status=1");
        
		$arrTSearch     		= array("[SITE_LOGO]", "[COPYRIGHT]", "[DATE]");
        $arrTReplace    		= array($this->siteLogo ,$this->siteCopyRight, $this->siteDate  );
        
		foreach($arrTSearch as $tempkey =>$tempValue) {
        	$mailContainer->mail_template_body = str_replace($tempValue, $arrTReplace[$tempkey], $mailContainer->mail_template_body);
        }
       	$emailBody 				= str_replace('[MAIL_BODY]', $mailBody, $mailContainer->mail_template_body);
		return $emailBody;
    }
    
    public function getMailTemplate($template){
        // get the mail template
        $db                       = new Db();
    	return $mailTemplate      = $db->selectRecord("mail_template", "*", "mail_template_name='".$template."' AND mail_template_status=1");
    }
    
    
    public function sendIntroductionMail($toMail,$toName='',$subject,$message) {
        PageContext::includePath('phpmailer');
        $db = new Db();
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->Host = SMTP_HOST;   // SMTP server example
        //$mail->SMTPDebug  = 2;      // enables SMTP debug information (for testing)
        $mail->SMTPAuth = true;                    // enable SMTP authentication
        $mail->Port = SMTP_PORT;       // set the SMTP port for the GMAIL server
        $mail->Username = SMTP_USERNAME;  // SMTP account username example
        $mail->Password = SMTP_PASSWORD;   // SMTP account password example
        //$mail->SMTPSecure = 'ssl';
        $mail->AddReplyTo(ADMIN_EMAILS, SITE_NAME);
        $mail->SetFrom(ADMIN_EMAILS, SITE_NAME);
        $emailBody = Mailer::prepareMail($message);

        $mail->AddAddress($toMail,$toName);
        //$mail->AddAddress('smitha.m@armiasystems.com','Smitha');
        $mail->Subject = $subject;
        $mail->AltBody = ''; // Optional, comment out and test.
        $mail->MsgHTML($emailBody);

        $mailsent = $mail->Send();

        return true;
    }
    

    
    
        
}


?>
