<?php 
/*
 * All Group Entity Logics should come here.
 */
class Groups{
   /*
Function to Register a  new Group
*/
   public static  function createGroup($objGroupVo)
 {         
            $objResult                      =   new Messages();
            $objResult->status              =   SUCCESS;
     
                if($objResult->status==SUCCESS) {
                 $db                             =   new Db();

                 $tableName                      =   Utils::setTablePrefix('groups');
                 
                 $excludeId                      =    '';
                 
                 $GroupAliasName                  =   Utils::generateAlias($tableName,$objGroupVo->group_name, $idColumn="group_id",$id = $excludeId, $aliasColumn="group_alias");


                 $groupId                     =   $db->insert($tableName, array( 'group_name'     =>  Utils::escapeString($objGroupVo->group_name),
                                                                                     'group_status'     =>  Utils::escapeString($objGroupVo->group_status),
                                                                                     'group_image_id'    => $objGroupVo->group_image_id,
                                                                                     'group_created_by'    =>  $objGroupVo->group_created_by,
                                                                                     'group_created_on'    =>  $objGroupVo->group_created_on,
                                                                                     'group_alias'         =>  Utils::escapeString($GroupAliasName)
                                                                                  ));
                 $objUserVo->group_id              =   $groupId;
                 $objResult->data                 =   $objGroupVo;
          }
          Logger::info($objResult);
          return $objResult;
                
 }   
 
 
 
  /*
Function to edit user profile information
   * Parameter -  Group Data
*/
  public  function editGroup($groupId='',$objGroupVo)
 {
      
      
       $objResult                      =   new Messages();
            $objResult->status              =   SUCCESS;
          //Check Entered User Email Exists OR NOT then stop user sign up if email exists
          
                    if($groupId == '') {
                        $objResult->status          =   "ERROR";
                        $objResult->message         =   GROUP_ID_NOT_EXIST; // Email Alreday Exists
                    }

        
                    
        // Update Group data to  User table 
                 
                if($objResult->status==SUCCESS) {

                 $db                             =   new Db();

                 $tableName                      =   Utils::setTablePrefix('groups');
                 $where                          =   "group_id = ".Utils::escapeString($groupId)." ";
                 $customerId                     =   $db->update($tableName, array( 'group_name'     =>  Utils::escapeString($objGroupVo->group_name),
                                                                                     'group_status'     =>  Utils::escapeString($objGroupVo->group_status),
                                                                                     'group_image_id'    => $objGroupVo->group_image_id,
                                                                                     'group_created_by'    =>  $objGroupVo->group_created_by,
                                                                                     'group_created_on'    =>  $objGroupVo->group_created_on,
                                                                                  
                                                                                   ),$where,$doAudit=false);
                 $objUserVo->group_id         =   $groupId;
                 $objResult->data                 =   $objGroupVo;

          }

          Logger::info($objResult);

          return $objResult;
     
                
 }  
 
  /*
Function to check Customer Exists or not if exists re-direct to login 
   * Parameters  : Username , Email Address , Password
*/

  /*
Function to check Customer Exists or not if exists re-direct to login 
   * Parameters  : User Id 
*/
 public static function changeGroupStatus($groupId,$objGroupVo='')
 {         
            $objResult                      =   new Messages();
            $objResult->status              =   SUCCESS;
         //  Check Entered User Id  exists in the system
         if($userId!='') {  
             
                                 $objResult->status           =   "ERROR";
                                 $objResult->message          =   GROUP_ID_NOT_EXIST; // User Id not  Exists
                         
         }         
        // Update User status to deleted  if user  already exists
                if($objResult->status==SUCCESS) {

                 $db                             =   new Db();
                 $tableName                      =   Utils::setTablePrefix('groups');
                 $selectCase                     =   '';
              
                 $where                          =  "group_id='".Utils::escapeString($groupId)."' ";    
                 $resultRows                     =  $db->update($tableName, array( 'group_status'     =>  Utils::escapeString($objGroupVo->group_status),
                                                                                  ),$where,$doAudit=false);
                 // Update  User Account with deleted status  and Set Corresponding Message
                 if($resultRows>0)
                 { 
                      $objResult->message        =   GROUP_DELETED_SUCCESS; // User Deleted Successfully
                 }
                 
          }

            Logger::info($objResult);
            return $objResult;

 }
 
 public  function listGroups($objTableVo)
 {    
            if($objTableVo->coulmns=='')
            {
                $objTableVo->coulmns  = '*';
            }
            
             $objTableVo->tablename          =   'groups';
             $objResult                      =   new Messages();
             $objResult->status              =   SUCCESS;
             $db                             =   new Db();
             $objResultRows                  =   $db->getPagingData($objTableVo->coulmns,$objTableVo->tablename,$objTableVo->join,$objTableVo->criteria,$objTableVo->group_by,$objTableVo->sort_order,$objTableVo->sort_field,$objTableVo->limit);
            //Check User Details exist 
                  if($objResultRows)
              // Assign User details to object
                      $objResult->data             =    $objResultRows;          
                 else {
                        $objResult->status         =   "ERROR";
                        $objResult->message        =   GROUP_NOT_EXIST; // Store details not exists 
                     }
       
            Logger::info($objResult);
          
            return $objResult;
 } 
 /*
Function to get Group details
   * Parameters  : Group id
*/
 public static function getGroup($groupId)
 {         
                $objResult                        =   new Messages();
                $objResult->status                =   SUCCESS;
         //Select User Record If User id is not null
                if($userId!='') {
                 $db                             =   new Db();
                 $tableName                      =   'groups';
                 $selectCase                     =   '  group_id ='.Utils::escapeString($groupId).' ';
                 Logger::info($selectCase);
                 
                 $objResultRow                   =  $db->selectRecord($tableName,'*',$selectCase);
         // If user exists then assign user data to result object
                 if($objResultRow)
                      $objResult->data           =   $objResultRow; // Assign user record  to object
                 else{
                     $objResult->status          =   "ERROR";
                     $objResult->message         =   GROUP_ID_NOT_EXIST; // User not exists
                  }
          }

            Logger::info($objResult);
            return $objResult;
 }
 
  public static function addMembertoGroup($objMemVo){
      
       $objResult                        =   new Messages();
       $objResult->status                =   SUCCESS;
       if($objMemVo->gmember_id == '' || $objMemVo->gmember_group_id =='') {  
             
                                 $objResult->status           =   "ERROR";
                                 $objResult->message          =   GROUP_ID_NOT_EXIST; // User Id not  Exists
                         
         }   
      
       if($objResult->status==SUCCESS) {

                 $db                             =   new Db();
                 $tableName                      =   Utils::setTablePrefix('group_member');
                 $communityId                     =   $db->insert($tableName, array( 'gmember_id'     =>  $objMemVo->gmember_id,
                                                                                     'gmember_group_id'     =>  $objMemVo->gmember_group_id,
                                                                                     'gmember_type'    => Utils::escapeString($objMemVo->gmember_type),
                                                                                     'gmember_created_on'    =>  $objMemVo->gmember_created_on,
                                                                                     'gmember_created_by'    =>  $objMemVo->gmember_created_by,
                                                                                     'gmember_status'         =>  Utils::escapeString($objMemVo->gmember_status)
                                                                                  ));
                 $objUserVo->gmember_list_id      =   $communityId;
                 $objResult->data                 =   $objMemVo;
                 
          }

            Logger::info($objResult);
            return $objResult;
  }
 
 public  function chanegeMemeberGroupStatus($objMemVo)
 {
      
      
       $objResult                      =   new Messages();
            $objResult->status              =   SUCCESS;
          //Check Entered User Email Exists OR NOT then stop user sign up if email exists
          
                   if($objMemVo->gmember_id == '' || $objMemVo->gmember_group_id =='') {  
                        $objResult->status          =   "ERROR";
                        $objResult->message         =   GROUP_ID_NOT_EXIST; // Email Alreday Exists
                    }

        
                    
        // Update Community data to  User table 
                 
                if($objResult->status==SUCCESS) {

                 $db                             =   new Db();

                 $tableName                      =   Utils::setTablePrefix('group_member');
                 $where                          =   "gmember_id = ".Utils::escapeString($objMemVo->gmember_id)." AND gmember_group_id = ".Utils::escapeString($objMemVo->gmember_group_id)." ";
                 $customerId                     =   $db->update($tableName, array( 'gmember_status'     =>  Utils::escapeString($objCommunityVo->gmember_status),
                                                                                     
                                                                                   ),$where,$doAudit=false);
                 $objUserVo->gmember_id         =   $objMemVo->gmember_id;
                 $objResult->data                 =   $objCommunityVo;

          }

          Logger::info($objResult);

          return $objResult;
     
                
 }  
 
	
}
?>