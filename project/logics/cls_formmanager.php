<?php

// +----------------------------------------------------------------------+
// | File name : CMS	                                          		  |
// | 					 	  |
// | PHP version >= 5.2                                                   |
// +----------------------------------------------------------------------+
// +----------------------------------------------------------------------+
// | Copyrights Armia Systems  2010                                    |
// | All rights reserved                                                  |
// +----------------------------------------------------------------------+
// | This script may not be distributed, sold, given away for free to     |
// | third party, or used as a part of any internet services such as      |
// | webdesign etc.                                                       |
// +----------------------------------------------------------------------+

class FormManager {

    public $formElements = array();
    public $formProperties = array();
    public $validationJsCode = '';
    public $formId;
    public $formErrorMessages = array();

    public function __construct($arrForm = array()) {
        $this->formProperties = $arrForm;
        $this->formId = $arrForm['id'];
    }

    /*
     * function to assign the form fields
     */

    public function addFields($formelements) {
        $this->formElements = $formelements;
    }

    public function addValidationMessages($formMessages) {

        $this->formErrorMessages = $formMessages['messages'];
    }

    /*
     * function to render the form
     */

    public function renderForm($fields = array()) {
        $formFields = $this->formElements;
        $formStarttag = $this->generateFormProperites($this->formProperties);
        $formElements = $formStarttag;
        foreach ($formFields as $key => $fields) {
            $type = $fields['type'];
            $formElements.= $this->generateFormField($type, $fields);
        }
        $formEndTag = '</form>' . $this->validationJsCode;
        $formElements .= $formEndTag;
        return $formElements;
    }

    /*
     * function to genertate the form fields
     */

    public function generateFormField($field, $params = array()) {
        $formElements = '';
        if ($field != '') {
            if ($field == 'text') {    // textbox field processing
                $formElements = $this->generateField($field, $params);
                $formField = '<div>' . $formElements . '</div>';
            } else if ($field == 'password') { // password field processing
                $formElements = $this->generateField($field, $params);
                $formField = '<div>' . $formElements . '</div>';
            } else if ($field == 'submit') { // submit button field processing
                $formElements = $this->generateSubmitField($field, $params);
                $formField = $formElements;
            } else if ($field == 'button') { // normal button field processing
                $formElements = $this->generateField($field, $params);
                $formField = $formElements;
            } else if ($field == 'textarea') { // text area field processing
                $formElements = $this->generateTextArea($field, $params);
                $formField = '<div>' . $formElements . '</div>';
            } else if ($field == 'hidden') { // hidden field processing
                $formElements = $this->generateField($field, $params);
                $formField = '<div>' . $formElements . '</div>';
            } else if ($field == 'select') { // hidden field processing
                $formElements = $this->generateSelectField($field, $params);
                $formField = '<div>' . $formElements . '</div>';
            } else if ($field == 'checkbox') { // hidden field processing
                $formElements = $this->generateCheckBoxField($field, $params);
                $formField = '<div>' . $formElements . '</div>';
            } else if ($field == 'file') {    // file field processing
                $formElements = $this->generateField($field, $params);
                $formField = '<div>' . $formElements . '</div>';
            } else if ($field == 'image') {    // file field processing
                $formElements = $this->generateImage($field, $params);
                $formField = '<div>' . $formElements . '</div>';
            } else if ($field == 'link') {    // file field processing
                $formElements = $this->generateLink($field, $params);
                $formField = '<div>' . $formElements . '</div>';
            }
        }
        return $formField;
    }

    /*
     * function to create labels
     */

    public function createLabel($arrLabel) {
        return '<div class="abtlabel"><label class="' . $arrLabel['class'] . '">' . $arrLabel['message'] . '</label></div>';
    }

    /*
     * function to generate the field.
     * It also processes the validation code.
     */

    public function generateField($type, $arrPwdParams) {
        $fieldStart = '<input  ';
        if (sizeof($arrPwdParams) > 0) {
            foreach ($arrPwdParams as $key => $params) {
                if ($key == 'label'){
                    if($params['message']!=''){
                        $label = $this->createLabel($params);
                    }
                }
                else if ($key == 'validations') {
                    $arrValCode = $this->createValidationsCode($arrPwdParams['name'], $params);
                    $paramValidations = $arrValCode[0];
                }
                 else if($key == 'title'){
                    $helptext  ='<img src="'.BASE_URL.'modules/cms/images/help_icon.png" title="'.$params.'" class="masterTooltip">';
                }
                else {
                    if ($key == 'class')
                        $paramClass = $params;
                    else
                        $parameters .= $key . '="' . $params . '" ';
                }
            }
        }
        $fieldClassed = 'class="' . $paramClass . ' ' . $paramValidations . '"';
        $fieldEnd = $fieldClassed . '>' . $arrValCode[1];
        //return $label.$arrValCode[2].$fieldStart.$parameters.$fieldEnd;
        return $label . $fieldStart . $parameters . $fieldEnd.$helptext;
    }

    /*
     * function to generate the text areas
     */

    public function generateTextArea($type, $arrPwdParams) {
        //echopre($arrPwdParams);
        $fieldStart = '<textarea ';
        if (sizeof($arrPwdParams) > 0) {
            foreach ($arrPwdParams as $key => $params) {
                if ($key == 'label')
                    $label = $this->createLabel($params);
                else if ($key == 'validations') {
                    $arrValCode = $this->createValidationsCode($arrPwdParams['name'], $params);
                    $paramValidations = $arrValCode[0];
                }
                 else if ($key == 'value') {
                   $paramValue = $params;
                }
                else {
                    if ($key == 'class')
                        $paramClass = $params;
                    else
                        $parameters .= $key . '="' . $params . '" ';
                }
            }
        }
        $fieldClassed = 'class="' . $paramClass . ' ' . $paramValidations . '"';
        $fieldEnd = $fieldClassed . '>'.$paramValue.'</textarea>' . $arrValCode[1];
        //return $label . $arrValCode[2] . $fieldStart . $parameters . $fieldEnd;
        return $label . $fieldStart . $parameters . $fieldEnd;
    }
    /*
     * function to generate the image tags
     */

    public function generateImage($type, $arrPwdParams) {
        //echopre($arrPwdParams);
        $fieldStart = '<img ';
        if (sizeof($arrPwdParams) > 0) {
            foreach ($arrPwdParams as $key => $params) {
                if ($key == 'label')
                    $label = $this->createLabel($params);
                else if($key == 'src'){
                	$paramSrc = $params;
                }
                else {
                    if ($key == 'class')
                        $paramClass = $params;
                    else
                        $parameters .= $key . '="' . $params . '" ';
                }
            }
        }
        $fieldClassed = 'class="' . $paramClass . '"';
        $fieldSrc = 'src="' . $paramSrc . '"';
        $fieldEnd = $fieldClassed . $fieldSrc.'/>';
        //return $label . $arrValCode[2] . $fieldStart . $parameters . $fieldEnd;
        return $label . $fieldStart . $parameters . $fieldEnd;
    }
    
    /*
     * function to generate the link tags
     */

    public function generateLink($type, $arrPwdParams) {
        //echopre($arrPwdParams);
        $fieldStart = '<a ';
        if (sizeof($arrPwdParams) > 0) {
            foreach ($arrPwdParams as $key => $params) {
                if ($key == 'label')
                    $label = $this->createLabel($params);
                else if($key == 'href'){
                	$paramSrc = $params;
                }else if ($key == 'value') {
                   $paramValue = $params;
                }
                else {
                    if ($key == 'class')
                        $paramClass = $params;
                    else
                        $parameters .= $key . '="' . $params . '" ';
                }
            }
        }
        $fieldClassed = 'class="' . $paramClass . '"';
        $fieldSrc = 'src="' . $paramSrc . '"';
        $fieldEnd = $fieldClassed . $fieldSrc.'>'.$paramValue.'</a>';
        //return $label . $arrValCode[2] . $fieldStart . $parameters . $fieldEnd;
        return $label . $fieldStart . $parameters . $fieldEnd;
    }

    /*
     * function to generate the  select box
     */

    public function generateSelectField($type, $arrParams) {
        $fieldStart = '<select ';
        if (sizeof($arrParams) > 0) {
            foreach ($arrParams as $key => $params) {
                if ($key == 'label')
                    $label = $this->createLabel($params);
                else if ($key == 'validations') {
                    $arrValCode = $this->createValidationsCode($arrParams['name'], $params);
                    $paramValidations = $arrValCode[0];
                } else if ($key == 'options')
                    $selectOptions = $this->createSelectOptions($params,@$arrParams['selected']);
                else {
                    if ($key == 'class')
                        $paramClass = $params;
                    else
                        $parameters .= $key . '="' . $params . '" ';
                }
            }
        }
        $fieldClassed = 'class="' . $paramClass . ' ' . $paramValidations . '"';
        $fieldEnd = $fieldClassed . '>' . $selectOptions . '</select>' . $arrValCode[1];
        //return $label . $arrValCode[2] . $fieldStart . $parameters . $fieldEnd;
        return $label . $fieldStart . $parameters . $fieldEnd;
    }

    /*
     * function to generate the select box options
     */

    public function createSelectOptions($options,$selected=  array()) {       
        $optionText = '';
        if (sizeof($options) > 0) {
            foreach ($options as $key => $option) {
                $optionText .= '<option value = '.$option. ' ';
                if(in_array($option, $selected))
                $optionText .= 'selected';
                foreach ($option as $sleKey => $selOption)
                    $optionText .= $sleKey . '="' . $selOption . '" ';
                $optionText .= '>' . $key . '</option>';
            }
        }
        return $optionText;
    }

    /*
     * function to generate the form with the form properties
     */

    public function generateFormProperites() {
        $formProperties = $this->formProperties;
        $formTags = '<form ';
        if (sizeof($formProperties) > 0) {
            foreach ($formProperties as $key => $params) {
                $formTags .= $key . '="' . $params . '" ';
            }
        }
        $formTags .= '>';
        return $formTags;
    }

    /*
     * function to generate the check boxes
     */

//    public function generateCheckBoxField($type, $arrParams) {
//        echopre($arrParams);
//        $checkLabel = $arrParams['label'];
//        $checkOptions = $arrParams['options'];
//        $label = $this->createLabel($checkLabel);
//        $validationClass = $this->createValidationsCode($arrParams['name'], $arrParams['validations']);
//        $validation = ' class="' . $validationClass[0] . '"';
//        $checkbox = '';
//        //foreach ($checkOptions as $checkName => $options) {
//            $checkbox .= '<input type="checkbox" name="' . $arrParams['name'] . '"  ' . $validation;
//            $validation = '';
//            foreach ($options as $key => $option) {
//                $checkbox .= $key . '="' . $option . '" ';
//            }
//            $checkbox .= '> ' . $arrParams['title'];
//        //}
//        $checkbox .= $validationClass[1];
//        return $label . $checkbox;
//    }
	/*
	* Pass checked =1 for keeping the checkbox checked
	*/
    public function generateCheckBoxField($type, $arrPwdParams) {
	$fieldStart = '<input  ';
        if (sizeof($arrPwdParams) > 0) {
            foreach ($arrPwdParams as $key => $params) {
                if ($key == 'label'){
                    if($params['message']!=''){
                        $label = $this->createLabel($params);
                    }
                }
                else if ($key == 'validations') {
                    $arrValCode = $this->createValidationsCode($arrPwdParams['name'], $params);
                    $paramValidations = $arrValCode[0];
                }
				elseif($key == 'checked'){
					if($params == 1){
						$parameters .= 'checked ';
					}
					
				}
				 else {
                    if ($key == 'class')
                        $paramClass = $params;
                    else
                        $parameters .= $key . '="' . $params . '" ';
                }
            }
        }
        $fieldClassed = 'class="' . $paramClass . ' ' . $paramValidations . '"';
        $fieldEnd = $fieldClassed . '> ' .$arrPwdParams['title'].$arrValCode[1];
        //return $label.$arrValCode[2].$fieldStart.$parameters.$fieldEnd;
        return $label . $fieldStart . $parameters . $fieldEnd;
    }

    /*
     * function to generate the js validation codes
     */

    public function createValidationsCode($fieldName, $validation) {
        $err_message = '';
        $validations = $validation['options'];
        $errorClass = $validation['class'];
        if (sizeof($validations) > 0) {
            if ($this->validationJsCode == '') {
                $this->validationJsCode = '<script type="text/javascript">
					$(document).ready(function() { $("#' . $this->formId . '").validate({meta: "validate"}); }); </script>';
            }
            foreach ($validations as $key => $items) {
                if ($key == 'required')
                    $mandate = '<span class="mandatory">*</span>';
                $validateCode .= $key . ':' . $items[0] . ',';
                $validateMsg .= $key . ":'" . $items[1] . "',";
            }
        }
        $arrValidation[0] = ' {validate:{' . $validateCode . ' messages:{' . $validateMsg . '}}}';
        if (array_key_exists($fieldName, $this->formErrorMessages)) {
            $err_message = $this->formErrorMessages[$fieldName];
        }
        $arrValidation[1] = '<div class="' . $errorClass . '"><label class="error" for="' . $fieldName . '" generated="true">' . $err_message . '</label></div>';
        $arrValidation[2] = $mandate;
        return $arrValidation;
    }
    
     /*
     * function to generate Submit buttton
     */
     public function generateSubmitField($type, $arrPwdParams) {
        $fieldStart = '<input  ';
        if (sizeof($arrPwdParams) > 0) {
            foreach ($arrPwdParams as $key => $params) {
               if ($key == 'class')
                        $paramClass = $params;
                    else
                        $parameters .= $key . '="' . $params . '" ';
                }
        }
        $fieldClassed = 'class="' . $paramClass.  '"';
        $fieldEnd = $fieldClassed . '>';
        //return $label.$arrValCode[2].$fieldStart.$parameters.$fieldEnd;
        return $label . $fieldStart . $parameters . $fieldEnd;
    }

}

/*
 * class to validate the form object
 */

class formValidator extends FormManager {

    public $formFields = array();
    public $validMessage = array();

    public function __construct() {
        
    }

    /*
     * function to validate the form element
     */

    public function validate($formFields) {
        
        $this->formFields = $formFields;
        //echopre1($this->formFields);
        if (sizeof($formFields) > 0) {
            foreach ($formFields as $fieldType => $fields) {  // iterate the form fields
                $fieldName = $fields['name'];
                $validations = $fields['validations']['options'];
                if (sizeof($validations) > 0) {  // check whether the field has a validation or not
                    $fieldValue = $_POST[$fieldName];
                    foreach ($validations as $key => $messages) {
                        switch ($key) {    // validate the fields
                            case 'required':
                                if ($fieldType == 'file') // check the input field is file or not
                                    $message = $this->checkFileRequired($fieldName, $messages[1]);
                                else
                                    $message = $this->checkRequired($fieldValue, $messages[1]);
                                break;
                            case 'email':
                                $message = $this->validateEmail($fieldValue, $messages[1]);
                                break;
                            case 'accept':
                                $message = $this->validateFile($fieldName, $messages);
                            case 'equalTo':
                                $message = $this->checkPasswordConfirm($fieldName, $messages);
                        }
                        if ($message) {
                            $validMessage['messages'][$fieldName] = $message;
                            $validMessage['error'] = 1;
                        }
                    }
                }
            }
        }
        return $validMessage;
    }

    /*
     * function to validate the filed value is blank or not
     */

    public function checkRequired($value, $messages) {
        if ($value == "")
            return $messages;
    }

    /*
     * function to validate the file field is entered or not
     */

    public function checkFileRequired($fileName, $messages) {
        if ($fileName != '') {
            $file = $_FILES[$fileName];
            if ($file['name'] == '')
                return $messages;
        }
    }

    /*
     * function to validate the input value is an email or not
     */

    public function validateEmail($email, $messages) {
        if (!preg_match('/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,4}(\.[a-zA-Z]{2,3})?(\.[a-zA-Z]{2,3})?$/i', $email))
            return $messages;
    }

    /*
     * function to validate the file field
     */

    public function validateFile($fileName, $messages) {
        if ($fileName != '') {
            $file = $_FILES[$fileName];
            if ($file['name'] != '') {
                $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
                $allowedExts = str_replace('\'', '', $messages[0]);
                $extensions = explode('|', $allowedExts);
                if (!in_array($ext, $extensions)) {
                    return $messages[1];
                }
            }
        }
    }
    /*
     * function to validate password
     */
    public function checkPasswordConfirm($fileName, $messages){
        if($_POST[$fileName] != $_POST[$messages[1]]){
            return $messages[2];
        }
        
    }
   

}

?>