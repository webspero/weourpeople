<?php 
/*
 * All User Entity Logics should come here.
 */
class Classifieds{
   /*
Function to Register a  new User
*/
   
 
 public  static function updateStatusDelete($communityId='')
 {
 
 	$objResult                      =   new Messages();
 	$objResult->status              =   SUCCESS;
 
 	//$data = array();
 
 	//Check Entered User Email Exists OR NOT then stop user sign up if email exists
 
 	if($communityId == '') {
 		$objResult->status          =   "ERROR";
 		$objResult->message         =   CLASSIFIEDS_ID_NOT_EXIST; // Email Alreday Exists
 	}
 
 
 
 	// Update Community data to  Community table
 	 
 	if($objResult->status==SUCCESS) {
 
 		$db                             =   new Db();
 
 		$tableName                      =   Utils::setTablePrefix('classifieds');
 		$where                          =   "classifieds_id = ".$db->escapeString($communityId)." ";
 		 
 		$data = array('classifieds_status' => 'D');
 		 
 		$customerId                     =   $db->update($tableName, $data,$where,$doAudit=false);
 		//$objUserVo->community_id         =   $communityId;
 		$objResult->data                 =   $data;
 
 	}
 
 	Logger::info($objResult);
 
 	return $objResult;
 	 
 }
 
 public static function getCategories()
 {
 	$db                             = new Db();
 	$table                          = Utils::setTablePrefix('categories');
 	$classifieds = Utils::setTablePrefix('classifieds');
 	$query = "SELECT c.*,count(cf.classifieds_id) AS cnt FROM $table c INNER JOIN $classifieds cf on  c.category_id  = cf.classifieds_category_id WHERE c.category_status ='A' AND cf.classifieds_status='A' GROUP BY c.category_id ORDER BY cf.classifieds_id DESC";
//  	$data1 = $db->selectResult('categories', '*', "category_status = 'A'");
    $data1 = $db->selectQuery($query); 
//     echopre($data1);
 	 $dataArray = new stdClass();
 	// $dataArray->data = $data1;
 	 $dataArray->data = $data1;
 	 $dataArray->totalrecords = count($data1);
//  	 echopre($dataArray);
 	//  	exit;
 	return $dataArray;
 
 }
 public static function getAllCategoryName() {
 	$db	    = new Db();
 	//echo $id;
 	$getAllStates = $db->selectResult('categories','category_id,category_name',"category_status='A' ORDER BY category_name");
 	$var=0;
 	foreach($getAllStates as $state) {
 		$result[$var]->value = $state->category_id;
 		$result[$var]->text = $state->category_name;
 		$var++;
 	}
 	return $result;
 }
 
 public static function getCategoryName($alias)
 {
 	$db                             = new Db();
 	$table                          = Utils::setTablePrefix('categories');
 	$classifieds = Utils::setTablePrefix('classifieds');
 	//$query = "SELECT category_name FROM $table WHERE category_status ='A' AND category_alias='$alias'";
 	//  	$data1 = $db->selectResult('categories', '*', "category_status = 'A'");
 	$data1 = $db->selectRow('categories', 'category_name', "category_status ='A' AND category_alias='$alias'");
//  	    echopre($data1);
 	
 	return $data1;
 
 }
}
?>