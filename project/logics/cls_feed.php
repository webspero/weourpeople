<?php 
/*
 * All Feed Entity Logics should come here.
 */
class Feed{
   /*
Function to Register a  new User
*/
  public static function listUserFeeds($business_type='All',$search='',$pagenum=1,$itemperpage=10,$orderfield='community_announcement_date',$orderby='DESC') {
      //echo $business_type;
        $user_id                = Utils::getAuth('user_id');
        $db                     = new Db();
    	$objResult              = new Messages();
    	$objResult->status      = SUCCESS;
        
    	//Select Buisiness Record If Buisiness id is not null
    	if ($user_id != '') {
            $date = date('Y-m-d h:m:s');
       		$db                         = new Db();
    		$announcements              = Utils::setTablePrefix('community_announcements');
    		$members                    = Utils::setTablePrefix('community_member');
		$files                      = Utils::setTablePrefix('files');
                $community                  = Utils::setTablePrefix('communities');
                $likes                      = Utils::setTablePrefix('announcement_likes');
                $objUserData                = new stdClass();
      		$objUserData->table         =  'community_announcements a';
    		$objUserData->key           = 'c.community_id';
    		$objUserData->fields	    = "a.community_announcement_num_likes,(SELECT COUNT(announcement_like_id) from $likes l WHERE l.announcement_like = '1' AND l.announcement_id = a.community_announcement_id)AS Count ,a.community_announcement_num_comments,
                                               a.community_announcement_num_shares,a.community_announcement_alias,(SELECT announcement_like_id 
                                               FROM ".$likes." l WHERE l.user_id=".$user_id." 
                                               AND l.announcement_id = a.community_announcement_id 
                                               AND announcement_like='1') AS LIKE_ID,c.community_created_by,
                                               a.community_announcement_id,c.community_name,c.community_alias,fl.file_path,
                                               community_announcement_title,community_announcement_content,
                                               c.community_id,c.community_type,c.community_created_by,community_announcement_date,DATEDIFF('$date',community_announcement_date) as days,c.community_business_id,community_announcement_image_id,community_announcement_image_path,community_logo_name ";
    		$objUserData->join	    = " LEFT JOIN $community c ON c.community_id = a.community_id  
                                                LEFT JOIN $files fl ON c.community_image_id = fl.file_id ";
        	$objUserData->where	    = " a.`community_id` 
                                                IN(SELECT DISTINCT(cmember_community_id) FROM $members 
                                                WHERE cmember_id =$user_id AND cmember_status='A') 
                                                AND community_announcement_status='A' ";
                if($business_type == 'Personal'){
                    $objUserData->where .=" AND community_business_id = 0";
                }elseif($business_type == 'Business'){
                    $objUserData->where .=" AND community_business_id > 0";
                }
    		 
//    		if ($search>0) {
//                    $objUserData->where .=" AND c.community_id=$search ";
//			}
		$objUserData->where;
    		$objUserData->groupbyfield  = '';
    		$objUserData->orderby	    = $orderby;
    		$objUserData->orderfield    = $orderfield;
    		$objUserData->itemperpage   = $itemperpage;
    		$objUserData->page	    = $pagenum;		// by default its 1
    		$objUserData->debug	    = true;
    		$users                      = $db->getData($objUserData);
                //echopre($users);
                return $users;
    	}
        
    }
    
    
    public static function listUserFeedsNewsFeed($business_type='All',$search='',$pagenum=1,$itemperpage=10,$orderfield='community_announcement_date',$orderby='DESC') {
      //echo $business_type;
        $user_id                = Utils::getAuth('user_id');
        $db                     = new Db();
    	$objResult              = new Messages();
    	$objResult->status      = SUCCESS;
        
    	//Select Buisiness Record If Buisiness id is not null
    	if ($user_id != '') {
            $date = date('Y-m-d h:m:s');
       		$db                         = new Db();
    		$announcements              = Utils::setTablePrefix('community_announcements');
    		$members                    = Utils::setTablePrefix('community_member');
		$files                      = Utils::setTablePrefix('files');
                $community                  = Utils::setTablePrefix('communities');
                $likes                      = Utils::setTablePrefix('announcement_likes');
                $user                       = Utils::setTablePrefix('users');
                $objUserData                = new stdClass();
      		$objUserData->table         =  'community_announcements a';
    		$objUserData->key           = 'c.community_id';
    		$objUserData->fields	    = "a.community_announcement_num_likes,(SELECT COUNT(announcement_like_id) from $likes l WHERE l.announcement_like = '1' AND l.announcement_id = a.community_announcement_id)AS Count ,a.community_announcement_num_comments,
                                               a.community_announcement_num_shares,a.community_announcement_alias,U.user_firstname, U.user_lastname,U.user_alias,(SELECT announcement_like_id 
                                               FROM ".$likes." l WHERE l.user_id=".$user_id." 
                                               AND l.announcement_id = a.community_announcement_id 
                                               AND announcement_like='1') AS LIKE_ID,c.community_created_by,
                                               a.community_announcement_id,c.community_name,c.community_alias,fl.file_path,
                                               community_announcement_title,community_announcement_content,
                                               c.community_id,c.community_type,c.community_created_by,community_announcement_date,DATEDIFF('$date',community_announcement_date) as days,c.community_business_id,community_announcement_image_id,community_announcement_image_path,community_logo_name ";
    		$objUserData->join	    = " LEFT JOIN $community c ON c.community_id = a.community_id 
                                                LEFT JOIN $user U ON U.user_id = a.community_announcement_user_id
                                                LEFT JOIN $files fl ON U.user_image_id = fl.file_id ";
        	$objUserData->where	    = " a.`community_id` 
                                                IN(SELECT DISTINCT(cmember_community_id) FROM $members 
                                                WHERE cmember_id =$user_id AND cmember_status='A') 
                                                AND community_announcement_status='A' AND c.community_status!='D'";
                if($business_type == 'Personal'){
                    $objUserData->where .=" AND community_business_id = 0";
                }elseif($business_type == 'Business'){
                    $objUserData->where .=" AND community_business_id > 0";
                }
    		 
//    		if ($search>0) {
//                    $objUserData->where .=" AND c.community_id=$search ";
//			}
		$objUserData->where;
    		$objUserData->groupbyfield  = '';
    		$objUserData->orderby	    = $orderby;
    		$objUserData->orderfield    = $orderfield;
    		$objUserData->itemperpage   = $itemperpage;
    		$objUserData->page	    = $pagenum;		// by default its 1
    		$objUserData->debug	    = true;
    		$users                      = $db->getData($objUserData);
                //echopre($users);
                return $users;
    	}
        
    }
     public static function getAllCommunityNames() {
        $db                     = new Db();
        $userId                 = Utils::getAuth('user_id');
        $community              = Utils::setTablePrefix('communities');
        $communityMembers       = Utils::setTablePrefix('community_member');

         $query                  = "SELECT C.community_name,C.community_id,C.community_alias FROM $community C 
                                    WHERE community_status='A' 
                                    AND community_id 
                                    IN(SELECT cmember_community_id 
                                    FROM $communityMembers  CM 
                                    WHERE CM.cmember_id=$userId AND cmember_status='A' )";
        
        Logger::info($query);
        $objResultRow               = $db->fetchAll($db->execute($query));
        if ($objResultRow)
            $objResult->data        = $objResultRow;
        else {
            $objResult->status      = "ERROR";
            $objResult->message     = NO_FRIEND_SUGGESTION;
        }
        return $objResult; 
    }
    
    public static function getNumPendingFriendRequest(){
        $db                 = new Db();
        $userId             = Utils::getAuth('user_id');
        $invitation         = Utils::setTablePrefix('invitation');
        $query              = "SELECT count(invitation_id) AS NumFriendRequest from $invitation I  
                                WHERE invitation_receiver_id ='" . $db->escapeString($userId) . "' 
                                AND invitation_status='P'           ";
        Logger::info($query);
        $objResultRow       = $db->fetchSingleRow($query);
        return $objResultRow->NumFriendRequest;
    }
    
    public static function getNumPendingBizcomMembers(){
        $db                 = new Db();
        $userId             = Utils::getAuth('user_id');
        $communityMember    = Utils::setTablePrefix('community_member');
        $community          = Utils::setTablePrefix('communities');
        $query              = "SELECT COUNT(cm.cmember_list_id) AS NumPendingBizcomMembers FROM $communityMember AS cm 
                               LEFT JOIN $community c ON c.community_id =cm.cmember_community_id 
                               WHERE c.community_id IN (SELECT community_id 
                               FROM $community cc 
                               LEFT JOIN $communityMember ccm ON ccm.cmember_community_id=cc.community_id 
                               WHERE ccm.cmember_type='A' AND ccm.cmember_id=$userId) 
                               AND cm.cmember_status ='I' 
                               AND cm.cmember_pif_id >0 
                               AND c.community_status != 'D' 
                               ORDER BY c.community_name";
        Logger::info($query);
        $objResultRow       = $db->fetchSingleRow($query);
        
        return $objResultRow->NumPendingBizcomMembers;
    }
    
    public static function postLike($data){ 
        $count = 0;
      //  echopre1($data);
        $db                         = new Db();
        $userId                     = Utils::getAuth('user_id');
//        echo $userId;
        $tableName                  = Utils::setTablePrefix('announcement_likes');
        $res                        = $db->selectRecord("announcement_likes","*","announcement_id=".$data['announcement_id']." AND user_id=".$userId);
       
        $res_announcement           = $db->selectRecord("community_announcements","community_announcement_num_likes","community_announcement_id=".$data['announcement_id']);
//        echo '-'.$res->announcement_like_id.'-'.$res->announcement_like.'-'.$res_announcement->community_announcement_num_likes;
        if($res->announcement_like_id>0){
            $data_array                          = array();
            $data_array['announcement_like']     = ($res->announcement_like==1)?0:1;
            $return                              = ($res->announcement_like==1)?'':'liked';
            $res1                                = $db->updateFields("announcement_likes", $data_array,"announcement_id=".$data['announcement_id']." AND user_id=".$userId);
            if($data_array['announcement_like']==0){ //echo 'decremented';
                $count                              = $res_announcement->community_announcement_num_likes-1;
            }
            else{ //echo 'incremented';
                $count                              = $res_announcement->community_announcement_num_likes+1;
            }
            //echo $count;
            $rescomm                            = $db->updateFields("community_announcements", array('community_announcement_num_likes'=>$count),"community_announcement_id=".$data['announcement_id']);
            $count                                  = ($count>0)?($count.' '.($count==1?'Like':'Likes')):'';
            $return_array               = array('classtype'=>$return,'count'=>$count);
            print json_encode($return_array);
            exit;
            
        }
        else{
            $data['announcement_like']              = '1';
            $communityId                            = $db->insert($tableName, $data);
            $count                                  = ($res_announcement->community_announcement_num_likes > 0)?($res_announcement->community_announcement_num_likes+1):1;

            $rescomm                                = $db->updateFields("community_announcements", array('community_announcement_num_likes'=>$count),"community_announcement_id=".$data['announcement_id']);
            $return                                 = 'liked';
            $count                                  = ($count>0)?($count.' '.($count==1?'Like':'Likes')):'';

            $return_array                           = array('classtype'=>$return,'count'=>$count);
            print json_encode($return_array);
            exit;
        }
    }
    
    public static function shareFeed($data){
        $db                         = new Db();
        $userId                     = Utils::getAuth('user_id');
        $tableName                  = Utils::setTablePrefix('announcement_shares');
        $res_announcement           = $db->selectRecord("community_announcements","community_announcement_num_shares","community_announcement_id=".$data['announcement_id']);
        $res_shares                 = $db->selectRecord("announcement_shares","announcement_id,num_shares","announcement_id=".$data['announcement_id']." AND user_id=".$data['user_id']);
        if($res_shares->announcement_id >0){
           $rescomm                    = $db->updateFields("announcement_shares", array('num_shares'=>$res_shares->num_shares+1,'share_date'=>date('Y-m-d H:i:s')),"announcement_id=".$data['announcement_id']." AND user_id=".$data['user_id']); 
        }
        else{
            $communityId                = $db->insert($tableName, $data);
        }
        $count                      = $res_announcement->community_announcement_num_shares+1;
        $rescomm                    = $db->updateFields("community_announcements", array('community_announcement_num_shares'=>$count),"community_announcement_id=".$data['announcement_id']);
        $return_array               =array('count'=>$count.' Shares');
        echo json_encode($return_array);
        exit;
        }
        
     public static function getFriendShares($pagenum) {
        $user_id            = Utils::getAuth('user_id');
        $db                 = new Db();
        $objResult          = new Messages();
        $objResult->status  = SUCCESS;
        //Select Buisiness Record If Buisiness id is not null
        if ($user_id != '') {
            $db                 = new Db();
            $announcements      = Utils::setTablePrefix('community_announcements');
            $shares             = Utils::setTablePrefix('announcement_shares');
            $friends            = Utils::setTablePrefix('friends_list');
            $files              = Utils::setTablePrefix('files');
            $community          = Utils::setTablePrefix('communities');
            $community_memeber  = Utils::setTablePrefix('community_member');
            //$community_memeber  = Utils::setTablePrefix('community_member');
            $objUserData        = new stdClass();
            $objUserData->table = 'community_announcements a';
            $objUserData->key   = 'c.community_id';
            $objUserData->fields = 'DISTINCT(a.community_announcement_id),c.community_name,c.community_alias,f.file_path,
                                                   community_announcement_title,community_announcement_content,
                                                   c.community_id,community_announcement_date,s.share_date,s.user_name,s.announcement_share_id  ';
            $objUserData->join  = " INNER  JOIN  $shares s ON s.announcement_id=a.community_announcement_id 
                                                AND user_id IN(SELECT DISTINCT(friends_list_friend_id) FROM  $friends
                                                WHERE friends_list_user_id=$user_id AND friends_list_status='A')
                                                LEFT JOIN  $community c ON c.community_id = a.community_id 
                                                LEFT JOIN  $files f ON f.file_id=c.community_image_id ";
                                              //  . "LEFT JOIN ";
            $objUserData->where  = "  c.community_id NOT IN(SELECT cmember_community_id 
                                                FROM  $community_memeber WHERE cmember_id=$user_id AND cmember_status='A') ";
            if ($search > 0) {
                $objUserData->where .=" AND c.community_id=$search ";
            }
            $objUserData->where;
           // echo $objUserData->where;exit;
            $objUserData->groupbyfield = '';
            $objUserData->orderby       = 'DESC';
            $objUserData->orderfield    = 's.share_date';
            $objUserData->itemperpage   = 20;
            $objUserData->page          = $pagenum;  // by default its 1
            $objUserData->debug         = true;
            $users                      = $db->getData($objUserData);
            return $users;
        }
    }

    public static function postComments($data_array,$cmnt='') {
        $db                         = new Db();
        $tableName                  = Utils::setTablePrefix('announcement_comments');
        $comment_id                 = $db->insert($tableName, $data_array);
        if($data_array['parent_comment_id']>0){
            $res_announcement           = $db->selectRecord("announcement_comments","num_replies","announcement_comment_id=".$data_array['parent_comment_id']);
            $count                      = $res_announcement->num_replies+1;
            $rescomm                    = $db->updateFields("announcement_comments", array('num_replies'=>$count),"announcement_comment_id=".$data_array['parent_comment_id']);
            $return_array               = array('comment'=>$cmnt,'count'=>$count.' Replies','comment_id'=>$comment_id,'announcement_id'=>$data_array['announcement_id']);
        }
        else{
            $res_announcement           = $db->selectRecord("community_announcements","community_announcement_num_comments","community_announcement_id=".$data_array['announcement_id']);
            $count                      = $res_announcement->community_announcement_num_comments+1;
            $rescomm                    = $db->updateFields("community_announcements", array('community_announcement_num_comments'=>$count),"community_announcement_id=".$data_array['announcement_id']);
            $return_array               = array('comment'=>$cmnt,'count'=>$count.' Comments','comment_id'=>$comment_id,'announcement_id'=>$data_array['announcement_id']);
        }
            
        return $return_array;
 
    }
    
     public static function getAnnouncementComments($announcemnt_id,$pagenum=1,$itemsperpage=5) {
        $user_id            = Utils::getAuth('user_id');
        $db                 = new Db();
        $objResult          = new Messages();
        $objResult->status  = SUCCESS;
        //Select Buisiness Record If Buisiness id is not null
        if ($user_id != '') {
            $db                 = new Db();
            $users              = Utils::setTablePrefix('users');
            $communities        = Utils::setTablePrefix('communities');
            $likes              = Utils::setTablePrefix('announcement_comments_like');
            $image              = Utils::setTablePrefix('announcement_commment_images');
            $files              = Utils::setTablePrefix('files');
            $objUserData        = new stdClass();
            $objUserData->table = 'announcement_comments C';
            $objUserData->key   = 'C.announcement_comment_id';
            $objUserData->fields = "F.file_path,U.user_id,C.*,DATE_FORMAT(C.comment_date,'%m-%d-%Y %h:%i %p') AS commentDate,U.user_image_name,U.user_alias,CONCAT(U.user_firstname,' ',U.user_lastname) AS Username,cm.community_created_by ,cm.community_type,(SELECT announcement_comment_like_id  
                                               FROM ".$likes." l WHERE l.user_id=".$user_id." 
                                               AND l.comment_id = C.announcement_comment_id 
                                               AND comment_like='1') AS LIKE_ID ";
            $objUserData->join   = " LEFT JOIN $image I ON I.comment_id = C.announcement_comment_id LEFT JOIN $files F ON F.file_id = I.image_file_id  LEFT JOIN  $users U ON U.user_id = C.user_id  LEFT JOIN  $communities AS cm ON cm.community_id = C.community_id ";
            $objUserData->where  = "  C.announcement_id = $announcemnt_id AND parent_comment_id=0 ";
            
            $objUserData->groupbyfield = '';
            $objUserData->orderby       = 'DESC';
            $objUserData->orderfield    = 'C.comment_date';
            $objUserData->itemperpage   = $itemsperpage;
            $objUserData->page          = $pagenum;  // by default its 1
            $objUserData->debug         = true;
            $users1                      = $db->getData($objUserData);
          //  echopre1($users1);
            return $users1;
        }
    }
        
     public static function deleteComments($comment_id,$announcement_id) {
        $db                         = new Db();
        $tableName                  = Utils::setTablePrefix('announcement_comments');
        $rescomm                    = $db->deleteRecord("announcement_comments",'announcement_comment_id ='.$comment_id );
        
        $res_announcement           = $db->selectRecord("community_announcements","community_announcement_num_comments","community_announcement_id=".$announcement_id);
        if($res_announcement->community_announcement_num_comments <=0){
            $count=0;
        }
        else{
            $count                      = $res_announcement->community_announcement_num_comments-1;
        }
        $rescomm                    = $db->updateFields("community_announcements", array('community_announcement_num_comments'=>$count),"community_announcement_id=".$announcement_id);
        echo  $count;
        exit;
    }
    
      public static function deleteNewsfeedComments($comment_id,$announcement_id) {
        $db                         = new Db();
        $tableName                  = Utils::setTablePrefix('news_feed_comments');
        $rescomm                    = $db->deleteRecord("news_feed_comments",'news_feed_comments_id ='.$comment_id );
        
        $res_announcement           = $db->selectRecord("news_feed","news_feed_num_comments","news_feed_id=".$announcement_id);
        if($res_announcement->news_feed_num_comments <=0){
            $count=0;
        }
        else{
            $count                      = $res_announcement->news_feed_num_comments-1;
        }
        $rescomm                    = $db->updateFields("news_feed", array('news_feed_num_comments'=>$count),"news_feed_id=".$announcement_id);
        echo  $count;
        exit;
    }
    
     public static function deleteOrgfeedComments($comment_id,$announcement_id) {
        $db                         = new Db();
        $tableName                  = Utils::setTablePrefix('organization_announcement_comments');
        $rescomm                    = $db->deleteRecord("organization_announcement_comments",'organization_announcement_comment_id ='.$comment_id );
        
        $res_announcement           = $db->selectRecord("organization_announcements","organization_announcement_num_comments","organization_announcement_id=".$announcement_id);
        if($res_announcement->organization_announcement_num_comments <=0){
            $count=0;
        }
        else{
            $count                      = $res_announcement->organization_announcement_num_comments-1;
        }
        $rescomm                    = $db->updateFields("organization_announcements", array('organization_announcement_num_comments'=>$count),"organization_announcement_id=".$announcement_id);
        echo  $count;
        exit;
    }
    
    public static function postCommentLike($data){
        $db                         = new Db();
        $userId                     = Utils::getAuth('user_id');
        $tableName                  = Utils::setTablePrefix('announcement_comments_like');
        $res                        = $db->selectRecord("announcement_comments_like","*","comment_id=".$data['comment_id']." AND user_id=".$userId);
        $res_comment                = $db->selectRecord("announcement_comments","num_comment_likes","announcement_comment_id=".$data['comment_id']);
        //echopre($res_comment);
        if($res->announcement_comment_like_id>0){
            $data_array                          = array();
            $data_array['comment_like']          = ($res->comment_like==1)?0:1;
            $return                              = ($res->comment_like==1)?'':'liked';
            $res1                                = $db->updateFields("announcement_comments_like", $data_array,"comment_id=".$data['comment_id']." AND user_id=".$userId);
            if($data_array['comment_like']==0){
                $count                              = $res_comment->num_comment_likes-1;
            }
            else{
                $count                              = $res_comment->num_comment_likes+1;
            }
             $rescomm                                = $db->updateFields("announcement_comments", array('num_comment_likes'=>$count),"announcement_comment_id=".$data['comment_id']);
             $count                                  = ($count>0)?($count.' '.($count==1?'Like':'Likes')):'';
            $return_array                           = array('classtype'=>$return,'count'=>$count);
            print json_encode($return_array);
            exit;
        }
        else{
            $data['comment_like']                   = '1';
            $communityId                            = $db->insert($tableName, $data);
            $count                                  = ($res_comment->num_comment_likes > 0)?($res_comment->num_comment_likes+1):1;

            $rescomm                                = $db->updateFields("announcement_comments", array('num_comment_likes'=>$count),"announcement_comment_id=".$data['comment_id']);
            $return                                 = 'liked';
            $count                                  = ($count>0)?($count.' '.($count==1?'Like':'Likes')):'';

            $return_array                           = array('classtype'=>$return,'count'=>$count);
            print json_encode($return_array);
            exit;
        }
    }
    
    public static function showCommentReply($comment_id,$pagenum=1,$itemsperpage=5){
        $user_id            = Utils::getAuth('user_id');
        $db                 = new Db();
        $objResult          = new Messages();
        $objResult->status  = SUCCESS;
        //Select Buisiness Record If Buisiness id is not null
        if ($user_id != '') {
            $db                 = new Db();
            $users              = Utils::setTablePrefix('users');
            $file               = Utils::setTablePrefix('files');
            $comment_image      = Utils::setTablePrefix('announcement_commment_images');
            $communities        = Utils::setTablePrefix('communities');
            $objUserData        = new stdClass();
            $objUserData->table = 'announcement_comments C';
            $objUserData->key   = 'C.announcement_comment_id';
            $objUserData->fields = "C.*,U.user_id,DATE_FORMAT(C.comment_date,'%m-%d-%Y %h:%i %p') AS commentDate,U.user_image_name,U.user_alias, F.file_path,cm.community_created_by ";
            $objUserData->join   = " LEFT JOIN  $users U ON U.user_id = C.user_id LEFT JOIN $comment_image A ON A.comment_id = C.announcement_comment_id"
                                    . " LEFT JOIN $file F ON A.image_file_id = F.file_id LEFT JOIN $communities AS cm ON cm.community_id = C.community_id";
            $objUserData->where  = " parent_comment_id='".$comment_id."'";
            
            $objUserData->groupbyfield = '';
            $objUserData->orderby       = 'DESC';
            $objUserData->orderfield    = 'C.comment_date';
            $objUserData->itemperpage   = $itemsperpage;
            $objUserData->page          = $pagenum;  // by default its 1
            $objUserData->debug         = true;
            $users1                      = $db->getData($objUserData);
            //echopre($users1);exit;
            return $users1;
    }
    }
    
    public static function deleteReply($comment_id,$announcement_id) {
        $db                         = new Db();
        $tableName                  = Utils::setTablePrefix('announcement_comments');
        $rescomm                    = $db->deleteRecord("announcement_comments",'announcement_comment_id ='.$comment_id ); 
        $res_announcement           = $db->selectRecord("announcement_comments","num_replies","announcement_comment_id=".$announcement_id);
        $count                      = $res_announcement->num_replies-1;
        $rescomm                    = $db->updateFields("announcement_comments", array('num_replies'=>$count),"announcement_comment_id=".$announcement_id);
        echo  $count;
        exit;
    }
    
    public static function deleteReplyNewsFeed($comment_id,$announcement_id) {
        $count = 0;
       // echo $comment_id;
        $db                         = new Db();
        $tableName                  = Utils::setTablePrefix('news_feed_comments');
        $rescomm                    = $db->deleteRecord("news_feed_comments",'news_feed_comments_id ='.$comment_id ); 
        $res_announcement           = $db->selectRecord("news_feed_comments","num_replies","news_feed_comments_id=".$announcement_id." OR news_feed_comments_id = ".$comment_id);
//        echopre1($res_announcement);
        $count                      = $res_announcement->num_replies-1;
        $rescomm                    = $db->updateFields("news_feed_comments", array('num_replies'=>$count),"news_feed_comments_id=".$announcement_id);
        echo  $count;
        exit;
    }
    
     public static function deleteReplyOrgFeed($comment_id,$announcement_id) {
       // echo $comment_id;
        $db                         = new Db();
        $tableName                  = Utils::setTablePrefix('organization_announcement_comments');
        $rescomm                    = $db->deleteRecord("organization_announcement_comments",'organization_announcement_comment_id ='.$comment_id ); 
        $res_announcement           = $db->selectRecord("organization_announcement_comments","num_replies","organization_announcement_comment_id=".$announcement_id);
        $count                      = $res_announcement->num_replies-1;
        $rescomm                    = $db->updateFields("organization_announcement_comments", array('num_replies'=>$count),"organization_announcement_comment_id=".$announcement_id);
        echo  $count;
        exit;
    }
    
    
     public static function postLikeAdmin($data){
         //echo "heer";exit;
        $db                         = new Db();
        $userId                     = Utils::getAuth('user_id');
        $tableName                  = Utils::setTablePrefix('admin_announcement_likes');
        $res                        = $db->selectRecord("admin_announcement_likes","*","admin_announcement_id=".$data['admin_announcement_id']." AND user_id=".$userId);
       
        $res_announcement           = $db->selectRecord("admin_announcements","admin_announcement_num_likes","admin_announcement_id=".$data['admin_announcement_id']);
        if($res->admin_announcement_like_id>0){
            $data_array                          = array();
            $data_array['admin_announcement_like']     = ($res->admin_announcement_like==1)?0:1;
            $return                              = ($res->admin_announcement_like==1)?'':'liked';
            $res1                                = $db->updateFields("admin_announcement_likes", $data_array,"admin_announcement_id=".$data['admin_announcement_id']." AND user_id=".$userId);
            if($data_array['admin_announcement_like']==0){
                $count                              = $res_announcement->admin_announcement_num_likes-1;
                $rescomm                            = $db->updateFields("admin_announcements", array('admin_announcement_num_likes'=>$count),"admin_announcement_id=".$data['admin_announcement_id']);
            }
            else{
                $count                              = $res_announcement->admin_announcement_num_likes+1;
                $rescomm                            = $db->updateFields("admin_announcements", array('admin_announcement_num_likes'=>$count),"admin_announcement_id=".$data['admin_announcement_id']);
            }
            $return_array               =array('classtype'=>$return,'count'=>$count.' Likes');
            print json_encode($return_array);
            exit;
            
        }
        else{
            $data['admin_announcement_like']              = '1';
            $communityId                            = $db->insert($tableName, $data);
            $rescomm                                = $db->updateFields("admin_announcements", array('admin_announcement_num_likes'=>1),"admin_announcement_id=".$data['admin_announcement_id']);
            $return                                 = 'liked';
            $return_array                           = array('classtype'=>$return,'count'=>'1 Like');
            print json_encode($return_array);
            exit;
        }
    }
    
        public static function getNumPendingFriendRequestByUser(){
        $db                             = new Db();
        $userId                         = Utils::getAuth('user_id');
        if($userId != ''){
        $tableName                      = Utils::setTablePrefix('users');
        $query                          = "SELECT user_friend_request_count FROM ".$tableName." WHERE user_id= '".$userId."'";
        $objResultRow                   = $db->fetchAll($db->execute($query));
        return $objResultRow;
        }
    }
    
    public static function getNumPendingPrivateRequestByUser(){
        $db                             = new Db();
        $userId                         = Utils::getAuth('user_id');
        if($userId != ''){
        $tableName                      = Utils::setTablePrefix('users');
        $query                          = "SELECT user_private_request_count FROM ".$tableName." WHERE user_id= '".$userId."'";
        $objResultRow                   = $db->fetchAll($db->execute($query));
        return $objResultRow;
        }
    }
    
     public static function postNewsfeedLike($data){
        $count = 0;
      //  echopre1($data);
        $db                         = new Db();
        $userId                     = Utils::getAuth('user_id');
        $tableName                  = Utils::setTablePrefix('news_feed_likes');
        $res                        = $db->selectRecord("news_feed_likes","*","news_feed_id=".$data['news_feed_id']." AND news_feed_like_user_id=".$userId);
       
        $res_announcement           = $db->selectRecord("news_feed","news_feed_num_like","news_feed_id=".$data['news_feed_id']);
//        print_r($res_announcement);
        //echopre1($res);
        if($res->news_feed_like_id>0){
//            echo 'case1: news feed already liked before';
            $data_array                          = array();
            $data_array['news_feed_like']     = ($res->news_feed_like==1)?0:1;
            $return                              = ($res->news_feed_like==1)?'':'liked';
            $res1                                = $db->updateFields("news_feed_likes", $data_array,"news_feed_id=".$data['news_feed_id']." AND news_feed_like_user_id=".$userId);
          
//            echo $data_array['news_feed_like'];
//            echo 'previous likes -- '.$res_announcement->news_feed_num_like;
            
            if($data_array['news_feed_like']==0){
//                echo 'unlike case';
                if($res_announcement->news_feed_num_like > 0){
                $count                              = $res_announcement->news_feed_num_like-1;
                }
                else{
                $count                              = $res_announcement->news_feed_num_like+1; 
                }
                $rescomm                            = $db->updateFields("news_feed", array('news_feed_num_like'=>$count),"news_feed_id=".$data['news_feed_id']);
//                echo 'count set to '.$count;
                
                }
            else{
//                echo 'like case-- multiple likes';
                $count                              = $res_announcement->news_feed_num_like+1;
                $rescomm                            = $db->updateFields("news_feed", array('news_feed_num_like'=>$count),"news_feed_id=".$data['news_feed_id']);
//                echo 'count set to '.$count;
            }
            $count=($count>0)?($count.' '.($count==1?'Like':'Likes')):'';
            $return_array               =array('classtype'=>$return,'count'=>$count);
            print json_encode($return_array);
            exit;
            
        }
        else{
            //echo 'case2: news feed already like first time';
            $data['news_feed_like']              = '1';
            $communityId                            = $db->insert($tableName, $data);
            $count=($res_announcement->news_feed_num_like > 0)?($res_announcement->news_feed_num_like+1):1;
            $rescomm                                = $db->updateFields("news_feed", array('news_feed_num_like'=>$count),"news_feed_id=".$data['news_feed_id']);
            $return                                 = 'liked';
            $count=($count>0)?($count.' '.($count==1?'Like':'Likes')):'';
            $return_array                           = array('classtype'=>$return,'count'=>$count);
            print json_encode($return_array);
            exit;
        }
    }
    
    public static function postNewsfeedComments($data_array,$cmnt='') {
        $db                         = new Db();
        $tableName                  = Utils::setTablePrefix('news_feed_comments');
        //echopre1($data_array);
        $comment_id                 = $db->insert($tableName, $data_array);
        if($data_array['parent_comment_id']>0){
            $res_announcement           = $db->selectRecord("news_feed_comments","num_replies","parent_comment_id=".$data_array['parent_comment_id']);
            $count                      = $res_announcement->num_replies+1;
            $rescomm                    = $db->updateFields("news_feed_comments", array('num_replies'=>$count),"parent_comment_id=".$data_array['parent_comment_id']);
            $return_array               = array('comment'=>$cmnt,'count'=>$count.' Replies','comment_id'=>$comment_id,'parent_comment_id'=>$data_array['parent_comment_id']);
        }
        else{
            $res_announcement           = $db->selectRecord("news_feed","news_feed_num_comments","news_feed_id=".$data_array['news_feed_id']);
            $count                      = $res_announcement->news_feed_num_comments+1;
            $rescomm                    = $db->updateFields("news_feed", array('news_feed_num_comments'=>$count),"news_feed_id=".$data_array['news_feed_id']);
            $return_array               = array('comment'=>$cmnt,'count'=>$count.' Comments','comment_id'=>$comment_id,'news_feed_id'=>$data_array['news_feed_id']);
        }
            
        return $return_array;
 
    }


    /**
     * getFeedComments
     *
     * Returns paginated comments for a certain news feed item Id
     *
     * @param $feed_id
     * @param int $pagenum
     * @param int $itemsperpage
     *
     * @return mixed
     */
    public static function getFeedComments($feed_id=0, $pagenum = 1, $itemsperpage = 5)
    {
        $user_id = Utils::getAuth('user_id');
        $db = new Db();
        $objResult = new Messages();
        $objResult->status = SUCCESS;

        if ($user_id != '') {
            $users = Utils::setTablePrefix('users');
            $comments = Utils::setTablePrefix('news_feed_comments');
            $likes = Utils::setTablePrefix('news_feed_likes');
            $image = Utils::setTablePrefix('newsfeed_comment_images');
            $files = Utils::setTablePrefix('files');

            $objUserData = new stdClass();
            $objUserData->table = 'news_feed_comments C';
            $objUserData->key = 'C.news_feed_id';
            $objUserData->fields = "F.file_path,U.user_id,C.*,DATE_FORMAT(C.news_feed_comment_date,'%m-%d-%Y %h:%i %p') AS commentDate,U.user_image_name,U.user_alias,CONCAT(U.user_firstname,' ',U.user_lastname) AS Username,(SELECT news_feed_like_id  
                                               FROM " . $likes . " l WHERE l.news_feed_like_user_id=" . $user_id . " 
                                               AND l.news_feed_comment_id = C.news_feed_comments_id) AS LIKE_ID,(SELECT count(num_replies) FROM $comments WHERE news_feed_id = $feed_id AND parent_comment_id !='0' ) AS Num_replies ";
            $objUserData->join = " LEFT JOIN $image I ON I.newsfeed_comment_id = C.news_feed_comments_id LEFT JOIN $files F ON F.file_id = I.newsfeed_comment_image_file_id LEFT JOIN  $users U ON U.user_id = C.news_feed_comment_user_id ";
            $objUserData->where = "  C.news_feed_id = $feed_id AND parent_comment_id=0 ";

            $objUserData->groupbyfield = '';
            $objUserData->orderby = 'DESC';
            $objUserData->orderfield = 'C.news_feed_comment_date';
            $objUserData->itemperpage = $itemsperpage;
            $objUserData->page = $pagenum;
            $objUserData->debug = true;

            $comments = $db->getData($objUserData);
            return $comments;
        }
    }
    public static function getFeedCommentsreply($feed_id,$pagenum=1,$itemsperpage=5) {
        $user_id            = Utils::getAuth('user_id');
        $db                 = new Db();
        $objResult          = new Messages();
        $objResult->status  = SUCCESS;
        //Select Buisiness Record If Buisiness id is not null
        if ($user_id != '') {
            $db                 = new Db();
            $users              = Utils::setTablePrefix('users');
            //$comments           = Utils::setTablePrefix('news_feed_comments');
            $likes              = Utils::setTablePrefix('news_feed_likes');
            //$image              = Utils::setTablePrefix('announcement_commment_images');
           // $files              = Utils::setTablePrefix('files');
            $objUserData        = new stdClass();
            $objUserData->table = 'news_feed_comments C';
            $objUserData->key   = 'C.news_feed_id';
            $objUserData->fields = "count(C.num_replies) AS num_replies";
            $objUserData->join   = " LEFT JOIN  $users U ON U.user_id = C.news_feed_comment_user_id ";
            $objUserData->where  = " parent_comment_id=$feed_id ";
            
            $objUserData->groupbyfield = '';
            $objUserData->orderby       = 'DESC';
            $objUserData->orderfield    = 'C.news_feed_comment_date';
            $objUserData->itemperpage   = $itemsperpage;
            $objUserData->page          = $pagenum;  // by default its 1
            $objUserData->debug         = true;
            $users1                      = $db->getData($objUserData);
            //echopre($users1);
            return $users1;
        }
    }
    
        public static function showFeedCommentReply($comment_id,$pagenum=1,$itemsperpage=5){
        $user_id            = Utils::getAuth('user_id');
        $db                 = new Db();
        $objResult          = new Messages();
        $objResult->status  = SUCCESS;
        //Select Buisiness Record If Buisiness id is not null
        if ($user_id != '') {
            $db                 = new Db();
            $users              = Utils::setTablePrefix('users');
            $file               = Utils::setTablePrefix('files');
            $comment_image      = Utils::setTablePrefix('newsfeed_comment_images');
            $communities        = Utils::setTablePrefix('communities');
            $objUserData        = new stdClass();
            $objUserData->table = 'news_feed_comments C';
            $objUserData->key   = 'C.news_feed_comments_id';
            $objUserData->fields = "C.*,U.user_id,DATE_FORMAT(C.news_feed_comment_date,'%m-%d-%Y %h:%i %p') AS commentDate,U.user_image_name,U.user_alias,CONCAT(U.user_firstname,' ',U.user_lastname) AS Username,F.file_path ";
            $objUserData->join   = "  LEFT JOIN $comment_image A ON A.newsfeed_comment_id = C.news_feed_comments_id  LEFT JOIN $file F ON A.newsfeed_comment_image_file_id = F.file_id LEFT JOIN  $users U ON U.user_id = C.news_feed_comment_user_id "
                                    . "";
            $objUserData->where  = " parent_comment_id='".$comment_id."'";
            
            $objUserData->groupbyfield = '';
            $objUserData->orderby       = 'ASC';
            $objUserData->orderfield    = 'C.news_feed_comment_date';
            $objUserData->itemperpage   = $itemsperpage;
            $objUserData->page          = $pagenum;  // by default its 1
            $objUserData->debug         = true;
            $users1                      = $db->getData($objUserData);
            //echopre($users1);exit;
            return $users1;
    }
    }
    
    public static function getNewsfeedDetail($id){
        if($id){
        $db                             = new Db();
       // $userId                         = Utils::getAuth('user_id');
        $tableName                      = Utils::setTablePrefix('news_feed');
//        $tableName1                      = Utils::setTablePrefix('communities');
        $query                          = "SELECT c.* FROM ".$tableName." AS c  WHERE c.news_feed_id='".$id."'";
        $objResultRow = $db->fetchSingleRow($query);
        return $objResultRow;
        }
    }
    
     public static function getNewsfeedDetails($alias){
        if($alias){
        $db                             = new Db();
       // $userId                         = Utils::getAuth('user_id');
        $tableName                      = Utils::setTablePrefix('news_feed');
//        $tableName1                      = Utils::setTablePrefix('communities');
        $query                          = "SELECT c.* FROM ".$tableName." AS c  WHERE c.news_feed_alias='".$alias."'";
       // echo $query; exit;
        $objResultRow = $db->fetchSingleRow($query);
        return $objResultRow;
        }
    }
    
    public static function postNewsfeedCommentLike($data){
    $db                         = new Db();
    $userId                     = Utils::getAuth('user_id');
    $tableName                  = Utils::setTablePrefix('newsfeed_comments_like');
    $res                        = $db->selectRecord("newsfeed_comments_like","*","newsfeed_comment_id=".$data['newsfeed_comment_id']." AND user_id=".$userId);
    $res_comment                = $db->selectRecord("news_feed_comments","num_comment_likes","news_feed_comments_id=".$data['newsfeed_comment_id']);
    //echopre($res_comment);
    if($res->newsfeed_comment_like_id>0){
        $data_array                          = array();
        $data_array['newsfeed_comment_like']          = ($res->newsfeed_comment_like==1)?0:1;
        $return                              = ($res->newsfeed_comment_like==1)?'':'liked';
        $res1                                = $db->updateFields("newsfeed_comments_like", $data_array,"newsfeed_comment_id=".$data['newsfeed_comment_id']." AND user_id=".$userId);
        if($data_array['newsfeed_comment_like']==0){
            $count                              = $res_comment->num_comment_likes-1;
        }
        else{
            $count                              = $res_comment->num_comment_likes+1;
        }
        $rescomm                            = $db->updateFields("news_feed_comments", array('num_comment_likes'=>$count),"news_feed_comments_id=".$data['newsfeed_comment_id']);
        $count=($count>0)?($count.' '.($count==1?'Like':'Likes')):'';
        $return_array                           = array('classtype'=>$return,'count'=>$count);
        print json_encode($return_array);
        exit;
    }
    else{
        $data['newsfeed_comment_like']                   = '1';
        $communityId                            = $db->insert($tableName, $data);
        $count=($res_comment->num_comment_likes > 0)?($res_comment->num_comment_likes+1):1;
        $rescomm                                = $db->updateFields("news_feed_comments", array('num_comment_likes'=>$count),"news_feed_comments_id=".$data['newsfeed_comment_id']);
        $return                                 = 'liked';
        $count=($count>0)?($count.' '.($count==1?'Like':'Likes')):'';

        $return_array                           = array('classtype'=>$return,'count'=>$count);
        print json_encode($return_array);
        exit;
    }
    }
    
     public static function getNewsfeedImage($page=0,$item = 3){
        //if($id){
        $db                             = new Db();
        $userId                         = $_POST['uid'];
        $tableName                      = Utils::setTablePrefix('news_feed');
        $query                          = "SELECT c.* FROM ".$tableName." AS c  WHERE c.news_feed_user_id='".$userId."' AND news_feed_image_name !='' ORDER BY c.news_feed_id DESC";
        $objResultRow = $db->selectQuery($query);
        return $objResultRow;
       // }
    }
    
     public static function getMyLatestNewsfeedImage($userId){
        //if($id){
        $db                             = new Db();
        //$userId                         = $_POST['uid'];
        $tableName                      = Utils::setTablePrefix('news_feed');
        $query                          = "SELECT c.* FROM ".$tableName." AS c  WHERE c.news_feed_user_id='".$userId."' AND news_feed_image_name !='' ORDER BY c.news_feed_id DESC LIMIT 0,3";
        $objResultRow = $db->selectQuery($query);
        return $objResultRow;
       // }
    }

    public static function postLikeOrganization($data)
    { 
        $count = 0;
        $db = new Db();
        $userId = Utils::getAuth('user_id');
        $tableName = Utils::setTablePrefix('organization_announcement_likes');
        $totalCount = $db->getDataCount("organization_announcement_likes", "*", " WHERE announcement_id=" . $data['announcement_id']);

        // Entry exist for current organization id
//        if ($totalCount > 0) {

            $res = $db->selectRecord("organization_announcement_likes","*","announcement_id=".$data['announcement_id']." AND user_id=".$userId);
            $res_announcement = $db->selectRecord("organization_announcements", "organization_announcement_num_likes", "organization_announcement_id=" . $data['announcement_id']);

//            echo $res->organization_announcement_like_id.'--';
//            echo $res->organization_announcement_num_likes.'------';
            
            if ($res->organization_announcement_like_id > 0) {
                $data_array = array();
                $data_array['announcement_like'] = ($res->announcement_like == 1) ? 0 : 1;
                $return = ($res->announcement_like == 1) ? '' : 'liked';
                $res1 = $db->updateFields("organization_announcement_likes", $data_array, "announcement_id=" . $data['announcement_id'] . " AND user_id=" . $userId);
               
                if ($data_array['announcement_like'] == 0) {
                        if($res_announcement->organization_announcement_num_likes > 0){
                               $count                              = $res_announcement->organization_announcement_num_likes - 1;
                        }
                        else{
                                 $count                              = $res_announcement->organization_announcement_num_likes + 1;
                        }
                         $rescomm                            =  $db->updateFields("organization_announcements", array('organization_announcement_num_likes' => $count), "organization_announcement_id=" . $data['announcement_id']);
//                echo 'this count is returning'.$count;
                }
                
                    
            // No like entry exists for user id, hence create and increment num_count
             else {
               
//                $data['announcement_like'] = '1';
//                $orgId = $db->insert($tableName, $data);
                $count = $res_announcement->organization_announcement_num_likes + 1;
                $rescomm = $db->updateFields("organization_announcements", array('organization_announcement_num_likes' => $count), "organization_announcement_id=" . $data['announcement_id']);
//                $return = 'liked';
            }
             $count=($count>0)?($count.' '.($count==1?'Like':'Likes')):'';
             $return_array = array('classtype' => $return, 'count' => $count);
                print json_encode($return_array);
                exit;
           }
        // No like entry exist for current organization id, hence create one with user id and update organization_announcements table entry
         else {
            
            $data['announcement_like'] = '1';
            $orgId = $db->insert($tableName, $data);
            $count =($res_announcement->organization_announcement_num_likes > 0)?($res_announcement->organization_announcement_num_likes+1):1;
            $rescomm = $db->updateFields("organization_announcements", array('organization_announcement_num_likes' => $count), "organization_announcement_id=" . $data['announcement_id']);
            $return = 'liked';
            $count=($count>0)?($count.' '.($count==1?'Like':'Likes')):'';
            $return_array = array('classtype' => $return, 'count' => $count);
            print json_encode($return_array);
            exit;
        }
//        }
    }
    
    public static function getFeedLikesUsers($id){
        if($id){
        $db                             = new Db();
       // $userId                         = Utils::getAuth('user_id');
        $tableName                      = Utils::setTablePrefix('news_feed_likes');
//        $tableName1                      = Utils::setTablePrefix('communities');
        $query                          = "SELECT news_feed_like_user_name FROM ".$tableName." AS c  WHERE c.news_feed_id=$id and c.news_feed_like <> 1";
       // echo $query; exit;
        $objResultRow = $db->fetchAll($db->execute($query));
        return $objResultRow;
        }
    }
    
    public static function getGroupCommentsLikesUsers($id){
        if($id){
        $db                             = new Db();
       // $userId                         = Utils::getAuth('user_id');
        $tableName                      = Utils::setTablePrefix('announcement_likes');
//        $tableName1                      = Utils::setTablePrefix('communities');
        $query                          = "SELECT user_name FROM ".$tableName." AS c  WHERE c.announcement_id=$id";
       // echo $query; exit;
        $objResultRow = $db->fetchAll($db->execute($query));
        return $objResultRow;
        }
    }
    
    public static function showFeedCommentOfCommentsLikeUser($id){
        if($id){
        $db                             = new Db();
       // $userId                         = Utils::getAuth('user_id');
        $tableName                      = Utils::setTablePrefix('newsfeed_comments_like');
//        $tableName1                      = Utils::setTablePrefix('communities');
        $query                          = "SELECT user_name FROM ".$tableName." AS c  WHERE c.newsfeed_comment_id=$id";
       // echo $query; exit;
        $objResultRow = $db->fetchAll($db->execute($query));
        return $objResultRow;
        }
    }
}
?>
