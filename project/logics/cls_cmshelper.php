<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
class Cmshelper {

    //Fetches Help Categories
 public static function getAllHelpCategories($fields='help_category_id,help_category_title') {
        $db	    = new Db();
        $getAllHelpCategories = $db->selectResult('HelpCategory',$fields," help_category_active='1'");
        $var=0;
        foreach($getAllHelpCategories as $catgory) {
            $result[$var]->value = $catgory->help_category_id;
            $result[$var]->text = $catgory->help_category_title;
            $var++;
        }
        return $result;
    }
    
    public static function imageResize($width, $height, $target) {
    
    
    	if ($width > $height) {
    		$percentage = ($target / $width);
    	} else {
    		$percentage = ($target / $height);
    	}
    
    
    	$width = round($width * $percentage);
    	$height = round($height * $percentage);
    
    
    	return "width:".$width."px ;height:".$height."px";
    
    }
    //Fetches Countries
  public static function getAllCountries($id='') {
        $db	    = new Db();
        $getAllCountries = $db->selectResult('country','tc_code,tc_name'," tc_status='A' ORDER BY tc_name");
        $var=0;   
        foreach($getAllCountries as $country) { 
            $result[$var]->value = $country->tc_code;
            $result[$var]->text = $country->tc_name;
            $var++;
        }
        return $result;
    }

    //Fetches States
    public static function getAllStates($id) {    	
        $db	    = new Db();
        $getAllStates = $db->selectResult('state','ts_code,ts_name'," ts_status='A'");
        $var=0;        
       // echopre($getAllStates);
        foreach($getAllStates as $state) {
            $result[$var]->value = $state->ts_code;
            $result[$var]->text = $state->ts_name;
            $var++;
        }
        return $result;
    }


    //Fetches user Count for dashboard
    public static function getUsersCount($startDate,$endDate) {
        $db     = new Db();        
        $where = "WHERE user_status='A'";       
       $where .= " AND DATE_FORMAT(user_created_on,'%Y-%m-%d')>= '$startDate' AND DATE_FORMAT(user_created_on,'%Y-%m-%d')<= '$endDate'";
        $count  = $db->getDataCount("users",'user_id',$where);       
        Logger::info($result);
       //echopre($count);       
       // print_r($count);
        return $count;
    }
    
    //TODO : get user count
//     //Fetches Customer Regsitartion Count
//     public static function getCustomerRegistrationCount($startDate,$endDate) {

//         $db     = new Db();
//         $count  = $db->getDataCount("customers","nCust_Id"," where vActive='active' AND DATE_FORMAT(dDatejoin,'%Y-%m-%d')>= '".$startDate."' AND DATE_FORMAT(dDatejoin,'%Y-%m-%d')< '".$endDate."'");
//         return $count;
//     }
//end    

    // Function to update User status
    public static function changeUserStatus($values) {
        $id     =  $values['id'];
        $value  =  $values['value'];
        $userStatusUrl = BASE_URL.'cmshelper/ajaxUserStatusChange?id='.$id.'&value='.$value;
        return $userStatusUrl;
    }
 
    public static function updateUserStatus($id,$value) {
        $db = new Db();
        $defaultVal = ($value == 'I')?'A':'I';

        $db->customQuery("UPDATE ".$db->tablePrefix."users SET user_status = '" . $defaultVal . "' WHERE user_id = '" . $id . "'");
        //reduce friends count on Inactive
        $getAllFriends = $db->selectResult('friends_list','friends_list_friend_id'," friends_list_status='A' AND friends_list_user_id=$id");
        if($defaultVal=='I'){
            foreach($getAllFriends as $friends) {
                $userFriendCount                = $db->selectRow('users','user_friends_count',"user_id='".$friends->friends_list_friend_id."'");
                $whereUserCount                 =   "user_id='".$friends->friends_list_friend_id."'"; 
                $friendResultRows               =   $db->updateFields('users', array('user_friends_count'     =>  $userFriendCount-1,
                                                                          ),$whereUserCount,$doAudit=false);
            }
        }
        else if($defaultVal=='A'){
            
            foreach($getAllFriends as $friends) {
                $userFriendCount                = $db->selectRow('users','user_friends_count',"user_id='".$friends->friends_list_friend_id."'");
                $whereUserCount                 =   "user_id='".$friends->friends_list_friend_id."'"; 
                $friendResultRows               =   $db->updateFields('users', array('user_friends_count'     =>  $userFriendCount+1,
                                                                          ),$whereUserCount,$doAudit=false);
            }
        }
        if(ADMIN_APPROVE_USER_AUTO == 0 )
        {
        	if($value == 'I'){
        		Logger::info($value);        		
        		$creatorMailId = $db->selectRecord('users', 'user_email,user_firstname,user_lastname', 'user_id = '.$id);
        		$mailIds             = array();
        		$replaceparams       = array();
        		$mailIds[$creatorMailId->user_email] = '';
        		$replaceparams['FIRST_NAME']                  = $creatorMailId->user_firstname;
        		$replaceparams['LAST_NAME']                   = $creatorMailId->user_lastname;
        		//$replaceparams['EMAIL']                       = $creatorMailId->user_email;        		
        		$objMailer                                    = new Mailer();
        		$objMailer->sendMail($mailIds, 'admin_account_activated', $replaceparams);
        		Logger::info($mailIds);
        	}
        }       
    }
    
    // Function to update Industry status
    public static function changeIndustryStatus($values) {
    	$id     =  $values['id'];
    	$value  =  $values['value'];
    	$userStatusUrl = BASE_URL.'cmshelper/ajaxIndustryStatusChange?id='.$id.'&value='.$value;
    	return $userStatusUrl;
    }
    
//    public static function updateIndustryStatus($id,$value) {
//    	$db = new Db();
//    	$defaultVal = ($value == 'I') ? 'A' : 'I';
//    	$db->customQuery("UPDATE ".$db->tablePrefix."industry SET industry_status = '" . $defaultVal . "' WHERE industry_id  = '" . $id . "'");
//    }
    
    // Function to update Category status
    public static function changeCategoryStatus($values) {
    	$id     =  $values['id'];
    	$value  =  $values['value'];
    	$userStatusUrl = BASE_URL.'cmshelper/ajaxCategoryStatusChange?id='.$id.'&value='.$value;
    	return $userStatusUrl;
    }
    
    public static function updateCategoryStatus($id,$value) {
    	$db = new Db();
    	$defaultVal = ($value == 'I') ? 'A' : 'I';
    	$db->customQuery("UPDATE ".$db->tablePrefix."categories SET category_status = '" . $defaultVal . "' WHERE category_id  = '" . $id . "'");
    }
    
    // Function to update mail template status
    public static function changeMailTemplateStatus($values) {
    	$id     =  $values['id'];
    	
    	
	    	$value  =  $values['value'];
	    	$userStatusUrl = BASE_URL.'cmshelper/ajaxMailTemplateStatusChange?id='.$id.'&value='.$value;
	    	return $userStatusUrl;
    	

    	
    }
        
   
    public static function updateMailTemplateStatus($id,$value) {
    	$db = new Db();
    	$defaultVal = ($value == 1)?0:1;
    	$db->customQuery("UPDATE ".$db->tablePrefix."mail_template SET mail_template_status = '" . $defaultVal . "' WHERE id  = '" . $id . "'");
    }    

    // Function to update business status
    public static function changeBusinessStatus($values) {
        $id     =  $values['id'];
        $value  =  $values['value'];
        $userStatusUrl = BASE_URL.'cmshelper/ajaxBusinessStatusChange?id='.$id.'&value='.$value;
        return $userStatusUrl;
    }
   
    public static function updateBusinessStatus($id,$value) {
        $db = new Db();
        $defaultVal = ($value == 'I')?'A':'I';
        $db->customQuery("UPDATE ".$db->tablePrefix."businesses SET business_status = '" . $defaultVal . "' WHERE business_id = '" . $id . "'");
        if(ADMIN_APPROVE_BUSINESS_AUTO == 0 )
        {
        	if($value == 'I'){
        		Logger::info($value);
        		$creator = $db->selectRecord('businesses', 'business_created_by,business_name', 'business_id = '.$id);
        		$creatorMailId = $db->selectRecord('users', 'user_email,user_firstname,user_lastname', 'user_id = '.$creator->business_created_by);
        		$mailIds             = array();
        		$replaceparams       = array();
        		$mailIds[$creatorMailId->user_email] = '';
        		$replaceparams['FIRST_NAME']                  = $creatorMailId->user_firstname;
        		$replaceparams['LAST_NAME']                   = $creatorMailId->user_lastname;
        		$replaceparams['EMAIL']                       = $creatorMailId->user_email;
        		$replaceparams['TITLE']                    = $creator->business_name;
        		$replaceparams['ENTITY']                    = 'Business';
        		$objMailer                                    = new Mailer();
        		$objMailer->sendMail($mailIds, 'admin_approved', $replaceparams);
        		Logger::info($mailIds);
        	}
        }
    }

    // Function to update classifieds status
    public static function changeClassifiedsStatus($values) {
        $id     =  $values['id'];
        $value  =  $values['value'];
        $userStatusUrl = BASE_URL.'cmshelper/ajaxClassifiedsStatusChange?id='.$id.'&value='.$value;
        return $userStatusUrl;
    }
   
    public static function updateClassifiedsStatus($id,$value) {
        $db = new Db();
        $defaultVal = ($value == 'I')?'A':'I';
        $db->customQuery("UPDATE ".$db->tablePrefix."classifieds SET classifieds_status = '" . $defaultVal . "' WHERE classifieds_id = '" . $id . "'");
        if(ADMIN_APPROVE_CLASSIFIEDS_AUTO == 0 )
        {
        	if($value == 'I'){        	
        	Logger::info($value);
        	$creator = $db->selectRecord('classifieds', 'classifieds_created_by,classifieds_title', 'classifieds_id = '.$id);
        	$creatorMailId = $db->selectRecord('users', 'user_email,user_firstname,user_lastname', 'user_id = '.$creator->classifieds_created_by);
        	$mailIds             = array();
        	$replaceparams       = array();
        	$mailIds[$creatorMailId->user_email] = '';
        	$replaceparams['FIRST_NAME']                  = $creatorMailId->user_firstname;
        	$replaceparams['LAST_NAME']                   = $creatorMailId->user_lastname;
        	$replaceparams['EMAIL']                       = $creatorMailId->user_email;
        	$replaceparams['TITLE']                    = $creator->classifieds_title;
        	$replaceparams['ENTITY']                    = 'Classifieds';
        	$objMailer                                    = new Mailer();
        	$objMailer->sendMail($mailIds, 'admin_approved', $replaceparams);        	
        	Logger::info($mailIds);
        }
        }
    }

    // Function to update community status
    public static function changeCommunityStatus($values) {
    	$id     =  $values['id'];
    	$value  =  $values['value'];
    	$userStatusUrl = BASE_URL.'cmshelper/ajaxCommunityStatusChange?id='.$id.'&value='.$value;
    	return $userStatusUrl;
    }
        
    public static function updateCommunityStatus($id,$value) {
    	$db = new Db();
    	$defaultVal = ($value == 'I')?'A':'I';
    	$db->customQuery("UPDATE ".$db->tablePrefix."communities SET community_status = '" . $defaultVal . "' WHERE community_id = '" . $id . "'");
    	if(ADMIN_APPROVE_BIZCOM_AUTO == 0 )
    	{
    		if($value == 'I'){
    			Logger::info($value);
    			$creator = $db->selectRecord('communities', 'community_created_by,community_name', 'community_id = '.$id);
    			$creatorMailId = $db->selectRecord('users', 'user_email,user_firstname,user_lastname', 'user_id = '.$creator->community_created_by);
    			$mailIds             = array();
    			$replaceparams       = array();
    			$mailIds[$creatorMailId->user_email] = '';
    			$replaceparams['FIRST_NAME']                  = $creatorMailId->user_firstname;
    			$replaceparams['LAST_NAME']                   = $creatorMailId->user_lastname;
    			$replaceparams['EMAIL']                       = $creatorMailId->user_email;
    			$replaceparams['TITLE']                    = $creator->community_name;
    			$replaceparams['ENTITY']                    = 'Community';
    			$objMailer                                    = new Mailer();
    			$objMailer->sendMail($mailIds, 'admin_approved', $replaceparams);
    			Logger::info($mailIds);
    		}
    	}
    }
    // Function to update Comment status
    public static function changeCommentStatus($values) {
    	$id     =  $values['id'];
    	$value  =  $values['value'];
    	$userStatusUrl = BASE_URL.'cmshelper/ajaxCommentStatusChange?id='.$id.'&value='.$value;
    	return $userStatusUrl;
    }
    
    public static function updateCommentStatus($id,$value) {
    	$db = new Db();
    	$defaultVal = ($value == 'I')?'A':'I';
    
    	$db->customQuery("UPDATE ".$db->tablePrefix."comments SET comment_status = '" . $defaultVal . "' WHERE comment_id = '" . $id . "'");
    
    }
    
    // Function to update banner status
  public static function changeBannerStatus($values) {
    	$id     =  $values['id'];
    	$value  =  $values['value'];
    	$userStatusUrl = BASE_URL.'cmshelper/ajaxBannerStatusChange?id='.$id.'&value='.$value;
    	return $userStatusUrl;
    }
        
    public static function updateBannerStatus($id,$value) {
    	$db = new Db();
    	$defaultVal = ($value == 'I')?'A':'I';
    
    	$db->customQuery("UPDATE ".$db->tablePrefix."banners SET banner_status = '" . $defaultVal . "' WHERE banner_id = '" . $id . "'");
    } 
    
    public static function updateReconcileStatus($id,$value) {
    	$db = new Db();
        //$defaultVal = ($value == 'Y')?'N':'Y';
     if($value=='N'){
    	$db->customQuery("UPDATE ".$db->tablePrefix."charity_reconcilation_details SET reconcilation_settle_status = 'Y', reconcilation_settle_date='".date('Y-m-d')."'  WHERE reconcilation_id = '" . $id . "'");
        $reconcileDetails       = $db->selectRecord('charity_reconcilation_details', '*', 'reconcilation_id = '.$id);
        $mailIds                = array();
        $replaceparams          = array();
        $mailIds[$reconcileDetails->user_email] = '';
        $replaceparams['NAME']                  = $reconcileDetails->user_name;
        $replaceparams['CHARITY']                 = $reconcileDetails->charity_organisation_name;
        $replaceparams['AMOUNT']                 = $reconcileDetails->reconcilation_amount;
        $replaceparams['CAMPAIGN_NAME']                = $reconcileDetails->campaign_name;
        $objMailer                                 = new Mailer();
        $objMailer->sendMail($mailIds, 'charity_reconcile_complete', $replaceparams);
        Logger::info($mailIds);
     }
    } 

 
    // dashboard section
    //view all link in dashboard
    public static function getNewUsersLink() {
    	$userUrl = BASE_URL.'cms?section=users';
    	return $userUrl ;
    }
    
    public static function getNewBusinessLink() {
    	$userUrl = BASE_URL.'cms?section=business';
    	return $userUrl ;
    }
    
    public static function getNewClassifiedsLink() {
    	$userUrl = BASE_URL.'cms?section=classifieds';
    	return $userUrl ;
    }
    
    public static function getNewCommunityLink() {
    	$userUrl = BASE_URL.'cms?section=bizcom';
    	return $userUrl ;
    }

    // dashboard table display data
    public static function getNewUsers() {
    	$db = new Db();

    	$users                      =   Utils::setTablePrefix('users');
    	//$files                      =   Utils::setTablePrefix('files');    	
    	     	    	    	 
    	$select = "SELECT user_id, CONCAT(user_firstname,' ',user_lastname) AS username, user_email AS email,  DATE_FORMAT(user_created_on,'%m/%d/%Y') AS joined_on FROM $users WHERE user_status !='D' ORDER BY user_id DESC LIMIT 0,5 " ;
    	//echo $select;
    	$result = $db->selectQuery($select);
    	foreach ($result as $data)
    	{
    		if(strlen($data->email) > 20){
    			$data->email = substr($data->email, 0,16)." ...";
    		}
    		if(strlen($data->username) > 20){
    			$data->username = substr($data->username, 0,16)." ...";
    		}
    		$data->username = "<a href='".BASE_URL."cms?section=users&action=showDetails#$data->user_id' class='jqShowDetails' >".ucwords($data->username)."</a>";
    	}
    	
    	//echopre($result);
    	Logger::info($result);
    	return $result;
    }
    
    public static function getNewBusiness() {
    	$db = new Db();
    
    	$users                      =   Utils::setTablePrefix('users');
    	$business                      =   Utils::setTablePrefix('businesses');
    	//$files                      =   Utils::setTablePrefix('files');
//    	$industry                      =   Utils::setTablePrefix('industry');
    
    	//$select = "SELECT business_name AS businessname, industry_name AS industry,file_path AS image,CONCAT(user_firstname,' ',user_lastname) AS created_by, DATE_FORMAT(business_created_on,'%m/%d/%Y') AS created_on FROM $business LEFT JOIN $files ON   $business.business_image_id = $files.file_id LEFT JOIN $industry ON $business.business_industry_id = $industry.industry_id LEFT JOIN $users ON $business.business_created_by = $users.user_id ORDER BY business_id DESC LIMIT 0,5" ;
    	$select = "SELECT business_id, business_name AS businessname, CONCAT(user_firstname,' ',user_lastname) AS created_by, DATE_FORMAT(business_created_on,'%m/%d/%Y') AS created_on FROM $business LEFT JOIN $users ON $business.business_created_by = $users.user_id WHERE business_status !='D' ORDER BY business_id DESC LIMIT 0,5" ;
    	//echo $select;
    	$result = $db->selectQuery($select);
    	foreach ($result as $data)
    	{
    		$data->created_by = ucwords($data->created_by);
    		if($data->created_by == '')
    		{
    		$data->created_by = 'Admin';
    		}
    		if(strlen($data->businessname) > 16){
    			$data->businessname = substr($data->businessname, 0,11)." ...";
    		}
    		$data->businessname = "<a href='".BASE_URL."cms?section=business&action=showDetails#$data->business_id' class='jqShowDetails' >".$data->businessname."</a>";
    		//$data->businessname = ucwords($data->businessname);
    		$data->industry = ucwords($data->industry);
    		if(strlen($data->industry) >= 16){
    			$data->industry = substr($data->industry, 0,11)." ...";
    		}
    		
    		/*
    		$filePath='';
    		if($data->image=='')
    		{
    			$filePath  =   BASE_URL. 'modules/cms/images/'."noImagePlaceholder.JPG";
    		}
    		else
    		{    			
    			if(!file_exists('project/files/'.$data->image))
    				$filePath  =   BASE_URL. 'modules/cms/images/'."missing-image.png";
    			else
    			{
    				$filePath = BASE_URL. 'project/files/'.$data->image;
    			}
    		}    		
    		$data->image = '   <ul class="thumbnails">
                    <li class="span">
                    <a href="#" class="thumbnail">
                    <img src="'.$filePath.'" style="width:60px; height:60px; ">
                    </a>
                    </li>
                    </ul>';    		
                    */
    		
    	}
    	//echopre($result);
    	Logger::info($result);
    	return $result;
    }
    
    public static function getNewClassifieds() {
    	$db = new Db();
    
    	$classifieds                      =   Utils::setTablePrefix('classifieds');
    	$users                      =   Utils::setTablePrefix('users');
    	//$files                      =   Utils::setTablePrefix('files');
    
    	$select = "SELECT classifieds_id, classifieds_title AS title, CONCAT(user_firstname,' ',user_lastname) AS created_by, DATE_FORMAT(classifieds_created_on,'%m/%d/%Y') AS created_on FROM $classifieds LEFT JOIN $users ON $classifieds.classifieds_created_by = $users.user_id WHERE classifieds_status !='D' ORDER BY classifieds_id DESC LIMIT 0,5" ;
    	//echo $select;
    	$result = $db->selectQuery($select);
    	foreach ($result as $data)
    	{
    		if(strlen($data->title) > 30){
    			$data->title = substr($data->title, 0,26)." ...";
    		}
    		$data->title = "<a href='".BASE_URL."cms?section=classifieds&action=showDetails#$data->classifieds_id' class='jqShowDetails' >".$data->title."</a>";
    		//$data->title = ucwords($data->title);
    		$data->created_by = ucwords($data->created_by);
    		if($data->created_by == '')
    		{
    			$data->created_by = 'Admin';
    		}
    	}
    	//echopre($result);
    	Logger::info($result);
    	return $result;
    }
    
    public static function getNewCommunity() {
    	$db = new Db();
    
    	$community                  =   Utils::setTablePrefix('communities');
    	$users                      =   Utils::setTablePrefix('users');
    	//$files                    =   Utils::setTablePrefix('files');
        
    	$select = "SELECT  community_id, community_name AS communityname, CONCAT(user_firstname,' ',user_lastname) AS created_by, DATE_FORMAT(community_created_on,'%m/%d/%Y') AS created_on FROM $community LEFT JOIN $users ON $community.community_created_by = $users.user_id WHERE community_status !='D' ORDER BY community_id DESC LIMIT 0,5" ;
    	//echo $select;
    	$result = $db->selectQuery($select);
    	foreach ($result as $data)
    	{
    		if(strlen($data->communityname) > 30){
    			$data->communityname = substr($data->communityname, 0,26)." ...";
    		}
    		$data->communityname = "<a href='".BASE_URL."cms?section=bizcom&action=showDetails#$data->community_id' class='jqShowDetails' >".$data->communityname."</a>";
    		//$data->communityname = ucwords($data->communityname);
    		$data->created_by = ucwords($data->created_by);
    		if($data->created_by == '')
    		{
    			$data->created_by = 'Admin';
    		}
    	}
    	//echopre($result);
    	Logger::info($result);
    	return $result;
    }
    
    //dashboard data
    public static function getStatistics() {
    	$db     = new Db();
    	$startdate = date('Y-m-d', strtotime('1 month ago'));
    	$enddate = date('Y-m-d', strtotime('now'));
    	
    	$countUser  = $db->getDataCount("users","user_id"," WHERE user_status!='D' AND DATE_FORMAT(user_created_on,'%Y-%m-%d')>= '$startdate' AND DATE_FORMAT(user_created_on,'%Y-%m-%d')<= '$enddate'");
    	$countBusiness  = $db->getDataCount("businesses","business_id"," WHERE business_status!='D' AND DATE_FORMAT(business_created_on,'%Y-%m-%d')>= '$startdate' AND DATE_FORMAT(business_created_on,'%Y-%m-%d')<= '$enddate'");
    	$countClassifiedsApproved  = $db->getDataCount("classifieds","classifieds_id"," WHERE classifieds_status='A' AND DATE_FORMAT(classifieds_created_on,'%Y-%m-%d')>= '$startdate' AND DATE_FORMAT(classifieds_created_on,'%Y-%m-%d')<= '$enddate'");
    	$countClassifiedsPending  = $db->getDataCount("classifieds","classifieds_id"," WHERE classifieds_status='I' AND DATE_FORMAT(classifieds_created_on,'%Y-%m-%d')>= '$startdate' AND DATE_FORMAT(classifieds_created_on,'%Y-%m-%d')<= '$enddate'");
    	$countCommunity  = $db->getDataCount("communities","community_id"," WHERE community_status!='D' AND DATE_FORMAT(community_created_on,'%Y-%m-%d')>= '$startdate' AND DATE_FORMAT(community_created_on,'%Y-%m-%d')<= '$enddate'");
    	$count = array();
    	$count[0]= new stdClass();
    	$count[1]= new stdClass();
    	$count[2]= new stdClass();
    	//$count[3]= new stdClass();
    	//$count[4]= new stdClass();
    	
    	$count[0]->title = 'Number Of Sign Ups';
    	$count[1]->title = 'Number Of Page Created';
    	$count[2]->title = 'Number Of Group Created';
    	//$count[3]->title = 'Number Of Classifieds Pending Approval';
    	//$count[4]->title = 'Number Of Classifieds Approved';
    	
    	$count[0]->count = $countUser;
    	$count[1]->count = $countBusiness;
    	$count[2]->count = $countCommunity;
    	//$count[3]->count = $countClassifiedsPending;
    	//$count[4]->count = $countClassifiedsApproved;
    	//echopre($count);
    	Logger::info($count);
    	return $count;
    }
    //end dashboard section
    
    //settings section 
    public static function changeLogo($postedArray) {
    	//echopre($_FILES); exit;
    	//$flag = 0;
    	$fileHandler = new Filehandler(); 
    	//  print_r($_FILES); //exit;    	
//     	if($postedArray['GoogleMapValue'] == ''){
//     		$postedArray['error'] = 'ERROR';
//     		$postedArray['errormessage'] = 'Please enter the Google Map Key.';
//     		//$flag = 1;
//     		return $postedArray;
//     		}    	
    	if($postedArray['googleaddemo'] != ''){
    		if($postedArray['googleadvalue'] == ''){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = 'Please enter the Invocation Code for Advertisement.';
    			return $postedArray;
    		}
    	}
       
    	//if($postedArray['EnableSMTP'] != ''){
    		if($postedArray['SMTPHost'] == '' || $postedArray['SMTPUsername'] == '' || $postedArray['SMTPPassword'] == '' || $postedArray['SMTPPort'] == ''){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = 'Please enter the SMTP details.';
    			return $postedArray;
    		}
    	 if($postedArray['SMTPUsername'] != '' && verify_email($postedArray['SMTPUsername']) == false){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = 'Please enter valid SMTP Username.';
    			return $postedArray;
    		}
    	//}
    	if($postedArray['vadmin_email'] == '' || verify_email($postedArray['vadmin_email']) == false){
    		$postedArray['error'] = 'ERROR';
    		$postedArray['errormessage'] = 'Invalid Admin Email.';
    		return $postedArray;
    	}
    	
//        if($postedArray['bbf_charge_0_50'] == '' || !is_numeric($postedArray['bbf_charge_0_50'])){
//    		$postedArray['error'] = 'ERROR';
//    		$postedArray['errormessage'] = 'Please enter valid charge for 1st category.';
//    		return $postedArray;
//    	}
//        
//        if($postedArray['bbf_charge_50_100'] == '' || !is_numeric($postedArray['bbf_charge_50_100'])){
//    		$postedArray['error'] = 'ERROR';
//    		$postedArray['errormessage'] = 'Please enter valid charge for 2nd category.';
//    		return $postedArray;
//    	}
//        
//        if($postedArray['bbf_charge_100plus'] == '' || !is_numeric($postedArray['bbf_charge_100plus'])){
//    		$postedArray['error'] = 'ERROR';
//    		$postedArray['errormessage'] = 'Please enter valid charge for 3rd category.';
//    		return $postedArray;
//    	}
//        
//        if($postedArray['bbf_charge_1000plus'] == '' || !is_numeric($postedArray['bbf_charge_1000plus'])){
//    		$postedArray['error'] = 'ERROR';
//    		$postedArray['errormessage'] = 'Please enter valid charge for 4th category.';
//    		return $postedArray;
//    	}
        
    	$fileHandler->file_upload_dir = FILE_UPLOAD_DIR."logo/";
    	if($_FILES['sitelogo']['name'] !='')
    	{
    		$type = $_FILES['sitelogo']['type'];
    		$type1 = @explode('/', $type);
    		if($type1[0] != 'image' || !in_array($type1[1], array("gif", "jpeg", "jpg", "png"))){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = "File format not supported.";
    			return $postedArray;
    		}
                
                $image_info = getimagesize($_FILES["sitelogo"]["tmp_name"]);
                $image_width = $image_info[0];
                $image_height = $image_info[1];
                $maxwidth = 246;
                $maxheight = 50;
    		if($image_width > $maxwidth){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = "Site Logo Image Width can not be more than {$maxwidth}.";
    			return $postedArray;
    		}
    		if($image_height > $maxheight){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = "Site Logo Image Height can not be more than {$maxheight}.";
    			return $postedArray;
    		}
                
    		try{
    			$siteLogoFileDetails = $fileHandler->handleUpload($_FILES['sitelogo']);
    			$postedArray['sitelogo'] = $siteLogoFileDetails->file_path;
    		}
    		catch (Exception $e){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = $e->getMessage();
    			return $postedArray;
    		}
    	}
        
        
    	$fileHandler->file_upload_dir = FILE_UPLOAD_DIR."logo/";
    	if($_FILES['innerlogo']['name'] !='')
    	{
    		$type = $_FILES['innerlogo']['type'];
    		$type1 = @explode('/', $type);
    		if($type1[0] != 'image' || !in_array($type1[1], array("gif", "jpeg", "jpg", "png"))){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = "File format not supported.";
    			return $postedArray;
    		}
                
                $image_info = getimagesize($_FILES["innerlogo"]["tmp_name"]);
                $image_width = $image_info[0];
                $image_height = $image_info[1];
                $maxwidth = 30;
                $maxheight = 30;
    		if($image_width > $maxwidth){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = "Inner Logo Image Width can not be more than {$maxwidth}.";
    			return $postedArray;
    		}
    		if($image_height > $maxheight){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = "Inner Logo Image Height can not be more than {$maxheight}.";
    			return $postedArray;
    		}
                
    		try{
    			$siteLogoFileDetails = $fileHandler->handleUpload($_FILES['innerlogo']);
    			$postedArray['innerlogo'] = $siteLogoFileDetails->file_path;
    		}
    		catch (Exception $e){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = $e->getMessage();
    			return $postedArray;
    		}
    	}
        $fileHandler->file_upload_dir = FILE_UPLOAD_DIR."favicon/";
    	if($_FILES['favicon_img']['name'] !='')
    	{
    		$type = $_FILES['favicon_img']['type'];
    		$type1 = @explode('/', $type);
    		if(!in_array($type1[1], array("x-icon"))){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = "File format not supported.";
    			return $postedArray;
    		}
                
                $image_info = getimagesize($_FILES["favicon_img"]["tmp_name"]);
                $image_width = $image_info[0];
                $image_height = $image_info[1];
                $maxwidth = 16;
                $maxheight = 16;
    		if($image_width > $maxwidth){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = "Favicon Width can not be more than {$maxwidth}.";
    			return $postedArray;
    		}
    		if($image_height > $maxheight){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = "Favicon Height can not be more than {$maxheight}.";
    			return $postedArray;
    		}
                
    		try{
    			$siteLogoFileDetails = $fileHandler->handleUpload($_FILES['favicon_img']);
    			$postedArray['favicon_img'] = $siteLogoFileDetails->file_path;
    		}
    		catch (Exception $e){
    			$postedArray['error'] = 'ERROR';
    			$postedArray['errormessage'] = $e->getMessage();
    			return $postedArray;
    		}
    	}
        
        
//     	if($_FILES['email_logo']['name'] !='')
//     	{
//     	$emailLogoFileDetails = $fileHandler->handleUpload($_FILES['email_logo']);    	  
//     	$postedArray['email_logo'] = $emailLogoFileDetails->file_path;
//     	}
    	//echopre($postedArray);
    	//exit;    	
    	return $postedArray;
    }
    
    //unique email validation
    public static function userUpdate($postedArray) {
    	//echopre($postedArray);exit;
    	if(User::checkUserEmailExists($postedArray['user_email'],$postedArray['user_id']) > 0){
    		$postedArray['error'] = 'ERROR';
    		$postedArray['errormessage'] = 'Email already exists';
    	}
        $filter = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{8,255}$/';
        if(!preg_match($filter,$postedArray['user_password'])){
    		$postedArray['error'] = 'ERROR';
    		$postedArray['errormessage'] = 'Please enter a password with at least 8 characters, an uppercase character and a numeric.';
    	}
        
//     	else {
//     		$mailIds             = array();
//     		$replaceparams       = array();
//     		$mailIds[$postedArray['user_email']] = '';
//     		$replaceparams['FIRST_NAME']                  = $postedArray['user_firstname'];
//     		$replaceparams['LAST_NAME']                   = $postedArray['user_lastname'];
//     		$replaceparams['EMAIL']                       = $postedArray['user_email'];
//     		$replaceparams['PASSWORD']                    = $postedArray['user_password'];
//     		$objMailer                                    = new Mailer();
//     		$objMailer->sendMail($mailIds, 'admin_user_registration', $replaceparams);
//     	}
    	//echopre($postedArray);
    	//exit;
    	return $postedArray;
    }
    
	// admin login as user
    public static function getUserLoginUrl($userid) {
        $userUrl = BASE_URL.'user/getUserLoggedInSession/'.$userid;
        $link = "<a href='".$userUrl."' target='_blank'>Login</a>";
        return $link ;
    }
 
  //Cmshelper::bindSiteLogo
    public static function bindSiteLogo() {
        $content = '<img alt="'.SITE_NAME.'" src="'.Cmshelper::getSiteLogo().'" />';
        return $content;
    }
    public static function getSiteLogo() {
        $dbh = new Db();
        $logoDetails= $dbh->selectRecord("lookup", "value"," settingfield='sitelogo'");

        $siteLogo =  BASE_URL.'project/files/logo/'.$logoDetails->value;
        return $siteLogo;
    } // End Function
 
    //external columns  & custom fields
//    public static function getAllIndustryName($id) {
//    	$db	    = new Db();
//    	//echo $id;
//    	$getAllStates = $db->selectResult('industry','industry_id,industry_name',"industry_parent_id=0 AND industry_status='A'");
//    	$var=0;
//    	foreach($getAllStates as $state) {
//    		$result[$var]->value = $state->industry_id;
//    		$result[$var]->text = $state->industry_name;
//    		$var++;    		
//    	}
//    	return $result;
//    }
    
    public static function getAllCategoryName($id) {
    	$db	    = new Db();
    	//echo $id;
    	$getAllStates = $db->selectResult('categories','category_id,category_name',"category_status='A'");
    	$var=0;
    	foreach($getAllStates as $state) {
    		$result[$var]->value = $state->category_id;
    		$result[$var]->text = $state->category_name;
    		$var++;
    	}
    	return $result;
    }
    
    public static function getCategoryAlias($id1,$postedArray) {
//     	echo $id1;
//     	echopre($postedArray);
    	$db	    = new Db();
    	$table = 'categories';
    	$alias                  =   Utils::generateAlias(MYSQL_TABLE_PREFIX.$table,$postedArray['category_name'], $idColumn="category_id",$id = $excludeId, $aliasColumn="category_alias");
//     	$postedArray['category_alias'] = $alias; 
    	$data = array();
    	$data['category_alias'] = $alias;
    	$db->update(MYSQL_TABLE_PREFIX.$table, $data,'category_id="'.$db->escapeString($id1).'"');
    	//return $postedArray;
//     	exit;
    }
    
//    public static function getAllIndustries($id) {
//    	$db	    = new Db();    	
//    	$getAllStates = $db->selectResult('industry','industry_id,industry_name'," industry_status='A'");
//    	$var=0;
//    	foreach($getAllStates as $state) {
//    		$result[$var]->value = $state->industry_id;
//    		$result[$var]->text = $state->industry_name;
//    		$var++;
//    	}
//    	return $result;
//    }
//    
    public static function  getFriendName($id) {
    	$db	    = new Db();    
    	$flist = $db->selectResult('friends_list','friends_list_friend_id'," friends_list_status='A' AND friends_list_id=$id");
    	$id2 = $flist[0]->friends_list_friend_id;
        if($id2)
        {
    	$getAllFriendName = $db->selectResult('users','user_firstname,user_lastname'," user_status='A' AND user_id=$id2");
    	$var=0;
    	foreach($getAllFriendName as $state) {
    		$result[$var]->value = ucwords($state->user_firstname ." " . $state->user_lastname);    		
    		$var++;
    	}  
    	return $result[0]->value;
        }
        else {
            return '';
        }
    }
    
    public static function  getFriendGender($id) {
    	$db	    = new Db();    	
    	$flist = $db->selectResult('friends_list','friends_list_friend_id'," friends_list_status='A' AND friends_list_id=$id");    
    	$id2 = $flist[0]->friends_list_friend_id;    
         if($id2)
        {
    	$getAllFriendGender = $db->selectResult('users','user_gender'," user_status='A' AND user_id=$id2");
    	$var=0;
    	foreach($getAllFriendGender as $state) {
    		$result[$var]->value = ucfirst($state->user_gender);    
    		$var++;
    	}
        
    	return $result[0]->value;
        }
        else
        {
            return '';
        }
    }
    
    public static function  getFriendImage($id) {
    	$db	    = new Db();
    	$flist = $db->selectResult('friends_list','friends_list_friend_id'," friends_list_status='A' AND friends_list_id=$id");
    	$id2 = $flist[0]->friends_list_friend_id;
        if($id2)
        {
    	$getAllFriendImage = $db->selectResult('users','user_image_id',"user_id=$id2");
    	$var=0;
    	$filePath='';
    	foreach($getAllFriendImage as $data) {
    		if($data->user_image_id==0)
    		{
    				$filePath  =   BASE_URL. 'modules/cms/images/'."noImagePlaceholder.JPG";
    		}		
    		else 
    		{
    				$fileName = $db->selectResult('files','file_path',"  file_id='".$data->user_image_id."'");
    				if(!file_exists('project/files/'.$fileName[0]->file_path))
    					$filePath  =   BASE_URL. 'modules/cms/images/'."missing-image.png";
    				else
    				{
    					$filePath = BASE_URL. 'project/files/'.$fileName[0]->file_path;
    				}
    		}
    		$result[$var]->value = $filePath;
    		$var++;
    	}
    //	$originalWidth = $fileName[0]->file_width;
    	//$originalHeight = $fileName[0]->file_height;
    	list($originalWidth, $originalHeight) = getimagesize($filePath);
    	$dimensions =  self::imageResize($originalWidth, $originalHeight, 60);
        if($dimensions ==  "width:0px ;height:0px"){
          $dimensions =  "width:60px ;height:60px";
        }
        
    	return  '   <ul class="thumbnails">
                    <li class="span">
                    <a href="#" class="thumbnail">
                    <img src="'.$filePath.'" style="'.$dimensions.';">
                    </a>
                    </li>
                    </ul>';    	 
        }
        else
        {
            return '';
        }
    }
    
    public static function  getFriendCountry($id) {
    	$db	    = new Db();    
    	$flist = $db->selectResult('friends_list','friends_list_friend_id'," friends_list_status='A' AND friends_list_id=$id");
    	$id2 = $flist[0]->friends_list_friend_id;
        if($id2)
        {
         
    	$getAllFriendCountry = $db->selectResult('users','user_country'," user_status='A' AND user_id=$id2");
    	$var=0;
    	foreach($getAllFriendCountry as $state) {
    		$country = $db->selectResult('country','tc_name'," tc_status='A' and tc_code='".$state->user_country."'");
    		$result[$var]->value = ucfirst($country[0]->tc_name);    	
    		$var++; 
    	}    
    	return $result[0]->value;
           
        }
        else
        {
            return '';
        }
    }

   public static function  getCommunityMemberImage($id) {
    	$db	    = new Db();
    	$flist = $db->selectResult('community_member','cmember_id'," cmember_status='A' AND cmember_list_id=$id");
    	$id2 = $flist[0]->cmember_id;
        if($id2)
        {
    	$getAllFriendImage = $db->selectResult('users','user_image_id',"user_id=$id2");
    	$var=0;
    	$filePath='';
    	foreach($getAllFriendImage as $data) {
    		if($data->user_image_id==0)
    		{
    				$filePath  =   BASE_URL. 'modules/cms/images/'."noImagePlaceholder.JPG";
    		}		
    		else 
    		{
    				$fileName = $db->selectResult('files','file_path',"  file_id='".$data->user_image_id."'");
    				if(!file_exists('project/files/'.$fileName[0]->file_path))
    					$filePath  =   BASE_URL. 'modules/cms/images/'."missing-image.png";
    				else
    				{
    					$filePath = BASE_URL. 'project/files/'.$fileName[0]->file_path;
    				}
    		}
    		$result[$var]->value = $filePath;
    		$var++;
    	}    	
    	 //	$originalWidth = $fileName[0]->file_width;
    	//$originalHeight = $fileName[0]->file_height;
    	list($originalWidth, $originalHeight) = getimagesize($filePath);
    	$dimensions =  self::imageResize($originalWidth, $originalHeight, 60);
        if($dimensions ==  "width:0px ;height:0px"){
          $dimensions =  "width:60px ;height:60px";
        }
        
    	return  '   <ul class="thumbnails">
                    <li class="span">
                    <a href="#" class="thumbnail">
                    <img src="'.$filePath.'" style="'.$dimensions.';">
                    </a>
                    </li>
                    </ul>';   
        }
        else
        {
            return '';
        }
    }

    public static function  getCommunityImage($id) {
    	$db	    = new Db();
    	$flist = $db->selectResult('community_member','cmember_community_id'," cmember_status!='D' AND cmember_list_id=$id");
    	$id2 = $flist[0]->cmember_community_id;
        if($id2)
        {
    	$getAllFriendImage = $db->selectResult('communities','community_image_id',"community_id=$id2");
    	$var=0;
    	$filePath='';
    	foreach($getAllFriendImage as $data) {
    		if($data->community_image_id==0)
    		{
    			$filePath  =   BASE_URL. 'modules/cms/images/'."noImagePlaceholder.JPG";
    		}
    		else
    		{
    			$fileName = $db->selectResult('files','file_path',"  file_id='".$data->community_image_id."'");
    			if(!file_exists('project/files/'.$fileName[0]->file_path))
    				$filePath  =   BASE_URL. 'modules/cms/images/'."missing-image.png";
    			else
    			{
    				$filePath = BASE_URL. 'project/files/'.$fileName[0]->file_path;
    			}
    		}
    		$result[$var]->value = $filePath;
    		$var++;
    	}
    	 //	$originalWidth = $fileName[0]->file_width;
    	//$originalHeight = $fileName[0]->file_height;
    	list($originalWidth, $originalHeight) = getimagesize($filePath);
    	$dimensions =  self::imageResize($originalWidth, $originalHeight, 60);
        if($dimensions ==  "width:0px ;height:0px"){
          $dimensions =  "width:60px ;height:60px";
        }
        
    	return  '   <ul class="thumbnails">
                    <li class="span">
                    <a href="#" class="thumbnail">
                    <img src="'.$filePath.'" style="'.$dimensions.';">
                    </a>
                    </li>
                    </ul>';    	
        }
        else
        {
            return '';
        }
    }
    
    public static function  getClassifiedsCreatorName($id) {
    	$db	    = new Db();    	
    	$flist = $db->selectResult('classifieds','classifieds_created_by',"classifieds_id=$id");
    	$id2 = $flist[0]->classifieds_created_by;
    	if($id2==0)
    	{
    		return 'Admin';
    	}
        if($id2)
        {
    	$getAllFriendCountry = $db->selectResult('users','user_id,user_firstname,user_lastname'," user_status!='D' AND user_id=$id2");
    	$var=0;
    	foreach($getAllFriendCountry as $state) {    	
    		$result[$var]->value = "<a href='".BASE_URL."cms?section=users&action=showDetails#$state->user_id' class='jqShowDetails' >".ucwords($state->user_firstname ." " . $state->user_lastname)."</a>";    		
    		$var++;
    	}    	
    	return $result[0]->value;
        }
        else
        {
            return '';
        }
    }
    
    public static function  getBusinessCreatorName($id) {
    	$db	    = new Db();    	
    	$flist = $db->selectResult('businesses','business_created_by',"business_id=$id");
    	$id2 = $flist[0]->business_created_by;
    	if($id2==0)
    	{
    		return 'Admin';
    	}
        if($id2)
        {
          
    	$getAllFriendCountry = $db->selectResult('users','user_id,user_firstname,user_lastname'," user_status!='D' AND user_id=$id2");
    	$var=0;
    	foreach($getAllFriendCountry as $state) {    		
    		$result[$var]->value = "<a href='".BASE_URL."cms?section=users&action=showDetails#$state->user_id' class='jqShowDetails' >".ucwords($state->user_firstname ." " . $state->user_lastname)."</a>";    		
    		$var++;
    	}    	
    	return $result[0]->value;
          
        }
        else
        {
            return '';
        }
    }
    
    public static function  getCommentCreatorName($id) {
    	$db	    = new Db();    	
    	$flist = $db->selectResult('comments','comment_created_by',"comment_id=$id");
    	$id2 = $flist[0]->comment_created_by;
    	if($id2==0)
    	{
    		return 'Admin';
    	}
        if($id2)
        {
    	$getAllFriendCountry = $db->selectResult('users','user_firstname,user_lastname'," user_status!='D' AND user_id=$id2");
    	$var=0;
    	foreach($getAllFriendCountry as $state) {    		
    		$result[$var]->value = ucwords($state->user_firstname ." " . $state->user_lastname);    		
    		$var++;
    	}    
    	return $result[0]->value;
        }
        else
        {
            return '';
        }
    }
    
    public static function  getMemberName($id) {
    	$db	    = new Db();
    	$flist = $db->selectResult('community_member','cmember_id',"cmember_list_id=$id");
    	$id2 = $flist[0]->cmember_id;
        if($id2)
        {
    	$getAllFriendCountry = $db->selectResult('users','user_firstname,user_lastname'," user_status!='D' AND user_id=$id2");
    	$var=0;
    	foreach($getAllFriendCountry as $state) {
    		$result[$var]->value = ucwords($state->user_firstname ." " . $state->user_lastname);
    		$var++;
    	}
    	return $result[0]->value;
        }
        else
        {
            return '';
        }
    }
    
    public static function  getCommunityOwner($id) {
    	$db	    = new Db();
    	$flist = $db->selectResult('communities','community_created_by',"community_id=$id");
    	$id2 = $flist[0]->community_created_by;
    	if($id2==0)
    	{
    		return 'Admin';
    	}
        if($id2)
        {
    	$getAllFriendCountry = $db->selectResult('users','user_id,user_firstname,user_lastname'," user_status!='D' AND user_id=$id2");
    	$var=0;
    	foreach($getAllFriendCountry as $state) {
    		//$result[$var]->value = ucwords($state->user_firstname ." " . $state->user_lastname);
    		$result[$var]->value = "<a href='".BASE_URL."cms?section=users&action=showDetails#$state->user_id' class='jqShowDetails' >".ucwords($state->user_firstname ." " . $state->user_lastname)."</a>";
    		$var++;
    	}
    	return $result[0]->value;
        }
        else
        {
            return '';
        }
    }
    
    //soft delete
    public static function deleteUser($id) {
    
    	$dbh                     = new Db();    	
    	$postedArray = array('user_status' => 'D');
    	$status = $dbh->updateFields('users', $postedArray, "user_id=$id");
        //reduce friends count on delete
        $getAllFriends = $dbh->selectResult('friends_list','friends_list_friend_id'," friends_list_status='A' AND friends_list_user_id=$id");
    	foreach($getAllFriends as $friends) {
            $userFriendCount                = $dbh->selectRow('users','user_friends_count',"user_id='".$friends->friends_list_friend_id."'");
            $whereUserCount                 =   "user_id='".$friends->friends_list_friend_id."'"; 
            $friendResultRows               =   $dbh->updateFields('users', array('user_friends_count'     =>  $userFriendCount-1,
                                                                      ),$whereUserCount,$doAudit=false);
    	}
    	/*
    	$postedArray = array('friends_list_status' => 'D');
    	$status = $dbh->updateFields('friends_list', $postedArray, "friends_list_user_id=$id OR friends_list_friend_id=$id");
    
    	$postedArray = array('classifieds_status' => 'D');
    	$status = $dbh->updateFields('classifieds', $postedArray, "classifieds_created_by=$id ");
    	
    	$postedArray = array('business_status' => 'D');
    	$status1 = $dbh->updateFields('businesses', $postedArray, "business_created_by=$id ");
    	echopre ($status);
    	if($status1 !='0')
    	{
    	$postedArray = array('comment_status' => 'D');
    	$status = $dbh->updateFields('comments', $postedArray, "comment_entity_type='B' AND comment_entity_id=$status1 ");
    	}
    	
    	$postedArray = array('comment_status' => 'D');
    	$status = $dbh->updateFields('comments', $postedArray, "comment_entity_type='M' AND comment_entity_id=$id ");
    	*/
    	
    	$message = array("status"=>"success");
    
    	return $message;
    }
    
    public static function deleteClassifieds($id) {
    
    	$dbh                     = new Db();    	
    	
    	$postedArray = array('classifieds_status' => 'D');
    	$status = $dbh->updateFields('classifieds', $postedArray, "classifieds_id=$id ");    	     	
    	
    	$message = array("status"=>"success");
    
    	return $message;
    }
    
    public static function deleteBusiness($id) {
    
    	$dbh                     = new Db();
    	    	
    	$postedArray = array('business_status' => 'D');
    	$status = $dbh->updateFields('businesses', $postedArray, "business_id=$id ");
    
    	/*
    	$postedArray = array('comment_status' => 'D');
    	$status = $dbh->updateFields('comments', $postedArray, "comment_entity_type='B' AND comment_entity_id=$id ");
    	*/
    	$creator = $dbh->selectResult('businesses','business_created_by',"business_id=$id");
    	if($creator[0]->business_created_by >0){
    		Business::updatebusinessCount($creator[0]->business_created_by);
    	}
    	$message = array("status"=>"success");
    
    	return $message;
    }
    
    public static function deleteComment($id) {
    
    	$dbh                     = new Db();
    
    	$postedArray = array('comment_status' => 'D');
    	$status = $dbh->updateFields('comments', $postedArray, "comment_id=$id ");
    
    	$message = array("status"=>"success");
    
    	return $message;
    }
    
    public static function deleteCommunity($id) {
    
    	$dbh                     = new Db();
    
    	$postedArray = array('community_status' => 'D');
    	$status = $dbh->updateFields('communities', $postedArray, "community_id=$id ");
    	$creator = $dbh->selectResult('communities','community_created_by',"community_id=$id");
    	if($creator[0]->community_created_by >0){
    		Communities::updateCommunityCounts($creator[0]->community_created_by);
    	}
    	$message = array("status"=>"success");
    
    	return $message;
    }

    public static function updateBanner($id) {
    	
    	$db = new Db();
    	$table                          = 'banners';
    	$dataArray                      = array();
    	
    	$data1 = $db->selectRecord($table, 'banner_image_id', 'banner_id = '.$id);
    	//print_r($data1); print_r($postedArray); exit;
    	if($data1->banner_image_id >0){
    	$file = $db->selectRecord('files', 'file_path,file_width,file_height', 'file_id = '.$data1->banner_image_id);
    	//echopre($file);
    	$dataArray['banner_image_name']         = $file->file_path; 
    	if($file->file_width > 560 && $file->file_height > 446){ //echo 2;
//     		if($file->file_width == $file->file_height){ //echo 1;
//     			Utils::allResize($file->file_path,"slider", 256, 256);
//     		}
//     		else{ //echo 3;
//     			Utils::allResize($file->file_path,"slider", 560, 446);
//     		}
    		Utils::allResize($file->file_path,"slider", 560, 446);
    	}
    	else if($file->file_width > 560){ //echo 2;
    		
    			Utils::allResize($file->file_path,"slider", 560, $file->file_height);
    		
    	}
    	else if($file->file_height > 446){ //echo 2;
    	
    		Utils::allResize($file->file_path,"slider", $file->file_width, 446);
    	
    	}
    	else{ //echo 4;
    		$status = copy(FILE_UPLOAD_DIR.$file->file_path, FILE_UPLOAD_DIR."slider/".$file->file_path);
    	}
    	//$img = new gdimagehandler(FILE_UPLOAD_DIR.$file);
    	//$img->generateThumbnail(FILE_UPLOAD_DIR."slider/".$file, 256, 256);
    	//Utils::allResize($file,"slider", 256, 256);
    	//$status = copy(FILE_UPLOAD_DIR.$file, FILE_UPLOAD_DIR."slider/".$file);
    	$db->update(MYSQL_TABLE_PREFIX.$table, $dataArray,'banner_id="'.$db->escapeString($id).'"');
    	}   
    	//echopre($postedArray); exit;
    	
    }
    public static function  getInvitationSenderName($id) {
    	$db            = new Db();
    	$flist = $db->selectResult('invitation','invitation_sender_id',"invitation_id=$id");
    	$id2 = $flist[0]->invitation_sender_id;
        if($id2)
        {
         
    	$getAllFriendCountry = $db->selectResult('users','user_firstname,user_lastname'," user_status!='D' AND user_id=$id2");
    	$var=0;
    	foreach($getAllFriendCountry as $state) {
    		$result[$var]->value = ucwords($state->user_firstname ." " . $state->user_lastname);
    		$var++;
    	}
    	return $result[0]->value;
        }
        else
        {
            return '';
        }
    }
    
    // admin login as user
    public static function changeReconcileStatus($values) {
        $id     =  $values['id'];
    	$value  =  $values['value'];
        $userStatusUrl = BASE_URL.'cmshelper/ajaxReconcileStatusChange?id='.$id.'&value='.$value;
    	return $userStatusUrl;
       
    }
    
}

?>
