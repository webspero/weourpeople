<?php /* Smarty version Smarty-3.1.13, created on 2017-01-05 11:05:44
         compiled from "project\modules\default\view\script\index\searchbox.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:18605586e1a78a94881-20291373%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f51f6d9a2a52a636fd00c3e5f58975a0e012f78c' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\index\\searchbox.tpl.php',
      1 => 1463469285,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18605586e1a78a94881-20291373',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_586e1a78ae2a91_85603555',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_586e1a78ae2a91_85603555')) {function content_586e1a78ae2a91_85603555($_smarty_tpl) {?><form name="frmsearch" method="get" action="<?php echo PageContext::$response->search['action'];?>
">
    <div class="srch_list">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        if(sizeof(PageContext::$response->search['fields']) > 0) {
        echo '<select name="searchtype" class="right">';
            foreach(PageContext::$response->search['fields'] as $key=>$fields){
            echo '<option '.((PageContext::$request['searchtype']==$key)?'selected="selected"':'').' value="'.$key.'">'.$fields.'</option>';
            }
            echo ' </select>';
        }
        else
        echo '<input type="hidden" value="all" id= "searchtype" name="searchtype">';
        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>
    <div class="input-group searchblk_box">
        <div class="display_table wid100per">
            <div class="display_table_cell wid90per vlaign_bottom">
                <input name="searchtext" type="text" class="input-sm form-control height32 no-box-shadow" placeholder="Search" value="<?php echo PageContext::$response->searchtext;?>
">
            </div>
            <div class="display_table_cell wid10per vlaign_bottom">
                <button type="submit" class="btn btn-sm btn-primary border-radious-left"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </div>
</form>

    <!--<div class="input-group min_width252px"><input type="text" placeholder="Search" class="input-sm form-control"> 
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-sm btn-primary"> <i class="fa fa-search"></i></button> 
                        </span>
                        </div>--><?php }} ?>