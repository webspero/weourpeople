<?php /* Smarty version Smarty-3.1.13, created on 2017-01-05 11:06:29
         compiled from "project\modules\default\view\script\user\add_community.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:11292586e1aa58f0e05-24553035%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f6a2248355107713f91bebcf8e8ef274eb3da131' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\user\\add_community.tpl.php',
      1 => 1469436877,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11292586e1aa58f0e05-24553035',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'id' => 0,
    'businessname' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_586e1aa5ab6076_93049915',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_586e1aa5ab6076_93049915')) {function content_586e1aa5ab6076_93049915($_smarty_tpl) {?><div class="container">
    <div class="row"> 
        <div class="col-md-offset-3 col-lg-offset-3 col-sm-12 col-md-6 col-lg-6 ">
            <div class="whitebox marg40col pad25px_left pad25px_right">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Create A New Group</h3>
                    </div>
                </div>
                <div class="form-bottom signup_form">
                    <div class="<?php echo PageContext::$response->message['msgClass'];?>
"><?php echo PageContext::$response->message['msg'];?>
</div>
                    <form enctype="multipart/form-data" class="" action="" method="post" id="frmBusinessRegister" name="frmBusinessRegister" novalidate="novalidate">
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="text" value="" name="community_name" class="form-control" placeholder="Group Name">
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cnameadd" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="community_name" class="error"></label>   
                        </div>
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <textarea class="form-control" name="community_description" placeholder="Group Description"></textarea>
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                </div>
                            </div>
                            <label generated="true" for="community_description" class="error"></label>	                    
                        </div>
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <select class="form-control" value="" name="community_type" id="community_type" type="select"><option value="">Access Type</option><option value="PUBLIC">PUBLIC</option><option value="PRIVATE">PRIVATE</option></select>
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_ctypeadd" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="community_type" class="error"></label>  
                        </div>
                        <?php if (PageContext::$response->org_grp==1){?>
                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="radio" class="radiobusiness_type" name="radiobusiness_type" value="Personal">Personal
                                     <input type="radio" class="radiobusiness_type" name="radiobusiness_type" value="Business">Page
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_btypeadd" style="color:red">*</span>
                                </div>
                            </div>
                            <label generated="true" for="radiobusiness_type" class="error"></label>  
                        </div>
                        
                        <div class="form-group relative">
                             <div class="display_table wid100p">
                            <!--<?php echo PageContext::$response->business_id;?>
--> 
<!--                            <?php echo print_r(PageContext::$response->businessArray);?>
-->
                                <div class="display_table_cell wid98per">
                                    <select class="form-control" value="" name="business_type" id="business_type" type="select" style="display: none;">
                                        <?php  $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['id']->_loop = false;
 $_smarty_tpl->tpl_vars['businessname'] = new Smarty_Variable;
 $_from = PageContext::$response->businessArray; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['id']->key => $_smarty_tpl->tpl_vars['id']->value){
$_smarty_tpl->tpl_vars['id']->_loop = true;
 $_smarty_tpl->tpl_vars['businessname']->value = $_smarty_tpl->tpl_vars['id']->key;
?>

                                                <option value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" <?php ob_start();?><?php echo PageContext::$response->business_id;?>
<?php $_tmp1=ob_get_clean();?><?php if (PageContext::$response->getData['community_business_id']==$_smarty_tpl->tpl_vars['id']->value||$_tmp1==$_smarty_tpl->tpl_vars['id']->value){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['businessname']->value;?>
</option>
                                        <?php } ?>
                                            <!--, <option value="0">Personal</option>-->

                                    </select>
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                </div>
                            </div>
                                <label generated="true" for="business_type" class="error"></label>
                        </div>
                        <?php }?>

                        <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    <input type="file" class="left" onchange="upload(1140,258,'community_image_id')" id="community_image_id" placeholder="Upload File" size="10" value="" name="community_image_id"><span id="spn_asterisk_cimageadd" style="color:red;">*</span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span class="left" id="spn_support_imgadd"></span>
                                </div>
                            </div>
                            <div class="wid100p">(Supported format:jpg,jpeg,gif,png)</div>
                             <label generated="true" id="label_image_id" for="community_image_id" class="error"></label>
                        </div>
<!--                         <div class="form-group relative">
                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    Upload logo 
                                    <input type="file" class="left" placeholder="Upload File" size="10" value="" name="community_logo_id"><span id="spn_asterisk_clogoadd" style="color:red;">*</span>
                                <div class="clearfix"></div>
                                <span class="left" id="spn_support_logoadd">(Supported format:jpg,jpeg,gif,png)</span>
                                </div>
                                
                                
                               
                            </div>
                        </div>-->
                        <div class="form-group relative">
                            <input type="submit" class="btn btn-primary yellow_btn2" value="Save" name="btnSubmit" id="btnSubmit" ondblclick="this.disabled=true;">
                            <input type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary yellow_btn2" value="Cancel" name="btnCancel">
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--

<div class="content_left">
    <div class="content">
        <div class="<?php echo PageContext::$response->message['msgClass'];?>
"><?php echo PageContext::$response->message['msg'];?>
</div>
        <h3>Add BizCom</h3>
        <div class="register_form">
            <?php echo PageContext::$response->objForm;?>

        </div>
    </div>
    <div class="clear"></div>
</div>
-->
<?php }} ?>