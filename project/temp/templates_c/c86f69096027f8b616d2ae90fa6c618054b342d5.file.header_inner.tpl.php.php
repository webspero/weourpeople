<?php /* Smarty version Smarty-3.1.13, created on 2017-02-03 04:28:33
         compiled from "project\modules\default\view\script\user\header_inner.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:112525869da0f8784d3-49028982%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c86f69096027f8b616d2ae90fa6c618054b342d5' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\user\\header_inner.tpl.php',
      1 => 1486034305,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '112525869da0f8784d3-49028982',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5869da0ff0bfe2_66068851',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5869da0ff0bfe2_66068851')) {function content_5869da0ff0bfe2_66068851($_smarty_tpl) {?>
<header class="inrheader">
    <div class="container">
        <div class="row"> 
            <div class="col-sm-12 col-md-1 col-lg-1">
                <div class="logo">
                    <a href="<?php if (PageContext::$response->sess_user_id>0){?>
                       <?php echo PageContext::$response->baseUrl;?>
newsfeed/<?php echo PageContext::$response->sess_user_alias;?>

                       <?php }else{ ?>
                       <?php echo PageContext::$response->baseUrl;?>

                       <?php }?>">
                        <img src="<?php echo PageContext::$response->innerlogo;?>
"/>
                    </a>
                </div>
            </div>
            <div class="col-sm-12 col-md-5 col-lg-5">
                <div class=" header-search login_db_search">

                    <form role="form" class="form" name="serachprofile" id="search" action="<?php echo PageContext::$response->baseUrl;?>
index/searchprofile" method="GET">
                        <div class="input-group">
                            <input id="jSearchProfile"  name="searchtext" type="text" placeholder="Search Member, Group, Page" class="form-control login_db_searchbox">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default login_db_searchbtn"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>

                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 ">
                <ul class="hdr_indication_icon pull-right" id="menu">

                    <?php if (PageContext::$response->sess_user_id>0){?>
                    <li>
                        <div class="mail_list">
                            <?php if (PageContext::$response->messageCount>0){?>
                                <a href="<?php echo PageContext::$response->baseUrl;?>
inbox" class="notifi_no notifi_no_msg" title="Mails"><?php echo PageContext::$response->messageCount;?>
</a>
                            <?php }?>
                            <a href="<?php echo PageContext::$response->baseUrl;?>
inbox" class="mail_list_icon" title="Mails"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li>
                        <div class="notifi_list">
                            <?php if (PageContext::$response->NumFriendRequest>0){?>
                                <a href="<?php echo PageContext::$response->baseUrl;?>
pending-request" class="notifi_no notifi_no_friend" title="New Friend Request"><?php echo PageContext::$response->NumFriendRequest;?>
</a>
                              <?php }?>
                            <a href="<?php echo PageContext::$response->baseUrl;?>
pending-request" class="notifi_list_icon" title="New Friend Request"><i class="fa fa-user-plus" aria-hidden="true"></i></a>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li>
                        <div class="notifi_list">
                            <?php if (PageContext::$response->NumPrivateRequest>0){?>
                                <a href="<?php echo PageContext::$response->baseUrl;?>
pending-members" class="notifi_no notifi_no_request" title="New Pending Joinee"><?php echo PageContext::$response->NumPrivateRequest;?>
</a>
                              <?php }?>
                            <a href="<?php echo PageContext::$response->baseUrl;?>
pending-members" class="notifi_pending_list_icon" title="New Pending Joinee"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li class="nomarg-nav <?php if (PageContext::$response->activeSubMenu=='logout'){?>active<?php }else{ ?> <?php }?> right_name">
                        <ul class="hdrtopnav nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle right" data-toggle="dropdown">

                                    <div class="userdisplayname">
                                        <?php if (PageContext::$response->sess_user_image!=''){?>
                                        <img class="my_pic" src="<?php echo PageContext::$response->userImagePath;?>
<?php echo PageContext::$response->sess_user_image;?>
">
                                        <?php }elseif(PageContext::$response->sess_Image!=''){?>
                                        <img class="my_pic" src="<?php echo PageContext::$response->userImagePath;?>
<?php echo PageContext::$response->sess_Image;?>
">
                                        <?php }else{ ?>
                                        <img src="<?php echo PageContext::$response->defaultImage;?>
user_thumb.png">
                                        <?php }?>
                                        <?php echo PageContext::$response->sess_user_name;?>

                                        <i class="fa fa-sort-desc" aria-hidden="true"></i>

                                    </div>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="<?php if (PageContext::$response->activeSubMenu=='profile'){?>active<?php }else{ ?> <?php }?>">
                                        <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo PageContext::$response->sess_user_alias;?>
">My Timeline</a>
                                    </li>

                                    <li>
                                        <a href="<?php echo PageContext::$response->baseUrl;?>
my-groups">My Groups</a>
                                    </li>

                                    <li>
                                        <a href="<?php echo PageContext::$response->baseUrl;?>
pages">My Pages</a>
                                    </li>

                                    <li>
                                        <a href="<?php echo PageContext::$response->baseUrl;?>
changepassword">Settings</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo PageContext::$response->baseUrl;?>
logout">Log out</a>
                                    </li>
                                    <div class="clearfix"></div>
                                </ul>
                                <div class="clearfix"></div>
                            </li>
                        </ul>

                    </li> 


                    <?php }?>
                </ul>
                <div class="clearfix"></div>

            </div>
        </div>
</header><?php }} ?>