<?php /* Smarty version Smarty-3.1.13, created on 2017-01-02 07:13:11
         compiled from "project\modules\default\view\script\user\community.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:234145869ef77689e45-10253128%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '117466b139508087c85aff65d5ec246f3a4fd9ec' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\user\\community.tpl.php',
      1 => 1482822663,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '234145869ef77689e45-10253128',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'friendslist' => 0,
    'gallerylist' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5869ef77e56195_28498730',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5869ef77e56195_28498730')) {function content_5869ef77e56195_28498730($_smarty_tpl) {?><script>
    $(document).ready(function(){
         magnificPopupGroupFn();
    });
</script>
<div class="joinMessage" id="joinMessage"></div>
<div class="jmessage" id="jmessage"></div>
<div class="container">
    <div class="row">
        <?php if (PageContext::$response->result->community_name!=''){?>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="timelineheader">
                <div class="timelineheader_right">
                    <div class="cover-wrapper"
                         style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
<?php echo PageContext::$response->community_image;?>
');">
                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <div class="timelineheader_left">
                                <div class="timeline_left_pic_nosec"></div>
                                <h3> <?php echo PageContext::$response->result->community_name;?>
</h3>
                            </div>
                        </div>
                        <div class="menufloattop">
                            <?php if (PageContext::$response->sess_user_id!=PageContext::$response->result->community_created_by){?>
                            <?php if (PageContext::$response->sess_user_id!=PageContext::$response->result->community_created_by&&PageContext::$response->sess_user_id!=PageContext::$response->checkmembers->cmember_id){?>
                            <a href="#"
                               onclick="<?php if (PageContext::$response->result->community_type=='PRIVATE'){?>joinMail(<?php echo PageContext::$response->result->community_id;?>
)<?php }else{ ?>joinCommunity(<?php echo PageContext::$response->result->community_id;?>
,'join')<?php }?>">
                                <input type="button" name="invite" value="Join" class="fontupper" title="invite-block">
                            </a>
                            <?php }?>
                            <?php if (PageContext::$response->checkmembers!=''){?>
                            <a href="#"
                               onclick="joinCommunity(<?php echo PageContext::$response->result->community_id;?>
,'decline')">
                                <input type="button" name="invite" value="Leave Group" class="fontupper"
                                       title="invite-block">
                            </a>
                            <?php }?>
                            <?php }?>
                        </div>

                        <?php if (PageContext::$response->sess_user_id==PageContext::$response->result->community_created_by){?>
                        <div class="timelineimage">
                            <form name="frmtimelineimageupload">
                                <i class="fa fa-camera" id="jGrpCam"></i><span class="name"></span>
                                <input type="file" name="jgrptimelinepic" id="jgrptimelinepic" style="display:none;">
                                <input type="hidden" value="<?php echo PageContext::$response->result->community_id;?>
" id="group_id" name="group_id">
                            </form>
                        </div>
                        <?php }?>

                    </div>
                    <div class="timeline_tabmenu">
                        <ul id="myTab" class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#" class="jtab general_tab" title="tab-Feed" data-toggle="tab">About</a>
                            </li>
                            <?php if (PageContext::$response->sess_user_id>0){?>
                            <?php if (PageContext::$response->sess_user_id==PageContext::$response->result->community_created_by||PageContext::$response->sess_user_id==PageContext::$response->checkmembers->cmember_id){?>
                            <li class=""><a href="#" class="jtab discussion_tab" title="tab-Feed" data-toggle="tab"
                                            alias="<?php echo PageContext::$response->result->community_alias;?>
">Discussions</a>
                            </li>

                            <li class=""><a href="#" alias="<?php echo PageContext::$response->result->community_id;?>
"
                                            class="jtab photos_tab" title="tab-Feed" data-toggle="tab">Photos</a>
                            </li>
                            <li><a href="#" title="tab-Friends" data-toggle="tab" class="jtab jShowMembers"
                                   alias="<?php echo PageContext::$response->alias;?>
" uid="<?php echo PageContext::$response->sess_user_id;?>
">Members(<?php echo PageContext::$response->memberCount;?>
)</a>
                            </li>
                            <?php }?>
                            <?php }?>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <?php }else{ ?>
        <br>
          <div class='whitebox'>No Data Found</div>
        <?php }?>
    </div>
</div>
<div class='clearfix'></div>
<div class='container'>
    <div class='row'>
   <div class="col-sm-4 col-md-5 col-lg-3">   
    <?php if (count(PageContext::$response->friend_display_list)>0){?>            
            <div class="whitebox marg20top">
                <div class="hdsec">
                    <h3><i class="fa fa-user" aria-hidden="true"></i> Members <a  href="#" id="jMoreGroupphotos" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist">
                    <?php  $_smarty_tpl->tpl_vars['friendslist'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['friendslist']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->friend_display_list; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['friendslist']->key => $_smarty_tpl->tpl_vars['friendslist']->value){
$_smarty_tpl->tpl_vars['friendslist']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['friendslist']->key;
?>
                      
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <a class="friendphotobox_blk" href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['friendslist']->value->user_alias;?>
">
                                <?php if ($_smarty_tpl->tpl_vars['friendslist']->value->file_path){?>
                                <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['friendslist']->value->file_path;?>
');">
                                </div>
                                <?php }else{ ?>
                                <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
medium/member_noimg.jpg');">
                                </div>
                                <?php }?>
                                <h4><?php echo ucfirst($_smarty_tpl->tpl_vars['friendslist']->value->user_firstname);?>
 <?php echo ucfirst($_smarty_tpl->tpl_vars['friendslist']->value->user_lastname);?>
</h4>
                            </a>
                        </li>
                   <?php } ?>
                </ul>
                </div>  
   <?php }?>
    <?php if (count(PageContext::$response->gallery_display_list)>0){?>   
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-file-image-o" aria-hidden="true"></i> Photos <a href="#" id="jMoreGroupGallery" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist portfolio">
                    <?php  $_smarty_tpl->tpl_vars['gallerylist'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gallerylist']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->gallery_display_list; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gallerylist']->key => $_smarty_tpl->tpl_vars['gallerylist']->value){
$_smarty_tpl->tpl_vars['gallerylist']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['gallerylist']->key;
?>
                      
                     
<!--                    <li class="display_table_cell wid32per">
                        <a class="friendphotobox_blk" href="#">
                            <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->news_feed_image_name;?>
');">-->
                                
<!--                            <ul class="portfolio" class="clearfix">-->
                                    <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4 vert_align_top"><a class="friendphotobox_blk" href="<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->community_announcement_image_path;?>
" title="<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->community_announcement_content;?>
"><img src="<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->community_announcement_image_path;?>
" alt=""></a></li>
<!--                            </ul>-->
                                
<!--                            </div>
                        </a>
                    </li>-->
                    <?php } ?> 
                </ul>
                <!--<div class="sidead" style="background-image: url('<?php echo PageContext::$response->ImagePath;?>
sidead.jpg');"> </div>-->
            </div>  
    <?php }?>   
   
</div>
        
        <div class="col-sm-8 col-md-7 col-lg-9 marg20col jDivMain">
            <input type="hidden" id="community_alias" value="<?php echo PageContext::$response->result->community_alias;?>
">
            <input type="hidden" id="community_id" value="<?php echo PageContext::$response->communityData->community_id;?>
">
            <div id="jDivDisplayBusiness" class="tab-content">
                <div class="wid100p">
                </div>
                 <?php if (PageContext::$response->result->community_name!=''){?>
                <div id="myTabContent" class="tab-content whitebox">
                    <div class="tab-pane fade active in" id="tab-Feed">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <div class="wel_community">
                                    <div id="sticky-anchor"></div>
                                    <div id="sticky">
                                       
                                        <div class="mediapost marg0top profile-bnr welcome">
                                            <div class="welcome_head">
                                                <div class="groupwelcome welcome_left">
                                                    <div class="wid100per pad5top">
                                                        <h4>Welcome to <span><?php echo PageContext::$response->result->community_name;?>
</span>
                                                        </h4>
                                                    </div>
                                                    <div class="wid100per pad5top">
                                                        <div class="display_table wid100per">
                                                            <?php if (PageContext::$response->org_grp==1){?>
                                                            <div class="display_table_row">
                                                                <div class="display_table_cell wid30per">
                                                                    <h5>Page</h5>
                                                                </div>
                                                                <div class="display_table_cell wid70per">
                                                                    <h5>
                                                                        <a <?php if (PageContext::$response->result->business_alias!=''){?> href="<?php echo PageContext::$response->baseUrl;?>
page/<?php echo PageContext::$response->result->business_alias;?>
" <?php }?>><span><?php echo PageContext::$response->result->business_name;?>
</span></a>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <?php }?>
                                                            <div class="display_table_row">
                                                                <div class="display_table_cell wid30per">
                                                                    <h5>Owned By</h5>
                                                                </div>
                                                                <div class="display_table_cell wid70per">
                                                                    <h5>
                                                                        <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo PageContext::$response->result->user_alias;?>
"><span><?php echo PageContext::$response->result->user_firstname;?>
 <?php echo PageContext::$response->result->user_lastname;?>
</span></a>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="display_table_row">
                                                                <div class="display_table_cell wid30per">
                                                                    <h5>Type</h5>
                                                                </div>
                                                                <div class="display_table_cell wid70per">
                                                                    <h5><span><?php if (PageContext::$response->result->community_type=='PRIVATE'){?> Private <?php }else{ ?> Public <?php }?></span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="display_table_row">
                                                                <div class="display_table_cell wid30per">
                                                                    <h5>Description</h5>
                                                                </div>
                                                                <div class="display_table_cell wid70per">
                                                                    <h5 class="welcomerighttext"><span><?php if (PageContext::$response->result->community_description!=''){?> <?php echo substr(stripslashes(PageContext::$response->result->community_description),0,200);?>
....<?php }?></span>
                                                                    </h5>
                                                                </div>
                                                            </div>

                                                            <input type="hidden"
                                                                   value="<?php echo PageContext::$response->sess_user_id;?>
"
                                                                   class="login_status" name="" id="login_status">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="welcome_right">
                                                    <div class="wid100per pad5top">
                                                        <?php if (PageContext::$response->result->community_status=='A'){?>
                                                        <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PageContext::renderRegisteredPostActions('rating');<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                                                        <?php }?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <?php if (PageContext::$response->sess_user_id==PageContext::$response->result->community_created_by){?>    
                                    <div class="right">
                                    <a class="edititem left marg5right" href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
user/edit_group/<?php echo PageContext::$response->result->community_id;?>
"><i class="fa fa-pencil"></i></a>        
                                    <a  href="<?php echo PageContext::$response->baseUrl;?>
index/delete_community/<?php echo PageContext::$response->result->community_id;?>
" onclick="return deleteConfirm();"
                                               class="deleteitem left"><i class="fa fa-trash"></i></a>
                                    </div>
                                <?php }?>
                            </div>
                            <div class="clearfix"></div>
                            
                        </div>
                    </div>
                </div>
                     <?php }?>
            </div>
        </div>
     <?php }} ?>