<?php /* Smarty version Smarty-3.1.13, created on 2017-02-03 04:29:53
         compiled from "project\modules\default\view\script\index\searchprofile.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:126565893f931022719-61056549%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7492223177ce368f56032f6f0c3fac737887942a' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\index\\searchprofile.tpl.php',
      1 => 1484114539,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '126565893f931022719-61056549',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5893f93132fbc0_77114877',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5893f93132fbc0_77114877')) {function content_5893f93132fbc0_77114877($_smarty_tpl) {?><div class="container">
    <div class="row"> 
    <div class="whitebox marg15col">
        <div class="col-sm-12 col-md-12 col-lg-12">

<?php if (count(PageContext::$response->usersList)>0){?> 
            <h3>Find Friends</h3>
            <?php }?>
        </div>
        <section class="bizcomlisting listin">
            
            <?php if (count(PageContext::$response->usersList)>0){?> 
            <?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->usersList; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value){
$_smarty_tpl->tpl_vars['user']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['user']->key;
?>

            <div class="col-sm-4 col-md-4 col-lg-4">
            <div class="mediapost">
                <div class="picpost_left pull-left">
                    <span class="picpost_left_pic marg10top">
                        <img src="<?php if ($_smarty_tpl->tpl_vars['user']->value->file_path==''){?><?php echo PageContext::$response->userImagePath;?>
member_noimg.jpg<?php }else{ ?><?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['user']->value->file_path;?>
<?php }?>">
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['user']->value->user_alias;?>
"><?php echo $_smarty_tpl->tpl_vars['user']->value->user_firstname;?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->user_lastname;?>
</a></h4>
                    <p><?php echo $_smarty_tpl->tpl_vars['user']->value->user_friends_count;?>
 Friends </p>
                    <p>
                        <?php if (PageContext::$response->sess_user_id>0&&$_smarty_tpl->tpl_vars['user']->value->user_id!=PageContext::$response->sess_user_id){?>
                        <?php if (in_array($_smarty_tpl->tpl_vars['user']->value->user_id,PageContext::$response->myfriends)){?> 
                        <a uid="<?php echo $_smarty_tpl->tpl_vars['user']->value->user_id;?>
" href="#"><span class="accept"><i class="fa fa-check"></i> Friends</span></a>
                        <?php }elseif(in_array($_smarty_tpl->tpl_vars['user']->value->user_id,PageContext::$response->myinvitedfriends)){?>
                        <a uid="<?php echo $_smarty_tpl->tpl_vars['user']->value->user_id;?>
" href="#"><span class="accept" id="jFriend_<?php echo $_smarty_tpl->tpl_vars['user']->value->user_id;?>
"><i class="fa fa-check"></i> Friend request sent</span></a>
                        <?php }elseif(in_array($_smarty_tpl->tpl_vars['user']->value->user_id,PageContext::$response->myPendingRequests)){?>
                        <a uid="<?php echo $_smarty_tpl->tpl_vars['user']->value->user_id;?>
" href="#" class="jaddfriend1" inviteid="<?php echo $_smarty_tpl->tpl_vars['user']->value->inviteId;?>
"><span class="accept" id="jFriend_<?php echo $_smarty_tpl->tpl_vars['user']->value->user_id;?>
"><b>+</b> Accept Friend</span></a>
                        <?php }else{ ?>
                        <a uid="<?php echo $_smarty_tpl->tpl_vars['user']->value->user_id;?>
" href="#" class="jaddfriend" inviteid="<?php echo $_smarty_tpl->tpl_vars['user']->value->inviteId;?>
"><span class="accept" id="jFriend_<?php echo $_smarty_tpl->tpl_vars['user']->value->user_id;?>
"><b>+</b> Add Friend</span></a>
                        <?php }?>
                        <?php }?>
                    </p>
                    <div class="loader loaderposition1" id="loading_<?php echo $_smarty_tpl->tpl_vars['user']->value->user_id;?>
">
                      <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif" />
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            </div>
            <?php } ?>
            <?php }else{ ?>
                <div class="rownoborder">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    No records found
                </div>
                </div>    
             <?php }?>

            
            <!--<div class="row rownoborder">
                <div class="colside1">
                    <div class="colside1_pic">
                        <img src="images/no_frnd_image.jpg">
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="colmiddle">
                    <h6><a href="#">Stuart brosnen</a></h6>
                    <p>
                        3 Friends
                    </p>
                </div>
                <div class="colside3">
                    <a class="friends_btn fright" href="#" style="float:left; ">
                        <span class="addfriend_icon"></span>
                        Add Friend
                    </a>

                </div>
            </div>-->
            
        </section>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <?php echo PageContext::$response->pagination;?>

            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        </div>
    </div>

    <div class="clear"></div>

</div>
<?php }} ?>