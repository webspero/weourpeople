<?php /* Smarty version Smarty-3.1.13, created on 2017-01-05 11:06:51
         compiled from "project\modules\default\view\script\user\my_groups.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:1990586e1abb94fa41-36515523%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'baf115aa4fc57888f447f99883cf6e8502f2fa40' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\user\\my_groups.tpl.php',
      1 => 1469591014,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1990586e1abb94fa41-36515523',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lead' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_586e1abbe87a77_29285919',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_586e1abbe87a77_29285919')) {function content_586e1abbe87a77_29285919($_smarty_tpl) {?><div class="container">
    <section class="whitebox marg20col">
    <div class="row"> 
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h3>Manage an existing Group <span class="round-search"><?php echo PageContext::$response->totalrecords;?>
</span> <?php if (PageContext::$response->sess_user_id!=0){?> <span class="addbusiness"><a href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
add-group">Add Group</a></span> <?php }?></h3>
        </div>
        <div class="searchbar">

            <div class="col-sm-12 col-md-12 col-lg-12">

                <div class="businesslist_search_blk">
                    <div class="row"> 
                        <div class="col-sm-6 col-md-8 col-lg-8">
                            <input type="hidden" value="<?php echo PageContext::$response->sess_user_id;?>
" class="login_status" name="" id="login_status">
                            Sort by : A-Z <a href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl; <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
my-groups?sort=community_name&sortby=<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->order; <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
&searchtext=<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->searchtext; <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
&searchtype=<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->searchtype; <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
&page=<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->page; <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
"><?php echo PageContext::$response->community_name_sortorder;?>
</a>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="right"><?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PageContext::renderRegisteredPostActions('searchbox');<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
                        </div>

                    </div>
                   
                </div>
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PageContext::renderRegisteredPostActions('messagebox');<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            </div>
        </div>
        <div class="clearfix"></div>
        <div class="grplisting">
            <?php if (PageContext::$response->totalrecords>0){?>
            <?php if (isset($_smarty_tpl->tpl_vars["i"])) {$_smarty_tpl->tpl_vars["i"] = clone $_smarty_tpl->tpl_vars["i"];
$_smarty_tpl->tpl_vars["i"]->value = PageContext::$response->slno; $_smarty_tpl->tpl_vars["i"]->nocache = null; $_smarty_tpl->tpl_vars["i"]->scope = 0;
} else $_smarty_tpl->tpl_vars["i"] = new Smarty_variable(PageContext::$response->slno, null, 0);?>
            <?php  $_smarty_tpl->tpl_vars['lead'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lead']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->leads; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lead']->key => $_smarty_tpl->tpl_vars['lead']->value){
$_smarty_tpl->tpl_vars['lead']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['lead']->key;
?>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="mediapost">
                    <div class="picpost_left pull-left">
                        <span class="picpost_left_pic">

                            <a href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
group/<?php echo $_smarty_tpl->tpl_vars['lead']->value->community_alias;?>
"><img class="business_profile_desc_colside_pic" alt="<?php echo $_smarty_tpl->tpl_vars['lead']->value->file_orig_name;?>
" src="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->userImagePath;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php if ($_smarty_tpl->tpl_vars['lead']->value->file_path!=''){?>medium/<?php echo $_smarty_tpl->tpl_vars['lead']->value->file_path;?>
<?php }elseif($_smarty_tpl->tpl_vars['lead']->value->file_path==''){?>default/no_image_bbf.jpg<?php }?>"> </a>
                        </span>
                    </div>

                    <div class="media-body">
                        <h4 class="media-heading"><span><a href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
group/<?php echo $_smarty_tpl->tpl_vars['lead']->value->community_alias;?>
"><?php echo $_smarty_tpl->tpl_vars['lead']->value->community_name;?>
<?php if (PageContext::$response->org_grp==1){?>(<?php echo $_smarty_tpl->tpl_vars['lead']->value->business_name;?>
)<?php }?></a></span></h4>
                        <?php if ($_smarty_tpl->tpl_vars['lead']->value->community_status=='I'){?>   
                        <span class="status clserror" >Pending Approval</span> 
                        <div class="right">
                                        <?php if (PageContext::$response->sess_user_id==$_smarty_tpl->tpl_vars['lead']->value->community_created_by){?>    
                                        <a href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
edit-group/<?php echo $_smarty_tpl->tpl_vars['lead']->value->community_id;?>
" class="edititem left marg5right"><i class="fa fa-pencil"></i></a>
                                        <a href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
index/delete_community/<?php echo $_smarty_tpl->tpl_vars['lead']->value->community_id;?>
"  onclick="return deleteConfirm();" class="deleteitem left"><i class="fa fa-trash"></i></a> 
                                        <?php }?>
                                    </div>
                        <?php }else{ ?>
                        <div class="display_table">
                            <div class="display_table_cell wid60per">
                                <input type="hidden" id="created_by" value="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo $lead->community_created_by;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
">
                                <div id="starring">
                                    <div class="rateit" id="rateit_<?php echo $_smarty_tpl->tpl_vars['lead']->value->ratetype_id;?>
" data-rateit-value="<?php echo $_smarty_tpl->tpl_vars['lead']->value->ratevalue;?>
" data-rateit-ispreset="true" data-rateit-id="<?php echo $_smarty_tpl->tpl_vars['lead']->value->rateuser_id;?>
" data-rateit-type="<?php echo $_smarty_tpl->tpl_vars['lead']->value->ratetype;?>
" data-rateit-typeid="<?php echo $_smarty_tpl->tpl_vars['lead']->value->ratetype_id;?>
" data-rateit-flag="<?php echo $_smarty_tpl->tpl_vars['lead']->value->rateflag;?>
" data-rateit-step=1 data-rateit-resetable="false">
                                    </div>
                                    <div class="right">
                                        <?php if (PageContext::$response->sess_user_id==$_smarty_tpl->tpl_vars['lead']->value->community_created_by){?>    
                                        <a href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
edit-group/<?php echo $_smarty_tpl->tpl_vars['lead']->value->community_id;?>
" class="edititem left marg5right"><i class="fa fa-pencil"></i></a>
                                        <a href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
index/delete_community/<?php echo $_smarty_tpl->tpl_vars['lead']->value->community_id;?>
"  onclick="return deleteConfirm();" class="deleteitem left"><i class="fa fa-trash"></i></a> 
                                        <?php }else{ ?>
                                         <a href="#" style="color: #0000FF;" onclick="DisjoinCommunity(<?php echo $_smarty_tpl->tpl_vars['lead']->value->community_id;?>
,'decline')">
                        <input type="button" name="invite" value="Unsubscribe" class="btn yellow_btn fontupper btt_full_width" title="invite-block">
                    </a>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <?php }?>
                        <p class="paragraph-height">
                            <?php echo substr($_smarty_tpl->tpl_vars['lead']->value->community_description,0,200);?>
....
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php if (isset($_smarty_tpl->tpl_vars["i"])) {$_smarty_tpl->tpl_vars["i"] = clone $_smarty_tpl->tpl_vars["i"];
$_smarty_tpl->tpl_vars["i"]->value = $_smarty_tpl->tpl_vars['i']->value+1; $_smarty_tpl->tpl_vars["i"]->nocache = null; $_smarty_tpl->tpl_vars["i"]->scope = 0;
} else $_smarty_tpl->tpl_vars["i"] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
            <?php } ?> 	
            <?php }else{ ?>
            <br>
            <div class="col-sm-12 col-md-12 col-lg-12"><?php if (PageContext::$response->searchtype==''){?>No groups added/joined yet.<?php }else{ ?>No matches found.<?php }?>.</div>
            <?php }?> 
            <div class="clear"></div>
            <?php if (PageContext::$response->totalrecords>PageContext::$response->itemperpage){?>
            <div class="col-sm-12 col-md-12 col-lg-12"><?php echo PageContext::$response->pagination;?>
 </div>
            <?php }?>

        </div>
    </div>
    </section>
    <input type="hidden" id="searchtext" name="searchtext" value="<?php echo PageContext::$response->searchtext;?>
">
    <input type="hidden" id="searchtype" name="searchtype" value="<?php echo PageContext::$response->searchtype;?>
">
    <!--<input type="hidden" id="page" name="page" value="<?php echo PageContext::$response->page;?>
">-->
    <input type="hidden" id="orderfield" name="orderfield" value="<?php echo PageContext::$response->orderfield;?>
">
    <input type="hidden" id="orderby" name="orderby" value="<?php echo PageContext::$response->orderby;?>
">

</div>

<script type ="text/javascript">
    $(document).ready(function() {
   // alert("jdfgj");
        var page = 1;  
        var searchtext = $("#searchtext").val();
        var searchtype = $("#searchtype").val();
        var orderfield = $("#orderfield").val();
        var orderby = $("#orderby").val();
       	var _throttleTimer = null;
	var _throttleDelay = 100;
            $(window ).scroll(function() {
              console.log('TotalPage'+TOT_PAGE+'Page'+page);
	    clearTimeout(_throttleTimer);
	    _throttleTimer = setTimeout(function () {
	    if($(window).scrollTop() + $(window).height() > $(document).height()-100) {
	        if(page <= TOT_PAGE){
                   // alert(page);
                    page++;
                    $.blockUI({ message: '' }); 
                     $.ajax({
                        type    : 'Post',
                        url     : mainUrl+'user/my_groupsajax/'+page+'/'+orderfield+'/'+orderby+'/'+searchtext+'/'+searchtype,
                        cache   : false,
                        success :function(data){
                            $('.grplisting').append(data);
                            $('.listitem').fadeIn(3000);
                            $('.listitem').hide();
                        }
                    }) 
    	        }
	    }
	    },_throttleDelay);
	    });
    });

</script>









<?php }} ?>