<?php /* Smarty version Smarty-3.1.13, created on 2017-02-03 04:28:33
         compiled from "project\modules\default\view\script\index\home_footer.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:260025869da1017b5c4-67600894%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5fe8be3155ef835c6a79332f703d037825895f0b' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\index\\home_footer.tpl.php',
      1 => 1485173439,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '260025869da1017b5c4-67600894',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5869da104cb116_30342778',
  'variables' => 
  array (
    'friendEntity' => 0,
    'user_image' => 0,
    'chat_status' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5869da104cb116_30342778')) {function content_5869da104cb116_30342778($_smarty_tpl) {?><?php if ($_SESSION['default_user_id']){?>
<?php if (PageContext::$response->chat_display==1){?>
<?php if (PageContext::$response->sess_user_status=='A'){?>
<a class="chatbutton" href="javascript:void(0)" id="chatIdentifier"><i class="fa fa-comments-o" aria-hidden="true"></i> Chat Now</a>
<div class="chatlist" id="onlineUsersList" style="display:none">
    <div class="chatlisthead">
        <h3>Chat List <a href="javascript:void(0)" class="right" id="chatMinimize"><i class="fa fa-minus" aria-hidden="true"></i></a></h3>
    </div>
    <ul id="chatListContainer">        
        <?php if (empty(PageContext::$response->leftMenuUsersList->records)){?>            
            <li>
                <div class="display_table">
                    No Friends Added Yet!
                </div>
            </li>
        <?php }else{ ?>
            <?php  $_smarty_tpl->tpl_vars['friendEntity'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['friendEntity']->_loop = false;
 $_from = PageContext::$response->leftMenuUsersList->records; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['friendEntity']->key => $_smarty_tpl->tpl_vars['friendEntity']->value){
$_smarty_tpl->tpl_vars['friendEntity']->_loop = true;
?>
            <?php if ($_smarty_tpl->tpl_vars['friendEntity']->value->file_path==''){?>
                <?php if (isset($_smarty_tpl->tpl_vars["user_image"])) {$_smarty_tpl->tpl_vars["user_image"] = clone $_smarty_tpl->tpl_vars["user_image"];
$_smarty_tpl->tpl_vars["user_image"]->value = "member_noimg.jpg"; $_smarty_tpl->tpl_vars["user_image"]->nocache = null; $_smarty_tpl->tpl_vars["user_image"]->scope = 0;
} else $_smarty_tpl->tpl_vars["user_image"] = new Smarty_variable("member_noimg.jpg", null, 0);?>
            <?php }else{ ?>
                <?php if (isset($_smarty_tpl->tpl_vars["user_image"])) {$_smarty_tpl->tpl_vars["user_image"] = clone $_smarty_tpl->tpl_vars["user_image"];
$_smarty_tpl->tpl_vars["user_image"]->value = $_smarty_tpl->tpl_vars['friendEntity']->value->file_path; $_smarty_tpl->tpl_vars["user_image"]->nocache = null; $_smarty_tpl->tpl_vars["user_image"]->scope = 0;
} else $_smarty_tpl->tpl_vars["user_image"] = new Smarty_variable($_smarty_tpl->tpl_vars['friendEntity']->value->file_path, null, 0);?>
            <?php }?>
            
            <?php if ($_smarty_tpl->tpl_vars['friendEntity']->value->user_online_status=='Idle'){?>
                <?php if (isset($_smarty_tpl->tpl_vars["chat_status"])) {$_smarty_tpl->tpl_vars["chat_status"] = clone $_smarty_tpl->tpl_vars["chat_status"];
$_smarty_tpl->tpl_vars["chat_status"]->value = "la"; $_smarty_tpl->tpl_vars["chat_status"]->nocache = null; $_smarty_tpl->tpl_vars["chat_status"]->scope = 0;
} else $_smarty_tpl->tpl_vars["chat_status"] = new Smarty_variable("la", null, 0);?>
            <?php }elseif($_smarty_tpl->tpl_vars['friendEntity']->value->user_online_status=='Offline'){?>
                <?php if (isset($_smarty_tpl->tpl_vars["chat_status"])) {$_smarty_tpl->tpl_vars["chat_status"] = clone $_smarty_tpl->tpl_vars["chat_status"];
$_smarty_tpl->tpl_vars["chat_status"]->value = "ma"; $_smarty_tpl->tpl_vars["chat_status"]->nocache = null; $_smarty_tpl->tpl_vars["chat_status"]->scope = 0;
} else $_smarty_tpl->tpl_vars["chat_status"] = new Smarty_variable("ma", null, 0);?>
            <?php }else{ ?>
                <?php if (isset($_smarty_tpl->tpl_vars["chat_status"])) {$_smarty_tpl->tpl_vars["chat_status"] = clone $_smarty_tpl->tpl_vars["chat_status"];
$_smarty_tpl->tpl_vars["chat_status"]->value = ''; $_smarty_tpl->tpl_vars["chat_status"]->nocache = null; $_smarty_tpl->tpl_vars["chat_status"]->scope = 0;
} else $_smarty_tpl->tpl_vars["chat_status"] = new Smarty_variable('', null, 0);?>
            <?php }?>
                <li>
                    <div class="display_table">
                    <div id="profilepic" class="profilepicsec">
                        <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['friendEntity']->value->user_alias;?>
">
                            <span class="mediapost_pic" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['user_image']->value;?>
');">
                            </span>
                        </a>
                    </div>
                    <div class="profilenamesec">
                        <h3 class="profile_name">
                            <a href="javascript:void(0)" onclick="chatWith('<?php echo $_smarty_tpl->tpl_vars['friendEntity']->value->user_alias;?>
','<?php echo $_smarty_tpl->tpl_vars['friendEntity']->value->user_firstname;?>
');" id="jProfile1" class="jtab " alias="<?php echo $_smarty_tpl->tpl_vars['friendEntity']->value->user_firstname;?>
">
                                <?php echo $_smarty_tpl->tpl_vars['friendEntity']->value->user_firstname;?>

                            </a>
                        </h3>
                    </div>
                    <div class="picstatus">
                           <i class="fa fa-circle <?php echo $_smarty_tpl->tpl_vars['chat_status']->value;?>
" aria-hidden="true"></i>
                    </div>
                    </div>
                </li>
               <?php } ?>
               <?php }?>
    </ul>
</div>
<?php }?>
<?php }?>
<?php }?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-8">
                <ul class="ftr_links">
                    <li><a href="<?php echo PageContext::$response->baseUrl;?>
">Home</a></li>
                    <li><a href="<?php echo PageContext::$response->baseUrl;?>
aboutus">About Us</a></li>
                    <li><a href="<?php echo PageContext::$response->baseUrl;?>
contact">Contact Us</a></li>
                    <li><a href="<?php echo PageContext::$response->baseUrl;?>
faq">FAQ</a></li>
                    <li><a href="<?php echo PageContext::$response->baseUrl;?>
feedback">Feedback</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="copyright">
                    © Copyright <?php echo PageContext::$response->curYear;?>
. Socialware. All rights reserved
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="clear"></div>
<?php }} ?>