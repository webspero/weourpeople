<?php /* Smarty version Smarty-3.1.13, created on 2017-01-02 11:40:45
         compiled from "project\modules\default\view\script\user\business1.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:205265869ee32969ea5-72766682%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8a1f30cc227f2ecdc266e9cdf37c2ca2c6037a2a' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\user\\business1.tpl.php',
      1 => 1483347181,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '205265869ee32969ea5-72766682',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5869ee33680003_56578525',
  'variables' => 
  array (
    'friendslist' => 0,
    'gallerylist' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5869ee33680003_56578525')) {function content_5869ee33680003_56578525($_smarty_tpl) {?><div class="container">
    <div class="row">
        <?php if (PageContext::$response->resultSet->business_name!=''){?>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="timelineheader">
                <div class="timelineheader_right">
                    <div class="cover-wrapper"
                         style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
<?php echo PageContext::$response->resultSet->business_timeline_image_name;?>
');">
                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <div class="timelineheader_left profilepic_business1blk">
                                <input type="hidden" name="business_id" id="business_id"
                                       value="<?php echo PageContext::$response->business_id;?>
">
                                 <input type="hidden" name="user_id" id="user_id"
                                       value="<?php echo PageContext::$response->sess_user_id;?>
">
                                <!--<?php if (PageContext::$response->user_image!=''){?>
                                     <div class="timeline_left_pic" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
<?php echo PageContext::$response->user_image;?>
');"></div>
                                     <?php }else{ ?>-->
                                <div class="timeline_left_pic"
                                     style="background-image: url('<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->userImagePath;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
medium/<?php echo PageContext::$response->resultSet->file_path;?>
');">
                                </div>

                                <!--<?php }?>-->
                                <h3><a href="#" target="_blank"><?php echo PageContext::$response->user_name;?>
</a></h3>
                                <?php if (PageContext::$response->resultSet->business_created_by==PageContext::$response->sess_user_id){?>
                                <form name="orgtimelineimageupload" class="upload">
                                    <i class="fa fa-camera" id="jOrgCamProfile"></i><span class="name"></span>
                                    <input type="file" name="timelinepic" id="jorgprofilepic" style="display:none;">
                                </form>
                                <?php }?>
                                <h3><?php echo PageContext::$response->resultSet->business_name;?>
</h3>
                            </div>
                              
                           
                        </div>
                        
                        <?php if (PageContext::$response->resultSet->business_created_by==PageContext::$response->sess_user_id){?>
                        <div class="timelineimage">
                            <form name="frmtimelineimageupload" id="jfrmtimelineimageupload"
                                  action="<?php echo PageContext::$response->baseUrl;?>
user/timelineimageresize"
                                  enctype="multipart/form-data" method="post">
                                <i class="fa fa-camera" id="jorgcam"></i><span class="name"></span>
                                <input type="file" name="jorgtimelinepic" id="jorgtimelinepic" style="display:none;">
                            </form>
                        </div>
                        <?php }?>
                        <div class="menufloattop">
                            <?php if (PageContext::$response->sess_user_id!=PageContext::$response->resultSet->business_created_by&&PageContext::$response->sess_user_id>0){?>
                              <?php if (in_array(PageContext::$response->sess_user_id,PageContext::$response->tagged)){?>
                                <a id="" class="jUnfollowPage" aid="<?php echo PageContext::$response->resultSet->business_id;?>
" href="javascript:void(0)" bid="<?php echo PageContext::$response->tagg_id;?>
"><i class="fa fa-user-times"></i> Unfollow</a>
                                <?php }else{ ?>
                                <a id="jFollowPage" aid="<?php echo PageContext::$response->resultSet->business_id;?>
" href="javascript:void(0)"><i class="fa fa-user-plus"></i> Follow</a>
                              <?php }?>
                            <?php }?>
                            </a></div>
                    </div>
                    <div class="timeline_tabmenu buztimelineblock">
                            <ul id="myTab" class="nav nav-tabs nav-justified">

                                <!-- Organization Details  tab -->
                                <li class="active"><a href="#" id="jViewBusiness" class="jtab" data-toggle="tab">About
                                        </a>
                                </li>

                                <!-- Discussion tab -->
                                <li class=""><a id="jViewDiscussion" alias="<?php echo PageContext::$response->rate_entity_alias;?>
"
                                                href="#jViewDiscussion" class="jtab" data-toggle="tab">Discussions</a>
                                </li>

                                <!-- Associated People tab -->
                                <li class="jShowProfile" alias="<?php echo PageContext::$response->rate_entity_alias;?>
"><a alias="<?php echo PageContext::$response->rate_entity_alias;?>
"
                                        id="jViewAssociatedPeople" bid="<?php echo PageContext::$response->business_id;?>
"
                                        bname="<?php echo PageContext::$response->resultSet->business_name;?>
" class="jtab"
                                        href="#jViewAssociatedPeople" data-toggle="tab">Followers</a>
                                </li>

                                <!-- Associated Group tab -->
                                <?php if (PageContext::$response->org_grp==1){?>
                                <li class="jShowFriends" alias="<?php echo PageContext::$response->rate_entity_alias;?>
"><a alias="<?php echo PageContext::$response->rate_entity_alias;?>
"
                                        bid="<?php echo PageContext::$response->business_id;?>
"
                                        bname="<?php echo PageContext::$response->resultSet->business_name;?>
" class="jtab "
                                        id="jViewAssociatedBizcom" href="#jViewAssociatedBizcom" data-toggle="tab"
                                        >Groups</a>
                                </li>
                                <?php }?>

                                <!-- Testimonials tab -->
                                <li class="jShowFriends" alias="<?php echo PageContext::$response->rate_entity_alias;?>
"><a
                                        bid="<?php echo PageContext::$response->business_id;?>
"
                                        bname="<?php echo PageContext::$response->resultSet->business_name;?>
" class="jtab "
                                        id="jViewAssociatedTestimonials" href="#jViewAssociatedTestimonials" data-toggle="tab"
                                        >Reviews</a>
                                </li>

                                <!-- Gallery tab -->
                                <li class="jShowFriends" alias="<?php echo PageContext::$response->rate_entity_alias;?>
"><a
                                        bid="<?php echo PageContext::$response->business_id;?>
"
                                        bname="<?php echo PageContext::$response->resultSet->business_name;?>
" class="jtab "
                                        id="jViewAssociatedGallery" href="#jViewAssociatedGallery" data-toggle="tab"
                                    >Photos</a>
                                </li>

                            </ul>
                        </div>
                </div>
            </div>
        </div>
        <?php }else{ ?>
        <br>
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PageContext::renderRegisteredPostActions('messagebox');<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php }?>
    </div>
</div>
<div class='clearfix'></div>
<div class='container'>
    <div class='row'>
         <?php if (PageContext::$response->resultSet->business_name!=''){?>
        <section class="marg15col">
        <div id="jDivDisplayBusiness" class="tab-content">
            <div class="loader" id="loader">
                <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif"/>
            </div>
            <div class='col-sm-4 col-md-5 col-lg-3'>   
           <?php if (count(PageContext::$response->friend_display_list)>0){?>            
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-user" aria-hidden="true"></i> Followers <a  href="#" id="jMoreOrgphotos" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist">
                    <?php  $_smarty_tpl->tpl_vars['friendslist'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['friendslist']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->friend_display_list; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['friendslist']->key => $_smarty_tpl->tpl_vars['friendslist']->value){
$_smarty_tpl->tpl_vars['friendslist']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['friendslist']->key;
?>
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <a class="friendphotobox_blk" href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['friendslist']->value->user_alias;?>
">
                                <?php if ($_smarty_tpl->tpl_vars['friendslist']->value->file_path){?>
                                <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['friendslist']->value->file_path;?>
');">
                                </div>
                                <?php }else{ ?>
                                <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
medium/member_noimg.jpg');">
                                </div>
                                <?php }?>
                                <h4><?php echo ucfirst($_smarty_tpl->tpl_vars['friendslist']->value->user_firstname);?>
 <?php echo ucfirst($_smarty_tpl->tpl_vars['friendslist']->value->user_lastname);?>
</h4>
                            </a>
                        </li>
                   <?php } ?>
                </ul>
                </div>  
            <?php }?>
   <?php if (count(PageContext::$response->gallery_display_list)>0){?>
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-file-image-o" aria-hidden="true"></i> Photos <a href="#" id="jMoreOrgGallery" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist portfolio">
                    <?php  $_smarty_tpl->tpl_vars['gallerylist'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gallerylist']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->gallery_display_list; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gallerylist']->key => $_smarty_tpl->tpl_vars['gallerylist']->value){
$_smarty_tpl->tpl_vars['gallerylist']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['gallerylist']->key;
?>
                     
                     
<!--                    <li class="display_table_cell wid32per">
                        <a class="friendphotobox_blk" href="#">
                            <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->news_feed_image_name;?>
');">-->
                                
<!--                            <ul class="portfolio" class="clearfix">-->
                                    <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4 vert_align_top"><a class="friendphotobox_blk" href="<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->organization_announcement_image_path;?>
" title="<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->organization_announcement_content;?>
"><img src="<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->organization_announcement_image_path;?>
" alt=""></a></li>
<!--                            </ul>-->
                                
<!--                            </div>
                        </a>
                    </li>-->
                    <?php } ?>
                </ul>
                <!--<div class="sidead" style="background-image: url('<?php echo PageContext::$response->ImagePath;?>
sidead.jpg');"> </div>-->
            </div>  
    <?php }?>  
            </div>
            <div class="col-sm-8 col-md-7 col-lg-9">
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PageContext::renderRegisteredPostActions('messagebox');<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

             <?php if (PageContext::$response->resultSet->business_name!=''){?>
            <div class="wid100per">
                <section class="business_profile_desc marg10col marg0top whitebox">
                    <h3><?php echo PageContext::$response->resultSet->business_name;?>

                        <?php if (PageContext::$response->resultSet->business_status=='A'){?>
                        <div class="business_profile_desc_starring">
                            <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PageContext::renderRegisteredPostActions('rating');<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                        </div>
                        <?php }?>
                    </h3>
                    <?php if (PageContext::$response->resultSet->user_firstname!=''){?>
                    <p class="owncaption"><span>Owned By</span> : <a
                            href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo PageContext::$response->resultSet->user_alias;?>
"><?php echo PageContext::$response->resultSet->user_firstname;?>

                            <?php echo PageContext::$response->resultSet->user_lastname;?>
</a></p>
                            <?php }?>
                    <!--<p class="owncaption"><span>Category</span> : <?php echo PageContext::$response->resultSet->category_name;?>
</p>-->

                    <div class="business_profile_desc_location">
                        <div class="wid100per"> <?php if (PageContext::$response->resultSet->business_address!=''){?>
                            <div class="left"><?php echo PageContext::$response->resultSet->business_address;?>
</div>
                            <?php }?>
                            <?php if (PageContext::$response->sess_user_id==PageContext::$response->resultSet->business_created_by){?>
                            <div class="right">
                                <a class="edititem left marg5right"
                                   href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
edit-page/<?php echo PageContext::$response->resultSet->business_id;?>
"><i
                                        class="fa fa-pencil"></i></a>
                                <a class="deleteitem left" onclick="return deleteConfirm();"
                                   href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
index/delete_organization/<?php echo PageContext::$response->resultSet->business_id;?>
"><i
                                        class="fa fa-trash"></i></a>
                            </div>
                            <?php }?>
                            <div class="clearfix"></div>
                        </div>
                        <p>
                            <?php echo stripslashes(PageContext::$response->resultSet->business_description);?>

                        </p>
                    </div>
                            <?php if (count(PageContext::$response->resultSet)>1){?>
                    <?php if (PageContext::$response->resultSet->business_status=='A'){?>
                    <div class="inquire_ratingblk">
                        <div class="business_profile_desc_action">
                            <input type="hidden" value="<?php echo PageContext::$response->sess_user_id;?>
" class="login_status"
                                   name="" id="login_status">
                            <?php if (PageContext::$response->sess_user_id==PageContext::$response->resultSet->business_created_by){?>
                            <?php }else{ ?>
                            <!--<input type="button" value="INQUIRE" class="yellow_btn" name="" onclick="return checkLoginInquire('B','<?php echo PageContext::$response->resultSet->business_id;?>
','Inquire');">-->
<!--                            <?php if (PageContext::$response->sess_user_status!='I'){?>
                            <input type="button" value="POST TESTIMONIAL" class="yellow_btn" name=""
                                   onclick=" return checkLogin('B','<?php echo PageContext::$response->resultSet->business_id;?>
','Tstimonial');">
                            <?php }?>-->
                            <?php }?>
                        </div>
                    </div>

                    <?php }else{ ?>
                    <div class="inquire_ratingblk" style="color:red">
                        Pending approval from administrator
                    </div>
                    <?php }?>
                    <?php }?>
                </section>
            </div>
             <?php }?>
            <!--<div class="col-sm-4 col-md-4 col-lg-4">
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 PageContext::renderRegisteredPostActions('googlemap'); <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            </div>-->
             <?php if (PageContext::$response->resultSet->business_name!=''){?>
            <div class="wid100per">
                <section class="whitebox">
                    <div class="display_table addresstbl">
                        <?php if (PageContext::$response->resultSet->business_address!=''){?>
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">Address</span>
                            <span class="display_table_cell wid70per"><?php echo PageContext::$response->resultSet->business_address;?>
</span>
                        </div>
                        <?php }?>
                        <?php if (PageContext::$response->resultSet->business_city!=''){?>
                        <div class="display_table_row">
                            <div class="display_table_cell wid30per hdleft">City</div>
                            <span
                                class="display_table_cell wid70per"><?php echo PageContext::$response->resultSet->business_city;?>
</span>
                        </div>
                        <?php }?>
                        <?php if (PageContext::$response->resultSet->business_state!=''){?>
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">State</span>
                            <span class="display_table_cell wid70per">        <?php if (PageContext::$response->resultSet->State){?><?php echo PageContext::$response->resultSet->State;?>
<?php }else{ ?><?php if (PageContext::$response->resultSet->business_state){?><?php echo PageContext::$response->resultSet->business_state;?>
<?php }?><?php }?></span>
                        </div>
                        <?php }?>
                        <?php if (PageContext::$response->resultSet->business_country!=''){?>
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">Country</span>
                            <span
                                class="display_table_cell wid70per"><?php echo PageContext::$response->resultSet->Country;?>
</span>
                        </div>
                        <?php }?>
                        <?php if (PageContext::$response->resultSet->business_zipcode!=''){?>
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">Zip</span>
                            <span class="display_table_cell wid70per"><?php echo PageContext::$response->resultSet->business_zipcode;?>
</span>
                        </div>
                        <?php }?>
                        <?php if (PageContext::$response->resultSet->business_phone!=''){?>
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">Phone</span>
                            <span class="display_table_cell wid70per"><?php echo PageContext::$response->resultSet->business_phone;?>
</span>
                        </div>
                        <?php }?>
                        <?php if (PageContext::$response->resultSet->business_email!=''){?>
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">Email</span>
                            <span class="display_table_cell wid70per"><a
                                    href="mailto:<?php echo PageContext::$response->resultSet->business_email;?>
"><?php echo PageContext::$response->resultSet->business_email;?>
</a></span>
                        </div>
                        <?php }?>
                        <?php if (PageContext::$response->resultSet->business_url!=''){?>
                        <div class="display_table_row">
                            <span class="display_table_cell wid30per hdleft">Website</span>
                            <span class="display_table_cell wid70per">
                               <a
                                    href="<?php echo PageContext::$response->resultSet->business_url;?>
"
                                    target="_new"><?php echo PageContext::$response->resultSet->business_url;?>
</a></span>
                        </div>
                        <?php }?>
                    </div>
                    <div class="clear"></div>
                </section>
            </div>
<?php }?>
            <div class="clear"></div>
            </div>

            <div class="listitem loader">
                <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif"/> Loading....
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        </section>
         <?php }?>
    </div>
</div>

<?php }} ?>