<?php /* Smarty version Smarty-3.1.13, created on 2017-01-02 06:50:06
         compiled from "project\modules\default\view\script\index\login.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:49605869ea0e4cbb43-77784488%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '306f78615ae665a34a3370cead60799bb511483f' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\index\\login.tpl.php',
      1 => 1468391333,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '49605869ea0e4cbb43-77784488',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5869ea0e5d1700_44324456',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5869ea0e5d1700_44324456')) {function content_5869ea0e5d1700_44324456($_smarty_tpl) {?><div class="container">
    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-4 "></div>
        <div class="col-sm-4 col-md-4 col-lg-4 ">
            <section class="whitebox marg40col">
            <div class="signinform">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3><i class="fa fa-envelope-o"></i> Sign in</h3>
                    </div>
                </div>
                <div class="form-bottom signup_form">
                    <form name="formLogin" id="formLogin" action="" method="post" role="form">
                        <!-- alert failed -->
                        <?php if (PageContext::$response->message['msg']!=''){?>
                        <div class="alert alert-danger <?php echo PageContext::$response->message['msgClass'];?>
">
                            <i class="fa fa-frown-o"></i> 
                            <?php echo PageContext::$response->message['msg'];?>

                        </div>
                        <?php }?>
                        <div class="form-group relative">
                            <i class="fa fa-envelope"></i>
                            <input type="text" value="<?php echo PageContext::$response->mail;?>
" name="user_email" class="form-email form-control" placeholder="Your Email" >
                            <label class="error" style="display:none;" generated="true" for="user_email"></label>
                        </div>
                        <div class="form-group relative">
                            <i class="fa fa-lock"></i>
                            <input type="password" value="<?php echo PageContext::$response->password;?>
" name="user_password" class="form-control" placeholder="Password" >
                            <label class="error" style="display:none;" generated="true" for="user_password"></label>
                        </div>
                        <div class="temrs_blk_signup">
                            <span class="remember-box checkbox">
                                <label for="rememberme">
                                    <input type="checkbox" value="1" name="remember_me" id="rememberme" checked="<?php echo PageContext::$response->rememberCheck;?>
">Remember Me
                                </label>
                            </span>
                        </div>
                        <input type="submit" name="btnSubmit" data-loading-text="Loading..." class="btn btn-primary btn-block yellow_btn2" value="Sign in">
                        <div class="clearfix"></div>
                        <?php if (PageContext::$response->isFBEnabled){?>
                        <!--start facebook div-->
                            <input id="fbBtn" type="button" class="loginfb_btn" value="Login with facebook">
                       <?php }?>
                       <div class="clearfix"></div>
                        <div class="wid100per">
                            <span class="forg_pwrd"><a href="<?php echo PageContext::$response->baseUrl;?>
lost-password">Forgot Password?</a></span>
                        </div>
                    </form>
                </div>
            </div>
            </section>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 "></div>
    </div>
</div>


<?php }} ?>