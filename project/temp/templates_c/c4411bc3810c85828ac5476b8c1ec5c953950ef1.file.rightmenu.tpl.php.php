<?php /* Smarty version Smarty-3.1.13, created on 2017-01-02 07:05:25
         compiled from "project\modules\default\view\script\user\rightmenu.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:224525869eda510cea2-41014327%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c4411bc3810c85828ac5476b8c1ec5c953950ef1' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\user\\rightmenu.tpl.php',
      1 => 1469506349,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '224525869eda510cea2-41014327',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5869eda52f9214_74948314',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5869eda52f9214_74948314')) {function content_5869eda52f9214_74948314($_smarty_tpl) {?><nav id="c-menu--slide-right" class="c-menu c-menu--slide-right">
  <button class="c-menu__close">Close Menu &rarr;</button>
  <ul class="c-menu__items">
      <div id='cssmenu'>
      <ul>
        <li class='has-sub'><a class ='main' href='#'>Profile</a>
            <ul>
              <li><a href="<?php echo PageContext::$response->baseUrl;?>
profile/<?php echo PageContext::$response->sess_user_alias;?>
">My Profile</a></li>
            </ul>
        </li>
        <li class='has-sub'><a class ='main' href='#'>Friends</a>
            <ul>
              <li><a class="tooltip-left" data-tooltip="Your Friends" href="<?php echo PageContext::$response->baseUrl;?>
my-friends">My Friends</a></li>
              <li><a class="tooltip-left" data-tooltip="Your Pending Friend Requests" href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl; <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
pending-request">Pending Friend Requests</a></li>
            </ul>
        </li>
        <li class='has-sub'><a class ='main' href='#'>Message</a>
            <ul>
              <li><a href="<?php echo PageContext::$response->baseUrl;?>
inbox">Inbox</a></li>
              <li><a href="<?php echo PageContext::$response->baseUrl;?>
sentitems">Sent Items</a></li>
              <!--<li><a href="<?php echo PageContext::$response->baseUrl;?>
invitation-sent">Invitations Sent</a></li>-->
            </ul>
        </li>
        <li class='has-sub'><a class ='main' href='#'>Groups</a>
            <ul>
              <li><a href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl; <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
add-group">Create A New Group</a></li>
              <li><a href="<?php echo PageContext::$response->baseUrl;?>
my-groups">Manage an existing Group</a></li>
              <li><a href="<?php echo PageContext::$response->baseUrl;?>
my-subscribed-group">My Subscribed Groups</a></li>
              <li><a href="<?php echo PageContext::$response->baseUrl;?>
add-announcement">Make New Announcement</a></li>
              <li><a href="<?php echo PageContext::$response->baseUrl;?>
pending-bizcom-request">Invitations for you to join other Groups</a></li>
              <li><a href="<?php echo PageContext::$response->baseUrl;?>
pending-bizcom-members">Pending requests to join your Groups</a></li>
              <li><a href="<?php echo PageContext::$response->baseUrl;?>
pending-members">Pending approvals to join your Groups</a></li>
            </ul>
        </li>

        <li class='has-sub'><a href='#'>Organiasations</a>
            <ul>
              <li><a href="<?php echo PageContext::$response->baseUrl;?>
pages">My Pages</a></li>
              <li><a href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl; <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
add-page">Create A New Page</a></li>
            </ul>
        </li>
      </ul>

      </div>
  </ul>
</nav><!-- /c-menu slide-right -->








            
        




























<div id="c-mask" class="c-mask"></div><!-- /c-mask -->

<?php }} ?>