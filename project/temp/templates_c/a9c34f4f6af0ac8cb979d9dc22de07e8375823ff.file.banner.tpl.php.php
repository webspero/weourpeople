<?php /* Smarty version Smarty-3.1.13, created on 2017-01-02 06:49:42
         compiled from "project\modules\default\view\script\index\banner.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:150095869e9f6552f91-81269670%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a9c34f4f6af0ac8cb979d9dc22de07e8375823ff' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\index\\banner.tpl.php',
      1 => 1473913361,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '150095869e9f6552f91-81269670',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'banner' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5869e9f68d95e9_95399177',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5869e9f68d95e9_95399177')) {function content_5869e9f68d95e9_95399177($_smarty_tpl) {?>


<div class="banner_outer">
	<div class="bnr_content_sec">
	  <div class="container">
	  <div class="row"> 
			<div class="col-sm-12 col-md-8 col-lg-8 ">
				<div class="bnrright_img">
					<img src="<?php echo PageContext::$response->themeImage;?>
bnr_left.png">
				</div>
				<div class="bnrright_hds">
					<!--<h3>Your <span>Personalised</span> Social Network Is Here</h3>-->
                                        <h3><?php echo PageContext::$response->site_description;?>
</h3>
				</div>
	        </div>
               <?php if (PageContext::$response->flagDisplay!=1){?>
	        <div class="col-sm-12 col-md-4 col-lg-4 ">
	        	<section class="signuphome">
					<div class="form-top">
                		<div class="form-top-left">
                			<h3>New To <?php echo PageContext::$response->site_label;?>
 <span>? Join Today!</span></h3>
                		</div>
                    </div>
                    <div class="form-bottom signup_form">
	                    <form name="frmUserRegister" id="frmUserRegister" method="post" action= '<?php echo PageContext::$response->baseUrl;?>
index' class='' role="form">
	                        <div class="form-group relative">
	                        	<i class="fa fa-envelope"></i>
	                        	<input type="text" id="user_emails" name = "user_emails" value ="<?php echo PageContext::$response->user_emails;?>
" class="form-email form-control" placeholder="Your Email" >
                                        <label <?php if (!PageContext::$response->message0){?> style="display:none;" <?php }?> generated="true" class="error"><?php echo PageContext::$response->message0;?>
</label>
	                        </div>
	                        <div class="form-group relative">
	                        	<i class="fa fa-lock"></i>
	                        	<input type="password" id="user_passwords" name = "user_passwords" value ="" class="form-control" placeholder="Password" >
                                        <label <?php if (!PageContext::$response->message1){?> style="display:none;" <?php }?> generated="true" class="error"><?php echo PageContext::$response->message1;?>
</label>
	                        </div>
	                        <div class="form-group relative">
	                        	<i class="fa fa-unlock-alt"></i>
	                        	<input type="password" id="user_confirm_password" name = "user_confirm_password" value ="" class="form-control" placeholder="Re - Enter Password">
                                        <label <?php if (!PageContext::$response->message2){?> style="display:none;" <?php }?> generated="true" class="error"><?php echo PageContext::$response->message2;?>
</label>
	                        </div>
	                        <div class="temrs_blk_signup">
	                        	<input id = "terms_conditions" name = "terms_conditions" value = "1" size = "10" type="checkbox" class="" title="I agree to the &lt;a href='<?php echo PageContext::$response->baseUrl;?>
terms' target='_blank'&gt;Terms of Service&lt;/a&gt; and &lt;a href='<?php echo PageContext::$response->baseUrl;?>
privacy-policy' target='_blank'&gt;Privacy Policy&lt;/a&gt;." placeholder=""> I agree to the <a target="_blank" href="<?php echo PageContext::$response->baseUrl;?>
terms">Terms of Service</a> and <a target="_blank" href="<?php echo PageContext::$response->baseUrl;?>
privacy-policy">Privacy Policy</a>.<div class="clserror"><label generated="true" for="terms_conditions" class="error"></label></div>
                                        <label <?php if (!PageContext::$response->message3){?> style="display:none;" <?php }?> generated="true" class="error"><?php echo PageContext::$response->message3;?>
</label>
	                        </div>
	                        <input type="submit" class="btn btn-primary btn-block blue_btn2" value="Sign Up" name="btnSubmit">
	                    </form>
                    </div>
                </section>
                   
	        </div>
               <?php }?>
               <div class="clearfix"></div>
	  </div>
	  </div>
	</div>


 
 <!-- 
	<div class="container">
		<div id="da-slider" class="da-slider">
			<?php  $_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['banner']->_loop = false;
 $_from = PageContext::$response->banner; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['banner']->key => $_smarty_tpl->tpl_vars['banner']->value){
$_smarty_tpl->tpl_vars['banner']->_loop = true;
?>
				<div class="da-slide">
						<h2><?php echo $_smarty_tpl->tpl_vars['banner']->value->banner_title;?>
</h2>
						<p><?php echo $_smarty_tpl->tpl_vars['banner']->value->banner_description;?>
</p>
						<?php if ($_smarty_tpl->tpl_vars['banner']->value->banner_link_text!=''){?>
						<a href="<?php echo PageContext::$response->baseUrl;?>
<?php echo $_smarty_tpl->tpl_vars['banner']->value->banner_link;?>
" class="da-link"><?php echo $_smarty_tpl->tpl_vars['banner']->value->banner_link_text;?>
</a>
						<?php }?>
						<div class="da-img">
                                                    <?php if ($_smarty_tpl->tpl_vars['banner']->value->banner_image_name!=''){?>
                                                    <img src="<?php echo PageContext::$response->userImagePath;?>
slider/<?php echo $_smarty_tpl->tpl_vars['banner']->value->banner_image_name;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['banner']->value->banner_title;?>
" />
				             <?php }?>
                                             </div> 
                                </div> 
				<?php } ?>		
				<nav class="da-arrows">
					<span class="da-arrows-prev"></span>
					<span class="da-arrows-next"></span>
				</nav>
			</div>
	</div>
	
   -->
    <div class="clearfix"></div>
</div>


<script>
$(function () {

      // Slideshow 1
      $("#slider1").responsiveSlides({
        
        speed: 800
      });
    });
</script><?php }} ?>