<?php /* Smarty version Smarty-3.1.13, created on 2017-02-03 04:29:01
         compiled from "project\modules\default\view\script\user\newsfeed.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:318795869eda1857500-98070064%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '35d9791d22022dc62e888f2bc52a822c5e16c66d' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\user\\newsfeed.tpl.php',
      1 => 1486039574,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '318795869eda1857500-98070064',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5869eda507c607_66897029',
  'variables' => 
  array (
    'first_col' => 0,
    'comments' => 0,
    'second_col' => 0,
    'flag' => 0,
    'feed' => 0,
    'timesplit' => 0,
    'reg_exUrl' => 0,
    'url' => 0,
    'val' => 0,
    'announcement' => 0,
    'feedlist' => 0,
    'img' => 0,
    'third_col' => 0,
    'friendslist' => 0,
    'gallerylist' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5869eda507c607_66897029')) {function content_5869eda507c607_66897029($_smarty_tpl) {?><script>

    $(document).ready(function () {
        magnificPopupSingleFnNewsfeed();

        var page = 1;
        var _throttleTimer = null;
        var _throttleDelay = 100;
        $(window).scroll(function () {

            clearTimeout(_throttleTimer);
            _throttleTimer = setTimeout(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                    if (page < TOTAL_FEED_PAGES) {
                        page++;
                        $.blockUI({ message : '' });
                        var user_alias = $("#user_alias").val();
                        $.ajax({
                            type: 'Post',
                            url: mainUrl + 'user/newsfeedajax/' + page,
                            data: 'alias=' + user_alias,
                            success: function (data) {

                                var html = $.parseHTML(data);
                                enableEmojiOn(html);
                                $('.jfeeddisplay_div').append(html);

                                // parse facebook share button
                                FB.XFBML.parse();

                                magnificPopupSingleFnNewsfeed();
                                $.unblockUI();
                            }
                        })
                    }
                }
            }, _throttleDelay);
        });
       
    });

</script>
<div class="container" id="div_container">
<div class="marg20col"> 
<div class="row">
    <?php if (PageContext::$response->sess_user_name!=''){?>
    <div id='msg'></div>
    <input type="hidden" value="<?php echo PageContext::$response->alias;?>
" id="user_alias">
<?php if (PageContext::$response->friend_display=='1'||PageContext::$response->gallery_display=='1'){?>
<?php if (count(PageContext::$response->friend_display_list)>0||count(PageContext::$response->gallery_display_list)>0){?> 
  <?php if (isset($_smarty_tpl->tpl_vars['first_col'])) {$_smarty_tpl->tpl_vars['first_col'] = clone $_smarty_tpl->tpl_vars['first_col'];
$_smarty_tpl->tpl_vars['first_col']->value = 'col-sm-12 col-md-3 col-lg-3'; $_smarty_tpl->tpl_vars['first_col']->nocache = null; $_smarty_tpl->tpl_vars['first_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['first_col'] = new Smarty_variable('col-sm-12 col-md-3 col-lg-3', null, 0);?> 
  <?php if (isset($_smarty_tpl->tpl_vars['second_col'])) {$_smarty_tpl->tpl_vars['second_col'] = clone $_smarty_tpl->tpl_vars['second_col'];
$_smarty_tpl->tpl_vars['second_col']->value = 'col-sm-8 col-md-5 col-lg-6'; $_smarty_tpl->tpl_vars['second_col']->nocache = null; $_smarty_tpl->tpl_vars['second_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['second_col'] = new Smarty_variable('col-sm-8 col-md-5 col-lg-6', null, 0);?> 
  <?php if (isset($_smarty_tpl->tpl_vars['third_col'])) {$_smarty_tpl->tpl_vars['third_col'] = clone $_smarty_tpl->tpl_vars['third_col'];
$_smarty_tpl->tpl_vars['third_col']->value = 'col-sm-4 col-md-4 col-lg-3'; $_smarty_tpl->tpl_vars['third_col']->nocache = null; $_smarty_tpl->tpl_vars['third_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['third_col'] = new Smarty_variable('col-sm-4 col-md-4 col-lg-3', null, 0);?> 
<?php }else{ ?>
  <?php if (isset($_smarty_tpl->tpl_vars['first_col'])) {$_smarty_tpl->tpl_vars['first_col'] = clone $_smarty_tpl->tpl_vars['first_col'];
$_smarty_tpl->tpl_vars['first_col']->value = 'col-sm-4 col-md-4 col-lg-3'; $_smarty_tpl->tpl_vars['first_col']->nocache = null; $_smarty_tpl->tpl_vars['first_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['first_col'] = new Smarty_variable('col-sm-4 col-md-4 col-lg-3', null, 0);?> 
  <?php if (isset($_smarty_tpl->tpl_vars['second_col'])) {$_smarty_tpl->tpl_vars['second_col'] = clone $_smarty_tpl->tpl_vars['second_col'];
$_smarty_tpl->tpl_vars['second_col']->value = 'col-sm-8 col-md-8 col-lg-9'; $_smarty_tpl->tpl_vars['second_col']->nocache = null; $_smarty_tpl->tpl_vars['second_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['second_col'] = new Smarty_variable('col-sm-8 col-md-8 col-lg-9', null, 0);?> 
  <?php if (isset($_smarty_tpl->tpl_vars['third_col'])) {$_smarty_tpl->tpl_vars['third_col'] = clone $_smarty_tpl->tpl_vars['third_col'];
$_smarty_tpl->tpl_vars['third_col']->value = 'col-sm-12 col-md-12 col-lg-12'; $_smarty_tpl->tpl_vars['third_col']->nocache = null; $_smarty_tpl->tpl_vars['third_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['third_col'] = new Smarty_variable('col-sm-12 col-md-12 col-lg-12', null, 0);?>
<?php }?>
<?php }else{ ?>
  <?php if (isset($_smarty_tpl->tpl_vars['first_col'])) {$_smarty_tpl->tpl_vars['first_col'] = clone $_smarty_tpl->tpl_vars['first_col'];
$_smarty_tpl->tpl_vars['first_col']->value = 'col-sm-4 col-md-4 col-lg-3'; $_smarty_tpl->tpl_vars['first_col']->nocache = null; $_smarty_tpl->tpl_vars['first_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['first_col'] = new Smarty_variable('col-sm-4 col-md-4 col-lg-3', null, 0);?> 
  <?php if (isset($_smarty_tpl->tpl_vars['second_col'])) {$_smarty_tpl->tpl_vars['second_col'] = clone $_smarty_tpl->tpl_vars['second_col'];
$_smarty_tpl->tpl_vars['second_col']->value = 'col-sm-8 col-md-8 col-lg-9'; $_smarty_tpl->tpl_vars['second_col']->nocache = null; $_smarty_tpl->tpl_vars['second_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['second_col'] = new Smarty_variable('col-sm-8 col-md-8 col-lg-9', null, 0);?> 
  <?php if (isset($_smarty_tpl->tpl_vars['third_col'])) {$_smarty_tpl->tpl_vars['third_col'] = clone $_smarty_tpl->tpl_vars['third_col'];
$_smarty_tpl->tpl_vars['third_col']->value = 'col-sm-12 col-md-12 col-lg-12'; $_smarty_tpl->tpl_vars['third_col']->nocache = null; $_smarty_tpl->tpl_vars['third_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['third_col'] = new Smarty_variable('col-sm-12 col-md-12 col-lg-12', null, 0);?>
<?php }?>
<div class="<?php echo $_smarty_tpl->tpl_vars['first_col']->value;?>
">
<div id="sticky-anchor"></div>
<div id="sticky" class="profilesec_left">
    <div class="display_table profilesec_left_top">
    <div class="display_table_cell wid20per">
        <div id="profilepic" class="profile_leftpicthumb">
            <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo PageContext::$response->sess_user_alias;?>
"    alias="<?php echo PageContext::$response->alias;?>
" uid="<?php echo PageContext::$response->sess_user_id;?>
">
                <img  id="profile_image" class="profile_sm_pic" src="<?php echo PageContext::$response->userImagePath;?>
<?php echo PageContext::$response->image;?>
">
            </a>
        </div>
    </div>
<!--        <?php if (PageContext::$response->alias==PageContext::$response->sess_user_alias&&PageContext::$response->sess_user_alias!=''){?>
        <a href="#" class="changepic" onclick="" ><label for="file_profile" ><i class="fa fa-camera"></i></label>
        </a>
        <input type="file" id="file_profile" class="jprofile_image" style="cursor: pointer;  display: none"/>
        <input type="hidden" value="<?php echo PageContext::$response->sess_user_id;?>
" id="user_id" name="user_id">        
        <?php }?>-->
   
    <div class="profilenamesec" >
        <h3 class="profile_name"><a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo PageContext::$response->sess_user_alias;?>
"  alias="<?php echo PageContext::$response->alias;?>
" uid="<?php echo PageContext::$response->sess_user_id;?>
">
                    <?php echo PageContext::$response->username;?>
</a>
        </h3>
        <?php if (PageContext::$response->alias==PageContext::$response->sess_user_alias&&PageContext::$response->sess_user_alias!=''){?>
            <h5>Last logged in : <span><?php echo time_elapsed_string(PageContext::$response->sess_user_last_login);?>
</span></h5>
        <?php }?>
    </div>
<!--    <div class="changepicsec" >
        <?php if (PageContext::$response->alias==PageContext::$response->sess_user_alias&&PageContext::$response->sess_user_alias!=''){?>
        <a href="#" class="changepic" onclick="" ><label for="file_profile" ><i class="fa fa-pencil"></i></label>
        </a>
        <input type="file" id="file_profile" class="jprofile_image" style="cursor: pointer;  display: none"/>
        <input type="hidden" value="<?php echo PageContext::$response->sess_user_id;?>
" id="user_id" name="user_id">        
        <?php }?>
    </div>-->
    </div>
    <div class="profilesec_left_bottom">
        <h4>LINKS</h4>
        <ul>
            <?php if (PageContext::$response->sess_user_id!=''){?>
            <li><a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo PageContext::$response->sess_user_alias;?>
"><i class="fa fa-newspaper-o" aria-hidden="true"></i>  Timeline</a></li>
            <li><a href="<?php echo PageContext::$response->baseUrl;?>
inbox"><i class="fa fa-envelope-o" aria-hidden="true"></i>  Messages</a></li>
            <?php }?>
            <?php if (PageContext::$response->sess_user_id==''){?>
            <li><a><i class="fa fa-users" aria-hidden="true"></i> Friends(<?php echo PageContext::$response->userDetails['user_friends_count'];?>
)</a></li>
            <?php }?>
            <li><a <?php if (PageContext::$response->sess_user_id!=''){?> href="<?php echo PageContext::$response->baseUrl;?>
pages"<?php }?>><i class="fa fa-university" aria-hidden="true"></i>  Pages<?php if (PageContext::$response->sess_user_id==''){?>(<?php echo PageContext::$response->userDetails['user_business_count'];?>
)<?php }?></a></li>
            <li><a <?php if (PageContext::$response->sess_user_id!=''){?> href="<?php echo PageContext::$response->baseUrl;?>
my-groups"<?php }?>><i class="fa fa-users" aria-hidden="true"></i>  Groups<?php if (PageContext::$response->sess_user_id==''){?>(<?php echo PageContext::$response->userDetails['user_community_count'];?>
)<?php }?></a></li>
            <?php if (PageContext::$response->sess_user_id!=''){?>
            <!--<li><a href="<?php echo PageContext::$response->baseUrl;?>
my-subscribed-group"><i class="fa fa-user-plus" aria-hidden="true"></i>  Subscribed Groups</a></li>-->
            <?php if (PageContext::$response->organization_display=='1'){?>
            <h4>
                PAGES 
                <div class="more_add pull-right">
                <?php if (count(PageContext::$response->organization_display_list)>0){?>
                <a href="<?php echo PageContext::$response->baseUrl;?>
pages">More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <?php }else{ ?>
                <a href="<?php echo PageContext::$response->baseUrl;?>
add-page">Add Page <i class="fa fa-plus" aria-hidden="true"></i></a>
                <?php }?> 
                </div>  
            </h4>
           
                <?php  $_smarty_tpl->tpl_vars['comments'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comments']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->organization_display_list; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comments']->key => $_smarty_tpl->tpl_vars['comments']->value){
$_smarty_tpl->tpl_vars['comments']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['comments']->key;
?>
                <li>
                    <a href="<?php echo PageContext::$response->baseUrl;?>
page/<?php echo $_smarty_tpl->tpl_vars['comments']->value->business_alias;?>
"><i class="fa fa-university" aria-hidden="true"></i><?php echo $_smarty_tpl->tpl_vars['comments']->value->business_name;?>
</a>
                </li>
                <?php } ?>
                 
            <?php }?>
            <?php if (PageContext::$response->group_display=='1'){?>
            <h4>
                GROUPS
                <div class="more_add pull-right">
                    <?php if (count(PageContext::$response->group_display_list)>0){?>
                    <a href="<?php echo PageContext::$response->baseUrl;?>
my-groups">More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    <?php }else{ ?>
                    <a href="<?php echo PageContext::$response->baseUrl;?>
add-group">Add Group <i class="fa fa-plus" aria-hidden="true"></i></a>
                    <?php }?>
                </div>
            </h4>
            
           
                <?php  $_smarty_tpl->tpl_vars['comments'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comments']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->group_display_list; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comments']->key => $_smarty_tpl->tpl_vars['comments']->value){
$_smarty_tpl->tpl_vars['comments']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['comments']->key;
?>
                <li>
                    <a href="<?php echo PageContext::$response->baseUrl;?>
group/<?php echo $_smarty_tpl->tpl_vars['comments']->value->community_alias;?>
"><i class="fa fa-users" aria-hidden="true"></i><?php echo $_smarty_tpl->tpl_vars['comments']->value->community_name;?>
</a>
                </li>
                <?php } ?>
            <?php }?>
<!--            <?php if (PageContext::$response->friend_display=='1'){?>
            <?php if (count(PageContext::$response->friend_display_list)>0){?>
            <h4>
                FRIENDS
                <div class="more_add pull-right">
                    <a id="jShowFriends" href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo PageContext::$response->sess_user_alias;?>
">More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </h4>
           
                <?php  $_smarty_tpl->tpl_vars['comments'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comments']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->friend_display_list; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comments']->key => $_smarty_tpl->tpl_vars['comments']->value){
$_smarty_tpl->tpl_vars['comments']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['comments']->key;
?>
                <li>
                    <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_alias;?>
"><i class="fa fa-user" aria-hidden="true"></i><?php echo $_smarty_tpl->tpl_vars['comments']->value->user_firstname;?>
 <?php echo $_smarty_tpl->tpl_vars['comments']->value->user_lastname;?>
</a>
                </li>
                <?php } ?>
            <?php }?>   
            <?php }?>-->

            <?php }?>
        </ul>

    </div>
</div>

<div class="clear"></div>
</div>
        

<div class="<?php echo $_smarty_tpl->tpl_vars['second_col']->value;?>
">    
    <form role="form" id="logoform" method="post" action="" enctype="multipart/form-data">
    <div class="whitebox">
    <div class="emotionsbtn_textblk">
        <textarea class="jEmojiTextarea" placeholder="Post your message.." name="newsfeed_comment" id="newsfeed_comment"></textarea>
        </div>
        <div class="wid100 pad5top">
             <div class="display_image_div">
                <div id="juploadimagepreview"></div>
             </div>
            <div class="pull-left">
                <a href="#" class="camicon"><label for="file_feed" ><i class="fa fa-camera" aria-hidden="true"></i></label></a>
                <input type="file" id="file_feed" class="" onchange="previewFile()" style="cursor: pointer;  display: none"/>

            </div>
            <div class="pull-right">
                 <input type="hidden" id="image_id" value="">
                <input type="button" onclick="postNewsFeed()" value="Post" ondblclick="this.disabled=true;" class="btn primary post_btn">
                <div class="loader" id="post_feed_loader" style="display: none;">
                    <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif" />
                </div>
            </div>
            <div class="profile-bnr">
            <img style="display: none;" class="img img-responsive center" src="<?php if (PageContext::$response->campaignDetails->voucher_image_name!=''){?><?php echo PageContext::$response->userImagePath;?>
<?php echo PageContext::$response->campaignDetails->voucher_image_name;?>
<?php }?>" height="50">
            </div>
            <div class="clearfix"></div>
            <div id="jerrordiv" style="color:#f00"></div>
        </div>
    </div>
        </form>
    <div class="jfeeddisplay_div"> 
     <?php  $_smarty_tpl->tpl_vars['feed'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['feed']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->news_feed; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['feed']->key => $_smarty_tpl->tpl_vars['feed']->value){
$_smarty_tpl->tpl_vars['feed']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['feed']->key;
?>
                                <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['flag']->value;?>
<?php $_tmp1=ob_get_clean();?><?php if (isset($_smarty_tpl->tpl_vars['flag'])) {$_smarty_tpl->tpl_vars['flag'] = clone $_smarty_tpl->tpl_vars['flag'];
$_smarty_tpl->tpl_vars['flag']->value = $_tmp1+1; $_smarty_tpl->tpl_vars['flag']->nocache = null; $_smarty_tpl->tpl_vars['flag']->scope = 0;
} else $_smarty_tpl->tpl_vars['flag'] = new Smarty_variable($_tmp1+1, null, 0);?>
                     
     <?php if ($_smarty_tpl->tpl_vars['feed']->value['community_announcement_id']){?>  
    <div class="whitebox timelinebox" id="group_feed_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
">
        <div class="wid100per">
            <div class="postheadtablediv">
            <div class="post_left">
                <span class="mediapost_pic">
                   <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['feed']->value['user_alias'];?>
">
                    <?php if ($_smarty_tpl->tpl_vars['feed']->value['file_path']!=''){?>
                       <img src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['feed']->value['file_path'];?>
">
                        <?php }else{ ?>
                         <img src="<?php echo PageContext::$response->userImagePath;?>
medium/member_noimg.jpg">
                        <?php }?>
                        </a>
                </span>
            </div>
            <div class="post_right">
                <div class="posthead">
                    <h4 class="media-heading">
                        <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['feed']->value['user_alias'];?>
"><?php echo $_smarty_tpl->tpl_vars['feed']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['feed']->value['user_lastname'];?>
</a> 
                        <a href="<?php echo PageContext::$response->baseUrl;?>
group/<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_alias'];?>
"><i class="fa fa-play" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['feed']->value['community_name'];?>
</a>
                    </h4>
                </div>
                <div class="postsubhead">
                    <div class="postsubhead_right">
                        <span> 
                            <?php if ($_smarty_tpl->tpl_vars['feed']->value['days']>0){?>
                                <?php echo $_smarty_tpl->tpl_vars['feed']->value['days'];?>
 days ago
                            <?php }else{ ?>
                                <?php if (isset($_smarty_tpl->tpl_vars["timesplit"])) {$_smarty_tpl->tpl_vars["timesplit"] = clone $_smarty_tpl->tpl_vars["timesplit"];
$_smarty_tpl->tpl_vars["timesplit"]->value = explode(":",$_smarty_tpl->tpl_vars['feed']->value['output_time']); $_smarty_tpl->tpl_vars["timesplit"]->nocache = null; $_smarty_tpl->tpl_vars["timesplit"]->scope = 0;
} else $_smarty_tpl->tpl_vars["timesplit"] = new Smarty_variable(explode(":",$_smarty_tpl->tpl_vars['feed']->value['output_time']), null, 0);?>
                                <?php if ($_smarty_tpl->tpl_vars['timesplit']->value[0]>0){?>
                                    <?php echo ltrim($_smarty_tpl->tpl_vars['timesplit']->value[0],'0');?>
 hrs ago
                                <?php }else{ ?>
                                    <?php if ($_smarty_tpl->tpl_vars['timesplit']->value[1]>0){?>
                                        <?php echo ltrim($_smarty_tpl->tpl_vars['timesplit']->value[1],'0');?>

                                            <?php if ($_smarty_tpl->tpl_vars['timesplit']->value[1]==1){?> min
                                            <?php }else{ ?> mins
                                            <?php }?> ago
                                    <?php }else{ ?>
                                        Just Now
                                    <?php }?>
                                <?php }?>
                            <?php }?>
                        </span>
                    </div>
                </div>
            </div>
            </div>
            <div class="clearfix"></div>
            <div class="postsubhead_left">
                <?php if (isset($_smarty_tpl->tpl_vars["reg_exUrl"])) {$_smarty_tpl->tpl_vars["reg_exUrl"] = clone $_smarty_tpl->tpl_vars["reg_exUrl"];
$_smarty_tpl->tpl_vars["reg_exUrl"]->value = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/'; $_smarty_tpl->tpl_vars["reg_exUrl"]->nocache = null; $_smarty_tpl->tpl_vars["reg_exUrl"]->scope = 0;
} else $_smarty_tpl->tpl_vars["reg_exUrl"] = new Smarty_variable('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/', null, 0);?>
                <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['reg_exUrl']->value;?>
<?php $_tmp2=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_content'];?>
<?php $_tmp3=ob_get_clean();?><?php if (preg_match($_tmp2,$_tmp3,$_smarty_tpl->tpl_vars['url']->value)){?>
                  
                <div class="jEmojiable"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['reg_exUrl']->value;?>
<?php $_tmp4=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_content'];?>
<?php $_tmp5=ob_get_clean();?><?php echo preg_replace($_tmp4,"<a href='".((string)$_smarty_tpl->tpl_vars['url']->value[0])."'>".((string)$_smarty_tpl->tpl_vars['url']->value[0])."</a>",$_tmp5);?>
</div>

                <?php }else{ ?>

                <div class="jEmojiable"><?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_content'];?>
</div>

                <?php }?>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>
        <?php if ($_smarty_tpl->tpl_vars['feed']->value['community_announcement_image_path']){?>
            <div class="postpic">
<!--                <img src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_image_path'];?>
" class="fancybox">-->
                <ul  class="portfolio" class="clearfix">
                        <li><a href="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_image_path'];?>
" title=""><img src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_image_path'];?>
" alt=""></a></li>
                </ul>
            </div>
        <?php }?>
        <div class="clearfix"></div>

        <div class="mediapost_user-side">
            <div class="row">
            <div class="col-sm-4 col-md-4 col-lg-5">
                <div class="likesec">
                <div class="display_table_cell pad10_right">
                <a href="#" class=" colorgrey_text jLikeFeedcommunity <?php if ($_smarty_tpl->tpl_vars['feed']->value->LIKE_ID>0){?>  liked <?php }?>" id="jlike_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" 
                   aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" cid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_id'];?>
">
                    <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                    <span id="jlikedisplay_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
">Like </span></a>
                </div>
                 <div class="display_table_cell pad10_right">
                    <i class="fa fa-comments-o icn-fnt-size"></i>
                <a href="#" id="jCommentButton_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" class="jCommentButton colorgrey_text">Comment</a>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['feed']->value['community_announcement_user_id']==PageContext::$response->sess_user_id){?>
                    <div class="display_table_cell pad10_right">
                    <i class="fa fa-trash-o icn-fnt-size"></i>
                    <a href="#" id="jDeleteButton_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" class="jGroupDeletetButton colorgrey_text">Delete</a>
                    </div>
               <?php }?>
                </div>
            </div>
            <div class="col-sm-8 col-md-8 col-lg-7">
                <div class="sharefnt">
                                                                     <div id="group_like_users_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" class="like_users_div" style=" display: none;">
                      <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->newsFeedLikeUsers[$_smarty_tpl->tpl_vars['feed']->value['community_announcement_id']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value){
$_smarty_tpl->tpl_vars['val']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['val']->key;
?> 
                      <span><?php echo $_smarty_tpl->tpl_vars['val']->value->user_name;?>
<br></span>
                      <?php } ?>  
                    </div>
                <span class="mediapost_links jShowGroupLikeUsers" id="jcountlike_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
"><?php if ($_smarty_tpl->tpl_vars['feed']->value['community_announcement_num_likes']>0){?><?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_num_likes'];?>
 Likes <?php }?></span>
                <span class="mediapost_links jCommentButton count_class"  aid ="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" id="jcountcomment_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
"><?php if ($_smarty_tpl->tpl_vars['feed']->value['community_announcement_num_comments']>0){?> <?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_num_comments'];?>

                    Comments 
                <?php }?>
                </span>
                <div class="fb-share-button" id="share_button_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" data-href="<?php echo PageContext::$response->baseUrl;?>
announcement-detail/<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_alias'];?>
" data-title ="Shared on facebook" data-layout="button_count" data-desc="You can share data" data-url="<?php echo PageContext::$response->baseUrl;?>
newsfeed-detail/<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_alias'];?>
"></div>
                <a href="<?php echo PageContext::$response->baseUrl;?>
announcement-detail/<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_alias'];?>
" title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                
                </div>
            </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <!---------------Referral offer-------------------------------->
        <!--                                                <div class="jReferralOptionsClass referraltype_blk" id="jDivdisplayReferralOptions_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_id;?>
_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_announcement_id;?>
" style="display:none;">
            <h4>Please choose the preferred referral type</h4>
            <ul>
                <li><input type="radio" name="RefferalType_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_id;?>
" value="1" > Free Referral</li>
                <li><input type="radio" name="RefferalType_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_id;?>
" value="2" > Financial Incentive Based Referral</li>
                <li><input type="radio" name="RefferalType_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_id;?>
"  value="3"> Charitable Donation Based Referral</li>
                <li><input type="radio" name="RefferalType_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_id;?>
" value="4" > Voucher or Product Based Referral</li>
                <li><input type="button" class="jClickFeedReferralOffer sendref_offr_btn" aid="<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_announcement_id;?>
"  bid="<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_id;?>
" uid="<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_created_by;?>
" value="Send Referral Offer"></li>
            </ul>
        </div>-->
        <div class="mediapost_user-side-top clear jCommentDisplayDiv" id="jCommentBoxDisplayDiv_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" style="display:none">
            <p class="lead emoji-picker-container position_relatv">
                <div class="emotionsbtn_textblk">
                    <textarea class="form-control textarea-control jEmojiTextarea" id="watermark_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" rows="3" placeholder="Write your comment here..."></textarea>
                    <a class="upload_cambtn" id="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
>">
                    <label for="file_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" ><i class="fa fa-camera feed-camera"></i></label></a>
                    <input type="file" id="file_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" class="jimage_file" style="cursor: pointer;  display: none"/>
                    <input type="hidden" id="announcement_id" value="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
">
                </div>
                
            </p>
             <div class="msg_display" id="msg_display<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
"></div>
										 	<div class="clearfix"></div>
            <a id="postButton" ondblclick="this.disabled=true;" cid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_id'];?>
" cmid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['announcement_comment_id'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
"  class="btn yellow_btn fontupper pull-right jPostCommentButtonCommunity"> Post</a>
        </div>
        <div class="loading" id="loading_<?php echo $_smarty_tpl->tpl_vars['feedlist']->value['community_announcement_id'];?>
" style="display: none;">
            <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif" />
        </div>
        <div id="juploadimagepreview_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
"></div>
        <div class="clear commentblk jCommentDisplayDiv" id="jCommentDisplayDiv_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" style="display:none">
            <div id="posting_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
">
                <!------------------------Comments-------------------------------->
                <?php if (isset($_smarty_tpl->tpl_vars["val"])) {$_smarty_tpl->tpl_vars["val"] = clone $_smarty_tpl->tpl_vars["val"];
$_smarty_tpl->tpl_vars["val"]->value = 0; $_smarty_tpl->tpl_vars["val"]->nocache = null; $_smarty_tpl->tpl_vars["val"]->scope = 0;
} else $_smarty_tpl->tpl_vars["val"] = new Smarty_variable(0, null, 0);?>                                           
                <?php  $_smarty_tpl->tpl_vars['comments'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comments']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = PageContext::$response->newsFeed[$_smarty_tpl->tpl_vars['feed']->value['community_announcement_id']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comments']->key => $_smarty_tpl->tpl_vars['comments']->value){
$_smarty_tpl->tpl_vars['comments']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['comments']->key;
?>
                <?php if ($_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id!=''){?>
                <input type="hidden" id="val_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" value="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
<?php $_tmp6=ob_get_clean();?><?php echo count(PageContext::$response->newsFeed[$_tmp6]);?>
">
                   
                    <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['val']->value;?>
<?php $_tmp7=ob_get_clean();?><?php if (isset($_smarty_tpl->tpl_vars["val"])) {$_smarty_tpl->tpl_vars["val"] = clone $_smarty_tpl->tpl_vars["val"];
$_smarty_tpl->tpl_vars["val"]->value = $_tmp7+1; $_smarty_tpl->tpl_vars["val"]->nocache = null; $_smarty_tpl->tpl_vars["val"]->scope = 0;
} else $_smarty_tpl->tpl_vars["val"] = new Smarty_variable($_tmp7+1, null, 0);?>
                      <input type="text" id="hid_announcement_id_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
" class="hid_announcement_id" value="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" style="display: none;" >   
                        <input type="text" id="hid_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
" value="<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
" style="display: none;">
                        <div class="col-md-12 btm-mrg pad10p" id="jdivComment_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
">
                            <div class="row">
                                <div class="col-md-1">
                                    <span class="mediapost_pic">
                                        <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_alias;?>
">
                                        <?php if ($_smarty_tpl->tpl_vars['comments']->value->user_image!=''){?>
                                        <img class="ryt-mrg" src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_image;?>
">
                                        <?php }else{ ?>
                                        <img class="ryt-mrg" src="<?php echo PageContext::$response->userImagePath;?>
medium/member_noimg.jpg">
                                        <?php }?>
                                        </a>
                                    </span>
                                </div>
                                <div class="col-md-11">
                                    <span class="name"><a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_alias;?>
"><?php echo $_smarty_tpl->tpl_vars['comments']->value->Username;?>
 </a></span>
                                    <span class="jEmojiable"><?php echo $_smarty_tpl->tpl_vars['comments']->value->comment_content;?>
</span>
                                     <div class="clearfix"></div>
                                        <span class="new-text">

                                        <?php echo $_smarty_tpl->tpl_vars['comments']->value->commentDate;?>

                                    </span>
                                    <div class="commentimg_box">
                                        <?php if ($_smarty_tpl->tpl_vars['comments']->value->file_path!=''){?>
<!--                                        <img class="fancybox img-responsive center" src="<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['comments']->value->file_path;?>
">-->
                                        <ul class="portfolio" class="clearfix">
                                                <li><a href="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['comments']->value->file_path;?>
" title=""><img src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['comments']->value->file_path;?>
" alt=""></a></li>
                                        </ul>
                                        
                                        
                                        
                                        <?php }?>
                                    </div>
                                    <div class="mediapost_user-side1">
                                        <span rel="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['comments']->value->users;?>
 liked this comment" class="mediapost_links" id="jcountcommentlike_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"><?php if ($_smarty_tpl->tpl_vars['comments']->value->num_comment_likes>0){?><?php echo $_smarty_tpl->tpl_vars['comments']->value->num_comment_likes;?>
 Likes <?php }?></span>
                                        <input type="hidden" id="reply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
" value="<?php if ($_smarty_tpl->tpl_vars['comments']->value->num_replies>0){?><?php echo $_smarty_tpl->tpl_vars['comments']->value->num_replies;?>
 <?php }else{ ?> 0 <?php }?>">
                                        <span class="mediapost_links jShowReply" cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"><span id="jcountreply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"><?php if ($_smarty_tpl->tpl_vars['comments']->value->num_replies>0){?><?php echo $_smarty_tpl->tpl_vars['comments']->value->num_replies;?>
 Replies <?php }?></span></a></span>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="comment_sharelike_sec">
                                      
                                        <?php if ($_smarty_tpl->tpl_vars['comments']->value->user_id==PageContext::$response->sess_user_id||$_smarty_tpl->tpl_vars['comments']->value->community_created_by==PageContext::$response->sess_user_id){?>
                                        <a href="#" class="jFeedCommentDelete marg10right" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"><i class="fa fa-times"></i> Delete</a>
                                        <?php }?>
                                        <a href="#" class="jFeedCommentReply marg10right" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"><i class="fa fa-reply"></i> Reply</a>
                                        <a href="#" class="jFeedCommentLike <?php if ($_smarty_tpl->tpl_vars['comments']->value->LIKE_ID>0){?> liked <?php }?>" id="jlikeComment_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
" cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" >

                                            <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                            <span id="jlikedisplaycomment_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
">Like </span></a>
                                    </div>
                                    </span>
                                    <div id="jdivReply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
" class="jDisplayReply">
                                        <p class="lead emoji-picker-container position_relatv">
                                            <div class="emotionsbtn_textblk">
                                                <textarea placeholder="Write your reply here..."
                                                          class="form-control textarea-control jEmojiTextarea"
                                                          style="height:35px;"
                                                          id="watermark_<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"
                                                          class="watermark" name="watermark"></textarea>
                                                <a class="upload_cambtn" id="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
">
                                                    <label for="imagereply_file_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"><i
                                                            class="fa fa-camera feed-reply-camera"></i></label>
                                                    <div class="file_button"><input type="file"
                                                                                    id="imagereply_file_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"
                                                                                    cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"
                                                                                    class="jcommunityimagereply_file"
                                                                                    style="display:none;"/></div>
                                                </a>
                                            </div>
                                        </p>
                                        <div class="msg_display" id="msg_display<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"></div>
										 	<div class="clearfix"></div>
                                            <a ondblclick="this.disabled=true;" id="shareButton" cid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_id'];?>
" cmid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
"  class="btn yellow_btn fontupper pull-right jPostCommentButtonCommunity"> Post</a>
                                    </div>
                                    <div class="loader" id="imagereply_loader_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
">
                                        <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif" />
                                    </div>
                                    <div id="jDivReplyImagePreview_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"></div>
                                    <div id="postingReply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"></div>
                                </div>
                            </div> 
                        </div>
<?php }?>
<?php } ?>
<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_announcement_id;?>
<?php $_tmp8=ob_get_clean();?><?php if (!empty(PageContext::$response->Comments[$_tmp8])){?>
          <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_announcement_id;?>
<?php $_tmp9=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_announcement_id;?>
<?php $_tmp10=ob_get_clean();?><?php if (PageContext::$response->Page[$_tmp9]['totpages']!=PageContext::$response->Page[$_tmp10]['currentpage']){?>
                <div id="jDisplayMoreComments_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_announcement_id;?>
" ><a href="#" id="jMorecommentsButton" pid="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_announcement_id;?>
<?php $_tmp11=ob_get_clean();?><?php echo PageContext::$response->Page[$_tmp11]['currentpage'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_announcement_id;?>
">Load more comments</a></div>
            <?php }?>

<?php }?>


               
                <!------------------------EOF Comments-------------------------------->
            </div>
        </div>
        </div>                            
                                
             <?php }?>                   
                                
                                
             <?php if ($_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id']){?>                              
    <div class="whitebox" id="org_feed_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
">
            <div class="wid100per">
                <div class="postheadtablediv">
                <div class="post_left">
                    <span class="mediapost_pic">
                       <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['feed']->value['user_alias'];?>
" >
                        <?php if ($_smarty_tpl->tpl_vars['feed']->value['file_path']!=''){?>
                        <img src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['feed']->value['file_path'];?>
">
                        <?php }else{ ?>
                         <img src="<?php echo PageContext::$response->userImagePath;?>
medium/member_noimg.jpg">
                        <?php }?>
                        </a>
                    </span>
                </div>
                <div class="post_right">
                    <div class="posthead"><h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['feed']->value['user_alias'];?>
" ><?php echo $_smarty_tpl->tpl_vars['feed']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['feed']->value['user_lastname'];?>
</a>
                         <a href="<?php echo PageContext::$response->baseUrl;?>
page/<?php echo $_smarty_tpl->tpl_vars['feed']->value['business_alias'];?>
"><i class="fa fa-play" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['feed']->value['business_name'];?>
 </a></h4></div>
                    <div class="postsubhead">
                        
                        <div class="postsubhead_right">
                            <span> 
                                <?php if ($_smarty_tpl->tpl_vars['feed']->value['days']>0){?>
                                    <?php echo $_smarty_tpl->tpl_vars['feed']->value['days'];?>
 days ago
                                <?php }else{ ?>
                                    <?php if (isset($_smarty_tpl->tpl_vars["timesplit"])) {$_smarty_tpl->tpl_vars["timesplit"] = clone $_smarty_tpl->tpl_vars["timesplit"];
$_smarty_tpl->tpl_vars["timesplit"]->value = explode(":",$_smarty_tpl->tpl_vars['feed']->value['output_time']); $_smarty_tpl->tpl_vars["timesplit"]->nocache = null; $_smarty_tpl->tpl_vars["timesplit"]->scope = 0;
} else $_smarty_tpl->tpl_vars["timesplit"] = new Smarty_variable(explode(":",$_smarty_tpl->tpl_vars['feed']->value['output_time']), null, 0);?>
                                    <?php if ($_smarty_tpl->tpl_vars['timesplit']->value[0]>0){?>
                                        <?php echo ltrim($_smarty_tpl->tpl_vars['timesplit']->value[0],'0');?>
 hrs ago
                                    <?php }else{ ?>
                                        <?php if ($_smarty_tpl->tpl_vars['timesplit']->value[1]>0){?>
                                            <?php echo ltrim($_smarty_tpl->tpl_vars['timesplit']->value[1],'0');?>

                                                <?php if ($_smarty_tpl->tpl_vars['timesplit']->value[1]==1){?> min
                                                <?php }else{ ?> mins
                                                <?php }?> ago
                                        <?php }else{ ?>
                                            Just Now
                                        <?php }?>
                                    <?php }?>
                                <?php }?>
                            </span>
                        </div>
                    </div>
                </div>
                </div>
                <div class="clearfix"></div>
                <div class="postsubhead_left">
                    <?php if (isset($_smarty_tpl->tpl_vars["reg_exUrl"])) {$_smarty_tpl->tpl_vars["reg_exUrl"] = clone $_smarty_tpl->tpl_vars["reg_exUrl"];
$_smarty_tpl->tpl_vars["reg_exUrl"]->value = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/'; $_smarty_tpl->tpl_vars["reg_exUrl"]->nocache = null; $_smarty_tpl->tpl_vars["reg_exUrl"]->scope = 0;
} else $_smarty_tpl->tpl_vars["reg_exUrl"] = new Smarty_variable('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/', null, 0);?>
                <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['reg_exUrl']->value;?>
<?php $_tmp12=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_content'];?>
<?php $_tmp13=ob_get_clean();?><?php if (preg_match($_tmp12,$_tmp13,$_smarty_tpl->tpl_vars['url']->value)){?>
                  
                <div class="jEmojiable"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['reg_exUrl']->value;?>
<?php $_tmp14=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_content'];?>
<?php $_tmp15=ob_get_clean();?><?php echo preg_replace($_tmp14,"<a href='".((string)$_smarty_tpl->tpl_vars['url']->value[0])."'>".((string)$_smarty_tpl->tpl_vars['url']->value[0])."</a>",$_tmp15);?>
</div>

                <?php }else{ ?>

                <div class="jEmojiable"><?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_content'];?>
</div>

                <?php }?>
                </div>
                <div class="clearfix"></div>
            </div>


           <?php if ($_smarty_tpl->tpl_vars['feed']->value['organization_announcement_image_path']){?>
                                                            <div class="postpic">

<!--                                                                <img src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_image_path'];?>
" class="fancybox">-->
                                                                <ul class="portfolio" class="clearfix">
                                                                        <li><a href="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_image_path'];?>
" title=""><img src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_image_path'];?>
" alt=""></a></li>
                                                                </ul>
                                                            
                                                            </div>
            <?php }?>
            <div class="clearfix"></div>

            <div class="mediapost_user-side">
                <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-5">
                    <div class="likesec">
                    <div class="display_table_cell pad10_right">
                    <a href="#" class=" colorgrey_text jLikeFeedOrganization <?php if ($_smarty_tpl->tpl_vars['feed']->value['LIKE_ID']>0){?> liked <?php }?>" id="jlike_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" 
                       aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" cid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_id'];?>
">
                        <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                        <span id="jlikedisplay_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
">Like </span></a>
                    </div>
                     <div class="display_table_cell pad10_right">
                        <i class="fa fa-comments-o icn-fnt-size"></i>
                    <a href="#" id="jCommentButton_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" class="jOrgCommentButton colorgrey_text">Comment</a>
                    </div> 
                        <?php if ($_smarty_tpl->tpl_vars['feed']->value['organization_announcement_user_id']==PageContext::$response->sess_user_id){?> 
                    <div class="display_table_cell pad10_right">
                        <i class="fa fa-trash-o icn-fnt-size"></i>
                    <a href="#" id="jDeleteButton_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" class="jOrgDeletetButton colorgrey_text">Delete</a>
                    </div>
                       <?php }?>
                    </div>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-7">
                    <div class="sharefnt">
                                                                       <div id="org_like_users_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" class="like_users_div" style=" display: none;">
                      <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->newsFeedLikeUsers[$_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value){
$_smarty_tpl->tpl_vars['val']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['val']->key;
?> 
                      <span><?php echo $_smarty_tpl->tpl_vars['val']->value->user_name;?>
<br></span>
                      <?php } ?>  
                    </div>
                    <span class="mediapost_links jShowOrgLikeUsers" id="jcountlike_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
"><?php if ($_smarty_tpl->tpl_vars['feed']->value['organization_announcement_num_likes']>0){?> <?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_num_likes'];?>
 Likes <?php }?></span>
                    <span class="mediapost_links jOrgCommentButton count_class"  aid ="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" id="jcountcomment_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
"><?php if ($_smarty_tpl->tpl_vars['feed']->value['organization_announcement_num_comments']>0){?> <?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_num_comments'];?>

                        Comments 
                    <?php }?>
                    </span>
                    <div class="fb-share-button" id="share_button_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" data-href="<?php echo PageContext::$response->baseUrl;?>
announcement-detail/<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_alias'];?>
" data-title ="Shared on facebook" data-layout="button_count" data-desc="You can share data" data-url="<?php echo PageContext::$response->baseUrl;?>
newsfeed-detail/<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_alias'];?>
"></div>
                    <a href="<?php echo PageContext::$response->baseUrl;?>
announcement-detail/<?php echo $_smarty_tpl->tpl_vars['announcement']->value['organization_announcement_alias'];?>
" title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                    
                    </div>
                </div>
                </div>
                <div class="clearfix"></div>
           </div>

            <!---------------Referral offer-------------------------------->
<!--                                                <div class="jReferralOptionsClass referraltype_blk" id="jDivdisplayReferralOptions_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_id;?>
_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_announcement_id;?>
" style="display:none;">
                <h4>Please choose the preferred referral type</h4>
                <ul>
                    <li><input type="radio" name="RefferalType_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_id;?>
" value="1" > Free Referral</li>
                    <li><input type="radio" name="RefferalType_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_id;?>
" value="2" > Financial Incentive Based Referral</li>
                    <li><input type="radio" name="RefferalType_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_id;?>
"  value="3"> Charitable Donation Based Referral</li>
                    <li><input type="radio" name="RefferalType_<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_id;?>
" value="4" > Voucher or Product Based Referral</li>
                    <li><input type="button" class="jClickFeedReferralOffer sendref_offr_btn" aid="<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_announcement_id;?>
"  bid="<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_id;?>
" uid="<?php echo $_smarty_tpl->tpl_vars['announcement']->value->community_created_by;?>
" value="Send Referral Offer"></li>
                </ul>
            </div>
-->          


            <div class="mediapost_user-side-top clear jCommentDisplayDiv" id="jCommentBoxDisplayDiv_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" style="display:none">
                <p class="lead emoji-picker-container position_relatv">
                    <div class="emotionsbtn_textblk">
                        <textarea class="form-control textarea-control jEmojiTextarea" id="watermark_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" rows="3" placeholder="Write your comment here..."></textarea>
                        <a class="upload_cambtn" id="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
">
                        <label for="file_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" ><i class="fa fa-camera feed-camera"></i></label></a>
                        <input type="file" id="file_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" class="jorgimage_file" style="cursor: pointer;  display: none"/>
                        <input type="hidden" id="organization_announcement_id" value="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
">
                    </div>
                </p>
                <div class="msg_display" id="msg_display_<?php echo $_smarty_tpl->tpl_vars['feed']->value['announcement_comment_id'];?>
"></div>
				<div class="clearfix"></div>
                <a id="postButton" ondblclick="this.disabled=true;" cid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_id'];?>
" cmid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['announcement_comment_id'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
"  class="btn yellow_btn fontupper pull-right jPostCommentButtonOrganization"> Post</a>
            </div>
            <div class="loading" id="loading_<?php echo $_smarty_tpl->tpl_vars['feedlist']->value['community_announcement_id'];?>
" style="display: none;">
                <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif" />
            </div>
            <div id="juploadimagepreview_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
"></div>
            <div class="clear commentblk jCommentDisplayDiv" id="jCommentDisplayDiv_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" style="display:none">
                <div id="posting_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
">
                    <!------------------------Comments-------------------------------->
                    <?php if (isset($_smarty_tpl->tpl_vars["val"])) {$_smarty_tpl->tpl_vars["val"] = clone $_smarty_tpl->tpl_vars["val"];
$_smarty_tpl->tpl_vars["val"]->value = 0; $_smarty_tpl->tpl_vars["val"]->nocache = null; $_smarty_tpl->tpl_vars["val"]->scope = 0;
} else $_smarty_tpl->tpl_vars["val"] = new Smarty_variable(0, null, 0);?>
                                            
                    <?php  $_smarty_tpl->tpl_vars['comments'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comments']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = PageContext::$response->newsFeed[$_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comments']->key => $_smarty_tpl->tpl_vars['comments']->value){
$_smarty_tpl->tpl_vars['comments']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['comments']->key;
?>
                    <?php if ($_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id!=''){?>
                    <input type="hidden" id="val_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" value="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
<?php $_tmp16=ob_get_clean();?><?php echo count(PageContext::$response->newsFeed[$_tmp16]);?>
">
                       <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['val']->value;?>
<?php $_tmp17=ob_get_clean();?><?php if (isset($_smarty_tpl->tpl_vars["val"])) {$_smarty_tpl->tpl_vars["val"] = clone $_smarty_tpl->tpl_vars["val"];
$_smarty_tpl->tpl_vars["val"]->value = $_tmp17+1; $_smarty_tpl->tpl_vars["val"]->nocache = null; $_smarty_tpl->tpl_vars["val"]->scope = 0;
} else $_smarty_tpl->tpl_vars["val"] = new Smarty_variable($_tmp17+1, null, 0);?>
                           <input type="text" id="hid_announcement_id_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
" class="hid_announcement_id" value="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" style="display: none;" >   
                            <input type="text" id="hid_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
" value="<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
" style="display: none;">
                            <div class="col-md-12 btm-mrg pad10p" id="jdivComment_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
">
                                <div class="row">
                                    <div class="col-md-1">
                                        <span class="mediapost_pic">
                                            <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_alias;?>
" >
                                            <?php if ($_smarty_tpl->tpl_vars['comments']->value->user_image!=''){?>
                                            <img class="ryt-mrg" src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_image;?>
">
                                            <?php }else{ ?>
                                            <img class="ryt-mrg" src="<?php echo PageContext::$response->userImagePath;?>
medium/member_noimg.jpg">
                                            <?php }?>
                                            </a>
                                        </span>
                                    </div>
                                    <div class="col-md-11">
                                        <span class="name jEmojiable" ><a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_alias;?>
" ><?php echo $_smarty_tpl->tpl_vars['comments']->value->Username;?>
</a></span>
                                          <?php echo $_smarty_tpl->tpl_vars['comments']->value->comment_content;?>
</span>
                                         <div class="clearfix"></div>
                                            <span class="new-text">

                                            <?php echo $_smarty_tpl->tpl_vars['comments']->value->commentDate;?>

                                        </span>
                                        <div class="commentimg_box">
                                            <?php if ($_smarty_tpl->tpl_vars['comments']->value->file_path!=''){?>
<!--                                            <img class="fancybox img-responsive center" src="<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['comments']->value->file_path;?>
">-->
                                            <ul class="portfolio" class="clearfix">
                                                    <li><a href="<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['comments']->value->file_path;?>
" title=""><img src="<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['comments']->value->file_path;?>
" alt=""></a></li>
                                            </ul>
                                            
                                            
                                            <?php }?>
                                        </div>
                                        <div class="mediapost_user-side1">
                                   
                                            <span rel="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['comments']->value->users;?>
 liked this comment" class="mediapost_links" id="jcountcommentlike_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
"><?php if ($_smarty_tpl->tpl_vars['comments']->value->num_comment_likes>0){?> <?php echo $_smarty_tpl->tpl_vars['comments']->value->num_comment_likes;?>
 Likes <?php }?></span>
                                            <input type="hidden" id="reply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
" value="<?php if ($_smarty_tpl->tpl_vars['comments']->value->num_replies>0){?><?php echo $_smarty_tpl->tpl_vars['comments']->value->num_replies;?>
<?php }else{ ?> 0 <?php }?>">
                                            <span class="mediapost_links jOrgShowReply" cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
">
                                                <a href="#" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
">
                                                    <span id="jcountreply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
"><?php if ($_smarty_tpl->tpl_vars['comments']->value->num_replies>0){?><?php echo $_smarty_tpl->tpl_vars['comments']->value->num_replies;?>
 Replies <?php }?>
                                                    </span>
                                                </a>                                                    
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="comment_sharelike_sec">
                                            <?php if ($_smarty_tpl->tpl_vars['comments']->value->user_id==PageContext::$response->sess_user_id||$_smarty_tpl->tpl_vars['comments']->value->community_created_by==PageContext::$response->sess_user_id){?>
                                            <a href="#" class="jOrgFeedCommentDelete marg10right" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
"><i class="fa fa-times"></i> Delete</a>
                                            <?php }?>
                                            <a href="#" class="jFeedCommentReply marg10right" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
"><i class="fa fa-reply"></i> Reply</a>
                                            <a href="#" class="jOrgFeedCommentLike <?php if ($_smarty_tpl->tpl_vars['comments']->value->LIKE_ID>0){?>  liked <?php }?>" id="jlikeComment_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
" cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" >

                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                                <span id="jlikedisplaycomment_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
">Like </span></a>
                                        </div>
                                        </span>
                                        <div id="jdivReply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
" class="jDisplayReply">
                                            <p class="lead emoji-picker-container position_relatv">
                                                            <textarea placeholder="Write your reply here..."  class="form-control textarea-control jEmojiTextarea" style="height:35px;" id="watermark_<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
" class="watermark" name="watermark"  ></textarea>
                                            </p>
                                            <a class="upload_cambtn" id="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
" >
                                                <label for="imagereply_file_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
"><i class="fa fa-camera feed-reply-camera"></i></label>
                                                <div class="file_button"> <input type="file" id="imagereply_file_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
" cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
" class="jorgimagereply_file" style="display:none;" /></div></a>
                                                <a id="shareButton" ondblclick="this.disabled=true;" cid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_id'];?>
" cmid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['organization_announcement_id'];?>
"  class="btn yellow_btn fontupper pull-right jPostCommentButtonOrganization"> Post</a>
                                        </div>
                                        <div class="loader" id="imagereply_loader_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
">
                                            <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif" />
                                        </div>
                                        <div id="jDivReplyImagePreview_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
"></div>
                                        <div id="postingReply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->organization_announcement_comment_id;?>
"></div>
                                    </div>
                                </div> 
                            </div>
                            <?php }?>
                      <?php } ?>

<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['announcement']->value['organization_announcement_id'];?>
<?php $_tmp18=ob_get_clean();?><?php if (!empty(PageContext::$response->Comments[$_tmp18])){?>
                                                            <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['announcement']->value['organization_announcement_id'];?>
<?php $_tmp19=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['announcement']->value['organization_announcement_id'];?>
<?php $_tmp20=ob_get_clean();?><?php if (PageContext::$response->Page[$_tmp19]['totpages']!=PageContext::$response->Page[$_tmp20]['currentpage']){?>
                                                                <div id="jDisplayMoreComments_<?php echo $_smarty_tpl->tpl_vars['announcement']->value['organization_announcement_id'];?>
" ><a href="#" id="jMorecommentsButton" pid="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['announcement']->value['organization_announcement_id'];?>
<?php $_tmp21=ob_get_clean();?><?php echo PageContext::$response->Page[$_tmp21]['currentpage'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['announcement']->value['organization_announcement_id'];?>
">Load more comments</a></div>
                                                        <?php }?>
                                                        
                                                        <?php }?>

                    <!------------------------EOF Comments-------------------------------->
                </div>
            </div>
        </div>
     <?php }?>  
     
     <?php if ($_smarty_tpl->tpl_vars['feed']->value['news_feed_id']){?>    
    <div class="whitebox" id="news_feed_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
">
        <div class="wid100per">
            <div class="postheadtablediv">
                <div class="post_left">
                    <span class="mediapost_pic">
                        <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['feed']->value['user_alias'];?>
">
                        <?php if ($_smarty_tpl->tpl_vars['feed']->value['file_path']!=''){?>
                        <img src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['feed']->value['file_path'];?>
">
                        <?php }else{ ?>
                         <img src="<?php echo PageContext::$response->userImagePath;?>
/member_noimg.jpg">
                        <?php }?>
                        </a>
                    </span>
                </div>
                <div class="post_right">
                    <div class="posthead">
                        <h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['feed']->value['user_alias'];?>
"><?php echo $_smarty_tpl->tpl_vars['feed']->value['Username'];?>
</a> <span></span></h4>
                    </div>
                    <div class="postsubhead">
                        <div class="postsubhead_right">
                             <span>
                                <?php if ($_smarty_tpl->tpl_vars['feed']->value['days']>0){?>
                                    <?php echo $_smarty_tpl->tpl_vars['feed']->value['days'];?>
 days ago
                                <?php }else{ ?>
                                    <?php if (isset($_smarty_tpl->tpl_vars["timesplit"])) {$_smarty_tpl->tpl_vars["timesplit"] = clone $_smarty_tpl->tpl_vars["timesplit"];
$_smarty_tpl->tpl_vars["timesplit"]->value = explode(":",$_smarty_tpl->tpl_vars['feed']->value['output_time']); $_smarty_tpl->tpl_vars["timesplit"]->nocache = null; $_smarty_tpl->tpl_vars["timesplit"]->scope = 0;
} else $_smarty_tpl->tpl_vars["timesplit"] = new Smarty_variable(explode(":",$_smarty_tpl->tpl_vars['feed']->value['output_time']), null, 0);?>
                                    <?php if ($_smarty_tpl->tpl_vars['timesplit']->value[0]>0){?>
                                        <?php echo ltrim($_smarty_tpl->tpl_vars['timesplit']->value[0],'0');?>
 hrs ago
                                    <?php }else{ ?>
                                        <?php if ($_smarty_tpl->tpl_vars['timesplit']->value[1]>0){?>
                                            <?php echo ltrim($_smarty_tpl->tpl_vars['timesplit']->value[1],'0');?>

                                                <?php if ($_smarty_tpl->tpl_vars['timesplit']->value[1]==1){?> min
                                                <?php }else{ ?> mins
                                                <?php }?> ago
                                        <?php }else{ ?>
                                            Just Now
                                        <?php }?>
                                    <?php }?>
                                <?php }?>
                            </span>
                        </div>
                </div>
            </div>
            </div>
            <div class="clearfix"></div>
            <div class="postsubhead_left">
                <?php if (isset($_smarty_tpl->tpl_vars["url"])) {$_smarty_tpl->tpl_vars["url"] = clone $_smarty_tpl->tpl_vars["url"];
$_smarty_tpl->tpl_vars["url"]->value = ''; $_smarty_tpl->tpl_vars["url"]->nocache = null; $_smarty_tpl->tpl_vars["url"]->scope = 0;
} else $_smarty_tpl->tpl_vars["url"] = new Smarty_variable('', null, 0);?>
            <?php if (isset($_smarty_tpl->tpl_vars["reg_exUrl"])) {$_smarty_tpl->tpl_vars["reg_exUrl"] = clone $_smarty_tpl->tpl_vars["reg_exUrl"];
$_smarty_tpl->tpl_vars["reg_exUrl"]->value = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/'; $_smarty_tpl->tpl_vars["reg_exUrl"]->nocache = null; $_smarty_tpl->tpl_vars["reg_exUrl"]->scope = 0;
} else $_smarty_tpl->tpl_vars["reg_exUrl"] = new Smarty_variable('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/', null, 0);?>
            <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['reg_exUrl']->value;?>
<?php $_tmp22=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_comment'];?>
<?php $_tmp23=ob_get_clean();?><?php if (preg_match($_tmp22,$_tmp23,$_smarty_tpl->tpl_vars['url']->value)){?>
                  
                <div class="jEmojiable"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['reg_exUrl']->value;?>
<?php $_tmp24=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_comment'];?>
<?php $_tmp25=ob_get_clean();?><?php echo preg_replace($_tmp24,"<a href='".((string)$_smarty_tpl->tpl_vars['url']->value[0])."'>".((string)$_smarty_tpl->tpl_vars['url']->value[0])."</a>",$_tmp25);?>
</div>

            <?php }else{ ?>
  
                <div class="jEmojiable"><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_comment'];?>
</div>

            <?php }?>
            </div>
                
            <div class="clearfix"></div>
        </div>
      
            <?php if ($_smarty_tpl->tpl_vars['feed']->value['news_feed_image_name']){?>
            <?php if (isset($_smarty_tpl->tpl_vars["img"])) {$_smarty_tpl->tpl_vars["img"] = clone $_smarty_tpl->tpl_vars["img"];
$_smarty_tpl->tpl_vars["img"]->value = $_smarty_tpl->tpl_vars['feed']->value['news_feed_image_name']; $_smarty_tpl->tpl_vars["img"]->nocache = null; $_smarty_tpl->tpl_vars["img"]->scope = 0;
} else $_smarty_tpl->tpl_vars["img"] = new Smarty_variable($_smarty_tpl->tpl_vars['feed']->value['news_feed_image_name'], null, 0);?>
            <div class="postpic">
                <ul class="portfolio" class="clearfix">
                        <li><a href="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['img']->value;?>
" title=""><img src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['img']->value;?>
" alt=""></a></li>
                </ul>


            </div>
            <?php }?>
      
                                    <!----------------Statistics ------------------------>
                                    <div class="mediapost_user-side">
                                        <div class="row">
                                        <div class="col-sm-4 col-md-4 col-lg-5">
                                            <div class="likesec">
                                            <div class="display_table_cell pad10_right">
                                                <a href="#" class=" colorgrey_text jLikeNewsFeed <?php if ($_smarty_tpl->tpl_vars['feed']->value['news_feed_num_like']>0){?> liked <?php }?>" id="jlike_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" cid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
">
                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                                <span id="jlikedisplay_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
">Like </span></a>
                                            </div>
                                             <div class="display_table_cell pad10_right">
                                                <i class="fa fa-comments-o icn-fnt-size"></i>
                                                <a href="#" id="jCommentButton_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" class="jNewsfeedCommentButton colorgrey_text">Comment</a>
                                            </div>
                                                    <?php if ($_smarty_tpl->tpl_vars['feed']->value['news_feed_user_id']==PageContext::$response->sess_user_id){?>
                                            <div class="display_table_cell pad10_right">
                                                <i class="fa fa-trash-o icn-fnt-size"></i>
                                            <a href="#" id="jDeleteButton_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" class="jNewsfeedDeletetButton colorgrey_text">Delete</a>
                                            </div>
                                                <?php }?>
                                            </div>
                                        </div>
                                        <div class="col-sm-8 col-md-8 col-lg-7">
                       
                                            <div class="sharefnt">
<!--                                                                     <div id="feed_like_users_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" class="like_users_div" style=" display: none;">
                      <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->newsFeedLikeUsers[$_smarty_tpl->tpl_vars['feed']->value['news_feed_id']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value){
$_smarty_tpl->tpl_vars['val']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['val']->key;
?> 
                      <span><?php echo $_smarty_tpl->tpl_vars['val']->value->news_feed_like_user_name;?>
<br></span>
                      <?php } ?>  
                    </div>-->
                                            <span class="mediapost_links jShowFeedLikeUsers" id="jcountlike_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"><?php if ($_smarty_tpl->tpl_vars['feed']->value['news_feed_num_like']>0){?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_num_like'];?>
 Likes <?php }?></span>
                                            <span class="mediapost_links jNewsfeedCommentButton count_class"  aid ="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" id="jcountcomment_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"><?php if ($_smarty_tpl->tpl_vars['feed']->value['news_feed_num_comments']>0){?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_num_comments'];?>

                                                Comments 
                                                <?php }?>
                                            </span>
                                            <div class="fb-share-button" id="share_button_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" data-href="<?php echo PageContext::$response->baseUrl;?>
newsfeed-detail/<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_alias'];?>
" data-title ="Shared on facebook" data-layout="button_count" data-desc="You can share data" data-url="<?php echo PageContext::$response->baseUrl;?>
newsfeed-detail/<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_alias'];?>
"></div>
                                            <a href="<?php echo PageContext::$response->baseUrl;?>
newsfeed-detail/<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_alias'];?>
" title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                                            
                                            </div>
                                        </div>
                                        </div>
                                        <div class="clearfix"></div>
                                   </div>
                                    <!----------------End of  Statistics ------------------------>
                                    <!---------------Share buttons-------------------------------->
                                   
                                    <!---------------End of Share buttons-------------------------------->
                                    <div class="mediapost_user-side-top clear jCommentDisplayDiv" id="jNewsfeedCommentBoxDisplayDiv_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" style="display:none">
                                         <p class="lead emoji-picker-container">
                                         <div class="emotionsbtn_textblk">
                                            <textarea cid="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value['community_id'];?>
" cmid="" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" class="form-control textarea-control jtxtSearchProdAssign jEmojiTextarea"  id="watermark_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" rows="3" placeholder="Write your comment here..." ></textarea>
                                             <a class="upload_cambtn" id="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value['community_announcement_id'];?>
">
                                             <label for="file_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" ><i class="fa fa-camera feed-camera"></i></label></a>
                                                  <input type="file" id="file_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" cmid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_comment_id'];?>
" class="jfeedimageimage_file" style="cursor: pointer;  display: none"/>
                                              <input type="hidden" id="announcement_id" value="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value['community_announcement_id'];?>
">
                                        </div>
                                         
                                         </p>
                                         <div class="display_image_div">
                                             <div id="juploadcommentfeedimagepreview_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"></div>
                                        </div>
                                          <div class="msg_display" id="msg_display_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"></div>
										 	<div class="clearfix"></div>
<!--                                         <?php echo print_r(PageContext::$response->Comments);?>
-->
                                         <a id="postButton" ondblclick="this.disabled=true;" cid="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value['community_id'];?>
" cmid="" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"  class="btn yellow_btn fontupper pull-left jPostNewsfeedCommentButton"> Post</a>
                                       </div>
                                           <div class="clear commentblk jCommentDisplayDiv" id="jNewsfeedCommentDisplayDiv_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" style="display:none">
                                                <div id="posting_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
">
                                                   <!------------------------Comments-------------------------------->
                                                   <?php if (isset($_smarty_tpl->tpl_vars['val'])) {$_smarty_tpl->tpl_vars['val'] = clone $_smarty_tpl->tpl_vars['val'];
$_smarty_tpl->tpl_vars['val']->value = 0; $_smarty_tpl->tpl_vars['val']->nocache = null; $_smarty_tpl->tpl_vars['val']->scope = 0;
} else $_smarty_tpl->tpl_vars['val'] = new Smarty_variable(0, null, 0);?>
                                                
                                                    <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
<?php $_tmp26=ob_get_clean();?><?php  $_smarty_tpl->tpl_vars['comments'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comments']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = PageContext::$response->newsFeed[$_tmp26]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comments']->key => $_smarty_tpl->tpl_vars['comments']->value){
$_smarty_tpl->tpl_vars['comments']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['comments']->key;
?>
                                                   
                                                    <?php if ($_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id){?>
                                                    
                                                    <input type="hidden" id="val_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" value="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
<?php $_tmp27=ob_get_clean();?><?php echo count(PageContext::$response->newsFeed[$_tmp27]);?>
">
                                                        <div class="col-md-12 btm-mrg pad10p" id="jdivComment_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
">
                                                            <div class="row">
                                                                <div class="col-md-1">
                                                                    <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_alias;?>
">
                                                                    <?php if ($_smarty_tpl->tpl_vars['comments']->value->user_image!=''){?>
                                                                        <span class="mediapost_pic">
                                                                            <img class="ryt-mrg" src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_image;?>
">
                                                                        </span>
                                                                    <?php }?> 
                                                                     <?php if ($_smarty_tpl->tpl_vars['comments']->value->user_image==''){?>
                                                                        <span class="mediapost_pic">
                                                                            <img class="ryt-mrg" src="<?php echo PageContext::$response->userImagePath;?>
medium/member_noimg.jpg">
                                                                        </span>
                                                                     <?php }?>
                                                                     </a>
                                                                    
                                                                </div>
                                                                <div class="col-md-11">
                                                                    <span class="name"><a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_alias;?>
"><?php echo $_smarty_tpl->tpl_vars['comments']->value->Username;?>
 </a></span>
                                                                    <span class="jEmojiable"><?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comment_content;?>
</span>
                                                                    <br>
                                                                    <span class="new-text">
                                                                        
                                                                        <?php echo $_smarty_tpl->tpl_vars['comments']->value->commentDate;?>
 
                                                                    </span>
                                                                    <div class="commentimg_box">
                                                                        <?php if ($_smarty_tpl->tpl_vars['comments']->value->file_path!=''){?>
<!--                                                                          <img class="fancybox" src="<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['comments']->value->file_path;?>
">-->
                                                                            <ul class="portfolio" class="clearfix">
                                                                                    <li><a href="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['comments']->value->file_path;?>
" title=""><img src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['comments']->value->file_path;?>
" alt=""></a></li>
                                                                            </ul>
                                                                          <?php }?>
                                                                    </div>
                                                                    <div class="mediapost_user-side1">
                                                                        <span  rel="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['comments']->value->users;?>
 liked this comment" class="mediapost_links" id="jcountfeedcommentlike_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"><?php if ($_smarty_tpl->tpl_vars['comments']->value->num_comment_likes>0){?><?php echo $_smarty_tpl->tpl_vars['comments']->value->num_comment_likes;?>
 Likes <?php }?></span>
                                                                        <input type="hidden" id="reply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
" value="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
<?php $_tmp28=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
<?php $_tmp29=ob_get_clean();?><?php if (PageContext::$response->newsFeed[$_tmp28][$_tmp29]>0){?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
<?php $_tmp30=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
<?php $_tmp31=ob_get_clean();?><?php echo PageContext::$response->newsFeed[$_tmp30][$_tmp31];?>
<?php }else{ ?>0<?php }?>">
                                                                        <div class="mediapost_links jShowFeedReply" cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
">
                                                                            <a href="#" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
">
                                                                                <span id="jcountreply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
<?php $_tmp32=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
<?php $_tmp33=ob_get_clean();?><?php if (PageContext::$response->newsFeed[$_tmp32][$_tmp33]>0){?> <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
<?php $_tmp34=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
<?php $_tmp35=ob_get_clean();?><?php echo PageContext::$response->newsFeed[$_tmp34][$_tmp35];?>
 Replies <?php }?>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                        <div class="comment_sharelike_sec">
                                                                              <!--<?php echo print_r($_smarty_tpl->tpl_vars['comments']->value);?>
-->
                                                                        <?php if ($_smarty_tpl->tpl_vars['comments']->value->news_feed_comment_user_id==PageContext::$response->sess_user_id){?>
                                                                        <a href="#" class="jNewsFeedCommentDelete marg10right" aid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_id;?>
" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"><i class="fa fa-times"></i> Delete</a>
                                                                        <?php }?>
                                                                        <a href="#" class="jFeedCommentReply marg10right" aid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_id;?>
" cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"><i class="fa fa-reply"></i> Reply</a>
                                                                        <!--<a href="#" class="jFeedCommentLike" aid="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value['community_announcement_id'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
">Like</a>-->
                                                                         <a href="#" class="jNewsFeedCommentLike <?php if ($_smarty_tpl->tpl_vars['comments']->value->num_comment_likes>0){?> liked <?php }?>" id="jlikeComment_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
" cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
" aid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_id;?>
" >
                                                
                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i> 
                                                    <span id="jlikedisplaycomment_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
">Like </span></a>
                                                                        </div>
                                                                    </span>
                                                                    <div id="jdivReply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
" class="jDisplayReply">
                                                                        <p class="lead emoji-picker-container">
                                                                        <div class="emotionsbtn_textblk">
                                                                            <textarea placeholder="Write your reply here..."  class="form-control textarea-control jEmojiTextarea" style="height:35px;" id="watermark_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_id;?>
_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
" class="watermark" name="watermark"  ></textarea>
                                                                            <a class="upload_cambtn" id="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value['community_announcement_id'];?>
" >
                                                                                <label for="imagereply_file_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"><i class="fa fa-camera feed-reply-camera"></i></label>
                                                                               <div class="file_button"> <input type="file" id="imagereply_file_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
" cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
" class="jfeedimagereply_file" style="display:none;" /></div></a>
                                                                            </div>
                                                                        </p>
<!--                                                                        <a class="upload_cambtn" id="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value['community_announcement_id'];?>
" >
                                                                                <label for="imagereply_file_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"><i class="fa fa-camera feed-reply-camera"></i></label>
                                                                               <div class="file_button"> <input type="file" id="imagereply_file_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
" cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
" class="jfeedimagereply_file" style="display:none;" /></div></a>-->
                                                                       <div class="msg_display" id="msg_display_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"></div>
										 	<div class="clearfix"></div>
                                                                        <a id="shareButton" ondblclick="this.disabled=true;" cid="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value['community_id'];?>
" cmid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
" aid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_id;?>
"  class="btn yellow_btn fontupper pull-right jPostNewsfeedCommentButton"> Post</a>
                                                                    </div>
                                                                     <div class="loader" id="imagereply_loader_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
">
                                                                        <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif" />
                                                                    </div>
                                                                    <div id="jDivReplyImagePreview_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"></div>
                                                                    <div id="postingReply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"></div>
                                                                </div>
                                                            </div> 
                                                            
                                                        </div>
                                                    <?php }?>
                                                    <?php } ?>

                                                    <?php if (!empty(PageContext::$response->newsFeed[$_smarty_tpl->tpl_vars['feed']->value['news_feed_id']])){?>
                                                    <?php if (PageContext::$response->Page[$_smarty_tpl->tpl_vars['feed']->value['news_feed_id']]['totpages']!=PageContext::$response->Page[$_smarty_tpl->tpl_vars['feed']->value['news_feed_id']]['currentpage']){?>
                                                    <div id="jDisplayMoreComments_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" ><a href="#" id="jMorecommentsButton" pid="<?php echo PageContext::$response->Page[$_smarty_tpl->tpl_vars['feed']->value['news_feed_id']]['currentpage'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
">Load more comments</a></div>
                                                    <?php }?>
                                                    <?php }?>
                                                    
                                                    <!------------------------EOF Comments-------------------------------->
                                                </div>
                                    </div>
                                    
                               
</div>  
     <?php }?>
<?php } ?>
</div>
</div>
<div class="<?php echo $_smarty_tpl->tpl_vars['third_col']->value;?>
"> 
    
     <?php if (PageContext::$response->friend_display=='1'){?>
        <?php if (count(PageContext::$response->friend_display_list)>0){?>            
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-user" aria-hidden="true"></i> Friends <a  href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo PageContext::$response->sess_user_alias;?>
#Friends" class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul class="row frndboxlist">
                    <?php  $_smarty_tpl->tpl_vars['friendslist'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['friendslist']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->friend_display_list; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['friendslist']->key => $_smarty_tpl->tpl_vars['friendslist']->value){
$_smarty_tpl->tpl_vars['friendslist']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['friendslist']->key;
?>
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <a class="friendphotobox_blk" href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['friendslist']->value->user_alias;?>
">
                                <?php if ($_smarty_tpl->tpl_vars['friendslist']->value->file_path){?>
                                <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['friendslist']->value->file_path;?>
');">
                                </div>
                                <?php }else{ ?>
                                <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
medium/member_noimg.jpg');">
                                </div>
                                <?php }?>
                                <h4><?php echo ucfirst($_smarty_tpl->tpl_vars['friendslist']->value->user_firstname);?>
 <?php echo ucfirst($_smarty_tpl->tpl_vars['friendslist']->value->user_lastname);?>
</h4>
                            </a>
                        </li>
                      <?php } ?>
                </ul>
                </div>  
        <?php }?>
     <?php }?>
    
     <?php if (PageContext::$response->gallery_display=='1'){?>
        <?php if (count(PageContext::$response->gallery_display_list)>0){?>  
            <div class="whitebox">
                <div class="hdsec">
                    <h3><i class="fa fa-file-image-o" aria-hidden="true"></i> Photos <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo PageContext::$response->sess_user_alias;?>
#Gallery"  class="pull-right">More <i class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                </div>
                <ul id="portfolionewsfeed" class="row frndboxlist ">
                     <?php  $_smarty_tpl->tpl_vars['gallerylist'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gallerylist']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->gallery_display_list; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gallerylist']->key => $_smarty_tpl->tpl_vars['gallerylist']->value){
$_smarty_tpl->tpl_vars['gallerylist']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['gallerylist']->key;
?>
                     
<!--                    <li class="display_table_cell wid32per">
                        <a class="friendphotobox_blk" href="#">
                            <div class="friendphotobox" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->news_feed_image_name;?>
');">-->
                                
<!--                            <ul class="portfolio" class="clearfix">-->
                                    <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4 vert_align_top">
                                        <a class="friendphotobox_blk" href="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->news_feed_image_name;?>
" title="<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->news_feed_comment;?>
">
                                            <img src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->news_feed_image_name;?>
" alt="">
                                        </a>
                                    </li>
                   <?php } ?> 
                </ul>
                <!--<div class="sidead" style="background-image: url('<?php echo PageContext::$response->ImagePath;?>
sidead.jpg');"> </div>-->
            </div>  
          <?php }?>
     <?php }?>
</div>
<?php }else{ ?>
<div class='whitebox'>
    No Data Found
</div>
<?php }?>
</div>
</div>
</div>

<?php }} ?>