<?php /* Smarty version Smarty-3.1.13, created on 2017-01-05 11:05:44
         compiled from "project\modules\default\view\script\user\businesses_list.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:8644586e1a787cd8e5-77046248%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '01b56bea70b44dc605c7bb6f378dde2cfcb455bf' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\user\\businesses_list.tpl.php',
      1 => 1482834706,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8644586e1a787cd8e5-77046248',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'category' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_586e1a78a75480_38601643',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_586e1a78a75480_38601643')) {function content_586e1a78a75480_38601643($_smarty_tpl) {?><script>
    $(document).ready(function() {
        console.info('TOTAL_PAG '+TOTAL_PAG);
        var page = 1;
       	var _throttleTimer = null;
	    var _throttleDelay = 100;
        $(window ).scroll(function() {
            clearTimeout(_throttleTimer);
            _throttleTimer = setTimeout(function () {
            if($(window).scrollTop() + $(window).height() > $(document).height()-100) {
                if(page < TOTAL_PAG){
                    console.info('Current Page '+page);
                    page++;
                    console.info('Next Page '+page);
                    $.blockUI({ message: '' });
                    $.ajax({
                        type    : 'Post',
                        url     : mainUrl+'user/organisations_listajax/'+page,
                        cache   : false,
                        success :function(data){
                            console.info('HTML Data');
                            console.log(data);
                            $('.cls_section').append(data);
                            $('.listitem').fadeIn(3000);
                            $('.listitem').hide();
                        }
                    })
                }
            }
            },_throttleDelay);
	    });
    });
</script>

<div class="container">
    <section class="inbox_section whitebox">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">

                <h3>Pages <span class="round-search"><?php echo PageContext::$response->total;?>
</span>
                    <?php if (PageContext::$response->sess_user_id!=0){?>
                    <span class="addbusiness"><a
                            href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
add-page">Add Page</a></span>
                    <?php }?>
                </h3>
                <div class="businesslist_search_blk">
                    <div class="row">
                        <div class="col-sm-6 col-md-8 col-lg-8">
                            <input type="hidden" value="<?php echo PageContext::$response->sess_user_id;?>
" class="login_status"
                                   name="" id="login_status">
                            Sort by : A-Z <a
                                href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl; <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
pages/<?php echo PageContext::$response->category_id;?>
?sort=business_name&sortby=<?php echo PageContext::$response->order;?>
&searchtext=<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->searchtext; <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
&searchtype=<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->searchtype; <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
&page=<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->page; <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
"><?php echo PageContext::$response->business_name_sortorder;?>
</a>
                           
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PageContext::renderRegisteredPostActions('searchbox');<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                        </div>
                    </div>
                </div>
            </div>
            <section class="cls_section">
                <?php if (PageContext::$response->total>0){?>
                <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->list; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
$_smarty_tpl->tpl_vars['category']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['category']->key;
?>

                <div class="col-sm-6 col-md-6 col-lg-3">
                    <div class="cateforylist_items">
                        <div class="wid100p position_relatv">
                            <div class="pageslisting_img" style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['category']->value->file_path;?>
');">  </div>
                            <div class="pageslisting_actions">
                                    <?php if ($_smarty_tpl->tpl_vars['category']->value->business_created_by==PageContext::$response->sess_user_id){?>
                                    <span class="pull-left">
                                    <a class="edititem left marg5right"
                                       href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
edit-page/<?php echo $_smarty_tpl->tpl_vars['category']->value->business_id;?>
"><i
                                            class="fa fa-pencil"></i></a>
                                    <a class="deleteitem left" onclick="return deleteConfirm();"
                                       href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
index/delete_organization/<?php echo $_smarty_tpl->tpl_vars['category']->value->business_id;?>
"><i
                                            class="fa fa-trash"></i></a>
                                    </span>
                                    <?php }?>
                                    <div class="clearfix"></div>
                            </div>
                        </div>
                            <div class="wid100per display_block">
                                <a href="<?php echo PageContext::$response->baseUrl;?>
page/<?php echo $_smarty_tpl->tpl_vars['category']->value->business_alias;?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value->business_name;?>
</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                    </div>
                </div>

                <?php } ?>
                <?php }else{ ?>
                <div class="categorylist_row rownoborder">
                    <div class="col-sm-12 col-md-12 col-lg-12"><?php if (PageContext::$response->searchtype==''){?>No
                        pages added.<?php }else{ ?>No matches found.<?php }?>
                        <?php if (PageContext::$response->sess_user_id!=0&&PageContext::$response->sess_user_status!='I'){?>Click to <a href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->baseUrl;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
add-page">add a
                            page</a>. <?php }?>
                    </div>
                </div>
                <?php }?>
            </section>
        </div>
    </section>
</div>





<?php }} ?>