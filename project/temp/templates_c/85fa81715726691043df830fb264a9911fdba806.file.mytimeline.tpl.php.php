<?php /* Smarty version Smarty-3.1.13, created on 2017-02-03 04:29:22
         compiled from "project\modules\default\view\script\user\mytimeline.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:73855869ee5f54d686-67415914%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '85fa81715726691043df830fb264a9911fdba806' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\user\\mytimeline.tpl.php',
      1 => 1486039574,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '73855869ee5f54d686-67415914',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5869ee61031305_29385014',
  'variables' => 
  array (
    'feed' => 0,
    'first_col' => 0,
    'friendslist' => 0,
    'gallerylist' => 0,
    'second_col' => 0,
    'flag' => 0,
    'timesplit' => 0,
    'reg_exUrl' => 0,
    'url' => 0,
    'pimg' => 0,
    'val' => 0,
    'url_share' => 0,
    'feedlist' => 0,
    'comments' => 0,
    'country' => 0,
    'id' => 0,
    'lead' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5869ee61031305_29385014')) {function content_5869ee61031305_29385014($_smarty_tpl) {?><div id="fb-root"></div>

<script>

    $(document).ready(function () { //alert('hi');
        magnificPopupSingleFn();
        var page = 1;
        var _throttleTimer = null;
        var _throttleDelay = 100;
        $(window).scroll(function () {
            clearTimeout(_throttleTimer);
            _throttleTimer = setTimeout(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                    if (page < TOT_TIMELINE_PAGES) {
                        page++;
                        $.blockUI({message: ''});
                        var user_alias = $("#user_alias").val();
                        $.ajax({
                            type: 'Post',
                            url: mainUrl + 'user/timelineajax/' + page,
                            data: 'alias=' + user_alias,
                            success: function (data) {

                                var html = $.parseHTML(data);
                                enableEmojiOn(html);
                                $('.jfeeddisplay_div').append(html);

                                // parse facebook share button                               
                                FB.XFBML.parse();

                                magnificPopupSingleFn();
                            }
                        })
                    }
                }
            }, _throttleDelay);
        });
    });

</script>


<!-- Profile Timeline-------->
<div class="container">
    <div class="row">
        <?php if (PageContext::$response->user_name!=''){?>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="timelineheader">

                <div class="timelineheader_right">
                    <div class="cover-wrapper"
                         style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
timeline/<?php echo PageContext::$response->timelineimage;?>
');">
                        <div class="profilepic_timelineblk">
                            <div class="timelineheader_left">
                                <?php if (PageContext::$response->user_image!=''){?>
                                <div class="timeline_left_pic"
                                     style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
<?php echo PageContext::$response->user_image;?>
');"></div>
                                <?php }else{ ?>
                                <div class="timeline_left_pic"
                                     style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
member_noimg.jpg');">

                                </div>
                                <?php }?>
                                <h3><a href="#" id="jtimelinepic_user" target="_blank"><?php echo PageContext::$response->user_name;?>
</a></h3>
                                <?php if (PageContext::$response->alias==PageContext::$response->sess_user_alias){?>
                                <form name="frmtimelineimageupload" id="jfrmtimelineimageupload"
                                      action="<?php echo PageContext::$response->baseUrl;?>
user/timelineimageresize"
                                      enctype="multipart/form-data" method="post">
                                    <i class="fa fa-camera" id="jcamProfile"></i><span class="name"></span>
                                    <input type="file" name="timelinepic" id="file_profile" style="display:none;">
                                    <input type="hidden" value="<?php echo PageContext::$response->sess_user_id;?>
" id="user_id"
                                           name="user_id">
                                </form>
                                <?php if (PageContext::$response->user_image!=''){?>     
                                    <a href="#" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['community_announcement_id'];?>
" class="dltbtn jProfileDeletetButton colorgrey_text"><i class="fa fa-trash icn-fnt-size"></i></a>
                                <?php }?>
                               
                                <?php }?>
                            </div>
                        </div>
                        <div class="menufloattop">
                            <?php if (PageContext::$response->alias!=PageContext::$response->sess_user_alias&&PageContext::$response->sess_user_id>0){?>
                            <?php if (in_array(PageContext::$response->id,PageContext::$response->myfriends)){?>
                            <a href="#" name="reply_message" class="replymessage" id=""
                               onclick="replay_message(<?php echo PageContext::$response->friend_to;?>
,'');" title="Send Message"
                               src="#">
                                <span class="accept"><i class="fa fa-envelope-o"></i> Send Message</span>
                            </a>
                            <?php if (PageContext::$response->userDetails['user_friends_privacy']=='N'&&PageContext::$response->sess_user_id>0){?>
                            <a href="#" name="mutual_friends" id=""
                               onclick="mutual_friends(<?php echo PageContext::$response->id;?>
,<?php echo PageContext::$response->sess_user_id;?>
);">
                                <span class="accept pull-right"> Mutual Friends(<?php echo PageContext::$response->mutual_friend_count;?>
)</span>
                            </a>
                            <?php }?>
                            <?php }?>
                            <?php }?>
                            <?php if (PageContext::$response->sess_user_id>0&&PageContext::$response->id!=PageContext::$response->sess_user_id&&PageContext::$response->sess_user_status!='I'){?>
                            <?php if (in_array(PageContext::$response->id,PageContext::$response->myfriends)){?>

                            <a class=" jUnfriend" uid="<?php echo PageContext::$response->friend_to;?>
" href="#"
                               id="jUnfriend_<?php echo PageContext::$response->friend_to;?>
">
                                <span class="reject"><i class="fa fa-times"></i> Unfriend</span>
                            </a>
                            <?php }elseif(in_array(PageContext::$response->id,PageContext::$response->myinvitedfriends)){?>
                            <a class="friends_btn fright friend " uid="<?php echo PageContext::$response->id;?>
" href="#">
                                <span class="addfriend_icon jaddfriend"></span>
                                <span id="jFriend_<?php echo PageContext::$response->id;?>
">Friend request sent</span>
                            </a>
                            <?php }elseif(in_array(PageContext::$response->id,PageContext::$response->myPendingRequests)){?>
                            <a class="friends_btn fright friend jaddfriend1" uid="<?php echo PageContext::$response->id;?>
" href="#"
                               inviteid="<?php echo PageContext::$response->inviteId;?>
">
                                <span class="addfriend_icon "></span>
                                <span id="jFriend_<?php echo PageContext::$response->id;?>
">Accept Friend</span>
                            </a>
                            <?php }else{ ?>
                            <a class="friends_btn fright friend jaddfriend" uid="<?php echo PageContext::$response->id;?>
" href="#">
                                <span class="addfriend_icon jaddfriend"></span>
                                <span id="jFriend_<?php echo PageContext::$response->id;?>
">Add Friend</span>

                            </a>

                            <div class="loading right" id="loading_<?php echo PageContext::$response->id;?>
" style="display: none;">
                                <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif"/>
                            </div>

                            <?php }?>
                            <?php }?>
                        </div>

                        <?php if (PageContext::$response->alias==PageContext::$response->sess_user_alias){?>
                        <div class="timelineimage">
                            <form name="frmtimelineimageupload" id="jfrmtimelineimageupload"
                                  action="<?php echo PageContext::$response->baseUrl;?>
user/timelineimageresize"
                                  enctype="multipart/form-data" method="post">
                                <i class="fa fa-camera" id="jcam"></i><span class="name"></span>
                                <input type="file" name="timelinepic" id="jtimelinepic" style="display:none;">
                            </form>
                        </div>
                        <?php }?>
                    </div>
                    <div class="timeline_tabmenu">
                        <?php if (!in_array(PageContext::$response->id,PageContext::$response->myfriends)&&PageContext::$response->id!=PageContext::$response->sess_user_id&&PageContext::$response->sess_user_id>0){?>
                        <input type='hidden' id='timeline_view' value='no_friend'>
                        <input type='hidden' id='user_id' value=<?php echo PageContext::$response->id;?>
>
                        <input type='hidden' id='user_alias' value=<?php echo PageContext::$response->alias;?>
>
                        <?php }?>
                        <ul id="myTab" class="nav nav-tabs nav-justified">
                          
                            <li class="active jShowFeeds"><a href="#newsfeed" data-toggle="tab">News Feed</a>
                            </li>
                   

                            <li class="jShowProfile" alias="<?php echo PageContext::$response->alias;?>
"
                                uid="<?php echo PageContext::$response->sess_user_id;?>
"><a href="#myprofile" data-toggle="tab"><?php if (PageContext::$response->alias==PageContext::$response->sess_user_alias&&PageContext::$response->sess_user_alias!=''){?>My Profile<?php }else{ ?>Profile<?php }?></a>
                            </li>
                            <li class="jShowFriends jShowFriends_tab" alias="<?php echo PageContext::$response->alias;?>
"
                                uid="<?php echo PageContext::$response->sess_user_id;?>
"><a href="#tab-Friends" data-toggle="tab">Friends
                                    (<?php if (PageContext::$response->userDetails['user_friends_count']>0){?><?php echo PageContext::$response->userDetails['user_friends_count'];?>
<?php }else{ ?>0<?php }?>)</a>
                            </li>
                            <?php if (PageContext::$response->alias==PageContext::$response->sess_user_alias){?>
                            <li class="jShowFriends_tab jshowtimeline" alias="<?php echo PageContext::$response->alias;?>
"
                                uid="<?php echo PageContext::$response->sess_user_id;?>
"><a href="#tab-Friends" data-toggle="tab">Photos</a>
                            </li>
                            <?php }?>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="marg15col">
    <div class="container">
        <div class="row">
            <?php if (PageContext::$response->friend_display=='1'||PageContext::$response->gallery_display=='1'){?>
            <?php if (count(PageContext::$response->friend_display_list)>0||count(PageContext::$response->gallery_display_list)>0){?>
            <?php if (isset($_smarty_tpl->tpl_vars['first_col'])) {$_smarty_tpl->tpl_vars['first_col'] = clone $_smarty_tpl->tpl_vars['first_col'];
$_smarty_tpl->tpl_vars['first_col']->value = 'col-sm-4 col-md-3 col-lg-3'; $_smarty_tpl->tpl_vars['first_col']->nocache = null; $_smarty_tpl->tpl_vars['first_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['first_col'] = new Smarty_variable('col-sm-4 col-md-3 col-lg-3', null, 0);?>
            <?php if (isset($_smarty_tpl->tpl_vars['second_col'])) {$_smarty_tpl->tpl_vars['second_col'] = clone $_smarty_tpl->tpl_vars['second_col'];
$_smarty_tpl->tpl_vars['second_col']->value = 'col-sm-8 col-md-9 col-lg-9'; $_smarty_tpl->tpl_vars['second_col']->nocache = null; $_smarty_tpl->tpl_vars['second_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['second_col'] = new Smarty_variable('col-sm-8 col-md-9 col-lg-9', null, 0);?>


            <?php }else{ ?>
            <?php if (isset($_smarty_tpl->tpl_vars['first_col'])) {$_smarty_tpl->tpl_vars['first_col'] = clone $_smarty_tpl->tpl_vars['first_col'];
$_smarty_tpl->tpl_vars['first_col']->value = 'col-sm-12 col-md-12 col-lg-12'; $_smarty_tpl->tpl_vars['first_col']->nocache = null; $_smarty_tpl->tpl_vars['first_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['first_col'] = new Smarty_variable('col-sm-12 col-md-12 col-lg-12', null, 0);?>
            <?php if (isset($_smarty_tpl->tpl_vars['second_col'])) {$_smarty_tpl->tpl_vars['second_col'] = clone $_smarty_tpl->tpl_vars['second_col'];
$_smarty_tpl->tpl_vars['second_col']->value = 'col-sm-12 col-md-12 col-lg-12'; $_smarty_tpl->tpl_vars['second_col']->nocache = null; $_smarty_tpl->tpl_vars['second_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['second_col'] = new Smarty_variable('col-sm-12 col-md-12 col-lg-12', null, 0);?>


            <?php }?>
            <?php }else{ ?>
            <?php if (isset($_smarty_tpl->tpl_vars['first_col'])) {$_smarty_tpl->tpl_vars['first_col'] = clone $_smarty_tpl->tpl_vars['first_col'];
$_smarty_tpl->tpl_vars['first_col']->value = 'col-sm-12 col-md-12 col-lg-12'; $_smarty_tpl->tpl_vars['first_col']->nocache = null; $_smarty_tpl->tpl_vars['first_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['first_col'] = new Smarty_variable('col-sm-12 col-md-12 col-lg-12', null, 0);?>
            <?php if (isset($_smarty_tpl->tpl_vars['second_col'])) {$_smarty_tpl->tpl_vars['second_col'] = clone $_smarty_tpl->tpl_vars['second_col'];
$_smarty_tpl->tpl_vars['second_col']->value = 'col-sm-12 col-md-12 col-lg-12'; $_smarty_tpl->tpl_vars['second_col']->nocache = null; $_smarty_tpl->tpl_vars['second_col']->scope = 0;
} else $_smarty_tpl->tpl_vars['second_col'] = new Smarty_variable('col-sm-12 col-md-12 col-lg-12', null, 0);?>

            <?php }?>
            <div class="<?php echo $_smarty_tpl->tpl_vars['first_col']->value;?>
">
                <?php if (PageContext::$response->friend_display=='1'){?>
                <?php if (count(PageContext::$response->friend_display_list)>0){?>
                <div class="whitebox">
                    <div class="hdsec">
                        <h3><i class="fa fa-user" aria-hidden="true"></i> Friends <a href="#" id="jMorephotos"
                                                                                     class="pull-right">More <i
                                    class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                    </div>
                    <ul class="row frndboxlist">
                        <?php  $_smarty_tpl->tpl_vars['friendslist'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['friendslist']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->friend_display_list; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['friendslist']->key => $_smarty_tpl->tpl_vars['friendslist']->value){
$_smarty_tpl->tpl_vars['friendslist']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['friendslist']->key;
?>
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <a class="friendphotobox_blk"
                               href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['friendslist']->value->user_alias;?>
">
                                <?php if ($_smarty_tpl->tpl_vars['friendslist']->value->file_path){?>
                                <div class="friendphotobox"
                                     style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['friendslist']->value->file_path;?>
');">
                                </div>
                                <?php }else{ ?>
                                <div class="friendphotobox"
                                     style="background-image: url('<?php echo PageContext::$response->userImagePath;?>
medium/member_noimg.jpg');">
                                </div>
                                <?php }?>
                                <h4><?php echo ucfirst($_smarty_tpl->tpl_vars['friendslist']->value->user_firstname);?>
 <?php echo ucfirst($_smarty_tpl->tpl_vars['friendslist']->value->user_lastname);?>
</h4>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <?php }?>
                <?php }?>

                <?php if (in_array(PageContext::$response->id,PageContext::$response->myfriends)||PageContext::$response->id==PageContext::$response->sess_user_id){?>
                <?php if (PageContext::$response->gallery_display=='1'){?>
                <?php if (count(PageContext::$response->gallery_display_list)>0){?>
                <div class="whitebox">
                    <div class="hdsec">
                        <h3><i class="fa fa-file-image-o" aria-hidden="true"></i> Photos <a href="#" id="jMoreGallery"
                                                                                            class="pull-right">More <i
                                    class="fa fa-angle-right" aria-hidden="true"></i></a></h3>
                    </div>
                    <ul id="portfolio_timeline" class="row frndboxlist">
                        <?php  $_smarty_tpl->tpl_vars['gallerylist'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gallerylist']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->gallery_display_list; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gallerylist']->key => $_smarty_tpl->tpl_vars['gallerylist']->value){
$_smarty_tpl->tpl_vars['gallerylist']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['gallerylist']->key;
?>
                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4 vert_align_top"><a class="friendphotobox_blk"
                                                                                  href="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->news_feed_image_name;?>
"
                                                                                  title="<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->news_feed_comment;?>
"><img
                                    src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['gallerylist']->value->news_feed_image_name;?>
"
                                    alt=""></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php }?>
                <?php }?>
                <?php }?>
            </div>
            <div class="<?php echo $_smarty_tpl->tpl_vars['second_col']->value;?>
 jdisplay_div">
                <?php if (PageContext::$response->sess_user_id>0&&PageContext::$response->id==PageContext::$response->sess_user_id){?>
                <form role="form" id="logoform" method="post" action="" enctype="multipart/form-data">
                <div class="whitebox jpost_comment_div">
                    <div class="emotionsbtn_textblk">
                        <textarea placeholder="Post your comments.." name="newsfeed_comment" id="newsfeed_comment"
                                  class="jEmojiTextarea"></textarea>
                    </div>
                    <div class="wid100 pad5top">
                        <div class="display_image_div">
                            <div id="juploadimagepreview"></div>
                        </div>
                        <div class="pull-left">
                            <a href="#" class="camicon"><label for="file_feed"><i class="fa fa-camera"
                                                                                  aria-hidden="true"></i></label></a>
                            <input type="file" id="file_feed" class="" onchange="previewFile1()"
                                   style="cursor: pointer;  display: none"/>
                        </div>
                        <div class="pull-right">
                               <input type="hidden" id="image_id" value="">
                            <input type="button" onclick="postNewsFeed()" value="Post" class="btn primary post_btn">
                            <div class="loader" id="post_feed_loader" style="display: none;">
                                <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif"/>
                            </div>
                        </div>
                <div class="profile-bnr">
                            <img class="img img-responsive center" src="<?php if (PageContext::$response->campaignDetails->voucher_image_name!=''){?><?php echo PageContext::$response->userImagePath;?>
<?php echo PageContext::$response->campaignDetails->voucher_image_name;?>
<?php }?>" height="50">
                            </div>
                        <div class="clearfix"></div>
                        <div id="jerrordiv" style="color:#f00"></div>
                    </div>
                </div>
                    </form>
                <?php }?>
                <div class='jfeeddisplay_div'>
                    <?php  $_smarty_tpl->tpl_vars['feed'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['feed']->_loop = false;
 $_from = PageContext::$response->news_feed1; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['feed']->key => $_smarty_tpl->tpl_vars['feed']->value){
$_smarty_tpl->tpl_vars['feed']->_loop = true;
?>

                    <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['flag']->value;?>
<?php $_tmp1=ob_get_clean();?><?php if (isset($_smarty_tpl->tpl_vars['flag'])) {$_smarty_tpl->tpl_vars['flag'] = clone $_smarty_tpl->tpl_vars['flag'];
$_smarty_tpl->tpl_vars['flag']->value = $_tmp1+1; $_smarty_tpl->tpl_vars['flag']->nocache = null; $_smarty_tpl->tpl_vars['flag']->scope = 0;
} else $_smarty_tpl->tpl_vars['flag'] = new Smarty_variable($_tmp1+1, null, 0);?>
                    <div class="whitebox timelinebox" id="news_feed_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
">
                        <div class="wid100per">
                            <div class="postheadtablediv">
                                <div class="post_left">
                                    <span class="mediapost_pic">
                                        <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['feed']->value['user_alias'];?>
">
                                        <?php if ($_smarty_tpl->tpl_vars['feed']->value['file_path']!=''){?>
                                        <img class='timeline_feed_user_image'
                                            src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['feed']->value['file_path'];?>
">
                                        <?php }else{ ?>
                                        <img class='timeline_feed_user_image' src="<?php echo PageContext::$response->userImagePath;?>
medium/member_noimg.jpg">
                                        <?php }?>
                                        </a>
                                    </span>
                                </div>
                                <div class="post_right">
                                    <div class="posthead"><h4 class="media-heading"><a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['feed']->value['user_alias'];?>
"><?php echo $_smarty_tpl->tpl_vars['feed']->value['Username'];?>
</a>
                                            <span></span></h4></div>
                                    <div class="postsubhead">
                                        <div class="postsubhead_right"><span><?php if ($_smarty_tpl->tpl_vars['feed']->value['days']>0){?> <?php echo $_smarty_tpl->tpl_vars['feed']->value['days'];?>
 days ago 
                                                <?php }else{ ?> <?php if (isset($_smarty_tpl->tpl_vars["timesplit"])) {$_smarty_tpl->tpl_vars["timesplit"] = clone $_smarty_tpl->tpl_vars["timesplit"];
$_smarty_tpl->tpl_vars["timesplit"]->value = explode(":",$_smarty_tpl->tpl_vars['feed']->value['output_time']); $_smarty_tpl->tpl_vars["timesplit"]->nocache = null; $_smarty_tpl->tpl_vars["timesplit"]->scope = 0;
} else $_smarty_tpl->tpl_vars["timesplit"] = new Smarty_variable(explode(":",$_smarty_tpl->tpl_vars['feed']->value['output_time']), null, 0);?>
                                                <?php if ($_smarty_tpl->tpl_vars['timesplit']->value[0]>0){?><?php echo ltrim($_smarty_tpl->tpl_vars['timesplit']->value[0],'0');?>
 hrs ago
                                                <?php }else{ ?>
                                                <?php if ($_smarty_tpl->tpl_vars['timesplit']->value[1]>0){?>
                                                <?php echo ltrim($_smarty_tpl->tpl_vars['timesplit']->value[1],'0');?>
<?php if ($_smarty_tpl->tpl_vars['timesplit']->value[1]==1){?> min<?php }else{ ?> mins<?php }?> ago
                                                <?php }else{ ?>
                                                Just Now
                                                <?php }?>
                                                <?php }?>
                                                <?php }?></span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="postsubhead_left">
                                <?php if (isset($_smarty_tpl->tpl_vars["reg_exUrl"])) {$_smarty_tpl->tpl_vars["reg_exUrl"] = clone $_smarty_tpl->tpl_vars["reg_exUrl"];
$_smarty_tpl->tpl_vars["reg_exUrl"]->value = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/'; $_smarty_tpl->tpl_vars["reg_exUrl"]->nocache = null; $_smarty_tpl->tpl_vars["reg_exUrl"]->scope = 0;
} else $_smarty_tpl->tpl_vars["reg_exUrl"] = new Smarty_variable('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/', null, 0);?>

                                <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['reg_exUrl']->value;?>
<?php $_tmp2=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_comment'];?>
<?php $_tmp3=ob_get_clean();?><?php if (preg_match($_tmp2,$_tmp3,$_smarty_tpl->tpl_vars['url']->value)){?>

                                    <div class="jEmojiable"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['reg_exUrl']->value;?>
<?php $_tmp4=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_comment'];?>
<?php $_tmp5=ob_get_clean();?><?php echo preg_replace($_tmp4,"<a href='".((string)$_smarty_tpl->tpl_vars['url']->value[0])."'>".((string)$_smarty_tpl->tpl_vars['url']->value[0])."</a>",$_tmp5);?>
</div>

                                <?php }else{ ?>

                                    <div class="jEmojiable"><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_comment'];?>
</div>

                                <?php }?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <a href="#">
                            <?php if ($_smarty_tpl->tpl_vars['feed']->value['news_feed_image_name']){?>
                            <?php if (isset($_smarty_tpl->tpl_vars["pimg"])) {$_smarty_tpl->tpl_vars["pimg"] = clone $_smarty_tpl->tpl_vars["pimg"];
$_smarty_tpl->tpl_vars["pimg"]->value = $_smarty_tpl->tpl_vars['feed']->value['news_feed_image_name']; $_smarty_tpl->tpl_vars["pimg"]->nocache = null; $_smarty_tpl->tpl_vars["pimg"]->scope = 0;
} else $_smarty_tpl->tpl_vars["pimg"] = new Smarty_variable($_smarty_tpl->tpl_vars['feed']->value['news_feed_image_name'], null, 0);?>
                            <div class="postpic">

                                <ul class="portfolio" class="clearfix main_gallary">
                                    <li><a href="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['pimg']->value;?>
" title=""><img
                                                src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['pimg']->value;?>
" alt=""></a>
                                    </li>
                                </ul>

                            </div>
                            <?php }?>
                        </a>
                        <!----------------Statistics ------------------------>
                        <?php if (PageContext::$response->sess_user_id>0){?>
                        <div class="mediapost_user-side">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="likesec">
                                        <div class="display_table_cell pad10_right">
                                            <a href="#"
                                               class=" colorgrey_text jLikeNewsFeed <?php if ($_smarty_tpl->tpl_vars['feed']->value['news_feed_num_like']>0){?> liked <?php }?>"
                                               id="jlike_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" cid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
">
                                                <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                                <span id="jlikedisplay_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
">Like </span></a>
                                        </div>
                                        <div class="display_table_cell pad10_right">
                                            <i class="fa fa-comments-o icn-fnt-size"></i>
                                            <a href="#" id="jCommentButton_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"
                                               aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"
                                               class="jNewsfeedCommentButton colorgrey_text">Comment</a>
                                        </div>
                                        <?php if (PageContext::$response->sess_user_id==$_smarty_tpl->tpl_vars['feed']->value['news_feed_user_id']){?>
                                        <div class="display_table_cell pad10_right">
                                            <i class="fa fa-trash-o icn-fnt-size"></i>
                                            <a href="#" id="jDeleteButton_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"
                                               aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"
                                               class="jNewsfeedDeletetButton colorgrey_text">Delete</a>
                                        </div>
                                        <?php }?>
                                    </div>
                                </div>
                                <div class="col-sm-8 col-md-8 col-lg-8">
                                    <div class="shareitemsblks">
                                        <div id="feed_like_users_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" class="like_users_div" style=" display: none;">
                      <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->newsFeedLikeUsers[$_smarty_tpl->tpl_vars['feed']->value['news_feed_id']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value){
$_smarty_tpl->tpl_vars['val']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['val']->key;
?> 
                      <span><?php echo $_smarty_tpl->tpl_vars['val']->value->news_feed_like_user_name;?>
<br></span>
                      <?php } ?>  
                    </div>
                                        <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 $url_share=urlencode(PageContext::$response->baseUrl.'newsfeed-detail/');<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                                        <span class="mediapost_links jShowFeedLikeUsers" id="jcountlike_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"><?php if ($_smarty_tpl->tpl_vars['feed']->value['news_feed_num_like']>0){?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_num_like'];?>
 Likes <?php }?></span>
                                    <span class="mediapost_links jNewsfeedCommentButton count_class"
                                          aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" id="jcountcomment_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"><?php if ($_smarty_tpl->tpl_vars['feed']->value['news_feed_num_comments']>0){?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_num_comments'];?>

                                        Comments 
                                        <?php }?>
                                    </span>
                                        <div class="fb-share-button"
                                             data-href="<?php echo PageContext::$response->baseUrl;?>
newsfeed-detail/<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_alias'];?>
"
                                             data-layout="button_count" data-mobile-iframe="true"><a
                                                class="fb-xfbml-parse-ignore" target="_blank"
                                                href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_smarty_tpl->tpl_vars['url_share']->value;?>
<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_alias'];?>
;src=sdkpreparse">Share</a>
                                        </div>
                                        <a href="<?php echo PageContext::$response->baseUrl;?>
newsfeed-detail/<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_alias'];?>
"
                                           title="Share on twitter" class="twitter-timeline" target="_blank">Tweet</a>

                                    </div>
                                </div>
                            </div>
                        </div>
<?php }?>
                        <div class="mediapost_user-side-top clear jCommentDisplayDiv"
                             id="jNewsfeedCommentBoxDisplayDiv_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" style="display:none">
                            <p class="lead emoji-picker-container">
                            <div class="emotionsbtn_textblk">
                                <textarea class="jEmojiTextarea form-control textarea-control"
                                          id="watermark_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" rows="3"
                                          placeholder="Write your comment here..."></textarea>
                                <a class="upload_cambtn" id="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value->community_announcement_id;?>
">
                                    <label for="file_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"><i
                                            class="fa fa-camera feed-camera"></i></label></a>
                                <input type="file" id="file_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"
                                       cmid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_comment_id'];?>
" class="jfeedimageimage_file"
                                       style="cursor: pointer;  display: none"/>
                                <input type="hidden" id="announcement_id"
                                       value="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value->community_announcement_id;?>
">
                            </div>

                            </p>
                            <div class="display_image_div">
                                <div id="juploadcommentfeedimagepreview_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"></div>
                            </div>
                            <div class="msg_display" id="msg_display_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"></div>
                            <div class="clearfix"></div>
                            <a id="postButton" cid="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value->community_id;?>
" cmid="" aid="<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"
                               class="btn yellow_btn fontupper pull-right jPostNewsfeedCommentButton"> Post</a>
                        </div>

                        <div class="clear commentblk jCommentDisplayDiv"
                             id="jNewsfeedCommentDisplayDiv_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
" style="display:none">
                            <div id="posting_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
">
                                <!------------------------Comments-------------------------------->
                                <?php if (isset($_smarty_tpl->tpl_vars['val'])) {$_smarty_tpl->tpl_vars['val'] = clone $_smarty_tpl->tpl_vars['val'];
$_smarty_tpl->tpl_vars['val']->value = 0; $_smarty_tpl->tpl_vars['val']->nocache = null; $_smarty_tpl->tpl_vars['val']->scope = 0;
} else $_smarty_tpl->tpl_vars['val'] = new Smarty_variable(0, null, 0);?>
                                <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
<?php $_tmp6=ob_get_clean();?><?php if (count(PageContext::$response->newsFeed[$_tmp6])>0){?>
                                <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
<?php $_tmp7=ob_get_clean();?><?php  $_smarty_tpl->tpl_vars['comments'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comments']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->newsFeed[$_tmp7]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comments']->key => $_smarty_tpl->tpl_vars['comments']->value){
$_smarty_tpl->tpl_vars['comments']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['comments']->key;
?>
                                <?php if ($_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id){?>
                                <input type="hidden" id="val_<?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
"
                                       value="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
<?php $_tmp8=ob_get_clean();?><?php echo count(PageContext::$response->newsFeed[$_tmp8]);?>
">
                                <div class="col-md-12 btm-mrg pad10p" id="jdivComment_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_alias;?>
">
                                                <?php if ($_smarty_tpl->tpl_vars['comments']->value->user_image!=''){?>
                                            <span class="mediapost_pic">
                                                <img class="ryt-mrg"
                                                     src="<?php echo PageContext::$response->userImagePath;?>
<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_image;?>
">
                                            </span>
                                                <?php }?>
                                                <?php if ($_smarty_tpl->tpl_vars['comments']->value->user_image==''){?>
                                            <span class="mediapost_pic">
                                                <img class="ryt-mrg"
                                                     src="<?php echo PageContext::$response->userImagePath;?>
medium/member_noimg.jpg">
                                            </span>
                                                <?php }?>
                                            </a>
                                        </div>
                                        <div class="col-md-11">
                                            <span class="name"> <a href="<?php echo PageContext::$response->baseUrl;?>
timeline/<?php echo $_smarty_tpl->tpl_vars['comments']->value->user_alias;?>
"><?php echo $_smarty_tpl->tpl_vars['comments']->value->Username;?>
</a> </span>
                                            <span class="jEmojiable"><?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comment_content;?>
</span>
                                            <span class="new-text">

                                                <?php echo $_smarty_tpl->tpl_vars['comments']->value->commentDate;?>
 
                                            </span>
                                            <div class="commentimg_box">
                                                <?php if ($_smarty_tpl->tpl_vars['comments']->value->file_path!=''){?>
                                                <img class="image_zoom"
                                                     src="<?php echo PageContext::$response->userImagePath;?>
medium/<?php echo $_smarty_tpl->tpl_vars['comments']->value->file_path;?>
">
                                                <?php }?>
                                            </div>
                                            <div class="mediapost_user-side1">
                                                <span rel="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['comments']->value->users;?>
 liked this comment" class="mediapost_links"
                                                      id="jcountfeedcommentlike_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"><?php if ($_smarty_tpl->tpl_vars['comments']->value->num_comment_likes>0){?><?php echo $_smarty_tpl->tpl_vars['comments']->value->num_comment_likes;?>
 Likes <?php }?></span>
                                                <input type="hidden" id="reply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"
                                                       value="<?php if ($_smarty_tpl->tpl_vars['comments']->value->num_replies>0){?><?php echo $_smarty_tpl->tpl_vars['comments']->value->num_replies;?>
<?php }else{ ?>0<?php }?>">
                                                <div class="mediapost_links " cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
"
                                                     id="jcountreply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
">
                                                    <a href="#" class="jShowFeedReply"
                                                       id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
">
                                                        <span
                                                            id="jcountreply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
<?php $_tmp9=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
<?php $_tmp10=ob_get_clean();?><?php if (PageContext::$response->newsFeed[$_tmp9][$_tmp10]>0){?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['feed']->value['news_feed_id'];?>
<?php $_tmp11=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
<?php $_tmp12=ob_get_clean();?><?php echo PageContext::$response->newsFeed[$_tmp11][$_tmp12];?>
 Replies <?php }?>
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="comment_sharelike_sec">
                                                <?php if ($_smarty_tpl->tpl_vars['comments']->value->news_feed_comment_user_id==PageContext::$response->sess_user_id){?>
                                                <a href="#" class="jNewsFeedCommentDelete marg10right"
                                                   aid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_id;?>
"
                                                   id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"><i class="fa fa-times"></i>
                                                    Delete</a>
                                                <?php }?>
                                                <a href="#" class="jFeedCommentReply marg10right"
                                                   aid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_id;?>
"
                                                   cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"
                                                   id="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"><i class="fa fa-reply"></i>
                                                    Reply</a>
                                                <a href="#"
                                                   class="jNewsFeedCommentLike <?php if ($_smarty_tpl->tpl_vars['comments']->value->num_comment_likes>0){?> liked <?php }?>"
                                                   id="jlikeComment_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"
                                                   cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"
                                                   aid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_id;?>
">

                                                    <i class="fa fa-thumbs-o-up icn-fnt-size"></i>
                                                    <span id="jlikedisplaycomment_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
">Like </span></a>
                                            </div>

                                            <div id="jdivReply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"
                                                 class="jDisplayReply">
                                                <p class="lead emoji-picker-container">
                                                    <div class="emotionsbtn_textblk">
                                                        <textarea placeholder="Write your reply here..."
                                                                  class="form-control textarea-control jEmojiTextarea"
                                                                  style="height:35px;"
                                                                  id="watermark_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_id;?>
_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"
                                                                  class="watermark" name="watermark">
                                                        </textarea>
                                                        <a class="upload_cambtn"
                                                           id="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value['community_announcement_id'];?>
">
                                                            <label for="imagereply_file_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"><i
                                                                    class="fa fa-camera feed-reply-camera"></i></label>
                                                            <div class="file_button">
                                                                <input type="file"
                                                                       id="imagereply_file_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"
                                                                       cid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"
                                                                       class="jfeedimagereply_file"
                                                                       style="display:none;"/>
                                                            </div>
                                                        </a>
                                                        <input type="hidden" id="reply_comment_id"
                                                               value="<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
">
                                                    </div>
                                                </p>
                                                <div class="msg_display"
                                                     id="msg_display_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"></div>
                                                <div class="clearfix"></div>
                                                <a id="shareButton" cid="<?php echo $_smarty_tpl->tpl_vars['feedlist']->value['community_id'];?>
"
                                                   cmid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"
                                                   aid="<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_id;?>
"
                                                   class="btn yellow_btn fontupper pull-right jPostNewsfeedCommentButton">
                                                    Post</a>
                                            </div>
                                            <div class="loader"
                                                 id="imagereply_loader_<?php echo $_smarty_tpl->tpl_vars['comments']->value->announcement_comment_id;?>
">
                                                <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif"/>
                                            </div>
                                            <div id="jDivReplyImagePreview_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"></div>
                                            <div id="postingReply_<?php echo $_smarty_tpl->tpl_vars['comments']->value->news_feed_comments_id;?>
"></div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                <?php } ?>
                                <?php }else{ ?>
                           
                                <?php }?>
                                <!------------------------EOF Comments-------------------------------->
                            </div>


                        </div>
                        <div class="clearfix"></div>
                    </div>


                    <?php } ?>
                </div>

                <!-----------Profile display and edit section--------------------------------------->
                <div class='jFriends_div whitebox' style='display:none'></div>
                <div class='jGallary_div whitebox' style='display:none'></div>
                <div class="tab-pane fade" id="myprofile" style='display:none'>

                    <div id="msgDisplay"></div>
                    <div id="jMyProfile" class='whitebox' style="display:none;">
                        <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif"/> Loading....
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins pad5top">
                                <div class="ibox-content bordertop_0" id="jDivDisplayEdit" style="display:none">
                                    <form id="frmprofile_edit">
                                        <div class="form-group"><label class="col-lg-3 control-label">First Name</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="text" id="user_firstname"
                                                               class="form-control valid"
                                                               value="<?php echo PageContext::$response->userDetails['user_firstname'];?>
"
                                                               placeholder="First Name" name="user_firstname"
                                                               required="true">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"> *</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="spn_asterisk" id="spn_asterisk_cnamemsg"
                                                  style="color:red"></span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Last Name</label>

                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="text" class="form-control" placeholder="Last Name"
                                                               size="10"
                                                               value="<?php echo PageContext::$response->userDetails['user_lastname'];?>
"
                                                               id="user_lastname" name="user_lastname" required="true">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"> *</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="spn_asterisk" id="spn_asterisk_clnamemsg"
                                                  style="color:red"></span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Email </label>

                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="email" class="form-control"
                                                               placeholder="Your Email" size="10"
                                                               value="<?php echo PageContext::$response->userDetails['Email'];?>
"
                                                               id="user_email" name="user_email" required="true">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"> *</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <span class="spn_asterisk" id="spn_asterisk_emailmsg"
                                                  style="color:red"></span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Date of
                                                Birth</label>

                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="text" class="form-control "
                                                               placeholder="Date of Birth" size="10"
                                                               value="<?php echo PageContext::$response->userDetails['Date_of_Birth'];?>
"
                                                               id="user_date_of_birth" name="user_date_of_birth">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Gender</label>

                                            <div class="col-lg-9">

                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <select class="form-control valid" placeholder="Gender"
                                                                selected="Array" id="user_gender" name="user_gender"
                                                                type="select">
                                                            <option value="">Select</option>
                                                            <option value="M" <?php if (PageContext::$response->userDetails['Gender']=='M'){?>selected<?php }?> >Male
                                                            </option>
                                                            <option value="F" <?php if (PageContext::$response->userDetails['Gender']=='F'){?>selected<?php }?>>Female
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Address</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <div class="emotionsbtn_textblk">
                                                            <textarea class="form-control" placeholder="Address"
                                                                      id="user_address" name="user_address"
                                                                      type="textarea"><?php echo PageContext::$response->userDetails['Address'];?>
</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">City</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="text" class="form-control" placeholder=" City"
                                                               size="10"
                                                               value="<?php echo PageContext::$response->userDetails['City'];?>
"
                                                               id="user_city" name="user_city">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Country</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <select class="form-control user_country valid"
                                                                placeholder="Business Country" selected="Array"
                                                                id="user_country" name="user_country" type="select">
                                                            <?php  $_smarty_tpl->tpl_vars['country'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['country']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->countrydropdown; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['country']->key => $_smarty_tpl->tpl_vars['country']->value){
$_smarty_tpl->tpl_vars['country']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['country']->key;
?>
                                                            <option value="<?php echo $_smarty_tpl->tpl_vars['country']->value;?>
" <?php if (PageContext::$response->userDetails['user_country']==$_smarty_tpl->tpl_vars['country']->value){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['id']->value;?>

                                                            </option>
                                                            <?php } ?>
                                                            <!--<option value="AF">Afghanistan</option><option value="AL">Albania</option><option value="DZ">Algeria</option>-->
                                                        </select>
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">State</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <select class="form-control user_state valid" id="user_state"
                                                                placeholder="Business Country" selected="Array"
                                                                name="user_state" type="select" required="true">
                                                        </select>
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Phone</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="text" class="form-control" placeholder="Telephone"
                                                               size="10"
                                                               value="<?php echo PageContext::$response->userDetails['Phone'];?>
"
                                                               id="user_phone" name="user_phone">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span id="spn_asterisk_cnameadd" style="color:red"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-3 control-label">Zipcode</label>
                                            <div class="col-lg-9">
                                                <div class="display_table wid100p">
                                                    <div class="display_table_cell wid98per">
                                                        <input type="text" class="form-control" placeholder="Zip Code"
                                                               size="10"
                                                               value="<?php echo PageContext::$response->userDetails['Zip_Code'];?>
"
                                                               id="user_zipcode" name="user_zipcode">
                                                    </div>
                                                    <div class="display_table_cell wid2per vlaign_middle align-right">
                                                        <span class="spn_asterisk" id="spn_asterisk_zipmsg"
                                                              style="color:red"></span></div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-3 col-lg-9">
                                                <input id="user_alias" class=" " type="hidden" placeholder="" size="10"
                                                       value="<?php echo PageContext::$response->alias;?>
" name="user_alias">
                                                <input type="button" class="btn btn-sm yellow_btn3 marg5right left"
                                                       value="Update" id="jUpdate" name="btnSubmit"> <input
                                                    type="button" class="btn btn-sm grey_btn jShowProfile left"
                                                    alias="<?php echo PageContext::$response->alias;?>
"
                                                    uid="<?php echo PageContext::$response->sess_user_id;?>
" value="Cancel"
                                                    name="btnCancel">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="tagg-business" style="display:none">
                        <div id="tagform" class="tagform whitebox">
                            <h3>Organisation</h3>
                            <div class="row">
                                <div id="jmessage"></div>
                                <div class="col-lg-12">
                                    <div class="ibox-content">
                                        <div class="row">
                                            <form name="serachbuisnessform" id="serachbuisnessform" action=""
                                                  method="GET">

                                                <div class="form-group"><label class="col-lg-3 control-label">Select
                                                        Organisation</label>
                                                    <div class="col-lg-9">
                                                        <input name="searchtext" id="serachbuisness" type="text"
                                                               placeholder="Organisation Name"
                                                               class="busname form-control" required="true">

                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-lg-9">
                                                    <input type="hidden" name="search_selected_activity_buisness"
                                                           id="search_selected_activity_buisness">
                                                    <input type="hidden" name="search_selected_activity_buisness_name"
                                                           id="search_selected_activity_buisness_name">
                                                    <input type="hidden" name="search_selected_activity_buisness_alias"
                                                           id="search_selected_activity_buisness_alias">
                                                    <input type="hidden" name="search_selected_activity_buisness_image"
                                                           id="search_selected_activity_buisness_image">
                                                    <input type="hidden" name="user_image_path" id="user_image_path"
                                                           value="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->userImagePath;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
">
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-3 col-lg-9">
                                                        <input type="submit" name="add" value="Add"
                                                               class="invite_btn btn btn-sm yellow_btn3"
                                                               title="invite-block" style="margin-top:0px!important;">
                                                        <input type="hidden" name="tagg_id" value="0" id="tagg_id">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <section class="businesseditlists">
                                        <div class="row whitebox" id="tagList">
                                            <?php  $_smarty_tpl->tpl_vars['lead'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lead']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = PageContext::$response->tagDetails; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lead']->key => $_smarty_tpl->tpl_vars['lead']->value){
$_smarty_tpl->tpl_vars['lead']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['lead']->key;
?>
                                            <div class="col-sm-12 col-md-12 col-lg-12" id="tag_list_<?php echo $_smarty_tpl->tpl_vars['lead']->value->tagg_id;?>
">
                                                <div class="mediapost">
                                                    <div class="mediapost_left pull-left">
                                                        <span class="mediapost_pic">
                                                            <a id="image_link_<?php echo $_smarty_tpl->tpl_vars['lead']->value->tagg_id;?>
"
                                                               href="<?php echo PageContext::$response->baseUrl;?>
organisation/<?php echo $_smarty_tpl->tpl_vars['lead']->value->business_alias;?>
">
                                                                <img
                                                                    src="<?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 echo PageContext::$response->userImagePath;<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php if ($_smarty_tpl->tpl_vars['lead']->value->file_path!=''){?>thumb/<?php echo $_smarty_tpl->tpl_vars['lead']->value->file_path;?>
<?php }elseif($_smarty_tpl->tpl_vars['lead']->value->file_path==''){?>thumb/noimage.jpg<?php }?>"
                                                                    alt="" id="image_<?php echo $_smarty_tpl->tpl_vars['lead']->value->tagg_id;?>
"
                                                                    class="business_profile_desc_colside_pic">
                                                            </a>
                                                        </span>
                                                    </div>

                                                    <div class="media-body">
                                                        <h4 class="media-heading"><span><a
                                                                    id="business_<?php echo $_smarty_tpl->tpl_vars['lead']->value->tagg_id;?>
"
                                                                    href="<?php echo PageContext::$response->baseUrl;?>
directory/<?php echo $_smarty_tpl->tpl_vars['lead']->value->business_alias;?>
"><?php echo $_smarty_tpl->tpl_vars['lead']->value->business_name;?>
</a></span>
                                                        </h4>
                                                        <div class="display_table">
                                                            <div class="display_table_cell wid70per">
                                                            </div>
                                                            <div class="display_table_cell wid30per">
                                                                <div class="right">
                                                                    <a class="edititem left marg5right"
                                                                       href="javascript:void(0)" title="Edit"
                                                                       onclick="updateTags('<?php echo $_smarty_tpl->tpl_vars['lead']->value->tagg_id;?>
','<?php echo $_smarty_tpl->tpl_vars['lead']->value->business_id;?>
')"><i
                                                                            class="fa fa-pencil"></i></a>
                                                                    <a class="deleteitem left" href="javascript:void(0)"
                                                                       title="Delete"
                                                                       onclick="deleteTags('<?php echo $_smarty_tpl->tpl_vars['lead']->value->tagg_id;?>
','<?php echo $_smarty_tpl->tpl_vars['lead']->value->business_id;?>
')"><i
                                                                            class="fa fa-trash"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </section>
                                    <div class="clearfix"></div>

                                </div>
                            </div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>

                <!--EOF profile section-->


            </div>
<?php }else{ ?>
 <div class="clear"></div>
 <br><br>
<div class='whitebox'>
    No data found
</div>
<?php }?>
        </div>
    </div>
</section>

<!--codeversion_dev-->


<div class="clear"></div>
<div class="listitem loader">
    <img src="<?php echo PageContext::$response->userImagePath;?>
default/loader.gif"/> Loading....
</div>
</div>
</div>
</div>
</div>
</div>

<!--Friends end-->
</div>
<div class="clear"></div>
</div>
<?php }} ?>