<?php /* Smarty version Smarty-3.1.13, created on 2017-01-05 11:05:21
         compiled from "project\modules\default\view\script\user\add_business.tpl.php" */ ?>
<?php /*%%SmartyHeaderCode:8586586e1a6140b547-24566742%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '718292137a45d908a1647df706f97562830696b7' => 
    array (
      0 => 'project\\modules\\default\\view\\script\\user\\add_business.tpl.php',
      1 => 1469437019,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8586586e1a6140b547-24566742',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'id' => 0,
    'country' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_586e1a615c4c32_55983764',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_586e1a615c4c32_55983764')) {function content_586e1a615c4c32_55983764($_smarty_tpl) {?><div class="container">
    <div class="row"> 
        <div class="col-md-offset-3 col-lg-offset-3 col-sm-12 col-md-6 col-lg-6 ">
            <div class="whitebox marg40col pad25px_left pad25px_right">
				<div class="form-top">
	        		<div class="form-top-left">
	        			<h3>Add Page</h3>
	        		</div>
                                    
	            </div>
	            <div class="form-bottom signup_form">
	            	<div class="<?php echo PageContext::$response->message['msgClass'];?>
"><?php echo PageContext::$response->message['msg'];?>
</div>
	                <form enctype="multipart/form-data" class="" action="" method="post" id="frmBizdirectoryRegister" name="frmBizdirectoryRegister" novalidate="novalidate">
                        <div class="form-group relative">
                        	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
		                    		<input type="text" value="" name="business_name" class="form-control" placeholder="Page Name">
	                            </div>
	                            <div class="display_table_cell wid2per vlaign_middle align-right">
	                            	<span id="spn_asterisk_bnameadd" style="color:red">*</span>
	                            </div>
                            </div>
                            <label generated="true" for="business_name" class="error"></label>
	                    </div>
	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
	                    			<input type="text" value="<?php echo PageContext::$request['business_email'];?>
" name="business_email" class="form-control" placeholder="Email">
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                	<span id="spn_asterisk_bemailadd" style="color:red">*</span>
                                </div>
                            </div>
	                    	<label generated="true" for="business_email" class="error"></label>
	                    </div>
	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
			                    	<input type="text" value="<?php echo PageContext::$request['business_url'];?>
" name="business_url" class="form-control" placeholder="Website URL(http://www.domainname.com)">
	                            </div>
	                            <div class="display_table_cell wid2per vlaign_middle align-right">
	                            	<span id="spn_asterisk_burladd" style="color:red">*</span>
	                            </div>
	                        </div>
			                <label generated="true" for="business_url" class="error"></label>
	                    </div>

	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
	                    			<textarea class="form-control" name="business_description" placeholder="Page Description"><?php echo PageContext::$request['business_description'];?>
</textarea>
	                    		</div>
	                    		<div class="display_table_cell wid2per vlaign_middle align-right">
                                	<span id="spn_asterisk_bdescriptionadd" style="color:red">*</span>
                                </div>
                            </div>
	                    	<label generated="true" for="business_description" class="error"></label>
	                    </div>

	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
	                    			<textarea class="form-control" name="business_address" placeholder="Address"><?php echo PageContext::$request['business_address'];?>
</textarea>
	                    		</div>
	                    		<div class="display_table_cell wid2per vlaign_middle align-right"></div>
	                    	</div>
	                    	<label generated="true" for="business_address" class="error"></label>
	                    </div>
	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
	                    			<input type="text" name="business_city" class="form-control" placeholder="City" value="<?php echo PageContext::$request['business_city'];?>
">
	                    		</div>
	                    		<div class="display_table_cell wid2per vlaign_middle align-right"></div>
	                    	</div>
	                    </div>
	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
                                <select class="form-control" placeholder="Country" selected="Array" name="business_country" id="business_country" type="select">
                                    <?php  $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['id']->_loop = false;
 $_smarty_tpl->tpl_vars['country'] = new Smarty_Variable;
 $_from = PageContext::$response->countries; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['id']->key => $_smarty_tpl->tpl_vars['id']->value){
$_smarty_tpl->tpl_vars['id']->_loop = true;
 $_smarty_tpl->tpl_vars['country']->value = $_smarty_tpl->tpl_vars['id']->key;
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" <?php if (PageContext::$response->data['business_country']==$_smarty_tpl->tpl_vars['id']->value||PageContext::$request['business_country']==$_smarty_tpl->tpl_vars['id']->value){?> selected="selected" <?php }?>><?php echo $_smarty_tpl->tpl_vars['country']->value;?>
</option>
                                    <?php } ?>
                                </select>
                                </div>
	                    		<div class="display_table_cell wid2per vlaign_middle align-right"></div>
	                    		</div>
	                    </div>
                            <div class="form-group relative">
                            	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
                                	<input type="text" class="form-control" name="business_state" id="business_state" placeholder="State" value="<?php echo PageContext::$request['business_state'];?>
">
                                </div>
	                    		<div class="display_table_cell wid2per vlaign_middle align-right"></div>
								</div>
                            </div>
	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
	                    			<input type="text" name="business_phone" class="form-control" placeholder="Telephone" size="10" value="<?php echo PageContext::$request['business_phone'];?>
">
	                    		</div>
	                    	
                            </div>
	                    </div>
	                    <div class="form-group relative">
	                    	<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
	                    			<input type="text" name="business_zipcode" class="form-control" placeholder="Zip Code" size="10" value="<?php echo PageContext::$request['business_zipcode'];?>
">
                                </div>
                            </div>
	                    </div>
	                    <div class="form-group relative">
							<div class="display_table wid100p">
	                        	<div class="display_table_cell wid98per">
                                            <input type="file" class="  left" placeholder="Upload File" onchange="upload(150,150,'business_image_id');" size="10" value="" id="business_image_id" name="business_image_id">
			                    	<!--<img class="masterTooltip" title="Business Image(Supported format:jpg,jpeg,gif,png)" src="<?php echo PageContext::$response->baseUrl;?>
modules/cms/images/help_icon.png">-->
                                </div>
                                <div class="display_table_cell wid2per vlaign_middle align-right">
                                	<span id="spn_asterisk_bzipcodeadd" style="color:red">*</span>
                                </div>
                            </div>
                            <div class="wid100p">
								<span id="spn_support_imgdirectory">(Supported format:jpg,jpeg,gif,png)</span>
                            </div>
                            <div class="wid100p">
								<label generated="true" id="label_image_id" for="business_image_id" class="error"></label>
                            </div>
	                    </div>
                              <div class="form-group relative">
<!--                            <div class="display_table wid100p">
                                <div class="display_table_cell wid98per">
                                    Upload logo 
                                    <input type="file" class="left" placeholder="Upload File" size="10" value="" name="business_logo_id">
                             <img class="masterTooltip" src="<?php echo PageContext::$response->baseUrl;?>
modules/cms/images/help_icon.png" title="Community Image(Supported format:jpg,jpeg,gif,png)"> 
                                <div class="clearfix"></div>
                                <span class="left" id="spn_support_logoadd">(Supported format:jpg,jpeg,gif,png)</span>
                                </div>
                                
                                 <div class="display_table_cell wid2per vlaign_middle align-right">
                                    <span id="spn_asterisk_cimageadd" style="color:red;">*</span>
                                </div> 
                               
                            </div>-->
                        </div>
	                    <div class="form-group relative">
	                    	<input type="submit" class="btn btn-primary yellow_btn2" value="Save" name="btnSubmit" ondblclick="this.disabled=true;">
                                <input type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary yellow_btn2" value="Cancel" name="btnCancel">
	                	</div>
	                	<div class="clearfix"></div>
	                </form>
	            </div>
	    </div>
        </div>
    </div>
</div>




<?php }} ?>