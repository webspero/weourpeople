<?php

/* * ******************************************
  GlobalGiving API Module
 * @author :Smitha Mohan
 * ****************************************** */
class GlobalGiving{
    var $api_key;
    var $api_url;
    var $api_email;
    var $api_password;
    var $operation;
    var $context;
    
    /*
     * Initialise Key and Url 
     */
    public function __construct() {
        $this->api_key      =    GLOBAL_GIVE_API_KEY;
        $this->api_url      =    GLOBAL_GIVE_API_URL;
        $this->api_email    =    GLOBAL_GIVE_API_EMAIL;
        $this->api_password =    GLOBAL_GIVE_API_PASSWORD;
    }
    
    /*
     * Create a stream 
     */
     public function create_api_stream(){
        $opts                   =   array('http'=>array('method'=>"GET",
                                                        'header'=>"Accept: application/json\r\n"
                                                    ));
        return $this->context  = stream_context_create($opts);
    }
    
    /*
     * Call to API end points
     */
    public function hash_call($url){
        $fileContents       = file_get_contents($url); 
        //echopre($fileContents);
//        $fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);
//        $fileContents = trim(str_replace('"', "'", $fileContents));
        $simpleXml          = simplexml_load_string($fileContents);
        
        $json               = json_decode(json_encode($simpleXml));
        
       // echopre1($json);
        return $json;
    }
   
    
    /*
     * get Access Token
     */
    public function get_access_token(){
       $operation          = '/userservice/tokens';
       $xml_data = <<<END
                    <?xml version='1.0' encoding='UTF-8' standalone='yes'?>
                    <auth_request>
                    <user>
                    <email>$this->api_email</email>
                    <password>$this->api_password</password>
                    </user>
                    <api_key>$this->api_key</api_key>
                    </auth_request>
END;
       $url                = "$this->api_url"."$operation";
       $output             =  self::post_curl_call($url,$xml_data);
       $simpleXml          = simplexml_load_string($output);
      // var_dump($simpleXml);
       $json               = json_decode(json_encode($simpleXml));
       return $json->access_token;
       
    }
    
    /*
     * CURL function call
     */
    public function post_curl_call($URL,$xml_data){
        $ch = curl_init($URL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, trim($xml_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
    
    /*
     * Get an Organisation detail
     */
    public function get_oranganisation($org_id){
      // $operation          = '/public/orgservice/organization/';
       $operation          = '/secure/orgservice/organization/';
       $query_string       = "$org_id?api_key=" . "$this->api_key";
       $url                = "$this->api_url"."$operation".$query_string;
       return $output      =  self::hash_call($url);
    }
    
    /*
     * Get All organisations
     */
     public function get_all_oranganisations($next_id=''){
       //$operation          = '/public/orgservice/all/organizations';
        $operation          = '/secure/orgservice/all/organizations/countries/GB';
       if($next_id ==''){
          $query_string       = "api_key=" . "$this->api_key"; 
       }
       else{
           $query_string       = "api_key=" . "$this->api_key&nextOrgId=$next_id";
       }
        $url                = "$this->api_url"."$operation"."?".$query_string;
        $output      =  self::hash_call($url);
        //echopre($output);
        return $output;
    }
    
    /*
     * Get All organisations
     */
     public function submit_an_organisation_order($data){
       $operation          = '/secure/givingservice/orgorders';
       $access_token       = self::get_access_token();
       
       $query_string       = "api_key=" . "$this->api_key&api_token=".$access_token."&is_test=true"; 
       $url                = "$this->api_url"."$operation"."?".$query_string;
       $xml_data           = <<<END
                            <?xml version='1.0' encoding='UTF-8' standalone='yes'?>
                            <order>
                            <refcode>$data->refcode</refcode>
                            <transactionId>$data->trasactionId</transactionId>
                            <email>$data->email</email>
                            <amount>$data->amt</amount>
                            <currencyCode>GBP</currencyCode>
                            <organization>
                               <id>$data->orgId</id>
                            </organization>
                            <signupForGGNewsletter>false</signupForGGNewsletter>
                            <signupForCharityNewsletter>false</signupForCharityNewsletter>
                         </order>
END;
       
       $output      =  self::post_curl_call($url,$xml_data);
       $simpleXml          = simplexml_load_string($output);
      // var_dump($simpleXml);
       $json               = json_decode(json_encode($simpleXml));
       echopre($json);
       return $json;
//       $url                = "$this->api_url"."$operation"."?".$query_string;
//        $output      =  self::hash_call($url);
//        return $output;
    }
    
    /*
     *  Organisation search
     */
     public function search_oranganisation($search_text='',$page=1,$total=''){
        $operation          = '/secure/orgservice/search';
        $query_string       = "api_key=" . "$this->api_key&q=$search_text&filter=country:GB&start=$page";
        //$query_string       = "api_key=" . "$this->api_key&q=$search_text&filter=addressCountry:GB&start=$page";
        $url                = "$this->api_url"."$operation"."?".$query_string;
        $output             =  self::hash_call($url);
        //echopre1($output);
        return $output;
    }
    public function search_oranganisationByCountry($search_text='',$country='',$page=1,$total=''){
        $operation          = '/secure/orgservice/search';
        
        //$query_string       = "api_key=" . "$this->api_key&q=$search_text&filter=country:GB&start=$page";
        $query_string       = "api_key=" . "$this->api_key&q=$search_text&filter=addressCountry:$country&start=$page";
       // echo $query_string;exit;
        $url                = "$this->api_url"."$operation"."?".$query_string;
        $output             =  self::hash_call($url);
       // echopre($output);
        return $output;
    }
    
     /*
    * Get All projects
    */
//    public function get_all_projects($next_id=''){
//      //$operation          = '/public/orgservice/all/organizations';
//      // $operation          = '/secure/orgservice/all/organizations/countries/GB';
//       
//       $operation          ='/public/projectservice/countries/GB/projects';
//      if($next_id ==''){
//         $query_string       = "api_key=" . "$this->api_key"; 
//      }
//      else{
//          $query_string       = "api_key=" . "$this->api_key&nextOrgId=$next_id";
//      }
//       $url                = "$this->api_url"."$operation"."?".$query_string;
//       $output      =  self::hash_call($url);
//       echopre1($output);
//       return $output;
//   }

    
    
}?>