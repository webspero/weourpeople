<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
// +----------------------------------------------------------------------+
// | File name : index.php                                                |
// | PHP version >= 5.2                                                   |
// +----------------------------------------------------------------------+
// | Author: BINU CHANDRAN.E<binu.chandran@armiasystems.com>              |
// +----------------------------------------------------------------------+
// | Modified: ARUN SADASIVAN (01/07/2012)								  |
// |----------------------------------------------------------------------+
// | Copyrights Armia Systems 2010                                      |
// | All rights reserved                                                  |
// +----------------------------------------------------------------------+
// | This script may not be distributed, sold, given away for free to     |
// | third party, or used as a part of any internet services such as      |
// | webdesign etc.                                                       |
// +----------------------------------------------------------------------+
error_reporting(0);
ini_set('display_errors', 1);
ob_start();
include_once('../config/config.php');
include_once('cls_serverconfig.php');

//prevent re-installation if already installed
if (INSTALLED) {
    header("location: ../../index");
    exit;
}


/*
 * get the path dynamically
 * NOTE:- need to add www via htaccess if required
 */
$s                       = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
$protocol                = substr(strtolower($_SERVER["SERVER_PROTOCOL"]), 0, strpos(strtolower($_SERVER["SERVER_PROTOCOL"]), "/")) .$s . "://";
define('ROOT_URL', $protocol );
        
//Generating Root URL
$currentDomain           =  $_SERVER['SERVER_NAME'];
$path                    = $_SERVER['PHP_SELF'];
$path_parts              = pathinfo($path);
$directory               = $path_parts['dirname'];

if($directory=='' || $directory=='/'){
    $root_url            = ROOT_URL.$currentDomain.'/';
}else{
    $directory              = explode("/index.php",$directory);
    $directory              = ($directory == "/") ? "" : $directory[0].'/';
    $directory              = str_replace('project/install/', '', $directory);
    $root_url               = ROOT_URL.$currentDomain.$directory;
}

//add trailing slashes
if($root_url[strlen($root_url) - 1] <> '/'){
    $root_url .= '/';
}

define('BASE_URL', $root_url);
$secure_root_url                = $root_url;
$secure_root_url                = str_replace('http:', 'https:', $secure_root_url);

$perm_flag                  = true;
$perm_msg                   = '';
$error_message              = '';
$error                      = false;
$installed                  = false;
$user_data                  = array();
$post_flag                  = false;

$configfile                 = "../config/config.php";
$settingsfile               = "../config/settings.php";
$schemafile                 = "sql/schema.sql";
$datafile                   = "sql/data.sql";


$txtTablePrefix             = 'tbl_';
$txtSiteNameArr             = explode('.', $_SERVER['SERVER_NAME']);
$txtSiteName                = $txtSiteNameArr[0];
$user_data['txtSiteName']   = $txtSiteName;

$directories = array(
    "files/",
    "files/medium/",
    "files/small/",
    "files/thumb/",
    "files/logo/",
    "files/default/",
    "files/timeline/",
    "temp/templates_c/",
    "styles/images/",
    "config/config.php",
    "config/settings.php");

$serverSettings             = ServerConfig::checkServerConfiguration();
$serverCurrentSettings      = ServerConfig::getServerSettings();
$host_name                  = parse_url($_SERVER['HTTP_HOST']);

if (isset($_POST['installerCheck']) || $_POST['cldpack'] == 1) {
   $post_flag = true;

//For Getting Folder Permission Status and Proper Error messages
    foreach ($directories as $dir) {
        $permission             = ServerConfig::fileWritable($dir, 'project/'.$dir);
        if (!$permission['status'] && $error == false) {
            $error                  = true;
            $serverPermission       ="true";
        }
    }

	
    if ($error == true) { //echo '<pre>'; print_r($_POST); echo '</pre>'; exit;
        if (isset($_POST["btnContinue"]) && !isset($_POST["auto_set"])) {
            $txtFTPusername             = $_POST['FTPusername'];
            $txtFTPpassword             = $_POST['FTPpassword'];

            $user_data['FTPusername']   = $_POST["FTPusername"];
            $user_data['FTPpassword']   = $_POST["FTPpassword"];

            if (trim($txtFTPusername) == '') {
                $perm_msg .= '* Please enter FTP username <br/>';
            }
            if (trim($txtFTPpassword) == '') {
                $perm_msg .= '* Please enter FTP password <br/>';
            } else {
                $conn_id                = @ftp_connect($host_name["path"]);
                $login_result           = @ftp_login($conn_id, $txtFTPusername, $txtFTPpassword);
                if ($login_result) {
                    $mode               = 777;
                    $np                 = '0' . $mode;
                    $user_install       = str_replace('install', '', getcwd());

                    //get the path staring from public_html
                    if (strstr($user_install, 'public_html')) {
                        $path_parts         = explode('/public_html', $user_install);
                        $user_install       = '/public_html' . $path_parts[1];
                    } elseif (strstr($user_install, 'httpdocs')) {
                        $path_parts         = explode('/httpdocs', $user_install);
                        $user_install       = '/httpdocs' . $path_parts[1];
                    }

                    foreach ($directories as $directory) {
                        $edited_path            = str_replace('..', '', $directory);
                        $directory = $user_install . $edited_path;
                        if ($directory[strlen($directory) - 1] == '/') {
                            $directory          = substr($directory, 0, strlen($directory) - 1);
                        }
                        $directory=ltrim($directory,"/");
                        if (!@ftp_chmod($conn_id, eval("return({$np});"), $directory)) {
                            $perm_flag      = false;
                        }
                    }
                    

                    if (!$perm_flag) {
                        $perm_msg .= '* Sorry, an error occurred. Please try again or set the permissions manually <br/>';
                    } else {
                        $perm_msg   = '<b>* File permissions successfuly set </b><br/>';
						$serverPermission="false";
						$error=false;
                    }
                } else {
                    $perm_msg .= '* Sorry, could not connect to the server. Please check the credentials <br/>';
                }
            }
        }
    }

    //check the user data
    if($_POST['cldpack'] == 1) {
        $user_data['txtDBServerName']           = "localhost";
        $user_data['txtDBName']                 = $_POST["db_name"];
        $user_data['txtDBUserName']             = $_POST["db_user"];
        $user_data['txtDBPassword']             = $_POST["db_password"];
        $user_data['txtSiteName']               = $_POST["store_name"];
        //$user_data['txtAdminName']              = $_POST["txtAdminName"];
        $user_data['txtAdminPassword']          = "q1w2e3";
        $user_data['txtLicenseKey']             = "icloud"; //temp license key
        $user_data['txtAdminEmail']             = $_POST["user_email"];
        $user_data['txtTablePrefix']            = $txtTablePrefix;
    } else {
        $user_data['txtDBServerName']           = $_POST["txtDBServerName"];
        $user_data['txtDBName']                 = $_POST["txtDBName"];
        $user_data['txtDBUserName']             = $_POST["txtDBUserName"];
        $user_data['txtDBPassword']             = $_POST["txtDBPassword"];
        $user_data['txtSiteName']               = $_POST["txtSiteName"];
        //$user_data['txtAdminName']              = $_POST["txtAdminName"];
        $user_data['txtAdminPassword']          = $_POST["txtAdminPassword"];
        $user_data['txtLicenseKey']             = $_POST["txtLicenseKey"];
        $user_data['txtAdminEmail']             = $_POST["txtAdminEmail"];
        $user_data['txtTablePrefix']            = $txtTablePrefix;
    }

    if (trim($user_data['txtDBServerName']) == '') {
        $message .= " * Database Server Name is empty!" . "<br>";
        $error = true;
    }
    if (trim($user_data['txtDBName']) == '') {
        $message .= " * Database Name is empty!" . "<br>";
        $error = true;
    }
    if (trim($user_data['txtDBUserName']) == '') {
        $message .= " * Database User Name is empty!" . "<br>";
        $error = true;
    }
    if (trim($user_data['txtSiteName']) == '') {
        $message .= " * Site Name is empty!" . "<br>";
        $error = true;
    }
    if (trim($user_data['txtLicenseKey']) == '') {
        $message .= " * License Key is empty!" . "<br>";
        $error = true;
    }
    if (trim($user_data['txtAdminEmail']) == '') {
        $message .= " * Admin Email is empty!" . "<br>";
        $error = true;
    } else {
        if (!ServerConfig::is_valid_email($user_data['txtAdminEmail'])) {
            $message .= " * Invalid Admin Email!" . "<br>";
            $error = true;
        }
    }

    //check the db connection
    $connection             = @mysqli_connect($user_data['txtDBServerName'], $user_data['txtDBUserName'], $user_data['txtDBPassword']);
    if ($connection === false) {
        $error              = true;
        $message .= " * Connection Not Successful! Please verify your database details!<br>";
    } else {
        $dbselected         = @mysqli_select_db($connection,$user_data['txtDBName']);
            mysqli_query("SET SESSION sql_mode ='' ");
        if (!$dbselected) {
            $error          = true;
            $message .= " * Database could not be selected! Please verify your database details!<br>";
        }
    }

    //proceed with corresponding action
    $version = '4.0';
    if (!$error) {
        //update Socialware config file
        $fp                 = fopen($configfile, "w+");
        $configcontent      = "<?php\n";
        $configcontent      .= "define('INSTALLED', true); \n\n";
        $configcontent      .= "define('VERSION', $version); \n\n";
        if($_POST['cldpack'] == 1) {
            $configcontent .= "define('CLOUDINSTALLED', true);"."\n\n";
        }
        $configcontent      .= "\n?>";
        fwrite($fp, $configcontent);

        //update Socialware settings file
        $handle                 = fopen($settingsfile, "rb");
        $contents               = fread($handle, filesize($settingsfile));
        fclose($handle);

        $contents               = str_replace('DB_NAME', $user_data['txtDBName'], $contents);
        $contents               = str_replace('USER_NAME', $user_data['txtDBUserName'], $contents);
        $contents               = str_replace('DB_PASSWORD', $user_data['txtDBPassword'], $contents);
        $contents               = str_replace('HOST_NAME', $user_data['txtDBServerName'], $contents);
        $contents               = str_replace('DB_PREFIX', $user_data['txtTablePrefix'], $contents);
        
        $fp = fopen($settingsfile, 'w');
        fwrite($fp, $contents);
        fclose($fp);

        //------------------------UPDATE THE DB---------------------------------//
        $sqlquery           = @fread(@fopen($schemafile, 'r'), @filesize($schemafile));
        $sqlquery           = preg_replace('/tbl_/', $user_data['txtTablePrefix'], $sqlquery);
        $sqlquery           = ServerConfig::splitsqlfile($sqlquery, ";");

        $tableName          = 'lookup';
        $labelField         = 'settingfield';
        $valueField         = 'value';

        for ($i = 0; $i < sizeof($sqlquery); $i++) {
            mysqli_query($connection,$sqlquery[$i]);
        }

        $dataquery              = @fread(@fopen($datafile, 'r'), @filesize($datafile));
        $dataquery              = preg_replace('/tbl_/', $user_data['txtTablePrefix'], $dataquery);
        $dataquery              = ServerConfig::splitsqlfile($dataquery, ";");

        for ($i = 0; $i < sizeof($dataquery); $i++) {
            mysqli_query($connection,$dataquery[$i]);
        }
        
        $sqlsettings1               = "UPDATE " . $user_data['txtTablePrefix'] . $tableName." SET ".$valueField." = '" . addslashes($user_data['txtLicenseKey']) . "' WHERE ".$labelField." = 'license'";
        mysqli_query($connection,$sqlsettings1) or die(mysqli_error());
        
        $sqlsettings1 = "UPDATE " . $user_data['txtTablePrefix'] . $tableName." SET ".$valueField." = '" . addslashes($user_data['txtSiteName']) . "' WHERE ".$labelField." = 'siteName'";
        mysqli_query($connection,$sqlsettings1) or die(mysqli_error());
        
        $sqlsettings1 = "UPDATE " . $user_data['txtTablePrefix'] . $tableName." SET ".$valueField." = '" . addslashes($user_data['txtSiteName']) . "' WHERE ".$labelField." = 'siteTitle'";
        mysqli_query($connection,$sqlsettings1) or die(mysqli_error());
        
        $sqlsettings1 = "UPDATE " . $user_data['txtTablePrefix'] . $tableName." SET ".$valueField." = '" . addslashes($user_data['txtAdminEmail']) . "' WHERE ".$labelField." = 'vadmin_email'";
        mysqli_query($connection,$sqlsettings1) or die(mysqli_error());

        $sqlsettings1 = "UPDATE " . $user_data['txtTablePrefix'] . $tableName." SET ".$valueField." = '" . addslashes($user_data['txtAdminEmail']) . "' WHERE ".$labelField." = 'addressfromemail'";
        mysqli_query($connection,$sqlsettings1) or die(mysqli_error());

        $sqlsettings1 = "UPDATE " . $user_data['txtTablePrefix'] . $tableName." SET ".$valueField." = '" . addslashes($user_data['txtAdminEmail']) . "' WHERE ".$labelField." = 'addressfromemailname'";
        mysqli_query($connection,$sqlsettings1) or die(mysqli_error());

        $sqlsettings1 = "UPDATE " . $user_data['txtTablePrefix'] . $tableName." SET ".$valueField." = '" . addslashes($user_data['txtAdminEmail']) . "' WHERE ".$labelField." = 'addressreplyemail'";
        mysqli_query($connection,$sqlsettings1) or die(mysqli_error());

        $sqlsettings1 = "UPDATE " . $user_data['txtTablePrefix'] . $tableName." SET ".$valueField." = '" . addslashes($user_data['txtAdminEmail']) . "' WHERE ".$labelField." = 'addressreplyemailname'";
        mysqli_query($connection,$sqlsettings1) or die(mysqli_error());
        
        $sqlsettings1 = "UPDATE " . $user_data['txtTablePrefix'] . $tableName." SET ".$valueField." = '" . addslashes($root_url) . "' WHERE ".$labelField." = 'vsite_url'";
        mysqli_query($connection,$sqlsettings1) or die(mysqli_error());
        
        $sqlsettings1 = "UPDATE " . $user_data['txtTablePrefix'] . $tableName." SET ".$valueField." = '" . addslashes($secure_root_url) . "' WHERE ".$labelField." = 'vsecure_server'";
        mysqli_query($connection,$sqlsettings1) or die(mysqli_error());
        
        if($_POST['cldpack'] == 1) {
            $sqlsettings1 = "UPDATE cms_users SET password = '" . md5($user_data['txtAdminPassword']) . "' WHERE username = 'admin'";
            mysqli_query($connection,$sqlsettings1) or die(mysqli_error());
        }

        /*
        //update cms settings
        $cms_query_str = "SELECT id, table_name, section_config FROM cms_sections";
        $cms_query = mysqli_query($connection,$cms_query_str);
        if($cms_query){
            while($cms_res = mysqli_fetch_array($cms_query)){
                $tbl_name       = $cms_res['table_name'];
                $section_config = $cms_res['section_config'];
                $tbl_id         = $cms_res['id'];
                
                //if($tbl_name <> 'tbl_cms_settings'){
                    $tbl_name = str_replace('tbl_', $user_data['txtTablePrefix'], $tbl_name);
                    $section_config = str_replace('tbl_', $user_data['txtTablePrefix'], $section_config);
                    
                    mysqli_query($connection,"UPDATE cms_sections SET table_name = '" . mysqli_real_escape_string($tbl_name) . "', section_config = '" . mysqli_real_escape_string($section_config) . "' WHERE id = $tbl_id");
                //}
            }
        }
        */

        //installation tracker
        $rootserver                         = BASE_URL;
        $productVersion                     = 'Socialware 4.0';
        $string                             = "";
        $pro                                = urlencode($productVersion);
        $dom                                = urlencode($rootserver);
        $ipv                                = urlencode($_SERVER['REMOTE_ADDR']);
        $mai                                = urlencode($user_data['txtAdminEmail']);
        $string                             = "pro=$pro&dom=$dom&ipv=$ipv&mai=$mai";
        $contents                           = "no";
        $file                               = @fopen("http://www.iscripts.com/installtracker.php?$string", 'r');
        if ($file) {
            $contents                       = @fread($file, 8192);
        }

        $installed                          = true;

        //send confirmation email to admin
        $subject                            = "Script Installed at " . $user_data['txtSiteName'];
        
        $headers                            = "From: " . $user_data['txtSiteName'] . "<" . $user_data['txtAdminEmail'] . ">\r\n";
        $headers                            .= "MIME-Version: 1.0\r\n";
        $headers                            .= "Content-type: text/html; charset=iso-8859-1\r\n";
        
        $mailcontent                        = "Hello , <br>";
        $mailcontent                       .= "Your Site is successfully installed.<br> <a href='" . BASE_URL . "' target='_blank'>Click Here to Access your Site</a>";
        $mailcontent                       .= "<br><a href='" . BASE_URL . "cms' target='_blank'>Click Here to Access your Site Administration Control Panel</a> <br><br>";
        $mailcontent                       .= "Your Admin Username   :  " . $user_data['txtAdminName'];
        $mailcontent                        .= "<br>Your Admin Password   :  " . $user_data['txtAdminPassword'];        
        $mailcontent                        .= "<br> Thanks and regards,<br> " . $user_data['txtSiteName'] . " Team";

        /* Email Template */
        $email_temp_qry                 = "SELECT mail_template_body FROM ".$user_data['txtTablePrefix']."mail_template WHERE mail_template_name='installer_mail'";
        $email_temp                     = mysqli_query($connection,$email_temp_qry);
        $mailMsgArr                     = mysqli_fetch_array($email_temp);
        $mailMsg                        = NULL;
        $dateVal                        = date("m/d/Y");
        $copyright                      = 'copyright '.date('Y').' '.$user_data['txtSiteName'].' All rights reserved';

        if(count($mailMsgArr) > 0) {
            $mailMsg                    = $mailMsgArr['mail_template_body'];
        } // End If
        if(!empty($mailMsg)){
                $mailMsg                = str_replace("{SITE_LOGO}", "<img src='".BASE_URL . "project/install/css/logo.png'/>", $mailMsg);
                $mailMsg                = str_replace("{Date}", $dateVal, $mailMsg);
                $mailMsg                = str_replace("{MAIL_CONTENT}", $mailcontent, $mailMsg);
                $mailMsg                = str_replace("{SITE_NAME}", $user_data['txtSiteName'], $mailMsg);
                $mailMsg                = str_replace("{COPYRIGHT}", $copyright, $mailMsg);
            } else {
                $mailMsg                = $mailcontent;
            }

        $mailcontent                    = $mailMsg;
        @mail(addslashes($user_data['txtAdminEmail']), $subject, $mailcontent, $headers);
    }
}

//For Getting Folder Permission Status and Proper Error messages
	
	foreach ($directories as $dir) {
        $permission                 = ServerConfig::fileWritable($dir, 'project/'.$dir);
        if (!$permission['status'] && $error == false) {
                $error              = true; 
                $serverPermission   ="true";
	}
         if (!$permission['status'] ) {
                $error_message      =$error_message.$permission['message'];
        }
}

if ($installed) {
    header("location: install_success.php");
    exit;
}
$installerTitle                 = 'iScripts Socialware Installer';
$productName                    = 'Socialware';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title><?php echo $installerTitle; ?></title>
    </head>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/install.js"></script>
    <link href="css/install.css" rel="stylesheet" type="text/css" />
    <body class="bodyinstaller">
        <div class="header_row" >
            <div class="header_container  sitewidth">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td width="23%" align="left" ><img src="css/logo.png" alt="Logo"></td>
                        <td width="77%" align="right">
                            <h4><?php echo $installerTitle; ?></h4>
                            <div align="center" id="items_top_area">
                                &nbsp;&nbsp;
                                <!--<a title="OnlineInstallationManual" href="#" onClick="window.open('<?php echo BASE_URL; ?>project/Docs/Installation_Manual.pdf','OnlineInstallationManual','top=100,left=100,width=820,height=550,scrollbars=yes,toolbar=no,status=yrd');"><strong>Installation manual</strong></a> |-->
                                <a title="Readme" href="#" onClick="window.open('<?php echo BASE_URL; ?>project/Docs/Readme.txt','Readme','top=100,left=100,width=820,height=550,scrollbars=yes,toolbar=no,status=yrd');"><strong>Readme</strong></a> | 
                                <a title="If you have any difficulty, submit a ticket to the support department" href="#" onClick="window.open('http://www.iscripts.com/support/postticketbeforeregister.php','','top=100,left=100,width=820,height=550,scrollbars=yes,toolbar=no,status=yrd,resizable=yes');">
                                <strong>Get Support</strong></a>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><img src="css/spacer.gif" width="1" height="5"></td>
            </tr>
        </table>
        
        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="76%" valign="top" height ="400">
                    <!-- Here's where I want my views to be displayed -->
                    <table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td>
                                <!--------Installer starts------------------------->

                                <!--items display area start -->
                                <?php
                                if ($serverSettings == "FAILURE") {
                                    ?>
                                    <table width="100%" border=0 align="center">
                                        <?php
                                        foreach ($serverCurrentSettings as $settings) {
                                            $span_class = $settings['flag'] ? "install_value_ok" : "install_value_fail";
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo $settings['feature']; ?> <span class="<?php echo $span_class; ?>"><?php echo $settings['setting']; ?></span>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                            <td>
                                                <span class="install_value_fail">Fatal errors detected.  Please correct the above red items and reload.</span>
                                            </td>
                                        </tr>
                                    </table>

                                    <?php
                                } else if (!$installed) {
                                    ?>
                                    <table width="80%" border="0" align="center">
                                        <tr>
                                            <td align="center" ><b><font size="1">
                                                    <div align="justify" >
                                                        <br>
                                                        <font color="#F4700E" size="+1">Thank you for choosing <?php echo $productName;?>&nbsp;</font> <br><br>
                                                        <font color="#000000" size="2">To complete this installation please enter the details below.</font>
                                                    </div>

                                                    </font></b>
                                            </td>
                                        </tr>
                                        <?php if ($post_flag) { ?>
                                            <tr>
                                                <td align=center class="message" >
                                                    <div align="left" class="text_information">
                                                        <br>
                                                        <?php if($error){?><u><b><font color="#FF0000">Please correct the following errors to continue:</font></b></u><p/><?php }?>
                                                        <font color="#FF0000"><?php echo $perm_msg; ?></font><br>
                                                        <font color="#FF0000"><?php echo $error_message . '<br/>' . $message; ?></font><br>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td class=maintext align="left" >
                                                Note: All Fields Are Mandatory.
                                                <br>
                                                <form name="frmInstall" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                                    <br>

                                                    <FIELDSET>
                                                        <LEGEND class='block_class'>File Permissions</LEGEND>
                                                        <table width=85% border="0" cellpadding="2" cellspacing="2" class=maintext>
                                                            <tr>
                                                                <td colspan="2" align="left">
                                                                    <b>
                                                                        <?php if ($serverPermission==true) { ?>
                                                                            <?php echo $productName;?> requires that some of the folders have write permission. You can provide an FTP login so that this process is done automatically.<br/><br/>
                                                                            For security reasons, it is best to create a separate FTP user account with access to the <?php echo $productName;?> installation only and not the entire web server. Your host can assist you with this.
                                                                            If you have difficulties completing installation without these credentials, please click "I would provide permissions manually" to do it yourself.<br/><br/>
                                                                        <?php } ?>
                                                                    </b>
                                                                </td>
                                                            </tr>
                                                            <?php if ($serverPermission==true) { ?>
                                                                <tr>
                                                                    <td class=maintext align="left">FTP username</td>
                                                                    <td width="70%" align=left>
                                                                        <input name="FTPusername"  id="FTPusername" type="text" size="50" value="<?php echo htmlentities($user_data['FTPusername']); ?>"> <img src="css/Help.png" width="20" height="20" title="FTP Username of Root folder for changing appropriate file permission">									
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class=maintext align="left">FTP password</td>
                                                                    <td width="70%" align=left>
                                                                        <input name="FTPpassword"  id="FTPpassword" type="password" size="50" value="<?php echo htmlentities($user_data['FTPpassword']); ?>"> <img src="css/Help.png" width="20" height="20" title="FTP password of above FTP user">
                                                                    </td>
                                                                </tr>
                                                                <?php if ($serverPermission==true) { ?>
                                                                    <tr>
                                                                        <td colspan="2" align="left">
                                                                            <input type="checkbox" name="auto_set" id="auto_set" <?php echo ($_POST['auto_set'])?'checked':'';?> onclick="divToggle(this)" /> &nbsp; I would provide permissions manually
                                                                        </td>
                                                                    </tr>                                                
                                                                    <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <tr>
                                                                    <td colspan="2" align="left">
                                                                        <b>File permissions are OK.</b>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        </table>
                                                        <?php if ($serverPermission==true) { ?>
                                                            <div id="err_div" style="display:none">
                                                                <fieldset>
                                                                    <legend>Directories/Files List</legend>
                                                                    <?php echo $error_message; ?>
                                                                </fieldset>
                                                            </div>
                                                        <?php } ?>
                                                    </FIELDSET>
                                                    <br>
                                                    <br>

                                                    <FIELDSET>
                                                        <LEGEND class='block_class'>Database Details</LEGEND>
                                                        <table width=85% border=0 cellpadding="2" cellspacing="2" class=maintext>
                                                            <tr>
                                                                <td colspan="2" class=maintext align="left">Database Server</td>
                                                                <td width="70%" align=left>
                                                                    <input type="text" name="txtDBServerName" id="txtDBServerName" value="<?php echo trim($user_data['txtDBServerName']) <> "" ? htmlentities($user_data['txtDBServerName']) : "localhost"; ?>" /> <img src="css/Help.png" width="20" height="20" title="Database server name,eg:localhost">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" class=maintext align="left">Database Name</td>
                                                                <td width="70%" align=left>
                                                                    <input name="txtDBName"  id="txtDBName" type="text"   class="textbox"  maxlength="100" size="50" value="<?php echo htmlentities($user_data['txtDBName']); ?>" > <img src="css/Help.png" width="20" height="20" title="Name of your Mysql Database">									
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" class=maintext align="left">Database User Name</td>
                                                                <td width="70%" align=left>
                                                                    <input name="txtDBUserName"  id="txtDBUserName" type="text" maxlength="100" size="50" value="<?php echo htmlentities($user_data['txtDBUserName']); ?>"> <img src="css/Help.png" width="20" height="20" title="Mysql Username">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" class=maintext align="left">Database Password</td>
                                                                <td width="70%" align=left>
                                                                    <input name="txtDBPassword"  id="txtDBPassword" type="text"  maxlength="100" size="50" value="<?php echo htmlentities($user_data['txtDBPassword']); ?>"> <img src="css/Help.png" width="20" height="20" title="Mysql Password">
                                                                </td>
                                                            </tr>
                                                            
                                                           

                                                        </table>
                                                    </FIELDSET><br><br>
                                                    <FIELDSET>
                                                        <LEGEND class='block_class'>Site Details</LEGEND>
                                                        <table width=85% border=0 cellpadding="2" cellspacing="2" class=maintext>
                                                            <tr>
                                                                <td colspan="2" class=maintext align="left">Site Name</td>
                                                                <td width="70%" align=left>
                                                                    <input name="txtSiteName"  id="txtSiteName" type="text" maxlength="100" size="50" value="<?php echo htmlentities($user_data['txtSiteName']); ?>"> <img src="css/Help.png" width="20" height="20" title="Official Sitename">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" class=maintext align="left">License Key</td>
                                                                <td width="70%" align=left>
                                                                    <input name="txtLicenseKey"  id="txtLicenseKey" type="text" maxlength="100" size="50" value="<?php echo htmlentities($user_data['txtLicenseKey']); ?>"> <img src="css/Help.png" width="20" height="20" title="License key provided  by iScripts.com">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FIELDSET>
                                                    <br>
                                                    <br>
                                                    <FIELDSET>
                                                        <LEGEND class="block_class">Administration Details</LEGEND>
                                                        <table width=85% border=0 cellpadding="2" cellspacing="2" class=maintext>							
                                                            <tr>
                                                                <td colspan="2" class=maintext  align="left">Admin Email</td>
                                                                <td width="70%" align=left>
                                                                    <input name="txtAdminName"  id="txtAdminName" type="hidden" maxlength="100" size="50" value="admin">

                                                                    <input name="txtAdminPassword"  id="txtAdminPassword" type="hidden" maxlength="100" size="50" value="admin">

                                                                    <input name="txtAdminEmail"  id="txtAdminEmail" type="text"  maxlength="100" size="50" value="<?php echo htmlentities($user_data['txtAdminEmail']); ?>"> <img src="css/Help.png" width="20" height="20" title="Email of Site Admin">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FIELDSET>
                                                    <br>
                                                    <table width=85% border=0 cellpadding="2" cellspacing="2" class=maintext>
                                                        <tr><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td align="center">
                                                                <input type="submit" name="btnContinue" value="Continue" class="buttn_admin">
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    <!-- DO NOT REMOVE -->
                                                    <input type="hidden" name="installerCheck" id="installerCheck" value="1" />
                                                    <!-- ------------- -->
                                                </form>	
                                            </td>
                                        </tr>
                                    </table>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
         <div class="installr_footer"></div>
    </body>
</html>
                            
                            