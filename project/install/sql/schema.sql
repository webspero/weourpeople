-- --------------------------------------------------------
-- Host:                         dev.iscripts.com
-- Server version:               5.5.40-cll - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2016-07-26 20:50:11
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table deviscri_connect10.cms_groups
DROP TABLE IF EXISTS `cms_groups`;
CREATE TABLE IF NOT EXISTS `cms_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) DEFAULT NULL,
  `position` int(3) DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `user_privilege` varchar(256) NOT NULL DEFAULT 'all',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.cms_privileges
DROP TABLE IF EXISTS `cms_privileges`;
CREATE TABLE IF NOT EXISTS `cms_privileges` (
  `privilege_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type` enum('group','section') NOT NULL,
  `entity_id` int(11) NOT NULL,
  `view_role_id` int(11) NOT NULL,
  `add_role_id` int(11) NOT NULL,
  `edit_role_id` int(11) NOT NULL,
  `delete_role_id` int(11) NOT NULL,
  `publish_role_id` int(11) NOT NULL,
  PRIMARY KEY (`privilege_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.cms_roles
DROP TABLE IF EXISTS `cms_roles`;
CREATE TABLE IF NOT EXISTS `cms_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(256) NOT NULL,
  `parent_role_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.cms_sections
DROP TABLE IF EXISTS `cms_sections`;
CREATE TABLE IF NOT EXISTS `cms_sections` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `group_id` int(8) DEFAULT NULL,
  `section_name` varchar(200) DEFAULT NULL,
  `section_alias` varchar(200) DEFAULT NULL,
  `table_name` varchar(100) DEFAULT NULL,
  `section_config` text,
  `visibilty` enum('0','1') NOT NULL,
  `display_order` int(11) NOT NULL,
  `user_privilege` varchar(256) NOT NULL DEFAULT 'all',
  `module` enum('admin','user') NOT NULL DEFAULT 'admin',
  `db_type` enum('main','client') NOT NULL DEFAULT 'main',
  `smb_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.cms_users
DROP TABLE IF EXISTS `cms_users`;
CREATE TABLE IF NOT EXISTS `cms_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('sadmin','admin') NOT NULL,
  `username` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` enum('active','deleted') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.fw_metadata
DROP TABLE IF EXISTS `fw_metadata`;
CREATE TABLE IF NOT EXISTS `fw_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.request
DROP TABLE IF EXISTS `request`;
CREATE TABLE IF NOT EXISTS `request` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `Username` varchar(200) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `Featuresadd` varchar(200) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `Featuresremove` varchar(200) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `GraphicDesign` varchar(200) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `Budget` int(100) NOT NULL DEFAULT '0',
  `Email` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `Phone` int(20) NOT NULL DEFAULT '0',
  `requestdate` varchar(20) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `vCountry` varchar(200) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `vComments` text CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_announcement_comments
DROP TABLE IF EXISTS `tbl_announcement_comments`;
CREATE TABLE IF NOT EXISTS `tbl_announcement_comments` (
  `announcement_comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `announcement_id` bigint(20) NOT NULL,
  `community_id` bigint(20) NOT NULL,
  `comment_content` text NOT NULL,
  `comment_date` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `parent_comment_id` bigint(20) NOT NULL,
  `num_comment_likes` bigint(20) NOT NULL,
  `num_replies` bigint(20) NOT NULL,
  `comment_status` enum('A','I','D') NOT NULL,
  KEY `announcement_comment_id` (`announcement_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_announcement_comments_like
DROP TABLE IF EXISTS `tbl_announcement_comments_like`;
CREATE TABLE IF NOT EXISTS `tbl_announcement_comments_like` (
  `announcement_comment_like_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) NOT NULL,
  `announcement_id` bigint(20) NOT NULL,
  `comment_like` enum('0','1') NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `like_date` datetime NOT NULL,
  PRIMARY KEY (`announcement_comment_like_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_announcement_commment_images
DROP TABLE IF EXISTS `tbl_announcement_commment_images`;
CREATE TABLE IF NOT EXISTS `tbl_announcement_commment_images` (
  `announcement_image_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `announcement_id` bigint(20) NOT NULL DEFAULT '0',
  `comment_id` bigint(20) NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `image_file_id` bigint(20) NOT NULL DEFAULT '0',
  `announcement_image_status` enum('A','I','D') NOT NULL DEFAULT 'A',
  `announcement_image_date` datetime NOT NULL,
  PRIMARY KEY (`announcement_image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_announcement_likes
DROP TABLE IF EXISTS `tbl_announcement_likes`;
CREATE TABLE IF NOT EXISTS `tbl_announcement_likes` (
  `announcement_like_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `community_id` bigint(20) NOT NULL,
  `announcement_id` bigint(20) NOT NULL,
  `announcement_like` enum('0','1') NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `like_date` datetime NOT NULL,
  PRIMARY KEY (`announcement_like_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_announcement_shares
DROP TABLE IF EXISTS `tbl_announcement_shares`;
CREATE TABLE IF NOT EXISTS `tbl_announcement_shares` (
  `announcement_share_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `announcement_id` bigint(20) NOT NULL,
  `community_id` bigint(20) NOT NULL,
  `share_date` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `num_shares` bigint(20) NOT NULL,
  `share_status` enum('A','I','D') NOT NULL,
  KEY `announcement_comment_id` (`announcement_share_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_audit
DROP TABLE IF EXISTS `tbl_audit`;
CREATE TABLE IF NOT EXISTS `tbl_audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audit_table` varchar(255) DEFAULT NULL,
  `audit_data` longtext,
  `where_condition` text,
  `audit_type` enum('insert','update','delete') DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_banners
DROP TABLE IF EXISTS `tbl_banners`;
CREATE TABLE IF NOT EXISTS `tbl_banners` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_title` varchar(250) DEFAULT NULL,
  `banner_image_id` int(11) DEFAULT NULL,
  `banner_description` text,
  `banner_link` varchar(250) DEFAULT NULL,
  `banner_link_text` varchar(250) DEFAULT NULL,
  `banner_image_name` varchar(250) DEFAULT NULL,
  `banner_status` enum('A','I','D') DEFAULT 'A' COMMENT 'A-active,I- inactive,D-deleted',
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_businesses
DROP TABLE IF EXISTS `tbl_businesses`;
CREATE TABLE IF NOT EXISTS `tbl_businesses` (
  `business_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `business_name` varchar(250) NOT NULL,
  `business_email` varchar(250) NOT NULL,
  `business_url` varchar(250) NOT NULL,
  `business_type` enum('Product','Service') NOT NULL COMMENT 'P-product,S-Service',
  `business_image_id` bigint(11) NOT NULL,
  `business_address` text NOT NULL,
  `business_city` varchar(250) NOT NULL,
  `business_state` varchar(10) NOT NULL,
  `business_country` varchar(10) NOT NULL,
  `business_phone` varchar(45) NOT NULL,
  `business_zipcode` varchar(45) NOT NULL,
  `business_industry_id` int(11) NOT NULL,
  `business_alias` varchar(250) NOT NULL,
  `business_description` text NOT NULL,
  `business_created_by` bigint(11) NOT NULL,
  `business_created_on` timestamp NULL DEFAULT NULL,
  `business_status` enum('A','I','D') NOT NULL DEFAULT 'A' COMMENT 'A->active,I->inactive,D->deleted',
  `business_category_id` bigint(20) NOT NULL,
  `business_timeline_image_id` bigint(20) NOT NULL,
  `business_timeline_image_name` varchar(250) NOT NULL,
  `business_timeline_date` datetime NOT NULL,
  `business_logo_id` bigint(20) NOT NULL,
  `business_logo_name` varchar(250) NOT NULL,
  PRIMARY KEY (`business_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_business_affiliates
DROP TABLE IF EXISTS `tbl_business_affiliates`;
CREATE TABLE IF NOT EXISTS `tbl_business_affiliates` (
  `business_affiliate_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `business_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `business_affiliate_created_date` datetime NOT NULL,
  `business_affiliate_status` enum('A','R','D','E') NOT NULL DEFAULT 'A' COMMENT 'A-Active,R-requested,,D-delete,E-exclude rquest',
  PRIMARY KEY (`business_affiliate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_business_testimonials
DROP TABLE IF EXISTS `tbl_business_testimonials`;
CREATE TABLE IF NOT EXISTS `tbl_business_testimonials` (
  `testimonial_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `business_id` bigint(20) NOT NULL,
  `user_company` varchar(250) NOT NULL,
  `user_designation` varchar(250) NOT NULL,
  `testimonial_content` text NOT NULL,
  `tetimonial_date` datetime NOT NULL,
  `testimonial_status` enum('A','I','D') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`testimonial_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_categories
DROP TABLE IF EXISTS `tbl_categories`;
CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(250) NOT NULL,
  `category_status` enum('A','I') NOT NULL DEFAULT 'A' COMMENT 'A - active, I - inactive',
  `category_alias` varchar(250) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_classifieds
DROP TABLE IF EXISTS `tbl_classifieds`;
CREATE TABLE IF NOT EXISTS `tbl_classifieds` (
  `classifieds_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `classifieds_title` varchar(250) NOT NULL,
  `classifieds_alias` varchar(250) NOT NULL,
  `classifieds_description` text NOT NULL,
  `classifieds_created_on` timestamp NULL DEFAULT NULL,
  `classifieds_created_by` bigint(20) NOT NULL,
  `classifieds_status` enum('A','I','D') NOT NULL DEFAULT 'I' COMMENT 'A-approved,I-notapproved,D-deleted',
  `classifieds_category_id` bigint(20) DEFAULT NULL,
  `classifieds_image_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`classifieds_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_cms_settings
DROP TABLE IF EXISTS `tbl_cms_settings`;
CREATE TABLE IF NOT EXISTS `tbl_cms_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_set_name` varchar(100) NOT NULL,
  `cms_set_value` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_comments
DROP TABLE IF EXISTS `tbl_comments`;
CREATE TABLE IF NOT EXISTS `tbl_comments` (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment_content` varchar(250) NOT NULL,
  `comment_created_on` timestamp NULL DEFAULT NULL,
  `comment_created_by` bigint(20) NOT NULL,
  `comment_entity_type` enum('B','M','C','CF') NOT NULL COMMENT 'B - Bussiness, M - Member/User, C - Community,CF - classifieds',
  `comment_entity_id` bigint(20) NOT NULL,
  `comment_status` enum('A','I','D') CHARACTER SET big5 NOT NULL DEFAULT 'A',
  `comment_parent` bigint(20) DEFAULT '0',
  PRIMARY KEY (`comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_communities
DROP TABLE IF EXISTS `tbl_communities`;
CREATE TABLE IF NOT EXISTS `tbl_communities` (
  `community_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `community_name` varchar(250) NOT NULL,
  `community_image_id` bigint(20) NOT NULL,
  `community_created_by` bigint(20) NOT NULL,
  `community_created_on` timestamp NULL DEFAULT NULL,
  `community_status` enum('A','I','D') NOT NULL DEFAULT 'A' COMMENT 'A-active,I-inactive,D-deleted',
  `community_alias` varchar(250) NOT NULL,
  `community_description` text,
  `community_type` enum('PRIVATE','PUBLIC') DEFAULT NULL,
  `community_business_id` bigint(20) NOT NULL,
  `community_member_count` bigint(20) DEFAULT '0',
  `community_category_id` bigint(20) DEFAULT '0',
  `community_logo_id` bigint(20) NOT NULL,
  `community_logo_name` text,
  `community_organization_relation_status` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`community_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_community_announcements
DROP TABLE IF EXISTS `tbl_community_announcements`;
CREATE TABLE IF NOT EXISTS `tbl_community_announcements` (
  `community_announcement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `community_id` bigint(20) NOT NULL,
  `community_announcement_title` text NOT NULL,
  `community_announcement_content` text NOT NULL,
  `community_announcement_date` datetime NOT NULL,
  `community_announcement_created_by` bigint(20) NOT NULL,
  `community_announcement_num_likes` bigint(20) NOT NULL,
  `community_announcement_num_comments` bigint(20) NOT NULL,
  `community_announcement_num_shares` bigint(20) NOT NULL,
  `community_announcement_status` enum('A','I','D') NOT NULL COMMENT 'A-Active,I-Inactive,D-delete',
  `community_announcement_image_id` bigint(20) NOT NULL,
  `community_announcement_image_path` text NOT NULL,
  `community_announcement_alias` varchar(250) NOT NULL,
  `community_announcement_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`community_announcement_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_community_member
DROP TABLE IF EXISTS `tbl_community_member`;
CREATE TABLE IF NOT EXISTS `tbl_community_member` (
  `cmember_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `cmember_id` bigint(20) DEFAULT NULL,
  `cmember_community_id` bigint(20) DEFAULT NULL,
  `cmember_type` enum('A','U') DEFAULT 'U' COMMENT 'A-Admin,U-user',
  `cmember_joined_on` timestamp NULL DEFAULT NULL,
  `cmember_status` enum('A','I','D','P') DEFAULT 'A' COMMENT 'A-active,I-inactive,D-deleted,P-Pending',
  `cmember_inv_permission` enum('Y','N') DEFAULT 'N',
  `cmember_pif_id` int(11) NOT NULL,
  `cmember_campaign_id` bigint(20) NOT NULL,
  `cmember_active_status` enum('A','P') NOT NULL DEFAULT 'P' COMMENT 'A-active,P-passive',
  `cmember_announcement_count` bigint(20) NOT NULL DEFAULT '0' COMMENT 'A-active,P-passive',
  PRIMARY KEY (`cmember_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_content
DROP TABLE IF EXISTS `tbl_content`;
CREATE TABLE IF NOT EXISTS `tbl_content` (
  `content_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content_alias` varchar(100) NOT NULL DEFAULT '',
  `content_title` varchar(100) NOT NULL DEFAULT '',
  `content_description` text NOT NULL,
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_country
DROP TABLE IF EXISTS `tbl_country`;
CREATE TABLE IF NOT EXISTS `tbl_country` (
  `tc_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tc_code` varchar(20) DEFAULT NULL,
  `tc_name` varchar(250) DEFAULT NULL,
  `tc_status` char(1) DEFAULT 'A',
  PRIMARY KEY (`tc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_entity_industry
DROP TABLE IF EXISTS `tbl_entity_industry`;
CREATE TABLE IF NOT EXISTS `tbl_entity_industry` (
  `entity_industry_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entity_name` varchar(100) DEFAULT NULL,
  `entity_id` bigint(20) DEFAULT NULL,
  `entity_status` enum('A','I') DEFAULT NULL COMMENT 'A- active,I-inactive',
  PRIMARY KEY (`entity_industry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_feeds
DROP TABLE IF EXISTS `tbl_feeds`;
CREATE TABLE IF NOT EXISTS `tbl_feeds` (
  `feed_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `feed_type` enum('FRR','FRS','AFR','IFC','JC','NJC','NRC','NRO','A') NOT NULL COMMENT 'FRR-Friend request recieved to me ,AFR-I Accepted Friend Request,IFC-Invite friends to my community,JC-I joined Community,NJC-New Joinee to my community,NRC-New referal campaign created by me,NRO-New referal offer sent by me,FRS-Friend request sent,A-Anno',
  `feed_date` datetime NOT NULL,
  `feed_user_id` bigint(20) NOT NULL,
  `from_user_id` bigint(20) NOT NULL,
  `to_user_id` bigint(20) NOT NULL,
  `community_id` bigint(20) NOT NULL,
  `table_name` varchar(100) NOT NULL,
  `table_key_id` bigint(20) NOT NULL,
  PRIMARY KEY (`feed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='To capture news feeds of users';

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_files
DROP TABLE IF EXISTS `tbl_files`;
CREATE TABLE IF NOT EXISTS `tbl_files` (
  `file_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_orig_name` varchar(255) DEFAULT NULL,
  `file_extension` varchar(10) DEFAULT NULL,
  `file_mime_type` varchar(50) DEFAULT NULL,
  `file_type` varchar(50) DEFAULT NULL COMMENT 'video/photo',
  `file_width` int(4) DEFAULT NULL,
  `file_height` int(4) DEFAULT NULL,
  `file_play_time` int(11) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_path` text,
  `file_status` int(11) DEFAULT NULL,
  `file_title` varchar(255) DEFAULT NULL,
  `file_caption` text,
  `file_tmp_name` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_friends_list
DROP TABLE IF EXISTS `tbl_friends_list`;
CREATE TABLE IF NOT EXISTS `tbl_friends_list` (
  `friends_list_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `friends_list_user_id` bigint(20) NOT NULL,
  `friends_list_friend_id` bigint(20) NOT NULL,
  `friends_list_status` enum('A','I','D') NOT NULL DEFAULT 'A' COMMENT 'A-active,I-inactive,D-deleted',
  `friends_list_view_status` enum('B','P') NOT NULL DEFAULT 'B' COMMENT 'B-Business,P-Personal',
  PRIMARY KEY (`friends_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_Help
DROP TABLE IF EXISTS `tbl_Help`;
CREATE TABLE IF NOT EXISTS `tbl_Help` (
  `help_id` int(11) NOT NULL AUTO_INCREMENT,
  `help_category_id` int(11) DEFAULT NULL,
  `help_title` varchar(100) DEFAULT NULL,
  `help_description` text,
  `help_position` int(11) DEFAULT NULL,
  `help_active` enum('0','1') DEFAULT NULL,
  `help_image` varchar(100) DEFAULT NULL,
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`help_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_HelpCategory
DROP TABLE IF EXISTS `tbl_HelpCategory`;
CREATE TABLE IF NOT EXISTS `tbl_HelpCategory` (
  `help_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `help_category_type` varchar(6) DEFAULT NULL,
  `help_category_title` varchar(100) DEFAULT NULL,
  `help_category_position` int(11) DEFAULT NULL,
  `help_category_active` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`help_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_invitation
DROP TABLE IF EXISTS `tbl_invitation`;
CREATE TABLE IF NOT EXISTS `tbl_invitation` (
  `invitation_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invitation_type` enum('E','U') DEFAULT NULL COMMENT 'E-email,U-User',
  `invitation_entity` enum('U','C') DEFAULT NULL COMMENT 'U - user, C- community',
  `invitation_entity_id` bigint(20) DEFAULT NULL,
  `invitation_sender_email` varchar(250) DEFAULT NULL,
  `invitation_sender_id` bigint(20) DEFAULT NULL,
  `invitation_receiver_id` bigint(20) DEFAULT NULL,
  `invitation_receiver_email` varchar(250) DEFAULT NULL,
  `invitation_created_on` timestamp NULL DEFAULT NULL,
  `invitation_status` enum('P','A','R','DJ') DEFAULT 'P' COMMENT 'P-pending,A-accepted,R-rejected,DJ-disjoined',
  `invitation_action_deffered` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`invitation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_lookup
DROP TABLE IF EXISTS `tbl_lookup`;
CREATE TABLE IF NOT EXISTS `tbl_lookup` (
  `settingfield` varchar(100) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `settinglabel` varchar(250) NOT NULL,
  `groupLabel` varchar(50) NOT NULL,
  `type` varchar(256) NOT NULL,
  `parent_settingfield` varchar(256) NOT NULL,
  `display_order` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_mail_template
DROP TABLE IF EXISTS `tbl_mail_template`;
CREATE TABLE IF NOT EXISTS `tbl_mail_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mail_template_name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `mail_template_sub` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `mail_template_body` text COLLATE latin1_general_ci NOT NULL,
  `mail_template_status` smallint(1) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `test` (`id`),
  FULLTEXT KEY `mail_template_title` (`mail_template_sub`,`mail_template_body`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='templates for all emails sent out from system';

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_messages
DROP TABLE IF EXISTS `tbl_messages`;
CREATE TABLE IF NOT EXISTS `tbl_messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_from_id` int(11) DEFAULT NULL,
  `message_to_id` int(11) DEFAULT NULL,
  `message_detail` text,
  `message_date` datetime DEFAULT NULL,
  `message_status` enum('1','0') DEFAULT '1',
  `message_sent_status` enum('1','0') DEFAULT '1',
  `message_subject` text,
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--Adding new column for tbl_messages--
ALTER TABLE `tbl_messages`
	ADD COLUMN `message_read_status` ENUM('1','0') NULL DEFAULT '0' AFTER `message_subject`;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_newsfeed_comments_like
DROP TABLE IF EXISTS `tbl_newsfeed_comments_like`;
CREATE TABLE IF NOT EXISTS `tbl_newsfeed_comments_like` (
  `newsfeed_comment_like_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `newsfeed_comment_id` bigint(20) NOT NULL,
  `newsfeed_id` bigint(20) NOT NULL,
  `newsfeed_comment_like` enum('0','1') NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `newsfeed_comment_like_date` datetime NOT NULL,
  PRIMARY KEY (`newsfeed_comment_like_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_newsfeed_comment_images
DROP TABLE IF EXISTS `tbl_newsfeed_comment_images`;
CREATE TABLE IF NOT EXISTS `tbl_newsfeed_comment_images` (
  `newsfeed_comment_image_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `newsfeed_id` bigint(20) NOT NULL,
  `newsfeed_comment_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `newsfeed_comment_image_file_id` bigint(20) NOT NULL,
  `newsfeed_comment_image_status` enum('A','I','D') NOT NULL DEFAULT 'A',
  `newsfeed_comment_image_date` datetime NOT NULL,
  PRIMARY KEY (`newsfeed_comment_image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_news_feed
DROP TABLE IF EXISTS `tbl_news_feed`;
CREATE TABLE IF NOT EXISTS `tbl_news_feed` (
  `news_feed_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `news_feed_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `news_feed_user_id` bigint(20) NOT NULL,
  `news_feed_user_name` varchar(250) NOT NULL,
  `news_feed_comment` text NOT NULL,
  `news_feed_image_id` bigint(11) NOT NULL,
  `news_feed_image_name` varchar(250) NOT NULL,
  `news_feed_num_like` bigint(20) NOT NULL DEFAULT '0',
  `news_feed_num_comments` bigint(20) NOT NULL DEFAULT '0',
  `news_feed_alias` varchar(250) NOT NULL,
  PRIMARY KEY (`news_feed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_news_feed_comments
DROP TABLE IF EXISTS `tbl_news_feed_comments`;
CREATE TABLE IF NOT EXISTS `tbl_news_feed_comments` (
  `news_feed_comments_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `news_feed_id` bigint(20) NOT NULL,
  `news_feed_comment_content` text NOT NULL,
  `news_feed_comment_date` datetime NOT NULL,
  `news_feed_comment_user_id` bigint(20) NOT NULL,
  `news_feed_comment_user_name` varchar(250) NOT NULL,
  `parent_comment_id` bigint(20) NOT NULL,
  `num_comment_likes` bigint(20) NOT NULL,
  `num_replies` bigint(20) NOT NULL,
  `news_feed_comment_status` enum('A','I','D') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`news_feed_comments_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_news_feed_likes
DROP TABLE IF EXISTS `tbl_news_feed_likes`;
CREATE TABLE IF NOT EXISTS `tbl_news_feed_likes` (
  `news_feed_like_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `news_feed_id` bigint(20) NOT NULL,
  `news_feed_comment_id` bigint(20) NOT NULL,
  `news_feed_like` enum('0','1') NOT NULL DEFAULT '0',
  `news_feed_like_user_id` bigint(20) NOT NULL,
  `news_feed_like_user_name` varchar(250) NOT NULL,
  `news_feed_like_date` datetime NOT NULL,
  PRIMARY KEY (`news_feed_like_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_organization_announcements
DROP TABLE IF EXISTS `tbl_organization_announcements`;
CREATE TABLE IF NOT EXISTS `tbl_organization_announcements` (
  `organization_announcement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `organization_id` bigint(20) NOT NULL,
  `organization_announcement_content` text NOT NULL,
  `organization_announcement_date` datetime NOT NULL,
  `organization_announcement_created_by` bigint(20) NOT NULL,
  `organization_announcement_num_likes` bigint(20) NOT NULL,
  `organization_announcement_num_comments` bigint(20) NOT NULL,
  `organization_announcement_num_shares` bigint(20) NOT NULL,
  `organization_announcement_status` enum('A','I','D') NOT NULL COMMENT 'A-Active,I-Inactive,D-delete',
  `organization_announcement_image_id` bigint(20) NOT NULL,
  `organization_announcement_image_path` text NOT NULL,
  `organization_announcement_alias` varchar(250) NOT NULL,
  `organization_announcement_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`organization_announcement_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_organization_announcement_comments
DROP TABLE IF EXISTS `tbl_organization_announcement_comments`;
CREATE TABLE IF NOT EXISTS `tbl_organization_announcement_comments` (
  `organization_announcement_comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `announcement_id` bigint(20) NOT NULL,
  `organization_id` bigint(20) NOT NULL,
  `comment_content` text NOT NULL,
  `comment_date` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `parent_comment_id` bigint(20) NOT NULL,
  `num_comment_likes` bigint(20) NOT NULL,
  `num_replies` bigint(20) NOT NULL,
  `comment_status` enum('A','I','D') NOT NULL,
  KEY `organization_announcement_comment_id` (`organization_announcement_comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_organization_announcement_comments_like
DROP TABLE IF EXISTS `tbl_organization_announcement_comments_like`;
CREATE TABLE IF NOT EXISTS `tbl_organization_announcement_comments_like` (
  `organization_announcement_comment_like_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) NOT NULL,
  `announcement_id` bigint(20) NOT NULL,
  `comment_like` enum('0','1') NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `like_date` datetime NOT NULL,
  PRIMARY KEY (`organization_announcement_comment_like_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_organization_announcement_commment_images
DROP TABLE IF EXISTS `tbl_organization_announcement_commment_images`;
CREATE TABLE IF NOT EXISTS `tbl_organization_announcement_commment_images` (
  `organization_announcement_image_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `announcement_id` bigint(20) NOT NULL DEFAULT '0',
  `comment_id` bigint(20) NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `image_file_id` bigint(20) NOT NULL DEFAULT '0',
  `announcement_image_status` enum('A','I','D') NOT NULL DEFAULT 'A',
  `announcement_image_date` datetime NOT NULL,
  PRIMARY KEY (`organization_announcement_image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_organization_announcement_likes
DROP TABLE IF EXISTS `tbl_organization_announcement_likes`;
CREATE TABLE IF NOT EXISTS `tbl_organization_announcement_likes` (
  `organization_announcement_like_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `organization_id` bigint(20) NOT NULL,
  `announcement_id` bigint(20) NOT NULL,
  `announcement_like` enum('0','1') NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `like_date` datetime NOT NULL,
  PRIMARY KEY (`organization_announcement_like_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_organization_announcement_shares
DROP TABLE IF EXISTS `tbl_organization_announcement_shares`;
CREATE TABLE IF NOT EXISTS `tbl_organization_announcement_shares` (
  `organization_announcement_share_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `announcement_id` bigint(20) NOT NULL,
  `organization_id` bigint(20) NOT NULL,
  `share_date` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `num_shares` bigint(20) NOT NULL,
  `share_status` enum('A','I','D') NOT NULL,
  KEY `organization_announcement_comment_id` (`organization_announcement_share_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_rating
DROP TABLE IF EXISTS `tbl_rating`;
CREATE TABLE IF NOT EXISTS `tbl_rating` (
  `rating_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rating_value` int(11) NOT NULL,
  `rating_entity` enum('B','C') NOT NULL,
  `rating_entity_id` bigint(20) NOT NULL,
  `rating_rated_by` bigint(20) NOT NULL,
  `rating_rated_on` timestamp NULL DEFAULT NULL,
  `rating_status` enum('A','I') NOT NULL DEFAULT 'A' COMMENT 'A- active,I-inactive',
  PRIMARY KEY (`rating_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_state
DROP TABLE IF EXISTS `tbl_state`;
CREATE TABLE IF NOT EXISTS `tbl_state` (
  `ts_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tc_id` bigint(20) DEFAULT '0',
  `ts_code` varchar(20) DEFAULT NULL,
  `ts_name` varchar(250) DEFAULT NULL,
  `ts_status` char(1) DEFAULT 'A',
  PRIMARY KEY (`ts_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_tagged_business
DROP TABLE IF EXISTS `tbl_tagged_business`;
CREATE TABLE IF NOT EXISTS `tbl_tagged_business` (
  `tagg_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `business_id` int(10) DEFAULT NULL,
  `role` text,
  `tag_buisness_email` varchar(100) NOT NULL,
  `status` enum('A','I','D') DEFAULT 'A',
  `affiliation_status` enum('Y','N','R') NOT NULL DEFAULT 'N' COMMENT 'R-requested,Y-yes,N-no',
  PRIMARY KEY (`tagg_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_users
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(250) NOT NULL,
  `user_lastname` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_paypal_email` varchar(250) NOT NULL,
  `user_password` varchar(250) NOT NULL,
  `user_gender` enum('M','F') DEFAULT NULL COMMENT 'M-male,F-female',
  `user_image_id` bigint(11) NOT NULL,
  `user_image_name` varchar(250) NOT NULL,
  `user_timeline_image_id` bigint(20) NOT NULL,
  `user_timeline_image_name` varchar(250) NOT NULL,
  `user_date_of_birth` date DEFAULT NULL,
  `user_address` text NOT NULL,
  `user_city` varchar(250) NOT NULL,
  `user_state` varchar(10) NOT NULL,
  `user_country` varchar(10) NOT NULL,
  `user_phone` varchar(45) NOT NULL,
  `user_zipcode` varchar(45) NOT NULL,
  `user_industry_id` int(11) NOT NULL,
  `user_membership_type` varchar(45) NOT NULL,
  `user_points` int(11) NOT NULL,
  `user_alias` varchar(250) NOT NULL,
  `user_last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_created_by` bigint(11) NOT NULL,
  `user_created_on` timestamp NULL DEFAULT NULL,
  `user_friends_count` bigint(20) DEFAULT '0',
  `user_community_count` bigint(20) DEFAULT '0',
  `user_business_count` bigint(20) DEFAULT '0',
  `user_status` enum('A','I','D') NOT NULL DEFAULT 'A' COMMENT 'A->active,I->inactive,D->deleted',
  `user_privacy` enum('SA','H','SM') NOT NULL DEFAULT 'SM',
  `user_reset_password` enum('Y','N') DEFAULT 'Y',
  `user_token_id` varchar(250) NOT NULL,
  `user_friends_privacy` enum('Y','N') NOT NULL DEFAULT 'N',
  `user_message_count` bigint(20) NOT NULL DEFAULT '0',
  `user_friend_request_count` bigint(20) NOT NULL DEFAULT '0',
  `user_referal_request_count` bigint(20) NOT NULL DEFAULT '0',
  `user_online_status` enum('Online','Offline','Idle') DEFAULT 'Offline',
  `user_last_activity` timestamp NULL DEFAULT NULL,
  `user_private_request_count` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table deviscri_connect10.tbl_users_chat
DROP TABLE IF EXISTS `tbl_users_chat`;
CREATE TABLE IF NOT EXISTS `tbl_users_chat` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `message` text NOT NULL,
  `sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recd` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
-- Data exporting was unselected.


--Adding new column for tbl_users--
ALTER TABLE tbl_users DROP COLUMN user_last_login;
ALTER TABLE tbl_users ADD COLUMN user_last_login timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
-- Data exporting was unselected.

/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
