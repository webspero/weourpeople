<?php

define("MAIN_ERROR_MSG","Sorry! Unexpected error");
define("SUCCESS","SUCCESS");
define("ERROR","ERROR");
define("ERROR_CLASS","error_div");
define("SUCCESS_CLASS","success_div");

/*------------User Related Variables Defined Here ----*/

define("USER_EMAIL_EXIST","Sorry! Email Already Exists");
define("USER_NAME_EXIST","Sorry! Email Already Exists");
define("USER_EMAIL_NOT_EXIST","Sorry! Email Address Not Exists");
define("USER_NAME_NOT_EXIST","Sorry! Username Not Exists");
define("USER_NOT_EXIST","Invalid Credentials");
define("USER_ID_NOT_EXIST","Sorry! User does not  exist");
define("BUSINESS_ID_NOT_EXIST","Sorry! Organization does not  exist");
define("USER_DELETED_SUCCESS","User Deleted Successfully");
define("USER_NOT_PUBLISHED","User not Approved");
define("USER_PASSWORD_RESETTED_SUCCESS","Your New Password Has Been Mailed To The Email Address");
define("USER_PASSWORD_RESETTED_FAILED","Your Password Reset Failed");
define("USER_PASSWORD_UPDATED_SUCCESS","Your Password Changed Successfully");
define("USER_PASSWORD_UPDATED_FAILED","Your Password Updation Failed");
define("USER_PASSWORD_WRONG","Your current pasword is wrong.Please retry!");
define("USER_PUBLISHED_SUCCESS","Your Account Published Successfully");
define("USER_PUBLISHED_FAILURE","Your Account Publishing Failed");
define("REGISTER_SUCCESS",'Successfully Registered. ');
define("FRIEND_SUCCESS",' has been added as your friend.');
define("BUSINESS_SUCCESS",'Successfully Created Business. ');
define("BUSINESS_EDIT_SUCCESS",'Successfully Edited  Business');
define("INVITATION_SEND_SUCCESS",'Invitations sent ');
define("INVITATION_SEND_FALIURE",'Invitations Could Not Be Sent. Please Try Again.');
define("CONTACTS_NOT_SELECTED",'You have not selected anyone. No Invitations have been sent.');
define("PROFILE_PIC_CHANGED",'Profile picture changed successfully.');
define("COMMUNITY_SUCCESS1",'Successfully joined the group. ');
define("COMMUNITY_SUCCESS_APPROVAL",'Awaiting Approval By Administrator.');
define("INVITATION_ACCEPTED","You have already responded to the request.");
define("REJECT_SUCCESS",'You have rejected the request ');
define("BUSINESS_DELETED","Business deleted successfully.");
define("BUSINESS_DELETED_FAIL","Business could not be deleted");
define("USER_NOT_ACTIVATED","Your Account need to be activated by the Administrator");
/*------------User Related Variables Ends Here ----*/


/*------------Biz Group Related Variables Defined Here ----*/
define("GROUP_ID_NOT_EXIST","Invalid Parameters");
define("GROUP_NOT_EXIST","Sorry! Either Group Credentials Invalid Or Group Not Exists");
define("GROUP_DELETED_SUCCESS","Group status changed successfully");
/*------------Biz Group Related Variables Ends Here ----*/

/*------------Group Related Variables Defined Here ----*/
define("COMMUNITY_ID_NOT_EXIST","Group doesn't exist");
define("COMMUNITY_NOT_EXIST","Sorry! Either Group Credentials Invalid Or Group Not Exists");
define("COMMUNITY_SUCCESS",'Successfully Created Group. ');
define("COMMUNITY_ED_SUCCESS",'Successfully Edited Group');
define("COMMUNITY_DELETED_SUCCESS","Group status changed successfully");
define("COMMUNITY_MEMBER","You are now a member of this Group!");
define("COMMUNITY_MEMBER_UNJOIN","You are Successfully Disjoined from the Group!");
define("COMMUNITY_MEMBER_PRIVATE","Group Admin will Verify Your Request!");
define("COMMUNITY_REQ_ACCEPT","Request Accepted!");
define("COMMUNITY_REQ_DECLINE","Request Declined!");
define("COMMUNITY_INVITE_SUCCESS", "Invited to Group Successfully! ");
define("COMMUNITY_DELETED","Group has been deleted ");
define("COMMUNITY_DELETED_FAIL","Group could not be deleted ");

/*------------Community Related Variables Ends Here ----*/

/*------------Comments Related Variables Defined Here ----*/
define("COMMENT_DELETED","Comment deleted successfully");
define("TESTIMONIAL_DELETED","Testimonial deleted successfully");
define("COMMENT_DELETED_FAIL","Comment could not be deleted");
define("TESTIMONIAL_DELETED_FAIL","Testimonial could not be deleted");
define("COMMENT_ID_NOT_EXIST","Invalid Parameters");
define("TESTIMONIAL_ID_NOT_EXIST","Invalid Parameters");

/*------------Comments Related Variables Ends Here ----*/


define("INQUIRY_MAIL","Inquiry sent successfully!");

define("CLASSIFIEDS_SUCCESS",'Successfully Created Classifieds.');
define("CLASSIFIEDS_ID_NOT_EXIST","Classifieds doesn't exist ");
define("CLASSIFIEDS_ED_SUCCESS",'Successfully Edited Classifieds ');
define("CLASSIFIEDS_DELETED","Classifieds has been deleted ");
define("CLASSIFIEDS_DELETED_FAIL","Classifieds could not be deleted ");

define('TAG_SUCCESS' , 'Success');
define('EDIT_TAG_SUCCESS' , 'Success');
define('INVALID_PAR', "Invalid Parametes");

/*---------------Charity Item--------------*/
define("CHARITY_ID_NOT_EXIST","Charity doesn't exist");
define("CHARITY_DELETED","Charity has been deleted ");
define("CHARITY_DELETED_FAIL","Charity could not be deleted ");

?>
