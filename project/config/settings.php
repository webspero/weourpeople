<?php 
error_reporting(0);
ini_set('display_errors', 1);
define('ENVIRONMENT', 'GLOBAL');

define('MYSQL_HOST',        'HOST_NAME');
define('MYSQL_USERNAME',    'USER_NAME');
define('MYSQL_PASSWORD',    'DB_PASSWORD');
define('MYSQL_DB',          'DB_NAME');
define('MYSQL_TABLE_PREFIX','tbl_');

define('MYSQL_TABLE_PREFIX','tbl_');

$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
$protocol = substr(strtolower($_SERVER["SERVER_PROTOCOL"]), 0, strpos(strtolower($_SERVER["SERVER_PROTOCOL"]), "/")) .$s . "://";
$currentDomain =  $_SERVER['SERVER_NAME'];
$path = $_SERVER['PHP_SELF'];
$path_parts = pathinfo($path);
$directory = $path_parts['dirname'];
if($directory=='' || $directory=='/'){
    $baseUrl = $protocol.$currentDomain.'/';
}else{
    $directory = explode("/index.php",$directory);
    $directory = ($directory == "/") ? "" : $directory[0].'/';
    $baseUrl = $protocol.$currentDomain.$directory;
}
define('BASE_URL',$baseUrl); 
define('FILE_UPLOAD_DIR', BASE_PATH . "project/files/");
define('IMAGE_ROOT_URL',BASE_PATH.'project/styles/images/');
define('PROJECT_URL', BASE_URL . 'project/');
define('EXTERNAL_API_URL', BASE_URL . 'project/lib/');
define('FILE_UPLOAD_TABLE', MYSQL_TABLE_PREFIX . "files");

define('PRODUCT_INSTALLER',1);

?>

