<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
// +----------------------------------------------------------------------+
// | file to define the global variables needed in the application        |
// | File name : globals.php                                                 |
// | PHP version >= 5.2                                                   |
// | Created On 19 Dec 2011                                               |
// +----------------------------------------------------------------------+
// | Author: JINSON MATHEW <jinson.m@armiasystems.com>                    |
// +----------------------------------------------------------------------+
// | Copyrights Armia Systems ? 2011                                      |
// | All rights reserved                                                  |
// +----------------------------------------------------------------------+
// | This script may not be distributed, sold, given away for free to     |
// | third party, or used as a part of any internet services such as      |
// | webdesign etc.                                                       |
// +----------------------------------------------------------------------+

define('ADMIN_APPROVE_USER_AUTO', 1);

$arrSettings = Utils::getSettings();
//echopre($arrSettings);

foreach ($arrSettings as $data) {
    if ($data->settingfield == 'perpage') {
        if ($data->value != '') {
            define('PAGE_LIST_COUNT', $data->value);
        } else {
            define('PAGE_LIST_COUNT', 5);
        }
    }
    if ($data->settingfield == 'vadmin_email') {
        if ($data->value != '') {
            define('ADMIN_EMAILS', $data->value);
        }
    }
    if ($data->settingfield == 'sitename') {
        if ($data->value != '') {
            define('SITE_NAME', $data->value);
        } else {
            define('SITE_NAME', 'Socialware');
        }
    }
    if ($data->settingfield == 'metatagKey') {
        if ($data->value != '') {
            define('META_KEYWORDS', $data->value);
        } else {
            define('META_KEYWORDS', SITE_NAME);
        }
    }
    if ($data->settingfield == 'metaTitle') {
        if ($data->value != '') {
            define('META_TITLE', $data->value);
        } else {
            define('META_TITLE', SITE_NAME);
        }
    }
    if ($data->settingfield == 'metatagDes') {
        if ($data->value != '') {
            define('META_DES', $data->value);
        } else {
            define('META_DES', SITE_NAME);
        }
    }
    if ($data->settingfield == 'SMTPHost') {
        if ($data->value != '') {
            define('SMTP_HOST', $data->value);
        }
    }
    if ($data->settingfield == 'SMTPUsername') {
        if ($data->value != '') {
            define('SMTP_USERNAME', $data->value);
        }
    }
    if ($data->settingfield == 'SMTPPassword') {
        if ($data->value != '') {
            define('SMTP_PASSWORD', $data->value);
        }
    }
    if ($data->settingfield == 'SMTPPort') {
        if ($data->value != '') {
            define('SMTP_PORT', $data->value);
        }
    }
    if ($data->settingfield == 'SSLEnabled') {
        if ($data->value != '') {
            define('SMTP_SSL', $data->value);
        }
    }

    if ($data->settingfield == 'sitelogo' and $data->value != '') {
        if ($data->value != '') {
            define('SITE_LOGO', BASE_URL . 'project/files/logo/' . $data->value);
        } else {
            define('SITE_LOGO', BASE_URL . 'project/themes/default/images/logo.png');
        }
    }
    
    if ($data->settingfield == 'innerlogo' and $data->value != '') {
        if ($data->value != '') {
            define('INNER_LOGO', BASE_URL . 'project/files/logo/' . $data->value);
        } else {
            define('INNER_LOGO', BASE_URL . 'project/themes/default/images/logo.png');
        }
    }
    
    if ($data->settingfield == 'CloudSpongekey' and $data->value != '') {
        if ($data->value != '') {
            define('CLOUDSPONGE_KEY', $data->value);
        } else {
            define('CLOUDSPONGE_KEY', '');
        }
    }
    if ($data->settingfield == 'ADMIN_APPROVE_CLASSIFIEDS_AUTO') {
        if ($data->value != '') {
            define('ADMIN_APPROVE_CLASSIFIEDS_AUTO', $data->value);
        }
//  		else{
//  			define('ADMIN_APPROVE_CLASSIFIEDS_AUTO', 0);
//  		}
    }
    if ($data->settingfield == 'auto_approval') {
        if ($data->value != '') {
            define('ADMIN_APPROVE_ORG_AUTO', $data->value);
        }
    }
    if ($data->settingfield == 'ADMIN_APPROVE_BIZCOM_AUTO') {
        if ($data->value != '') {
            define('ADMIN_APPROVE_BIZCOM_AUTO', $data->value);
        }
    }
    if ($data->settingfield == 'ADMIN_APPROVE_USER_AUTO') {
        if ($data->value != '') {
            define('ADMIN_APPROVE_USER_AUTO', $data->value);
        }
    }
    if ($data->settingfield == 'Paypal_api_username' and $data->value != '') {
        if ($data->value != '') {
            define('PAYPAL_USERNAME', $data->value);
        } else {
            define('PAYPAL_USERNAME', '');
        }
    }
    if ($data->settingfield == 'PayPal_api_password' and $data->value != '') {
        if ($data->value != '') {
            define('PAYPAL_PASSWORD', $data->value);
        } else {
            define('PAYPAL_PASSWORD', '');
        }
    }
    if ($data->settingfield == 'PayPal_api_signature' and $data->value != '') {
        if ($data->value != '') {
            define('PAYPAL_SIGNATURE', $data->value);
        } else {
            define('PAYPAL_SIGNATURE', '');
        }
    }
    if ($data->settingfield == 'PayPall_test_mode' and $data->value != '') {
        if ($data->value != '') {
            define('PAYPAL_SANDBOX_STATUS', $data->value);
        } else {
            define('PAYPAL_SANDBOX_STATUS', '');
        }
    }
    if ($data->settingfield == 'currency_code' and $data->value != '') {
        if ($data->value != '') {
            define('PAYPAL_CURRENCY_CODE', $data->value);
        } else {
            define('PAYPAL_CURRENCY_CODE', '');
        }
    }
    if ($data->settingfield == 'enable_fb') {
        if ($data->value != '') {
            define('FB_ENABLED', $data->value);
        } else {
            define('FB_ENABLED', 0);
        }
    }
    if ($data->settingfield == 'fb_app_id') {
        if ($data->value != '') {
            define('FB_APP_ID', $data->value);
        } else {
            define('FB_APP_ID', '');
        }
    }
    if ($data->settingfield == 'fb_app_secret') {
        if ($data->value != '') {
            define('FB_APP_SECRET', $data->value);
        } else {
            define('FB_APP_SECRET', '');
        }
    }
    if ($data->settingfield == 'twitter_app_id') {
        if ($data->value != '') {
            define('TWITTER_APP_ID', $data->value);
        } else {
            define('TWITTER_APP_ID', '');
        }
    }
    if ($data->settingfield == 'organization_display') {
        if ($data->value != '') {
            define('ORG_DISPLAY', $data->value);
        } else {
            define('ORG_DISPLAY', '');
        }
    }
   // echo ORG_DISPLAY1;
    if ($data->settingfield == 'group_display') {
        if ($data->value != '') {
            define('GRP_DISPLAY', $data->value);
        } else {
            define('GRP_DISPLAY', '');
        }
    }
    if ($data->settingfield == 'friend_display') {
        if ($data->value != '') {
            define('FRND_DISPLAY', $data->value);
        } else {
            define('FRND_DISPLAY', '');
        }
    }
    
    if ($data->settingfield == 'galleryForUsers') {
       if ($data->value != '') {
            define('GALLERY_DISPLAY', $data->value);
        } else {
            define('GALLERY_DISPLAY', '');
        }
    }
    
    if ($data->settingfield == 'pageGroupRelation') {
        if ($data->value != '') {
            define('ORG_GRP', $data->value);
        } else {
            define('ORG_GRP', '');
        }
    }
    
     //define('ORG_GRP', '1');
    if ($data->settingfield == 'theme') {
        if ($data->value != '') {
            define('CURRENT_THEME', $data->value);
            $CURRENT_THEME =$data->value;
        } else {
            define('CURRENT_THEME', '');
             $CURRENT_THEME ='default';
        }
    }
     if ($data->settingfield == 'chat_display') {
        if ($data->value != '') {
            define('CHAT_DISPLAY', $data->value);
            $CHAT_DISPLAY =$data->value;
        } else {
            define('CHAT_DISPLAY', '');
             $CHAT_DISPLAY ='';
        }
    }
    if ($data->settingfield == 'sitelabel') {
        if ($data->value != '') {
            define('SITELABEL', $data->value);
        } else {
            define('SITELABEL', '');
        }
       // echo SITELABEL;exit;
    }
    if ($data->settingfield == 'sitedescription') {
        if ($data->value != '') {
            define('SITEDESCRIPTION', $data->value);
        } else {
            define('SITEDESCRIPTION', '');
        }
       // echo SITELABEL;exit;
    }
//    if ($data->settingfield == 'favicon_img') {
//        if ($data->value != '') {
//            define('FAVICON', $data->value);
//        } else {
//            define('FAVICON', '');
//        }
//       // echo SITELABEL;exit;
//    }
}
define('FAVICON', 'favicon.ico');
define('PROJECT_URL', BASE_URL . 'project/');
define('EXTERNAL_API_URL', BASE_URL . 'project/lib/');
define('FILE_UPLOAD_TABLE', MYSQL_TABLE_PREFIX . "files");
define('IMAGE_URL', BASE_URL . 'project/images/');
define('IMAGE_MAIN_URL', BASE_URL . 'project/styles/images/');
define('USER_IMAGE_URL', BASE_URL . 'project/files/');
define('USER_IMAGE_URL_DEFAULT', BASE_URL . 'project/files/default/');

define('DB_BASED_SESSION', false);
define('CACHE_ENABLED', false);
define('CACHE_TYPE', 'memcache');
define('CACHE_SERVER', '192.168.0.11');
define('CACHE_PORT', 11211);

// CMS
define('CMS_DEVELOPER_USERNAME', 'developer');
define('CMS_DEVELOPER_PASSWORD', 'developer');
define('CMS_ROLES_ENABLED', TRUE);
define('GLOBAL_DATE_FORMAT_SEPERATOR', "/");

define('SMARTY_ENABLED', 1);
define('DYNAMIC_THEME_ENABLED', 1);
define('THEME_IMAGE_URL', BASE_URL . 'project/themes/' . $CURRENT_THEME . '/images/');
define('LAZYLOAD_COUNT', PAGE_LIST_COUNT);
PageContext::$metaTitle = META_TITLE;
PageContext::addJsVar("mainUrl", BASE_URL);
PageContext::addJsVar("MAIN_URL", BASE_URL);
PageContext::addJsVar("CLOUDSPONGE_KEY", CLOUDSPONGE_KEY);

function DisplayLookUp($name) {
    $sql = mysqli_query("select set_value from " . MYSQL_TABLE_PREFIX . "settings where set_name='" . addslashes($name) . "'") or die(mysqli_error());
    if (mysqli_num_rows($sql) > 0) {
        return (mysqli_result($sql, 0, 'set_value'));
    }//end if
}

function updateSettings($param, $value) {
    if ($param != '' && $value != '') {
        $sql = mysqli_query("UPDATE " . MYSQL_TABLE_PREFIX . "settings SET set_value = '" . addslashes($value) . "' WHERE set_name='" . addslashes($param) . "'") or die(mysqli_error());
    }
}

/*
  Function to print the array
 */

function echopre($printArray) {
    echo "<pre>";
    print_r($printArray);
    echo "</pre>";
}

function echopre1($printArray) {
    echo "<pre>";
    print_r($printArray);
    echo "</pre>";
    exit();
}

function fdate($dateval) {
    if ($dateval != '')
        return date('m-d-Y', $dateval);
}

/* Email validation */

function is_valid_email($address) {
    $rx = "^[a-z0-9\\_\\.\\-]+\\@[a-z0-9\\-]+\\.[a-z0-9\\_\\.\\-]+\\.?[a-z]{1,4}$";
    return (preg_match("~" . $rx . "~i", $address));
}

/*
  function to calculate the age. It shows how old our details with the current time

 */

function time_elapsed_string($date) {
    //echo xdebug_is_enabled();
//    //return time();
//    echo $date.'----';
//    echo date('Y-m-d H:i:s');
    
// echo date('Y-m-d H:i:s', strtotime('+8 hour'));
// echo 'current time-'.date("Y-m-d H:i:s");
// echo 'last login-'. date("Y-m-d H:i:s");
//     $time =  strtotime($date)-time(); // to get the time since that moment
//     return $time;//1482361512-1482322102
//   
//     $days    = floor($time / 86400);
//$hours   = floor(($time - ($days * 86400)) / 3600);
//$minutes = floor(($time - ($days * 86400) - ($hours * 3600))/60);
//$seconds = floor(($time - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
//     
//     return $days.' days -'.$hours.' hours -'.$minutes.'minutes -'.$seconds.'seconds ';
     
     
//     
//     $time = ($time<1)? 1 : $time;
//    
//    $tokens = array (
//        31536000 => 'year',
//        2592000 => 'month',
//        604800 => 'week',
//        86400 => 'day',
//        3600 => 'hour',
//        60 => 'minute',
//        1 => 'second'
//    );
//
//    foreach ($tokens as $unit => $text) {
//        if ($time < $unit) continue;
//        $numberOfUnits = floor($time / $unit);
//        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'').' ago';
//    }

//    
////    
    
    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

    $now = time();
    $unix_date = strtotime($date);

    // check validity of date
    if (empty($unix_date)) {
        return "Bad date";
    }

// is it future date or past date
    if ($now > $unix_date) {
        $difference = $now - $unix_date;
        $tense = "ago";
    } else {
        $difference = $unix_date - $now;
        $tense = "ago";
    }

    for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    if ($difference != 1) {
        $periods[$j].= "s";
    }

    return "$difference $periods[$j] {$tense}";
}

function verify_email($email) {

    if (!preg_match('/^[_A-z0-9-]+((\.|\+)[_A-z0-9-]+)*@[A-z0-9-]+(\.[A-z0-9-]+)*(\.[A-z]{2,4})$/', $email)) {
        return false;
    } else {
        return true;
    }
}

// Function to refine call_user_func() [php compatibility fix]
function call_user_func_refined($functionName, $params1 = null, $params2 = null) {
    if (phpversion() < '5.2') {
        $dataVal = explode("::", $functionName);
        if ($params1 != '' && $params2 != '')
            $functionVal = call_user_func(array($dataVal[0], $dataVal[1]), $params1, $params2);
        else if ($params1 != '')
            $functionVal = call_user_func(array($dataVal[0], $dataVal[1]), $params1);
        else
            $functionVal = call_user_func(array($dataVal[0], $dataVal[1]));
    }else {
        if ($params1 != '' && $params2 != '')
            $functionVal = call_user_func($functionName, $params1, $params2);
        else if ($params1 != '')
            $functionVal = call_user_func($functionName, $params1);
        else
            $functionVal = call_user_func($functionName);
    }
    return $functionVal;
}
// Php backward compatibility Fixes
?>
