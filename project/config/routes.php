<?php
//alias definition
  Router::alias(":static", "(aboutus|features|terms|privacy-policy|faq|site-map|disclaimer|specialterms)");

//redirection
////route configuration
Router::connect("fw", "framework");
Router::connect("admin", "cms/cms");
Router::connect("admin/", "cms/cms");
Router::connect("admin/:alias", "cms/cms");
Router::connect("cms", "cms/cms");
Router::connect("cms/", "cms/cms");
//CMS
Router::connect("cms/developer/", "cms/cms/developer/");
Router::connect("cms/:alias", "cms/$1");

//Home page
Router::connect(":static", "index/staticpages/$1");
Router::connect("join", "index/join");
Router::connect("login", "index/login");
Router::connect("lost-password", "index/lostpassword");
Router::connect("searchprofile", "index/searchprofile");

//Member page
Router::alias(":alias", "(.*)");
Router::connect("account/:alias", "user/home/$1");
Router::connect("newsfeed/:alias", "user/newsfeed/$1");
Router::connect("timeline/:alias", "user/timeline/$1");
Router::connect("logout", "index/logout");
Router::connect("organisation-list", "user/organization_list");
Router::connect("groups", "user/group_list");
Router::connect("my-groups", "user/my_groups");
Router::connect("my-subscribed-group", "user/my_subscribed_group");

Router::connect("add-page", "user/add_organization");

Router::connect("add-group", "user/add_group");
Router::connect("edit-page/:alias", "user/edit_organization/$1");
Router::connect("edit-group/:alias", "user/edit_group/$1");
Router::connect("group/:alias", "user/group/$1");
Router::connect("page/:alias", "user/organization/$1");
Router::connect("directory/add", "user/add_organization");
Router::connect("myprofile", "user/myprofile");
Router::connect("listfriends", "user/listfriends");
Router::connect("pending-request", "user/pendingrequests");
Router::connect("autocomplete", "index/profileautocomplete");
Router::connect("friendautocomplete", "index/friendautocomplete");
Router::connect("unfriend", "user/unfriend");
Router::connect("inactiveuser", "index/inactiveuser");
Router::connect("error", "index/error");
Router::connect("announcement-detail/:alias", "user/announcement_detail/$1");
Router::connect("newsfeed-detail/:alias", "user/newsfeed_detail/$1");
Router::connect("my-gallary", "user/my_gallary");
Router::connect("communitydiscussion", "user/communitydiscussion");
Router::connect("organizationdiscussion", "user/organizationdiscussion");
Router::connect("pending-members", "user/my_pending_members");
Router::connect("private-membership-request/:alias", "user/private_membership_request/$1");
//invitation
Router::connect("accept-invitation/:alias", "index/accept_invitation/$1");
Router::connect("invitation-sent", "user/invitation_sent");
Router::connect("changepassword", "user/changepassword");
Router::connect("sendmessage", "user/sendmessage");
Router::connect("inbox", "user/inbox");
Router::connect("sentitems", "user/sent_items");

Router::connect("add-announcement", "user/add_announcement");
Router::connect("my-friends", "user/my_friends");
Router::connect("org-groups/:alias", "user/my_organization_groups/$1");
Router::connect("pages", "user/organisations_list");

Router::connect("contact", "index/contact");
Router::connect("feedback", "index/feedback");


 ?>
