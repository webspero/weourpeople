ALTER TABLE `tbl_messages` ADD COLUMN `message_read_status` ENUM('1','0') NULL DEFAULT '0' AFTER `message_subject`;
ALTER TABLE tbl_users DROP COLUMN user_last_login;
ALTER TABLE tbl_users ADD COLUMN user_last_login timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
