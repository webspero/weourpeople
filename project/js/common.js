      $( function()
{

    var targets = $( '[rel~=tooltip]' ),
        target  = false,
        tip = '',
        tooltip = false;
        //title   = false;

    targets.bind( 'mouseenter', function()
    {

        target  = $( this );
        //alert(target);
        tip     = target.attr( 'title' );
        tooltip = $( '<div id="tooltip"></div>' );

        if( !tip || tip == '' )
            return false;

        target.removeAttr( 'title' );
        tooltip.css( 'opacity', 0 )
               .html( tip )
               .appendTo( 'body' );

        var init_tooltip = function()
        {
            if( $( window ).width() < tooltip.outerWidth() * 1.5 )
                tooltip.css( 'max-width', $( window ).width() / 2 );
            else
                tooltip.css( 'max-width', 340 );

            var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                pos_top  = target.offset().top - tooltip.outerHeight() - 20;

            if( pos_left < 0 )
            {
                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                tooltip.addClass( 'left' );
            }
            else
                tooltip.removeClass( 'left' );

            if( pos_left + tooltip.outerWidth() > $( window ).width() )
            {
                pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                tooltip.addClass( 'right' );
            }
            else
                tooltip.removeClass( 'right' );

            if( pos_top < 0 )
            {
                var pos_top  = target.offset().top + target.outerHeight();
                tooltip.addClass( 'top' );
            }
            else
                tooltip.removeClass( 'top' );

            tooltip.css( { left: pos_left, top: pos_top } )
                   .animate( { top: '+=10', opacity: 1 }, 50 );
        };

        init_tooltip();
        $( window ).resize( init_tooltip );

        var remove_tooltip = function()
        {
            tooltip.animate( { top: '-=10', opacity: 0 }, 50, function()
            {
                $( this ).remove();
            });

            target.attr( 'title', tip );
        };

        target.bind( 'mouseleave', remove_tooltip );
        tooltip.bind( 'click', remove_tooltip );
    });
});

/**
 * Check whether image is valid for upload
 *
 * @param {string} tagId - input file tag id
 * @param {int} maxHeight - maximum height for image
 * @param {int} maxWidth - maximum width for image
 * @param {string} uploadUrl - url to which the image to be uploaded
 * @param {string} affectedClass - class which will be affected on success call back
 * @param imagefolder
 *
 * @return void
 *
 * */
var validImageCheck = function(tagId, maxWidth, maxHeight, uploadUrl, affectedClass,imagefolder) {
    var fd = new FormData();
    var file = $('#'+tagId)[0].files[0];
    fd.append('file', file);

    //Get reference of FileUpload.
    var fileUpload = document.getElementById(tagId);

    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpeg|.jpg|.png|.gif)$");
    if (regex.test(fileUpload.value.toLowerCase())) {

        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {

            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {

                //Initiate the JavaScript Image object.
                var image = new Image();

                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;

                //Validate the File Height and Width.
                image.onload = function () {

                    var height = this.height;
                    var width = this.width;
                    
                    if (width < maxWidth || height < maxHeight) {
                        swal("Error","Height must be more than "+maxHeight+"px and width must be more than "+maxWidth+"px","error");
                    } else {
//                        if(affectedClass == 'timeline_left_pic'){
//                            if (width < maxWidth || height < maxHeight || width > maxWidth || height > maxHeight) {
//                        swal("Error","Height must be "+maxHeight+"px and width must be "+maxWidth+"px","error");
//                        return false;
//                             } 
//                        }
                        $("div."+affectedClass).block({ message: null });
                        $.ajax({
                            url: mainUrl + uploadUrl,
                            data: fd,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            success: function (data) {
                                console.log(data);
                                var data = data.replace(/^\s+|\s+$/g, '');
                                if(imagefolder != null){
                                 $("."+affectedClass).css('background-image', 'url(' + mainUrl + 'project/files/'+imagefolder+'/' + data + ')');
                                }
                                else{
                                  $("."+affectedClass).css('background-image', 'url(' + mainUrl + 'project/files/' + data + ')');
                                }
                                if(affectedClass == 'timeline_left_pic'){
                                   $(".my_pic").attr("src",mainUrl + "project/files/thumb/" + data ); 
                                   $(".timeline_feed_user_image").attr("src",mainUrl + "project/files/" + data ); 
                                }
                                $("div."+affectedClass).unblock();
                            }
                        });
                    }
                };
            }
        } else {
            swal("This browser does not support HTML5.");
        }
    } else {
        swal("Please select a valid Image file.");
    }
};

var validImageMaximumAndMinimumCheck = function(tagId, maxWidth, maxHeight, minWidth, minHeight, uploadUrl, affectedClass,imagefolder) {
    var fd = new FormData();
    var file = $('#'+tagId)[0].files[0];
    fd.append('file', file);

    //Get reference of FileUpload.
    var fileUpload = document.getElementById(tagId);

    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpeg|.jpg|.png|.gif)$");
    if (regex.test(fileUpload.value.toLowerCase())) {

        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {

            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {

                //Initiate the JavaScript Image object.
                var image = new Image();

                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;

                //Validate the File Height and Width.
                image.onload = function () {

                    var height = this.height;
                    var width = this.width;
                    if (width < minWidth || height < minHeight) {
                        swal("Error","Height must be more than "+minHeight+"px and width must be more than "+minWidth+"px","error");
                        return false;
                    } else {
                        if (width > maxWidth || height > maxHeight) {
                            swal("Error","Height must be less than "+maxHeight+"px and width must be less than "+maxWidth+"px","error");
                        } else {
                            $("div."+affectedClass).block({ message: null });
                            $.ajax({
                                url: mainUrl + uploadUrl,
                                data: fd,
                                processData: false,
                                contentType: false,
                                type: 'POST',
                                success: function (data) {
                                    console.log(data);
                                    var data = data.replace(/^\s+|\s+$/g, '');
                                    if(imagefolder != null){
                                     $("."+affectedClass).css('background-image', 'url(' + mainUrl + 'project/files/'+imagefolder+'/' + data + ')');
                                    }
                                    else{
                                      $("."+affectedClass).css('background-image', 'url(' + mainUrl + 'project/files/' + data + ')');
                                    }

                                    $("div."+affectedClass).unblock();
                                }
                            });
                        }
                    }
                };
            }
        } else {
            swal("This browser does not support HTML5.");
        }
    } else {
        swal("Please select a valid Image file.");
    }
};


/**
 * Replace emotion strings to respective png images
 *
 * @param {string} comment
 *
 * @return {string}
 *
 */
var createIcon = function(comment){
    if(!comment.match(/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/)){
    return (comment).replace(/(\:\+?\-?\w+\:)/g,function(match){
        match = match.replace(/:/g,'');
        return '<img src ="' + mainUrl + 'public/images/emojis/' + match + '.png" style=" width: 20px;height: 20px;vertical-align: middle;margin: -3px 0 0 0;"/>';
    });
    }
};

/**
 * Replace emoji string with corresponding emoji image in scope html
 *
 * @param {object} commentHtml
 *
 * @return void
 */
var injectEmojiHtml = function(commentHtml){
    var comment = $(commentHtml).html();
    console.log(comment);
    var commentWithEmoji = createIcon(comment);
    $(commentHtml).html(commentWithEmoji);
};

/**
 * Initialize emojiarea plugin on the passed element
 *
 * @param {object} element
 */
var initEmojiPicker = function(element){
    $.emojiarea.path =mainUrl+'/public/images/emojis';
    $(element).find("textarea.jEmojiTextarea").emojiarea({buttonLabel: null});
};

/**
 *  Inject emoji images inside passed html using injectEmojiHtml()
 *
 * @param element
 */
var parseEmojiInside = function(element){
    $(element).find("span.jEmojiable").each(function(){
        injectEmojiHtml(this);
    });
     $(element).find("div.jEmojiable").each(function(){
        injectEmojiHtml(this);
    });
};

/**
 * Enable emoji support on passed html
 *
 * @param element
 */
var enableEmojiOn = function(element){
    initEmojiPicker(element);
    parseEmojiInside(element);
};

/**
 * Initiates AJAX request on submit of click
 *
 * @param {string} submitUrl
 * @param {string} data
 * @param {function} callBackFn
 */
var postCommentContent = function (submitUrl, data, callBackFn) {
    $.ajax({
        type: 'POST',
        url: mainUrl + submitUrl,
        data: data,
        cache: false,
        processData: false,
        contentType: false,
        success: callBackFn
    });
};

var postCommentContents = function (submitUrl, data, callBackFn) {
    $.ajax({
        type: 'POST',
        url: mainUrl + submitUrl,
        data: data,
        cache: false,
        success: callBackFn
    });
};


/**
 * Post comment content in user timeline
 */
var postNewsFeed = function () {

    var comment = $.trim($("#newsfeed_comment").val());
    var imgid = $.trim($("#image_id").val());
    var fd = new FormData();
    var file_data = $("#file_feed").prop("files")[0];
    comment = comment.replace(/(?:\r\n|\r|\n)/g, '<br />');
  //  comment =  nl2br(comment);
    //alert(comment);exit;
    if (imgid != '' || comment != '') {
        $.blockUI({ message: '' });
        var submitUrl = 'user/updatenewsfeed';
        fd.append("file", file_data);
        fd.append("comment", comment);

        var successCallBack = function (data) {

            var html = $.parseHTML(data);
            enableEmojiOn(html);
            // var comment = $(html).find("div.postsubhead_left span");
            // injectEmojiHtml(comment);
             
            $("div.emoji-wysiwyg-editor").html('');
            $('.jfeeddisplay_div').prepend(html);
            magnificPopupSingleFn();
            $(".member_profile_pic").html('');
            $("#newsfeed_comment").val('');
            $("#image_id").val('');
            $(".img").attr('src','');
            $(".img").hide();
            $("#jerrordiv").html('');
            $("#file_feed").val('');
            FB.XFBML.parse();
        };
        postCommentContent(submitUrl, fd, successCallBack);
    }
    else {
        $("#jerrordiv").html('Please enter your comment or upload an image');
    }
       $("#newsfeed_comment").focus();
};




/**
 *
 * @param wt
 * @param ht
 * @param id
 * @returns {boolean}
 */
function upload(wt,ht,id){
     var fileUpload = $("#"+id)[0];

        //Check whether the file is valid Image.
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:]|[a-zA-Z0-9()\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
        if (regex.test(fileUpload.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            if (typeof (fileUpload.files) != "undefined") {
                //Initiate the FileReader object.
                var reader = new FileReader();
                //Read the contents of Image File.
                reader.readAsDataURL(fileUpload.files[0]);
                reader.onload = function (e) {
                    //Initiate the JavaScript Image object.
                    var image = new Image();
                    //Set the Base64 string return from FileReader as source.
                    image.src = e.target.result;
                    image.onload = function () {
                        //Determine the Height and Width.
                        var height = this.height;
                        var width = this.width;
                        if (height < ht || width < wt) {
                            $("#business_image_id").val('');
                            $("#community_image_id").val('');
                            $("#label_image_id").html("Height must be more than "+ht+"px and width must be more than "+wt+"px");
                            return false;
                        }
                      //  $("#label_image_id").html("Uploaded image has valid Height and Width.");
                        return true;
                    };
                }
            } else {
                $("#label_image_id").html("This browser does not support HTML5.");
                return false;
            }
        } else {
            alert("Please select a valid Image file.");
            return false;
        }
}

function replay_message(to_user_id, message_subject) {
    //alert("here");exit;
    var mlink = mainUrl + "user/create_message/" + to_user_id + "/" + message_subject;
    $.colorbox({width: "400px", height: "400px", iframe: true, href: mlink, overlayClose: false});
    return false;
}

function deleteConfirm() {
    var del_confirm = confirm("Are you sure you want to delete the record?");
    return del_confirm;
}

$(document).ready(function () {
    //datepicker
    $('#jCampaignExpiry').datepicker({
        minDate: 0
    });


});

function uploadimage(id) {
    $.ajax({
        url: MAIN_URL + "user/uploadimage/" + id, //your server side script
        async: false,
        //data: { id: commentId, content: comment}, //our data
        type: 'POST',
        success: function (data) {
//	            alert(data);
            $("#jCommentBoxDisplayDiv_" + id).append(data);
//	        	$(".comment_content_"+commentId).text(data);
//	        	$(".edit_delete").show();
        }
    });
    //$.colorbox({width:"400px", height:"390px", iframe:true, href:mlink, overlayClose: false});
    return false;
}

function uploadimagereply(id, announcement_id) {
    $.ajax({
        url: MAIN_URL + "user/uploadimagereply/" + id + "/" + announcement_id, //your server side script
        async: false,
        //data: { id: commentId, content: comment}, //our data
        type: 'POST',
        success: function (data) {
//	            alert(data);
            $("#jCommentBoxDisplayDiv_" + id).append(data);
//	        	$(".comment_content_"+commentId).text(data);
//	        	$(".edit_delete").show();
        }
    });
    //$.colorbox({width:"400px", height:"390px", iframe:true, href:mlink, overlayClose: false});
    return false;
}

$(window).load(function () {

    $(document).on('click','a.twitter-timeline', function(e){
        e.preventDefault();
        var loc = $(this).attr('href');
        var title = encodeURIComponent($(this).attr('title'));
        window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 225) + ', left=' + $(window).width() / 2 + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    });

    // One line for enabling emoji :-)
    enableEmojiOn(document.body);

    var track_charity_invitation_click = 1;
    //Footer hide and show
    $('.footer_main').bind('hover', function () {
        $('.footer').stop((true, true)).slideToggle('slow');
        $("html, body").stop((true, true)).animate({scrollTop: $(document).height()}, 1000);
    });
    //Hide right content sub menu
    //$('.jMenu').hide();
    //Toggle Right Menu
    $('.jMenuTitle').unbind().bind('click', function () {
        $('.jMenu').hide('scale');
        var selectedTab = $(this).attr('sid');
        $('#' + selectedTab).show('scale');
    });

    $(document).ajaxStop($.unblockUI);
    
    
    $(document).ajaxComplete(function() {
        magnificPopupGroupFn();
    });
    


    $('.masterTooltip').hover(function () {
        // Hover over code
        var title = $(this).attr('title');
        $(this).data('tipText', title).removeAttr('title');
        $('<p class="tooltip"></p>')
            .text(title)
            .appendTo('body')
            .fadeIn('slow');
    }, function () {
        // Hover out code
        $(this).attr('title', $(this).data('tipText'));
        $('.tooltip').remove();
    }).mousemove(function (e) {
        var mousex = e.pageX + 20; //Get X coordinates
        var mousey = e.pageY + 10; //Get Y coordinates
        $('.tooltip')
            .css({top: mousey, left: mousex})
    });
    if ($("#jSearchProfile")) {
        if (parseInt($('#jSearchProfile').length) == 1) {
            $("#jSearchProfile").autocomplete({
                source: mainUrl + "autocomplete",
                minLength: 1,
                select: function (event, ui) {
                    $(event.target).val(ui.item.label);
                    $("#search_selected_activity").val(ui.item.label);
                    $("#jSearchProfile").val(ui.item.label);
                    if (ui.item.type == 'user') {
                        window.location.href = mainUrl + 'timeline/' + ui.item.value;
                    }
                    if (ui.item.type == 'business') {
                        window.location.href = mainUrl + 'page/' + ui.item.value;
                    }
                    if (ui.item.type == 'community') {
                        window.location.href = mainUrl + 'group/' + ui.item.value;
                    }
                    return false;
                }
            }).data("autocomplete")._renderItem = function (ul, item) {

                if (item.type == 'user') {
                    if (item.file_path == null || item.file_path == '') {


                        var image = 'member_noimg.jpg';


                    } else {
                        var image = item.file_path;
                    }

                    var inner_html = '<a><div class="list_item_container"><div class="image"><img src="' + mainUrl + 'project/files/' + image + '"></div><div class="label">' + item.label + '</div><span class="search_info">User</span></div></a>';

                }
                if (item.type == 'business') {
                    if (item.image == null || item.image == '') {
                        var image = 'noimage.jpg';


                    } else {
                        var image = item.image;
                    }
                    var inner_html = '<a><div class="list_item_container"><div class="image"><img src="' + mainUrl + 'project/files/' + image + '"></div><div class="label">' + item.label + '</div><span class="search_info">Page</span></div></a>';

                }

                if (item.type == 'community') {
                    if (item.image === null || item.image == '') {
                        var image = 'noimage.jpg';


                    } else {
                        var image = item.image;
                    }

                    var inner_html = '<a><div class="list_item_container"><div class="image"><img src="' + mainUrl + 'project/files/' + image + '"></div><div class="label">' + item.label + '</div><span class="search_info">Group</span></div></a>';

                }
                return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append(inner_html)
                    .appendTo(ul);
            };
        }
    }

    if ($("#jSearchFriend")) {
        if (parseInt($('#jSearchFriend').length) == 1) {
            $("#jSearchFriend").autocomplete({
                source: mainUrl + "friendautocomplete",
                minLength: 1,
                select: function (event, ui) {
                    $(event.target).val(ui.item.label);
                    $("#search_selected_activity").val(ui.item.label);
                    $("#jSearchFriend").val(ui.item.label);
                    $("#friend_id").val(ui.item.value);
//                 if(ui.item.type == 'user'){
//                 window.location.href = mainUrl+'profile/'+;
//                 }
//                 if(ui.item.type == 'business')
//                 {
//                      window.location.href = mainUrl+'directory/'+ui.item.value;
//                 }
//                  if(ui.item.type == 'community')
//                 {
//                      window.location.href = mainUrl+'community/'+ui.item.value;
//                 }
                    return false;
                },
                change: function (event, ui) {
                    if (ui.item == null) {
                        //here is null if entered value is not match in suggestion list
                        $(this).val((ui.item ? ui.item.id : ""));
                    }
                }
            }).data("autocomplete")._renderItem = function (ul, item) {

                // if(item.type=='user'){
                if (item.image == null || item.image == '') {


                    var image = 'member_noimg.jpg';


                } else {
                    var image = item.image;
                }

                var inner_html = '<a><div class="list_item_container"><div class="image"><img src="' + mainUrl + 'project/files/thumb/' + image + '"></div><div class="label">' + item.label + '</div><span class="search_info">User</span></div></a>';

                return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append(inner_html)
                    .appendTo(ul);
            };
        }
    }

    $('#remove_image').live('change', function () {
        $("#friend_id").val('');
    });
    //classifieds remove photo
    $('#remove_image').live('click', function () {
        var image_id = $("[name=classifieds_image]").attr('id');
        var class_id = $("[name=btnSubmit]").attr('id');
        var file_path = $("[name=image_name]").attr('id');
        $.post(mainUrl + "user/removeClassifiedImage", {image_id: image_id, class_id: class_id, file_path: file_path}, function (data) {
            $('#classifieds_image_id').show();
            $('#remove_image').hide();
            $("[name=classifieds_image]").hide();
            $("input[type=file]").filestyle({
                image: mainurl + "project/themes/default/images/upload_photo.png",
                imageheight: 29,
                imagewidth: 109,
                width: 190,
                height: 25
            });
            $('.masterTooltip').hide();
        });
    });

    $('#jRedirectToInitiate').live("click", function () {
        var alias = $(this).attr('alias');
        window.location = mainUrl + 'community/' + alias + '#InitiateIntroduction';
    });

    /*
     * Tab functionailty
     */

    $("a.jtab").click(function () {
        // switch all tabs off
        $(".active").removeClass("active");

        // switch this tab on
        //$(this).addClass("active");
        $(this).parent().addClass("active");

        // slide all elements with the class 'content' up
        $(".contents").slideUp();

        // Now figure out what the 'title' attribute value is and find the element with that id.  Then slide that down.
        var content_show = $(this).attr("title");
        $("#" + content_show).slideDown();

    });

    $('#frmEditCampaign').validate({
        rules: {
            txtPifIncentive: {
                number: true
            },
            txtAnJIncentive: {
                number: true
            },
            txtBusinessBonus: {
                number: true
            }
        },
        messages: {
            txtPifIncentive: {
                number: "Please enter numeric value"
            },
            txtAnJIncentive: {
                number: "Please enter numeric value"
            },
            txtBusinessBonus: {
                number: "Please enter numeric value"
            }
        }
    })

    $('.jDisplayJoineeVoucher').live('click', function () {
        $('#jDisplayAcceptDiv').hide();
        $('#jdisplayVoucherDiv').show();
        $('#ExpDate').datepicker({
            minDate: 0

        });
        $("#ExpDate").datepicker("setDate", 90);
        $('#JoineeExpDate').datepicker({
            minDate: 0

        });
        $("#JoineeExpDate").datepicker("setDate", 90);
    })

    $('.jaddaffiliate').live('click', function () {
        $.blockUI({message: ''});
        var bizdirectory_id = $(this).attr('bid');
        var user_id = $(this).attr('uid');
        if (confirm("Are you sure you want to add the user as an affilate?") == true) {
            $.ajax({
                type: 'Post',
                url: mainUrl + 'user/add_affiliate',
                data: 'bizdirectory_id=' + bizdirectory_id + '&user_id=' + user_id,
                cache: false,
                success: function (data) {
                    $('#affiliate_' + user_id).html('Affiliate <a uid="' + user_id + '" bid="' + bizdirectory_id + '" class="jremoveaffiliate">x Remove</a>');
                }
            })
        }
    });
    $('.jremoveaffiliate').live('click', function () {
        $.blockUI({message: ''});
        var bizdirectory_id = $(this).attr('bid');
        var user_id = $(this).attr('uid');
        if (confirm("Are you sure you want to remove affilation?") == true) {
            $.ajax({
                type: 'Post',
                url: mainUrl + 'user/removeAffiliate',
                data: 'bizdirectory_id=' + bizdirectory_id + '&user_id=' + user_id,
                cache: false,
                success: function (data) {
                    $('#affiliate_' + user_id).html('+<span><a uid="' + user_id + '" bid="' + bizdirectory_id + '" class="jaddaffiliate">Add as affiliate</a>');

                }
            })
        }
    });

    $('.jAcceptAffilaite').live('click', function () {
        $.blockUI({message: ''});
        var user_id = $(this).attr('uid');
        var business_id = $(this).attr('bid');
        $.ajax({
            type: 'Post',
            url: mainUrl + 'user/acceptAffiliation',
            data: 'business_id=' + business_id + '&user_id=' + user_id,
            cache: false,
            success: function (data) {
                $('#affiliate_request_' + user_id).html('Accepted');
            }
        })

    })

    $('.jRejectAffilaite').live('click', function () {
        $.blockUI({message: ''});
        var user_id = $(this).attr('uid');
        var business_id = $(this).attr('bid');
        $.ajax({
            type: 'Post',
            url: mainUrl + 'user/acceptAffiliation',
            data: 'business_id=' + business_id + '&user_id=' + user_id,
            cache: false,
            success: function (data) {
                $('#affiliate_request_' + user_id).html('Rejected');
            }
        })

    })

    //
    $('#jViewBusiness').live('click', function () {
        location.reload();
//        var alias = $(this).attr('alias');
//        $.ajax({
//            type    : 'Post',
//            url     : mainUrl+'user/view_business',
//            data    : 'alias='+alias,
//            cache   : false,
//            success :function(data){
//                $('#jDivDisplayBusiness').html(data);
//            }
//        })
    });



    $('#jViewAssociatedPeople').live('click', function () {
        //$("#loader").show();
        $.blockUI({message: ''});
        var business_id = $(this).attr('bid');
        var business_name = $(this).attr('bname');
        $.ajax({
            type: 'Post',
            url: mainUrl + 'user/view_organization_associated_people',
            data: 'business_id=' + business_id + '&business_name=' + business_name,
            cache: false,
            success: function (data) {
                //$("#loader").hide();
                $('#jDivDisplayBusiness').html(data);

            }
        })
    })

    $('#jViewAssociatedBizcom').live('click', function () {
        $.blockUI({message: ''});
        var business_id = $(this).attr('bid');
        var business_name = $(this).attr('bname');
        var alias = $(this).attr('alias');
        $.ajax({
            type: 'Post',
            url: mainUrl + 'user/view_organization_associated_bizcom',
            data: 'business_id=' + business_id + '&business_name=' + business_name + '&alias=' + alias,
            cache: false,
            success: function (data) {
                $('#jDivDisplayBusiness').html(data);
            }
        })
    })

    $('#jViewAssociatedTestimonials').live('click', function () {
        $.blockUI({message: ''});
        var business_id = $(this).attr('bid');
        var business_name = $(this).attr('bname');
        var alias = $(this).attr('alias');
        $.ajax({
            type: 'Post',
            url: mainUrl + 'user/view_organization_associated_testimonials',
            data: 'business_id=' + business_id + '&business_name=' + business_name + '&alias=' + alias,
            cache: false,
            success: function (data) {
                $('#jDivDisplayBusiness').html(data);
            }
        })
    })

$('#jViewAssociatedGallery').live('click', function () {
        $.blockUI({message: ''});
        var business_id = $(this).attr('bid');
        var business_name = $(this).attr('bname');
        var alias = $(this).attr('alias');
        $.ajax({
            type: 'Post',
            url: mainUrl + 'user/view_organization_associated_gallery',
            data: 'business_id=' + business_id + '&business_name=' + business_name + '&alias=' + alias,
            cache: false,
            success: function (data) {
                $('#jDivDisplayBusiness').html(data);
            }
        })
    });

    $('#jViewAssociatedAffilates').live('click', function () {
        $.blockUI({message: ''});
        var business_id = $(this).attr('bid');
        var business_name = $(this).attr('bname');
        $.ajax({
            type: 'Post',
            url: mainUrl + 'user/view_business_affiliates',
            data: 'business_id=' + business_id + '&business_name=' + business_name,
            cache: false,
            success: function (data) {
                $('#jDivDisplayBusiness').html(data);
            }
        })
    })

    $('#jViewAssociatedAffilateRequest').live('click', function () {
        $.blockUI({message: ''});
        var business_id = $(this).attr('bid');
        var business_name = $(this).attr('bname');
        $.ajax({
            type: 'Post',
            url: mainUrl + 'user/view_business_affiliate_request',
            data: 'business_id=' + business_id + '&business_name=' + business_name,
            cache: false,
            success: function (data) {
                $('#jDivDisplayBusiness').html(data);
            }
        })
    })

    $("#jaccept").live('click', function () {
        if ($("#special_terms_conditions").is(':checked')) {
            $("#error_message").hide();
            $('#hid_paypal_payment').val('Y');
            var $email = $('#paypal_email').val();
            if ($email == '') {
                $("#error_message").show();
                $("#error_message").html('Please enter email');
                $("#error_message").addClass('error_div');
                return false;
            }
            else {
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                //alert(filter.test($email));exit;
                if (filter.test($email)) {

                    var x = $(this).attr('href');
                    x = x + "/" + $email;
                    $(this).attr('href', x);
                    //alert(x);exit;
                    //alert($(this).attr('href'));exit;
                    // Yay! valid
                    return true;
                }
                else {
                    $("#error_message").show();
                    $("#error_message").html('Please enter valid email');
                    $("#error_message").addClass('error_div');
                    return false;
                }
            }
            // needToConfirm = false;
            // $(this).closest('form').submit();
            //return false;
        }
        else {
            $("#error_message").show();
            $("#error_message").html('Please accept the terms and conditions');
            $("#error_message").addClass('error_div');
            return false;
        }
    });

    $('#frmccdetails').validate({
        rules: {
            credit_card_number: {
                required: true
            },
            cvv_number: {
                required: true
            },
            exp_month: {
                required: true
            },
            exp_year: {
                required: true

            }
        },
        messages: {
            credit_card_number: {
                required: "Please enter credit card number"
            },
            cvv_number: {
                required: "Please enter your CVV number"
            },
            exp_month: {
                required: "Please select expiry month"
            },
            exp_year: {
                required: "Please select expiry year"
            }
        }
    })


    if (location.hash == '#Edit') {
        setTimeout(function () {
            $(".jShowProfile").trigger("click");
        }, 10);
    }




    //Populate hidden field with selected members id
    if ($('#chk_user_campaign').length > 0) {
        $checks = $(":checkbox");
        $checks.bind('click', function () {
            var amount = 0;
            var fields = '';
            $(":checkbox").each(function () {
                if (this.checked) {
                    var splitArray = $(this).val().split('_');
                    amount = parseInt(amount) + parseInt(splitArray[0]);
                    fields += splitArray[1] + ',';
                }
            });
            $('#hidSelectedMembers').val(fields);
            $('#hidSelectedAmount').val(amount);
            console.log(fields);
        });
    }


    $('.jExpressPayPalBusinessIncentive').bind('click', function () {
        //alert("heree");
        var id = $(this).attr('cid');
        // alert(id);
        if ($("#special_terms_conditions_" + id).is(':checked')) {
            $("#error_message_" + id).hide();
        }
        else {
            $("#error_message_" + id).show();
            $("#error_message_" + id).html('Please accept the terms and conditions');
            $("#error_message_" + id).addClass('error_div');
            return false;
        }
        var campaign_id = $(this).attr('cid');
        $(this).closest('form').submit();
    })

    $(".jExpressPayPalBusinessIncentivePersonal").on('click', function () {
        // var id = $(this).attr('cid');
        // alert(id);
        if ($("#special_terms_conditions").is(':checked')) {
            $("#error_message").hide();
        }
        else {
            $("#error_message").show();
            $("#error_message").html('Please accept the terms and conditions');
            $("#error_message").addClass('error_div');
            return false;
        }
        $(this).closest('form').submit();
    });

    var x;
    $('.jShowReferralOptions').bind('click', function () {
        var bid = $(this).attr('bid');
        if (x == bid) {
            $('#jDivdisplayReferralOptions_' + bid).toggle();
        } else {
            $('.jReferralOptionsClass').hide();
            $('#jDivdisplayReferralOptions_' + bid).show();
        }
        x = bid;
        return false;
    });

    $('.jShowFeedReferralOptions').live('click', function () {
        var bid = $(this).attr('bid');
        var aid = $(this).attr('aid');
        $('.jReferralOptionsClass').hide();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        }
        else {
            $(this).addClass('active');
        }
        if ($(this).hasClass('active')) {
            $('#jDivdisplayReferralOptions_' + bid + '_' + aid).toggle();
        }
        //$('.jReferralOptionsClass').hide();
        //$('#jDivdisplayReferralOptions_'+bid+'_'+aid+' .jReferralOptionsClass').show();
        return false;
    });

    $('.jMyUnfriend').live('click', function () {
        if (confirm("Are you sure you want to unfriend?")) {
            if ($(this).data('clicked') == 'yes')return;
            $(this).data('clicked', 'yes');
            var uid = $(this).attr('uid');
            $.blockUI({message: ''});
            $.ajax({
                type: "POST",
                url: mainUrl + "unfriend",
                data: 'uid=' + uid,
                cache: false,
                success: function (data) {
                    $('#jmessage').html('Friend is removed...');
                    $('#jmessage').addClass('success_div');
                    setTimeout(function () {
                        location.reload(true);
                    }, 10);

                }
            });
        }

    });

        $('.jcommunityimagereply_file').live('change', function () {
        //alert("here");
        var cid = $(this).attr('cid');
        var fd = new FormData();
        var fileid = 'imagereply_file_' + cid;
        //fd.append( 'file',  $('input[type=file]')[0].files[0]);
        fd.append('file', $('#' + fileid)[0].files[0]);
//       var file = this.files[0];
//       var formData = new FormData();
//       formData.append('file',file);
//        var id = document.getElementById('reply_comment_id').value;
        //var announce_id = document.getElementById('announceid').value;
        // alert(id);
        $("#imagereply_loader_" + cid).show();
        $.ajax({
          url: mainUrl+"user/uploadimagereplysubmitcommunity/"+cid,
          data: fd,
          processData: false,
          contentType: false,
          type: 'POST',
          success: function(data){
            $("#imagereply_loader_"+cid).hide();
            $("#Image_Uploads").hide();
            $("#jDivReplyImagePreview_"+cid).append(data);
          }
        });
    });


    $('.jimagereply_file').live('change', function () {
        //alert("here");
        var cid = $(this).attr('cid');
        var fd = new FormData();
        var fileid = 'imagereply_file_' + cid;
        //fd.append( 'file',  $('input[type=file]')[0].files[0]);
        fd.append('file', $('#' + fileid)[0].files[0]);
//       var file = this.files[0];
//       var formData = new FormData();
//       formData.append('file',file);
//        var id = document.getElementById('reply_comment_id').value;
        //var announce_id = document.getElementById('announceid').value;
        // alert(id);
        $("#imagereply_loader_" + cid).show();
        $.ajax({
          url: mainUrl+"user/uploadimagereplysubmit/"+cid,
          data: fd,
          processData: false,
          contentType: false,
          type: 'POST',
          success: function(data){
            $("#imagereply_loader_"+cid).hide();
            $("#Image_Uploads").hide();
            $("#jDivReplyImagePreview_"+cid).append(data);
          }
        });
    });

    $('.jfeedimagereply_file').live('change', function () {
        //alert("here");
        var cid = $(this).attr('cid');
        var fd = new FormData();
        var fileid = 'imagereply_file_' + cid;
        //fd.append( 'file',  $('input[type=file]')[0].files[0]);
        fd.append('file', $('#' + fileid)[0].files[0]);
//       var file = this.files[0];
//       var formData = new FormData();
//       formData.append('file',file);
//        var id = document.getElementById('reply_comment_id').value;
        //var announce_id = document.getElementById('announceid').value;
        // alert(id);
        $("#imagereply_loader_" + cid).show();
        $.ajax({
            url: mainUrl + "user/uploadfeedimagereplysubmit/" + cid,
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                $("#imagereply_loader_" + cid).hide();
                $("#Image_Uploads").hide();
                $("#jDivReplyImagePreview_" + cid).append(data);
            }
        });
    });

    //$('input[type="file"]').on('change', function(e){
    $('.jimage_file').bind('change', function (e) {
//         var aid=e.target.attributes.aid.value;
//          alert(aid);
        var aid = $(this).attr('aid');
        var fd = new FormData();
        var fileid = 'file_' + aid;
        //fd.append( 'file',  $('input[type=file]')[0].files[0]);
        fd.append('file', $('#' + fileid)[0].files[0]);
        $("#loader_" + aid).show();
        $.ajax({
            url: mainUrl + "user/uploadimagesubmit/" + aid,
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                //alert(data);
                $("#loader_" + aid).hide();
                $("#Image_Uploads").hide();
                $("#juploadimagepreview_" + aid).html(data);
            }
        });
    });

    $('.jfeedimageimage_file').live('change', function (e) {
//         var aid=e.target.attributes.aid.value;
//          alert(aid);
        var aid = $(this).attr('aid');
        var fd = new FormData();
        var fileid = 'file_' + aid;
        //fd.append( 'file',  $('input[type=file]')[0].files[0]);
        fd.append('file', $('#' + fileid)[0].files[0]);
        $("#loader_" + aid).show();
        $.ajax({
            url: mainUrl + "user/uploadfeedcommentimagesubmit/" + aid,
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                $('#image_id').remove();
                $("#loader_" + aid).hide();
                $("#Image_Uploads").hide();
                $("#juploadcommentfeedimagepreview_" + aid).html(data);
            }
        });
    });

    $('.jimage_feed').bind('change', function (e) {
//         var aid=e.target.attributes.aid.value;
//          alert(aid);
        //var aid = $(this).attr('aid');
        var fd = new FormData();
        var fileid = 'file_feed';
        //fd.append( 'file',  $('input[type=file]')[0].files[0]);
        fd.append('file', $('#' + fileid)[0].files[0]);
        // alert(fd);exit;
        // $("#loader_"+aid).show();

        $.ajax({
            url: mainUrl + "user/uploadimagefeed",
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                //alert(data);exit;
                // $("#loader_"+aid).hide();
                // $("#Image_Uploads").hide();
                $("#juploadimagepreview").html(data);
            }
        });
    });


$('.view_other_bizcom').live('click',function(e){
     var sid = $(this).attr('id');
   //alert(sid);exit;
     $('.jslideSuggestion').hide();
     if($(this).hasClass('active')){
         $(this).removeClass('active');
     }
     else{
         $(this).addClass('active');
     }
    if($(this).hasClass('active')){
      $('#loading_'+sid).show();
        var split_id = sid.split("suggest_");
        var next_split = split_id[1].split("_");
        var community_id = next_split[0];
        var business_id = next_split[1];
        var mlink=mainUrl+"user/suggestlist/"+community_id+"/"+business_id;
        $.ajax({
          url: mlink,
          processData: false,
          contentType: false,
          type: 'POST',
          success: function(data){
              $('#loading_'+sid).hide();
              $('#'+sid+'_slide').html(data);
              $('#'+sid+'_slide').show();
          }
        });
    }
    else{
         $('#'+sid+'_slide').hide();
    }
     return false;
});

    $(".radiobusiness_type").on('click', function () {
        var val = $(this).val();
        if (val == 'Personal') {
            $(".category_div").show();
            $("#business_type").hide();
            $(".business_div").hide();
        }
        else {
            $("#business_type").show();
            $(".category_div").hide();
            $(".business_div").show();
        }
    });

    $("#community_business_id").on('change', function () {
        var val = $(this).val();
        // alert(val);
        if (val == 0) {
            $(".category_div").show();
        }
        else {
            $(".category_div").hide();
        }
    });

    $("#pif_sent").on('click', function () {
        alert("PIF Request already sent...");
    });
    $("#refer_friend").on('click', function () {
        var mlink = mainUrl + "index/refer_friend";
        $.colorbox({width: "430px", height: "407px", iframe: true, href: mlink, overlayClose: false});
        return false;
    });
    $(".jchangestatus").live('click', function () {
        //alert("here");
        var id = $(this).attr('id');
        $(".jfriendstatus").hide();
        $("#friend_status_" + id).show();
        // alert("here");
    });
    $("#jshowfriendstatus").live('click', function () {
        //alert("here");
        var id = $(this).attr('uid');
        $(".jfriendstatus").hide();
        $("#friend_status_" + id).show();
        // alert("here");
    });
//$(".mediapost").on('mouseout',function(){
//    $(".jfriendstatus").hide();
//  // alert("here");
//});
    $(".jChangetopersonal").live('click', function () {
        var id = $(this).attr('id');
        $(this).html('<span class="tik_icon"></span> Personal');
        // $("#jChangetoBusiness_"+id).text(' ');
        //.$("#jChangetoBusiness_"+id).text().replace('-');
        $(".jChangetoBusiness_" + id).html(' Business');
        var type = "P";
        $.ajax({
            type: "Post",
            url: mainUrl + "user/changefriendstatus",
            data: "id=" + id + "&type=" + type,
            cache: false,
            success: function (data) {

                //  alert(data);
                $("#friend_status_" + id).hide();
                $("#msg").addClass('success_div');
                $("#msg").html(data);
            }
        });
    });
    $(".jChangetoBusiness").live('click', function () {
        var id = $(this).attr('id');
        $(this).html('<span class="tik_icon"></span> Business');
        // $(".jChangetopersonal_"+id).text().replace('-');
        $(".jChangetopersonal_" + id).text(' Personal');
        // $("#jChangetopersonal_"+id).html(' Personal');
        var type = "B";
        $.ajax({
            type: "Post",
            url: mainUrl + "user/changefriendstatus",
            data: "id=" + id + "&type=" + type,
            cache: false,
            success: function (data) {

                //  alert(data);
                $("#friend_status_" + id).hide();
                $("#msg").addClass('success_div');
                $("#msg").html(data);
            }
        });
    });
    $("#my_friend_category").on('change', function () {
        var cat_id = $(this).val();
        var search = $("#search_val").val();
        var sort = $("#sort_val").val();
        var page = $("#page_val").val();
        // alert(sort);
        //document.getElementsByName('searchtext').value;
        window.location = mainUrl + "my-friends/?searchtext=" + search + "&sort=&sortby=" + sort + "&groupby=" + cat_id + "&page=" + page;
    });
    $("#my_friend_category_feed").live('change', function () {
        var cat_id = $(this).val();
        var uid = $(this).attr('uid');
        var alias = $(this).attr('alias');
        $.ajax({
            type: "Post",
            url: mainUrl + "listfriends",
            data: "groupby=" + cat_id + "&uid=" + uid + "&alias=" + alias,
            cache: false,
            success: function (data) {
                $('#tab-Friends').html(data);
                $('#tab-Friends').show();
                $(".jShowFriends_tab").show();
                $(".Jtabfriends_data").hide();
            }
        });
    });
    
     $('.jEmojiable>a').live('click',function(e){
            e.preventDefault();
            window.open(this.href, '_blank'); 
        });
    

 $("#jaddTestimonial").live('click',function(){
    $(".jtestimonial_section").toggle();
});

$(".jdeleteTestimonial").live('click',function(){
    var del_confirm = confirm("Are you sure you want to delete the record?");
    if(del_confirm == true){
         var id = $(this).attr('aid');
             $.ajax({
        url: MAIN_URL + "index/delete_testimonial/" + id, //your server side script
        async: false,
        //data: { id: commentId, content: comment}, //our data
        type: 'POST',
        success: function (data) {
               $("#comment_"+id).hide();
//	            alert(data);
           // $("#jCommentBoxDisplayDiv_" + id).append(data);
//	        	$(".comment_content_"+commentId).text(data);
//	        	$(".edit_delete").show();
        }
    });
    }
});

$("#savetestimonial").live("click",function(){

	var testimonialId = $(this).attr("cid");
	var testimonial = $("#testimonialarea_"+testimonialId).val();
	 $.ajax({
	        url: MAIN_URL+'index/savetestimonialedit',
	        async : false,
	        data: { id: testimonialId, content: testimonial},
	        type: 'POST',
	        success: function (data) {
                        $(".testimonial_content_"+testimonialId +" #jtestimonial_content_"+testimonialId).html(data);
	        	$(".testimonial_content_"+testimonialId).html(data);
                        $("#jtestimonial_content_"+testimonialId).html(data);
	        	$(".jtestimonial_content_"+testimonialId).html(data);
	        	$(".edit_delete").show();
	        }
	   });
	return false;
});

 $(".jedit_link_testimonial").live("click", function () {

        var testimonialId = $(this).attr("cid");

        var content = '';
        if($("#jtestimonial_content_"+testimonialId).length >0){
            content = ($("#jtestimonial_content_"+testimonialId).html()).trim();
        }
        if(content == ''){
           content = $(".testimonial_content_"+testimonialId).html();
        }
        //
        $("#jtestimonial_content_"+testimonialId).html('');
        var testimonial = ($(".testimonial_content_" + testimonialId).text()).trim();
        var editBox =   '<textarea style="width:98%" id="testimonialarea_' + testimonialId + '" class="textarea1">' + content + '</textarea>' +
                        '<input type="button" value="Save" cid="' + testimonialId + '" id="savetestimonial" class="btn primary pull-left marg5right textnormal">' +
                        '<input type="button" value="Cancel" cid="' + testimonialId + '" id="cancel" class="btn grey_btn pull-left textnormal"><div class="clearfix"></div>';
        testimonial = null;
        $(".testimonial_content_" + testimonialId).html(editBox);
        $("#jtestimonial_content_" + testimonialId).html(editBox);
        return false;
    });

    $("#cancel").live("click",function(){

    var commentId = $(this).attr("cid");

	var comment = $("#testimonialarea_"+commentId).text();
        $(".testimonial_content_"+commentId).text(comment);
	$(".edit_delete").show();
	return false;
});


$('#jEmailFriendRequest').live("click",function(){ 
//	 var contact_email      = $("#contact_email").val();
//     var contact_list       = $("#contact_list").val();  
//     var btnImport          = $("#btnImport").val();
//	 var contact_type      = $("#contact_type").val();
	 var contact_typeid      = $(this).attr('uid');
//     var contacts_check = ' ';     
     $("#loader").show();
     //alert(contact_typeid);
     //alert(contact_type);
//     var count = $("input[type='checkbox']").length;     
//     for(i=0;i<count;i++){
// 	   if($('#contacts_check'+i).is(":checked")){
// 	      contacts_check = contacts_check + "," + $('#contacts_check'+i).val();
// 	    }
//     }  
//    if($('#txtmailids').val()==''){
//      var contacts_check  = $('#txtmailids').val();  
//    }
//    else{
//       var contacts_check  = $('#txtmailids').val()+','; 
//    }
     var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//alert($('#txtmailids').val());
    if($('#txtmailids').val()==''){
      var contacts_check  = $('#txtmailids').val();  
       var x = re.test(contacts_check);
     //  alert(x);exit;
     if(x == false){
            $('#msgDisplay1').show();
            $('#msgDisplay1').removeClass('success_div');
            $('#msgDisplay1').html('Invalid mail address');
            $('#msgDisplay1').addClass('error_div');
            setTimeout(function() {	$('#msgDisplay1').fadeOut('fast');
				}, 3000);
         return false;
     }
    }
    else{
       var contacts_check  = $('#txtmailids').val()+','; 
       
        var data = contacts_check.split(",");
        //alert(data);
    for(var i= 0 ; i< data.length; i++){ 
        if(data[i] != ''){
           var x = re.test(data[i]);
   
     if(x == false){
            $('#msgDisplay1').show();
            $('#msgDisplay1').removeClass('success_div');
            $('#msgDisplay1').html('Invalid mail address');
            $('#msgDisplay1').addClass('error_div');
            setTimeout(function() {	$('#msgDisplay1').fadeOut('fast');
				}, 3000);
         return false;
     }
 }
    }
    }
     
    var dataString          = 'contacts_check='+contacts_check;
     
     $.ajax({
         type    : "POST",
         dataType: 'json',
         url     : mainUrl+"index/inviteFriendsthroughMail/user/"+contact_typeid,
         data    : dataString,
         cache   : false,
         success : function(data){
        	// alert(data);
                $('#msgDisplay1').show();
             $('#msgDisplay1').html(data.msg);
             $('#msgDisplay1').addClass(data.cls);
             $('.invite_contacts_list').hide();
            
//        	 $('#btnImport').hide();
// 	        $('#contact_list').hide();
 	       $('#checkbox').html(" "); 	      
// 	       $('#contact_list1').hide(); 	      
// 	        $('#checkbox').hide();
                $("#loader").hide();
                setTimeout(function() {	$('#msgDisplay1').fadeOut('fast');
				}, 5000);
                                // $("#jDivEmail").hide();
         }
     });
     return false;
});
})
var magnificPopupFn = function(){
  $('#portfolio').magnificPopup({
    delegate: 'a',
    type: 'image',
    image: {
      cursor: null,
        titleSrc :function(item){
            var title = item.el.attr('title');
            title = createIcon(title);
            return title;
        }
    },
    gallery: {
      enabled: true,
      preload: [0,1], // Will preload 0 - before current, and 1 after the current image
      navigateByImgClick: true
                }
  });
 }

 var magnificPopupGroupFn = function(){
  $('.portfolio').magnificPopup({
    delegate: 'a',
    type: 'image',
    image: {
      cursor: null,
        titleSrc :function(item){
            var title = item.el.attr('title');
            title = createIcon(title);
            return title;
        }
    },
    gallery: {
    //  enabled: true,
     // preload: [0,1], // Will preload 0 - before current, and 1 after the current image
      navigateByImgClick: true
    }
  });
 }

 var magnificPopupSingleFn = function(){
  $('#portfolio_timeline').magnificPopup({
    delegate: 'a',
    type: 'image',
    image: {
      cursor: null,
        titleSrc :function(item){
            var title = item.el.attr('title');
            title = createIcon(title);
            return title;
        }
    },
    gallery: {
      enabled: true,
      preload: [0,1], // Will preload 0 - before current, and 1 after the current image // Will preload 0 - before current, and 1 after the current image
      navigateByImgClick: true
    }
  });
 }

 var magnificPopupSingleFnNewsfeed = function(){
  $('#portfolionewsfeed').magnificPopup({
    delegate: 'a',
    type: 'image',
    image: {
      cursor: null,
        titleSrc :function(item){
            var title = item.el.attr('title');
            title = createIcon(title);
            return title;
        }
    },
    gallery: {
      enabled: true,
      preload: [0,1], // Will preload 0 - before current, and 1 after the current image // Will preload 0 - before current, and 1 after the current image
      navigateByImgClick: true
    }
  });
 }

 var magnificPopupSingleFn1 = function(){
  $('#portfolio1').magnificPopup({
    delegate: 'a',
    type: 'image',
    image: {
      cursor: null,
        titleSrc :function(item){
            var title = item.el.attr('title');
            title = createIcon(title);
            return title;
        }
    },
    gallery: {
      enabled: true,
      preload: [0,1], // Will preload 0 - before current, and 1 after the current image // Will preload 0 - before current, and 1 after the current image
      navigateByImgClick: true
    }
  });
 }
 
 function inboxMessages(){
       // alert("xkfxkl");
        $.ajax({
            type:"POST",
	  url: mainUrl + "user/inboxMessages",
	  cache: false,
	 // dataType: "json",
	  success: function(data) {
              var image = '';
             //console.log(data.message_id);exit;
             if(data.user_image_name == ''){
                image = 'http://localhost/connect10/project/files/small/member_noimg.jpg';
                 }
            else{
                image = 'http://localhost/connect10/project/files/small/'+data.user_image_name;
            }
            var html =  '<div class="inboxlist" id="inbox_1">'+
                            '<div class="row">'+
                            '<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 border_bot">'+
                                '<div class="mediapost_left pull-left">'+
                                    '<a href="http://localhost/connect10/timeline/'+data.user_image_name+'>'+
                                        '<span class="mediapost_pic" style="background-image: url('+image+');">'+
                                        '</span>'+
                                    '</a>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 border_bot">'+
                                '<h5 class="normal_user"><a href="http://localhost/connect10/timeline/'+data.user_alias+'">'+data.user+'</a></h5>'+
                            '</div>'+
                            '<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 border_bot">'+
                                '<h6 class="normal_head">'+
                                    '<a href="#" onclick="return show_message('+data.message_id+')">'+data.message_subject - +'<span>'+data.message_detail+'</span></a>'+
                                '</h6>'+
                            '</div>'+
                            '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 border_bot">'+

                                '<div class="wid100per inbox_date">'+
                                    '<span class="date left">'+data.message_date+'</span>'+
                                    '<span class="dropdown right">'+
                                        '<a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-ellipsis-v"></i></a>'+
                                        '<ul class="dropdown-menu dropdown-menu-right">'+
                                            '<li>'+
                                                '<a name="reply_message" id="'+data.user_image_name+'" onclick="return reply_message('+data.message_id+',"'+data.message_subject+'")" title="Reply Message" src="#"><i class="fa fa-reply"></i> Reply</a>'+
                                            '</li>'+
                                            '<li>'+
                                                '<a href="#" onclick="return show_message('+data.message_id+')"><i class="fa fa-envelope"></i> Show full Message</a>'+
                                            '</li>'+
                                            '<li>'+
                                                '<a href="#" onclick="return delete_message('+data.message_id+')"><i class="fa fa-trash"></i> Delete</a>'+
                                            '</li>'+
                                        '</ul>'+
                                    '</span>'+
                                '</div>'+

                                
                            '</div>'+
                            '</div>'+
                        '</div>';
           //  alert(html) ;          
             $("#myTabContent").append(html);           
          }
      });
    }
    
        function previewFile() {
        $(".img").show();
  var preview = document.querySelector('.img');
  var file    = document.querySelector('input[type=file]').files[0];
  var reader  = new FileReader();
  $("#image_id").val(file['name']);
  reader.onloadend = function () {
    
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

function previewFile1() {
        $(".img").show();
  var preview = document.querySelector('.img');
  var file    = document.querySelector('#file_feed').files[0];
  var reader  = new FileReader();
  $("#image_id").val(file['name']);
  reader.onloadend = function () {
    
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

function previewFile2() {
        $(".img").show();
  var preview = document.querySelector('.img');
  var file    = document.querySelector('#jorganizationimage_feed').files[0];
  var reader  = new FileReader();
  $("#image_id_org").val(file['name']);
  reader.onloadend = function () {
    
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

function previewFile3() {
        $(".img").show();
  var preview = document.querySelector('.img');
  var file    = document.querySelector('#jgroupimage_feed').files[0];
  var reader  = new FileReader();
  $("#image_id_grp").val(file['name']);
  reader.onloadend = function () {
    
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}