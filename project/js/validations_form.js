$(function() {
    $("#formLogin").validate({
        rules: {
                
            user_password: {
                required: true,
                minlength: 5
            },
               
            user_email: {
                required: true,
                email: true
            }
        },
        messages: {
                
            user_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            user_email: {
                required:"Please provide an email address",
                email : "Please enter a valid email"
            }
        }
    });

    $("#frmUserRegister").validate({
        rules: {
            user_passwords: {
                required: true,
                minlength: 5,
                passcheck:true
            },
            user_confirm_password: {
                required: true,
                equalTo: "#user_passwords"
            },
            user_emails: {
                required: true,
                email: true
            },
            terms_conditions: "required"

        },
        messages: {
            user_passwords: {
                required: "Please enter password",
                minlength: "Please enter at least 8 characters",
                passcheck:"Please enter a password with at least 8 characters, an uppercase character and a numeric."
            },
            user_confirm_password: {
                required: "Please re-enter password",
                equalTo: "Password mismatch!"
            },
            user_emails:
            {
                required:"Please enter email",
                email:"Please enter a valid email"
            },
            terms_conditions: "Please accept the terms and conditions."

        }
    });
        
    $("#frmLostPassword").validate({
        rules: {
            user_email: {
                required: true,
                email: true
            }
        },
        messages: {
                
            user_email: {
                required:"Please provide an email address",
                email : "Please enter a valid email"
            }
        }
    });
        
    $("#frmBusinessRegister").validate({
        rules: {
                
            community_name: {
                required: true
            },
            community_type: {
                required: true
            },
            business_type: {
                required: true
            },
            community_image_id:{
                required: true
            },
//            ,
//            community_category_id_add:{
//                required:true
//            },
            radiobusiness_type:{
                required:true
            }
        },
        messages: {
                
            community_name: {
                required: "Please enter group name"
            },
            community_type: {
                required:"Please select group type"
            },
            business_type: {
                required:"Please select a page"
            },
            community_image_id: {
                required:"Please upload a group image"
            },
//            community_category_id_add: {
//                required:"Please select category"
//            },
            radiobusiness_type: {
                required:"Please select type relation"
            }
        }
    });
        
    $("#frmBusinessEdit").validate({
        rules: {
                
            community_name: {
                required: true
            },
            community_type: {
                required: true
            },
            business_type: {
                required: true
            },
//            community_category_id: {
//                required: true
//            },
            community_business_id: {
                required: true
            }
        },
        messages: {
                
            community_name: {
                required: "Please enter group name"
            },
            community_type: {
                required:"Please select group type"
            },
            business_type: {
                required:"Please select a page"
            },
//            community_category_id: {
//                required:"Please select a category" 
//            },
             community_business_id: {
                required:"Please select a page" 
            }
        }
    });
    
    $("#frmBizdirectoryRegister").validate({
        rules: {
                
            business_name: {
                required: true
            },
            business_email: {
                required: true,
                email:true
            },
            business_url: {
                required: true,
                url:true
            },
//            business_type: {
//                required: true
//            },
//            business_industry_id:{
//              required: true  
//            },
            community_image_id:{
                required: true
            },
            business_description:{
                required: true
            },
           
            business_image_id:{
                required: true
            }
        },
        messages: {
                
            business_name: {
                required: "Please enter page name"
            },
            business_email: {
                required:"Please enter email",
                email:"Please enter a valid email address"
            },
            business_url: {
                required: "Please enter page URL",
                url:"Please enter a valid url"
            },
//            business_type: {
//                required:"Please select a type"
//            },
//            business_industry_id:{
//              required: "Please select an industry"
//            },
            community_image_id: {
                required:"Please upload a community image"
            },
             business_description:{
                required: "Please enter page description"
            },
            
            business_image_id:{
               required: "Please select an image"
            }
        }
    });
    
    $("#frmBizdirectoryEdit").validate({
        rules: {
                
            business_name: {
                required: true
            },
            business_email: {
                required: true,
                email:true
            },
            business_url: {
                required: true,
                url:true
            },
//            business_type: {
//                required: true
//            },
//            business_industry_id:{
//              required: true  
//            },
            community_image_id:{
                required: true
            },
            business_description:{
                required: true
            },
//            ,
//            business_category_id: {
//                required: true
//            }
        },
        messages: {
                
            business_name: {
                required: "Please enter page name"
            },
            business_email: {
                required:"Please enter email",
                email:"Please enter a valid email address"
            },
            business_url: {
                required: "Please enter page URL",
                url:"Please enter a valid url"
            },
//            business_type: {
//                required:"Please select an organization"
//            },
//            business_industry_id:{
//              required: "Please select an industry"
//            },
            community_image_id: {
                required:"Please upload a community image"
            },
             business_description:{
                required: "Please enter page description"
            },
            business_category_id:{
                required: "Please select category"  
            }
        }
    });

        $("#frmChangePassword").validate({
        rules: {
            user_password:{
                 required: true,
                minlength: 5,
                passcheck:true
            },
            user_newpassword: {
                required: true,
                minlength: 5,
                passcheck:true
            },
            user_confirm_password: {
                required: true,
                equalTo: "#user_new_password"
            }
            //terms_conditions: "required"

        },
        messages: {
             user_password: {
                required: "Please enter current password",
//                minlength:"Please enter at least 8 characters",
//                passcheck:"Please enter current password with atleast 8 characters, an uppercase character and a numeric."
            },
            user_newpassword: {
                required: "Please enter new password",
                minlength: "Please enter at least 8 characters",
                passcheck:"Please enter a password with at least 8 characters, an uppercase character and a numeric."
            },
            user_confirm_password: {
                required: "Please re-enter new password",
                equalTo: "Password mismatch!"
            }
           // terms_conditions: "Please accept the terms and conditions."

        }
    });
    
        $("#frmContact").validate({
        rules: {
                
            email_subject: {
                required: true,

            },
               
            sender_email: {
                required: true,
                email: true
            },
            email_description: {
                required: true,
            }
        },
        messages: {
                
            email_description: {
                required: "Please provide a description",
            },
            email_subject: {
                required: "Please provide a subject",
            },
            sender_email: {
                required:"Please provide an email address",
                email : "Please enter a valid email"
            }
        }
    });
    
    $("#add_announcement").validate({
        rules: {
            community: {
                required: true
            },
            announcement_title: {
                required: true
            },
            announcement_desc: {
                required: true
            }
            
            //terms_conditions: "required"

        },
        messages: {
            community: {
                required: "Please select community"
                
            },
            announcement_title: {
                required: "Please enter title"
            },
            announcement_desc: {
                required: "Please enter description"
            }
           // terms_conditions: "Please accept the terms and conditions."

        }
    });
    
          $("#frmprofile_edit").validate({
        rules: {
                
            user_firstname: {
                required: true
            },
            user_email: {
                required: true,
                email:true
            },
            user_lastname: {
                required: true
            }
              
        },
        messages: {
                
            user_firstname: {
                required: "Please enter first name"
            },
            user_email: {
                required:"Please enter email",
                email:"Please enter a valid email address"
            },
            user_lastname: {
                required:"Please enter last name"
            }
        }
    });
    
        $("#frmLogin").validate({
        rules: {
                
            user_password: {
                required: true,
                minlength: 5
            },
               
            user_email: {
                required: true,
                email: true
            }
        },
        messages: {
                
            user_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            user_email: {
                required:"Please provide an email address",
                email : "Please enter a valid email"
            }
        }
    });
//    $("#frmLogin_rate").validate({
//        rules: {
//                
//            user_password: {
//                required: true,
//                minlength: 5
//            },
//               
//            user_email: {
//                required: true,
//                email: true
//            }
//        },
//        messages: {
//                
//            user_password: {
//                required: "Please provide a password",
//                minlength: "Your password must be at least 5 characters long"
//            },
//            user_email: {
//                required:"Please provide an email address",
//                email : "Please enter a valid email"
//            }
//        }
//    });
    
     $("#frmUserRegister").validate({
        rules: {
            user_password: {
                required: true,
                minlength: 5,
                passcheck:true
            },
            user_confirm_password: {
                required: true,
                equalTo: "#user_password"
            },
            user_email: {
                required: true,
                email: true
            }
          //  terms_conditions: "required"

        },
        messages: {
            user_password: {
                required: "Please enter password",
                minlength: "Please enter at least 8 characters",
                passcheck:"Please enter a password with at least 8 characters, an uppercase character and a numeric."
            },
            user_confirm_password: {
                required: "Please re-enter password",
                equalTo: "Password mismatch!"
            },
            user_email:
            {
                required:"Please enter email",
                email:"Please enter a valid email"
            }
           // terms_conditions: "Please accept the terms and conditions."

        }
    });
    $("#frmUserRegister1").validate({
        rules: {
            user_password: {
                required: true,
                minlength: 5,
                passcheck:true
            },
            user_confirm_password: {
                required: true,
                equalTo: "#user_password"
            },
            user_email: {
                required: true,
                email: true
            },
            terms_conditions: "required"

        },
        messages: {
            user_password: {
                required: "Please enter password",
                minlength: "Please enter at least 8 characters",
                passcheck:"Please enter a password with at least 8 characters, an uppercase character and a numeric."
            },
            user_confirm_password: {
                required: "Please re-enter password",
                equalTo: "Password mismatch!"
            },
            user_email:
            {
                required:"Please enter email",
                email:"Please enter a valid email"
            },
           terms_conditions: "Please accept the terms and conditions."

        }
    });
//         $("#frmUserRegister_rate").validate({
//        rules: {
//              user_email: {
//                required: true,
//                email: true
//            },
//            user_password: {
//                required: true,
//                minlength: 5,
//                passcheck:true
//            },
//            user_confirm_password: {
//                required: true,
//                equalTo: "#user_password"
//            }
//          
//          //  terms_conditions: "required"
//
//        },
//        messages: {
//             user_email:
//            {
//                required:"Please enter email",
//                email:"Please enter a valid email"
//            },
//            user_password: {
//                required: "Please enter password",
//                minlength: "Please enter at least 8 characters",
//                passcheck:"Please enter a password with atleast 8 characters, an uppercase character and a numeric."
//            },
//            user_confirm_password: {
//                required: "Please re-enter password",
//                equalTo: "Password mismatch!"
//            }
//           // terms_conditions: "Please accept the terms and conditions."
//
//        }
//    });
        
        $("#frmfeedback").validate({
        rules: {
            sender_name: {
                required: true,

            },
                
            email_subject: {
                required: true,

            },
               
            sender_email: {
                required: true,
                email: true
            },
            email_description: {
                required: true,
            }
        },
        messages: {
            sender_name: {
                required: "Please provide a name",
            },   
            email_description: {
                required: "Please provide a description",
            },
            email_subject: {
                required: "Please provide a subject",
            },
            sender_email: {
                required:"Please provide an email address",
                email : "Please enter a valid email"
            }
        }
    });
    
            
        $("#frmSendMessage").validate({
        rules: {
            friend_name: {
                required: true,

            },
                
            message_subject: {
                required: true,

            },
            message_detail: {
                required: true
            }
        },
        messages: {
            friend_name: {
                required: "Please provide a name",
            },   
            message_detail: {
                required: "Please provide a description",
            },
            message_subject: {
                required: "Please provide a subject",
            }
        }
    });
});