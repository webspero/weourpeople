$(window).load(function () {

    magnificPopupGroupFn();

    // $(".loading").hide();
    //$(".jCommentDisplayDiv").hide();
    $('.jDisplayReply').hide();
    $("#active_loading").hide();
    //  $(".category_div").hide();
    $("#inviteblock").hide();
    $(".descriptive_div").hide();

    $('.members').click(function () {
        $('#inviteblock').slideToggle();
    });

    $('#frmInviteSetting').submit(function (e) {

        e.preventDefault();
        var request;

        $.ajax({
            url: '<?php echo BASE_URL ?>user/invite_permission_setting_set',
            type: "post",
            data: $(this).serialize(),
            success: function (response) {
                //alert(response);
                parent.location.reload();
                $.colorbox.close();
            }
        });

    });

    $('.jLikeFeedcommunity').live('click', function () {
        $.blockUI({message: ''});
        var community_id = $(this).attr('cid');
        var announcement_id = $(this).attr('aid');
//        var old_val = 0;
//        var old = $('#jcountlike_' + announcement_id).html().split(' ');
//        old_val = old[0];
//        if (old_val == '') {
//            old_val = 0;
//        }
        // alert(old_val);
        $.ajax({
            type: 'POST',
            url: mainUrl + 'user/postMyLike',
            data: 'community_id=' + community_id + '&announcement_id=' + announcement_id,
            dataType: 'json',
            cache: false,
            success: function (data) {
//                if (data.count != '1 Like' && data.count != '1 Likes') {
//                    old_val = old_val - 1;
//                }
//                else {
//                    old_val = Number(old_val) + 1;
//                }
                $('#jlike_' + announcement_id).removeClass();
                if (data.classtype == '') {
                    $('#jlike_' + announcement_id).addClass('unliked jLikeFeedcommunity');
                }
                else {
                    $('#jlike_' + announcement_id).addClass(data.classtype + ' jLikeFeedcommunity');
                }


                $('.jShowGroupLikeUsers#jcountlike_' + announcement_id).html(data.count);
            }
        })
        return false;
    })

    $('.jCommentButton').live('click', function () {
        var announcemnet_id = $(this).attr('aid');
        $('#jCommentBoxDisplayDiv_' + announcemnet_id).show();
        if($("#val_"+announcemnet_id).val()>0){
        $('#jCommentDisplayDiv_' + announcemnet_id).show();
    }
        return false;
    });

    $('body').on('click', '.jPostCommentButtonCommunity', function () {
        $.blockUI({ message: '' }); 
        if($(this).parent().hasClass('jDisplayReply'))
        {
        var id = $('#frmReplyProfile>#image_id').val();
        }
        else
        {
             var id = $('#image_id').val();
        }
        var comment_id = $(this).attr('cmid');
        var announcement_id = $(this).attr('aid');
        var community_id = $(this).attr('cid');
        var count = $('#reply_' + comment_id).val();
        var new_count = Number(count) + 1;

        if (comment_id > 0) {
            var comments = $.trim($("#watermark_" + announcement_id + '_' + comment_id).val());
        }
        else {
            var comments = $.trim($("#watermark_" + announcement_id).val());
        }


        if (!comment_id) {
            if (comments == '' && isNaN(id)) {
                $("#msg_display_" + announcement_id).html('Please enter comments/images');
                $("#msg_display_" + announcement_id).addClass('error_div');
                return false;
            }
        } else {
            if (comments == '' && isNaN(id)) {
                $("#msg_display_" + comment_id).html('Please enter comments/images');
                $("#msg_display_" + comment_id).addClass('error_div');
                return false;
            }
        }
        $('#jCommentBoxDisplayDiv_' + comment_id).block();
        $.ajax({
            type: 'POST',
            url: mainUrl + 'user/postfeedcomment',
            data: 'announcement_id=' + announcement_id + '&comments=' + comments + '&community_id=' + community_id + '&comment_id=' + comment_id + '&image_id=' + id,
            cache: false,
            success: function (data) {
                
                var html = $.parseHTML(data);
                enableEmojiOn(html);


                $('#jCommentBoxDisplayDiv_' + comment_id).unblock();
                if (comment_id > 0) {
                    var comments = $('#jcountreply_' + comment_id).html().split(' ');
                    var numcomments = Number(comments[0]) + 1;
                    $('#jCommentDisplayDiv_' + announcement_id).show();
                    $('#postingReply_' + comment_id).prepend(html);

                    // Reset values
                    $("#msg_display_" + announcement_id).html('');
                    $("#msg_display_" + announcement_id).removeClass('error_div');
                    $("#watermark_" + comment_id).val("");
                    $("#watermark_" + announcement_id).val("");
                    $("#watermark_" + announcement_id + "_" + comment_id).val("");
                    $('#jcountreply_' + comment_id).html(new_count + ' Replies');
                    $(".emoji-wysiwyg-editor").html('');
                }
                else {
                    var comments = $('#jcountcomment_' + announcement_id).html().split(' ');
                    var numcomments = Number(comments[0]) + 1;
                    $('#jCommentDisplayDiv_' + announcement_id).show();
                    $('#posting_' + announcement_id).prepend(html);

                    // Reset values
                    $("#msg_display_" + announcement_id).html('');
                    $("#msg_display_" + announcement_id).removeClass('error_div');
                    $("#watermark_" + announcement_id).val("");
                    $("#watermark_" + announcement_id + "_" + comment_id).val("");
                    $('#jcountcomment_' + announcement_id).html(numcomments + ' Comments');
                    $('.emoji-wysiwyg-editor').html('');
                }
                $('#reply_' + comment_id).val(new_count);
                magnificPopupSingleFn();
                $(".register_form").remove();
                $(".emoji-wysiwyg-editor .fa-camera").html(" ");
                return false;
            }
        })
    });

    //---------Delete post comment------
    $('body').on('click', '.jFeedCommentDelete', function () {
        var comment_id = $(this).attr('id');
        var announcement_id = $(this).attr('aid');
        var dataString = 'comment_id=' + comment_id + '&announcement_id=' + announcement_id;
        $(this).attr('disabled', 'disabled');
        $("#loading_" + announcement_id).show();
        $.ajax({
            type: "POST",
            url: mainUrl + "user/feedCommentDelete",
            data: dataString,
            cache: false,
            success: function (data) {
                $("#loading_" + announcement_id).hide();
                $('.jCommentDisplayDiv #posting_' + announcement_id + ' #jdivComment_' + comment_id).remove();
                $('#jcountcomment_' + announcement_id).html(data + ' comments');
            }

        });
        return false;
    })
    $('body').on('click', '.jShowReply', function () {
        var comment_id = $(this).attr('id');
        var replyexist = $('#postingReply_' + comment_id).text().length;
        if (replyexist > 0) {
            $('#postingReply_' + comment_id).toggle();
            return false
        }
        else {
            $.ajax({
                type: 'POST',
                url: mainUrl + 'user/showcommentreply',
                data: 'comment_id=' + comment_id,
                cache: false,
                success: function (data) {
                    var html = $.parseHTML(data);
                    enableEmojiOn(html);
                    
                    var comments = $('#jcountreply_' + comment_id).html().split(' ');
                    var numcomments = Number(comments[0]) + 1;
                    
                    $('#postingReply_' + comment_id).prepend(html);
                    $("#watermark_" + comment_id).val("");
                }
            })
        }
        return false;
    });


    $('.jFeedReplyDelete').live('click', function () {
        var comment_id = $(this).attr('id');
        var announcement_id = $(this).attr('aid');
        var dataString = 'comment_id=' + comment_id + '&announcement_id=' + announcement_id;
        var x = $('#reply_' + announcement_id).val();
        x = x - 1;
        //alert(announcement_id);
        //exit;
        //  alert($('#jcountreply_'+announcement_id).innerHTML);
        $(this).attr('disabled', 'disabled');
        $("#loading_" + announcement_id).show();
        $.ajax({
            type: "POST",
            url: mainUrl + "user/feedReplyDelete",
            data: dataString,
            cache: false,
            success: function (data) {
                $("#loading_" + announcement_id).hide();
                $('#jcountreply_' + announcement_id).html(x + ' Replies');
                $('#reply_' + announcement_id).val(x);
                // console.log ("jgj"+announcement_id);
                $('#jdivComment_' + comment_id).hide();
                // $('#jcountreply_'+announcement_id).html(" ");

                //  $('#jcountreply_'+announcement_id).innerHTML("6 Replies");
                //$('#jdivComment_'+comment_id).remove();
                // $('#jcountcomment_'+announcement_id).html(data+' comments');

            }

        });
        // window.location.reload();
        return false;
    })

    //----------------Comment Like------------
//     $('.jFeedCommentLike').live('click',function(){
//        var announcement_id     = $(this).attr('aid');
//        var comment_id           = $(this).attr('cid');
//         $.ajax({
//                type    :'POST',
//                url     : mainUrl+'user/postCommentLike',
//                data    :'announcement_id='+announcement_id+'&comment_id='+comment_id,
//                dataType : 'json',
//                cache    : false,
//                success : function(data){
//                    //console.log(data);
//                   //$('#jlike_'+announcement_id).html(data.type); 
//                   $('#jlikeComment_'+comment_id).removeClass();
//                   if(data.classtype==''){
//                      $('#jlikeComment_'+comment_id).addClass('unliked jFeedCommentLike'); 
//                   }
//                   else{
//                     $('#jlikeComment_'+comment_id).addClass(data.classtype+' jFeedCommentLike');  
//                   }
//                   
//                   $('#jcountcommentlike_'+comment_id).html(data.count); 
//                }
//        })
//      return false;  
//    })

    $('body').on('click', '.jFeedCommentReply', function () {

//        window.emojiPicker = new EmojiPicker({
//        emojiable_selector: '[data-emojiable=true]',
//        assetsPath: mainUrl+'project/themes/default/images/',
//        popupButtonClasses: 'fa fa-smile-o'
//      });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        //  window.emojiPicker.discover();
        var comment_id = $(this).attr('cid');
        $('.jDisplayReply').hide();
        $('.jCommentDisplayDiv #jdivReply_' + comment_id).toggle();
        return false;
    })

    //---------Load more comments------
//    $('#jMorecommentsButton').live('click',function(){
//        var announcement_id = $(this).attr('aid');
//        var pagenum         = $(this).attr('pid');
//        $(this).hide();
//         $.ajax({
//                type    :'POST',
//                url     : mainUrl+'user/loadMoreFeedComments',
//                data    :'announcement_id='+announcement_id+'&pagenum='+pagenum,
//                cache    : false,
//                success : function(data){
//                    $('#posting_'+announcement_id).append(data);
//                }
//            })
//        return false;
//    })
    //------------share button click-----------
    $('.jShareFeedCommunity').live('click', function () {
        $.blockUI({message: ''});
        var community_id = $(this).attr('cid');
        var announcement_id = $(this).attr('aid');
        $.ajax({
            type: 'POST',
            url: mainUrl + 'user/shareFeed',
            data: 'community_id=' + community_id + '&announcement_id=' + announcement_id,
            dataType: 'json',
            cache: false,
            success: function (data) {
                $('#jmessage').html('Shared with your friends successfully');
                $('#jmessage').addClass('success_div');
                $('#jcountshare_' + announcement_id).html(data.count);
            }
        })
    })

    $(".show_descriptive").on('click', function () {
        var div_id = $(this).attr('id');
        //  $(".descriptive_div").hide();
        $("#descriptive_div_" + div_id).toggle();
    });

    // Trigger click event on group timeline file upload
    $("#jGrpCam").bind('click', function () {
        console.info('Clicked');
        $("#jgrptimelinepic").trigger('click');
    });

// Initiate upload flow on file selection of group timeline background image
    $('#jgrptimelinepic').change(function () {
        var uploadUrl = 'user/grpTimelineImageUpload/' + $('#group_id').val();
        validImageMaximumAndMinimumCheck('jgrptimelinepic', 2000, 452, 1140, 258, uploadUrl, 'cover-wrapper');
        //validImageCheck('jgrptimelinepic', 1140, 258, uploadUrl, 'cover-wrapper');
    });

});

$('.discussion_tab').live('click', function () {
    var alias = $(this).attr('alias');
    $("#myprofile").hide();
    $("#jFeedSearch").hide();
    $.blockUI({message: ''});
    $.ajax({
        type: "POST",
        url: mainUrl + "communitydiscussion",
        data: "alias=" + alias,
        cache: false,
        success: function (data) {

            var html = $.parseHTML(data,true);
            enableEmojiOn(html);
            
            $("#jDivDisplayBusiness").html(html);

            // parse facebook share button
            FB.XFBML.parse();
        }
    });
});

$('.photos_tab').live('click', function () {
    var comm_id = $(this).attr('alias');
    $("#myprofile").hide();
    $("#jFeedSearch").hide();
    $.blockUI({message: ''});
    $.ajax({
        type: "POST",
        url: mainUrl + "user/group_gallary",
        data: "comm_id=" + comm_id,
        cache: false,
        success: function (data) {
            $("#jDivDisplayBusiness").html(data);
            //$('#jDivDisplayBusiness').hide();
            //$(".jDivMain").html(data);
        }
    });
});

$('.jgroupimage_feed').live('change', function (e) {
//         var aid=e.target.attributes.aid.value;
//          alert(aid);
    var group_id = $(this).attr('bid');
    var fd = new FormData();
    var fileid = 'jgroupimage_feed';
    //fd.append( 'file',  $('input[type=file]')[0].files[0]);
    fd.append('file', $('#' + fileid)[0].files[0]);
    // alert(fd);exit;
    // $("#loader_"+aid).show();

    $.ajax({
        url: mainUrl + "user/uploadgroupfeed/" + group_id,
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data) {
            //alert(data);exit;
            // $("#loader_"+aid).hide();
            // $("#Image_Uploads").hide();
            $("#juploadgroupimagepreview").html(data);
        }
    });
});

$('.jimage_file').live('change', function (e) {
//         var aid=e.target.attributes.aid.value;
//          alert(aid);
    var aid = $(this).attr('aid');
    var fd = new FormData();
    var fileid = 'file_' + aid;
    //fd.append( 'file',  $('input[type=file]')[0].files[0]);
    fd.append('file', $('#' + fileid)[0].files[0]);
    $("#loader_" + aid).show();
    $.ajax({
        url: mainUrl + "user/uploadimagesubmit/" + aid,
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data) {
            //alert(data);
            $("#loader_" + aid).hide();
            $("#Image_Uploads").hide();
            $("#juploadimagepreview_" + aid).html(data);
        }
    });
});

$('.jGroupDeletetButton').live('click', function () {
    if (confirm("This post will be deleted. Are you sure to continue?")) {
        //  var comment_id        = $(this).attr('id');
        var announcement_id = $(this).attr('aid');
        var dataString = 'announcement_id=' + announcement_id;
        $(this).attr('disabled', 'disabled');
        $("#loading_" + announcement_id).show();
        $.ajax({
            type: "POST",
            url: mainUrl + "user/groupfeeddelete",
            data: dataString,
            cache: false,
            success: function (data) {
                $("#loading_" + announcement_id).hide();
                $('#group_feed_' + announcement_id).remove();
                //$('#jdivComment_'+comment_id).remove();
                //  $('#jcountcomment_'+announcement_id).html(data+' comments');

            }

        });
        //  window.location.reload();

    }
    return false;
});

 $('#jMoreGroupGallery').live('click',function(){
         $('.jtab li.active').removeClass('active');
        $('.photos_tab ').addClass('active');
        $( ".photos_tab" ).trigger('click');
    })
     $('#jMoreGroupphotos').live('click',function(){
         $('.jtab li.active').removeClass('active');
        $('.jShowMembers ').addClass('active');
        $( ".jShowMembers" ).trigger('click');
    })




function joinCommunity(communityId, type) {
//   if($("#login_status").val()==0){
//     var  entity_type = 'refer';
//     var  entity_id = communityId;
//     var mlink=mainUrl+"index/loginpopup/"+entity_id+"/"+entity_type;
//     $.colorbox({width:"430px", height:"407px", iframe:true, href:mlink, overlayClose: false});
//     return false;
//    }
//    else{

    var dataString = 'community_id=' + communityId + '&type=' + type;
//        $(".more a").attr('disabled','disabled'); 
    $(this).attr('disabled', 'disabled');
    $("#loader").show();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: mainUrl + "index/joingroup",
        data: dataString,
        cache: false,
        success: function (data) {
            console.log(data.status);
            if (data.status == 'SUCCESS') {
                $("#" + communityId).hide();
                $("#jmessage").hide();
                $("#joinMessage").show();
                // window.location.reload();
                $("#joinMessage").addClass('success_div');
                $("#joinMessage").html(data.message);
                //$('.more a').off('click');
                $("#loader").hide();
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            } else {

                //$("#msg").html("Invalid Credentials");
                //$("#msg").addClass('error_div');
            }

        }
    });
//    }


}
function joinMail(communityId) {
    //alert("here");
//       if($("#login_status").val()==0){
//     var  entity_type = 'referprivate';
//     var  entity_id = communityId;
//     var mlink=mainUrl+"index/loginpopup/"+entity_id+"/"+entity_type;
//     $.colorbox({width:"430px", height:"407px", iframe:true, href:mlink, overlayClose: false});
//     return false;
//    }else{
    var join_confirm = confirm("Are you sure you want to join this group?");
    if (join_confirm) {
        var dataString = 'community_id=' + communityId;
//        $(".more a").attr('disabled','disabled'); 
        $(this).attr('disabled', 'disabled');
        $("#loading").show();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: mainUrl + "index/joinmail",
            data: dataString,
            cache: false,
            success: function (data) {
                $("#loading").hide();
                //console.log(data);
                $("#joinMessage").addClass('success_div');
                $("#joinMessage").html(data.message);
                //   window.location.reload();
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }


        });

    }
//    }
}
function acceptCommunityInvite(inviteId) {


    var dataString = 'invitation_id=' + inviteId;
//    $(".more a").attr('disabled','disabled'); 
    $(this).attr('disabled', 'disabled');
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: mainUrl + "index/accept_group_invite/" + inviteId,
        data: dataString,
        cache: false,
        success: function (data) {
            if (data.status == 'SUCCESS') {
                $("#joinMessage").addClass('success_div');
                $("#joinMessage").html(data.message);
                //$('.more a').off('click');
                setTimeout(function () {
                    location.reload()
                }, 10);

            } else {

                //$("#msg").html("Invalid Credentials");
                //$("#msg").addClass('error_div');
            }

        }
    });


}

function changeCommunitMemberStatus(communityId, userId, type) {

    var dataString = 'community_id=' + communityId + '&type=' + type + '&userId=' + userId;
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: mainUrl + "index/acceptgroup",
        data: dataString,
        cache: false,
        success: function (data) {
            if (data.status == 'SUCCESS') {
                $("div").find("#frnd_content_accept_" + userId).addClass('success_div');
                $("div").find("#frnd_content_accept_" + userId).html(data.message);
                $("div").find("#frnd_content_decline_" + userId).html('');

            } else {

                //$("#msg").html("Invalid Credentials");
                //$("#msg").addClass('error_div');
            }

        }
    });


}


function inviteFriends(userId, communityId) {

    var mlink = mainUrl + "index/invitetogroup/" + communityId + "/" + userId;
    $.colorbox({width: "430px", height: "400px", iframe: true, href: mlink, overlayClose: false});
    return false;

}

//function invite_permission_setting(communityId){
//    var mlink=mainUrl+"user/invite_permission_setting/"+communityId;
//    $.colorbox({width:"500px", height:"600px", iframe:true, href:mlink, overlayClose: false});
//    return false;
//}

function reply_message(to_user_id, message_subject) {
    var mlink = mainUrl + "user/create_message/" + to_user_id + "/" + message_subject;
    $.colorbox({width: "400px", height: "400px", iframe: true, href: mlink, overlayClose: false});
    return false;
}

function show_message(message_id) {
    var mlink = mainUrl + "user/show_message/" + message_id;
    $.colorbox({width: "400px", height: "400px", iframe: true, href: mlink, overlayClose: false});
    return false;
}

function delete_message(message_id) {
    if (confirm("Are you sure you want to delete this message?")) {
        $.post(mainUrl + "user/deletemessages/" + message_id, {}, function (response) {
            var alias = '';
            //var mlink=mainUrl+"user/create_message/"+message_id;
            //$.colorbox({width:"400px", height:"400px", iframe:true, href:mlink, overlayClose: false});
            $.ajax({
                type: "POST",
                url: mainUrl + "user/listmessagesajax",
                data: 'communityAlias=' + alias,
                cache: false,
                success: function (data) {
                    // $('#inbox_'+message_id).hide();

                    //$('.find_friends_list_outer').html(data);

                }
            });
            $('#content_msg').html('Your message has been deleted');
            $('#content_msg').addClass('error_div');
            setTimeout(function () {
                location.reload()
            }, 1000);

        });
        return false;
    }
}
function delete_sentmessage(message_id) {
    if (confirm("Are you sure you want to delete this message?")) {
        $.post(mainUrl + "user/deletesentmessages/" + message_id, {}, function (response) {
            $('#content_msg').html('Your message has been deleted');
            $('#content_msg').addClass('error_div');
            setTimeout(window.location.reload(), 1000);

        });

        //  return false;
    }
}
function make_active_passive(communityId, type, userId) {

    var dataString = 'community_id=' + communityId + '&type=' + type + '&userId=' + userId;
    // alert(type);
    if (type == 'Active') {
        if ($("#div_active_passive_" + communityId).length) {
            $("#div_active_passive_" + communityId + " .passive_active").show();
            $("#div_active_passive_" + communityId + " .active_passive").hide();
            $("#div_active_passive_" + communityId).addClass('active_user_box');
        }
        else {
            $(".passive_active").show();
            $(".active_passive").hide();
        }

    }
    else {
        if ($("#div_active_passive_" + communityId).length) {
            $("#div_active_passive_" + communityId + " .passive_active").hide();
            $("#div_active_passive_" + communityId + " .active_passive").show();
            $("#div_active_passive_" + communityId).removeClass('active_user_box');
        }
        else {
            $(".passive_active").hide();
            $(".active_passive").show();
        }


    }
    $("#active_loading").show();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: mainUrl + "user/make_active_passive",
        data: dataString,
        cache: false,
        success: function (data) {
            if (data.status == 'SUCCESS') {

                $("#active_loading").hide();
                $("div").find("#joinMessage").addClass('success_div');
                $("div").find("#joinMessage").html(data.message);
                //setTimeout(function(){location.reload()},2000);
            }

        }
    });
    return false;
}

function DisjoinCommunity(communityId, type) {
    if (confirm("Are you sure you want to unsubscribe from this group ?")) {
        if ($("#login_status").val() == 0) {
            var entity_type = 'refer';
            var entity_id = communityId;
            var mlink = mainUrl + "index/loginpopup/" + entity_id + "/" + entity_type;
            $.colorbox({width: "430px", height: "407px", iframe: true, href: mlink, overlayClose: false});
            return false;
        }
        else {

            var dataString = 'community_id=' + communityId + '&type=' + type;
//        $(".more a").attr('disabled','disabled'); 
            $(this).attr('disabled', 'disabled');
            $("#loader").show();
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: mainUrl + "index/joingroup",
                data: dataString,
                cache: false,
                success: function (data) {
                    console.log(data.status);
                    if (data.status == 'SUCCESS') {
                        window.location.reload();

                        // setTimeout(window.location.reload(),1000);
                        //$('.more a').off('click');
                        // $("#loader").hide();


                    } else {

                        //$("#msg").html("Invalid Credentials");
                        //$("#msg").addClass('error_div');
                    }
//              $("#joinMessage").addClass('success_div');
//              $("#joinMessage").html(data.message);
                }
            });
        }

    }
}

/**
 * Initiate ajax call for appending a new feed in group
 */
var postGroupFeed = function () {
$("#groupfeed_comment").focus();
    var comment = $.trim($("#groupfeed_comment").val());
    comment = comment.replace(/(?:\r\n|\r|\n)/g, '<br />');
    var imgid = $.trim($("#image_id_grp").val());
    var comm_id = $.trim($("#community_id").val());
    var fd = new FormData();
    var file_data = $("#jgroupimage_feed").prop("files")[0];
    if (imgid != '' || comment != '') {
         fd.append("file", file_data);
            fd.append("comment", comment);
            fd.append("id", imgid);
            fd.append("community_id", comm_id);
        $(".jpost_comment_div").block({message: null});
        $.ajax({
            type: 'POST',
            url: mainUrl + "user/updategroupfeed",
            data: fd,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {

                var html = $.parseHTML(data);
                enableEmojiOn(html);
                $('.jgroupfeeddisplay_div').prepend(html);
                $(".jpost_comment_div").unblock();

                magnificPopupSingleFn();

                // Reset values in html
                $("#juploadgroupimagepreview").html('');
                $("#groupfeed_comment").val('');
                $("#jerrordiv").html('');
                $(".emoji-wysiwyg-editor").html('');
                $("#image_id").val('');
                $("#image_id_grp").val('');
                $(".img").attr('src','');
                $(".img").hide();
                $("#jgroupimage_feed").val('');
            }
        });
    }
    else {
        $("#jerrordiv").html('Please enter your comment or upload an image');
    }
};

function checkLogin1(entity_type,entity_id,flag)
{
    if($("#login_status").val()==0){
     var mlink=mainUrl+"index/loginpopup/"+entity_type+"/"+flag;
    // $.colorbox({width:"430px", height:"407px", iframe:true, href:mlink, overlayClose: false});
    $.pgwModal({
    url: mlink,
    iframe:true,
    overlayClose: false
    //loadingContent: '<span style="text-align:center">Loading in progress</span>',
    //closable: false,
  //  titleBar: false
});
     return false;
    }  
    else{
    	return true;
    }
    	
}