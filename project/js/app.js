$( document ).ready(function() {
    
    function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
    
    if(getParameterByName('section')=='settings')
        {
         var url = MAIN_URL+"cmshelper/setTheme/";    
         $("#theme").css('display','none');
         $.ajax({
             url: url,
             type: "POST",
             success: function(result) {
               $('#general').children(':eq(2)').after(result);
            
            $('#themeselect').change(function () {
                $("#theme").val($(this).val());
            });
             }
    
         });   
        }
        
        
});

