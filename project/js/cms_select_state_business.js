
$( window ).load(function() {

    // CMS: Location (state change on country selection)
    var stateHtml = $("#business_state").html(); 
    var countryVal = $("#business_country").val();
    var businessId = $("#business_id").val();


    //var stateVal =  $("#business_state").val(); alert(stateVal);

    if(countryVal != 'US' && countryVal != 'IN' && $("#business_country").val() != 'CA' ){ 
        $("#business_state").parent().html("<input type='text' placeholder='State' class='form-control' name='business_state' id='business_state'/>&nbsp");
        $.get(mainUrl+'cmshelper/ajaxGetOrganizationState/'+businessId, function(result) {
           $('#business_state').val($.trim(result)); 
           //$('#business_state').val(result);
        });
    }else{ 
            $.get(mainUrl+'cmshelper/ajaxGetStatesByCountryOrganization/'+$("#business_country").val()+'/'+businessId+'/edit', function(result) {
                $("#business_state").parent().html("<select class='form-control' name='business_state' id='business_state'>"+result+"</select>&nbsp");
            });
        }

    $("#business_country").live("change", function() {
        if($("#business_country").val() != 'US' && $("#business_country").val() != 'IN' && $("#business_country").val() != 'CA' ){
            $("#business_state").parent().html("<input placeholder='State' type='text' class='form-control' name='business_state' id='business_state'/>&nbsp");
            $.get(mainUrl+'cmshelper/ajaxGetOrganizationState/'+businessId, function(result) {
                $('#business_state').val();
            });
        }
        else{  
            $.get(mainUrl+'cmshelper/ajaxGetStatesByCountryOrganization/'+$("#business_country").val(), function(result) {
                $("#business_state").parent().html("<select class='form-control' name='business_state' id='business_state'>"+result+"</select>&nbsp ");
            });
        }
    });
    

});
