var refreshId;
var chatListRefreshId;

$(document).idle({
    onIdle: function(){
        //update user status as idle
        $.post(mainUrl + "index/useronlineactivity", {
            userActivityStatus : 'Idle'
        });
    },
    onActive: function(){
        //update user status as active
        $.post(mainUrl + "index/useronlineactivity", {
            userActivityStatus : 'Active'
        });
    },
    idle: 25000
});

$(function() {
    //keep-alive request every 30 minutes
    refreshId = setInterval(function() {
        $.post(mainUrl + "index/useronlineactivity", {
            userActivityStatus : 'Active'
        });
    }, 1800000); 
   
    $('#chatIdentifier').click(function() {
        $('#onlineUsersList').show('medium', function() {
            $('#chatIdentifier').hide();
        });
    });
   
    $('#chatMinimize').click(function() {
        $('#onlineUsersList').hide('medium', function() {
            $('#chatIdentifier').show();
        });       
    });
   
    if(isUserLoggedIn) {
        chatListRefreshId = setInterval(function() {
            $.ajax({
                url: mainUrl + "index/updatechatlist",
                success: function(data) {
                    $('ul#chatListContainer').html(data);
                }
            })
            }, 30000); 
    }
});