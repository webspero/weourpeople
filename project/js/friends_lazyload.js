$( window ).load(function() {
	 $( ".listitem" ).hide();
	 var page = 1;               
//	                $('#tab-Friends').jscroll({
//	                    loadingHtml: '<img src="'+MAIN_URL+'project/files/default/loader.gif" alt="Loading" /> Loading...',
//	                    padding: 20,
//	                    autoTrigger: true
//	                    nextSelector: MAIN_URL+"user/listfriendsajax/"+page+"/"+ALIAS,
//	                    contentSelector: ''
//	                });
	$( ".jShowFriends" ).click(function() { 
//	    alert(TOTAL_PAGES); 
		page = 1;	
               
       	var _throttleTimer = null;
	  	var _throttleDelay = 100;
	    $( window ).scroll(function() { 
	        //alert("hi");
                 var groupby = $("#my_friend_category_feed").val();
	    	clearTimeout(_throttleTimer);
	        _throttleTimer = setTimeout(function () {
	        	if($(window).scrollTop() + $(window).height() > $(document).height()-100) {
	            	// $(window).unbind('scroll');
	                //	alert($(document).height());	                	  
	                if(page <TOTAL_PAGES){
	                	page++;
	                	$(".listitem" ).show();	                  
	                    $.ajax({
//	                        	beforeSend: function(){page++;},
	                        	type:"POST",
	                            url: MAIN_URL+"user/listfriendajax/"+page+"/"+ALIAS+"/"+groupby,
	                            dataType:"html",
	                            success: function(res) {                                 
//	                            	alert(res);
	                                $('#tab-Friends').append(res);
	                                $('.listitem').fadeIn(3000);
	                                $('.listitem').hide();
	                                //$(window).bind('scroll');	                                    	              
	                            }
	                        });		                   
	                }
	        	}
	        },_throttleDelay);
	    });
	});
         //Make initaite introduction tab as selected based on url
    if(location.hash=='#Friends'){
        $( ".jShowFriends" ).trigger('click');
    }
});