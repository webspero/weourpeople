<?php if(PageContext::$response->dashboardPanel2) {  ?>


<div class="section_list_view ">

    <div class="row have-margin">
        <span class="legend">
                <?php  echo "Section : ".PageContext::$response->sectionName; ?>
        </span>
        <!-- graph display -->
            <?php for($rowLoop=1;$rowLoop<=PageContext::$response->dashboardRowCount;$rowLoop++) { ?>
        <div class="row-fluid general_content_boxes">
                    <?php for($colLoop=1;$colLoop<=PageContext::$response->rowArray[$rowLoop]->columnCount;$colLoop++) {  ?>



            <div class="span4 <?php if(PageContext::$response->rowArray[$rowLoop]->columnCount>1) {  ?>span4 dboard_block1<?php } else {  ?>dboard_block2<?php } ?>">
                            <?php if(PageContext::$response->columnConfig[$rowLoop][$colLoop]->display=="graph") { ?>
                <p class="graph_hdr2"><?php echo PageContext::$response->columnConfig[$rowLoop][$colLoop]->caption; ?>
                </p> <?php echo PageContext::$response->columnData[$rowLoop][$colLoop]->renderChart();  ?>
                                <?php }
                            else if(PageContext::$response->columnConfig[$rowLoop][$colLoop]->display=="listing") {
                                ?>

                <table  id="" class="table  table-striped table-bordered table-hover " >

                    <span class="legend"><?php echo PageContext::$response->columnConfig[$rowLoop][$colLoop]->title;?><?php if(PageContext::$response->columnConfig[$rowLoop][$colLoop]->titlelink) { ?><a class="btn btn-link" href="<?php echo PageContext::$response->columnTitleLink[$rowLoop][$colLoop];?>" ><?php echo PageContext::$response->columnConfig[$rowLoop][$colLoop]->titlelink;?></a><?php } ?></span>
                    <thead>

                        <tr>
                                            <?php //echopre(PageContext::$response->panelConfig[$rowLoop][$colLoop]->listcolumns);
                                            $columnCount = 0;
                                            foreach(PageContext::$response->columnConfig[$rowLoop][$colLoop]->listcolumns as $key=>$column) {
                                                ?>
                            <th class="table-header"><?php echo $column->name;?></th>
                                                <?php $columnCount ++;
                                            } ?>
                        </tr> </thead>
                    <tbody> <?php if (PageContext::$response->columnData[$rowLoop][$colLoop]) { ?>
                                            <?php
                                            foreach(PageContext::$response->columnData[$rowLoop][$colLoop] as $data) {


                                                ?><tr><?php

                                                    foreach(PageContext::$response->columnConfig[$rowLoop][$colLoop]->listcolumns as $key=>$column) {
                                                        ?><td class="wordWrap"><?php    echo $data->$key;
                                                            ?></td><?php
                                                    }
                                                        ?></tr><?php


                                                    ?>


                                                <?php  } ?>
                                            <?php } else { ?>
                        <tr><td colspan="<?php echo $columnCount;?>">     No Records Found</td></tr>
                                            <?php } ?>
                    </tbody>
                </table>

                                <?php } ?>
            </div><?php
                    }?>

        </div>

                <?php } ?>

        <!-- listing-->
    </div>
</div>


    <?php }  else { ?>
<div class="section_list_view ">

    <div class="row have-margin">
        <span class="legend">
                <?php  echo "Section : ".PageContext::$response->sectionName; ?>
        </span>
        <!-- graph display -->
            <?php for($rowLoop=1;$rowLoop<=PageContext::$response->graphRowCount;$rowLoop++) { ?>
        <div class="row-fluid general_content_boxes">
                    <?php for($colLoop=1;$colLoop<=PageContext::$response->graphPanels[$rowLoop]->columnCount;$colLoop++) { ?>



            <div class="span4 <?php if(PageContext::$response->graphPanels[$rowLoop]->columnCount>1) {  ?>span4 dboard_block1<?php } else {  ?>dboard_block2<?php } ?>">
                <p class="graph_hdr2"><?php echo PageContext::$response->graphPanelsConfig[$rowLoop][$colLoop]->caption; ?>
                </p> <?php echo PageContext::$response->graphObjArray[$rowLoop][$colLoop]->renderChart();  ?></div>
                        <?php } ?>
        </div>

                <?php } ?>
        <!-- graph display END-->

        <!-- listing-->
            <?php for($rowLoop=1;$rowLoop<=PageContext::$response->listinPanelRow;$rowLoop++) {  ?>
        <div class="row-fluid general_content_boxes ">
                    <?php for($colLoop=1;$colLoop<=PageContext::$response->listingPanels[$rowLoop]->columnCount;$colLoop++) { ?>

            <div class="<?php if(PageContext::$response->listingPanels[$rowLoop]->columnCount>1) {  ?>span4 dboard_block1<?php } else {  ?>dboard_block2<?php } ?>">
                <table  id="" class="table  table-striped table-bordered table-hover " >

                    <span class="legend"><?php echo PageContext::$response->panelConfig[$rowLoop][$colLoop]->title;?><?php if(PageContext::$response->panelConfig[$rowLoop][$colLoop]->titlelink) { ?><a class="btn btn-link" href="<?php echo PageContext::$response->listTitleLink[$rowLoop][$colLoop];?>" ><?php echo PageContext::$response->panelConfig[$rowLoop][$colLoop]->titlelink;?></a><?php } ?></span>
                    <thead>

                        <tr>
                                        <?php //echopre(PageContext::$response->panelConfig[$rowLoop][$colLoop]->listcolumns);
                                        $columnCount = 0;
                                        foreach(PageContext::$response->panelConfig[$rowLoop][$colLoop]->listcolumns as $key=>$column) {
                                            ?>
                            <th class="table-header"><?php echo $column->name;?></th>
                                            <?php $columnCount ++;
                                        } ?>
                        </tr> </thead>
                    <tbody> <?php if (PageContext::$response->listDataArray[$rowLoop][$colLoop]) { ?>
                                        <?php
                                        foreach(PageContext::$response->listDataArray[$rowLoop][$colLoop] as $data) {


                                            ?><tr><?php

                                                foreach(PageContext::$response->panelConfig[$rowLoop][$colLoop]->listcolumns as $key=>$column) {
                                                    ?><td class="wordWrap"><?php    echo $data->$key;
                                                        ?></td><?php
                                                }
                                                    ?></tr><?php


                                                ?>


                                            <?php  } ?>
                                        <?php } else { ?>
                        <tr><td colspan="<?php echo $columnCount;?>">     No Records Found</td></tr>
                                        <?php } ?>
                    </tbody>
                </table>
            </div>
                        <?php } ?>
        </div>
                <?php } ?>
        <!-- listing-->
    </div>
</div>

    <?php } ?>