<div class="section_list_view " >

    <div class="row have-margin"  >
        <span class="legend">
            <?php  echo "Section : ".PageContext::$response->sectionName; ?>
        </span>
<?php if(PageContext::$response->message!="") { ?><div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">x</button>  <?php echo PageContext::$response->message ?></div> <?php } ?>
 <?php if( pageContext::$response->errorMessage!="") { ?> <div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">Ã—</button>   <?php echo  pageContext::$response->errorMessage; ?></div><?php } ?>

<?php if(PageContext::$response->settingStyle=="tab") { ?>


<ul class="nav nav-tabs" id="settingtab">
        <?php  $loop=0;
    foreach(PageContext::$response->settingsTabs as $tab) { ?>


    <li class="<?php  if($loop==0) { ?>active<?php } ?>"><a href="#<?php echo $tab->id ?>" data-toggle="tab" ><?php echo $tab->label ?></a></li>


        <?php  $loop++;
    } ?>
</ul>
<div style="border-bottom:1px solid #DDDDDD;border-left:1px solid #DDDDDD;border-right:1px solid #DDDDDD;margin-top: -20px;">
    <div class="control-group">&nbsp;</div>
    <form action="" name="settingsForm" id="settingsForm" method="post" class="form-horizontal" enctype="multipart/form-data">
    <div class="tab-content">

    <?php $loop=0;
                foreach(PageContext::$response->settingsTabs as $tab) { ?>
        <div class="tab-pane <?php if($loop==0) { ?>active<?php } ?>" id="<?php echo $tab->id ?>">

        <?php foreach($tab->tabContent as $tabContent) {
            if($tabContent->parent_settingfield=="") {
                $currentParent = $tabContent->settingfield;
                $currentParentStatus = $tabContent->value;
                } ?>

            <?php if($tabContent->type=="checkbox") { 
               ?>
            <div class="control-group <?php echo "jq".$tabContent->parent_settingfield;?>"  <?php if($currentParentStatus!="1" && $currentParent==$tabContent->parent_settingfield) { ?> style="display:none;"<?php } ?>>
                <label class="control-label" for="<?php echo $tabContent->settingfield;?>"><?php echo $tabContent->settinglabel;?></label>
                <div class="controls">
                    <input type="checkbox" class="jqSettingCheckbox"  id="<?php echo $tabContent->settingfield;?>" name="<?php echo $tabContent->settingfield;?>" <?php if($tabContent->value=="1") { ?>checked <?php } ?>><span class="help-inline"><?php echo $tabContent->customColumn->hint; ?></span>
                  <?php if($tabContent->hint) {  // if($tabContent->hint) { ?>
                  <?php if($tabContent->settingfield=='chat_display'){?>
                    <span style="color: #a0a6ab;font-size: 13px;">If enabled, Please buy the licence key for using it <a href='https://www.cometchat.com/?r=ag'>https://www.cometchat.com/?r=ag</a></span>
                 <?php }else{?>
                                <a href="#" class="tooltiplink" data-original-title="<?php echo $tabContent->hint; ?>"><span class="help-icon"><img src="<?php echo BASE_URL;?>modules/cms/images/help_icon.png"><span></a>
                 <?php } } ?>
                </div>

            </div>
             <?php } else if($tabContent->type=="textarea") { ?>
             <div class="control-group <?php  echo "jq".$tabContent->parent_settingfield;?>" <?php if($currentParentStatus!="1" && $currentParent==$tabContent->parent_settingfield) { ?> style="display:none;"<?php } ?>>
                <label class="control-label" for="<?php echo $tabContent->settingfield;?>"><?php echo $tabContent->settinglabel;?></label>
                <div class="controls">
                    <textarea  name="<?php echo $tabContent->settingfield;?>" id="<?php echo $tabContent->settingfield;?>" ><?php echo $tabContent->value;?></textarea>
                  <?php if($tabContent->hint) {  // if($tabContent->hint) { ?>
                                <a href="#" class="tooltiplink" data-original-title="<?php echo $tabContent->hint; ?>"><span class="help-icon"><img src="<?php echo BASE_URL;?>modules/cms/images/help_icon.png"><span></a>
                                                                <?php } ?>
                                            </div>
            </div>
            <?php } else if($tabContent->type=="link") { ?>
             <div class="control-group <?php echo "jq".$tabContent->parent_settingfield;?>" <?php if($currentParentStatus!="1" && $currentParent==$tabContent->parent_settingfield) { ?> style="display:none;"<?php } ?>>
                <label class="control-label" for="<?php echo $tabContent->settingfield;?>"><?php echo $tabContent->settinglabel;?></label>
                <div class="controls">
                    <?php echo $tabContent->value;?>
                      <?php if($tabContent->hint) {  // if($tabContent->hint) { ?>
                                <a href="#" class="tooltiplink" data-original-title="<?php echo $tabContent->hint; ?>"><span class="help-icon"><img src="<?php echo BASE_URL;?>modules/cms/images/help_icon.png"><span></a>
                                                                <?php } ?>
                    <input type="hidden"   name="<?php echo $tabContent->settingfield;?>" value="<?php echo $tabContent->value;?>"></div>
            </div>
             <?php } else if($tabContent->type=="file") { ?>
            <div class="control-group <?php echo "jq".$tabContent->parent_settingfield;?>"  <?php if($currentParentStatus!="1" && $currentParent==$tabContent->parent_settingfield) { ?> style="display:none;"<?php } ?>>
                <label class="control-label" for="<?php echo $tabContent->settingfield;?>"><?php echo $tabContent->settinglabel;?></label>
                <div class="controls">
                    <input type="file" class=""  id="<?php echo $tabContent->settingfield;?>" name="<?php echo $tabContent->settingfield;?>" ><span class="help-inline"><?php echo $tabContent->customColumn->hint; ?></span>
                      <?php if($tabContent->hint) {  // if($tabContent->hint) { ?>
                                <a href="#" class="tooltiplink" data-original-title="<?php echo $tabContent->hint; ?>"><span class="help-icon"><img src="<?php echo BASE_URL;?>modules/cms/images/help_icon.png"><span></a>
                                                                <?php } ?>
                    </div>


            </div>
            <div class="control-group <?php echo "jq".$tabContent->parent_settingfield;?>"  <?php if($currentParentStatus!="1" && $currentParent==$tabContent->parent_settingfield) { ?> style="display:none;"<?php } ?>>

                <div class="controls">
                   <img src="project/files/logo/<?php echo $tabContent->value;?>">
                    </div>


            </div>
           <?php } else if($tabContent->type=="password") {?>
                <div class="control-group <?php echo "jq".$tabContent->parent_settingfield;?>" <?php if($currentParentStatus!="1" && $currentParent==$tabContent->parent_settingfield) { ?> style="display:none;"<?php } ?>>
                <label class="control-label" for="<?php echo $tabContent->settingfield;?>"><?php echo $tabContent->settinglabel;?></label>
                <div class="controls">
                    <input type="password"   id="<?php echo $tabContent->settingfield;?>" name="<?php echo $tabContent->settingfield;?>" value="<?php echo $tabContent->value;?>" >
           <?php if($tabContent->hint) {  // if($tabContent->hint) { ?>
                                <a href="#" class="tooltiplink" data-original-title="<?php echo $tabContent->hint; ?>"><span class="help-icon"><img src="<?php echo BASE_URL;?>modules/cms/images/help_icon.png"><span></a>
                                                                <?php } ?>
          </div>  </div>

            <?php } else if($tabContent->type=="") { ?>
            <div class="control-group <?php echo "jq".$tabContent->parent_settingfield;?>" <?php if($currentParentStatus!="1" && $currentParent==$tabContent->parent_settingfield) { ?> style="display:none;"<?php } ?>>
                <label class="control-label" for="<?php echo $tabContent->settingfield;?>"><?php echo $tabContent->settinglabel;?></label>
                <div class="controls">
                    <input type="text"   id="<?php echo $tabContent->settingfield;?>" name="<?php echo $tabContent->settingfield;?>" value="<?php echo $tabContent->value;?>" >
           <?php if($tabContent->hint) {  // if($tabContent->hint) { ?>
                                <a href="#" class="tooltiplink" data-original-title="<?php echo $tabContent->hint; ?>"><span class="help-icon"><img src="<?php echo BASE_URL;?>modules/cms/images/help_icon.png"><span></a>
                                                                <?php } ?>
          	</div>  </div>

            	<?php } ?>
            <?php } ?>
            <div class="controls"><input class="cancelButton btn cancelButtonSettings" type="button" value="Cancel" name="cancel">
            <input class="submitButton btn" type="submit" value="Save" name="submit"></div>

        </div>

        <?php $loop++;
    } ?>

    </div>
</form>
    <?php  } ?>
    </div>
    </div>
    <br>
    <br>
</div>
